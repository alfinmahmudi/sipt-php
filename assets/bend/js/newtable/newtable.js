$(document).ready(function () {
    var checked = false;
    var listmenu = $("#tb_process").html();
    if ($(".datepickers").length > 0) {
        $(".datepickers").datepicker({
            autoclose: true
        });
    }
    if ($(".datepickers-range").length > 0) {
        $(".datepickers-range").datepicker({weekStart: 1, autoclose: !0, todayHighlight: !0});
    }
    $(".defaulthide").hide();

    $(".chk-advanced").change(function () {
        var $chk = $(this);
        if ($chk.is(":checked")) {
            $(".defaulthide").show();
        } else {
            $(".defaulthide").hide();
        }
        return false;
    });

    $("ul#tb_menus .tbs_menu").click(function () {
        chk = $(".tb_chk:checked").length;
        url = $(this).attr('url');
        jml = $(this).attr('jml');
        met = $(this).attr('met');
        isngajax = $(this).attr('isngajax');
        datapost = $(this).attr('data-form');
        isbody = $(this).attr('data-body');
        if (url == "")
            return false;
        if (chk == 0 && jml != '0') {
            BootstrapDialog.show({
                title: '',
                message: '<p>Maaf, tidak ada data yang di pilih</p>'
            });
            return false;
        }
        if (jml == '1' && chk > 1) {
            BootstrapDialog.show({
                title: '',
                message: '<p>Maaf, data yang bisa di proses atau di pilih hanya 1 (satu)</p>'
            });
            return false;
        }
        if (met == "GET") {
            BootstrapDialog.confirm('Proses data terpilih sekarang ?', function (r) {
                if (r) {
                    if (jml == '0')
                        location.href = url;
                    else
                        location.href = url + '/' + $(".tb_chk:checked").val();
                } else {
                    return false;
                }
            });
        } else if (met == "GETNEW") {
            BootstrapDialog.confirm('Proses data terpilih sekarang ?', function (r) {
                if (r) {
                    if (jml == '0')
                        window.open(url, '_blank');
                    else
                        var arr = new Array();
                    $.each($(".tb_chk:checked"), function () {
                        arr.push($(this).val());
                    });
                    window.open(url + '/' + arr.join(), '_blank');
                } else {
                    return false;
                }
            });
        } else if (met == "BOOTSTRAPDIALOG") {
            BootstrapDialog.confirm('Proses data terpilih sekarang ?', function (r) {
                if (r) {
                    $.get(url + $(".tb_chk:checked").val(), function (hasil) {
                        if (hasil) {
                            BootstrapDialog.show({
                                title: '',
                                type: BootstrapDialog.TYPE_SUCCESS,
                                size: BootstrapDialog.SIZE_WIDE,
                                message: hasil,
                            });
                        }
                    });
                } else {
                    return false;
                }
            });
            return false;
        } else if (met == "POST") {
            if (isngajax)
                var data = $($(this).attr("data-form")).serialize();
            else
                var data = $("#tb_form").serialize();
            BootstrapDialog.confirm('Proses data terpilih sekarang ?', function (r) {
                if (r) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: data,
                        error: function () {
                            BootstrapDialog.show({
                                title: '',
                                message: '<p>Maaf, request halaman tidak ditemukan</p>'
                            });
                            return false;
                        },
                        beforeSend: function () {
                            $("body").append($("<div></div>").attr("class", "overlays"));
                            $('.overlays').css('width', $('body').css('width'));
                            $('.overlays').css('height', $('body').css('height'));
                        },
                        complete: function () {
                            if ($(".overlays").length > 0) {
                                $(".overlays").remove();
                            }
                        },
                        success: function (data) {
                            if (data.search("MSG") >= 0) {
                                arrdata = data.split('#');
                                BootstrapDialog.show({
                                    title: '',
                                    message: '<p>' + arrdata[1] + '</p>'
                                });
                                if (arrdata.length > 1) {
                                    if ($('#useajax').val() == "TRUE" && arrdata[2] == "refresh") {
                                        newhal = parseInt($("#tb_hal").val());
                                        setTimeout(function () {
                                            redirect_url(newhal, true);
                                        }, 1000);
                                    } else if ($('#useajax').val() == "FALSE" && arrdata[2] == "refresh") {
                                        setTimeout(function () {
                                            location.reload(true)
                                        }, 1000);
                                    } else if (arrdata[2] == "redirect") {
                                        setTimeout(function () {
                                            location.href = arrdata[3]
                                        }, 1000);
                                    } else {
                                        if (isngajax) {
                                            setTimeout(function () {
                                                location.reload(true)
                                            }, 1000);
                                        } else {
                                            location.href = arrdata[2];
                                        }
                                    }
                                }
                            } else {
                                BootstrapDialog.show({
                                    title: '',
                                    message: '<p>Proses Gagal</p>'
                                });
                            }
                        }
                    });
                } else {
                    return false;
                }
            });
        } else if (met == "POSTGET") {
            if (isngajax)
                var data = $($(this).attr("data-form")).serialize();
            else
                var data = $("#tb_form").serialize();
            BootstrapDialog.confirm('Proses data terpilih sekarang ?', function (r) {
                if (r) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: data,
                        error: function () {
                            BootstrapDialog.show({
                                title: '',
                                message: '<p>Maaf, request halaman tidak ditemukan</p>'
                            });
                            return false;
                        },
                        beforeSend: function () {
                            $("body").append($("<div></div>").attr("class", "overlays"));
                            $('.overlays').css('width', $('body').css('width'));
                            $('.overlays').css('height', $('body').css('height'));
                        },
                        complete: function () {
                            if ($(".overlays").length > 0) {
                                $(".overlays").remove();
                            }
                        },
                        success: function (data) {
                            $(isbody).html(data);
                            $(".modal-backdrop, .modal, .bootstrap-dialog, .modal-dialog").remove();
                            return false;
                        }
                    });
                } else {
                    return false;
                }
            });
        }
        $(this).val(0);
        return false;
    });


    $("#tb_keycari").change(function () {
        lang = $("#tblang").val();
        var isi = $(this).val();
        var obj = '';


        $("#tb_keycari option:selected").each(function () {
            tipe = $(this).attr('cb');
            urtipe = $(this).attr('urcb');
            datapost = $(this).attr('data-post');
            picker = $(this).attr('picker');
            range = $(this).attr('range');
            pickerdata = $(this).attr('data-picker');
            pickerformat = $(this).attr('data-format');
        });

        /*- Extend */

        var $this = $(this);
        var dataform = $(this).attr('data-form');
        var dataaction = $(this).attr('data-action');
        var datatarget = $(this).attr('data-target');

        var $div = $this.parent().parent().parent().next();
        var $divnext = $div.children().children().next().children();
        var $targetinput = $div.children().children().next().children().children();

        $targetinput.val('');
        /*- End Extend */

        if (tipe) {
            arrtipe = tipe.split(';');
            if (parseInt($this.attr("data-postmethod")) != 1) {
                obj = '<select id="tb_cari" class="form-control input-sm" onchange="bind_tb_cari(\'seltbcari\');">';
            } else {
                obj = '<select id="key_search" class="form-control input-sm key_search" name="key_search">'; // + Onchange Post
            }
            obj += '<option></option>';
            if (urtipe) {
                arrurtipe = urtipe.split(';');
                for (a = 0; a < arrtipe.length; a++) {
                    obj += '<option value="' + arrtipe[a] + '">';
                    obj += arrurtipe[a];
                    obj += '</option>';
                }
            } else {
                for (a = 0; a < arrtipe.length; a++) {
                    obj += '<option value="' + arrtipe[a] + '">';
                    obj += arrtipe[a];
                    obj += '</option>';
                }
            }
            obj += '</select>';
        }
        if (obj != "") {
            if (parseInt($this.attr("data-postmethod")) != 1) {
                $("#tb_cari").replaceWith(obj);
            } else {
                //$("#key_search").replaceWith(obj);
                if (picker) {
                    if ($divnext.hasClass('input-daterange')) {
                        var $str = '<input class="form-control input-sm key_search" type="text" id="key_search" name="key_search" data-date-format = "' + pickerformat + '"><span class="input-group-addon" name="key_search"><i class="fa fa fa-search btnsearch" data-form = "' + dataform + '" data-action = "' + dataaction + '" data-target ="' + datatarget + '" data-post = "TRUE" style="cursor:pointer;" onclick="postobj($(this));"></i></span>';
                        $divnext.html($str);
                        $divnext.removeAttr('data-date-format');
                    }
                    $divnext.attr("class", "input-group col-xs-6");
                    $("#key_search").datepicker();
                } else if (range) {
                    if ($divnext.hasClass('col-xs-6')) {
                        $divnext.attr("class", "input-daterange input-group");
                        $divnext.attr(pickerdata, pickerformat);
                    } else {
                        $divnext.attr("class", "input-daterange input-group");
                        $divnext.attr(pickerdata, pickerformat);
                    }
                    var $str = '<input class="form-control input-sm rangedate" type="text" name="range[]" placeholder="Dari"><span class="input-group-addon">&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;</span><input class="form-control input-sm rangedate" type="text" name="range[]" placeholder="Sampai"><span class="input-group-addon" name="key_search">&nbsp;<i class="fa fa fa-search btnsearch" data-form = "' + dataform + '" data-action = "' + dataaction + '" data-target ="' + datatarget + '" data-post = "TRUE" style="cursor:pointer;" id="btnrange" onclick="postobj($(this));"></i>&nbsp;</span>';
                    $divnext.html($str);
                    $($divnext).datepicker({weekStart: 1, autoclose: !0, todayHighlight: !0});
                } else {
                    if ($divnext.hasClass('col-xs-6') || $divnext.attr('data-date-format')) {
                        $divnext.attr("class", "input-group");
                    } else {
                        //$divnext.attr("class","form-material form-material-primary input-group remove-margin-t remove-margin-b");
                        $divnext.attr("class", "input-group");
                        $("#key_search").datepicker('remove');
                        $("#key_search").removeAttr('data-date-format');
                    }
                    $divnext.removeAttr('data-date-format');
                    $("#key_search").replaceWith(obj);
                }
            }
        } else {
            if (parseInt($this.attr("data-postmethod")) != 1) {
                if (lang == "EN")
                    $("#tb_cari").replaceWith('<input type="text" class="tb_text form-control input-sm" id="tb_cari" title="Type &amp; Press Enter To Search" value="" placeholder="..." onkeypress="return bind_tb_cari(event);" />');
                else
                    $("#tb_cari").replaceWith('<input type="text" class="tb_text form-control input-sm" id="tb_cari" title="Ketik Kata Kunci &amp; Tekan Enter Untuk Mencari" value="" placeholder="..." onkeypress="return bind_tb_cari(event);" />');
                if (picker) {
                    $divnext.attr("class", "form-material form-material-primary input-group remove-margin-t remove-margin-b col-xs-6");
                    $("#tb_cari").attr(pickerdata, pickerformat);
                    $("#tb_cari").datepicker().on('changeDate', function (e) {
                        $("#tb_cari").datepicker('hide');
                        return bind_tb_cari(e);
                    });
                } else {
                    $divnext.attr("class", "form-material form-material-primary input-group remove-margin-t remove-margin-b");
                    $("#tb_cari").datepicker('remove');
                    $("#tb_cari").removeAttr('data-date-format');
                }

            } else {
                if (picker) {
                    if ($divnext.hasClass('input-daterange')) {
                        var $str = '<input class="form-control input-sm key_search" type="text" id="key_search" name="key_search" data-date-format = "' + pickerformat + '"><span class="input-group-addon" name="key_search"><i class="fa fa fa-search btnsearch" data-form = "' + dataform + '" data-action = "' + dataaction + '" data-target ="' + datatarget + '" data-post = "TRUE" style="cursor:pointer;" onclick="postobj($(this));"></i></span>';
                        $divnext.html($str);
                        $divnext.removeAttr('data-date-format');
                    }
                    $divnext.attr("class", "input-group col-xs-6");
                    $("#key_search").datepicker();
                } else if (range) {
                    if ($divnext.hasClass('col-xs-6')) {
                        $divnext.attr("class", "input-daterange input-group");
                        $divnext.attr(pickerdata, pickerformat);
                    } else {
                        $divnext.attr("class", "input-daterange input-group");
                        $divnext.attr(pickerdata, pickerformat);
                    }
                    var $str = '<input class="form-control input-sm rangedate" type="text" name="range[]" placeholder="Dari"><span class="input-group-addon">&nbsp;<i class="fa fa-chevron-right"></i>&nbsp;</span><input class="form-control input-sm rangedate" type="text" name="range[]" placeholder="Sampai"><span class="input-group-addon" name="key_search">&nbsp;<i class="fa fa fa-search btnsearch" data-form = "' + dataform + '" data-action = "' + dataaction + '" data-target ="' + datatarget + '" data-post = "TRUE" style="cursor:pointer;" id="btnrange" onclick="postobj($(this));"></i>&nbsp;</span>';
                    $divnext.html($str);
                    $($divnext).datepicker({weekStart: 1, autoclose: !0, todayHighlight: !0});
                } else {
                    if ($divnext.hasClass('col-xs-6') || $divnext.attr('data-date-format')) {
                        $divnext.attr("class", "input-group");
                    }
                    var $html = '<input class="form-control input-sm key_search" type="text" id="key_search" name="key_search" onkeypress="_keypress($(this));"><span class="input-group-addon" name="key_search"><i class="fa fa fa-search btnsearch" data-form = "' + dataform + '" data-action = "' + dataaction + '" data-target ="' + datatarget + '" data-post = "TRUE" style="cursor:pointer;" onclick="postobj($(this));"></i></span>';
                    $divnext.html($html);
                    $divnext.removeAttr('data-date-format');
                }
            }

        }
    });
    $(".tabelajax tbody tr").mouseover(function () {
        $(this).addClass("hilite");
        if ($(this).next().hasClass('tdmenu')) {
            $(this).next().addClass("hilite");
        } else if ($(this).hasClass('tdmenu')) {
            $(this).prev().addClass("hilite");
        }
    });
    $(".tabelajax tbody tr").mouseout(function () {
        $(this).removeClass("hilite");
        if ($(this).next().hasClass('tdmenu')) {
            $(this).next().removeClass("hilite");
        } else if ($(this).hasClass('tdmenu')) {
            $(this).prev().removeClass("hilite");
        }
    });
    $(".btnsubmenu").click(function (e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.attr("data-modal") || $this.attr("data-append")) {
            $.get($this.attr('act'), function (hasil) {
                if (hasil) {
                    if ($this.attr("data-modal")) {
                        if ($(".modal-backdrop").length === 0) {
                            BootstrapDialog.show({
                                title: '',
                                type: BootstrapDialog.TYPE_SUCCESS,
                                size: BootstrapDialog.SIZE_WIDE,
                                message: hasil
                            });
                            return false;
                        }
                    }
                    if ($this.attr("data-append")) {
                        $($this.attr("data-class")).fadeOut();
                        $($this.attr("data-div")).fadeIn(function () {
                            $($this.attr("data-div")).html(hasil);
                            $($this.attr("data-div")).css("margin", "10px");
                        });
                    }
                    return false;
                }
            });
        } else {
            location.href = $(this).attr('act');
        }
        return false;
    });
    $(".tb_chk").click(function () {
        checked = true;
        curtr = $(this).parent().parent();
        if (!this.checked) {
            curtr.removeClass("selected");
            if (curtr.next().hasClass('tdmenu')) {
                curtr.next().removeClass("selected");
                curtr.next().children().children().last().html('');
            } else if (curtr.hasClass('tdmenu')) {
                curtr.prev().removeClass("selected");
                curtr.children().children().last().html('');
            }
            if ($(".tabelajax input:checkbox.tb_chk:checked").length == 1)
                $(".tabelajax input:checkbox.tb_chk:checked").parent().parent().next().children().children().last().html(listmenu);
            $("#tb_chkall").attr('checked', this.checked);
        } else {
            curtr.addClass("selected");
            if (curtr.next().hasClass('tdmenu')) {
                curtr.next().addClass("selected");
                if ($(".tabelajax input:checkbox.tb_chk:checked").length == 1)
                    curtr.next().children().children().last().html(listmenu);
                else
                    $(".tabelajax tr td span.tb_process").html('');
            } else if (curtr.hasClass('tdmenu')) {
                curtr.prev().addClass("selected");
                if ($(".tabelajax input:checkbox.tb_chk:checked").length == 1)
                    curtr.children().children().last().html(listmenu);
                else
                    $(".tabelajax tr td span.tb_process").html('');
            }
            if ($(".tabelajax input:checkbox.tb_chk:checked").length == $(".tabelajax input:checkbox:not(#tb_chkall)").length)
                $("#tb_chkall").attr('checked', this.checked);
        }
    });
    $(".tabelajax tbody td").click(function (btn) {
        var urldtl = '';
        if ($("#tb_chkall").attr('id')) {
            if (checked) {
                checked = false;
                return true;
            }
            if (!btn.ctrlKey) {
                $(".tabelajax tr").removeClass("selected");
                $(".tabelajax input:checkbox").attr('checked', false);
                $(".tabelajax tr td span.tb_process").html('');
            }
            $(this).parent().addClass("selected");
            if ($(this).parent().next().hasClass('tdmenu')) {
                $(this).parent().children().children().attr('checked', true);
                $(this).parent().next().addClass("selected");
                if ($(".tabelajax input:checkbox.tb_chk:checked").length == 1)
                    $(this).parent().next().children().children().last().html(listmenu);
                else
                    $(".tabelajax tr td span.tb_process").html('');
            } else if ($(this).parent().hasClass('tdmenu')) {
                $(this).parent().prev().children().children().attr('checked', true);
                $(this).parent().prev().addClass("selected");
                if ($(".tabelajax input:checkbox.tb_chk:checked").length == 1)
                    $(this).parent().children().children().last().html(listmenu);
                else
                    $(".tabelajax tr td span.tb_process").html('');
            }
            if ($(".tabelajax input:checkbox.tb_chk:checked").length == $(".tabelajax input:checkbox:not(#tb_chkall)").length)
                $("#tb_chkall").attr('checked', true);
        } else {
            if (checked) {
                checked = false;
                return true;
            }
            $(".tabelajax tr").removeClass("selected");
            $(".tabelajax input:checkbox").attr('checked', false);
            $(this).parent().addClass("selected");
            if ($(this).parent().next().hasClass('tdmenu')) {
                $(this).parent().next().addClass("selected");
            } else if ($(this).parent().hasClass('tdmenu')) {
                $(this).parent().prev().addClass("selected");
            }
            $(this).parent().children().children().attr('checked', true);
            if ($(".tabelajax input:checkbox.tb_chk:checked").length == $(".tabelajax input:checkbox:not(#tb_chkall)").length)
                $("#tb_chkall").attr('checked', true);
        }
        return true;
    });
    $(".tabelajax tr.tdmenu td a.sub-menu").click(function () {
        var urldtl = '';
        var trnow = $(this).parent().parent();
        if ($("#tb_chkall").attr('id')) {
            if (checked) {
                checked = false;
                return true;
            }
            if (trnow.prev().attr('urldetil'))
                urldtl = trnow.prev().attr('urldetil');
            var jmltd = $('td', trnow.prev()).length;
            var cls = trnow.attr('class');
            if (urldtl != '') {
                cls = cls.replace('hilite', '');
                cls = cls.replace('tdmenu', '');
                $('#newtr').remove();
                trnow.after('<tr id="newtr" class="' + cls + '"><td id="filltd" colspan="' + jmltd + '"></td></tr>');
                $('#filltd').html('Loading..');
                urldtl = $('#urldtl').val() + urldtl;
                $.get(urldtl, function (data) {
                    $('#filltd').html(data);
                    $('#newtr').removeClass("selected");
                });
            }
        }
        return false;
    });
    $("#tb_chkall").click(function () {
        $(".tabelajax").find(':checkbox').attr('checked', this.checked);
        $('#newtr').remove();
        $(".tabelajax tr td span.tb_process").html('');
        if (!this.checked) {
            $(".tabelajax input:checkbox:not(#tb_chkall)").parent().parent().removeClass("selected");
            $(".tabelajax input:checkbox:not(#tb_chkall)").parent().parent().next().removeClass("selected");
        } else {
            $(".tabelajax input:checkbox:not(#tb_chkall)").parent().parent().addClass("selected");
            $(".tabelajax input:checkbox:not(#tb_chkall)").parent().parent().next().addClass("selected");
        }
    });
    $("ul.nav-kanan li a.page").click(function () {
        if (!$(this).attr("data-post")) {
            newhal = parseInt($(this).html());
            redirect_url(newhal);
        } else {
            $("input[name='tb_hal']").val(parseInt($(this).html()));
            if ($(".btnsearch").length > 0)
                $(".btnsearch").click();
            else
                postobj($(this));
        }
        return false;
    });
    $("ul.nav-kiri li a.per").click(function () {
        if (!$(this).attr("data-post")) {
            $("#tb_view").val(parseInt($(this).html()));
            newhal = parseInt($("#tb_hal").val());
            redirect_url(newhal);
        } else {
            $("input[name='tb_view']").val(parseInt($(this).html()));
            if ($(".btnsearch").length > 0)
                $(".btnsearch").click();
            else
                postobj($(this));
        }
        return false;
    });
    $(".order").click(function () {
        if ($(this).attr("orderby")) {
            if (!$(this).attr("data-post")) {
                $("#orderby").val($(this).attr("orderby"));
                $("#sortby").val($(this).attr("sortby"));
                redirect_url($("#tb_hal").val());
            } else {
                $("input[name='orderby']").val($(this).attr("orderby"));
                $("input[name='sortby']").val($(this).attr("sortby"));
                if ($(".btnsearch").length > 0)
                    $(".btnsearch").click();
                else
                    postobj($(this));
            }
            return false;
        }
    });
    $("#tb_cari").bind('keypress', function (kode) {
        return bind_tb_cari(kode);
    });
    $(".tb_keycariadv").bind('keypress', function (kode) {
        return bind_tb_cari(kode);
    });
    $('.key_search, .keywords').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == 13) {
            $(".btnsearch").click();
            return false;
        }
        event.stopPropagation();
    });
    $(".keywords").change(function (e) {
        var $e = $(this);
        if ($e.attr("data-url")) {
            var $parent = $e.parent().parent().parent();
            var $target = $parent.next().children().children().children().attr("id");
            $.get($e.attr("data-url") + $e.val(), function ($respon) {
                if ($respon) {
                    $('#' + $target).html($respon);
                }
            });
        } else {
            return false;
        }
    });
    $(".btnreload").click(function (e) {
        if ($(".btnsearch").length > 0)
            $(".btnsearch").click();
        else
            postobj($(this));
        return false;
    });
    $(".btnsearch").click(function (e) {
        var $this = $(this);
        var $data = $($this.attr("data-form")).serializeArray();
        $data.push({name: "data-post", value: $this.attr("data-post")});
        $.ajax({
            type: 'POST',
            url: $this.attr("data-action"),
            data: $data,
            error: function () {
                BootstrapDialog.show({
                    title: '',
                    message: '<p>Maaf, request halaman tidak ditemukan</p>'
                });
                return false;
            },
            beforeSend: function () {
                if ($("#progress").length === 0) {
                    $($this.attr("data-target")).append($("<div></div>").attr("class", "overlays"))
                    $($this.attr("data-target")).append($("<div><dt/><dd/></div>").attr("id", "progress-ojan"));
                    $("#progress").width((50 + Math.random() * 30) + "%");
                }
            },
            complete: function () {
                if ($(".overlays").length > 0) {
                    $(".overlays").remove();
                }
            },
            success: function (data) {
                $($this.attr("data-target")).html(data);
            }
        });
        return false;
    });
    $(".btnreset").click(function () {
        $(".keywords").val("");
        return false;
    });
});

function do_post(obj) {
    var url = obj.attr('act');
    var met = obj.attr('met');
    var jml = obj.attr('jml');
    if (met == "GET") {
        var _val = $(".tb_chk:checked").val();
        BootstrapDialog.confirm('Proses data terpilih sekarang ?', function (r) {
            if (r) {
                if (jml == '0')
                    location.href = url;
                else
                    location.href = url + '/' + _val;
            } else {
                return false;
            }
        });
    } else if (met == "GETNEW") {
        var arr = new Array();
        $.each($(".tb_chk:checked"), function () {
            arr.push($(this).val());
        });
        BootstrapDialog.confirm('Proses data terpilih sekarang ?', function (r) {
            if (r) {
                if (jml == '0')
                    window.open(url, '_blank');
                else
                    window.open(url + '/' + arr.join(), '_blank');
            } else {
                return false;
            }
        });
    } else if (met == "BOOTSTRAPDIALOG") {
        BootstrapDialog.confirm('Proses data terpilih sekarang ?', function (r) {
            if (r) {
                $.get(url + $(".tb_chk:checked").val(), function (hasil) {
                    if (hasil) {
                        BootstrapDialog.show({
                            title: 'Detil Data',
                            type: BootstrapDialog.TYPE_PRIMARY,
                            size: BootstrapDialog.SIZE_WIDE,
                            message: hasil,
                        });
                    }
                });
            } else {
                return false;
            }
        });
        return false;
    } else if (met == "POST") {
        var data = $("#tb_form").serialize();
        BootstrapDialog.confirm('Proses data terpilih sekarang ?', function (r) {
            if (r) {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    error: function () {
                        BootstrapDialog.show({
                            title: '',
                            message: '<p>Maaf, request halaman tidak ditemukan</p>'
                        });
                        return false;
                    },
                    beforeSend: function () {
                        $("body").append($("<div></div>").attr("class", "overlays"));
                        $('.overlays').css('width', $('body').css('width'));
                        $('.overlays').css('height', $('body').css('height'));
                    },
                    complete: function () {
                        if ($(".overlays").length > 0) {
                            $(".overlays").remove();
                        }
                    },
                    success: function (data) {
                        if (data.search("MSG") >= 0) {
                            arrdata = data.split('#');
                            BootstrapDialog.show({
                                title: '',
                                message: '<p>' + arrdata[1] + '</p>'
                            });
                            if (arrdata.length > 1) {
                                if ($('#useajax').val() == "TRUE" && arrdata[2] == "refresh") {
                                    newhal = parseInt($("#tb_hal").val());
                                    redirect_url(newhal, true);
                                } else if ($('#useajax').val() == "FALSE" && arrdata[2] == "refresh") {
                                    setTimeout(function () {
                                        location.reload(true)
                                    }, 1000);
                                } else {
                                    location.href = arrdata[2];
                                }
                            }
                        } else {
                            BootstrapDialog.show({
                                title: '',
                                message: '<p>Proses Gagal</p>'
                            });
                        }
                    }
                });
            } else {
                return false;
            }
        });
    }
    return false;
}

function bind_tb_cari(eve) {
    if ($("#searchtipe").val() == 1) {
        if (eve.type == 'keypress' || eve == "seltbcari") {
            if (eve.keyCode == 13 || eve == "seltbcari") {
                if ($("#tb_cari").is('select')) {
                    $("#tb_keycari").after('<input type="hidden" class="tb_keycariadv" id="tb_keycari' + $("#tb_keycari").val() + '-' + $(".tb_keycariadv").length + '" value="' + $("#tb_cari option:selected").val().replace('/', '') + '">');
                } else {
                    $("#tb_keycari").after('<input type="hidden" class="tb_keycariadv" id="tb_keycari' + $("#tb_keycari").val() + '-' + $(".tb_keycariadv").length + '" value="' + $("#tb_cari").val().replace('/', '') + '">');
                }
                newhal = parseInt($("#tb_hal").val());
                redirect_url(newhal);
                return false;
            }
        }
    } else {
        if (eve.keyCode == 13) {
            newhal = parseInt($("#tb_hal").val());
            redirect_url(newhal);
            return false;
        }
    }
}

function remove_filter(obj) {
    obj.remove();
    newhal = parseInt($("#tb_hal").val());
    redirect_url(newhal);
    return false;
}

function redirect_url(newhal, rload) {
    rload = typeof rload !== 'undefined' ? rload : false;
    newlocation = 'row/' + $("#tb_view").val() + '/page/' + newhal + '/order/' + $("#orderby").val() + '/' + $("#sortby").val();
    if ($(".tb_keycariadv").length > 0) {
        var skey = 0;
        var keysrc = '';
        var txtsrc = '';
        var arrkey = new Array();
        var arrsrc = new Array();
        var tmpspr = '';
        var carr = 0;
        $(".tb_keycariadv").each(function () {
            arrkey[carr] = $(this).attr('id').replace('tb_keycari', '');
            arrsrc[$(this).attr('id').replace('tb_keycari', '')] = $(this).val();
            carr++;
        });
        arrkey.sort();
        for (skey; skey < arrkey.length; skey++) {
            var ckey = arrkey[skey];
            var ckeytemp = ckey.split('-');
            var ckeys = parseInt(ckeytemp[0]);
            if (ckeys >= 0) {
                txtsrc += tmpspr + arrsrc[ckey];
                keysrc += tmpspr + ckeys;
                if (tmpspr == '')
                    tmpspr = '|';
            }
        }
        if (keysrc != "" || txtsrc != "") {
            newlocation += '/search/' + ReplaceAll(keysrc, '%7C', '|') + '/' + ReplaceAll(txtsrc, '%20', ' ');
        }
    }
    if ($('#useajax').val() == "TRUE") {
        $("#tb_hal").val(newhal);
        newlocation = fctrim('ajax/' + newlocation);
        if (rload == true)
            window.location.hash = newlocation;
        else if (window.location.hash != newlocation)
            window.location.hash = newlocation;
        newlocation = $("#tb_form").attr('action') + '/' + newlocation;
        if ($("#progress-ojan").length === 0) {
            $('.responsive').append($("<div></div>").attr("class", "overlays"));
            $('.responsive').append($("<div><dt/><dd/></div>").attr("id", "progress-ojan"));
            $("#progress-ojan").width((50 + Math.random() * 30) + "%");
        }
        $.get(newlocation, function (data) {
            $('#divtabelajax').html(data);
            $("#progress-ojan").width("101%").delay(200).fadeOut(400, function () {
                $(this).remove();
            });
            if ($(".overlays").length > 0) {
                $(".overlays").remove();
            }
        });
        return false;
    } else {
        newlocation = fctrim($("#tb_form").attr('action') + '/' + newlocation);
        location.href = newlocation;
    }
}

function fctrim(str) {
    str = str.replace(/^\s+/, '');
    for (var i = str.length - 1; i >= 0; i--) {
        if (/\S/.test(str.charAt(i))) {
            str = str.substring(0, i + 1);
            break;
        }
    }
    return str;
}

/* Extend Code */
function order(a) {
    var $this = $(a);
    if ($this.attr("orderby") || $this.attr("data-post")) {
        $("input[name='orderby']").val($this.attr("orderby"));
        $("input[name='sortby']").val($this.attr("sortby"));
        if ($(".btnsearch").length > 0)
            $(".btnsearch").click();
        else
            postobj($this);
    }
    return false;
}
function view(b) {
    var $this = $(b);
    if ($this.attr("data-post")) {
        $("input[name='tb_view']").val(parseInt($this.html()));
        if ($(".btnsearch").length > 0)
            $(".btnsearch").click();
        else
            postobj($this);
    }
    return false;
}
function nextprevpage(c) {
    var $this = $(c);
    if ($this.attr("data-post")) {
        $("input[name='tb_hal']").val(parseInt($this.html()));
        if ($(".btnsearch").length > 0)
            $(".btnsearch").click();
        else
            postobj($this);
    }
    return false;
}
function postobj(d) {
    console.log(d);
    var $this = $(d);
    var $data = $($this.attr("data-form")).serializeArray();
    $data.push({name: "data-post", value: $(d).attr("data-post")});
    $.ajax({
        type: 'POST',
        url: $(d).attr("data-action"),
        data: $data,
        error: function () {
            BootstrapDialog.show({
                title: '',
                message: '<p>Maaf, request halaman tidak ditemukan</p>'
            });
            return false;
        },
        beforeSend: function () {
            if ($("#progress").length === 0) {
                $($this.attr("data-target")).append($("<div></div>").attr("class", "overlays"));
                $($this.attr("data-target")).append($("<div><dt/><dd/></div>").attr("id", "progress-ojan"));
                $("#progress").width((50 + Math.random() * 30) + "%");
            }
        },
        complete: function () {
            if ($(".overlays").length > 0) {
                $(".overlays").remove();
            }
        },
        success: function (data) {
            $($this.attr("data-target")).html(data);
        }
    });
    return false;
}
function expand(e) {
    var $checked = false;
    var $this = $(e);
    var $exrow = $this.parent().parent();
    var $urldtl = '';
    /*if($("#tb_chkall").attr('id')){     
     if($checked){ 
     $checked = false; 
     return true;     
     }*/
    if ($exrow.attr('urldetil')) {
        if ($exrow.attr('urldetil') != '') {
            $('#newtr').remove();
            var $jmltd = $('td', $exrow).length;
            var $addtd = '';
            var $cls = '';
            if ($(this).parent().children().first().attr('class'))
                $cls = ' class="' + $(this).parent().children().first().attr('class') + '"';
            $exrow.after('<tr id="newtr">' + $addtd + '<td id="filltd" style="font-size:12px;" colspan="' + $jmltd + '"' + $cls + '></td></tr>');
            $('#filltd').html('Loading..');
            $urldtl = $('#urldtl').val() + $exrow.attr('urldetil');
            $.get($urldtl, function (data) {
                $('#filltd').html(data);
                $('#newtr').removeClass("selected");
            });
        }
    }
    //} 
    return false;
}
function modalrow(f) {
    var $this = $(f);
    if ($this.attr("data-url")) {
        $.get($this.attr("data-url"), function (hasil) {
            if (hasil) {
                if ($(".modal-backdrop").length === 0) {
                    BootstrapDialog.show({
                        title: '',
                        type: BootstrapDialog.TYPE_PRIMARY,
                        size: BootstrapDialog.SIZE_WIDE,
                        message: hasil,
                    });
                }
            }
        });
    }
}
function _keypress(g) {
    $(g).keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == 13) {
            $(".btnsearch").click();
            return false;
        }
        event.stopPropagation();
    });
}
function selectedrow(h) {
    var $this = $(h);
    if ($this.attr("data-target") && $this.attr("data-retrive")) {
        if ($this.attr("data-url-callback") && $this.attr("data-field-callback")) {
            $.get($this.attr("data-url-callback"), function ($hasil) {
                if ($hasil) {
                    var $arrdata = $hasil.split('|');
                    var $field = $this.attr("data-field-callback").split('.');
                    for (var $x = 0; $x < $arrdata.length; $x++) {
                        var $object = $("#" + $field[$x]).get(0).tagName;
                        if ($object === 'INPUT' || $object === 'SELECT' || $object === 'TEXTAREA') {
                            $("#" + $field[$x]).val($arrdata[$x]);
                        } else {
                            $("#" + $field[$x]).html($arrdata[$x]);
                        }
                    }
                }
            });
        } else {
            var $tmpids = $this.attr("data-target").split('|');
            var $tdresponse = $this.attr("data-retrive").split('|');
            for (var $i = 0; $i < $tmpids.length; $i++) {
                var $tipe = $("#" + $tmpids[$i]).get(0).tagName;
                if ($tipe === 'INPUT' || $tipe === 'SELECT') {
                    $("#" + $tmpids[$i]).val($tdresponse[$i]);
                } else {
                    $("#" + $tmpids[$i]).html($tdresponse[$i]);
                }
            }
        }
        $(".modal-backdrop, .modal, .bootstrap-dialog, .modal-dialog").remove();
    } else {
        return false;
    }
    return false;
}
function _chkall(i) {
    var $this = $(i);
    var $target = $this.attr("data-body");
    if ($this.is(":checked")) {
        $($target).find(':checkbox').prop('checked', true);
    } else {
        $($target).find(':checkbox').prop('checked', false);
    }
    return false;
}
function closepanel(j) {
    var $this = $(j);
    BootstrapDialog.confirm('Anda yakin akan menutup form berikut ?', function (r) {
        if (r) {
            $($this.attr("data-show")).fadeIn();
            $($this.attr("data-closed")).fadeOut(function () {
                $($this.attr("data-closed")).html('');
            });
        } else {
            return false;
        }
    });
}
function appendrow(k) {
    var $this = $(k);
    var $exrow = $this.closest("tr");
    var $jmltd = $('td', $exrow).length;
    if ($this.attr("data-url")) {
        $('#newtr').remove();
        var $addtd = '';
        var $cls = '';
        $exrow.after('<tr id="newtr">' + $addtd + '<td id="filltd" style="font-size:12px;" colspan="' + $jmltd + '"' + $cls + '></td></tr>');
        $('#filltd').html('Loading..');
        $.get($this.attr("data-url"), function (data) {
            $('#filltd').html(data);
            $('#newtr').removeClass("selected");
        });

    }
    return false;
}

function showlog(l) {
    var $this = $(l);
    if ($this.attr("data-url")) {
        $.get($this.attr("data-url"), function (hasil) {
            if (hasil) {
                if ($(".modal-backdrop").length === 0) {
                    BootstrapDialog.show({
                        title: '',
                        type: BootstrapDialog.TYPE_PRIMARY,
                        size: BootstrapDialog.SIZE_WIDE,
                        message: hasil,
                    });
                }
            }
        });
    }
    return false;
}

function ReplaceAll(Source, stringToFind, stringToReplace) {
    var temp = Source;
    var index = temp.indexOf(stringToFind);
    while (index != -1) {
        temp = temp.replace(stringToFind, stringToReplace);

        index = temp.indexOf(stringToFind);
    }
    return temp;
}
/* End Extend Code */