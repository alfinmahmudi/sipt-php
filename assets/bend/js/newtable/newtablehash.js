$(document).ready(function(){
	readhash();
	$(window).bind('hashchange', function(){
		readhash();
	});
});

function readhash(){
	var urlgo = window.location.hash.replace('#', '').split('/');
	if(urlgo.length>1){
		if(urlgo[0]=='ajax' && urlgo.length>7){
			$("#tb_view").val(parseInt(urlgo[2]));
			$("#orderby").val(parseInt(urlgo[6]));
			$("#sortby").val(urlgo[7]);
			if(urlgo.length==11){
				$(".tb_keycariadv").remove();
				var arrkey = urlgo[9].split('|');
				var txtkey = urlgo[10].split('|');
				if($("#searchtipe").val() == 1){
					for(var x=0;x<arrkey.length;x++){
						$("#tb_keycari").after('<input type="hidden" class="tb_keycariadv" id="tb_keycari' + arrkey[x] + '" value="' + txtkey[x] + '">');
					}
				}else if($("#searchtipe").val() == "N"){
					var arrlbl = new Array();
					var keylbl = 0;
					var panjang = $(".srcpanel > div").length;
					$(".srcpanel>label").each(function(){
						arrlbl[keylbl] = $(this).html();
						keylbl++;
                    });
					var tmppanel = '<div class="row srcpanel">';
					for(var y=0;y<panjang;y++){
						tmppanel += '<label class="control-label col-lg-3" id="lbl_'+y+'">'+arrlbl[y]+'</label>';
						tmppanel += '<div class="col-lg-3" id="'+y+'"><input id="tb_keycari'+arrkey[y]+'-'+arrkey[y]+'" class="tb_keycariadv form-control input-sm m-bot15" type="text" value="'+unescape(txtkey[y])+'" placeholder="Berdasarkan '+arrlbl[y]+'" data-original-title="Info" data-placement="right" data-trigger="hover"></div>';
					}
					tmppanel += '</div>';
					$("#search-panel").html(tmppanel);
				}
			}
			redirect_url(parseInt(urlgo[4]));
		}
	}
}

function fctrim(str){
	str = str.replace(/^\s+/, '');
	for(var i = str.length - 1; i >= 0; i--){
		if(/\S/.test(str.charAt(i))){
			str = str.substring(0, i + 1);
			break;
		}
	}
	return str;
}