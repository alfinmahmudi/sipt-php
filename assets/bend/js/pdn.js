/*
- Riel.js
- Application Name : SIPT PDN
- Author : DEV EDII
- Last Modified : 13 Augst 2015
- Modified By : Syafrizal
*/
$(document).ready(function(){
});
function post(a){
	var $form = $(a);
	var $wajib = 0;
	$.each($("input:visible, select:visible, textarea:visible"), function(){
		if($(this).attr('wajib')){
			if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
				$(this).css('border-color','#b94a48');
				$wajib++;
			}
		}
	});
	if($wajib > 0){
		BootstrapDialog.show({
			title: 'Informasi',
			type: BootstrapDialog.TYPE_WARNING,
			message: '<p>Ada <b>'+$wajib+'</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>'
		});
		return false;
	}else if($wajib == -1){
		
	}else{
		if($form.attr("data-report") || $form.attr("data-registrasi")){
			
			BootstrapDialog.confirm('Apakah anda yakin dengan data yang Anda isikan ?', function(r){
				if(r){
					$form.submit();
				}else{
					return false;
				}
			});
			return false;
		}else{
			BootstrapDialog.confirm('Apakah anda yakin dengan data yang Anda isikan ?', function(r){
				if(r){
					$.ajax({
						type: "POST",
						url: $(a).attr('action') + '/ajax',
						data: $(a).serialize(),
						error: function() {
							BootstrapDialog.show({
								title: 'Informasi',
								type: BootstrapDialog.TYPE_DANGER,
								message: '<p>Maaf, request halaman tidak ditemukan</p>'
							});
						},
						beforeSend: function(){
							if($("#progress").length === 0){
								$("body").append($("<div id=\"progress\"><dt/><dd/></div><div class=\"overlays\"></div>"));
								$('#progress').width((50 + Math.random() * 30) + "%");
								$('.overlays').css('width', $('body').css('width'));
								$('.overlays').css('height', $('body').css('height'));
							}
						},
						complete: function(){
							$("#progress").width("101%").delay(200).fadeOut(400, function(){
								$("#progress").remove();
								$(".overlays").remove();
							});
						},
						success: function(data) {
							if (data.search("MSG") >= 0) {
								arrdata = data.split('||');
								if (arrdata[1] == "YES") {
									$(".overlays").remove();
									BootstrapDialog.show({
										title: 'Informasi',
										type: BootstrapDialog.TYPE_SUCCESS,
										message: '<p>'+arrdata[2]+'</p>'
									});
									if(arrdata.length > 3){
									   setTimeout(function(){
										   if($f.attr("data-redirect")){
											   location.href = arrdata[3];
										   }else{
										   }
										}, 2000);
									}
								} else if (arrdata[1] == "NO") {
									BootstrapDialog.show({
										title: 'Informasi',
										type: BootstrapDialog.TYPE_DANGER,
										message: '<p>'+arrdata[2]+'</p>'
									});
								}
							}
						}
					});
				}else{
					return false;
				}
			});
		}
	}
}
function uploads(f){
	var $obj = f, $parent = $obj.parent(), $loading = $obj.parent().next().next(), $return = $obj.parent().next();
	if($obj.attr("wajib")) $obj.removeAttr("wajib");
	if($loading.css('display') == 'none') $loading.show();
	$parent.css('display','none');
	$loading.html('Data sedang di upload, mohon menunggu ...');
	$("body").append($("<div></div>").attr("class", "overlays"));
	$('.overlays').css('width', $('body').css('width'));
	$('.overlays').css('height', $('body').css('height'));
	$.ajaxFileUpload({
		url: $obj.attr("url") +'/'+ $obj.attr("allowed"),
		secureuri: false,
		fileElementId: $obj.attr("id"),
		dataType: "json",
		success: function(data){
			var arrdata = data.msg.split("#");
			if(typeof(data.error) != "undefined"){
				if(data.error != ""){
					$(".overlays").remove();
					BootstrapDialog.show({
						title: 'Informasi',
						type: BootstrapDialog.TYPE_WARNING,
						message: '<p>'+data.error+'</p>'
					});
					$loading.html('');
					if($parent.css('display') == 'none') $parent.css('display','');
				}else{
					$return.show();
					setTimeout(function(){
						$loading.html('');
						$loading.hide(function(){
							$return.html(arrdata[0]);
							$(".overlays").remove();
						});
					},2000);
				}
			}
		},
		error: function (data, status, e){
			BootstrapDialog.show({
				title: 'Informasi',
				message: '<p>'+e+'</p>'
			});
			$(".overlays").remove();
			if($loading.css('display') == '') $loading.hide();
			if($parent.css('display') == 'none') $parent.css('display','');
		}
	});
}
function remove_uploads(g){
	var $obj = g, $curr = $obj.parent(), $tmp = $obj.parent().prev(), $loading = $obj.parent().next(), $file = $tmp.children();
	BootstrapDialog.confirm('Apakah anda yakin akan menghapus data tersebut ?', function(r){
		if(r){
			$tmp.show(); $file.attr("wajib"); $curr.html(''); $curr.hide();
		}else{
			return false;
		}
	});
	return false;
}
function redirectblank(k){
	var $this = $(k);
	window.open($this.attr("data-url"), '_blank');
	return false;
}

function popuptabel(l){
	var $this = $(l);
	if($this.attr("data-url")){
		var $url = "";
		if($this.attr("data-callback")) $url = $this.attr("data-url") + $this.attr("data-target") + '/' + $this.attr("data-callback") + '/' + $this.attr("data-fieldcallback");
		else $url = $this.attr("data-url") + $this.attr("data-target");
		$.get($url, function(hasil){
			if(hasil){
				if($(".modal-backdrop").length === 0){
					BootstrapDialog.show({
						title: $this.attr("data-title"),
						type: BootstrapDialog.TYPE_SUCCESS,
						size : BootstrapDialog.SIZE_WIDE,
						message: hasil,
					});
				}
			}
		});
		return false;
	}else{
		return false;
	}
}

function combobox(m, n){
	var $this = $(m);
	var $target = $(n);
	if($this.attr("data-url")){
		$.get($this.attr("data-url") + $this.val(), function($result){
			if($result){
				$target.html($result);
			}
		});
	}else{
		return false;
	}
	return false;
}

function redirect(o){
	var $this = $(o);
	if($this.attr("data-url")){
		location.href = $this.attr("data-url");
	}else{
		return false;
	}
}

function ReplaceAll(Source,stringToFind,stringToReplace){
	var temp = Source;
	var index = temp.indexOf(stringToFind);
	while(index != -1){
		temp = temp.replace(stringToFind,stringToReplace);

		index = temp.indexOf(stringToFind);
	}
	return temp;
}