/*
- sipt.js
- Application Name : SIPT PDN Kemendag
- Author : DEV EDII
- Last Modified : 13 Augst 2015
- Modified By : Syafrizal
*/

$(document).ready(function(e){
    $(".log-izin").click(function(){
        var $this = $(this);
		if($this.attr("data-url")){
			$.get($this.attr("data-url"), function($respon){
				$($this.attr("data-target")).html($respon);
			});
		}
    });
});
function post(a){
	var $form = $(a);
	var $wajib = 0;
	var $msg = "";
	if($form.attr("data-hidden")){
		$.each($("input:hidden"), function(){
			if($(this).attr('wajib')){
				if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
					$wajib++;
				}
			}
		});
		$msg='<p>Ada <b>'+$wajib+'</b> Dokumen persyaratan yang harus dilengkapi. <br>Silahkan periksa kembali isian form anda.</p>';
	}

	else if($form.attr("data-siupagen")){
		$.each($("input:visible, input:hidden,select:visible, textarea:visible"), function(){
			if($(this).attr('wajib')){
				if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
					$(this).css('border-color','#b94a48');
					$wajib++;
				}
			}
		});
		$msg = '<p>Ada <b>'+$wajib+'</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>';
	}
	else{
		$.each($("input:visible, select:visible, textarea:visible"), function(){
			if($(this).attr('wajib')){
				if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
					$(this).css('border-color','#b94a48');
					$wajib++;
				}
			}
		});
		$msg = '<p>Ada <b>'+$wajib+'</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>';
	}
	if($wajib > 0){
		BootstrapDialog.show({
			title: '',
			type: BootstrapDialog.TYPE_WARNING,
			message: $msg
		});
		return false;
	}else{
		if($form.attr("data-report") || $form.attr("data-registrasi")){
			
			BootstrapDialog.confirm('Apakah anda yakin dengan data yang Anda isikan ?', function(r){
				if(r){
					$form.submit();
				}else{
					return false;
				}
			});
			return false;
		}else{
			BootstrapDialog.confirm('Apakah anda yakin dengan data yang Anda isikan ?', function(r){
				if(r){
					$.ajax({
						type: "POST",
						url: $(a).attr('action') + '/ajax',
						data: $(a).serialize(),
						error: function() {
							BootstrapDialog.show({
								title: '',
								type: BootstrapDialog.TYPE_DANGER,
								message: '<p>Maaf, request halaman tidak ditemukan</p>'
							});
						},
						beforeSend: function(){
							if($("#progress").length === 0){
								$("body").append($('<div id=\"progress\"></div><div class=\"overlays\"><div class="la-square-spin" style="margin-left:5px; margin-top: 10px; position: fixed; z-index: 19832509; color: #63c5ab;"><div>Loading ... </div></div></div>'));
								$('#progress').width((50 + Math.random() * 30) + "%");
								$('.overlays').css('width', $('body').css('width'));
								$('.overlays').css('height', $('body').css('height'));
							}
						},
						complete: function(){
							$("#progress").width("101%").delay(200).fadeOut(400, function(){
								$("#progress").remove();
								$(".overlays").remove();
							});
						},
						success: function(data) {
							if (data.search("MSG") >= 0) {
								arrdata = data.split('||');
								if (arrdata[1] == "YES") {
									$(".overlays").remove();
									BootstrapDialog.show({
										title: '',
										type: BootstrapDialog.TYPE_SUCCESS,
										message: '<p>'+arrdata[2]+'</p>'
									});
									if(arrdata.length > 3){
										if(arrdata[3] == "REFRESH"){
											setTimeout(function(){location.reload(true);}, 2000);
										}else{
										   setTimeout(function(){
											   if($form.attr("data-redirect")){
												   location.href = arrdata[3];
											   }else{
												   //Belum didefinisikan.
											   }
											}, 2000);
										}
									}
								} else if (arrdata[1] == "NO") {
									BootstrapDialog.show({
										title: '',
										type: BootstrapDialog.TYPE_DANGER,
										message: '<p>'+arrdata[2]+'</p>'
									});
								}
							}
						}
					});
				}else{
					return false;
				}
			});
		}
	}
}

function divobj(p){
	var $this = $(p);
	var $wajib = 0;
	var $check = 0;
	//$.each($("input:visible, select:visible, textarea:visible"), function(){
	if($this.attr("data-agreement")){
		$.each($("input:checkbox"), function(){
			if($(this).attr('check-agreement')){
				if($(this).attr('check-agreement')=="yes" && ($(this).is(':checked') || $(this).val()==null)){
					$('input:visible, input:checkbox, select:visible, textarea:visible', p).each(function(){
						if($(this).attr('wajib')){
							if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
								$(this).css('border-color','#b94a48');
								$wajib++;
							}
						}
					});
				}else{
					$(this).css('border-color','#b94a48');
					$check++;
				}
			}
		});		
	}
	if($check > 0){
		BootstrapDialog.show({
			title: '',
			type: BootstrapDialog.TYPE_WARNING,
			message: '<p>Silahkan pilih pernyataan keaslian dokumen</p>'
		});
		return false;
	}else{
		if($wajib > 0){
			BootstrapDialog.show({
				title: '',
				type: BootstrapDialog.TYPE_WARNING,
				message: '<p>Ada <b>'+$wajib+'</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>'
			});
			return false;
		}else{
			BootstrapDialog.confirm('Apakah anda yakin dengan data yang Anda isikan ?', function(r){
				if(r){
					if($this.attr("data-onpost")){
						$(p).submit();
					}else{
						$.ajax({
							type: "POST",
							url: $(p).attr('action') + '/ajax',
							data: $(p).serialize(),
							error: function() {
								BootstrapDialog.show({
									title: '',
									type: BootstrapDialog.TYPE_DANGER,
									message: '<p>Maaf, request halaman tidak ditemukan</p>'
								});
							},
							beforeSend: function(){
								if($("#progress").length === 0){
									$("body").append($('<div id=\"progress\"></div><div class=\"overlays\"><div class="la-square-spin" style="margin-left:5px; margin-top: 10px; position: fixed; z-index: 19832509; color: #63c5ab;"><div>Loading ... </div></div></div>'));
									$('#progress').width((50 + Math.random() * 30) + "%");
									$('.overlays').css('width', $('body').css('width'));
									$('.overlays').css('height', $('body').css('height'));
								}
							},
							complete: function(){
								$("#progress").width("101%").delay(200).fadeOut(400, function(){
									$("#progress").remove();
									$(".overlays").remove();
								});
							},
							success: function(data) {
								if (data.search("MSG") >= 0) {console.log(data);
									arrdata = data.split('||');
									if(arrdata[1] == "YES"){
										BootstrapDialog.show({
											title: '',
											type: BootstrapDialog.TYPE_SUCCESS,
											message: '<p>'+arrdata[2]+'</p>'
										});
										if(arrdata.length > 3){
											if(arrdata[3] == "APPEND"){
												setTimeout(function(){
													$(".modal-backdrop, .modal, .bootstrap-dialog, .modal-dialog").remove();
													$("#"+$this.attr("data-target")).append(arrdata[4]);
												},2000);
											}else if(arrdata[3] == "REPLACE"){
												var tmp = arrdata[4].split("#");
												var idx = 1;
												for(var i = 0; i < tmp.length; i++){
													$("tr#"+tmp[0] + " td:nth("+parseInt(idx+i)+")").html(tmp[(idx+i)]);
												}
												setTimeout(function(){
													$(".modal-backdrop, .modal, .bootstrap-dialog, .modal-dialog").remove();
												},2000);
											}else if(arrdata[3] == "REFRESH"){
												setTimeout(function(){location.reload(true);}, 1000);
											}
										}
									} else if (arrdata[1] == "NO") {
										BootstrapDialog.alert(arrdata[2]);
									}
								}
							}
						});
					}
				}else{
					return false;
				}
			});
			return false
		}

	}
}

function uploads(f){
	var $obj = f, $parent = $obj.parent(), $loading = $obj.parent().next().next(), $return = $obj.parent().next();
	if($obj.attr("wajib")) $obj.removeAttr("wajib");
	if($loading.css('display') == 'none') $loading.show();
	$parent.css('display','none');
	$loading.html('Data sedang di upload, mohon menunggu ...');
	$("body").append($("<div></div>").attr("class", "overlays"));
	$('.overlays').css('width', $('body').css('width'));
	$('.overlays').css('height', $('body').css('height'));
	$.ajaxFileUpload({
		url: $obj.attr("url") +'/'+ $obj.attr("allowed"),
		secureuri: false,
		fileElementId: $obj.attr("id"),
		dataType: "json",
		success: function(data){
			var arrdata = data.msg.split("#");
			if(typeof(data.error) != "undefined"){
				if(data.error != ""){
					$(".overlays").remove();
					BootstrapDialog.show({
						title: '',
						type: BootstrapDialog.TYPE_WARNING,
						message: '<p>'+data.error+'</p>'
					});
					$loading.html('');
					if($parent.css('display') == 'none') $parent.css('display','');
				}else{
					$return.show();
					setTimeout(function(){
						$loading.html('');
						$loading.hide(function(){
							$return.html(arrdata[0]);
							$(".overlays").remove();
						});
					},2000);
				}
			}
		},
		error: function (data, status, e){
			BootstrapDialog.show({
				title: '',
				message: '<p>'+e+'</p>'
			});
			$(".overlays").remove();
			if($loading.css('display') == '') $loading.hide();
			if($parent.css('display') == 'none') $parent.css('display','');
		}
	});
}
function remove_uploads(g){
	var $obj = g, $curr = $obj.parent(), $tmp = $obj.parent().prev(), $loading = $obj.parent().next(), $file = $tmp.children();
	BootstrapDialog.confirm('Apakah anda yakin akan menghapus data tersebut ?', function(r){
		if(r){
			$tmp.show(); $file.attr("wajib"); $curr.html(''); $curr.hide();
		}else{
			return false;
		}
	});
	return false;
}
function redirectblank(k){
	var $this = $(k);
	window.open($this.attr("data-url"), '_blank');
	return false;
}

function popuptabel(l){
	var $this = $(l);
	
	if($this.attr("data-url")){
		var $url = "";
		if($this.attr("data-callback")){
			if(parseInt($this.attr("data-multi")) == 1)
			$url = $this.attr("data-url") + $this.attr("data-target") + '/' + $this.attr("data-callback") + '/' + $this.attr("data-fieldcallback") + '/' + $this.attr("data-permohonan") + '/' + $this.attr("data-multi") + '/' + $this.attr("data-body") + '/' + $this.attr("data-doc");
			else
			$url = $this.attr("data-url") + $this.attr("data-target") + '/' + $this.attr("data-callback") + '/' + $this.attr("data-fieldcallback") + '/' + $this.attr("data-permohonan");
		} else if($this.attr("data-target")){
			$url = $this.attr("data-url") + $this.attr("data-target");
		} else{
			$url = $this.attr("data-url");
		}
		$.get($url, function(hasil){
			if(hasil){
				if($(".modal-backdrop").length === 0){
					BootstrapDialog.show({
						title: '',
						type: BootstrapDialog.TYPE_SUCCESS,
						size : BootstrapDialog.SIZE_WIDE,
						message: hasil,
					});
				}
			}else{
				
			}
		});
		return false;
	}else{
		return false;
	}

}


function combobox(m, n, o){
	var $this = $(m);
	var $target = $(n);
	var targets = n;
	if($this.attr("data-url")){
		$target.html('');
		$.get($this.attr("data-url") + $this.val(), function($result){
			if($result){
				if (o == 1) {
					$(targets).html($result);
				}else{
					$target.html($result);
				}
			}
		});
	}else{
		return false;
	}
	return false;
}

function combobox_status(m, k, n, o){
	var $this = $(m);
        var $this2 = $(k);
	var $target = $(n);
	var targets = n;
	if($this.attr("data-url")){
		$target.html('');
		$.get($this.attr("data-url") + $this.val() + '/' + $this2.val(), function($result){
			if($result){
				if (o == 1) {
					$(targets).html($result);
				}else{
					$target.html($result);
				}
			}
		});
	}else{
		return false;
	}
	return false;
}

function combobox_produk(m, k, n){
	var produksi = $(m).val();
    var status = k;
	var produk = $(n).val();
	var targets = n;
	var url = $(m).attr("data-url");
	// console.log(url);
	// return false;
	if(url){
		$(k).html('');
		$.get(url + produksi + '/' + produk, function($result){
			if($result){
				$(k).html($result);
			}
		});
	}else{
		return false;
	}
	return false;
}

function combobox_hs(m, n, o){
	var $this = $(m);
	var $target = $(n);
	var targets = n;
	// console.log(targets);
	// return false;
	if($this.attr("data-url")){
		$target.html('');
		$.get($this.attr("data-url") + $this.val(), function($result){
			if($result){
				if (targets == '#hs_6') {
					$("#hs_6").select2("val", "");
					$("#hs_8").select2("val", "");
					$("#hs_uraian").val('');
				}else if (targets == '#hs8') {
					$("#hs8").select2("val", "");
					$("#hs_uraian").val('');
				}
				$target.html($result);
			}
		});
	}else{
		return false;
	}
	return false;
}

function set_val(m, n, o){
	var $this = $(m);
	var $target = $(n);
	var targets = n;
	if($this.attr("data-url")){
		$target.html('');
		$.get($this.attr("data-url") + $this.val(), function($result){
			if($result){
				$(targets).val($result);
			}
		});
	}else{
		return false;
	}
	return false;
}

function redirect(o){
	var $this = $(o);
	if($this.attr("data-url")){
		location.href = $this.attr("data-url");
	}else{
		return false;
	}
}

function serializediv(q){
	var $this = $(q);
	var $target = $this.attr("data-target");
	var $wajib = 0;
	$.each($("input:visible, select:visible, textarea:visible"), function(){
		if($(this).attr('wajib')){
			if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
				$(this).css('border-color','#b94a48');
				$wajib++;
			}
		}
	});
	if($wajib > 0){
		BootstrapDialog.show({
			title: '',
			type: BootstrapDialog.TYPE_WARNING,
			message: '<p>Ada <b>'+$wajib+'</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>'
		});
		return false;
	}else{
		$.ajax({
			  type: "POST",
			  url: $(q).attr('action'),
			  data: $(q).serialize(),
			  error: function() {
				  BootstrapDialog.show({
					  title: '',
					  type: BootstrapDialog.TYPE_DANGER,
					  message: '<p>Maaf, request halaman tidak ditemukan</p>'
				  });
			  },
			  beforeSend: function(){
				  if($("#progress").length === 0){
					  $("body").append($('<div id=\"progress\"></div><div class=\"overlays\"><div class="la-square-spin" style="margin-left:5px; margin-top: 10px; position: fixed; z-index: 19832509; color: #63c5ab;"><div>Loading ... </div></div></div>'));
					  $('#progress').width((50 + Math.random() * 30) + "%");
					  $('.overlays').css('width', $('body').css('width'));
					  $('.overlays').css('height', $('body').css('height'));
				  }
			  },
			  complete: function(){
				  $("#progress").width("101%").delay(200).fadeOut(400, function(){
					  $("#progress").remove();
					  $(".overlays").remove();
				  });
			  },
			  success: function(data){
				  if(data){
					  $($target).html(data);
				  }
			  }
		});
	}
	return false;
}

function postdirect(r){
	var $this = $(r);
	var $wajib = 0;
	if($this.attr("choice")){
		var urls = $(r).attr('choice');
	}else{
		var urls = $(r).attr('action');
	}

	$.each($("input:visible, select:visible, textarea:visible"), function(){
		if($(this).attr('wajib')){
			if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
				$(this).css('border-color','#b94a48');
				$wajib++;
			}
		}
	});
	if($wajib > 0){
		BootstrapDialog.show({
			title: '',
			type: BootstrapDialog.TYPE_WARNING,
			message: '<p>Ada <b>'+$wajib+'</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>'
		});
		return false;
	}else{
		$.ajax({
			  type: "POST",
			  url: urls,
			  data: $(r).serialize(),
			  error: function() {
				  BootstrapDialog.show({
					  title: '',
					  type: BootstrapDialog.TYPE_DANGER,
					  message: '<p>Maaf, request halaman tidak ditemukan</p>'
				  });
			  },
			  beforeSend: function(){
				  if($("#progress").length === 0){
					  $("body").append($('<div id=\"progress\"></div><div class=\"overlays\"><div class="la-square-spin" style="margin-left:5px; margin-top: 10px; position: fixed; z-index: 19832509; color: #63c5ab;"><div>Loading ... </div></div></div>'));
					  $('#progress').width((50 + Math.random() * 30) + "%");
					  $('.overlays').css('width', $('body').css('width'));
					  $('.overlays').css('height', $('body').css('height'));
				  }
			  },
			  complete: function(){
				  $("#progress").width("101%").delay(200).fadeOut(400, function(){
					  $("#progress").remove();
					  $(".overlays").remove();
				  });
			  },
			  success: function(data){
				  	if (data.search("MSG") >= 0) {console.log(data);
						arrdata = data.split('||');
					  setTimeout(function(){
						  location.href = arrdata[2];
					  },100);
				  }
			  }
		});
	}
	return false;
}

function popup(s){
	var $this = $(s);
	if($this.attr("data-url")){
		$.get($this.attr("data-url"), function(hasil){
			if(hasil){
				if($(".modal-backdrop").length === 0){
					BootstrapDialog.show({
						title: '',
						type: BootstrapDialog.TYPE_SUCCESS,
						size : BootstrapDialog.SIZE_WIDE,
						message: hasil,
					});
				}
			}
		});
		return false;
	}
}

function proccess(t,proccess,tolak){
	var $this = $(proccess);
	var $wajib = 0;
	var $check = 0;
	$.each($("input:visible, input:checkbox, select:visible, textarea:visible"), function(){
		if($(this).attr('wajib')){
			if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
				$(this).css('border-color','#b94a48');
				$wajib++;
			}
		}
		if($(this).attr('check-agreement')){
			if($(this).attr('check-agreement')=="yes" && $(this).is(':not(:checked)')){
				$(this).css('border-color','#b94a48');
				$check++;
			}
		}
	});

	var catatan = $(t).find('textarea[name="catatan"]').val();

	if (tolak == 'tolak' && catatan == '') {
		BootstrapDialog.show({
			title: '',
			type: BootstrapDialog.TYPE_WARNING,
			message: '<p>Harap Mengisi Catatan Terlebih Dahulu Untuk Aksi Penolakan</p>'
		});
		return false;
	}
	
	if($check > 0){
		BootstrapDialog.show({
			title: '',
			type: BootstrapDialog.TYPE_WARNING,
			message: '<p>Silahkan centang ( &#10004; ) pernyataan kebenaran dan keaslian data</p>'
		});
		return false;
	}else{
		if($wajib > 0){
			BootstrapDialog.show({
				title: '',
				type: BootstrapDialog.TYPE_WARNING,
				message: '<p>Ada <b>'+$wajib+'</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>'
			});
			return false;
		}else{
			BootstrapDialog.confirm('Apakah anda yakin dengan data yang akan di proses?', function(r){
				if(r){
					var ParseData = $(t).serializeArray();
						ParseData.push({name: "SESUDAH", value: $this.attr("data-status") });
					$.ajax({
						type: "POST",
						url: $(t).attr('action') + '/ajax',
						data: ParseData,
						error: function(){
							BootstrapDialog.show({
								title: '',
								type: BootstrapDialog.TYPE_DANGER,
								message: '<p>Maaf, request halaman tidak ditemukan</p>'
							});
						},
						beforeSend: function(){
							if($("#progress").length === 0){
								$("body").append($('<div id=\"progress\"></div><div class=\"overlays\"><div class="la-square-spin" style="margin-left:5px; margin-top: 10px; position: fixed; z-index: 19832509; color: #63c5ab;"><div>Loading ... </div></div></div>'));
								$('#progress').width((50 + Math.random() * 30) + "%");
								$('.overlays').css('width', $('body').css('width'));
								$('.overlays').css('height', $('body').css('height'));
							}
						},
						complete: function(){
							$("#progress").width("101%").delay(200).fadeOut(400, function(){
								$("#progress").remove();
								$(".overlays").remove();
							});
						},
						success: function(data) {
							if(data.search("MSG") >= 0){
								if (data.search("MSG") >= 0) {
									arrdata = data.split('||');
									if (arrdata[1] == "YES") {
										$(".overlays").remove();
										BootstrapDialog.show({
											title: '',
											type: BootstrapDialog.TYPE_SUCCESS,
											message: '<p>'+arrdata[2]+'</p>'
										});
										if(arrdata.length > 3){
											if(arrdata[3] == "REFRESH"){
												setTimeout(function(){location.reload(true);}, 2000);
											}else if(arrdata[3] == "BACK"){
												setTimeout(function(){javascript:window.history.back();}, 2000);
											}else{
												setTimeout(function(){
													if($form.attr("data-redirect")){
														location.href = arrdata[3];
													}
												}, 2000);
											}
										}
									} else if (arrdata[1] == "NO") {
										BootstrapDialog.show({
											title: '',
											type: BootstrapDialog.TYPE_DANGER,
											message: '<p>'+arrdata[2]+'</p>'
										});
									}
								}
							}
						}
					});
				}else{
					return false;
				}
			});
			return false
		}
	}
}



function ReplaceAll(Source,stringToFind,stringToReplace){
	var temp = Source;
	var index = temp.indexOf(stringToFind);
	while(index != -1){
		temp = temp.replace(stringToFind,stringToReplace);

		index = temp.indexOf(stringToFind);
	}
	return temp;
}

function get_mask_number(idInput, idNotif)
{
	$('#'+idInput).blur(function() {
			//$('#'+idNotif).html(null);
			$(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: -1 });
		})
		.keyup(function(e) {
			var e = window.event || e;
			var keyUnicode = e.charCode || e.keyCode;
			if (e !== undefined) {
				switch (keyUnicode) {
					case 16: break; // Shift
					case 17: break; // Ctrl
					case 18: break; // Alt
					case 27: this.value = ''; break; // Esc: clear entry
					case 35: break; // End
					case 36: break; // Home
					case 37: break; // cursor left
					case 38: break; // cursor up
					case 39: break; // cursor right
					case 40: break; // cursor down
					case 78: break; // N (Opera 9.63+ maps the "." from the number key section to the "N" key too!) (See: http://unixpapa.com/js/key.html search for ". Del")
					case 110: break; // . number block (Opera 9.63+ maps the "." from the number block to the "N" key (78) !!!)
					case 190: break; // .
					default: $(this).formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: -1, eventOnDecimalsEntered: true });
				}
			}
		})
		.bind('decimalsEntered', function(e, cents) {
			/*if (String(cents).length > 2) {
				var errorMsg = 'Please do not enter any cents (0.' + cents + ')';
				$('#formatWhileTypingAndWarnOnDecimalsEnteredNotification2').html(errorMsg);
				log('Event on decimals entered: ' + errorMsg);
			}*/
		});
}

function rmtrreq(a, multi){ 
	multi = typeof multi !== 'undefined' ? multi : false; 
	var $this = $(a);
	var $parentx = $(a).closest("tr");
	if($(".modal-backdrop").length === 0){
		BootstrapDialog.confirm('Apakah anda menghapus data terpilih ?', function(r){
			if(r){
				if(multi){
					$($parentx).remove();
				}else{
					var $length = $('td', $parentx).length;
					for(var $z = 2; $z <= $length; $z++){
						$('tr#' + $parentx.attr("id") + ' td:nth-child('+$z+')').each(function(){
							$(this).children().children().html('');
                        });
					}
					$('#' + $parentx.attr("id") + ' td:nth-child(1) .persyaratan').each(function(){
						var $age = $(this);
						if($age.attr("data-type")){
							if($age.attr("data-type")){
								if($age.attr("data-type") == "nyumput" && !$age.attr("ngalewat")) {
									$($age).val('');
								}
								if($age.attr("data-type") == "nulis"){
									$($age).html('');
								}
							}
						}
					});
				}
			}else{
				return false;
			}
		});
	}
}