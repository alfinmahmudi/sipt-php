function obj(p){
	var $this = $(p);
	/* if($this.attr('tes')){
	}else{ */
		var $wajib = 0;
		var $check = 0;
		var cek = /^[0-9]+$/;
		var cekmail = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		var number = 0;
		var	mail = 0;
		//$.each($("input:visible, select:visible, textarea:visible"), function(){
		if($this.attr("data-agreement")){
			$.each($("input:checkbox"), function(){
				if($(this).attr('check-agreement')){
					if($(this).attr('check-agreement')=="yes" && ($(this).is(':checked') || $(this).val()==null)){
						$('input:visible, input:checkbox, select:visible, textarea:visible', p).each(function(){
							if($(this).attr('wajib')){
								if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){//alert("bebas");return false;
									$(this).css('border-color','#b94a48');
									$wajib++;
								}
							}
						});
					}else{
						$(this).css('border-color','#b94a48');
						$check++;
					}
				}
			});		
		}else{
			$.each($("input:visible, select:visible, textarea:visible"), function(){
			var a = $(this).val();
			// if($(this).attr('wajib')){
			// 	if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
			// 		$(this).css('border-color','#b94a48');
			// 		$wajib++;
			// 	}
			// }
			if($(this).attr('nomor') == "yes"){
				if (a.match(cek)) {
					$(this).css('border-color','#dfdfdf');
				}else{
					$(this).css('border-color','#b94a48');
					number++;
				}
			}
			if($(this).attr('email') == "yes"){
				if (a.match(cekmail)) {
					$(this).css('border-color','#dfdfdf');
				}else{
					$(this).css('border-color','#b94a48');
					mail++;
				}
			}
			
			});
		}
		if($check > 0){
			BootstrapDialog.show({
				title: '',
				type: BootstrapDialog.TYPE_WARNING,
				message: '<p>Silahkan pilih pernyataan keaslian dokumen</p>'
			});
			return false;
		}else{
			if($wajib > 0 || number > 0 || mail > 0){
					if (number > 0) {
						BootstrapDialog.show({
							title: '',
							type: BootstrapDialog.TYPE_WARNING,
							message: '<p>Ada <b>'+number+'</b> kolom yang harus diisi dengan angka. <br>Silahkan periksa kembali isian form anda.</p>'
						});
					}else if (mail > 0){
						BootstrapDialog.show({
							title: '',
							type: BootstrapDialog.TYPE_WARNING,
							message: '<p>Ada <b>'+mail+'</b> kolom yang harus diisi dengan format email. <br>Silahkan periksa kembali isian form anda.</p>'
						});
					}else{
						BootstrapDialog.show({
							title: '',
							type: BootstrapDialog.TYPE_WARNING,
							message: '<p>Ada <b>'+$wajib+'</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>'
						});
					}
					return false;
			}else{
				BootstrapDialog.confirm('<b>Apakah anda yakin dengan data yang Anda isikan ?.</b>', function(r){
					if(r){
						
							$.ajax({
								type: "POST",
								url: $(p).attr('action') + '/ajax',
								data: new FormData(document.getElementById('fsupportdocnew')),
								processData: false,
								contentType: false,
								dataType:($this.attr("tes")) ? 'json': '',
								error: function() {
									BootstrapDialog.show({
										title: '',
										type: BootstrapDialog.TYPE_DANGER,
										message: '<p>Maaf, request halaman tidak ditemukan</p>'
									});
								},
								beforeSend: function(){
									if($("#progress").length === 0){
										$("body").append($('<div id=\"progress\"></div><div class=\"overlays\"><div class="la-square-spin" style="margin-left:5px; margin-top: 10px; position: fixed; z-index: 19832509; color: #63c5ab;"><div>Loading ... </div></div></div>'));
										$('#progress').width((50 + Math.random() * 30) + "%");
										$('.overlays').css('width', $('body').css('width'));
										$('.overlays').css('height', $('body').css('height'));
									}
								},
								complete: function(){
									$("#progress").width("101%").delay(200).fadeOut(400, function(){
										$("#progress").remove();
										$(".overlays").remove();
									});
								},
								success: function(data) {
									if($this.attr('tes')){
									//	console.log(data);return false;
										$(".close").click();
										var dok_id = $this.attr("doc_tipe");
										$("#no_dok_"+dok_id).html(data[0]['NoDokumen']);
										$("#penerbit_"+dok_id).html(data[0]['Penerbit']);
										$("#tgl_dok_"+dok_id).html(data[0]['TglDokumen']);
										$("#tgl_exp_"+dok_id).html(data[0]['TglAkhir']);
										$("#View_"+dok_id).html(data[0]['View']);
									}else{
										if (data.search("MSG") >= 0) {
											arrdata = data.split('||');
											if(arrdata[1] == "YES"){
												if (arrdata[4] == 'APPEND') {
													var ids = arrdata[5];
													var temp = "<i class='fa fa-check'></i>";
													$('#status_'+ids).append(temp);
												}
												$(".close").click();
												BootstrapDialog.alert({
													title:'',
													message:arrdata[2],
													callback:
														function(){
															if($this.attr('data-onpost') == "TRUE" ){
																location.reload();
															}
														}
												});
												
											} else{
												$(".close").click();
												BootstrapDialog.alert({
													title:'',
													message:arrdata[2],
													callback:
														function(){
															if($(p).attr('data-onpost')){
																location.reload();
															}
														}
												});
											}
										}
									}	
								}
							});
						
					}else{
						return false;
					}
				});
				return false;
			}
		}
	//}
}

function popup(p){
	var $this = $(p);
	/* if($this.attr('tes')){
	}else{ */
		var $wajib = 0;
		var $check = 0;
		var cek = /^[0-9]+$/;
		var cekmail = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		var number = 0;
		var	mail = 0;
		//$.each($("input:visible, select:visible, textarea:visible"), function(){
		if($this.attr("data-agreement")){
			$.each($("input:checkbox"), function(){
				if($(this).attr('check-agreement')){
					if($(this).attr('check-agreement')=="yes" && ($(this).is(':checked') || $(this).val()==null)){
						$('input:visible, input:checkbox, select:visible, textarea:visible', p).each(function(){
							if($(this).attr('wajib')){
								if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){//alert("bebas");return false;
									$(this).css('border-color','#b94a48');
									$wajib++;
								}
							}
						});
					}else{
						$(this).css('border-color','#b94a48');
						$check++;
					}
				}
			});		
		}else{
			$.each($("input:visible, select:visible, textarea:visible"), function(){
			var a = $(this).val();
			// if($(this).attr('wajib')){
			// 	if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
			// 		$(this).css('border-color','#b94a48');
			// 		$wajib++;
			// 	}
			// }
			if($(this).attr('nomor') == "yes"){
				if (a.match(cek)) {
					$(this).css('border-color','#dfdfdf');
				}else{
					$(this).css('border-color','#b94a48');
					number++;
				}
			}
			if($(this).attr('email') == "yes"){
				if (a.match(cekmail)) {
					$(this).css('border-color','#dfdfdf');
				}else{
					$(this).css('border-color','#b94a48');
					mail++;
				}
			}
			
			});
		}
		if($check > 0){
			BootstrapDialog.show({
				title: '',
				type: BootstrapDialog.TYPE_WARNING,
				message: '<p>Silahkan pilih pernyataan keaslian dokumen</p>'
			});
			return false;
		}else{
			if($wajib > 0 || number > 0 || mail > 0){
					if (number > 0) {
						BootstrapDialog.show({
							title: '',
							type: BootstrapDialog.TYPE_WARNING,
							message: '<p>Ada <b>'+number+'</b> kolom yang harus diisi dengan angka. <br>Silahkan periksa kembali isian form anda.</p>'
						});
					}else if (mail > 0){
						BootstrapDialog.show({
							title: '',
							type: BootstrapDialog.TYPE_WARNING,
							message: '<p>Ada <b>'+mail+'</b> kolom yang harus diisi dengan format email. <br>Silahkan periksa kembali isian form anda.</p>'
						});
					}else{
						BootstrapDialog.show({
							title: '',
							type: BootstrapDialog.TYPE_WARNING,
							message: '<p>Ada <b>'+$wajib+'</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>'
						});
					}
					return false;
			}else{
				BootstrapDialog.confirm('<b>Apakah anda yakin dengan data yang Anda isikan ?.</b>', function(r){
					if(r){
							var tipe_dok = $('#tipe_dok').val();
							$.ajax({
								type: "POST",
								url: $(p).attr('action') + '/ajax',
								data: new FormData(document.getElementById('fsupportdocnew')),
								processData: false,
								contentType: false,
								dataType:($this.attr("tes")) ? 'json': '',
								error: function() {
									BootstrapDialog.show({
										title: '',
										type: BootstrapDialog.TYPE_DANGER,
										message: '<p>Maaf, request halaman tidak ditemukan</p>'
									});
								},
								beforeSend: function(){
									if($("#progress").length === 0){
										$("body").append($('<div id=\"progress\"></div><div class=\"overlays\"><div class="la-square-spin" style="margin-left:5px; margin-top: 10px; position: fixed; z-index: 19832509; color: #63c5ab;"><div>Loading ... </div></div></div>'));
										$('#progress').width((50 + Math.random() * 30) + "%");
										$('.overlays').css('width', $('body').css('width'));
										$('.overlays').css('height', $('body').css('height'));
									}
								},
								complete: function(){
									$("#progress").width("101%").delay(200).fadeOut(400, function(){
										$("#progress").remove();
										$(".overlays").remove();
									});
								},
								success: function(data) {
									if($this.attr('tes')){
									//	console.log(data);return false;
										$(".close").click();
										var dok_id = $this.attr("doc_tipe");
										$("#no_dok_"+dok_id).html(data[0]['NoDokumen']);
										$("#penerbit_"+dok_id).html(data[0]['Penerbit']);
										$("#tgl_dok_"+dok_id).html(data[0]['TglDokumen']);
										$("#tgl_exp_"+dok_id).html(data[0]['TglAkhir']);
										$("#View_"+dok_id).html(data[0]['View']);
									}else{
										if (data.search("MSG") >= 0) {
											arrdata = data.split('||');
											if(arrdata[1] == "YES"){
												if (arrdata[4] == 'APPEND') {
													var ids = arrdata[5];
													var temp = "<i class='fa fa-check'></i>";
													$('#status_'+ids).append(temp);
												}
												$(".close").click();
												$('#btn-syarat-'+tipe_dok).click();
												// BootstrapDialog.alert({
												// 	title:'',
												// 	message:arrdata[2],
												// 	callback:
												// 		function(){
												// 			if($this.attr('data-onpost') == "TRUE" ){
												// 				location.reload();
												// 			}
												// 		}
												// });

												
											} else{
												$(".close").click();
												BootstrapDialog.alert({
													title:'',
													message:arrdata[2],
													callback:
														function(){
															if($(p).attr('data-onpost')){
																location.reload();
															}
														}
												});
											}
										}
									}	
								}
							});
						
					}else{
						return false;
					}
				});
				return false;
			}
		}
	//}
}

function cekNPWP(val,btn){
	var url=$(btn).attr('data-url');
	var npwp = $(val).val();
	$.ajax({
		url:url,
		data:'a='+npwp,
		type:'post',
		dataType:'json',
		success:
			function(data){
				console.log(data);
				if(data['ERROR']){
					alert('NPWP tidak ditemukan');
					return false;
				}
				var y = $("#fnewstpw").attr("action");
				var r = y.replace("/search","");
				$.each(data['sess'], function(i,n){
					if(i == "kdprop"){
						if(data['propinsi'][data['sess']['kdprop']]){
							var q = "<option value='"+data['sess']['kdprop']+"' selected> "+data['propinsi'][data['sess']['kdprop']]+"</option>";
							$("#kdprop").append(q);
							$("#kdprop").select2("val",data['sess']['kdprop']);
						}else{
							$("#kdprop").select2({"val":null, placeholder:"Select"});
						}
					}else if(i == "kdkab"){
						if(data['kab'][data['sess']['kdkab']]){
							var q = "<option value='"+data['sess']['kdkab']+"' selected> "+data['kab'][data['sess']['kdkab']]+"</option>";
							$("#kdkab").append(q);
							$("#kdkab").select2("val",data['sess']['kdkab']);
						}else{
							$("#kdkab").select2("val",null);
						}
					}else if(i == "kdkec"){
						if(data['kec'][data['sess']['kdkec']]){
							var w = "<option value='"+data['sess']['kdkec']+"' selected> "+data['kec'][data['sess']['kdkec']]+"</option>";
							$("#kdkec").append(w);
							$("#kdkec").select2("val",data['sess']['kdkec']);
						}else{
							$("#kdkec").select2("val",null);
						}
					}else if(i == "kdkel"){
						if(data['kel'][data['sess']['kdkel']]){
							var e = "<option value='"+data['sess']['kdkel']+"' selected> "+data['kel'][data['sess']['kdkel']]+"</option>";
							$("#kdkel").append(e);
							$("#kdkel").select2("val",data['sess']['kdkel']);
						}else{
							$("#kdkel").select2("val",null);
						}
					}else{
						$("#"+i).val(n);
					}
				});	
				$("#fnewstpw").attr("action",r+'/search');
			},
		error:
			function(){
			//	alert('dd');
			}
	});
	return false;

}

function blank(btn){
	var frm = $('#fpreview');
	var url_m = frm.attr('action');
	var url = $(btn).attr('data-url');
	frm.attr('target','_blank');
	frm.attr('action',url);
	frm.submit();
	frm.removeAttr('target');
	frm.attr('action',url_m);
}

function set_produk(r){
	var url = $(r).attr('data-url');
	$.ajax({
		url:url,
		type:'POST',
		error:
			function(){
				alert('Error');
			},	
		success:
			function(data){
				location.href=data;
			}
	});
	return false;
}
