<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Label_act extends CI_Model {

    function set_proccess($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($act == "verification") {
                $respon = FALSE;
                $msgok = "MSG||YES||Data permohonan berhasil di proses||REFRESH";
                $msgerr = "MSG||NO||Data permohonan gagal di update. Silahkan coba lagi.";
                $sebelum = $this->main->get_uraian("SELECT uraian FROM m_reff WHERE jenis = 'STATUS' AND kode = '" . $this->input->post('SEBELUM') . "'", "uraian");
                $sesudah = $this->main->get_uraian("SELECT uraian FROM m_reff WHERE jenis = 'STATUS' AND kode = '" . $_POST['SESUDAH'] . "'", "uraian");
                $arrupdate = $this->main->post_to_query($this->input->post('data'));
                $arrupdate['status'] = $_POST['SESUDAH'];
                //print_r($arrupdate['status']);die($this->newsession->userdata('role'));
                $arrupdate['id'] = hashids_decrypt($arrupdate['id'], _HASHIDS_, 9);
                $arrupdate['kd_izin'] = hashids_decrypt($arrupdate['kd_izin'], _HASHIDS_, 9);
                $direktorat = hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                $kd_izin = $arrupdate['kd_izin'];
                $id = $arrupdate['id'];
                $tipe = base64_decode($this->input->post('tipe'));
                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $arrupdate['kd_izin'] . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $txt = "Proses permohonan";
                if ($this->input->post('catatan') == '' && $arrupdate['status'] == '0501') {
                    return "MSG||NO||Harap Mengisi Catatan Terlebih Dahulu Untuk Aksi Penolakan";
                }
                if ($arrupdate['status'] == 'resend_email') {
                    // $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha('1000', $direktorat, $kd_izin, $id, $tipe, "resend");
                    $sq = " SELECT a.no_izin
                            FROM t_label a 
                            WHERE a.id = '" . $arrupdate['id'] . "' AND a.status = '1000'";
                    $no_izin = $this->db->query($sq)->row_array();
                    $cek = "SELECT a.id FROM t_upload a WHERE a.nomor = '" . $no_izin['no_izin'] . "'";
                    $res = $this->db->query($cek)->num_rows();
                    if ($res != '') {
                        $resdel = FALSE;
                        $this->db->trans_begin();
                        $this->db->where('nomor', trim($no_izin['no_izin']));
                        $this->db->delete(t_upload);

                        if ($this->db->affected_rows() >= 1) {
                            $resdel = TRUE;
                        }

                        if ($this->db->trans_status() === FALSE || !$resdel) {
                            $this->db->trans_rollback();
                            $msgok = "MSG||NO||Email Gagal Di Kirim||BACK";
                            return $msgok;
                        } else {
                            $this->db->trans_commit();
                            $msgok = "MSG||YES||Email Berhasil Di Kirim||BACK";
                        }
                    }
                    return $msgok;
                }
                /* Update waktu tanggal kirim permohonan */
                if ($this->newsession->userdata('role') == "05") {
                    if ($arrupdate['status'] == "1000") {
                        $arrupdate['updated'] = 'GETDATE()';
                        $arrupdate['updated_user'] = $this->newsession->userdata('nama');
                        $arrupdate['tgl_kirim'] = 'GETDATE()';
                        $txt = "Mengirim permohonan";
                    }
                    $selisih = 0;
                }
                $arrupdate['last_proses'] = 'GETDATE()';
                if ((!$this->main->requirements($id, $kd_izin, $tipe)) && ($arrupdate['status'] == "0100")) {
                    $respon = FALSE;
                    $msgerr = "MSG||NO||Lengkapi Dokumen Persyaratan.";
                } else {
                    $this->db->trans_begin();
                    $id = $arrupdate['id'];
                    $kd = $arrupdate['kd_izin'];
                    $aju = $arrupdate['no_aju'];
                    unset($arrupdate['id']);
                    unset($arrupdate['kd_izin']);
                    unset($arrupdate['no_aju']);
                    $this->db->where('id', $id);
                    $this->db->where('kd_izin', $kd);
                    $this->db->where('no_aju', $aju);
                    if ($this->newsession->userdata('role') == "05") {
                        $this->db->where('trader_id', $this->newsession->userdata('trader_id'));
                        if ($arrupdate['status'] == '0100') {
                            $sql = "UPDATE t_upload 
SET flag_used = '1'
							from t_upload a
							LEFT join t_upload_syarat b on a.id = b.upload_id
							LEFT JOIN m_dok_izin c on c.dok_id = b.dok_id and c.izin_id = b.izin_id  
							WHERE c.kategori <> '03' and b.izin_id = " . $kd . " and b.permohonan_id = " . $id; //die($sql);
                            $this->db->query($sql);
                        }
                    }
                    $this->db->update('t_label', $arrupdate);
                    if ($this->db->affected_rows() == 1) {
                        $respon = TRUE;
                        $logu = array('aktifitas' => $txt . ' ' . $nama_izin . ' dengan nomor permohonan : ' . $aju,
                            'url' => '{c}' . site_url() . 'post/proccess/label_act/verification' . ' {m} models/proccess/label_act {f} set_proccess($act, $ajax)');
                        $this->main->set_activity($logu);
                        $logi = array('kd_izin' => $kd,
                            'permohonan_id' => $id,
                            'keterangan' => $sebelum . ' <i class="fa fa-arrow-circle-right"></i> ' . $sesudah . '<br> ' . $txt . ' ' . $nama_izin . ' dengan nomor permohonan : ' . $aju,
                            'catatan' => ($_POST['catatan'] ? $_POST['catatan'] : '-'),
                            'status' => $arrupdate['status'],
                            'selisih' => $selisih);
                        $this->main->set_loglicensing($logi);
                        // $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha($arrupdate['status'], $direktorat, $kd_izin, $id, $tipe);
                        // $send_email_pemroses = $this->email_act->email_pemroses($arrupdate['status'], $direktorat, $kd_izin, $id, $tipe);
                    }
                }
                if ($this->db->trans_status() === FALSE || !$respon) {
                    $this->db->trans_rollback();
                    return $msgerr;
                } else {
                    $this->db->trans_commit();
                    if ($arrupdate['status'] == '1000') {
                        $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha_beras($arrupdate['status'], $direktorat, $kd_izin, $id, $tipe);
                        // $send_email_pemroses = $this->email_act->email_pemroses($arrupdate['status'], $direktorat, $kd_izin, $id, $tipe);
                    }
                    return $msgok;
                }
            }
        }
    }

    function get_pejabatTTD($kd_izin) {
        $query = "SELECT * FROM m_ttd WHERE status = 1 AND kd_izin = " . $kd_izin;
        $data = $this->main->get_result($query);
        if ($data) {
            foreach ($query->result_array() as $row) {
                $arrdata = $row;
            }
        }
        return $arrdata;
    }

    function get_tahun($kd_izin, $dok_id, $permohonan_id) {
        $query = "  SELECT b.tgl_exp, DATEDIFF(day, CONVERT(DATE,getdate(),120), CONVERT(DATE,b.tgl_exp,120)) as 'result'
                    FROM t_upload_syarat a
                    LEFT JOIN t_upload b ON b.id = a.upload_id
                    WHERE a.izin_id = '" . $kd_izin . "' AND a.dok_id = '" . $dok_id . "' AND a.permohonan_id = '" . $permohonan_id . "'";
        $arrresult = $this->db->query($query)->row_array();
        return $arrresult;
    }

}

?>