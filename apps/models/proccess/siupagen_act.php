<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Siupagen_act extends CI_Model {

    function set_proccess($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($act == "verification") {
                $respon = FALSE;
                $msgok = "MSG||YES||Data permohonan berhasil di proses||REFRESH";
                $msgerr = "MSG||NO||Data permohonan gagal di update. Silahkan coba lagi.";
                $sebelum = $this->main->get_uraian("SELECT uraian FROM m_reff WHERE jenis = 'STATUS' AND kode = '" . $this->input->post('SEBELUM') . "'", "uraian");
                $sesudah = $this->main->get_uraian("SELECT uraian FROM m_reff WHERE jenis = 'STATUS' AND kode = '" . $_POST['SESUDAH'] . "'", "uraian");
                $arrupdate = $this->main->post_to_query($this->input->post('data'));
                $arrupdate['status'] = $_POST['SESUDAH'];
                $arrupdate['id'] = hashids_decrypt($arrupdate['id'], _HASHIDS_, 9);
                $arrupdate['kd_izin'] = hashids_decrypt($arrupdate['kd_izin'], _HASHIDS_, 9);
                $direktorat = hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                $kd_izin = $arrupdate['kd_izin'];
                $id = $arrupdate['id'];
                $tipe = base64_decode($this->input->post('tipe'));
                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $arrupdate['kd_izin'] . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $txt = "Proses permohonan";
                if($this->input->post('catatan') == '' && $arrupdate['status'] == '0501'){
                     return "MSG||NO||Harap Mengisi Catatan Terlebih Dahulu Untuk Aksi Penolakan";
                }
                if ($arrupdate['status'] == 'resend_email') {
                    // $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha('1000', $direktorat, $kd_izin, $id, $tipe, "resend");
                    $sq = " SELECT a.no_izin
                            FROM t_siup_agen a 
                            WHERE a.id = '".$arrupdate['id']."' AND a.status = '1000'";
                    $no_izin = $this->db->query($sq)->row_array();
                    $cek = "SELECT a.id FROM t_upload a WHERE a.nomor = '".$no_izin['no_izin']."'";
                    $res = $this->db->query($cek)->num_rows();
                    if ($res != '') {
                        $resdel = FALSE;
                        $this->db->trans_begin();
                        $this->db->where('nomor', trim($no_izin['no_izin']));
                        $this->db->delete(t_upload);

                        if ($this->db->affected_rows() >= 1) {
                            $resdel = TRUE;
                        }

                        if ($this->db->trans_status() === FALSE || !$resdel) {
                            $this->db->trans_rollback();
                            $msgok = "MSG||NO||Email Gagal Di Kirim||BACK";
                            return $msgok;
                        } else {
                            $this->db->trans_commit();
                            $msgok = "MSG||YES||Email Berhasil Di Kirim||BACK";   
                        }
                    }
                    return $msgok;
                }
                /* Update waktu tanggal kirim permohonan */
                if ($this->newsession->userdata('role') == "05") {
                    if ($arrupdate['status'] == "0100") {
                        $arrupdate['updated'] = 'GETDATE()';
                        $arrupdate['updated_user'] = $this->newsession->userdata('nama');
                        $arrupdate['tgl_kirim'] = 'GETDATE()';
                        $txt = "Mengirim permohonan";
                    }
                    $selisih = 0;
                } else {
                    /* Update waktu tanggal terima permohonan oleh pemroses */
                    if ($this->newsession->userdata('role') == '01') {
                        if ($arrupdate['status'] == '0700') {
                            $arrupdate['updated'] = 'GETDATE()';
                            $arrupdate['updated_user'] = $this->newsession->userdata('nama');
                            $arrupdate['tgl_terima'] = 'GETDATE()';
                            $arrupdate['proses_userid'] = $this->newsession->userdata('id');
                            $txt = "Terima permohonan";
                        }
                    }
                    if (($arrupdate['status'] == '0105') || ($arrupdate['status'] == '0601') || ($arrupdate['status'] == '0701')) {
                        $arrupdate['updated'] = 'GETDATE()';
                        $arrupdate['updated_user'] = $this->newsession->userdata('nama');
                        $txt = "Rollback Permohonan";
                    }

                    if ($arrupdate['status'] == '0600') {
                        $arrupdate['updated'] = 'GETDATE()';
                        $arrupdate['updated_user'] = $this->newsession->userdata('nama');
                    }

                    // if($arrupdate['status'] == '0102'){
                    // 	$arrupdate['updated'] = 'GETDATE()';
                    //                	$arrupdate['updated_user'] = $this->newsession->userdata('nama');
                    // 	$arrupdate['no_izin'] = $this->main->set_nomor($arrupdate['kd_izin']);
                    // 	$arrupdate['tgl_izin'] = 'GETDATE()';
                    // 	$arrupdate['tgl_izin_exp'] = $this->main->set_expired($arrupdate['kd_izin']);
                    // 	$txt = "Persetujuan permohonan";
                    // }

                    if ($arrupdate['status'] == '0200') {
                        $pejabatTTD = $this->get_pejabatTTD($kd_izin);
                        if ($pejabatTTD) {
                            $arrupdate['updated'] = 'GETDATE()';
                            $arrupdate['updated_user'] = $this->newsession->userdata('nama');
                            $arrupdate['nama_ttd'] = $pejabatTTD['nama'];
                            $arrupdate['jabatan_ttd'] = $pejabatTTD['jabatan'];
                            $arrupdate['nip_ttd'] = $pejabatTTD['nip'];
                        }
                    }

                    if ($arrupdate['status'] == '1000') {
                        $arrupdate['updated'] = 'GETDATE()';
                        $arrupdate['updated_user'] = $this->newsession->userdata('nama');
                        $get_prod = "SELECT produksi, produk, jenis_agen FROM t_siup_agen WHERE id = '".$arrupdate['id']."'";
                        $prod = $this->db->query($get_prod)->row_array();
                        $add = '';
                        if ($prod['jenis_agen'] == 03 || $prod['jenis_agen'] == 06) {
                            $add = ", '140'";
                        }
                        $this->main->set_nomor($arrupdate['kd_izin']);
                        $no = $this->main->get_uraian("SELECT nomor from m_nomor where kd_izin = '" . $arrupdate['kd_izin'] . "'", "nomor");
                        if ($prod['produksi'] == '01') {
                            if ($prod['produk'] == '02') {
                                $prod = "STP-JS/SIPT";
                                $jasa = 1;
                            } else{
                                $prod = "STP-DN/SIPT";
                            }
                        } else {
                            if ($prod['produk'] == '02') {
                                $prod = "STP-JS/SIPT";
                                $jasa = 1;
                            } else{
                                $prod = "STP-LN/SIPT";
                            }
                        }
                        $tgl = getdate();
                        $nomor_agen = trim($no) . "/" . $prod . "/" . $tgl['mon'] . "/" . $tgl['year'];
                        $arrupdate['no_izin'] = $nomor_agen;
                        $query = "	SELECT MIN(b.tgl_exp) as tgl_exp, DATEDIFF(day,CONVERT(DATE,getdate(),120),CONVERT(DATE,MIN(b.tgl_exp),120)) as 'result'
                    		FROM t_upload_syarat a 
                    		LEFT JOIN t_upload b ON b.id = a.upload_id
                    		where a.izin_id = '" . $kd_izin . "' and a.dok_id IN('40', '29', '36'".$add.")  and a.permohonan_id = '" . $id . "' and b.tgl_exp > getdate()"; //print_r($query);die();
                        $tahun = $this->db->query($query)->row_array();
                        $sql = "SELECT a.dok_id 
				                FROM t_upload_syarat a
				                LEFT JOIN t_upload b ON b.id = a.upload_id
				                WHERE b.tgl_exp = '" . $tahun['tgl_exp'] . "' AND a.izin_id = '" . $kd_izin . "' AND a.permohonan_id = '" . $id . "'"; //print_r($sql);die();
                        $dok_id = $this->db->query($sql)->result_array(); //print_r($dok_id);die();
                        foreach ($dok_id as $key) {//print_r($key);die();
                            if ($key['dok_id'] == 36) {
                                $query = "SELECT MAX (b.tgl_dok) as tgl_dok, DATEDIFF(day,CONVERT(DATE,getdate(),120),CONVERT(DATE,MIN(b.tgl_exp),120)) as 'result', b.tgl_exp
                                    FROM t_upload_syarat a 
                                    LEFT JOIN t_upload b ON b.id = a.upload_id
                                    where a.izin_id = '" . $kd_izin . "' and a.dok_id IN('40', '36')  and a.permohonan_id = '" . $id . "' and b.tgl_exp > getdate() and b.tgl_dok is not NULL
                                    GROUP BY tgl_dok, b.tgl_exp
                                    ORDER BY tgl_dok DESC";
                                //print_r($query);die();
                                $konfirmasi = $this->db->query($query)->row_array(); //print_r($konfirmasi);die();

                                if (trim($konfirmasi['result']) == '') {
                                    if ($tahun['result'] <= 730 and trim($tahun['result']) != '') {
                                        $arrupdate['tgl_izin_exp'] = $tahun['tgl_exp'];
                                        $temp = $tahun['result'];
                                    } else {
                                        $arrupdate['tgl_izin_exp'] = $this->main->set_expired($kd_izin);
                                        $temp = $tahun['result'];
                                    }
                                } else {
                                    if ($konfirmasi['result'] <= 730 and trim($konfirmasi['result']) != '') {
                                        $arrupdate['tgl_izin_exp'] = $konfirmasi['tgl_exp'];
                                        $temp = $konfirmasi['result'];
                                    } else {
                                        $arrupdate['tgl_izin_exp'] = $this->main->set_expired($kd_izin);
                                        $temp = $konfirmasi['result'];
                                    }
                                    $tahun = $konfirmasi;
                                    break;
                                }
                            } elseif ($key['dok_id'] == 40 || $key['dok_id'] == 29) {
                                if ($tahun['result'] <= 730 and trim($tahun['result']) != '') {
                                    $arrupdate['tgl_izin_exp'] = $tahun['tgl_exp'];
                                    $temp = $tahun['result'];
                                } else {
                                    $arrupdate['tgl_izin_exp'] = $this->main->set_expired($kd_izin);
                                    $temp = $tahun['result'];
                                }
                                break;
//                            } elseif($key['dok_id'] == 140) {
////                                if ($tahun['result'] <= 730 and trim($tahun['result']) != '') {
//                                if (trim($tahun['result']) != '') {
//                                    $arrdata['tgl_izin_exp'] = $tahun['tgl_exp'];
//                                    $temp = $tahun['result'];
//                                } else {
//                                    $arrdata['tgl_izin_exp'] = $this->main->set_expired($kd_izin);
//                                    $temp = $tahun['result'];
//                                }
                            }else{
                                if ($tahun['result'] <= 730 and trim($tahun['result']) != '') {
                                    $arrupdate['tgl_izin_exp'] = $tahun['tgl_exp'];
                                    $temp = $tahun['result'];
                                } else {
                                    $arrupdate['tgl_izin_exp'] = $this->main->set_expired($kd_izin);
                                    $temp = $tahun['result'];
                                }
                            }
                        }
                        // cek lagi result sama izin teknis
                        // $cek = "SELECT MIN(b.tgl_exp) as tgl_exp, DATEDIFF(day,CONVERT(DATE,getdate(),120),CONVERT(DATE,MIN(b.tgl_exp),120)) as 'result'
                        //         FROM t_upload_syarat a 
                        //         LEFT JOIN t_upload b ON b.id = a.upload_id
                        //         where a.izin_id = '" . $kd_izin . "' and a.dok_id IN('39', '29')  and a.permohonan_id = '" . $id . "' and b.tgl_exp > getdate()";
                        // $teknis = $this->db->query($cek)->row_array(); //print_r($konfirmasi);die();
                        // if ($teknis['result'] <= 730 && trim($teknis['result']) != '') {
                        //     if ($teknis['result'] <= $temp) {
                        //         $arrdata['tgl_izin_exp'] = $teknis['tgl_exp'];
                        //         $temp = $teknis['result'];
                        //     }else{
                        //         $arrdata['tgl_izin_exp'] = $tahun['tgl_exp'];
                        //         $temp = $teknis['result'];
                        //     }
                        // }

                        if ($jasa == '1') {
                            // if produk jasa, badingin lagi sama stp barang nya
                            $cek = "SELECT MIN(b.tgl_exp) as tgl_exp, DATEDIFF(day,CONVERT(DATE,getdate(),120),CONVERT(DATE,MIN(b.tgl_exp),120)) as 'result'
                                    FROM t_upload_syarat a 
                                    LEFT JOIN t_upload b ON b.id = a.upload_id
                                    where a.izin_id = '" . $kd_izin . "' and a.dok_id IN('135')  and a.permohonan_id = '" . $id . "' and b.tgl_exp > getdate()";
                            $stp_barang = $this->db->query($cek)->row_array(); //print_r($konfirmasi);die();
                            if ($stp_barang['result'] <= 730 && trim($stp_barang['result']) != '') {
                                if ($stp_barang['result'] <= $temp) {
                                    $arrupdate['tgl_izin_exp'] = $stp_barang['tgl_exp'];
                                    $temp = $stp_barang['result'];
                                }else{
                                    $arrupdate['tgl_izin_exp'] = $tahun['tgl_exp'];
                                    $temp = $stp_barang['result'];
                                }
                            }
                        }
                        if ($tipe == 'Perubahan') {
                            $cek_perubahan = "SELECT MIN(b.tgl_exp) as tgl_exp, DATEDIFF(day,CONVERT(DATE,getdate(),120),CONVERT(DATE,MIN(b.tgl_exp),120)) as 'result'
                                FROM t_upload_syarat a 
                                LEFT JOIN t_upload b ON b.id = a.upload_id
                                where a.izin_id = '" . $kd_izin . "' and a.dok_id IN('85')  and a.permohonan_id = '" . $id . "' and b.tgl_exp > getdate()";
                            $izin_lama = $this->db->query($cek_perubahan)->row_array(); //print_r($teknis);die();
                            if ($izin_lama['result'] <= 730 && trim($izin_lama['result']) != '') {
                                $arrupdate['tgl_izin_exp'] = $izin_lama['tgl_exp'];
                            }else{
                                $arrupdate['tgl_izin_exp'] = $this->main->set_expired($a);
                            }
                        }
                        $arrupdate['tgl_izin'] = 'GETDATE()';

                        // if kalo dia perubahan PASTI! pake yang lama
                        if ($tipe == 'Perubahan') {
                            $get = "SELECT a.no_izin_lama from t_siup_agen a where a.id = '".$arrupdate['id']."'";
                            $no_izin_lama = $this->db->query($get)->row_array();
                            $arrdet['status_dok'] = '02';
                            $get_cek = "SELECT a.id from t_siup_agen a where a.no_izin = '".$no_izin_lama['no_izin_lama']."'";
                            $cek = $this->db->query($get_cek)->num_rows();
                            if ($cek == 1) {
                            	$this->db->where('no_izin', $no_izin_lama['no_izin_lama']);
                            	$this->db->update('t_siup_agen', $arrdet);
                            }   
                            //print_r($this->db->last_query());die();
                        }
                        $txt = "Penerbitan";

                        // ADD TO TABLE FLAG SIP
                        $arradd = array(
                            'id_permohonan' => $id,
                            'nm_table' => 't_siup_agen',
                            'status_flag' => '0',
                            'created' => 'GETDATE()',
                            'id_table' => $kd_izin
                        );
                        $this->db->insert('t_flag_sip', $arradd);
                        //print_r($this->db->last_query());die();
                    }
                    //print_r($arrupdate);die("sip");

                    $selisih = (int) $this->main->get_selisih($arrupdate['kd_izin'], $arrupdate['id'], "t_siup_agen"); //print_r($selisih);exit();
                    $msgok = "MSG||YES||Data permohonan berhasil di proses||BACK";
                }
                $arrupdate['last_proses'] = 'GETDATE()';
                $requirement = $this->main->requirements($id, $kd_izin, $tipe);
                $legalitasProdusen = $this->cek_legalitas_produsen($id, $kd_izin, $tipe);
                if (((!$requirement) || (!$legalitasProdusen)) && ($arrupdate['status'] == "0100")) {
                    $respon = FALSE;
                    if ((!$requirement) && (!$legalitasProdusen)) {
                        $msgerr = "MSG||NO||Lengkapi :\n-. Dokumen Persyaratan.\n-. Data Legalitas Produsen.";
                    } elseif (!$requirement) {
                        $msgerr = "MSG||NO||Lengkapi Dokumen Persyaratan.";
                    } elseif (!$legalitasProdusen) {
                        $msgerr = "MSG||NO||Lengkapi Data Legalitas Produsen.";
                    }
                } else {
                    $this->db->trans_begin();
                    $id = $arrupdate['id'];
                    $kd = $arrupdate['kd_izin'];
                    $aju = $arrupdate['no_aju'];
                    unset($arrupdate['id']);
                    unset($arrupdate['kd_izin']);
                    unset($arrupdate['no_aju']);
                    $this->db->where('id', $id);
                    $this->db->where('kd_izin', $kd);
                    $this->db->where('no_aju', $aju);
                    if ($this->newsession->userdata('role') == "05") {
                        $this->db->where('trader_id', $this->newsession->userdata('trader_id'));
                        if($arrupdate['status'] == '0100'){
                                $sql = "UPDATE t_upload 
SET flag_used = '1'
							from t_upload a
							LEFT join t_upload_syarat b on a.id = b.upload_id
							LEFT JOIN m_dok_izin c on c.dok_id = b.dok_id and c.izin_id = b.izin_id  
							WHERE c.kategori = '01' and b.izin_id = ".$kd." and b.permohonan_id = ".$id;//die($sql);
                                $this->db->query($sql);
                            }
                    }
                    //print_r($email."-".$message);die();
                    $this->db->update('t_siup_agen', $arrupdate);
                    if ($this->db->affected_rows() == 1) {
                        $respon = TRUE;
                        $logu = array('aktifitas' => $txt . ' ' . $nama_izin . ' dengan nomor permohonan : ' . $aju,
                            'url' => '{c}' . site_url() . 'post/proccess/siupagen_act/verification' . ' {m} models/proccess/siupagen_act {f} set_proccess($act, $ajax)');
                        $this->main->set_activity($logu);
                        $logi = array('kd_izin' => $kd,
                            'permohonan_id' => $id,
                            'keterangan' => $sebelum . ' <i class="fa fa-arrow-circle-right"></i> ' . $sesudah . '<br> ' . $txt . ' ' . $nama_izin . ' dengan nomor permohonan : ' . $aju,
                            'catatan' => ($_POST['catatan'] ? $_POST['catatan'] : '-'),
                            'status' => $arrupdate['status'],
                            'selisih' => $selisih);
                        $this->main->set_loglicensing($logi);
                        // $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha($arrupdate['status'], $direktorat, $kd_izin, $id, $tipe);
                        // $send_email_pemroses = $this->email_act->email_pemroses($arrupdate['status'], $direktorat, $kd_izin, $id, $tipe);
                    }
                }
                if ($this->db->trans_status() === FALSE || !$respon) {
                    $this->db->trans_rollback();
                    return $msgerr;
                } else {
                    $this->db->trans_commit();
                    if ($arrupdate['status'] != '1000') {
                        $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha($arrupdate['status'], $direktorat, $kd_izin, $id, $tipe);
                        $send_email_pemroses = $this->email_act->email_pemroses($arrupdate['status'], $direktorat, $kd_izin, $id, $tipe);
                    }
                    return $msgok;
                }
            }
        }
    }

    function get_pejabatTTD($kd_izin) {
        $query = "SELECT * FROM m_ttd WHERE status = 1 AND kd_izin = " . $kd_izin;
        $data = $this->main->get_result($query);
        if ($data) {
            foreach ($query->result_array() as $row) {
                $arrdata = $row;
            }
        }
        return $arrdata;
    }

    function cek_legalitas_produsen($id, $kd_izin, $tipe) {
        $ret = FALSE;
        $qlegal = "SELECT COUNT(*) AS JMLSYARAT FROM m_dok_izin WHERE izin_id = '" . $kd_izin . "' AND tipe = 1 AND kategori = 04 ";
        if ($tipe == "Baru") {
            $qlegal .= $this->main->find_where($qlegal);
            $qlegal .= "baru = 1";
        } else if ($tipe == "Perubahan") {
            $qlegal .= $this->main->find_where($qlegal);
            $qlegal .= "perubahan = 1";
        } else if ($tipe == "Perpanjangan") {
            $qlegal .= $this->main->find_where($qlegal);
            $qlegal .= "perpanjangan = 1";
        }
        $jmlsyarat = (int) $this->main->get_uraian($qlegal, "JMLSYARAT");
        $jmlupload = (int) $this->main->get_uraian("SELECT COUNT(*) as JML FROM t_upload_syarat a LEFT join t_upload b on b.id = a.upload_id LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id  WHERE a.izin_id = '" . $kd_izin . "' and a.permohonan_id = '" . $id . "'", "JML");
        if ($jmlupload >= $jmlsyarat) {
            $ret = TRUE;
        }
        return $ret;
    }

}

?>