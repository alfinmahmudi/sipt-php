<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Siupmb_act extends CI_Model{
	
	function set_proccess($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if($act == "verification"){
				$respon = FALSE;
                $msgok = "MSG||YES||Data permohonan berhasil di proses||REFRESH";
                $msgerr = "MSG||NO||Data permohonan gagal di update. Silahkan coba lagi.";
				$sebelum = $this->main->get_uraian("SELECT uraian FROM m_reff WHERE jenis = 'STATUS' AND kode = '" . $this->input->post('SEBELUM') . "'", "uraian");
                $sesudah = $this->main->get_uraian("SELECT uraian FROM m_reff WHERE jenis = 'STATUS' AND kode = '" . $_POST['SESUDAH'] . "'", "uraian");
                $arrupdate = $this->main->post_to_query($this->input->post('data'));
                $arrupdate['status'] = $_POST['SESUDAH'];
				$arrupdate['id'] = hashids_decrypt($arrupdate['id'], _HASHIDS_, 9);
				$arrupdate['kd_izin'] = hashids_decrypt($arrupdate['kd_izin'], _HASHIDS_, 9);
				$direktorat = hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
				$kd_izin = $arrupdate['kd_izin'];
				$id = $arrupdate['id'];
				$tipe = base64_decode($this->input->post('tipe'));
				$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$arrupdate['kd_izin']."'";
				$nama_izin = $this->main->get_uraian($sql,'nama_izin');
				$txt = "Proses permohonan";	
				
				/* Update waktu tanggal kirim permohonan */
                if($this->newsession->userdata('role') == "05"){
                    if($arrupdate['status'] == "0100"){
						$arrupdate['tgl_kirim'] = 'GETDATE()';
                        $txt ="Mengirim permohonan";
                        $query_email = "SELECT email FROM t_user a LEFT JOIN t_user_role b ON b.user_id = a.id WHERE b.role = 01 AND b.direktorat = ".$direktorat." AND b.izin_id = ".$arrupdate['kd_izin'];
                        $data = $this->main->get_result($query_email);
                        $email = $query_email->result_array();
                        for($i = 0; $i < count($email); $i++){
							$emails[$i] = $email[$i]['email'];
						}

						$data_permohonan = $this->get_data_permohonan($direktorat, $kd_izin, $id);
						//print_r($data_permohonan);die(); email ke pemroses
                        $subject = 'Pengajuan Permohonan '.$tipe.' SIPT PDN';
                        $message = '<html><head><title>'.$subject.'</title></head><body>';
						$message .= '<p>Pengajuan Permohonan '.$nama_izin.'</p>';
						$message .= '<p>Nomor Pengajuan : <b>'.$data_permohonan['no_aju'].'</b></p>';
						$message .= '<p>Tanggal Pengajuan : <b>'.$data_permohonan['tgl_aju'].'</b></p>';
						$message .= '<p>NPWP Perusahaan : <b>'.$data_permohonan['npwp'].'</b></p>';
						$message .= '<p>Nama Perusahaan : <b>'.$data_permohonan['nm_perusahaan'].'</b></p>';
						$message .= '<p>* This is a system-generated email, please do not reply.</p>';
						$message .= '</body></html>';
						//print_r($message);die();
						#email ke pelaku usaha
						$message2 = '<html><head><title>'.$subject.'</title></head><body>';
						$message2 .= '<p>Yth Bapak/Ibu '.$data_permohonan['nama_pj'].'</p>';
						$message2 .= '<p>Pengajuan permohonan '.$nama_izin.' anda berhasil dikirim. <br> Nomor Pengajuan : <b>'.$data_permohonan['no_aju'].'</b><br> Tanggal Pengajuan : <b>'.$data_permohonan['tgl_aju'].'</b> <br> Silahkan melakukan pengecekan pada histori permohonan untuk mengetahui status permohonan anda.</p>';
						$message2 .= '<p>* This is a system-generated email, please do not reply.</p>';
						$message2 .= '</body></html>';
                    }
					$selisih = 0;
                }else{
					/* Update waktu tanggal terima permohonan oleh pemroses */
					if($this->newsession->userdata('role') == '01'){
						if($arrupdate['status'] == '0700'){
							$arrupdate['tgl_terima'] = 'GETDATE()';
							$arrupdate['proses_userid'] = $this->newsession->userdata('id');
							$txt = "Terima permohonan";

							#email ke pelaku usaha
							$data_permohonan = $this->get_data_permohonan($direktorat, $kd_izin, $id);
							$emails = $data_permohonan['email_pj'];
							$subject = 'Terima Permohonan '.$tipe.' SIPT PDN';
							$message = '<html><head><title>'.$subject.'</title></head><body>';
							$message .= '<p>Yth Bapak/Ibu '.$data_permohonan['nama_pj'].'</p>';
							$message .= '<p>Pengajuan permohonan '.$nama_izin.' anda telah diterima oleh pemroses. <br> Nomor Pengajuan : <b>'.$data_permohonan['no_aju'].'</b><br> Tanggal Pengajuan : <b>'.$data_permohonan['tgl_aju'].'</b> ';
							$message .= '<p>* This is a system-generated email, please do not reply.</p>';
							$message .= '</body></html>';
						}
					}
					if($arrupdate['status'] == '0102'){
						$arrupdate['no_izin'] = $this->main->set_nomor($arrupdate['kd_izin']);
						$arrupdate['tgl_izin'] = 'GETDATE()';
						$arrupdate['tgl_izin_exp'] = $this->main->set_expired($arrupdate['kd_izin']);
						$txt = "Persetujuan permohonan";
					}
					if($arrupdate['status'] == '0200'){
						$pejabatTTD = $this->get_pejabatTTD($kd_izin);
						if($pejabatTTD){
							$arrupdate['nama_ttd'] = $pejabatTTD['nama'];
							$arrupdate['jabatan_ttd'] = $pejabatTTD['jabatan'];
							$arrupdate['nip_ttd'] = $pejabatTTD['nip'];
						}
					}

					if(($arrupdate['status'] == '0105') || ($arrupdate['status'] == '0601') || ($arrupdate['status'] == '0701')){
						$txt = "Rollback Permohonan";
					}
					
					if($arrupdate['status'] == '1000'){
						$txt = "Penerbitan";	

						#email ke pelaku usaha
						$data_permohonan = $this->get_data_permohonan($direktorat, $kd_izin, $id);
						$emails = $data_permohonan['email_pj'];
						$subject = 'Penerbitan Dokumen Permohonan '.$tipe.' SIPT PDN';
						$message = '<html><head><title>'.$subject.'</title></head><body>';
						$message .= '<p>Yth Bapak/Ibu '.$data_permohonan['nama_pj'].'</p>';
						$message .= '<p>Pengajuan permohonan '.$nama_izin.' anda telah diterbitkan. <br> Nomor Pengajuan : <b>'.$data_permohonan['no_aju'].'</b><br> Tanggal Pengajuan : <b>'.$data_permohonan['tgl_aju'].'</b> ';
						$message .= '<p>Pengambilan Dokumen dapat dilakukan melalui loket UPP Kementrian Perdagangan.</p>';
						$message .= '<p>* This is a system-generated email, please do not reply.</p>';
						$message .= '</body></html>';
						
					}
					$selisih = (int)$this->main->get_selisih($arrupdate['kd_izin'], $arrupdate['id'], "t_siupmb");//print_r($selisih);exit();
					$msgok = "MSG||YES||Data permohonan berhasil di proses||BACK";
				}
				$arrupdate['last_proses'] = 'GETDATE()';
				if(!$this->main->requirements($id, $kd_izin, $tipe)){
            		$respon = FALSE;
					$msgerr = "MSG||NO||Lengkapi Dokumen Persyaratan.";
				}else{
					$this->db->trans_begin();
					$id = $arrupdate['id'];
					$kd = $arrupdate['kd_izin'];
					$aju = $arrupdate['no_aju'];
					unset($arrupdate['id']);
					unset($arrupdate['kd_izin']);
					unset($arrupdate['no_aju']);
	                $this->db->where('id', $id);
	                $this->db->where('kd_izin', $kd);
	                $this->db->where('no_aju', $aju);
	                if($this->newsession->userdata('role') == "05") {
	                    $this->db->where('trader_id', $this->newsession->userdata('trader_id'));
	                }
	                print_r($emails);die();
					$this->db->update('t_siupmb', $arrupdate);
					if($this->db->affected_rows() == 1){
						$respon = TRUE;
						$logu = array('aktifitas' => $txt.' '.$nama_izin.' dengan nomor permohonan : '.$aju,
									  'url' => '{c}'. site_url().'post/proccess/siupmb_act/verification'. ' {m} models/proccess/siupmb_act {f} set_proccess($act, $ajax)');
						$this->main->set_activity($logu);
						$logi = array('kd_izin' => $kd,
									  'permohonan_id' => $id,
									  'keterangan' => $sebelum . ' <i class="fa fa-arrow-circle-right"></i> ' .$sesudah . '<br> '.$txt.' '.$nama_izin.' dengan nomor permohonan : '.$aju,
									  'catatan' => ($_POST['catatan'] ? $_POST['catatan'] : '-'),
									  'status' => $arrupdate['status'],
									  'selisih' => $selisih);
						$this->main->set_loglicensing($logi);
						if(($message != "") && ($subject) && ($emails)){
							$this->main->send_email($emails, $message, $subject);
						}
						if($this->newsession->userdata('role') == "05") {
							$this->main->send_email($this->newsession->userdata('email_pj'), $message2, $subject);
						}

					}
				}
				if($this->db->trans_status() === FALSE || !$respon){
                    $this->db->trans_rollback();
                    return $msgerr;
                }else{
					$this->db->trans_commit();
                    return $msgok;
				}
			}
		}
	}

	function get_data_permohonan($dir,$kd_izin, $id){
		
		$query = "SELECT a.no_aju, a.tgl_aju, dbo.npwp(a.npwp)as npwp, a.nm_perusahaan, a.nama_pj, b.email_pj
				FROM t_siupmb a
				LEFT JOIN m_trader b on b.id = a.trader_id WHERE a.id = ".$id." AND a.kd_izin = ".$kd_izin;
		$data = $this->main->get_result($query);
		if($data){
			foreach($query->result_array() as $row){
				$arrdata = $row;
			}
		}
		return $arrdata;

	}

	function get_pejabatTTD($kd_izin){
		$query = "SELECT * FROM m_ttd WHERE status = 1 AND kd_izin = ".$kd_izin;
		$data = $this->main->get_result($query);
		if($data){
			foreach($query->result_array() as $row){
				$arrdata = $row;
			}
		}
		return $arrdata;
	}
	
}
?>