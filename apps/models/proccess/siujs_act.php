<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Siujs_act extends CI_Model{
	
	function set_proccess($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if($act == "verification"){
                            var_dump($this->main->set_log_oss());die();
                        }
                        if($act == "verifi"){
				$respon = FALSE;
                $msgok = "MSG||YES||Data permohonan berhasil di proses||REFRESH";
                $msgerr = "MSG||NO||Data permohonan gagal di update. Silahkan coba lagi.";
				$sebelum = $this->main->get_uraian("SELECT uraian FROM m_reff WHERE jenis = 'STATUS' AND kode = '" . $this->input->post('SEBELUM') . "'", "uraian");
                $sesudah = $this->main->get_uraian("SELECT uraian FROM m_reff WHERE jenis = 'STATUS' AND kode = '" . $_POST['SESUDAH'] . "'", "uraian");
                $arrupdate = $this->main->post_to_query($this->input->post('data'));
                $arrupdate['status'] = $_POST['SESUDAH'];
				$arrupdate['id'] = hashids_decrypt($arrupdate['id'], _HASHIDS_, 9);
				$arrupdate['kd_izin'] = hashids_decrypt($arrupdate['kd_izin'], _HASHIDS_, 9);
				$direktorat = hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
				$kd_izin = $arrupdate['kd_izin'];
				$id = $arrupdate['id'];
				$tipe = base64_decode($this->input->post('tipe'));
				$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$arrupdate['kd_izin']."'";
				$nama_izin = $this->main->get_uraian($sql,'nama_izin');
				$txt = "Proses permohonan";
                if($this->input->post('catatan') == '' && $arrupdate['status'] == '0501'){
                     return "MSG||NO||Harap Mengisi Catatan Terlebih Dahulu Untuk Aksi Penolakan";
                }
				if ($arrupdate['status'] == 'resend_email') {
                    // $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha('1000', $direktorat, $kd_izin, $id, $tipe, "resend");
                    $sq = " SELECT a.no_izin
                            FROM t_siujs a 
                            WHERE a.id = '".$arrupdate['id']."' AND a.status = '1000'";
                    $no_izin = $this->db->query($sq)->row_array();
                    $cek = "SELECT a.id FROM t_upload a WHERE a.nomor = '".$no_izin['no_izin']."'";
                    $res = $this->db->query($cek)->num_rows();
                    if ($res != '') {
                        $resdel = FALSE;
                        $this->db->trans_begin();
                        $this->db->where('nomor', trim($no_izin['no_izin']));
                        $this->db->delete(t_upload);

                        if ($this->db->affected_rows() >= 1) {
                            $resdel = TRUE;
                        }

                        if ($this->db->trans_status() === FALSE || !$resdel) {
                            $this->db->trans_rollback();
                            $msgok = "MSG||NO||Email Gagal Di Kirim||BACK";
                            return $msgok;
                        } else {
                            $this->db->trans_commit();
                            $msgok = "MSG||YES||Email Berhasil Di Kirim||BACK";   
                        }
                    }
                    return $msgok;
                }
				/* Update waktu tanggal kirim permohonan */
                if($this->newsession->userdata('role') == "05"){
                    if($arrupdate['status'] == "0100"){
                    	// $query = "SELECT COUNT(*) FROM t_siujs_ahli a WHERE a.permohonan_id = '".$arrupdate['id']."'";
                    	$arrupdate['updated'] = 'GETDATE()';
                    	$arrupdate['updated_user'] = $this->newsession->userdata('nama');
                        $arrupdate['tgl_kirim'] = 'GETDATE()';
                        $txt ="Mengirim permohonan";
                    }
					$selisih = 0;
                }else{
					/* Update waktu tanggal terima permohonan oleh pemroses */
					if($this->newsession->userdata('role') == '01'){
						if($arrupdate['status'] == '0700'){
							$arrupdate['updated'] = 'GETDATE()';
                    		$arrupdate['updated_user'] = $this->newsession->userdata('nama');
							$arrupdate['tgl_terima'] = 'GETDATE()';
							$arrupdate['proses_userid'] = $this->newsession->userdata('id');
							$txt = "Terima permohonan";
						}
					}
					if(($arrupdate['status'] == '0105') || ($arrupdate['status'] == '0601') || ($arrupdate['status'] == '0701')){
						$txt = "Rollback Permohonan";
						$arrupdate['updated'] = 'GETDATE()';
                    	$arrupdate['updated_user'] = $this->newsession->userdata('nama');
					}
					
					// if($arrupdate['status'] == '0102'){
					// 	$arrupdate['no_izin'] = $this->main->set_nomor($arrupdate['kd_izin']);
					// 	$arrupdate['tgl_izin'] = 'GETDATE()';
					// 	$arrupdate['tgl_izin_exp'] = $this->main->set_expired($arrupdate['kd_izin']);
					// 	$arrupdate['updated'] = 'GETDATE()';
     //                	$arrupdate['updated_user'] = $this->newsession->userdata('nama');
					// 	$txt = "Persetujuan permohonan";
					// }

					if($arrupdate['status'] == '0600'){
						$arrupdate['updated'] = 'GETDATE()';
                    	$arrupdate['updated_user'] = $this->newsession->userdata('nama');
					}

					if($arrupdate['status'] == '0200'){
						$pejabatTTD = $this->get_pejabatTTD($kd_izin);
						if($pejabatTTD){
							$arrupdate['nama_ttd'] = $pejabatTTD['nama'];
							$arrupdate['jabatan_ttd'] = $pejabatTTD['jabatan'];
							$arrupdate['nip_ttd'] = $pejabatTTD['nip'];
							$arrupdate['updated'] = 'GETDATE()';
                    		$arrupdate['updated_user'] = $this->newsession->userdata('nama');
						}
					}
					
					if($arrupdate['status'] == '1000'){
                    	$arrupdate['no_izin'] = $this->main->set_nomor($arrupdate['kd_izin'], "", $arrupdate['id']);
						$arrupdate['tgl_izin'] = 'GETDATE()';
						$arrupdate['tgl_izin_exp'] = $this->main->set_expired($arrupdate['kd_izin']);
						$arrupdate['updated'] = 'GETDATE()';
                    	$arrupdate['updated_user'] = $this->newsession->userdata('nama');
						
						if ($tipe == 'Perubahan') {
                            $get = "SELECT a.no_izin_lama from t_siujs a where a.id = '".$arrupdate['id']."'";
                            $no_izin_lama = $this->db->query($get)->row_array();
                            $arrdet['status_dok'] = '02';
                            $get_cek = "SELECT a.id from t_siujs a where a.no_izin = '".$no_izin_lama['no_izin_lama']."'";
                            $cek = $this->db->query($get_cek)->num_rows();
                            if ($cek == 1) {
                            	$this->db->where('no_izin', $no_izin_lama['no_izin_lama']);
                            	$this->db->update('t_siujs', $arrdet);
                            }   
                            //print_r($this->db->last_query());die();
                        }
                        $txt = "Penerbitan";

                        // ADD TO TABLE FLAG SIP
                        $arradd = array(
                            'id_permohonan' => $id,
                            'nm_table' => 't_siujs',
                            'status_flag' => '0',
                            'created' => 'GETDATE()',
                            'id_table' => $kd_izin
                        );
                        $this->db->insert('t_flag_sip', $arradd);
                        //print_r($this->db->last_query());die();
					}
					$selisih = (int)$this->main->get_selisih($arrupdate['kd_izin'], $arrupdate['id'], "t_siujs");//print_r($selisih);exit();
					$msgok = "MSG||YES||Data permohonan berhasil di proses||BACK";
				}
				$arrupdate['last_proses'] = 'GETDATE()';
				$requirement = $this->main->requirements($id, $kd_izin, $tipe);
				$tenagaahli = $this->cek_tenaga_ahli($id, $kd_izin, $tipe);
				// $bidang = $this->cek_bidang($id);
				//print_r($bidang."q");die();
				if(((!$requirement) || (!$tenagaahli)) && ($arrupdate['status'] == "0100")){
            		$respon = FALSE;
            		if ((!$requirement) && (!$tenagaahli)) {
            			$msgerr = "MSG||NO||Lengkapi Dokumen Persyaratan.\nLengkapi Data Tenaga Ahli.\nLengkapi Data Bidang.";
            		}elseif(!$requirement){
            			$msgerr = "MSG||NO||Lengkapi Dokumen Persyaratan.";
            		}elseif (!$tenagaahli) {
            			$msgerr = "MSG||NO||Data Tenaga Ahli Minimal Lima Orang.";
            		}
				}else{
					$this->db->trans_begin();
					$id = $arrupdate['id'];
					$kd = $arrupdate['kd_izin'];
					$aju = $arrupdate['no_aju'];
					unset($arrupdate['id']);
					unset($arrupdate['kd_izin']);
					unset($arrupdate['no_aju']);
	                $this->db->where('id', $id);
	                $this->db->where('kd_izin', $kd);
	                $this->db->where('no_aju', $aju);
	                if($this->newsession->userdata('role') == "05") {
	                    $this->db->where('trader_id', $this->newsession->userdata('trader_id'));
                            if($arrupdate['status'] == '0100'){
                                $sql = "UPDATE t_upload 
SET flag_used = '1'
							from t_upload a
							LEFT join t_upload_syarat b on a.id = b.upload_id
							LEFT JOIN m_dok_izin c on c.dok_id = b.dok_id and c.izin_id = b.izin_id  
							WHERE c.kategori <> '03' and b.izin_id = ".$kd." and b.permohonan_id = ".$id;//die($sql);
                                $this->db->query($sql);
                            }
	                }
					$this->db->update('t_siujs', $arrupdate);
					if($this->db->affected_rows() == 1){
						$respon = TRUE;
						$logu = array('aktifitas' => $txt.' '.$nama_izin.' dengan nomor permohonan : '.$aju,
									  'url' => '{c}'. site_url().'post/proccess/siujs_act/verification'. ' {m} models/proccess/siujs_act {f} set_proccess($act, $ajax)');
						$this->main->set_activity($logu);
						$logi = array('kd_izin' => $kd,
									  'permohonan_id' => $id,
									  'keterangan' => $sebelum . ' <i class="fa fa-arrow-circle-right"></i> ' .$sesudah . '<br> '.$txt.' '.$nama_izin.' dengan nomor permohonan : '.$aju,
									  'catatan' => ($_POST['catatan'] ? $_POST['catatan'] : '-'),
									  'status' => $arrupdate['status'],
									  'selisih' => $selisih);
						$this->main->set_loglicensing($logi);
                                                $this->main->set_log_oss();
					}
				} 
				if($this->db->trans_status() === FALSE || !$respon){
                    $this->db->trans_rollback();
                    return $msgerr;
                }else{
					$this->db->trans_commit();
					if($arrupdate['status'] != '1000'){
						$send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha($arrupdate['status'], $direktorat, $kd_izin, $id, $tipe);
						$send_email_pemroses = $this->email_act->email_pemroses($arrupdate['status'], $direktorat, $kd_izin, $id, $tipe);
                    }
                    return $msgok;
				}
				
			}
		}
	}

	function get_pejabatTTD($kd_izin){
		$query = "SELECT * FROM m_ttd WHERE status = 1 AND kd_izin = ".$kd_izin;
		$data = $this->main->get_result($query);
		if($data){
			foreach($query->result_array() as $row){
				$arrdata = $row;
			}
		}
		return $arrdata;
	}

	function cek_tenaga_ahli($id, $kd_izin, $tipe){
		$ret = FALSE;
		$qahli = "SELECT COUNT(*) AS JMLAHLI FROM t_siujs_ahli WHERE permohonan_id = '".$id."'";
		$jmlahli = (int)$this->main->get_uraian($qahli,"JMLAHLI");
		//print_r($jmlahli);die();

		$qsyaratahli= "SELECT COUNT(*) AS JMLSYARATAHLI FROM m_dok_izin WHERE izin_id = '".$kd_izin."' AND tipe = 1 AND kategori = 03 ";
		/*if($tipe == "Baru"){
			$qsyaratahli .= $this->main->find_where($qsyaratahli);
			$qsyaratahli .= "baru = 1";
		}else if($tipe == "Perubahan"){
			$qsyaratahli .= $this->main->find_where($qsyaratahli);
			$qsyaratahli .= "perubahan = 1";
		}else if($tipe == "Perpanjangan"){
			$qsyaratahli .= $this->main->find_where($qsyaratahli);
			$qsyaratahli .= "perpanjangan = 1";
		}*/
		$jmlsyarat = (int)$this->main->get_uraian($qsyaratahli,"JMLSYARATAHLI");
		$harusupload = $jmlahli * $jmlsyarat;
		$jmlupload = (int)$this->main->get_uraian("SELECT COUNT(*) AS JMLUPLOADAHLI FROM t_upload_syarat WHERE detail_id is not null and permohonan_id = '".$id."' GROUP BY permohonan_id","JMLUPLOADAHLI");
		if($jmlahli <= 4){
			$ret = FALSE;
		}else{
			if($jmlupload >= $harusupload){
				$ret = TRUE;
			}
		}
                $ret = TRUE;
		return $ret;
	}

	function cek_bidang($id){
		$ret = FALSE;
		$qbidang = "SELECT COUNT(*) AS JMLBIDANG FROM t_siujs_bidang WHERE permohonan_id = '".$id."'";
		//print_r($qbidang);die();
		$jmlbidang = (int)$this->main->get_uraian($qbidang,"JMLBIDANG");
		//print_r($jmlbidang.'z');die();
		if($jmlbidang > 0){
			$ret = TRUE;
		}
		return $ret;

	}

}
?>