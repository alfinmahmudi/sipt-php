<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sppgrap_cetak_act extends CI_Model {

    function set_prints($a, $b, $c = "") {
        //	if($this->newsession->userdata('_LOGGED')){
        /*
          @ $a = Kode Izin
          @ $b = Permohonan ID
         */
        $arrdata = array();

        $query = "SELECT a.jabatan_pj,a.id, a.no_izin, a.tgl_izin_exp as exp, dbo.dateIndo(a.tgl_izin) AS izin_ind , a.tgl_izin as tgl_izin , b.direktorat_id, a.kd_izin, a.no_aju, a.trader_id, a.tipe_permohonan, c.uraian AS permohonan, a.npwp, d.uraian AS tipe_perusahaan, d.uraian +'. '+a.nm_perusahaan as nm_perusahaan, 
					a.almt_perusahaan, dbo.get_region(2, a.kdprop) AS prop_perusahaan, dbo.get_region(4, a.kdkab) AS kab_perusahaan, dbo.get_region(7, a.kdkec) AS kec_perusahaan, dbo.get_region(10, a.kdkel) AS kel_perusahaan,
					a.kdpos, a.nama_pj, a.telp, a.fax, a.id_pgapt, a.no_pgapt, a.tgl_pgapt, a.tgl_exp_pgapt, a.status_dok,
					a.jns_gula, a.jml_gula, f.uraian as discharge_gula, e.uraian as loading_gula, g.uraian as asal_gula, 
					dbo.get_region(2, a.kdprop_asal_gula) as kdprop_asal_gula, dbo.get_region(2, a.kdprop_tujuan_gula) as kdprop_tujuan_gula,
					a.status, b.nama_izin, b.disposisi, f.uraian as discharge_gula,
					m.nama_produsen as nm_produsen, m.alamat as alamat_produsen, n.nama_produsen as nm_industri, n.alamat as alm_industri, dbo.get_region(2, n.kdprop) AS prop_produsen,
					dbo.get_region(4, n.kdkab) AS kab_produsen, dbo.get_region(7, n.kdkec) AS kec_produsen, dbo.get_region(10, n.kdkel) AS kel_produsen, a.tgl_aju, a.nama_ttd, a.jabatan_ttd, h.uraian as jabatan_pj
					FROM t_sppgrapt a 
					LEFT JOIN m_izin b ON b.id = a.kd_izin 
					LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
					LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'KDTIPE_PERUSAHAAN'
                                        LEFT JOIN m_reff h ON d.kode = a.jabatan_pj AND h.jenis = 'JABATAN'
					LEFT JOIN m_peldn e ON e.kode = a.loading_gula
					LEFT JOIN m_peldn f ON f.kode = a.discharge_gula
					LEFT JOIN m_reff g ON g.kode = a.asal_gula AND g.jenis = 'PRODUKSI'
					LEFT JOIN t_sppgrapt_distributor m ON a.id = m.permohonan_id
				    LEFT JOIN t_sppgrapt_produksi n ON a.id = n.permohonan_id
					  WHERE a.id = '" . hashids_decrypt($b, _HASHIDS_, 9) . "'";

        $arrdata['row'] = $this->db->query($query)->row_array();
        $ss = "	SELECT a.nama_produsen, a.alamat ,  dbo.get_region(2,kdprop) as prop,dbo.get_region(4,kdkab) as kab, dbo.get_region(7,kdkec) as kec, dbo.get_region(10,kdkel) as kel     FROM t_sppgrapt_distributor a 
					WHERE a.permohonan_id = '" . hashids_decrypt($b, _HASHIDS_, 9) . "' ";
        $arrdata['nama_industri'] = $this->db->query($ss)->result_array(); //print_r($arrdata['nama_industri']);die();
        $qy = "SELECT b.tgl_dok, b.tgl_exp, b.penerbit_dok,b.nomor
				 FROM t_upload_syarat a 
				 LEFT join t_upload b on b.id = a.upload_id
				 LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id
				 WHERE a.izin_id = '" . $a . "' and a.permohonan_id = '" . hashids_decrypt($b, _HASHIDS_, 9) . "' and a.detail_id is null";
        $arrdata['upload'] = $this->db->query($qy)->row_array();
        $arrdata['ttd'] = $this->suggest_ttd($a);
        $arrdata['link'] = url_sipt . 'verifikasi/' . hashids_encrypt((int) $arrdata['row']['direktorat_id'], _HASHIDS_, 9) . '/' . hashids_encrypt($a, _HASHIDS_, 9) . '/' . $b;
        $arrdata['tembusan'] = $this->get_tembusan($a, $b);
        //$arrdata['pemasaran'] = $this->get_pemasaran($a,$b,$arrdata['row']['pemasaran']);
        $arrdata['urlFoto'] = $this->get_pendukung($a, $b, '16');
        $arrdata['suratDistributor'] = $this->get_pendukung($a, $b, '23');
        $arrdata['suratSubDistributor'] = $this->get_pendukung($a, $b, '28');
        $arrdata['suratITMB'] = $this->get_pendukung($a, $b, '44');
        $arrdata['suratSKPLA'] = $this->get_pendukung($a, $b, '33');
        $arrdata['suratSKPA'] = $this->get_pendukung($a, $b, '30');
        //print_r($_POST['catatan']);die();
        $no_aju = str_replace("/", "", $arrdata['row']['no_aju']);
        if ($c != "") {
            $kode = '88';
            $folder = 'DOKSPPGRAP';
            $arrdata['namafile'] = "DOCUMENT-" . $no_aju. '.pdf';
            $arrdata['dir'] = './upL04d5/document/' . $folder . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            $sqlInsertPendukung = "INSERT INTO t_upload (trader_id, tipe_dok, nomor, tgl_dok, tgl_exp, penerbit_dok, nama_file, ori_file, folder, ekstensi, ukuran, created_user, created) 
				VALUES('" . $arrdata['row']['trader_id'] . "','" . $kode . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['row']['tgl_izin'] . "','" . $arrdata['row']['exp'] . "','Direktorat Jenderal Perdagangan Dalam Negeri','" . $arrdata['namafile'] . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['dir'] . "','.pdf','0','system',GETDATE())";
            $insert = $this->db->query($sqlInsertPendukung);
        }
        return $arrdata;
        //	}
    }

    function get_tembusan($a, $b) {
        $tembusan = array();
        $query = "SELECT urutan, keterangan FROM m_tembusan WHERE kd_izin = " . $a . " AND permohonan_id =" . hashids_decrypt($b, _HASHIDS_, 9);
        $data = $this->main->get_result($query);
        if ($data) {
            $tembusan = $query->result_array();
        }

        return $tembusan;
    }

    function suggest_ttd($a){
        $sql = "SELECT nama, jabatan FROM m_ttd a WHERE a.kd_izin = '".$a."' AND a.status = '1'";
        $data = $this->main->get_result($sql);
        if ($data) {
            $ttd = $sql->row_array();
        }
        return $ttd;
    }

    function get_pemasaran($a, $b, $c) {
        if ($a == '4') {
            $idprop = str_replace(";", "','", $c);
            $sqlprop = "SELECT 'Prov. ' + nama AS nama FROM m_prop WHERE id IN ('" . $idprop . "')";
            $dataprop = $this->main->get_result($sqlprop);
            if ($dataprop) {
                foreach ($sqlprop->result_array() as $rprop) {
                    $arrprop[] = $rprop['nama'];
                }
                $pemasaran = join("; ", $arrprop);
            }
        } elseif (($a != '4') || ($a != '3')) {
            $idkab = str_replace(";", "','", $c);
            $sqlkab = "SELECT nama FROM m_kab WHERE id IN ('" . $idkab . "')";
            $datakab = $this->main->get_result($sqlkab);
            if ($datakab) {
                foreach ($sqlkab->result_array() as $rkab) {
                    $arrkab[] = $rkab['nama'];
                }
                $pemasaran = join("; ", $arrkab);
            }
        }
        return $pemasaran;
    }

    function get_pendukung($a, $b, $c) {
        $query = "SELECT b.nomor, dbo.dateIndo(b.tgl_dok) AS tgl_dok, b.penerbit_dok, SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file as url, b.ekstensi
				FROM t_upload_syarat a 
				LEFT JOIN t_upload b ON b.id = a.upload_id
				WHERE a.permohonan_id = " . hashids_decrypt($b, _HASHIDS_, 9) . " AND a.izin_id = " . $a . " AND a.dok_id =" . $c;
        //print_r($query);die();
        $data = $this->main->get_result($query);
        if ($data) {
            $arrsiupmb = array('23', '28', '33', '30');
            if (in_array($c, $arrsiupmb)) {
                $arrPendukung = $query->result_array();
                $banyakPendukung = count($arrPendukung);
                for ($p = 0; $p < $banyakPendukung; $p++) {
                    $pendukung .= $arrPendukung[$p]['penerbit_dok'];
                    $pendukung .= " Nomor :" . $arrPendukung[$p]['nomor'] . " tanggal " . $arrPendukung[$p]['tgl_dok'];

                    if ($banyakPendukung > 2) {
                        if ($p != ($banyakPendukung - 1)) {
                            $pendukung .= "., ";
                        } else {
                            $pendukung .= " ";
                        }
                    } else if ($banyakPendukung == 2) {
                        if ($p != ($banyakPendukung - 1)) {
                            $pendukung .= "., ";
                        } else {
                            $pendukung .= " ";
                        }
                    } else {
                        $pendukung .= " ";
                    }
                }
            } else {
                foreach ($query->result_array() as $row) {
                    $pendukung = $row;
                }
            }
        }
        return $pendukung;
    }

}

?>