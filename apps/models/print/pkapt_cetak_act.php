<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pkapt_cetak_act extends CI_Model {

    function set_prints($a, $b, $c = "") {
        //if($this->newsession->userdata('_LOGGED')){
        /*
          @ $a = Kode Izin
          @ $b = Permohonan ID
         */
        $arrdata = array();

        $query = "SELECT a.no_izin,  a.id, b.direktorat_id, a.kd_izin, a.no_aju, dbo.dateIndo(a.tgl_aju) AS tgl_aju, a.trader_id, a.tipe_permohonan, c.uraian AS permohonan, dbo.npwp(a.npwp) AS npwp, d.uraian AS tipe_perusahaan, 
					a.almt_perusahaan, dbo.get_region(2, a.kdprop) AS prop_perusahaan, dbo.get_region(4, a.kdkab) AS kab_perusahaan, dbo.get_region(7, a.kdkec) AS kec_perusahaan, dbo.get_region(10, a.kdkel) AS kel_perusahaan,
					a.kdpos, a.telp, a.fax, i.uraian as identitas_pj, a.nama_pj, a.noidentitas_pj, a.jabatan_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, a.alamat_pj, 
					a.almt_perusahaan, dbo.get_region(2, a.kdprop_pj) AS prop_pj, dbo.get_region(4, a.kdkab_pj) AS kab_pj, dbo.get_region(7, a.kdkec_pj) AS kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj,
					a.telp_pj, a.fax_pj, a.no_pkapt, a.status_dok,
					a.status, b.nama_izin, b.disposisi, dbo.get_region(2,a.kdprop_rekom) AS kdprop_rekom, a.tgl_rekom, a.no_rekom,a.nama_rekom, a.tgl_izin, a.tgl_izin_exp,
					f.no_tdp, a.nama_ttd, a.jabatan_ttd, dbo.dateIndo(a.tgl_izin) AS izin, dbo.dateIndo(a.tgl_izin_exp) AS exp, g.nama as kab_rekom,
                    CASE WHEN a.tipe_perusahaan = '05' THEN a.nm_perusahaan
                    ELSE p.uraian+'. '+a.nm_perusahaan
                    END as 'nm_perusahaan'
					FROM t_pkapt a 
					LEFT JOIN m_izin b ON b.id = a.kd_izin 
                                        LEFT JOIN m_kab g ON g.id = a.kdkab_rekom
					LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
					LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
                                        LEFT JOIN m_reff p ON p.kode = a.tipe_perusahaan AND p.jenis = 'KDTIPE_PERUSAHAAN'
					LEFT JOIN m_trader f ON f.id = a.trader_id
				    WHERE a.id = '" . hashids_decrypt($b, _HASHIDS_, 9) . "'";
                    //print_r($query);die();
        $arrdata['row'] = $this->db->query($query)->row_array();
        $arrdata['ttd'] = $this->suggest_ttd($a);
        $arrdata['link'] = url_sipt . 'verifikasi/' . hashids_encrypt((int) $arrdata['row']['direktorat_id'], _HASHIDS_, 9) . '/' . hashids_encrypt($a, _HASHIDS_, 9) . '/' . $b;
        $arrdata['suges'] = $this->get_suges($a, $b);//print_r($arrdata['suges']);die();
        $arrdata['tembusan'] = $this->get_tembusan($a, $b);
        //$arrdata['pemasaran'] = $this->get_pemasaran($a,$b,$arrdata['row']['pemasaran']);
        $arrdata['urlFoto'] = $this->get_pendukung($a, $b, '16');
        $arrdata['siup'] = $this->get_pendukung($a, $b, '128'); //print_r($arrdata['siup']['nomor']);die();
        $no_aju = str_replace("/", "", $arrdata['row']['no_aju']);
        if ($c != "") {
            $kode = '60';
            $folder = 'LMPKAPT';
            $arrdata['namafile'] = "DOCUMENT-" . $no_aju.'.pdf';
            $arrdata['dir'] = './upL04d5/document/' . $folder . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            $sqlInsertPendukung = "INSERT INTO t_upload (trader_id, tipe_dok, nomor, tgl_dok, tgl_exp, penerbit_dok, nama_file, ori_file, folder, ekstensi, ukuran, created_user, created) 
				VALUES('" . $arrdata['row']['trader_id'] . "','" . $kode . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['row']['tgl_izin'] . "','" . $arrdata['row']['tgl_izin_exp'] . "','Direktorat Jenderal Perdagangan Dalam Negeri','" . $arrdata['namafile'] . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['dir'] . "','.pdf','0','system',GETDATE())";
            $insert = $this->db->query($sqlInsertPendukung);
        }
        if($arrdata['row']['no_tdp'] == ""){
            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id
							LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id  
							WHERE c.kategori = '01' and a.dok_id = '31' and a.izin_id = '" .$a . "' and a.permohonan_id = '" . hashids_decrypt($b, _HASHIDS_, 9) . "'";
							//print_r($query_syarat);die();
							$data = $this->main->get_result($query_syarat);
            $arr = array();
            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
            }
            $arrdata['row']['no_tdp'] = $keys['nomor'];
//            $arrdata['row']['no_tdp'] = 'a';
//            print_r($keys['nomor']);die();
        }
        return $arrdata;
        //}
    }

    function get_tembusan($a, $b) {
        $tembusan = array();
        $query = "SELECT urutan, keterangan FROM m_tembusan WHERE kd_izin = " . $a . " AND permohonan_id =" . hashids_decrypt($b, _HASHIDS_, 9);
        $data = $this->main->get_result($query);
        if ($data) {
            $tembusan = $query->result_array();
        }

        return $tembusan;
    }

    function suggest_ttd($a){
        $sql = "SELECT nama, jabatan FROM m_ttd a WHERE a.kd_izin = '".$a."' AND a.status = '1'";
        $data = $this->main->get_result($sql);
        if ($data) {
            $ttd = $sql->row_array();
        }
        return $ttd;
    }

    function get_pemasaran($a, $b, $c) {
        if ($a == '4') {
            $idprop = str_replace(";", "','", $c);
            $sqlprop = "SELECT 'Prov. ' + nama AS nama FROM m_prop WHERE id IN ('" . $idprop . "')";
            $dataprop = $this->main->get_result($sqlprop);
            if ($dataprop) {
                foreach ($sqlprop->result_array() as $rprop) {
                    $arrprop[] = $rprop['nama'];
                }
                $pemasaran = join("; ", $arrprop);
            }
        } elseif (($a != '4') || ($a != '3')) {
            $idkab = str_replace(";", "','", $c);
            $sqlkab = "SELECT nama FROM m_kab WHERE id IN ('" . $idkab . "')";
            $datakab = $this->main->get_result($sqlkab);
            if ($datakab) {
                foreach ($sqlkab->result_array() as $rkab) {
                    $arrkab[] = $rkab['nama'];
                }
                $pemasaran = join("; ", $arrkab);
            }
        }
        return $pemasaran;
    }

    function get_pendukung($a, $b, $c) {
        $query = "SELECT b.nomor, dbo.dateIndo(b.tgl_dok) AS tgl_dok, b.penerbit_dok, SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file as url, b.ekstensi
				FROM t_upload_syarat a 
				LEFT JOIN t_upload b ON b.id = a.upload_id
				WHERE a.permohonan_id = " . hashids_decrypt($b, _HASHIDS_, 9) . " AND a.izin_id = " . $a . " AND a.dok_id =" . $c;
        //print_r($query);die();
        $data = $this->main->get_result($query);
        if ($data) {
            $arrsiupmb = array('23', '28', '33', '30');
            if (in_array($c, $arrsiupmb)) {
                $arrPendukung = $query->result_array();
                $banyakPendukung = count($arrPendukung);
                for ($p = 0; $p < $banyakPendukung; $p++) {
                    $pendukung .= $arrPendukung[$p]['penerbit_dok'];
                    $pendukung .= " Nomor :" . $arrPendukung[$p]['nomor'] . " tanggal " . $arrPendukung[$p]['tgl_dok'];

                    if ($banyakPendukung > 2) {
                        if ($p != ($banyakPendukung - 1)) {
                            $pendukung .= "., ";
                        } else {
                            $pendukung .= " ";
                        }
                    } else if ($banyakPendukung == 2) {
                        if ($p != ($banyakPendukung - 1)) {
                            $pendukung .= "., ";
                        } else {
                            $pendukung .= " ";
                        }
                    } else {
                        $pendukung .= " ";
                    }
                }
            } else {
                foreach ($query->result_array() as $row) {
                    $pendukung = $row;
                }
            }
        }
        return $pendukung;
    }

    public function get_suges($a, $b){
        $id = hashids_decrypt($b, _HASHIDS_, 9);
        $sql = "SELECT a.tipe_permohonan, a.tipe_perusahaan FROM t_pkapt a WHERE a.id = '" . $id . "'";
        $tipe_permohonan = $this->db->query($sql)->row_array();
        if ($tipe_permohonan['tipe_permohonan'] == '01') {
            $ss = "SELECT b.nomor 
                    FROM t_upload_syarat a
                    LEFT JOIN t_upload b ON b.id = a.upload_id 
                    WHERE a.permohonan_id = '" . $id . "' AND a.dok_id = '31' AND a.izin_id = '" . $a . "' ";
            $no_tdp = $this->db->query($ss)->row_array();
            $tdp = str_replace(".", "", $no_tdp['nomor']);
            $tdp1 = substr($tdp, 0, 2);
            $tdp2 = substr($tdp, 2, 2);
            $tdp3 = substr($tdp, 4, 1);
            $aa = "SELECT nomor + 1 as nomor from m_pkapt";
            $nomor = $this->db->query($aa)->row_array();
            $arrupdate['no_pkapt'] = $tdp1 . '.' . $tdp2 . '.' . $tdp3 . '.' . str_pad($nomor['nomor'],5,'0',STR_PAD_LEFT);
            // $arrupdate['no_izin'] = $this->main->set_nomor($arrupdate['kd_izin']);
            // $arrno['nomor'] = $nomor['nomor'];
            // $this->db->update('m_pkapt', $arrno);
            return $arrupdate;
        }
    }
}

?>