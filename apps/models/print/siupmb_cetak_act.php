<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Siupmb_cetak_act extends CI_Model {

    function set_prints($a, $b, $c = "") {
        //if($this->newsession->userdata('_LOGGED')){
        /*
          @ $a = Kode Izin
          @ $b = Permohonan ID
         */
        $arrdata = array();
        if (in_array($a, array(6, 13))) {
            $query = "SELECT a.tgl_terima, a.tgl_aju, a.id, a.kd_izin,  a.no_aju, a.no_izin, a.tgl_izin as tgl_dok, a.tgl_izin_exp as tgl_exp, dbo.dateIndo(a.tgl_izin) AS tgl_izin, dbo.dateIndo(a.tgl_izin_exp) AS tgl_izin_exp , a.trader_id, a.tipe_permohonan, 
					  c.uraian AS permohonan ,dbo.npwp(a.npwp) AS npwp, d.uraian AS tipe_perusahaan, a.almt_usaha,
					  dbo.get_region(2, a.kdprop_usaha) AS prop_perusahaan, dbo.get_region(4, a.kdkab_usaha) AS kab_perusahaan,
					  dbo.get_region(7, a.kdkec_usaha) AS kec_perusahaan, dbo.get_region(10, a.kdkel_usaha) AS kel_perusahaan,
					  a.kdpos_usaha, a.telp_usaha, a.fax_usaha, a.no_siup, a.tgl_siup, k.uraian AS jenis_siup,
					  l.uraian AS identitas_pj, a.nama_pj, a.noidentitas_pj, a.jabatan_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, a.alamat_pj,
					  dbo.get_region(2, a.kdprop_pj) prop_pj, dbo.get_region(4, a.kdkab_pj) AS kab_pj,
					  dbo.get_region(7, a.kdkec_pj) kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj, a.telp_pj, a.fax_pj,
					  dbo.rupiah(a.nilai_modal) AS nilai_modal, a.status_dok, nama_pj_usaha, alamat_pj_usaha, identitas_pj_usaha,
					  a.fl_gol_a, a.fl_gol_b,a.fl_gol_c, dbo.get_region(2,a.pemasaran) as wil_pem,
					  a.jns_gol_a, a.jns_gol_b , a.jns_gol_c,
					  a.gol_a, a.gol_b,a.gol_c, a.no_penjelasan, a.tgl_penjelasan,
					  b.nama_izin, b.disposisi, b.direktorat_id,
					  a.pemasaran, a.nama_ttd, a.jabatan_ttd, a.nip_ttd, a.status, a.almt_usaha, nm_usaha,
                      CASE WHEN a.tipe_perusahaan = '05' or a.tipe_perusahaan = '07' or a.tipe_perusahaan = '06' THEN a.nm_perusahaan
                      ELSE p.uraian+'. '+a.nm_perusahaan
                      END as 'nm_perusahaan'
					  FROM t_siupmb a
					  LEFT JOIN m_izin b ON b.id = a.kd_izin
					  LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN'
					  LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
					  LEFT JOIN m_reff k ON k.kode = a.lokasi AND k.jenis = 'JENIS_SIUP'
					  LEFT JOIN m_reff l ON l.kode = a.identitas_pj AND l.jenis = 'JENIS_IDENTITAS'
                                           LEFT JOIN m_reff p ON p.kode = a.tipe_perusahaan AND p.jenis = 'KDTIPE_PERUSAHAAN'
					  WHERE a.id = '" . hashids_decrypt($b, _HASHIDS_, 9) . "'";
        } else {

            $query = "SELECT a.tgl_terima, a.tgl_aju, a.id, a.kd_izin,  a.no_aju, a.no_izin, a.tgl_izin as tgl_dok, a.tgl_izin_exp as tgl_exp, dbo.dateIndo(a.tgl_izin) AS tgl_izin, dbo.dateIndo(a.tgl_izin_exp) AS tgl_izin_exp , a.trader_id, a.tipe_permohonan, 
					  c.uraian AS permohonan ,dbo.npwp(a.npwp) AS npwp, d.uraian AS tipe_perusahaan, a.almt_perusahaan,
					  dbo.get_region(2, a.kdprop) AS prop_perusahaan, dbo.get_region(4, a.kdkab) AS kab_perusahaan,
					  dbo.get_region(7, a.kdkec) AS kec_perusahaan, dbo.get_region(10, a.kdkel) AS kel_perusahaan,
					  a.kdpos, a.telp, a.fax, a.no_siup, a.tgl_siup, k.uraian AS jenis_siup, a.penetapan_itmb,
					  l.uraian AS identitas_pj, a.nama_pj, a.noidentitas_pj, a.jabatan_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, a.alamat_pj,
					  dbo.get_region(2, a.kdprop_pj) prop_pj, dbo.get_region(4, a.kdkab_pj) AS kab_pj,
					  dbo.get_region(7, a.kdkec_pj) kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj, a.telp_pj, a.fax_pj,
					  dbo.rupiah(a.nilai_modal) AS nilai_modal, a.status_dok,
					  a.fl_gol_a, a.fl_gol_b,a.fl_gol_c, dbo.get_region(2,a.pemasaran) as wil_pem,
					  a.jns_gol_a, a.jns_gol_b , a.jns_gol_c,
					  a.gol_a, a.gol_b,a.gol_c, a.no_penjelasan, a.tgl_penjelasan,
					  b.nama_izin, b.disposisi, b.direktorat_id,
					  a.pemasaran, a.nama_ttd, a.jabatan_ttd, a.nip_ttd, a.status, a.almt_usaha, nm_usaha,
                      CASE WHEN a.tipe_perusahaan = '05' or a.tipe_perusahaan = '07' or a.tipe_perusahaan = '06' THEN a.nm_perusahaan
                      ELSE p.uraian+'. '+a.nm_perusahaan
                      END as 'nm_perusahaan'
					  FROM t_siupmb a
					  LEFT JOIN m_izin b ON b.id = a.kd_izin
					  LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN'
					  LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
					  LEFT JOIN m_reff k ON k.kode = a.lokasi AND k.jenis = 'JENIS_SIUP'
					  LEFT JOIN m_reff l ON l.kode = a.identitas_pj AND l.jenis = 'JENIS_IDENTITAS'
                                           LEFT JOIN m_reff p ON p.kode = a.tipe_perusahaan AND p.jenis = 'KDTIPE_PERUSAHAAN'
					  WHERE a.id = '" . hashids_decrypt($b, _HASHIDS_, 9) . "'";
        }

        $arrdata['row'] = $this->db->query($query)->row_array(); //print_r($arrdata['row']);die();
        if ($arrdata['row']['jns_gol_a']) {
            $ex_a = explode(",", $arrdata['row']['jns_gol_a']);

            foreach ($ex_a as $ax) {
                $sql = $this->db->query("select dbo.get_jenisminol('" . $ax . "') as 'jns'")->result_array();
                $tmp[] = $sql[0]['jns'];
            }
        }
        if ($arrdata['row']['jns_gol_b']) {
            $ex_b = explode(",", $arrdata['row']['jns_gol_b']);
            foreach ($ex_b as $bx) {
                $sqlb = $this->db->query("select dbo.get_jenisminol('" . $bx . "') as 'jns'")->result_array();
                $tmpb[] = $sqlb[0]['jns'];
            }
        }
        if ($arrdata['row']['jns_gol_c']) {
            $ex_c = explode(",", $arrdata['row']['jns_gol_c']);
            foreach ($ex_c as $cx) {
                $sqlc = $this->db->query("select dbo.get_jenisminol('" . $cx . "') as 'jns'")->result_array();
                $tmpc[] = $sqlc[0]['jns'];
            }
        }

        $arrdata['jns_gol_a'] = $tmp;
        $arrdata['jns_gol_b'] = $tmpb;
        $arrdata['jns_gol_c'] = $tmpc;
        $arrdata['catatan'] = $_POST['catatan'];
        //print_r(hashids_encrypt((int)$arrdata['row']['direktorat_id'], _HASHIDS_, 9)."-".hashids_encrypt(2, _HASHIDS_, 9));die();
        $arrdata['link'] = url_sipt . 'verifikasi/' . hashids_encrypt((int) $arrdata['row']['direktorat_id'], _HASHIDS_, 9) . '/' . hashids_encrypt($a, _HASHIDS_, 9) . '/' . $b;
        
        $arrdata['tembusan'] = $this->get_tembusan($a, $b);
        $arrdata['ttd'] = $this->suggest_ttd($a);

        if ($a == '3') {
            $penetapan = $this->get_penetapan($a, $b, $arrdata['row']['penetapan_itmb']);
            $arrdata['penetapan'] = implode(",", $penetapan); //print_r($arrdata['penetapan']);die();
        }

        $pemasaran = $this->get_pemasaran($a, $b, $arrdata['row']['pemasaran']);
        $arrdata['pemasaran'] = implode(",", $pemasaran); //print_r($arrdata['pemasaran']);die();
        
        $arrdata['urlFoto'] = $this->get_pendukung($a, $b, '16'); //print_r($arrdata['urlFoto']);die();
        $arrdata['suratDistributor'] = $this->get_pendukung($a, $b, '54');
        $arrdata['suratSubDistributor'] = $this->get_pendukung($a, $b, '28'); //print_r($arrdata['suratDistributor']);die();
        $arrdata['suratITMB'] = $this->get_pendukung($a, $b, '44');
        $arrdata['suratSKPLA'] = $this->get_pendukung($a, $b, '33');//print_r($arrdata['suratSKPLA']);die("ok");
        $arrdata['suratSKPA'] = $this->get_pendukung($a, $b, '30');
        $arrdata['suggest'] = $this->get_suggest($a, $b, $arrdata['row']['tipe_permohonan']);
        $no_aju = str_replace("/", "", $arrdata['row']['no_aju']);
        if ($c != "") {
            if ($arrdata['row']['kd_izin'] == '3') {
                $kode = '49';
                $folder = 'SIUPMBIT';
            } elseif ($arrdata['row']['kd_izin'] == '4') {
                $kode = '79';
                $folder = 'DOKMBDIST';
            } elseif ($arrdata['row']['kd_izin'] == '5') {
                $kode = '57';
                $folder = 'SIUPDIST';
            } elseif ($arrdata['row']['kd_izin'] == '6') {
                $kode = '76';
                $folder = 'DOKSKPA';
            } elseif ($arrdata['row']['kd_izin'] == '13') {
                $kode = '77';
                $folder = 'DOKSKPLA';
            }
            $arrdata['namafile'] = "DOCUMENT-" . $no_aju . '.pdf';
            $arrdata['dir'] = './upL04d5/document/' . $folder . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            $sqlInsertPendukung = "INSERT INTO t_upload (trader_id, tipe_dok, nomor, tgl_dok, tgl_exp, penerbit_dok, nama_file, ori_file, folder, ekstensi, ukuran, created_user, created) 
					VALUES('" . $arrdata['row']['trader_id'] . "','" . $kode . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['row']['tgl_dok'] . "','" . $arrdata['row']['tgl_exp'] . "','Direktorat Jenderal Perdagangan Dalam Negeri','" . $arrdata['namafile'] . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['dir'] . "','.pdf','0','system',GETDATE())";
            $insert = $this->db->query($sqlInsertPendukung);
        }


        //print_r($arrdata);die();
        return $arrdata;
        //}
    }

    function get_tembusan($a, $b) {
        $tembusan = array();
        $query = "SELECT urutan, keterangan FROM m_tembusan WHERE kd_izin = " . $a . " AND permohonan_id =" . hashids_decrypt($b, _HASHIDS_, 9);
        $data = $this->main->get_result($query);
        if ($data) {
            $tembusan = $query->result_array();
        }

        return $tembusan;
    }

    function suggest_ttd($a) {
        $sql = "SELECT nama, jabatan FROM m_ttd a WHERE a.kd_izin = '" . $a . "' AND a.status = '1'";
        $data = $this->main->get_result($sql);
        if ($data) {
            $ttd = $sql->row_array();
        }
        return $ttd;
    }

    function get_pemasaran($a, $b, $c) {
        $ex = explode(",", $c);//print_r($ex);die();
        foreach ($ex as $id_ex) {
            $jml = strlen($id_ex);
            if ($jml != '0' && $jml == '4') {
                $sqlkab = "SELECT nama FROM m_kab WHERE id = ('" . $id_ex . "')";
                $datakab = $this->db->query($sqlkab)->row_array();
                $arrkab[] = $datakab['nama'];
            }elseif ($jml != '0' && $jml == '2') {
                if ($id_ex == '00') {
                    $arrkab[] = "Seluruh Indonesia";    
                }else{
                    $sqlkab = "SELECT nama FROM m_prop WHERE id = ('" . $id_ex . "')";
                    $datakab = $this->db->query($sqlkab)->row_array();
                    $arrkab[] = "Propinsi ".$datakab['nama'];
                }
            }elseif ($jml != '0' && $jml == '1'){
                $sqlkab = "SELECT a.kode, a.uraian FROM m_reff a where a.jenis = 'PEMASARAN_ITMB' AND kode = '" . $id_ex . "'";
                $datakab = $this->db->query($sqlkab)->row_array();
                $arrkab[] = $datakab['uraian'];
            }
        }
        return $arrkab;
    }

    function get_penetapan($a, $b, $c) {
        $ex = explode(",", $c);//print_r($ex);die();
        foreach ($ex as $id_ex) {
            $jml = strlen($id_ex);
            if ($jml == '2') {
                $sqlkab = "SELECT a.kode, a.uraian FROM m_reff a where a.jenis = 'PENETAPAN_ITMB' AND kode = '" . $id_ex . "'";//print_r($sqlkab);die();
                $datakab = $this->db->query($sqlkab)->row_array();
                $arrkab[] = $datakab['uraian'];
            }
        }
        return $arrkab;
    }

    function get_pendukung($a, $b, $c) {
        $query = "SELECT b.nomor, dbo.dateIndo(b.tgl_dok) AS tgl_dok, b.penerbit_dok, SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file as url, b.ekstensi
				FROM t_upload_syarat a 
				LEFT JOIN t_upload b ON b.id = a.upload_id
				WHERE a.permohonan_id = " . hashids_decrypt($b, _HASHIDS_, 9) . " AND a.izin_id = " . $a . " AND a.dok_id =" . $c;
        //print_r($query);die();
        $data = $this->main->get_result($query);
        if ($data) {
            $arrsiupmb = array('115', '28', '33', '30', '54', '119');
            if (in_array($c, $arrsiupmb)) {
                $arrPendukung = $query->result_array();
                $banyakPendukung = count($arrPendukung);
                for ($p = 0; $p < $banyakPendukung; $p++) {
                    $pendukung .= $arrPendukung[$p]['penerbit_dok'];
                    $pendukung .= " Nomor : " . $arrPendukung[$p]['nomor'] . " tanggal " . $arrPendukung[$p]['tgl_dok'];

                    if ($banyakPendukung > 2) {
                        if ($p != ($banyakPendukung - 1)) {
                            $pendukung .= "., ";
                        } else {
                            $pendukung .= " ";
                        }
                    } else if ($banyakPendukung == 2) {
                        if ($p != ($banyakPendukung - 1)) {
                            $pendukung .= "., ";
                        } else {
                            $pendukung .= " ";
                        }
                    } else {
                        $pendukung .= " ";
                    }
                }
            } else {
                foreach ($query->result_array() as $row) {
                    $pendukung = $row;
                }
            }
        }
        return $pendukung;
    }

    function get_tahun($kd_izin, $dok_id, $permohonan_id){
        $query = "  SELECT dbo.dateIndo(MAX(b.tgl_exp)) as tgl_exp, DATEDIFF(day, CONVERT(DATE,getdate(),120), CONVERT(DATE,MAX(b.tgl_exp),120)) as 'result'
                    FROM t_upload_syarat a
                    LEFT JOIN t_upload b ON b.id = a.upload_id
                    WHERE a.izin_id = '".$kd_izin."' AND a.dok_id = '".$dok_id."' AND a.permohonan_id = '".hashids_decrypt($permohonan_id, _HASHIDS_, 9)."'";
        $arrresult = $this->db->query($query)->row_array();
        return $arrresult;
    }

    function get_suggest($a, $b, $tipe_permohonan){
        $perubahan_it = $this->get_tahun($a, '49', $b);
        $perubahan_dis = $this->get_tahun($a, '79', $b);
        $perubahan_subdis = $this->get_tahun($a, '57', $b);
        $perubahan_skpa = $this->get_tahun($a, '76', $b);
        $perubahan_skpla = $this->get_tahun($a, '77', $b);
        if ($tipe_permohonan == '02' && $b != 'nD3kzMYe8') {
            if ($a == 3) {
                $arrupdate['tgl_izin_exp'] = $perubahan_it['tgl_exp'];//print_r($perubahan_it);die("okokok");
            }elseif ($a == 4) {
                $arrupdate['tgl_izin_exp'] = $perubahan_dis['tgl_exp'];
            }elseif ($a == 5) {
                $arrupdate['tgl_izin_exp'] = $perubahan_subdis['tgl_exp'];
            }elseif ($a == 6) {
                $arrupdate['tgl_izin_exp'] = $perubahan_skpa['tgl_exp'];
            }elseif ($a == 13) {
                $arrupdate['tgl_izin_exp'] = $perubahan_skpla['tgl_exp'];
            }
            return $arrupdate;
        }
        if ($a == '4') {
            $tahun_dis = $this->get_tahun($a, '54', $b);//print_r($tahun_dis);die();
            // $pen_it = $this->get_tahun($a, '115', $b);//print_r($pen_it);die();
            // if (trim($pen_it['result']) != '') {
            //     if ($pen_it['result'] < $tahun_dis['result']) {
            //         if ($pen_it['result'] <= '1080') {
            //             $arrupdate['tgl_izin_exp'] = $pen_it['tgl_exp'];
            //         }else{
            //             $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
            //         }  
            //     }else{
            //         if ($tahun_dis['result'] <= '1080') {
            //             $arrupdate['tgl_izin_exp'] = $tahun_dis['tgl_exp'];
            //         }else{
            //             $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
            //         }
            //     }
            // }else
            if (trim($tahun_dis['result']) != '') {
                if ($tahun_dis['result'] <= '1080') {
                    $arrupdate['tgl_izin_exp'] = $tahun_dis['tgl_exp'];
                }else{
                    $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
                }
            }else {
                $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
            }
        }elseif ($a == '5') {
            // $tahun_dis = $this->get_tahun($a, '119', $b);
            $tahun_rekom = $this->get_tahun($a, '28', $b);
            // if ($tahun_dis['result'] == '0' && $tahun_rekom['result'] != '0') {
            //     if ($tahun_rekom['result'] >= '1080') {
            //         $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");        
            //     }else{
            //         $arrupdate['tgl_izin_exp'] = $tahun_rekom['tgl_exp'];
            //     }
            // }elseif ($tahun_rekom['result'] == '0' && $tahun_dis['result'] != '0') {
            //     if ($tahun_dis['result'] >= '1080') {
            //         $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
            //     }else{
            //         $arrupdate['tgl_izin_exp'] = $tahun_dis['tgl_exp'];
            //     }
            // }elseif ($tahun_rekom['result'] != '0' && $tahun_dis['result'] != '0') {
            //     if ($tahun_dis['result'] >= $tahun_rekom['result']) {
            //         if ($tahun_rekom['result'] >= '1080') {
            //             $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");        
            //         }else{
            //             $arrupdate['tgl_izin_exp'] = $tahun_rekom['tgl_exp'];
            //         }
            //     }elseif ($tahun_dis['result'] <= $tahun_rekom['result']) {
            //         if ($tahun_dis['result'] >= '1080') {
            //             $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
            //         }else{
            //             $arrupdate['tgl_izin_exp'] = $tahun_dis['tgl_exp'];
            //         }
            //     }
            // }else {
            //     $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
            // }
            // if ($tahun_rekom <= '730') {
            //     $arrupdate['tgl_izin_exp'] = $tahun_rekom['tgl_exp'];    
            // }else{
            //     $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
            // }
            if ($tahun_rekom['result'] != 0) {
            	if ($tahun_rekom['result'] >= '1080') {
	                $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");        
	            }else{
	                $arrupdate['tgl_izin_exp'] = $tahun_rekom['tgl_exp'];
	            }
            }else{
            	$arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
            }

            if ($tipe_permohonan == '02') {
                if ($a == 4) {
                    $arrupdate['tgl_izin_exp'] = $perubahan_dis['tgl_exp'];
                }elseif ($a == 5) {
                    $arrupdate['tgl_izin_exp'] = $perubahan_subdis['tgl_exp'];
                }
            }
        }elseif ($a == '3') {
                $tahun_pen = $this->get_tahun($a, '44', $b);
                if ($tahun_pen['result'] <= '1080') {
                    $arrupdate['tgl_izin_exp'] = $tahun_pen['tgl_exp'];
                }else{
                    $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
                }
            if ($tipe_permohonan == '02') {
                $arrupdate['tgl_izin_exp'] = $perubahan_it['tgl_exp'];
            }
        }else{
            // if ($a == 6) {
            //     $thn_skpa = $this->get_tahun($a, '30', $b);
            //     $arrupdate['tgl_izin_exp'] = $perubahan_skpa['tgl_exp'];
            //     if ($thn_skpa['result'] <= '1080' && $thn_skpa['tgl_exp'] != '') {
            //         $arrupdate['tgl_izin_exp'] = $thn_skpa['tgl_exp'];
            //     }else{
            //         $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
            //     }
            // }elseif ($a == 13) {
            //     $thn_skpla = $this->get_tahun($a, '33', $b);
            //     if ($thn_skpla['result'] <= '1080' && $thn_skpla['tgl_exp'] != '') {
            //         $arrupdate['tgl_izin_exp'] = $thn_skpla['tgl_exp'];
            //     }else{
            //         $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
            //     }
            // }else{
            //     $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
            // }
            $arrupdate['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
        }
        return $arrupdate;
    }

}

?>