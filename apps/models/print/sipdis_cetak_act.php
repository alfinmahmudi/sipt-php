<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sipdis_cetak_act extends CI_Model {

    function set_prints($a, $b, $c = "") {
        //if($this->newsession->userdata('_LOGGED')){
        /*
          @ $a = Kode Izin
          @ $b = Permohonan ID
         */
        $arrdata = array();
        $query = "SELECT a.tgl_terima, a.tgl_aju, a.id, a.kd_izin, a.no_aju, a.no_izin, a.tgl_izin AS tgl_dok, a.tgl_izin_exp AS tgl_exp, 
                dbo.dateIndo(a.tgl_izin) AS tgl_izin, dbo.dateIndo(a.tgl_izin_exp) AS tgl_izin_exp, a.trader_id, a.tipe_permohonan, 
                c.uraian AS permohonan,dbo.npwp(a.npwp) AS npwp, d.uraian AS tipe_perusahaan, a.almt_perusahaan, 
                dbo.get_region(2, a.kdprop) AS prop_perusahaan, dbo.get_region(4, a.kdkab) AS kab_perusahaan, dbo.get_region(7, a.kdkec) AS kec_perusahaan, 
                dbo.get_region(10, a.kdkel) AS kel_perusahaan, a.telp, a.fax, a.pemasaran, a.pemasaran_prop, a.pemasaran_kab,
                l.uraian AS identitas_pj, a.nama_pj, a.noidentitas_pj, e.uraian as 'jenis_izin', a.bidang_usaha,
                a.jabatan_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, a.alamat_pj, dbo.get_region(2, a.kdprop_pj) prop_pj, 
                dbo.get_region(4, a.kdkab_pj) AS kab_pj, dbo.get_region(7, a.kdkec_pj) kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj, a.telp_pj, 
                a.fax_pj, b.nama_izin, b.disposisi, b.direktorat_id, a.nama_ttd, a.jabatan_ttd, a.nip_ttd, a.status,
                CASE WHEN a.tipe_perusahaan = '05' OR a.tipe_perusahaan = '07' OR a.tipe_perusahaan = '06' 
                THEN a.nm_perusahaan 
                ELSE p.uraian+'. '+a.nm_perusahaan 
                END AS 'nm_perusahaan'
                FROM t_bapok a
                LEFT JOIN m_izin b ON b.id = a.kd_izin
                LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN'
                LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
                LEFT JOIN m_reff l ON l.kode = a.identitas_pj AND l.jenis = 'JENIS_IDENTITAS'
                LEFT JOIN m_reff p ON p.kode = a.tipe_perusahaan AND p.jenis = 'KDTIPE_PERUSAHAAN'
                LEFT JOIN m_reff e ON e.kode = a.jenis_izin AND e.jenis = 'JENIS_DISTRIBUTOR_BAPOK'
                WHERE a.id = '" . hashids_decrypt($b, _HASHIDS_, 9) . "'";

        $arrdata['row'] = $this->db->query($query)->row_array(); //print_r($arrdata['row']);die();
        $arrdata['catatan'] = $_POST['catatan'];
        //print_r(hashids_encrypt((int)$arrdata['row']['direktorat_id'], _HASHIDS_, 9)."-".hashids_encrypt(2, _HASHIDS_, 9));die();
        $arrdata['link'] = url_sipt . 'verifikasi/' . hashids_encrypt((int) $arrdata['row']['direktorat_id'], _HASHIDS_, 9) . '/' . hashids_encrypt($a, _HASHIDS_, 9) . '/' . $b;
        $kb = explode(",", $arrdata['row']['bidang_usaha']);
        foreach ($kb as $kbl) {
            if ($kbl != '') {
                $arrkb[] = $kbl;
            }
        }
        $arrdata['row']['bidang_usaha'] = implode(",", $arrkb);
        $pemasaran = explode(",", $arrdata['row']['pemasaran']); //print_r($pemasaran);die();
        foreach ($pemasaran as $pem) {
            if ($pem == '01') {
                $arrdata['pemasaran'][] = "Seluruh Wilayah Indonesia";
            }
            if ($pem == '02') {
                $arrprop = explode(",", $arrdata['row']['pemasaran_prop']);
                foreach ($arrprop as $prop) {
                    if (trim($prop) != '') {
                       $get_prop = "SELECT dbo.get_region(2, '".trim($prop)."') AS prop ";
                       $rprop = $this->db->query($get_prop)->row_array();
                       $arrdata['pemasaran'][] = $rprop['prop'];
                        
                    }
                }
            }
            if ($pem == '03') {
                $arrkab = explode(",", $arrdata['row']['pemasaran_kab']);
                foreach ($arrkab as $kab) {
                    if (trim($kab) != '') {
                        $get_kab = "SELECT dbo.get_region(4, '" . $kab . "') AS kab ";
                        $rkab = $this->db->query($get_kab)->row_array();
                        $arrdata['pemasaran'][] = $rkab['kab'];
                    }
                }
            }
        }
        $arrdata['pemasaran'] = implode(", ", $arrdata['pemasaran']);

        $arrdata['detil'] = $this->get_detil(hashids_decrypt($b, _HASHIDS_, 9));

        $arrdata['tembusan'] = $this->get_tembusan($a, $b);
        $arrdata['ttd'] = $this->suggest_ttd($a);

        $no_aju = str_replace("/", "", $arrdata['row']['no_aju']);
        if ($c != "") {
            $kode = '142';
            $folder = 'DOKSIPDIS';
            $arrdata['namafile'] = "DOCUMENT-" . $no_aju . '.pdf';
            $arrdata['dir'] = './upL04d5/document/' . $folder . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            $sqlInsertPendukung = "INSERT INTO t_upload (trader_id, tipe_dok, nomor, tgl_dok, tgl_exp, penerbit_dok, nama_file, ori_file, folder, ekstensi, ukuran, created_user, created) 
					VALUES('" . $arrdata['row']['trader_id'] . "','" . $kode . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['row']['tgl_dok'] . "','" . $arrdata['row']['tgl_exp'] . "','Direktorat Jenderal Perdagangan Dalam Negeri','" . $arrdata['namafile'] . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['dir'] . "','.pdf','0','system',GETDATE())";
            $insert = $this->db->query($sqlInsertPendukung);
        }
        return $arrdata;
        //}
    }

    function get_detil($permohonan_id) {
        $urdet = array();
        $fl_01 = 0;
        $fl_02 = 0;
        $fl_03 = 0;
        $query = "SELECT jns_bapok, jns_brg FROM t_bapok_brg WHERE permohonan_id = '" . $permohonan_id . "'";
        $arrdet = $this->db->query($query)->result_array();
        foreach ($arrdet as $detil) {
            $get_bapok2 = "SELECT uraian FROM m_reff WHERE jenis = 'JENIS_BAPOK' AND kode = '".$detil['jns_bapok']."' ";
            $res = $this->db->query($get_bapok2)->row_array(); 
            $result['bapok'][] = $res['uraian'];
            $get_bapok2 = "SELECT a.uraian FROM m_jenis_bapok a WHERE a.id = '" . $detil['jns_brg'] . "' ";
            $res = $this->db->query($get_bapok2)->row_array();
            $result['jenis'][] = $res['uraian'];
        }
        // print_r(array_unique($result['bapok']));die();
        $result['bapok'] = implode(", ", array_unique($result['bapok']));
        $result['jenis'] = implode(", ", $result['jenis']);

        return $result;
    }

    function get_tembusan($a, $b) {
        $tembusan = array();
        $query = "SELECT urutan, keterangan FROM m_tembusan WHERE kd_izin = " . $a . " AND permohonan_id =" . hashids_decrypt($b, _HASHIDS_, 9);
        $data = $this->main->get_result($query);
        if ($data) {
            $tembusan = $query->result_array();
        }

        return $tembusan;
    }

    function suggest_ttd($a) {
        $sql = "SELECT nama, jabatan FROM m_ttd a WHERE a.kd_izin = '" . $a . "' AND a.status = '1'";
        $data = $this->main->get_result($sql);
        if ($data) {
            $ttd = $sql->row_array();
        }
        return $ttd;
    }

    function get_pemasaran($a, $b, $c) {
        $ex = explode(",", $c); //print_r($ex);die();
        foreach ($ex as $id_ex) {
            $jml = strlen($id_ex);
            if ($jml != '0' && $jml == '4') {
                $sqlkab = "SELECT nama FROM m_kab WHERE id = ('" . $id_ex . "')";
                $datakab = $this->db->query($sqlkab)->row_array();
                $arrkab[] = $datakab['nama'];
            } elseif ($jml != '0' && $jml == '2') {
                $sqlkab = "SELECT nama FROM m_prop WHERE id = ('" . $id_ex . "')";
                $datakab = $this->db->query($sqlkab)->row_array();
                $arrkab[] = "Propinsi " . $datakab['nama'];
            } elseif ($jml != '0' && $jml == '1') {
                $sqlkab = "SELECT a.kode, a.uraian FROM m_reff a where a.jenis = 'PEMASARAN_ITMB' AND kode = '" . $id_ex . "'";
                $datakab = $this->db->query($sqlkab)->row_array();
                $arrkab[] = $datakab['uraian'];
            }
        }
        return $arrkab;
    }

    function get_pendukung($a, $b, $c) {
        $query = "SELECT b.nomor, dbo.dateIndo(b.tgl_dok) AS tgl_dok, b.penerbit_dok, SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file as url, b.ekstensi
				FROM t_upload_syarat a 
				LEFT JOIN t_upload b ON b.id = a.upload_id
				WHERE a.permohonan_id = " . hashids_decrypt($b, _HASHIDS_, 9) . " AND a.izin_id = " . $a . " AND a.dok_id =" . $c;
        //print_r($query);die();
        $data = $this->main->get_result($query);
        if ($data) {
            $arrsiupmb = array('115', '28', '33', '30', '54', '119');
            if (in_array($c, $arrsiupmb)) {
                $arrPendukung = $query->result_array();
                $banyakPendukung = count($arrPendukung);
                for ($p = 0; $p < $banyakPendukung; $p++) {
                    $pendukung .= $arrPendukung[$p]['penerbit_dok'];
                    $pendukung .= " Nomor : " . $arrPendukung[$p]['nomor'] . " tanggal " . $arrPendukung[$p]['tgl_dok'];

                    if ($banyakPendukung > 2) {
                        if ($p != ($banyakPendukung - 1)) {
                            $pendukung .= "., ";
                        } else {
                            $pendukung .= " ";
                        }
                    } else if ($banyakPendukung == 2) {
                        if ($p != ($banyakPendukung - 1)) {
                            $pendukung .= "., ";
                        } else {
                            $pendukung .= " ";
                        }
                    } else {
                        $pendukung .= " ";
                    }
                }
            } else {
                foreach ($query->result_array() as $row) {
                    $pendukung = $row;
                }
            }
        }
        return $pendukung;
    }

}

?>