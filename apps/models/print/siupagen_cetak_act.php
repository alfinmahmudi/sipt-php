<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Siupagen_cetak_act extends CI_Model {

    function set_prints($a, $b, $c = "") {
        // if($this->newsession->userdata('_LOGGED')){
        /*
          @ $a = Kode Izin
          @ $b = Permohonan ID
         */
        $arrdata = array();
        $query = "SELECT a.id,a.no_izin, dbo.dateIndo(a.tgl_izin_exp) AS exp,  dbo.dateIndo(a.tgl_izin) AS izin , b.direktorat_id, a.kd_izin, a.no_aju, a.trader_id, a.tipe_permohonan, c.uraian AS permohonan, g.uraian as produksi, h.uraian as jenis_agen,a.npwp, d.uraian AS tipe_perusahaan, p.uraian + '. '+a.nm_perusahaan as nm_perusahaan,  
					a.almt_perusahaan, dbo.get_region(2, a.kdprop) AS prop_perusahaan, dbo.get_region(4, a.kdkab) AS kab_perusahaan, dbo.get_region(7, a.kdkec) AS kec_perusahaan, dbo.get_region(10, a.kdkel) AS kel_perusahaan,
					a.kdpos, a.telp, a.fax, i.uraian as identitas_pj, a.nama_pj, a.noidentitas_pj, z.uraian AS jabatan_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, a.alamat_pj, 
					a.almt_perusahaan, dbo.get_region(2, a.kdprop_pj) AS prop_pj, dbo.get_region(4, a.kdkab_pj) AS kab_pj, dbo.get_region(7, a.kdkec_pj) AS kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj,
					a.telp_pj, a.fax_pj, 
					a.almt_perusahaan_prod, a.tgl_pendirian_prod, 
					dbo.get_region(2, a.kdprop_prod) AS prop_prod, dbo.get_region(4, a.kdkab_prod) AS kab_prod, dbo.get_region(7, a.kdkec_prod) AS kec_prod, dbo.get_region(10, a.kdkel_prod) AS kel_prod,
					a.telp_prod, a.fax_prod,j.uraian AS produk,a.meerk, a.almt_perusahaan_supl, a.tgl_pendirian_supl, a.telp_supl, a.fax_supl,
					dbo.get_region(2, a.kdprop_supl) AS prop_supl, dbo.get_region(4, a.kdkab_supl) AS kab_supl, dbo.get_region(7, a.kdkec_supl) AS kec_supl, dbo.get_region(10, a.kdkel_supl) AS kel_supl,
					a.bidang_usaha, a.pegawai_lokal, a.pegawai_asing, a.status, a.pemasaran,b.nama_izin, b.disposisi, a.produk AS 'jns_produk', a.nama_ttd, a.jabatan_ttd, a.status_dok,
					k.jenis,k.merk,k.hs, b.direktorat_id, a.produksi as jns_produksi, a.tgl_izin_exp as tgl_end, a.tgl_izin as tgl_set,
					CASE WHEN LTRIM(RTRIM(a.kdprop_pj)) = '' 
					THEN LTRIM(RTRIM(CONVERT(VARCHAR(MAX), a.alamat_pj))) 
					ELSE CONCAT(LTRIM(RTRIM(CONVERT(VARCHAR(MAX), a.alamat_pj))), ', Kel. ', dbo.get_region(10, a.kdkel_pj), ', Kec. ', dbo.get_region(7, a.kdkec_pj), ', ', dbo.get_region(4, a.kdkab_pj), ', Prop. ', dbo.get_region(2, a.kdprop_pj)) END AS alamat_jp,
					CASE WHEN LTRIM(RTRIM(a.kdprop)) = '' 
					THEN LTRIM(RTRIM(CONVERT(VARCHAR(MAX), a.almt_perusahaan)))
					ELSE CONCAT(LTRIM(RTRIM(CONVERT(VARCHAR(MAX), a.almt_perusahaan))), ', Kel. ', dbo.get_region(10, a.kdkel), ', Kec. ', dbo.get_region(7, a.kdkec), ', ', dbo.get_region(4, a.kdkab), ', Prop. ', dbo.get_region(2, a.kdprop)) END AS alamat,
                    CASE WHEN LTRIM(RTRIM(a.kdprop_prod)) = '' 
                    THEN LTRIM(RTRIM(CONVERT(VARCHAR(MAX), a.almt_perusahaan_prod)))
                    ELSE CONCAT(LTRIM(RTRIM(CONVERT(VARCHAR(MAX), a.almt_perusahaan_prod))), ', Kel. ', dbo.get_region(10, a.kdkel_prod), ', Kec. ', dbo.get_region(7, a.kdkec_prod), ', ', dbo.get_region(4, a.kdkab_prod), ', Prop. ', dbo.get_region(2, a.kdprop_prod)) END AS alamat_prod,
                    CASE WHEN a.tipe_perusahaan = '05' THEN a.nm_perusahaan
                    ELSE p.uraian+'. '+a.nm_perusahaan
                    END as 'nm_perusahaan',
                    CASE WHEN a.tipe_perusahaan_prod = '06' or a.tipe_perusahaan_prod = '07' or a.tipe_perusahaan_prod = null THEN a.nm_perusahaan_prod
                    ELSE pa.uraian+'. '+a.nm_perusahaan_prod
                    END as 'nm_perusahaan_prod',
                    CASE WHEN a.tipe_perusahaan_supl = '06' or a.tipe_perusahaan_supl = '07' or a.tipe_perusahaan_prod = null   THEN a.nm_perusahaan_supl
                    ELSE pb.uraian+'. '+a.nm_perusahaan_supl
                    END as 'nm_perusahaan_supl'
					FROM t_siup_agen a 
					LEFT JOIN m_izin b ON b.id = a.kd_izin 
					LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
					LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff e ON e.kode = a.tipe_perusahaan_prod AND e.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff f ON f.kode = a.tipe_perusahaan_supl AND f.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff g ON g.kode = a.produksi AND g.jenis = 'PRODUKSI_KEAGENAN'
					LEFT JOIN m_reff h ON h.kode = a.jenis_agen AND h.jenis = 'JENIS_KEAGENAN'
					LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
					LEFT JOIN m_reff j ON j.kode = a.produk AND j.jenis = 'PRODUK'
					LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
                    LEFT JOIN m_reff p ON p.kode = a.tipe_perusahaan AND p.jenis = 'KDTIPE_PERUSAHAAN'
                    LEFT JOIN m_reff pa ON pa.kode = a.tipe_perusahaan_prod AND pa.jenis = 'KDTIPE_PERUSAHAAN'
                    LEFT JOIN m_reff pb ON pb.kode = a.tipe_perusahaan_supl AND pb.jenis = 'KDTIPE_PERUSAHAAN'
					LEFT JOIN t_siup_agen_detil k ON k.permohonan_id = a.id
				    WHERE a.id = '" . hashids_decrypt($b, _HASHIDS_, 9) . "'";
//        print_r($query);die();
        $arrdata['row'] = $this->db->query($query)->row_array(); //print_r($arrdata['row']);die();
        $arrdata['ttd'] = $this->suggest_ttd($a); //print_r($arrdata['ttd']);die();
        $arrdata['temp_terbit'] = $this->suggest_terbit($a, hashids_decrypt($b, _HASHIDS_, 9), $arrdata['row']['jns_produksi'], $arrdata['row']['tipe_permohonan']); //print_r($arrdata['temp_terbit']);die();
        $arrdata['no_penjelasan'] = $this->main->set_nomor("99", "01");
        $arrdata['link'] = url_sipt . 'verifikasi/' . hashids_encrypt((int) $arrdata['row']['direktorat_id'], _HASHIDS_, 9) . '/' . hashids_encrypt($a, _HASHIDS_, 9) . '/' . $b;
        $arrdata['tembusan'] = $this->get_tembusan($a, $b);
        $arrdata['pemasaran'] = $this->get_pemasaran($a, $b, $arrdata['row']['pemasaran']);
        $arrdata['urlFoto'] = $this->get_pendukung($a, $b, '16');
        $arrdata['suratDistributor'] = $this->get_pendukung($a, $b, '23');
        $arrdata['suratSubDistributor'] = $this->get_pendukung($a, $b, '28');
        $arrdata['suratITMB'] = $this->get_pendukung($a, $b, '44');
        $arrdata['suratSKPLA'] = $this->get_pendukung($a, $b, '33');
        $arrdata['suratSKPA'] = $this->get_pendukung($a, $b, '30');
        $arrdata['catatan'] = ($_POST['catatan']) ? $_POST['catatan'] : '';
        $no_aju = str_replace("/", "", $arrdata['row']['no_aju']);
        if ($c != "") {
            $kode = '85';
            $folder = 'DOKAGEN';
            $arrdata['namafile'] = "DOCUMENT-" . $no_aju . '.pdf';
            $arrdata['dir'] = './upL04d5/document/' . $folder . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            $sqlInsertPendukung = "INSERT INTO t_upload (trader_id, tipe_dok, nomor, tgl_dok, tgl_exp, penerbit_dok, nama_file, ori_file, folder, ekstensi, ukuran, created_user, created) 
				VALUES('" . $arrdata['row']['trader_id'] . "','" . $kode . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['row']['tgl_set'] . "','" . $arrdata['row']['tgl_end'] . "','Direktorat Jenderal Perdagangan Dalam Negeri','" . $arrdata['namafile'] . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['dir'] . "','.pdf','0','system',GETDATE())";
            $insert = $this->db->query($sqlInsertPendukung);
        }
        //print_r($arrdata);die();
        return $arrdata;

        //}
    }

    function get_tembusan($a, $b) {
        $query = "SELECT urutan, keterangan FROM m_tembusan WHERE kd_izin = " . $a . " AND permohonan_id =" . hashids_decrypt($b, _HASHIDS_, 9);
        $tembusan = $this->db->query($query)->result_array();
        return $tembusan;
    }

    function suggest_ttd($a) {
        $sql = "SELECT nama, jabatan FROM m_ttd a WHERE a.kd_izin = '" . $a . "' AND a.status = '1'";
        $data = $this->main->get_result($sql);
        if ($data) {
            $ttd = $sql->row_array();
        }
        return $ttd;
    }

    function suggest_terbit($a, $b, $c, $d) {
    	// $a = kd_izin, $b = id_permohonan
        $arrdata = array();
        $get_prod = "SELECT produksi, produk, jenis_agen FROM t_siup_agen WHERE id = '".$b."'";
        $prod = $this->db->query($get_prod)->row_array();
        // cek status agen permohonan nya, jika sub-agen (03) atau sub-distributor (06) maka siup-agen (agen nya atau distributor nya) jadi parameter penentuan tgl akhir.
        $add = '';
        if ($prod['jenis_agen'] == 03 || $prod['jenis_agen'] == 06) {
        	$add = ", '140'";
        }
        $no = $this->main->get_uraian("SELECT nomor from m_nomor where kd_izin = '" . $a . "'", "nomor");
        //generate suggestion number
        if ($prod['produksi'] == '01') {
            if ($prod['produk'] == '02') {
                $prod = "STP-JS/SIPT";
                $jasa = 1;
            }else{
                $prod = "STP-DN/SIPT";
            }
        }  else {
            if ($prod['produk'] == '02') {
                $prod = "STP-JS/SIPT";
                $jasa = 1;
            }else{
                $prod = "STP-LN/SIPT";
            }
        }
        
        $tgl = getdate();
        $nomor_agen = "--/" . $prod . "/" . $tgl['mon'] . "/" . $tgl['year'] . "";
        $arrdata['no_izin'] = $nomor_agen; //print_r($nomor_agen);die();
        // cari dulu yang paling pendek masa berlaku nya
        $query = "	SELECT MIN(b.tgl_exp) as tgl_exp, DATEDIFF(day,CONVERT(DATE,getdate(),120),CONVERT(DATE,MIN(b.tgl_exp),120)) as 'result'
                    FROM t_upload_syarat a 
                    LEFT JOIN t_upload b ON b.id = a.upload_id
                    where a.izin_id = '" . $a . "' and a.dok_id IN('40', '29', '36'".$add.")  and a.permohonan_id = '" . $b . "' and b.tgl_exp > getdate()"; //print_r($query);die();

       // dokumen id referensi :
       // 36 = Surat Perjanjian yang sudah dilegalisasi oleh Notaris (untuk Prinsipal dalam negeri) dan Notary Public & Atase Perdagangan/Kantor Perwakilan RI yang ada di negara Prinsipal (untuk Prinsipal luar negeri), (Surat Perjanjian / Penunjukan dari Agen / Distributor untuk Sub Agen /Distributor yang sudah dilegalisasi oleh Notaris)

       // 29 = Surat Perjanjian atau Penunjukan dari Prinsipal Produsen kepada Prinsipal Supplier apabila surat perjanjian bukan dari Prinsipal Produsen (supplier, subsidiary, atau perwakilan)

       // 40 = Surat Konfirmasi mengenai masa berlaku perjanjian atau perubahan

       // 39 = izin teknis  -- pertanggal 14 sept 2017 tidak mengacu lagi

       // 140 = SIUP-AGEN dengan status permohonan agen atau distributor   
        
        
        $tahun = $this->db->query($query)->row_array(); //print_r($tahun);die(); //(dapet tgl paling akhir)
        // cek dokumen mana yang paling akhir (biasanya query nya hasilin lebih dari 1 makanya di foreach)
        $sql = "SELECT a.dok_id 
                FROM t_upload_syarat a
                LEFT JOIN t_upload b ON b.id = a.upload_id
                WHERE b.tgl_exp = '" . $tahun['tgl_exp'] . "' AND a.izin_id = '" . $a . "' AND a.permohonan_id = '" . $b . "'"; //print_r($sql);die();
        $dok_id = $this->db->query($sql)->result_array(); //print_r($dok_id);die();
        foreach ($dok_id as $key) { 
            if ($key['dok_id'] == 36) {
            	// kalo result nya id nya 36, cek konfirmasi nya (Karena perjanjian bisa di perbaharui oleh konfirmasi), jadi kalo tanggal awal dokumen perjanjiannya lebih baru dari tanggal awal konfirmasi maka pake yang perjanjian untuk masa berlaku nya dan sebaliknya.
                $query = "SELECT MAX (b.tgl_dok) as tgl_dok, DATEDIFF(day,CONVERT(DATE,getdate(),120),CONVERT(DATE,MIN(b.tgl_exp),120)) as 'result', b.tgl_exp
						FROM t_upload_syarat a 
						LEFT JOIN t_upload b ON b.id = a.upload_id
						where a.izin_id = '" . $a . "' and a.dok_id IN('40', '36')  and a.permohonan_id = '" . $b . "' and b.tgl_exp > getdate() and b.tgl_dok is not NULL
						GROUP BY tgl_dok, b.tgl_exp
						ORDER BY tgl_dok DESC";
                //print_r($query);die();
                $konfirmasi = $this->db->query($query)->row_array(); //print_r($konfirmasi);die();
                // untuk cek masa berlaku lebih dari 2 tahun
                if (trim($konfirmasi['result']) == '') {
                    if ($tahun['result'] <= 730 and trim($tahun['result']) != '') {
                        $arrdata['tgl_izin_exp'] = date_indo($tahun['tgl_exp']);
                        $temp = $tahun['result'];
                    } else {
                        $arrdata['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
                        $temp = $tahun['result'];
                    }
                } else {
                    if ($konfirmasi['result'] <= 730 and trim($konfirmasi['result']) != '') {
                        $arrdata['tgl_izin_exp'] = date_indo($konfirmasi['tgl_exp']);
                        $temp = $konfirmasi['result'];
                    } else {
                        $arrdata['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
                        $temp = $konfirmasi['result'];
                    }
                }
                $tahun = $konfirmasi;
                $arrdata['tgl_izin'] = date_indo(date('Y-m-d'));
                break;
            } elseif ($key['dok_id'] == 40 || $key['dok_id'] == 29) {
                if ($tahun['result'] <= 730 and trim($tahun['result']) != '') {
                    $arrdata['tgl_izin_exp'] = date_indo($tahun['tgl_exp']);
                    $temp = $tahun['result'];
                } else {
                    $arrdata['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
                    $temp = $tahun['result'];
                }
                $arrdata['tgl_izin'] = date_indo(date('Y-m-d'));
                break;
            }else{
            	if ($tahun['result'] <= 730 and trim($tahun['result']) != '') {
                    $arrdata['tgl_izin_exp'] = date_indo($tahun['tgl_exp']);
                    $temp = $tahun['result'];
                } else {
                    $arrdata['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
                    $temp = $tahun['result'];
                }
                $arrdata['tgl_izin'] = date_indo(date('Y-m-d'));
            }
        }
        // // cek lagi result sama izin teknis
        // $cek = "SELECT MIN(b.tgl_exp) as tgl_exp, DATEDIFF(day,CONVERT(DATE,getdate(),120),CONVERT(DATE,MIN(b.tgl_exp),120)) as 'result'
        //         FROM t_upload_syarat a 
        //         LEFT JOIN t_upload b ON b.id = a.upload_id
        //         where a.izin_id = '" . $a . "' and a.dok_id IN('39', '29')  and a.permohonan_id = '" . $b . "' and b.tgl_exp > getdate()";
        // $teknis = $this->db->query($cek)->row_array(); //print_r($teknis);die();
        // if ($teknis['result'] <= 730 && trim($teknis['result']) != '') {
        //     if ($teknis['result'] <= $temp) {
        //         $arrdata['tgl_izin_exp'] = date_indo($teknis['tgl_exp']);
        //         $temp = $teknis['result'];
        //     }else{
        //         $arrdata['tgl_izin_exp'] = date_indo($tahun['tgl_exp']);
        //         $temp = $teknis['result'];
        //     }
        // }

        
        // if produk jasa, badingin lagi sama stp barang nya
        if ($jasa == '1') {
            $cek = "SELECT MIN(b.tgl_exp) as tgl_exp, DATEDIFF(day,CONVERT(DATE,getdate(),120),CONVERT(DATE,MIN(b.tgl_exp),120)) as 'result'
                FROM t_upload_syarat a 
                LEFT JOIN t_upload b ON b.id = a.upload_id
                where a.izin_id = '" . $a . "' and a.dok_id IN('135')  and a.permohonan_id = '" . $b . "' and b.tgl_exp > getdate()";
            $stp_barang = $this->db->query($cek)->row_array(); //print_r($stp_barang);die();
            if ($stp_barang['result'] <= 730 && trim($stp_barang['result']) != '') {
                if ($stp_barang['result'] <= $temp) {
                    $arrdata['tgl_izin_exp'] = date_indo($stp_barang['tgl_exp']);
                }else{
                    $arrdata['tgl_izin_exp'] = date_indo($tahun['tgl_exp']);
                }
            }   
        }
        // if kalo dia perubahan PASTI! pake yang lama
        if ($d == '02') {
        	$cek_perubahan = "SELECT MIN(b.tgl_exp) as tgl_exp, DATEDIFF(day,CONVERT(DATE,getdate(),120),CONVERT(DATE,MIN(b.tgl_exp),120)) as 'result'
                FROM t_upload_syarat a 
                LEFT JOIN t_upload b ON b.id = a.upload_id
                where a.izin_id = '" . $a . "' and a.dok_id IN('85')  and a.permohonan_id = '" . $b . "' and b.tgl_exp > getdate()";
        	$izin_lama = $this->db->query($cek_perubahan)->row_array(); //print_r($teknis);die();
        	if ($izin_lama['result'] <= 730 && trim($izin_lama['result']) != '') {
	            $arrdata['tgl_izin_exp'] = date_indo($izin_lama['tgl_exp']);
        	}else{
        		$arrdata['tgl_izin_exp'] = date_indo($this->main->set_expired($a), "cetakan");
        	}
        }
        return $arrdata;
    }

    function get_pemasaran($a, $b, $c) {
        if ($a == '4') {
            $idprop = str_replace(";", "','", $c);
            $sqlprop = "SELECT 'Prop. ' + nama AS nama FROM m_prop WHERE id IN ('" . $idprop . "')";
            $dataprop = $this->main->get_result($sqlprop);
            if ($dataprop) {
                foreach ($sqlprop->result_array() as $rprop) {
                    $arrprop[] = $rprop['nama'];
                }
                $pemasaran = join("; ", $arrprop);
            }
        } elseif (($a != '4') || ($a != '3')) {
            $idkab = str_replace(";", "','", $c);
            $sqlkab = "SELECT nama FROM m_kab WHERE id IN ('" . $idkab . "')";
            $datakab = $this->main->get_result($sqlkab);
            if ($datakab) {
                foreach ($sqlkab->result_array() as $rkab) {
                    $arrkab[] = $rkab['nama'];
                }
                $pemasaran = join("; ", $arrkab);
            }
        }
        return $pemasaran;
    }

    function get_pendukung($a, $b, $c) {
        $query = "SELECT b.nomor, dbo.dateIndo(b.tgl_dok) AS tgl_dok, b.penerbit_dok, SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file as url
				FROM t_upload_syarat a 
				LEFT JOIN t_upload b ON b.id = a.upload_id
				WHERE a.permohonan_id = " . hashids_decrypt($b, _HASHIDS_, 9) . " AND a.izin_id = " . $a . " AND a.dok_id =" . $c;
        $data = $this->main->get_result($query);
        if ($data) {
            $arrsiupmb = array('23', '28', '33', '30');
            if (in_array($c, $arrsiupmb)) {
                $arrPendukung = $query->result_array();
                $banyakPendukung = count($arrPendukung);
                for ($p = 0; $p < $banyakPendukung; $p++) {
                    $pendukung .= $arrPendukung[$p]['penerbit_dok'];
                    $pendukung .= " Nomor :" . $arrPendukung[$p]['nomor'] . " tanggal " . $arrPendukung[$p]['tgl_dok'];

                    if ($banyakPendukung > 2) {
                        if ($p != ($banyakPendukung - 1)) {
                            $pendukung .= "., ";
                        } else {
                            $pendukung .= " ";
                        }
                    } else if ($banyakPendukung == 2) {
                        if ($p != ($banyakPendukung - 1)) {
                            $pendukung .= "., ";
                        } else {
                            $pendukung .= " ";
                        }
                    } else {
                        $pendukung .= " ";
                    }
                }
            } else {
                foreach ($query->result_array() as $row) {
                    $pendukung = $row;
                }
            }
        }
        return $pendukung;
    }

}

?>