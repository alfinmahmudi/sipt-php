<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Pameranp_cetak_act extends CI_Model{
	
	function set_prints($a, $b, $c=""){
		//if($this->newsession->userdata('_LOGGED')){
			/*
			@ $a = Kode Izin
			@ $b = Permohonan ID
			*/
			$arrdata = array();
			$query = "SELECT a.id, a.no_izin, dbo.dateIndo(a.tgl_izin_exp) AS exp, dbo.dateIndo(a.tgl_aju) AS tg_aju, dbo.dateIndo(a.tgl_izin) AS izin, b.direktorat_id, a.kd_izin, a.no_aju, a.trader_id,almt_perusahaan, a.tipe_permohonan, c.uraian AS permohonan, a.npwp, d.uraian AS tipe_perusahaan, p.uraian+'. '+a.nm_perusahaan as nm_perusahaan , a.kdprop,a.kdkab,a.kdkec,a.kdkel,a.kdpos,a.fax,a.telp, 
				      dbo.get_region(2, a.kdprop) AS prop_perusahaan,dbo.get_region(4, a.kdkab) AS kab_perusahaan, dbo.get_region(7, a.kdkec) AS kec_perusahaan, dbo.get_region(10, a.kdkel) AS kel_perusahaan,
					c.uraian as warganegara,a.noidentitas_pj,k.uraian AS identitas_pj,a.nama_pj,a.jabatan_pj,a.tmpt_lahir_pj,a.tgl_lahir_pj,a.alamat_pj,
					 dbo.get_region(2, a.kdprop_pj) AS prop_pj,dbo.get_region(4, a.kdkab_pj) AS kab_pj, dbo.get_region(7, a.kdkec_pj) AS kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj,a.telp_pj,fax_pj, a.status_dok,
					b.disposisi, a.status, dbo.dateIndo(a.awal_pameran) AS tgl_awal, dbo.dateIndo(a.akhir_pameran) AS tgl_akhir,a.tema_pameran,
					dbo.get_region(2, a.kdprop_pameran) AS prop_pameran,a.lokasi_pameran,dbo.dateIndo(a.tgl_terima) AS tgl_terima,dbo.dateIndo(a.tgl_kirim) AS tgl_kirim, a.jabatan_ttd, a.nama_ttd, a.tgl_izin as tgl_set, a.tgl_izin_exp as tgl_abis
					FROM t_pameran a 
					LEFT JOIN m_izin b ON b.id = a.kd_izin 
					LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
					LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff k ON k.kode = a.identitas_pj AND k.jenis = 'JENIS_IDENTITAS'
                                         LEFT JOIN m_reff p ON p.kode = a.tipe_perusahaan AND p.jenis = 'KDTIPE_PERUSAHAAN'
					WHERE a.id = '".hashids_decrypt($b, _HASHIDS_, 9)."'";
			//print_r($query);die();
			$sql = "SELECT nama, organizer, dbo.dateIndo(mulai) AS mulai, dbo.dateIndo(berakhir) AS berakhir FROM t_pameran_event where permohonan_id='".hashids_decrypt($b, _HASHIDS_, 9)."'";
			//print_r(hashids_decrypt($b, _HASHIDS_, 9));die();
			$main = $this->main->get_result($sql);
			if($main){
				foreach($sql->result_array() as $data){
					$arrdata['event'][] = $data;
				}
			}
			$arrdata['tahun'] = $data['berakhir'];
			//print_r($arrdata);die();
			//echo $arrdata['event']['0']['nama'];die();
			//$arrdata['row1'] = $this->db->query($sql)->row_array();
			//print_r($arrdata);die();
			//print_r($arrdata[]['sess']);die();
			$arrdata['row'] = $this->db->query($query)->row_array();
			$arrdata['ttd'] = $this->suggest_ttd($a);//print_r($arrdata['ttd']);die();
			$arrdata['tembusan'] = $this->get_tembusan($a, $b);
			$arrdata['link'] = url_sipt.'verifikasi/'.hashids_encrypt((int)$arrdata['row']['direktorat_id'], _HASHIDS_, 9).'/'.hashids_encrypt($a, _HASHIDS_, 9).'/'.$b;
			$arrdata['pemasaran'] = $this->get_pemasaran($a,$b,$arrdata['row']['pemasaran']);
			$arrdata['urlFoto'] = $this->get_pendukung($a,$b,'16');
			$arrdata['suratDistributor'] = $this->get_pendukung($a,$b,'23');
			$arrdata['suratSubDistributor'] = $this->get_pendukung($a,$b,'28');
			$arrdata['suratITMB'] = $this->get_pendukung($a,$b,'44');
			$arrdata['suratSKPLA'] = $this->get_pendukung($a,$b,'33');
			$arrdata['suratSKPA'] = $this->get_pendukung($a,$b,'30');
			$arrdata['tembusan'] = $this->get_tembusan($a,$b);
			$no_aju = str_replace("/", "", $arrdata['row']['no_aju']);
			if($c != ""){
				$kode = '87'; $folder = 'DOKPMRN';
				$arrdata['namafile'] = "DOCUMENT-" . $no_aju .'.pdf';
				$arrdata['dir'] = './upL04d5/document/'.$folder.'/'. date("Y")."/".date("m")."/".date("d");
				$sqlInsertPendukung = "INSERT INTO t_upload (trader_id, tipe_dok, nomor, tgl_dok, tgl_exp, penerbit_dok, nama_file, ori_file, folder, ekstensi, ukuran, created_user, created) 
				VALUES('".$arrdata['row']['trader_id']."','".$kode."','".$arrdata['row']['no_izin']."','".$arrdata['row']['tgl_set']."','".$arrdata['row']['tgl_abis']."','Direktorat Jenderal Perdagangan Dalam Negeri','".$arrdata['namafile']."','".$arrdata['row']['no_izin']."','".$arrdata['dir']."','.pdf','0','system',GETDATE())";
				$insert = $this->db->query($sqlInsertPendukung);
			}
			return $arrdata;
		//}
	}

	function get_tembusan($a, $b){
		$query = "SELECT urutan, keterangan FROM m_tembusan WHERE kd_izin = ".$a." AND permohonan_id =".hashids_decrypt($b,_HASHIDS_,9);
		$tembusan = $this->db->query($query)->result_array();
		return $tembusan;
	}

	function suggest_ttd($a){
        $sql = "SELECT nama, jabatan FROM m_ttd a WHERE a.kd_izin = '".$a."' AND a.status = '1'";
        $data = $this->main->get_result($sql);
        if ($data) {
            $ttd = $sql->row_array();
        }
        return $ttd;
    }

	function get_pemasaran($a,$b,$c){
		if($a =='4'){
			$idprop = str_replace(";", "','", $c);
			$sqlprop = "SELECT 'Prop. ' + nama AS nama FROM m_prop WHERE id IN ('".$idprop."')";
			$dataprop = $this->main->get_result($sqlprop);
			if($dataprop){
				foreach($sqlprop->result_array() as $rprop){
					$arrprop[] = $rprop['nama'];
				}
				$pemasaran = join("; ", $arrprop);
			}
		}elseif (($a!='4') || ($a!='3')) {
			$idkab = str_replace(";", "','", $c);
			$sqlkab = "SELECT nama FROM m_kab WHERE id IN ('".$idkab."')";
			$datakab = $this->main->get_result($sqlkab);
			if($datakab){
				foreach($sqlkab->result_array() as $rkab){
					$arrkab[] = $rkab['nama'];
				}
				$pemasaran = join("; ", $arrkab);
			}
		}
		return $pemasaran;

	}

	function get_pendukung($a,$b,$c){
		$query = "SELECT b.nomor, dbo.dateIndo(b.tgl_dok) AS tgl_dok, b.penerbit_dok, SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file as url
				FROM t_upload_syarat a 
				LEFT JOIN t_upload b ON b.id = a.upload_id
				WHERE a.permohonan_id = ".hashids_decrypt($b,_HASHIDS_,9)." AND a.izin_id = ".$a." AND a.dok_id =".$c;
		$data = $this->main->get_result($query);
		if($data){
			$arrsiupmb =array('23','28','33','30');
			if (in_array($c, $arrsiupmb)) {
				$arrPendukung = $query->result_array();
				$banyakPendukung = count($arrPendukung);
				for ($p=0; $p<$banyakPendukung; $p++){
					$pendukung .= $arrPendukung[$p]['penerbit_dok'];
					$pendukung .= " Nomor :".$arrPendukung[$p]['nomor']." tanggal ".$arrPendukung[$p]['tgl_dok'];
					
					if ($banyakPendukung > 2){
						if ($p != ($banyakPendukung-1)){
							$pendukung .= "., ";
						}
						else{
							$pendukung .= " ";
						}
					}
					else if ($banyakPendukung == 2){
						if ($p != ($banyakPendukung-1)){
							$pendukung .= "., ";
						}
						else{
							$pendukung .= " ";
						}
					}
					else{
						$pendukung .= " ";
					}
				}
			}
			else{
				foreach($query->result_array() as $row){
					$pendukung = $row;
				}
			}
		}
		return $pendukung;

	}
		
}
?>