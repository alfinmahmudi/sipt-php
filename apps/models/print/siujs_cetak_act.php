<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Siujs_cetak_act extends CI_Model {

    function set_prints($a, $b, $c = "") {
        //if($this->newsession->userdata('_LOGGED')){
        /*
          @ $a = Kode Izin
          @ $b = Permohonan ID
         */
        $arrdata = array();
        $query = "SELECT a.id, a.kd_izin, a.no_aju, a.tgl_izin_exp as exp,  a.tgl_izin as izin , dbo.dateIndo(a.tgl_aju) AS tgl_aju, a.no_izin, dbo.dateIndo(a.tgl_izin) AS tgl_izin, dbo.dateIndo(a.tgl_izin_exp) AS tgl_izin_exp , a.trader_id, a.tipe_permohonan, 
					c.uraian AS permohonan ,dbo.npwp(a.npwp) AS npwp, d.uraian AS tipe_perusahaan, a.almt_perusahaan,
					dbo.get_region(2, a.kdprop) AS prop_perusahaan, dbo.get_region(4, a.kdkab) AS kab_perusahaan,
					dbo.get_region(7, a.kdkec) AS kec_perusahaan, dbo.get_region(10, a.kdkel) AS kel_perusahaan,
					a.kdpos, a.telp, a.fax, a.status_dok,
					l.uraian AS identitas_pj, a.nama_pj, a.noidentitas_pj, a.jabatan_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, a.alamat_pj,
					dbo.get_region(2, a.kdprop_pj) prop_pj, dbo.get_region(4, a.kdkab_pj) AS kab_pj,
					dbo.get_region(7, a.kdkec_pj) kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj, a.telp_pj, a.fax_pj, 
					a.relasi, a.kegiatan_usaha, a.pegawai_tetap, a.pegawai_honor, a.pegawai_asing, a.pegawai_lokal, a.nama_ttd, a.jabatan_ttd, a.nip_ttd,
					b.nama_izin, b.disposisi, b.direktorat_id, a.kategori_survey,
                    CASE WHEN a.tipe_perusahaan = '05' THEN a.nm_perusahaan
                    ELSE p.uraian+'. '+a.nm_perusahaan
                    END as 'nm_perusahaan'
					FROM t_siujs a
					LEFT JOIN m_izin b ON b.id = a.kd_izin
					LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN'
					LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff l ON l.kode = a.identitas_pj AND l.jenis = 'JENIS_IDENTITAS'
                                        LEFT JOIN m_reff p ON p.kode = a.tipe_perusahaan AND p.jenis = 'KDTIPE_PERUSAHAAN'
					WHERE a.id = '" . hashids_decrypt($b, _HASHIDS_, 9) . "'";//print_r($query);die();
        //$sql = $this->main->get_uraian("select id AS 'no' from t_siujs where permohonan_id='".$B."'","no");
        $arrdata['max'] = hashids_decrypt($b, _HASHIDS_, 9);
        $arrdata['ttd'] = $this->suggest_ttd($a);
        $arrdata['row'] = $this->db->query($query)->row_array(); //print_r($arrdata['row']);die();
        $id_sur = explode(";", $arrdata['row']['kategori_survey']);
        $id_test = array('0' => '3',
                        '1' => '1',
                        '2' => '10',
                        '3' => '2',
                        '4' => '5');//print_r($id_sur);die();
        foreach ($id_sur as $key) {
            $kat_sur = $this->main->get_uraian("SELECT kegiatan_eng FROM m_survey a WHERE a.id = '".$key."'", "kegiatan_eng");
            // if ($key != '2' && $key != '10') {
            //     $kat_sur = explode("(", $kat_sur);
            //     $kat_sur = substr($kat_sur[1], 0,-1);    
            // }elseif ($key == '10') {
            //     $kat_sur = explode("(", $kat_sur);
            //     $kat_sur = substr($kat_sur[2], 0,-1);
            // }
            $kegiatan[] = $kat_sur;
        }
        
        if (trim($kegiatan[0]) == '') {
            $kegiatan = implode(", ", $kegiatan);
            $kegiatan = substr($kegiatan, 2);
        }else{
            $kegiatan = implode(", ", $kegiatan);
        }
        //print_r($kegiatan);die();
        $arrdata['kegiatan'] = $kegiatan;//print_r($arrdata['kegiatan']);die();
        $arrdata['aktaPengesahan'] = $this->get_pendukung($a, $b, '5');//print_r($arrdata['aktaPengesahan']);die();
        $arrdata['cabang'] = $this->get_cabang($a, $b);
        $arrdata['foto_pemilik'] = $this->get_pendukung($a, $b, '16');
        $arrdata['link'] = url_sipt . 'verifikasi/' . hashids_encrypt((int) $arrdata['row']['direktorat_id'], _HASHIDS_, 9) . '/' . hashids_encrypt($a, _HASHIDS_, 9) . '/' . $b;
        $no_aju = str_replace("/", "", $arrdata['row']['no_aju']);
        if ($c != "") {
            $kode = '42';
            $folder = 'DOKSIUJS';
            $arrdata['namafile'] = "DOCUMENT-" . $no_aju.'.pdf';
            $arrdata['dir'] = './upL04d5/document/' . $folder . '/' . date("Y") . "/" . date("m") . "/" . date("d");
            $sqlInsertPendukung = "INSERT INTO t_upload (trader_id, tipe_dok, nomor, tgl_dok, tgl_exp, penerbit_dok, nama_file, ori_file, folder, ekstensi, ukuran, created_user, created) 
				VALUES('" . $arrdata['row']['trader_id'] . "','" . $kode . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['row']['izin'] . "','" . $arrdata['row']['exp'] . "','Direktorat Jenderal Perdagangan Dalam Negeri','" . $arrdata['namafile'] . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['dir'] . "','.pdf','0','system',GETDATE())";
            $insert = $this->db->query($sqlInsertPendukung);
        }
        //print_r($arrdata);die();
        return $arrdata;
        // }
    }

    function get_cabang($a, $b) {
        $query = "SELECT a.id, a.permohonan_id, a.nama_cabang, a.lokasi, telp, fax,
				CASE WHEN a.lokasi = 'Dalam Negeri' THEN CAST(a.alamat AS VARCHAR) +isnull((( ', ' +d.nama)+(', '+c.nama)+(', '+b.nama)+(', '+e.nama)),'') ELSE a.alamat END as alamat 
				FROM t_siujs_cabang a 
				LEFT JOIN m_kab b ON b.id = a.kdkab 
				LEFT JOIN m_kec c ON c.id = a.kdkec 
				LEFT JOIN m_kel d ON d.id = a.kdkel 
				LEFT JOIN m_prop e ON e.id = a.kdprop 
				WHERE a.permohonan_id =" . hashids_decrypt($b, _HASHIDS_, 9);
        $cabang = $this->db->query($query)->result_array();
        return $cabang;
    }

    function suggest_ttd($a){
        $sql = "SELECT nama, jabatan FROM m_ttd a WHERE a.kd_izin = '".$a."' AND a.status = '1'";
        $data = $this->main->get_result($sql);
        if ($data) {
            $ttd = $sql->row_array();
        }
        return $ttd;
    }

    function get_pendukung($a, $b, $c) {
        $query = "SELECT b.nomor, dbo.dateIndo(b.tgl_dok) AS tgl_dok, b.penerbit_dok, SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file as url
				FROM t_upload_syarat a 
				LEFT JOIN t_upload b ON b.id = a.upload_id
				WHERE a.permohonan_id = " . hashids_decrypt($b, _HASHIDS_, 9) . " AND a.izin_id = " . $a . " AND a.dok_id =" . $c;
        $data = $this->main->get_result($query);
        if ($data) {
            $arrsiupmb = array('23', '28', '33', '30');
            if (in_array($c, $arrsiupmb)) {
                $arrPendukung = $query->result_array();
                $banyakPendukung = count($arrPendukung);
                for ($p = 0; $p < $banyakPendukung; $p++) {
                    $pendukung .= $arrPendukung[$p]['penerbit_dok'];
                    $pendukung .= " Nomor :" . $arrPendukung[$p]['nomor'] . " tanggal " . $arrPendukung[$p]['tgl_dok'];

                    if ($banyakPendukung > 2) {
                        if ($p != ($banyakPendukung - 1)) {
                            $pendukung .= "., ";
                        } else {
                            $pendukung .= " ";
                        }
                    } else if ($banyakPendukung == 2) {
                        if ($p != ($banyakPendukung - 1)) {
                            $pendukung .= "., ";
                        } else {
                            $pendukung .= " ";
                        }
                    } else {
                        $pendukung .= " ";
                    }
                }
            } else {
                foreach ($query->result_array() as $row) {
                    $pendukung = $row;
                }
            }
        }
        return $pendukung;
    }

}

?>