<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Siup4_cetak_act extends CI_Model{
	
	function set_prints($a, $b, $c=""){
		// if($this->newsession->userdata('_LOGGED')){
			/*
			@ $a = Kode Izin
			@ $b = Permohonan ID
			*/
			$arrdata = array();
			$query = "SELECT a.id, a.kd_izin, a.no_aju, a.no_izin, a.tgl_izin as tgl_dok, a.tgl_izin_exp as tgl_exp, dbo.dateIndo(a.tgl_izin) AS tgl_izin, dbo.dateIndo(a.tgl_izin_exp) AS tgl_izin_exp , a.trader_id, a.tipe_permohonan, 
					c.uraian AS permohonan ,dbo.npwp(a.npwp) AS npwp, d.uraian AS tipe_perusahaan, a.almt_perusahaan,
					dbo.get_region(2, a.kdprop) AS prop_perusahaan, dbo.get_region(4, a.kdkab) AS kab_perusahaan,
					dbo.get_region(7, a.kdkec) AS kec_perusahaan, dbo.get_region(10, a.kdkel) AS kel_perusahaan,
					a.kdpos, a.telp, a.fax, a.status_dok, status,
					l.uraian AS identitas_pj, a.nama_pj, a.noidentitas_pj, a.jabatan_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, a.alamat_pj,
					dbo.get_region(2, a.kdprop_pj) prop_pj, dbo.get_region(4, a.kdkab_pj) AS kab_pj,
					dbo.get_region(7, a.kdkec_pj) kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj, a.telp_pj, a.fax_pj,
					a.nilai_modal AS nilai_modal, a.nilai_saham AS nilai_saham, b.nama_izin, b.disposisi, a.nama_ttd, a.jabatan_ttd, a.nip_ttd, 
					b.direktorat_id,
					CASE WHEN a.tipe_perusahaan = '05' THEN a.nm_perusahaan
                    ELSE p.uraian+'. '+a.nm_perusahaan
                    END as 'nm_perusahaan'
					FROM t_siup4 a
					LEFT JOIN m_izin b ON b.id = a.kd_izin
					LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN'
					LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff l ON l.kode = a.identitas_pj AND l.jenis = 'JENIS_IDENTITAS'
                                        LEFT JOIN m_reff p ON p.kode = a.tipe_perusahaan AND p.jenis = 'KDTIPE_PERUSAHAAN'
					WHERE a.id = '".hashids_decrypt($b, _HASHIDS_, 9)."'";
			$arrdata['row'] = $this->db->query($query)->row_array();//print_r($arrdata['row']);die();
			$arrdata['ttd'] = $this->suggest_ttd($a);
			$arrdata['link'] = url_sipt.'verifikasi/'.hashids_encrypt((int)$arrdata['row']['direktorat_id'], _HASHIDS_, 9).'/'.hashids_encrypt($a, _HASHIDS_, 9).'/'.$b;
			$no_aju = str_replace("/", "", $arrdata['row']['no_aju']);
			$arrdata['tembusan'] = $this->get_tembusan($a, $b);
			$arrdata['foto_pemilik'] = $this->get_pendukung($a, $b, '16');//print_r($arrdata['foto_pemilik']);die();
			if($c != ""){
				if ($arrdata['row']['kd_izin'] == '2') {
				    $kode = '80'; $folder = 'DOKSIUP4';
				} 
				$arrdata['namafile'] = "DOCUMENT-" . $no_aju.'.pdf';
				$arrdata['dir'] = './upL04d5/document/'.$folder.'/'. date("Y")."/".date("m")."/".date("d");
				$sqlInsertPendukung = "INSERT INTO t_upload (trader_id, tipe_dok, nomor, tgl_dok, tgl_exp, penerbit_dok, nama_file, ori_file, folder, ekstensi, ukuran, created_user, created) 
					VALUES('".$arrdata['row']['trader_id']."','".$kode."','".$arrdata['row']['no_izin']."','".$arrdata['row']['tgl_dok']."','".$arrdata['row']['tgl_exp']."','Direktorat Jenderal Perdagangan Dalam Negeri','".$arrdata['namafile']."','".$arrdata['row']['no_izin']."','".$arrdata['dir']."','.pdf','0','system',GETDATE())";
				$insert = $this->db->query($sqlInsertPendukung);
			}//print_r($arrdata);die();
			return $arrdata;
		//}
	}

	function get_tembusan($a, $b){
		$tembusan =array();
		$query = "SELECT urutan, keterangan FROM m_tembusan WHERE kd_izin = ".$a." AND permohonan_id =".hashids_decrypt($b,_HASHIDS_,9);
		$data = $this->main->get_result($query);
		if($data){
			$tembusan = $query->result_array();
		}
		
		return $tembusan;
	}

	function suggest_ttd($a){
        $sql = "SELECT nama, jabatan FROM m_ttd a WHERE a.kd_izin = '".$a."' AND a.status = '1'";
        $data = $this->main->get_result($sql);
        if ($data) {
            $ttd = $sql->row_array();
        }
        return $ttd;
    }

	function get_pendukung($a,$b,$c){
		$query = "SELECT b.nomor, dbo.dateIndo(b.tgl_dok) AS tgl_dok, b.penerbit_dok, SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file as url
				FROM t_upload_syarat a 
				LEFT JOIN t_upload b ON b.id = a.upload_id
				WHERE a.permohonan_id = ".hashids_decrypt($b,_HASHIDS_,9)." AND a.izin_id = ".$a." AND a.dok_id =".$c;
		$data = $this->main->get_result($query);
		if($data){
			$arrsiupmb =array('23','28','33','30');
			if (in_array($c, $arrsiupmb)) {
				$arrPendukung = $query->result_array();
				$banyakPendukung = count($arrPendukung);
				for ($p=0; $p<$banyakPendukung; $p++){
					$pendukung .= $arrPendukung[$p]['penerbit_dok'];
					$pendukung .= " Nomor :".$arrPendukung[$p]['nomor']." tanggal ".$arrPendukung[$p]['tgl_dok'];
					
					if ($banyakPendukung > 2){
						if ($p != ($banyakPendukung-1)){
							$pendukung .= "., ";
						}
						else{
							$pendukung .= " ";
						}
					}
					else if ($banyakPendukung == 2){
						if ($p != ($banyakPendukung-1)){
							$pendukung .= "., ";
						}
						else{
							$pendukung .= " ";
						}
					}
					else{
						$pendukung .= " ";
					}
				}
			}
			else{
				foreach($query->result_array() as $row){
					$pendukung = $row;
				}
			}
		}
		return $pendukung;

	}
		
}
?>