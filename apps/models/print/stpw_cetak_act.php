<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Stpw_cetak_act extends CI_Model{
	
	function set_prints($a, $b, $c=""){
		//if($this->newsession->userdata('_LOGGED')){
			/*
			@ $a = Kode Izin
			@ $b = Permohonan ID
			*/
			$arrdata = array();
			$query = "SELECT a.id, a.no_izin,dbo.dateIndo(a.tgl_izin_exp) AS tgl_ex, a.tgl_izin_exp AS EXP, dbo.dateIndo(a.tgl_izin) AS izin, b.direktorat_id, a.kd_izin, a.no_aju, a.trader_id,almt_perusahaan, a.tipe_permohonan, c.uraian AS permohonan, a.npwp, d.uraian AS tipe_perusahaan,g.uraian AS produk_wrlb, h.nama AS negara_asal, a.kdprop,a.kdkab,a.kdkec,a.kdkel,a.kdpos,a.fax,a.telp, dbo.get_region(2, a.kdprop) AS prop_perusahaan,dbo.get_region(4, a.kdkab) AS kab_perusahaan, dbo.get_region(7, a.kdkec) AS kec_perusahaan, dbo.get_region(10, a.kdkel) AS kel_perusahaan, e.uraian AS warganegara,i.nama AS negara_pj,a.noidentitas_pj,k.uraian AS identitas_pj,a.nama_pj,z.uraian AS jabatan_pj,a.tmpt_lahir_pj,a.tgl_lahir_pj,a.alamat_pj, dbo.get_region(2, a.kdprop_pj) AS prop_pj,dbo.get_region(4, a.kdkab_pj) AS kab_pj, dbo.get_region(7, a.kdkec_pj) AS kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj,a.telp_pj,fax_pj, c.uraian AS tipe_pw,a.npwp_pw,a.nm_perusahaan_pw,a.almt_perusahaan_pw,c.uraian AS negara_pw, a.email_pw, pj_pw,a.alamat_dn, dbo.get_region(2, a.kdprop_pw) AS prop_pw,dbo.get_region(4, a.kdkab_pw) AS kab_pw, dbo.get_region(7, a.kdkec_pw) AS kec_pw, dbo.get_region(10, a.kdkel_pw) AS kel_pw,a.telp_pw,fax_pw, a.outlet_sendiri,a.outlet_waralaba,a.jenis_dagang,a.merk_dagang, dbo.get_region(4, a.wilayah_dagang) AS wilayah_dagang_old,b.disposisi, a.status, a.tgl_aju, a.nama_ttd, a.jabatan_ttd, a.tipe_pemasaran, a.wilayah_dagang, a.negara_dagang, a.tgl_izin AS tgl_set, a.tgl_izin_exp AS tgl_abis, a.email, a.email_pj, x.uraian AS jenis_usaha, a.jns_usaha, CASE WHEN p.uraian IS NULL THEN a.nm_perusahaan ELSE p.uraian+'. '+a.nm_perusahaan END AS nm_perusahaan, a.produk_waralaba, a.status_dok,
				CASE WHEN LTRIM(RTRIM(a.kdprop_pj)) = '' 
				THEN LTRIM(RTRIM(CONVERT(VARCHAR(MAX), a.alamat_pj))) 
				ELSE CONCAT(LTRIM(RTRIM(CONVERT(VARCHAR(MAX), a.alamat_pj))), ', Kel. ', dbo.get_region(10, a.kdkel_pj), ', Kec. ', dbo.get_region(7, a.kdkec_pj), ', Kab. ', dbo.get_region(4, a.kdkab_pj), ', Prop. ', dbo.get_region(2, a.kdprop_pj)) END AS alamat_jp,
				CASE WHEN LTRIM(RTRIM(a.kdprop)) = '' 
				THEN LTRIM(RTRIM(CONVERT(VARCHAR(MAX), a.almt_perusahaan)))
				ELSE CONCAT(LTRIM(RTRIM(CONVERT(VARCHAR(MAX), a.almt_perusahaan))), ', Kel. ', dbo.get_region(10, a.kdkel), ', Kec. ', dbo.get_region(7, a.kdkec), ', Kab. ', dbo.get_region(4, a.kdkab), ', Prop. ', dbo.get_region(2, a.kdprop)) END AS alamat,
                                a.almt_perusahaan AS 'alamat_ln'
				FROM t_stpw a
				LEFT JOIN m_izin b ON b.id = a.kd_izin
				LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN'
				LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
				LEFT JOIN m_reff e ON e.kode = a.warganegara_pj AND e.jenis = 'WARGANEGARA'
				LEFT JOIN m_reff g ON g.kode = a.produk_waralaba AND g.jenis = 'PRODUK_WARALABA'
				LEFT JOIN m_reff k ON k.kode = a.identitas_pj AND k.jenis = 'JENIS_IDENTITAS'
				LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
				LEFT JOIN m_negara h ON h.kode = a.negara_perusahaan
				LEFT JOIN m_negara i ON i.kode = a.asalnegara_pj
				LEFT JOIN m_reff x ON x.kode = a.jns_usaha AND x.jenis = 'KEGIATAN_USAHA'
				LEFT JOIN m_kab f ON f.id = a.wilayah_dagang
				LEFT JOIN m_reff p ON p.kode = a.tipe_perusahaan AND p.jenis = 'KDTIPE_PERUSAHAAN'
				WHERE a.id = '".hashids_decrypt($b, _HASHIDS_, 9)."'";
			//print_r($query);die();
			
			//$arrdata['row1'] = $this->db->query($sql)->row_array();
			//print_r($arrdata);die();
			//print_r($arrdata[]['sess']);die();
			$arrdata['row'] = $this->db->query($query)->row_array();//print_r($arrdata['row']);die();
			$arrdata['ttd'] = $this->suggest_ttd($a);
			$ex = explode(",",$arrdata['row']['wilayah_dagang']);
			for($i=0;$i<count($ex);$i++){
				if (strlen($c) == 2) {
                    $sql = "select nama from m_prop where id='" . $ex[$i] . "'";
                } else {
                    $sql = "select nama from m_kab where id='" . $ex[$i] . "'";
                }
                $propkab = $this->db->query($sql)->row_array();
				$dagang[] = $propkab['nama'];
			}

			$ex_1 = explode(",",$arrdata['row']['negara_dagang']);
			for($i=0;$i<count($ex);$i++){
				$sql = "select nama from m_negara where kode = '" . $ex_1[$i] . "'";
                $nama_nagara = $this->db->query($sql)->row_array();
				$negara[] = $nama_nagara['nama'];
			}
			$arrdata['dagang'] = implode(', ', $dagang);
			$arrdata['negara'] = implode(', ', $negara);
			$arrdata['link'] = url_sipt.'verifikasi/'.hashids_encrypt((int)$arrdata['row']['direktorat_id'], _HASHIDS_, 9).'/'.hashids_encrypt($a, _HASHIDS_, 9).'/'.$b;
			$arrdata['tembusan'] = $this->get_tembusan($a, $b);
			$arrdata['pemasaran'] = $this->get_pemasaran($a,$b,$arrdata['row']['pemasaran']);
			$arrdata['urlFoto'] = $this->get_pendukung($a,$b,'16');
			$arrdata['suratPerjanjian'] = $this->get_pendukung($a,$b,'90');
			$arrdata['STPW'] = $this->get_pendukung($a,$b,'94');//print_r($arrdata['STPW']);die();
			$no_aju = str_replace("/", "", $arrdata['row']['no_aju']);
			if($c != ""){	
					if($a == "17"){
						$kode = '81'; 
						$folder = 'DOKWRPW';
					}elseif($a == "18"){
						$kode = '82'; 
						$folder = 'DOKWRPWJ';
					}elseif($a == "19"){
						$kode = '83'; 
						$folder = 'DOKWRPN';
					}elseif($a == "20"){
						$kode = '84'; 
						$folder = 'DOKWRPNJ';
					}
					$arrdata['namafile'] = "DOCUMENT-" . $no_aju.'.pdf';
					$arrdata['dir'] = './upL04d5/document/'.$folder.'/'. date("Y")."/".date("m")."/".date("d");
					$sqlInsertPendukung = "INSERT INTO t_upload (trader_id, tipe_dok, nomor, tgl_dok, tgl_exp, penerbit_dok, nama_file, ori_file, folder, ekstensi, ukuran, created_user, created) 
					VALUES('".$arrdata['row']['trader_id']."','".$kode."','".$arrdata['row']['no_izin']."','".$arrdata['row']['tgl_set']."','".$arrdata['row']['tgl_abis']."','Direktorat Jenderal Perdagangan Dalam Negeri','".$arrdata['namafile']."','".$arrdata['row']['no_izin']."','".$arrdata['dir']."','.pdf','0','system',GETDATE())";
					$insert = $this->db->query($sqlInsertPendukung);
				}
			return $arrdata;
		//}
	}

	function get_tembusan($a, $b){
		$query = "SELECT urutan, keterangan FROM m_tembusan WHERE kd_izin = ".$a." AND permohonan_id =".hashids_decrypt($b,_HASHIDS_,9);
		$tembusan = $this->db->query($query)->result_array();
		return $tembusan;
	}

	function suggest_ttd($a){
        $sql = "SELECT nama, jabatan FROM m_ttd a WHERE a.kd_izin = '".$a."' AND a.status = '1'";
        $data = $this->main->get_result($sql);
        if ($data) {
            $ttd = $sql->row_array();
        }
        return $ttd;
    }

	function get_pemasaran($a,$b,$c){
		if($a =='4'){
			$idprop = str_replace(";", "','", $c);
			$sqlprop = "SELECT 'Prop. ' + nama AS nama FROM m_prop WHERE id IN ('".$idprop."')";
			$dataprop = $this->main->get_result($sqlprop);
			if($dataprop){
				foreach($sqlprop->result_array() as $rprop){
					$arrprop[] = $rprop['nama'];
				}
				$pemasaran = join("; ", $arrprop);
			}
		}elseif (($a!='4') || ($a!='3')) {
			$idkab = str_replace(";", "','", $c);
			$sqlkab = "SELECT nama FROM m_kab WHERE id IN ('".$idkab."')";
			$datakab = $this->main->get_result($sqlkab);
			if($datakab){
				foreach($sqlkab->result_array() as $rkab){
					$arrkab[] = $rkab['nama'];
				}
				$pemasaran = join("; ", $arrkab);
			}
		}
		return $pemasaran;

	}

	function get_pendukung($a,$b,$c){
		$query = "SELECT b.nomor, dbo.dateIndo(b.tgl_dok) AS tgl_dok, b.penerbit_dok, SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file as url
				FROM t_upload_syarat a 
				LEFT JOIN t_upload b ON b.id = a.upload_id
				WHERE a.permohonan_id = ".hashids_decrypt($b,_HASHIDS_,9)." AND a.izin_id = ".$a." AND a.dok_id =".$c;
                
		$data = $this->main->get_result($query);
		if($data){
			$arrsiupmb =array('23','28','33','30');
			if (in_array($c, $arrsiupmb)) {
				$arrPendukung = $query->result_array();
				$banyakPendukung = count($arrPendukung);
				for ($p=0; $p<$banyakPendukung; $p++){
					$pendukung .= $arrPendukung[$p]['penerbit_dok'];
					$pendukung .= " Nomor :".$arrPendukung[$p]['nomor']." tanggal ".$arrPendukung[$p]['tgl_dok'];
					
					if ($banyakPendukung > 2){
						if ($p != ($banyakPendukung-1)){
							$pendukung .= "., ";
						}
						else{
							$pendukung .= " ";
						}
					}
					else if ($banyakPendukung == 2){
						if ($p != ($banyakPendukung-1)){
							$pendukung .= "., ";
						}
						else{
							$pendukung .= " ";
						}
					}
					else{
						$pendukung .= " ";
					}
				}
			}
			else{
				foreach($query->result_array() as $row){
					$pendukung = $row;
				}
			}
		}
		return $pendukung;

	}
		
}
?>