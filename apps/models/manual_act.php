<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Manual_act extends CI_Model{
	
	
	function get_manual(){
		if($this->newsession->userdata('_LOGGED')){
			$direktorat = "'".join("','", $this->newsession->userdata('dir_id'))."'";
			$arrdata['jenis_izin'] = $this->main->set_combobox("SELECT id, nama_izin FROM m_izin WHERE direktorat_id IN (".$direktorat.") AND aktif='1'","id","nama_izin", TRUE);
			return $arrdata;
		}
	}

	function get_data($dbTable, $kd_izin, $nomor_aju){
		$sqlCekData = "SELECT COUNT(*) AS JML FROM dbo.view_permohonan WHERE kd_izin = ".$kd_izin." AND no_aju ='".$nomor_aju."'";
		$jmlData = (int)$this->main->get_uraian($sqlCekData,"JML");
		if($jmlData > 0){
			$ret = "MSG||NO||Data Sudah Ada di SIPT-PDN";
            return $ret;
		}
		$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$kd_izin."'";
		$nama_izin = $this->main->get_uraian($sql,'nama_izin');

		$this->load->library('nusoap');
		$xmlSend = '<?xml version="1.0" encoding="UTF-8"?>
					<data_request>
					<nomor_inatrade>'.$nomor_aju.'</nomor_inatrade>
					</data_request>';
		$parameter = array("PDN","userpdn",$xmlSend);
		//$urlwsdl = "http://inatrade.kemendag.go.id/ws/akses.php?wsdl";
		$urlwsdl = "http://10.1.19.44/inatrade_be/upp.php?wsdl";
		$client = new nusoap_client($urlwsdl, true);
		$result = $client->call('getDocument2',$parameter);
		if($result){
			$res = simplexml_load_string($result);
			$json = json_encode($res);
			$arrxml = json_decode($json,TRUE);// print_r($arrxml); die();
			if($arrxml['data_permohonan']['result'] == "True"){
				if(array_key_exists('data_permohonan', $arrxml)){
					if(is_array($arrxml['data_pendaftar']['no_ktp'])) $no_ktp = "-";
					else $no_ktp = $arrxml['data_pendaftar']['no_ktp'];

					if(is_array($arrxml['data_perusahaan']['email'])) $email = "-";
					else $email = $arrxml['data_perusahaan']['email'];

					$arrPermohonan['kd_izin'] = $kd_izin;
					$arrPermohonan['no_aju'] = $nomor_aju;
//					$arrPermohonan['trader_id'] = '0';
					$arrPermohonan['tgl_aju'] = $arrxml['data_pendaftar']['tgl_daftar'];
					$arrPermohonan['tipe_permohonan'] = '0'.$arrxml['data_permohonan']['jenis_permohonan'];
					$arrPermohonan['npwp'] = $arrxml['data_perusahaan']['npwp'];
					$arrPermohonan['nm_perusahaan'] = $arrxml['data_perusahaan']['nama'];
					$arrPermohonan['almt_perusahaan'] = $arrxml['data_perusahaan']['alamat'];
					$arrPermohonan['kdprop'] = $arrxml['data_perusahaan']['kode_propinsi'];
					$arrPermohonan['kdkab'] = $arrxml['data_perusahaan']['kode_kabupaten'];
					$arrPermohonan['kdpos'] = $arrxml['data_perusahaan']['kode_pos'];
					$arrPermohonan['telp'] = $arrxml['data_perusahaan']['telepon'];
					$arrPermohonan['fax'] = $arrxml['data_perusahaan']['fax'];
					$arrPermohonan['noidentitas_pj'] = $no_ktp;
					$arrPermohonan['nama_pj'] = $arrxml['data_perusahaan']['nama_penanggung_jawab'];
					$arrPermohonan['jabatan_pj'] = $arrxml['data_perusahaan']['jabatan_penanggung_jawab'];
					$arrPermohonan['telp_pj'] = $arrxml['data_perusahaan']['telepon_penanggung_jawab'];
					$arrPermohonan['fl_pencabutan'] = '0';
					$arrPermohonan['status'] = '0100';
					$arrPermohonan['source'] = '1';
					$arrPermohonan['created'] = 'GETDATE()';
					$arrPermohonan['created_user'] = $this->newsession->userdata('username');
					$res = FALSE;
					$arrTrader['npwp'] = $arrxml['data_perusahaan']['npwp'];
					$arrTrader['nm_perusahaan'] = $arrxml['data_perusahaan']['nama'];
					$arrTrader['almt_perusahaan'] = $arrxml['data_perusahaan']['alamat'];
					$arrTrader['kdprop'] = $arrxml['data_perusahaan']['kode_propinsi'];
					$arrTrader['kdkab'] = $arrxml['data_perusahaan']['kode_kabupaten'];
					$arrTrader['telp'] = $arrxml['data_perusahaan']['telepon'];
					$arrTrader['fax'] = $arrxml['data_perusahaan']['fax'];
					$arrTrader['jns_usaha'] = $arrxml['data_perusahaan']['bidang_usaha'];
					$arrTrader['na_pj'] = $arrxml['data_perusahaan']['nama_penanggung_jawab'];
					$arrTrader['jabatan_pj'] = $arrxml['data_perusahaan']['jabatan_penanggung_jawab'];
					$arrTrader['email_pj'] = $email;
					$arrTrader['created'] = 'GETDATE()';
					$chktrader = (int)$this->main->get_uraian("SELECT COUNT(*) AS JML FROM m_trader WHERE npwp = '".$arrTrader['npwp']."'","JML");
					
					if($chktrader > 0){
						$trader_id = $this->main->get_uraian("SELECT id FROM m_trader WHERE npwp = '".$arrTrader['npwp']."'","id");
					}else{
						$this->db->insert('m_trader',$arrTrader); 
						if($this->db->affected_rows() > 0){
							$trader_id = $this->db->insert_id();
						}
					}
					if($trader_id){
						$this->db->trans_begin();
						$arrPermohonan['trader_id'] = $trader_id;
						if($dbTable == "t_stpw"){
							$arrPermohonan['produk_waralaba'] = " ";
						}
						if($dbTable == "t_siup_agen"){
							$arrPermohonan['produksi '] = " ";
							$arrPermohonan['jenis_agen']=" ";
						}
						if($dbTable == "t_siupbb"){
							$arrPermohonan['kategori'] = " ";
							$arrPermohonan['tipe_perusahaan'] = " ";
						}
						//print_r($arrPermohonan);die();
						$this->db->insert($dbTable,$arrPermohonan); 
						if($this->db->affected_rows() > 0){
							$res = TRUE;	
							$idPermohonan = $this->db->insert_id();						
							$ret = "MSG||YES||Data Berhasil Disimpan||REFRESH";
							$logu = array('aktifitas' => 'Penarikan data permohonan '.$nama_izin.' dari INATRADE dengan nomor permohonan : '.$nomor_aju,
										  'url' => '{c}'. site_url().'manual/get_data'. ' {m} models/manual_act {f} get_data($act, $ajax)');
							$this->main->set_activity($logu);
							$logi = array('kd_izin' => $kd_izin,
										  'permohonan_id' => $idPermohonan,
										  //Draft 
										  'keterangan' => 'UPP INATRADE <i class="fa fa-arrow-circle-right"></i> Pemroses Subdit - Terkirim ke Pemroses Subdit <br> Penarikan data permohonan '.$nama_izin.' dengan nomor permohonan : '.$nomor_aju,
										  'catatan' =>  '-',
										  'status' => '0100',
										  'selisih' => '0');
							$this->main->set_loglicensing($logi);
						}
						
						if($this->db->trans_status() === FALSE || !$res){
							$this->db->trans_rollback();
							$ret = 'MSG||NO||Data Gagal Disimpan';
						}else{
							$this->db->trans_commit();
						}
					}
				}
			}else if($arrxml['data_permohonan']['result'] == "False"){
				$ret = 'MSG||NO||Tidak Terdapat Data';
			}
		}else{
			$ret = 'MSG||NO||Tidak Terdapat Data';
		}
		return $ret;
	}
}
?>