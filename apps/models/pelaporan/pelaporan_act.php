<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Pelaporan_act extends CI_Model{
	
	var $ineng = "";
	
	function get_pelaporan($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			$arrdata = array();
			$idprop = $this->newsession->userdata('kdprop');
			$idkab = $this->newsession->userdata('kdkab');
			$idkec = $this->newsession->userdata('kdkec');
			$idkel = $this->newsession->userdata('kdkel');//print_r($idkel);die();
			$tipe_perusahaan = $this->newsession->userdata('tipe_perusahaan');
			$jenis_identitas = $this->newsession->userdata('tipe_identitas');
			$jabatan_pj = $this->newsession->userdata('jabatan_pj');
			$id_user = $this->newsession->userdata('id');
			$arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '".$idprop."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '".$idkab."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '".$idkec."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '".$idkel."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['direktorat'] = $dir;
			$arrdata['tipe'] = $type;
			$arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' AND kode = '01'","kode","uraian", TRUE);
			$arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '".$tipe_perusahaan."' AND jenis = 'TIPE_PERUSAHAAN'","kode","uraian", TRUE);
			$arrdata['kategori'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'KATEGORI_SIUPBB'","kode","uraian", TRUE);
			$arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '".$jenis_identitas."' and jenis = 'JENIS_IDENTITAS'","kode","uraian", TRUE);
			$arrdata['jabatan_pj'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JABATAN' AND kode = '".$jabatan_pj."' ","kode","uraian", TRUE);
			$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
			$nama_izin = $this->main->get_uraian($sql,'nama_izin');
			$arrdata['nama_izin'] = $nama_izin;
			$minol = array("3", "4", "5", "6", "13");
			$waralaba = array("17", "18", "19", "20");
			if ($doc == '1') {
				$ijin = 'siujs_act';
			}elseif ($doc == '2') {
				$ijin = 'siup4_act';
			}elseif (in_array($doc, $minol)) {
				$ijin = 'siupmb_act';
			}elseif (in_array($doc, $waralaba)) {
				$ijin = 'stpw_act';
			}elseif ($doc == '12') {
				$ijin = 'siupagen_act';
			}elseif ($doc == '14') {
				$ijin = 'garansi_act';
			}elseif ($doc == '15' || $doc == '16') {
				$ijin = 'pameran_act';
			}elseif ($doc == '11') {
				$ijin = 'siupbb_act';
			}elseif ($doc == '7') {
				$ijin = 'pkapt_act';
			}elseif ($doc == '10') {
				$ijin = 'sppgrap_act';
			}
			$arrdata['act'] = site_url('post/licensing/'.$ijin.'/pelaporan/save');
			$arrdata['sess']['tipe_permohonan'] = $type;
			$arrdata['sess']['kd_izin'] = $doc;
			return $arrdata;
		}
	}

	function set_pelaporan($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			$res = FALSE;
			$txt = "Surat Izin Usaha Perdagangan Bahan Berbahaya (SIUP B2) sebagai DT-B2";
			$msg = "MSG||NO||Data Permohonan gagal disimpan";
			$ressiup = FALSE;
			$arrsiup = $this->main->post_to_query($this->input->post('siupbb'));
			$doc = $arrsiup['kd_izin'];
			$minol = array("3", "4", "5", "6", "13");
			$waralaba = array("17", "18", "19", "20");
			if ($doc == '1') {
				$ijin = 't_siujs';
			}elseif ($doc == '2') {
				$ijin = 't_siup4';
			}elseif (in_array($doc, $minol)) {
				$ijin = 't_siupmb';
			}elseif (in_array($doc, $waralaba)) {
				$ijin = 't_stpw';
			}elseif ($doc == '12') {
				$ijin = 't_siup_agen';
				$arrsiup['produksi'] = '-';
				$arrsiup['jenis_agen'] = '-';
			}elseif ($doc == '14') {
				$ijin = 't_garansi';
			}elseif ($doc == '15' || $doc == '16') {
				$ijin = 't_pameran';
			}elseif ($doc == '11') {
				$ijin = 't_siupbb';
			}elseif ($doc == '7') {
				$ijin = 't_pkapt';
			}elseif ($doc == '10') {
				$ijin = 't_sppgrap';
			}

			$arrsiup['no_aju'] = $this->main->set_aju();
			$arrsiup['tipe_permohonan'] = '01';
			$arrsiup['fl_pencabutan'] = '0';
            $arrsiup['status'] = '1000';
            $arrsiup['tgl_aju'] = 'GETDATE()';
            $arrsiup['trader_id'] = $this->newsession->userdata('trader_id');
            $arrsiup['created'] = 'GETDATE()';
            $arrsiup['created_user'] = $this->newsession->userdata('username');
            $cek_noizin = "SELECT id FROM ".$ijin." WHERE no_izin = '".$arrsiup['no_izin']."' ";
            $cek = $this->db->query($cek_noizin)->num_rows();
            if ($cek == '0') {
            	$this->db->trans_begin();
				$this->db->insert($ijin, $arrsiup);//print_r($this->db->last_query());die();
				if ($this->db->affected_rows() > 0) {
	                $res = TRUE;
	            }
				if ($this->db->trans_status() === FALSE) {
	                $this->db->trans_rollback();
	                $msg = "MSG||NO||Data Gagal disimpan";
	            } else {
	                $this->db->trans_commit();
	                $msg = "MSG||YES||Data Berhasil disimpan";
	            }
            }else{
            	$msg = "MSG||NO||Nomer Izin Sudah Terdaftar";
            }
            return $msg;
		}
	}

}