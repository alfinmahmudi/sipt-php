<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Management_act extends CI_Model {

    function get_profil($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($this->newsession->userdata('role') != '05') {
                $direktorat = "'" . join("','", $this->newsession->userdata('dir_id')) . "'";
                //print_r($direktorat);die();
                $sqlDir = "SELECT uraian FROM m_reff WHERE jenis = 'DIREKTORAT' AND kode IN (" . $direktorat . ")";
                $dataDir = $this->main->get_result($sqlDir);
                if ($dataDir) {
                    foreach ($sqlDir->result_array() as $rdir) {
                        $arrDir[] = $rdir['uraian'];
                    }
                    //$arrdata['dir'] = join(", ", $arrDir);
                    $arrdata['dir'] = $sqlDir->result_array();
                }
                $kd_izin = "'" . join("','", $this->newsession->userdata('kode_izin')) . "'";
                $sqlDoc = "SELECT nama_izin FROM m_izin WHERE id IN (" . $kd_izin . ")";
                $dataDoc = $this->main->get_result($sqlDoc);
                if ($dataDoc) {
                    foreach ($sqlDoc->result_array() as $rdoc) {
                        $arrdoc[] = $rdoc['nama_izin'];
                    }
                    //$arrdata['doc'] = join(", ", $arrdoc);
                    $arrdata['doc'] = $sqlDoc->result_array();
                }
                $arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_IDENTITAS'", "kode", "uraian", TRUE);
                $query = "SELECT a.id, a.username, a.password, a.nama, a.alamat, a.telp, a.fax, a.email, a.no_identitas, a.tipe_identitas, a.trader_id, b.uraian as role, a.last_login, c.uraian AS status  
						FROM t_user a
						LEFT JOIN m_reff b ON b.kode = a.role and b.jenis = 'ROLE'
						LEFT JOIN m_reff c ON c.kode = a.status and c.jenis = 'STATUS_USER'
						WHERE id = " . $this->newsession->userdata('id');
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                }
                $arrdata['act'] = site_url('post/management/profil/update');
            } else {
                $arrdata['act'] = site_url('post/management/profil/update');
                $arrdata['readonly'] = "readonly";
                $arrdata['disabled'] = "disabled";
            }
            return $arrdata;
        }
    }

    function set_profil($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data Profil gagal disimpan";
            $result = FALSE;
            $arrprofil = $this->main->post_to_query($this->input->post('PROFIL'));
            $id_session = $this->newsession->userdata('id');
            $idx = $this->input->post('id');
            $pwd = $this->input->post('password');
            $newpwd = $this->input->post('retypepwd');
    
            //for validate old password
            $oldpwd = $this->input->post('oldpwd');
            $lama = $this->db->query("SELECT password FROM t_user WHERE id = '".$id_session."' ")->row_array();
            $user = base64_encode(trim(strtolower($this->newsession->userdata('username'))));
            $pass = md5($oldpwd.$user);

            if ($oldpwd != "" && $this->newsession->userdata('role') == '05') {
                if ($lama['password'] != $pass) {
                    $msg = "MSG||NO||Data Profil gagal disimpan. Password Lama / Sebelumnya Tidak Sesuai";
                    return $msg;
                }
            }

            if ($pwd != "") {
                if ($newpwd != $pwd) {
                    $msg = "MSG||NO||Data Profil gagal disimpan. Re-Type Password tidak sesuai";
                    return $msg;
                } else {
                    $usr = base64_encode(strtolower(trim($this->newsession->userdata('username'))));
                    $arrprofil['password'] = md5($pwd . $usr);
                }
            }
            //print_r($arrprofil);die();
            $this->db->trans_begin();
            if ($this->newsession->userdata('role') == '05') {
                $this->db->where('id', $id_session);    
            }else{
                $this->db->where('id', $idx);
            }
            $this->db->update('t_user', $arrprofil);
            if ($this->db->affected_rows() == 1) {
                $result = TRUE;
                $msg = "MSG||YES||Data berhasil di update||REFRESH";
            }
            if ($this->db->trans_status() === FALSE || !$result) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
            return $msg;
        }
    }

}

?>