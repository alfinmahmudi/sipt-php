<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Company_act extends CI_Model{

	var $arrext = array('.jpg', '.JPG', '.jpeg', '.JPEG', '.pdf', ".PDF", ".png", ".PNG");
	
	function get_profil($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			$id = $this->newsession->userdata('trader_id');
			$query = "SELECT dbo.npwp(npwp) as npwp,
						CASE 
						WHEN tipe_perusahaan = 1 THEN 'Perseroan Terbatas'
						WHEN tipe_perusahaan = 2 THEN 'Koperasi'
						WHEN tipe_perusahaan = 3 THEN 'Persekutuan Komanditer (CV)'
						WHEN tipe_perusahaan = 4 THEN 'Persekutuan Firma'
                        WHEN tipe_perusahaan = 5 THEN 'Perorangan'
						END AS tipe_perusahaan, nm_perusahaan, almt_perusahaan, email,
						dbo.get_region(2, kdprop) AS kdprop, dbo.get_region(4, kdkab) AS kdkab,
						dbo.get_region(7, kdkec) AS kdkec,
						telp, fax, jns_usaha, no_tdp, dbo.dateIndo(tgl_tdp) as tgl_tdp, dbo.dateIndo(tgl_exp_tdp) as tgl_exp_tdp,
						na_pj, b.uraian as jabatan_pj, telp_pj, fax_pj, email_pj, kdpos
						FROM m_trader
						LEFT JOIN m_reff b ON b.kode = jabatan_pj and b.jenis = 'JABATAN'
						WHERE id = '".$id."'";
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['sess'] = $row;
						$arrdata['act'] = site_url('setting/edit_per');
					}
				}	
			return $arrdata;
		}
	}

	function edit_per(){
		 if ($this->newsession->userdata('_LOGGED')) {
            if (trim($this->newsession->userdata('tipe_perusahaan')) == '') {
                $add =  "";
            }else{
                $add = "and kode = '".$this->newsession->userdata('tipe_perusahaan')."'";
            }
            $get_usaha = "SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN' ".$add." ";
            $arrdata['usaha'] = $this->main->set_combobox($get_usaha, "kode", "uraian", TRUE);

            $get_prop = "SELECT id, nama from m_prop";
            $arrdata['prop'] = $this->main->set_combobox($get_prop, "id", "nama", TRUE);
            $arrdata['identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_IDENTITAS' ", "kode", "uraian", TRUE);

            $arrdata['jabatan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JABATAN' ", "kode", "uraian", TRUE);
            //if (trim($this->newsession->userdata('tipe_registrasi')) == '') {
                $arrdata['pendaftaran'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_REGISTRASI'", "kode", "uraian", TRUE);
            //}else{
              //  $arrdata['pendaftaran'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_REGISTRASI' and kode = '".$this->newsession->userdata('tipe_registrasi')."' ", "kode", "uraian", TRUE);
            //}
            $sql = " SELECT a.id, a.npwp, a.nm_perusahaan, a.tipe_registrasi, a.tipe_perusahaan, a.almt_perusahaan, a.kdprop as 'kdprop_per', a.kdkab as 'kdkab_per', a.email,
                    a.kdkec as 'kdkec_per', a.kdkel as 'kdkel_per', a.kdpos as 'kdpos_per', a.telp as 'telp_per', a.fax as 'fax_per', a.jns_usaha, a.no_tdp, 
                    a.tgl_tdp, a.file_tdp, a.file_npwp, a.na_pj, a.jabatan_pj, a.tgl_lahir_pj, a.tgl_lahir_pj, a.tmpt_lahir_pj, a.identitas_pj, a.noidentitas_pj,
                    a.alamat_pj, a.kdprop_pj, a.kdkab_pj, a.kdkec_pj, a.kdkel_pj, a.telp_pj, a.fax_pj, a.email_pj, a.direktorat, b.id as 'user_id', b.username,
                    b.nama as 'nama_koor', b.alamat as 'alamat_koor', b.telp as 'telp_koor', b.fax as 'fax_koor', b.email as 'email_koor', b.no_identitas, 
                    b.tipe_identitas, b.file_identitas, b.file_kuasa, '<a href=\"" . site_url() . "download/doc' + substring(a.file_tdp, 19, (LEN(a.file_tdp)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewTDP', '<a href=\"" . site_url() . "download/doc' + substring(a.file_npwp, 19, (LEN(a.file_npwp)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewNpwp', '<a href=\"" . site_url() . "download/doc' + substring(b.file_identitas, 19, (LEN(b.file_identitas)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewKTP', '<a href=\"" . site_url() . "download/doc' + substring(b.file_kuasa, 19, (LEN(b.file_kuasa)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewSK', '<a href=\"" . site_url() . "download/doc' + substring(a.file_ktp, 19, (LEN(a.file_ktp)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewKTP_PJ', a.tgl_exp_tdp
                    FROM m_trader a
                    LEFT JOIN t_user b ON b.trader_id = a.id
                    WHERE a.id = '" . $this->newsession->userdata('trader_id') . "' ";
            $arrsess = $this->db->query($sql)->row_array();
            $arrdata['sess'] = $arrsess;
            $arrdata['kab_per'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $arrsess['kdprop_per'] . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kec_per'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $arrsess['kdkab_per'] . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kel_per'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $arrsess['kdkec_per'] . "' ORDER BY 2", "id", "nama", TRUE);

            $arrdata['kab_pj'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $arrsess['kdprop_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kec_pj'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $arrsess['kdkab_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kel_pj'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $arrsess['kdkec_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
            //print_r($arrdata);die();
            $pass = 'sipt'.date('YmdH').'-'.date('YmdHis');
            $arrdata['addGets'] = $this->main->encrypt($pass);
            $arrdata['act'] = site_url('setting/set_per');
            return $arrdata;
        }
	}
	
	function set_per($isajax){
		if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
            }
            //print_r($this->main->allowed($_FILES['filenpwp']['name']));exit();
            $msg = "MSG||NO||Update Gagal.";
            $resreg = FALSE;
            $arrregis = $this->main->post_to_query($this->input->post('REG'));//print_r($arrregis);die();

            $dirTDP = './upL04d5/document/TDP/' . date("Y") . "/" . date("m") . "/" . date("d") . "/";
            $dirKTP_PJ = './upL04d5/document/KTP_PJ/' . date("Y") . "/" . date("m") . "/" . date("d") . "/";
            $dirSK = './upL04d5/document/SURATKUASA/' . date("Y") . "/" . date("m") . "/" . date("d") . "/";

            if ($_FILES['filetdp']['name'] != NULL) {
                if (!empty($_FILES['filetdp']['error'])) {
                    switch ($_FILES['filetdp']['error']) {
                        case '1':
                            $msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
                            return $msg;
                            break;
                        case '2':
                            $msg = 'MSG||NO||The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                            return $msg;
                            break;
                        case '3':
                            $msg = 'MSG||NO||The uploaded file was only partially uploaded';
                            return $msg;
                            break;
                        case '4':
                            $msg = 'MSG||NO||The uploded file TDP was not found';
                            return $msg;
                            break;
                        case '6':
                            $msg = 'MSG||NO||Missing a temporary folder';
                            return $msg;
                            break;
                        case '7':
                            $msg = 'MSG||NO||Failed to write file to disk';
                            return $msg;
                            break;
                        case '8':
                            $msg = 'MSG||NO||File upload stopped by extension';
                            return $msg;
                            break;
                        default:
                            return "MSG||NO||File upload Error";
                   }
                } elseif (!in_array($this->main->allowed($_FILES['filetdp']['name']), $this->arrext)) {
                    $msg = "MSG||NO||Upload File Surat TDP hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png";
                    return $msg;
                    break;
                } else if (empty($_FILES['filetdp']['tmp_name']) || $_FILES['filetdp']['tmp_name'] == 'none') {
                    $msg = 'MSG||NO||File TDP Upload tidak di temukan';
                    return $msg;
                    break;
                }
                if (file_exists($dirTDP) && is_dir($dirTDP)) {
                    $config['upload_path'] = $dirTDP;
                } else {
                    if (mkdir($dirTDP, 0777, true)) {
                        if (chmod($dirTDP, 0777)) {
                            $config['upload_path'] = $dirTDP;
                        }
                    }
                }
                if ($_FILES['filetdp'] != null) {
                    $config4['upload_path'] = $dirTDP;
                    $config4['allowed_types'] = 'jpg|jpeg|pdf|png';
                    $config4['remove_spaces'] = TRUE;
                    $ext4 = pathinfo($_FILES['filetdp']['name'], PATHINFO_EXTENSION);
                    $config4['file_name'] = "TDP-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext4;
                    $this->load->library('upload');
                    $this->upload->initialize($config4);
                    $this->upload->display_errors('', '');

                    $this->upload->do_upload("filetdp");
                    $dataTDP = $this->upload->data("filetdp");
                }
                $tmptdp = $dirTDP . $config4['file_name'];
                $arrregis['file_tdp'] = $tmptdp;
            }

            if($_FILES['filektp_pj']['name'] != null){
	            if (!empty($_FILES['filektp_pj']['error'])) {
	                if ($_FILES['filektp_pj']['error'] == 1) {
	                    $msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
	                    return $msg;
	                    break;
	                } elseif ($_FILES['filektp_pj']['error'] == 2) {
	                    $msg = 'MSG||NO||The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
	                    return $msg;
	                    break;
	                } elseif ($_FILES['filektp_pj']['error'] == 3) {
	                    $msg = 'MSG||NO||The uploaded file was only partially uploaded';
	                    return $msg;
	                    break;
	                } elseif ($_FILES['filektp_pj']['error'] == 4) {
	                    $msg = 'MSG||NO||The uploded file KTP was not found';
	                    return $msg;
	                    break;
	                } elseif ($_FILES['filektp_pj']['error'] == 5) {
	                    $msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
	                    return $msg;
	                    break;
	                } elseif ($_FILES['filektp_pj']['error'] == 6) {
	                    $msg = 'MSG||NO||Missing a temporary folder';
	                    return $msg;
	                    break;
	                } elseif ($_FILES['filektp_pj']['error'] == 7) {
	                    $msg = 'MSG||NO||Failed to write file to disk';
	                    return $msg;
	                    break;
	                } elseif ($_FILES['filektp_pj']['error'] == 8) {
	                    $msg = 'MSG||NO||File upload stopped by extension';
	                    return $msg;
	                    break;
	                }
	                //if success validate again if file null and not compatible format with jpg, pdf, or png
	            } else if (!in_array($this->main->allowed($_FILES['filektp_pj']['name']), $this->arrext)) {
	                $msg = "MSG||NO||Upload File KTP PJ hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png";
	                return $msg;
	                break;
	            } else if (empty($_FILES['filektp_pj']['tmp_name']) || $_FILES['filektp_pj']['tmp_name'] == 'none') {
	                $msg = 'MSG||NO||File Surat Kuasa Upload tidak di temukan';
	                return $msg;
	                break;
	            }

	            //make a directory if not have a directory 
	            if (file_exists($dirKTP_PJ) && is_dir($dirKTP_PJ)) {
	                $config['upload_path'] = $dirKTP_PJ;
	            } else {
	                if (mkdir($dirKTP_PJ, 0777, true)) {
	                    if (chmod($dirKTP_PJ, 0777)) {
	                        $config['upload_path'] = $dirKTP_PJ;
	                    }
	                }
	            }

	            //if all validation success, this is uploading process! with upload library
	            if ($_FILES['filektp_pj'] != null) {
	                $configpj['upload_path'] = $dirKTP_PJ;
	                $configpj['allowed_types'] = 'jpg|jpeg|pdf|png';
	                $configpj['remove_spaces'] = TRUE;
	                $ext1 = pathinfo($_FILES['filektp_pj']['name'], PATHINFO_EXTENSION);
	                $configpj['file_name'] = "KTP_PJ-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext1;
	                $this->load->library('upload');
	                $this->upload->initialize($configpj);
	                $this->upload->display_errors('', '');

	                $this->upload->do_upload("filektp_pj");
	                $dataKTP_PJ = $this->upload->data("filektp_pj");
	            }
	            $tmpktp = $dirKTP_PJ . $configpj['file_name'];
                $arrregis['file_ktp'] = $tmpktp;
	        }

            if ($_FILES['filesk']['name'] != '') {
                if (!empty($_FILES['filesk']['error']) ) {
                    if ($_FILES['filesk']['error'] == 1) {
                        $msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
                        return $msg;
                        break;
                    } elseif ($_FILES['filesk']['error'] == 2) {
                        $msg = 'MSG||NO||The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                        return $msg;
                        break;
                    } elseif ($_FILES['filesk']['error'] == 3) {
                        $msg = 'MSG||NO||The uploaded file was only partially uploaded';
                        return $msg;
                        break;
                    } elseif ($_FILES['filesk']['error'] == 4) {
                        $msg = 'MSG||NO||The uploded file was not found ininininin';
                        return $msg;
                        break;
                    } elseif ($_FILES['filesk']['error'] == 5) {
                        $msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
                        return $msg;
                        break;
                    } elseif ($_FILES['filesk']['error'] == 6) {
                        $msg = 'MSG||NO||Missing a temporary folder';
                        return $msg;
                        break;
                    } elseif ($_FILES['filesk']['error'] == 7) {
                        $msg = 'MSG||NO||Failed to write file to disk';
                        return $msg;
                        break;
                    } elseif ($_FILES['filesk']['error'] == 8) {
                        $msg = 'MSG||NO||File upload stopped by extension';
                        return $msg;
                        break;
                    }
                }elseif (!in_array($this->main->allowed($_FILES['filesk']['name']), $this->arrext)) {
                    $msg = "MSG||NO||Upload File Surat Kuasa hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png";
                    return $msg;
                    break;
                } elseif (empty($_FILES['filesk']['tmp_name']) || $_FILES['filesk']['tmp_name'] == 'none') {
                    $msg = 'MSG||NO||File Surat Kuasa Upload tidak di temukan';
                    return $msg;
                    break;
                }elseif (empty($_FILES['filesk']['tmp_name']) || $_FILES['filesk']['tmp_name'] == 'none') {
                    $msg = 'MSG||NO||File Surat Kuasa Upload tidak di temukan';
                    return $msg;
                    break;
                }

                if (file_exists($dirSK) && is_dir($dirSK)) {
                    $config['upload_path'] = $dirSK;
                } else {
                    if (mkdir($dirSK, 0777, true)) {
                        if (chmod($dirSK, 0777)) {
                            $config['upload_path'] = $dirSK;
                        }
                    }
                }
                
                if ($_FILES['filesk'] != null) {
                    $config3['upload_path'] = $dirSK;
                    $config3['allowed_types'] = 'jpg|jpeg|pdf|png';
                    $config3['remove_spaces'] = TRUE;
                    $ext3 = pathinfo($_FILES['filesk']['name'], PATHINFO_EXTENSION);
                    $config3['file_name'] = "SK-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext3;
                    $this->load->library('upload');
                    $this->upload->initialize($config3);
                    $this->upload->display_errors('', '');

                    $this->upload->do_upload("filesk");
                    $dataSK = $this->upload->data("filesk");
                    $tmptsk = $dirSK . $config3['file_name'];
                    $arrregis['file_kuasa'] = $tmptsk;
                }
            }
            //print_r($arrregis);die();
            $arrregis['status'] = '03';
            $arrregis['created'] = 'GETDATE()';
            $this->db->trans_begin();
            $this->db->where('id', $this->newsession->userdata('trader_id'));
            $this->db->update('m_trader', $arrregis);//print_r($this->db->last_query());die();

            $kuasa = array('file_kuasa' => $arrregis['file_kuasa']);
            $this->db->where('trader_id', $this->newsession->userdata('trader_id'));
            $this->db->update('t_user', $kuasa);//print_r($this->db->last_query());die();  

            if ($this->db->affected_rows() > 0) {
                $resreg = TRUE;
            }

            if ($this->db->trans_status() === FALSE || $resreg == FALSE) {
                //die("rollback");
                $this->db->trans_rollback();
                $msg = "MSG||NO||Update Gagal.";
            } else {

                $this->db->trans_commit();
                
//                $log = $this->input->post('log');
//                $arrlog['json'] = $log;
//                $arrlog['nib'] = $arrregis['nib'];
//                $arrlog['date_request'] = 'GETDATE()';
//                $this->db->insert('m_oss', $arrlog);

                //mail from system part!                                
                $html = "";
                $domain = "sipt.kemendag.go.id";
                $html .= "Terima kasih telah Mengupdate profile perusahaan Anda dalam rangka mengaktifkan Kembali Hak Akses SIPT PDN <br>";
                $html .= "NPWP            	 	= " . $arrregis['npwp']. "<br>";
                $html .= "Nama Perushaaan 	 	= " . $arrregis['nm_perusahaan'] . "<br>";
                $html .= "Nama Pimpinan   	 	= " . $arrregis['na_pj'] . "<br>";
                $html .= "Tanggal Terbit TDP 	= " . $arrregis['tgl_tdp'] . "<br>";
                $html .= "Tanggal Akhir TDP 	= " . $arrregis['tgl_exp_tdp'] . "<br>";
                $html .= "Nomor TDP 			= " . $arrregis['no_tdp'] . "<br>";
                $html .= "Data anda akan diverifikasi untuk selanjutnya diberikan hak akses SIPT PDN<br>";
                $html .= "Silahkan mengunjungi website kami di http://sipt.kemendag.go.id<br>";
                $html .= "Direktorat Jenderal Perdagangan Dalam Negeri<br>";
                $html .= "Kementerian Perdagangan Republik Indonesia<br>";
                $mail = $arrregis['email_pj'] . ',' . $arrkoor['email_koor'];
                $this->main->send_email($mail, $html, 'Pembaharuan Data Hak Akses SIPT PDN');

                $msg = "MSG||YES||Update Berhasil.||site_url('setting/edit_per')";
            }

            return $msg;
        }
	}
}
?>