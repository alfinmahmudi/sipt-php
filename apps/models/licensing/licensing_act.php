<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Licensing_act extends CI_Model{
	
	
	public function get_licensing(){
		if($this->newsession->userdata('_LOGGED')){
			$sql = "SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT'";
			$arrdata['direktorat'] = $this->main->set_combobox($sql,"kode","uraian", TRUE);
			$m = $this->main->get_result($sql);
			if($m){
				foreach($sql->result_array() as $r){
					$dir[] = $r;
				}
			}
			//print_r($dir);die();
			$i=0;
				foreach($dir as $b){
					$q = "select a.id,a.nama_izin, b.uraian from m_izin a left JOIN m_reff b on a.direktorat_id= b.kode where a.direktorat_id='".$b['kode']."' AND b.jenis='DIREKTORAT' AND a.aktif=1 ";
					$j=$this->main->get_result($q);
					if($j){
						foreach($q->result_array() as $w){
							$n[$b['kode']][] = $w;
						}	
					}	
				$i++;
				}
				
			$arrdata['dir'] = $dir;	
			$arrdata['izin'] = $n;
			$arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'","kode","uraian");
			$query = "SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_GARANSI'";
			$arrdata['garansi'] = $this->db->query($query)->result_array();
			return $arrdata;
		}
	}

	public function view_licensing($p=''){
		if($this->newsession->userdata('_LOGGED')){
			$sql = "SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT'";
			$arrdata['direktorat'] = $this->main->set_combobox($sql,"kode","uraian", TRUE);
			$m = $this->main->get_result($sql);
			if($m){
				foreach($sql->result_array() as $r){
					$dir[] = $r;
				}
			}
			//print_r($dir);die();
			$i=0;
				foreach($dir as $b){
					$q = "select a.id,a.nama_izin, b.uraian from m_izin a left JOIN m_reff b on a.direktorat_id= b.kode where a.direktorat_id='".$b['kode']."' AND b.jenis='DIREKTORAT' AND a.aktif=1 ";
					$j=$this->main->get_result($q);
					if($j){
						foreach($q->result_array() as $w){
							$n[$b['kode']][] = $w;
						}	
					}	
				$i++;
				}
				
			$arrdata['dir'] = $dir;	
			$arrdata['izin'] = $n;
			if ($p == 'sla') {
				$arrdata['act'] = site_url('sla/get_sla');
				$arrdata['sla'] = 'sla';
                                $arrdata['judul'] = 'Pengaturan SLA';
			}else{
				$arrdata['act'] = site_url('licensing/get_per');
                                $arrdata['judul'] = 'Atur Persyaratan';
			}
			$arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'","kode","uraian");
			return $arrdata;
		}
	}
	
	public function view(){
		if($this->newsession->userdata('_LOGGED')){
			$value = explode("|", $this->input->post('dokumen'));
			$direktorat = $value[0];
			$kd_izin = $value[1];
			$tipe_permohonan = $this->input->post('jenis');
			$garansi = $this->input->post('garansi');
			if ($tipe_permohonan == '01') {
				$addCon = "AND C.baru = '1'";
			}elseif ($tipe_permohonan=='02') {
				$addCon = "AND C.perubahan = '1'";
			}elseif ($tipe_permohonan=='03') {
				$addCon = "AND C.perpanjangan = '1'";
			}
			if ($garansi == '01') {
				$addGar = "AND C.tipe_izin = '01'";
			}elseif ($garansi == '02') {
				$addGar = "AND C.tipe_izin = '02'";
			}
			$query = "SELECT CAST(D.keterangan AS VARCHAR(MAX)) AS 'Dokumen', B.uraian AS 'Direktorat', 
					  C.baru, C.perpanjangan, C.perubahan, C.kategori, C.tipe, E.uraian AS 'Kategori', F.uraian AS 'Syarat', C.izin_id, C.dok_id
					  FROM m_izin A LEFT JOIN m_reff B ON A.direktorat_id = B.kode AND B.jenis = 'DIREKTORAT'
					  LEFT JOIN m_dok_izin C ON A.id = C.izin_id
					  LEFT JOIN m_dok D ON C.dok_id = D.id
					  LEFT JOIN m_reff E ON C.kategori = E.kode AND E.jenis = 'KATEGORI_DOKIZIN'
					  LEFT JOIN m_reff F ON C.tipe = F.kode AND F.jenis = 'TIPE_DOKIZIN'
					  WHERE A.direktorat_id = '".$direktorat."' AND C.izin_id = '".$kd_izin."' ".$addCon." ".$addGar." ORDER BY 6,7";
			//print_r($query);die();
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['sess'][] = $row;
				}
				return $arrdata;
			}
		}
	}
	
	public function document(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
			$arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
			$query = "SELECT A.direktorat_id, A.id, G.nama_izin AS 'Dokumen Perizinan', CAST(D.keterangan AS VARCHAR(MAX)) AS 'Dokumen', B.uraian AS 'Direktorat', E.uraian AS 'Kategori', F.uraian AS 'Syarat'
					  FROM m_izin A LEFT JOIN m_reff B ON A.direktorat_id = B.kode AND B.jenis = 'DIREKTORAT'
					  LEFT JOIN m_dok_izin C ON A.id = C.izin_id
					  LEFT JOIN m_dok D ON C.dok_id = D.id
					  LEFT JOIN m_reff E ON C.kategori = E.kode AND E.jenis = 'KATEGORI_DOKIZIN'
					  LEFT JOIN m_reff F ON C.tipe = F.kode AND F.jenis = 'TIPE_DOKIZIN'
					  LEFT JOIN m_izin G ON A.direktorat_id = G.direktorat_id";
			$table->title("");
			$table->columns(array("A.direktorat_id", "A.id", "G.nama_izin", "CAST(D.keterangan AS VARCHAR(MAX))", "B.uraian", "E.uraian", "F.uraian"));
			$this->newtable->width(array('Dokumen Perizinan' => 350 , 'Dokumen' => 200 , 'Direktorat' => 200, "Kategori" => 130, "Syarat" => 70));
			$this->newtable->search(array(array("A.direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url().'get/cb/set_dokumen/')),
										  array("A.id", 'Dokumen Perizinan', array('ARRAY', array())),
										  array("CAST(D.keterangan AS VARCHAR(MAX))","Nama Dokumen")
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/view/status");
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("direktorat_id"));
			$table->hiddens(array("direktorat_id","id"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->tbtarget("tb_requirements");
			$table->menu(array('Tambah' => array('GET', site_url().'setting/view/document/new', '0', 'home', 'modal'),
							   'Edit' => array('GET', site_url().'setting/view/document/new', '1', 'fa fa-pencil-square-o'),
							   'Hapus' => array('POST', site_url().'setting/document/new', 'N', 'fa fa-trash-o', 'isngajax')));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Persyaratan Dokumen Perizinan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	public function add_new($act, $isajax){
		if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $resper = FALSE;
            $arrdata = $this->input->post('persyaratan');
            $arrper['kode'] = $arrdata['kode'];
            $arrper['keterangan'] = $arrdata['keterangan'];
            $arrper['created'] = 'GETDATE()';
            $arrper['created_user'] = $this->newsession->userdata('nama');//print_r($arrper);die();
            if ($act == 'save') {
            	$this->db->trans_begin();
            	$this->db->insert('m_dok', $arrper);
            	if ($this->db->affected_rows() > 0) {
            		$logu = array('aktifitas' => 'Menambahkan Master Persyaratan Baru, dengan Kode '. $arrper['kode'] . ' dan Keterangan '. $arrper['keterangan'],
                        'url' => '{c}' . site_url() . 'post/licensing/persyaratan/save' . ' {m} models/licensing/licensing_act {f} add_new($act, $isajax)');
                    $this->main->set_activity($logu);
            		$resper = TRUE;
            		$msg = "MSG||YES||Data berhasil disimpan||".site_url('licensing/view/persyaratan');
            	}
            	if ($this->db->trans_status() === FALSE || $resper == FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            }else{
            	$this->db->trans_begin();
            	$this->db->where('id', $arrdata['id']);
            	$this->db->update('m_dok', $arrper);
            	if ($this->db->affected_rows() > 0) {
            		$logu = array('aktifitas' => 'Mengupdate Master Persyaratan, dengan Kode '. $arrper['kode'] . ' dan Keterangan '. $arrper['keterangan'],
                        'url' => '{c}' . site_url() . 'post/licensing/persyaratan/save' . ' {m} models/licensing/licensing_act {f} add_new($act, $isajax)');
                    $this->main->set_activity($logu);
            		$resper = TRUE;
            		$msg = "MSG||YES||Data berhasil disimpan||".site_url('licensing/view/persyaratan');
            	}
            	if ($this->db->trans_status() === FALSE || $resper == FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            }

			return $msg;
		}
	}

	public function get_new($id, $post){
		if($this->newsession->userdata('_LOGGED')){
			if ($id != '') {
				$sql = "SELECT a.id, a.kode, a.keterangan FROM m_dok a WHERE a.id = '".$id."' ";
				$sess = $this->db->query($sql)->row_array();
				$arrdata['sess'] = $sess;
				$arrdata['act'] = site_url('post/licensing/persyaratan/update');  
			}else{
				$arrdata['act'] = site_url('post/licensing/persyaratan/save');
			}
			return $arrdata;
		}
	}

	public function get_per(){
		if($this->newsession->userdata('_LOGGED')){
			$izin_id = explode('|', $this->input->post('dokumen'));
			$sql = "SELECT b.id, a.id as dok_id,a.kode, a.keterangan, b.urutan,b.tipe, b.baru, b.multi, b.perpanjangan, b.perubahan, b.kategori
					FROM m_dok a
					LEFT JOIN m_dok_izin b ON b.dok_id = a.id
					WHERE b.izin_id = '".$izin_id[1]."' ";//print_r($sql);die();
			$sess = $this->db->query($sql)->result_array();
			$arrdata['nama_izin'] = $this->db->query("SELECT nama_izin FROM m_izin where id = '".$izin_id[1]."' ")->row_array();
			$query = "SELECT a.id, a.kode + ' - ' + CAST(a.keterangan AS varchar) as keterangan
						FROM m_dok a";
			$arrdata['arrket'] = $this->db->query($query)->result_array();
			$q = "SELECT kode, uraian FROM m_reff where jenis = 'KATEGORI_DOKIZIN' ";
			$arrdata['arrkat'] = $this->db->query($q)->result_array();
			// $arrdata['keterangan'] = $this->main->set_combobox($query,"id","keterangan", TRUE);
			// $arrdata['kategori'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff where jenis = 'KATEGORI_DOKIZIN' ","kode","uraian", TRUE);
			$arrdata['kategori'] = $this->db->query("SELECT kode, uraian FROM m_reff where jenis = 'KATEGORI_DOKIZIN'")->result_array();//print_r($arrdata['kategori']);die();
			$arrdata['keterangan'] = $this->db->query($query)->result_array();
			$arrdata['sess'] = $sess;
			$arrdata['act'] = site_url('licensing/set_per/'.$izin_id[1]);
			return $arrdata;
		}
	}

	public function set_per($id, $isajax){
		if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $resper = FALSE;
			$arrper = $this->input->post('persyaratan');//print_r($arrper);die();
			//print_r($arrper);die();
			$sql = "SELECT b.id
					FROM m_dok a
					LEFT JOIN m_dok_izin b ON b.dok_id = a.id
					WHERE b.izin_id = '".$id."' ";
			$arrid_dok = $this->db->query($sql)->result_array();
			$i = 0;
			
			foreach ($arrper as $key) {
				$arrsave['izin_id'] = $id;
				$arrsave['dok_id'] = $key['keterangan'];
				$arrsave['tipe'] = $key['tipe'];
				$arrsave['multi'] = $key['multi'];;
				$arrsave['baru'] = $key['baru'];
				$arrsave['urutan'] = $key['urutan'];
				if ($key['perpanjangan'] == '') {
					$arrsave['perpanjangan'] = '0';
				}else{
					$arrsave['perpanjangan'] = $key['perpanjangan'];
				}
				if ($key['baru'] == '') {
					$arrsave['baru'] = '0';
				}else{
					$arrsave['baru'] = $key['baru'];
				}
				if ($key['multi'] == '') {
					$arrsave['multi'] = '0';
				}else{
					$arrsave['multi'] = $key['multi'];
				}
				if ($key['perubahan'] == '') {
					$arrsave['perubahan'] = '0';
				}else{
					$arrsave['perubahan'] = $key['perubahan'];	
				}
				if ($key['kategori'] == '') {
					$arrsave['kategori'] = '0';
				}else{
					$arrsave['kategori'] = $key['kategori'];	
				}
				if ($key['tipe'] == '') {
					$arrsave['tipe'] = '0';
				}else{
					$arrsave['tipe'] = $key['tipe'];	
				}
				if ($key['id'] == '') {
					$this->db->trans_begin();
					$this->db->insert('m_dok_izin', $arrsave);
					if ($this->db->affected_rows() > 0) {
						$logu = array('aktifitas' => 'Mendambahkan Persyaratan ke m_dok_izin',
                        'url' => '{c}' . site_url() . 'licensing/persyaratan/set_per' . ' {m} models/licensing/licensing_act {f} set_per($id, $isajax)');
                    	$this->main->set_activity($logu);
                    	$resper = TRUE;
                    	$msg = "MSG||YES||Data berhasil disimpan||".site_url('licensing/view/add_requirements');
					}
					if ($this->db->trans_status() === FALSE || $resper == FALSE) {
                    	$this->db->trans_rollback();
	                } else {
	                    $this->db->trans_commit();
	                }
				}elseif($key != ''){
					$this->db->trans_begin();
					$this->db->where('id', $key['id']);
					$this->db->update('m_dok_izin', $arrsave);print_r($this->db->last_query());
					if ($this->db->affected_rows() > 0) {
						$logu = array('aktifitas' => 'Mengupdate Persyaratan ke m_dok_izin',
                        'url' => '{c}' . site_url() . 'licensing/persyaratan/set_per' . ' {m} models/licensing/licensing_act {f} set_per($id, $isajax)');
                    	$this->main->set_activity($logu);
                    	$resper = TRUE;
                    	$msg = "MSG||YES||Data berhasil disimpan||".site_url('licensing/view/add_requirements');
					}
					if ($this->db->trans_status() === FALSE || $resper == FALSE) {
                    	$this->db->trans_rollback();
	                } else {
	                    $this->db->trans_commit();
	                }
				}
				$i++;
			}
			return $msg;
		}
	}

	public function del_lic(){
		if($this->newsession->userdata('_LOGGED')){
			$id = $_POST['id'];
			$this->db->delete('m_dok_izin', array('id' => $id));
		}
	}

	public function del_persyaratan($id){
		if($this->newsession->userdata('_LOGGED')){
			$this->db->trans_begin();
			foreach ($id as $data) {
            	$this->db->delete('m_dok', array('id' => $data));
        	}
			if ($this->db->affected_rows() > 0) {
            		$logu = array('aktifitas' => 'Mendelete Master Persyaratan',
                        'url' => '{c}' . site_url() . 'post/licensing/persyaratan/save' . ' {m} models/licensing/licensing_act {f} del_persyaratan($act, $isajax)');
                    $this->main->set_activity($logu);
            	}
			if ($this->db->trans_status === FALSE) {
            	$this->db->trans_rollback();
            	$msg = "MSG#Proses Gagal#refresh";
	        } else {
	            $this->db->trans_commit();
            	$msg = "MSG#Proses Berhasil#refresh";
	        }
			return $msg;
		}
	}

	public function master_view(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
			$arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
			$query = "SELECT a.id, a.kode as Kode, a.keterangan as Keterangan FROM m_dok a";
			$table->title("");
			$table->columns(array("a.id", "a.kode", "a.keterangan"));
			$this->newtable->search(array(array("a.kode","Kode Dokumen"),array("a.keterangan","Keterangan")
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/view/persyaratan");
			$table->orderby(1);
			$table->sortby("DESC");
			$table->keys(array("id"));
			$table->hiddens(array("id"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->use_ajax(TRUE);
            $table->postmethod(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->tbtarget("tb_requirements");
			$table->menu(array('Tambah' => array('GET', site_url().'licensing/persyaratan', '0', 'home', 'modal'),
							   'Edit' => array('BOOTSTRAPDIALOG', site_url().'licensing/dialog_persyaratan/'.$id.'/', '1', 'fa fa-pencil-square-o'),
							   'Hapus' => array('POST', site_url().'licensing/delete_persyaratan', 'N', 'fa fa-trash-o', 'isngajax')));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Tambah Persyaratan Dokumen Perizinan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

}
?>