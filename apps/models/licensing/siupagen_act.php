<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Siupagen_act extends CI_Model {

    var $ineng = "";

    function get_first($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata = array();
            $idprop = $this->newsession->userdata('kdprop');
            $idkab = $this->newsession->userdata('kdkab');
            $idkec = $this->newsession->userdata('kdkec');
            $idkel = $this->newsession->userdata('kdkel');//print_r($idkel);die();
            $tipe_perusahaan = $this->newsession->userdata('tipe_perusahaan');
            
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '".$idprop."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '".$idkab."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '".$idkec."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '".$idkel."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['direktorat'] = $dir;
            $arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian", TRUE);
            $arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN' AND kode = '".$tipe_perusahaan."' ", "kode", "uraian", TRUE);
            $arrdata['produk'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'PRODUK'", "kode", "uraian", TRUE);
            $arrdata['produksi'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'PRODUKSI_KEAGENAN'", "kode", "uraian", TRUE);
            $arrdata['jenis_agen'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_KEAGENAN'", "kode", "uraian", TRUE);
            $arrdata['kab2'] = $this->main->set_combobox("SELECT id, nama FROM m_kab ORDER BY 2", "id", "nama", TRUE);
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;

            if ($id == "") {
                $arrdata['act'] = site_url('post/licensing/siupagen_act/first/save');
                //$type = hashids_decrypt($type,_HASHIDS_,6);
                $arrdata['sess']['tipe_permohonan'] = $type;
                $arrdata['sess']['kd_izin'] = $doc;
            } else {
                $arrdata['act'] = site_url('post/licensing/siupagen_act/first/update');
                $query = "SELECT id, kd_izin, produksi, produk, jenis_agen, tipe_permohonan, npwp, tipe_perusahaan, nm_perusahaan, almt_perusahaan, kdprop, kdkab, kdkec, kdkel, kdpos, fax, telp, bidang_usaha, pemasaran,id_permohonan_lama, no_izin_lama, tgl_izin_lama, tgl_izin_exp_lama
						,pemasaran FROM t_siup_agen WHERE id = '" . $id . "'";
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    //$arrdata['pemasaran'] = explode(';', $row['pemasaran']);
                    $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec'] . "' ORDER BY 2", "id", "nama", TRUE);
                }
            }
            return $arrdata;
        }
    }

    function set_first($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $txt = "Surat Tanda Pendaftaran Agen atau Distributor Barang atau Jasa Dalam atau Luar Negeri";

            $msg = "MSG||NO||Data Permohonan " . $txt . " gagal disimpan";
            $ressiup = FALSE;
            $arrsiup = $this->main->post_to_query($this->input->post('AGEN'));
            $dir = $this->input->post('direktorat');
            $doc = $arrsiup['kd_izin'];
            $type = $arrsiup['tipe_permohonan'];
            if ($act == "save") {
                $arrsiup['no_aju'] = $this->main->set_aju();
                $arrsiup['fl_pencabutan'] = '0';
                $arrsiup['status'] = '0000';
                $arrsiup['tgl_aju'] = 'GETDATE()';
                $arrsiup['trader_id'] = $this->newsession->userdata('trader_id');
                $arrsiup['created'] = 'GETDATE()';
                $arrsiup['created_user'] = $this->newsession->userdata('username');
                $this->db->trans_begin();
                if (($type != '01') && ($arrsiup['id_permohonan_lama'] != "")) {
                    if ($type == '03')
                        $arrsiup['fl_pembaharuan'] = "1";
                    $query_old_doc = "SELECT a.tipe_perusahaan_prod, a.nm_perusahaan_prod, a.almt_perusahaan_prod, a.tgl_pendirian_prod, 
									  a.kdprop_prod, a.kdkab_prod, a.kdkec_prod, a.kdkel_prod, a.telp_prod, a.fax_prod,
									  a.tipe_perusahaan_supl, a.nm_perusahaan_supl, a.almt_perusahaan_supl, a.tgl_pendirian_supl,a.kdprop_supl, a.kdkab_supl, a.kdkec_supl, a.kdkel_supl, a.telp_supl, a.fax_supl, 
									  a.identitas_pj, a.noidentitas_pj, a.nama_pj, a.jabatan_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, 
									  a.alamat_pj, a.kdprop_pj, a.kdkab_pj, a.kdkel_pj, a.kdkec_pj,a.telp_pj, a.fax_pj, a.pegawai_lokal, a.pegawai_asing, a.bidang_usaha, a.pemasaran
									  FROM t_siup_agen a WHERE a.id = " . $arrsiup['id_permohonan_lama'] . " AND a.kd_izin = " . $arrsiup['kd_izin'];
                    $data_old = $this->main->get_result($query_old_doc);
                    if ($data_old) {
                        foreach ($query_old_doc->result_array() as $keys) {
                            $jml = count($keys);
                            $array_keys = array_keys($keys);
                            $array_values = array_values($keys);
                            for ($i = 0; $i < $jml; $i++) {
                                $arrsiup[$array_keys[$i]] = $array_values[$i];
                            }
                        }
                    }
                }
                //print_r($arrsiup);die();
                $exec = $this->db->insert('t_siup_agen', $arrsiup);
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idredir = $this->db->insert_id();
                    $msg = "MSG||YES||Data Permohonan " . $txt . " berhasil disimpan. Silahkan lanjutkan mengisi data Barang||" . site_url() . 'licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $idredir;

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' dengan nomor permohonan : ' . $arrsiup['no_aju'],
                        'url' => '{c}' . site_url() . 'post/licensing/siupagen_act/first/save' . ' {m} models/licensing/siupagen_act {f} set_first($act, $ajax)');
                    $this->main->set_activity($logu);
                    $logi = array('kd_izin' => $arrsiup['kd_izin'],
                        'permohonan_id' => $idredir,
                        'keterangan' => 'Menambahkan daftar permohonan ' . $txt . ' dengan nomor permohonan : ' . $arrsiup['no_aju'],
                        'catatan' => '',
                        'status' => '0000',
                        'selisih' => 0);
                    $this->main->set_loglicensing($logi);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } elseif ($act == "update") {
                $id = $this->input->post('id');
                $this->db->where('id', $id);
                $this->db->update('t_siup_agen', $arrsiup);
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $msg = "MSG||YES||Data Permohonan " . $txt . " berhasil diupdate.||REFRESH";
                }
                return $msg;
            }
        }
    }

    function get_second($dir, $doc, $type, $id) {
        if ($this->newsession->userdata("_LOGGED")) {
            $arrdata = array();
            $sql = "select id,jenis,hs,merk from t_siup_agen_detil where permohonan_id='" . $id . "'";
            $data = $this->main->get_result($sql);
            $arrdata['nama_izin'] = "Surat Tanda Pendaftaran Agen atau Distributor Barang atau Jasa Dalam atau Luar Negeri";
            $arrdata['direktorat'] = $dir;
            $arrdata['kd_izin'] = $doc;
            $arrdata['tipe_permohonan'] = $this->main->get_uraian("select tipe_permohonan from t_siup_agen where id='" . $id . "'", "tipe_permohonan");
            $arrdata['permohonan_id'] = $id;
            $arrdata['act'] = site_url('post/licensing/siupagen_act/second/save');
            if ($data) {
                $arrdata['act'] = site_url('post/licensing/siupagen_act/second/update');
                foreach ($sql->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
            }
            //print_r($arrdata);die();
            return $arrdata;
        }
    }

    function set_second($act, $isajax) {
        if ($this->newsession->userdata("_LOGGED")) {
            $detil = $this->main->post_to_query($this->input->post('detil'));
            $permohonan_id = $this->input->post("permohonan_id");
            $detil['permohonan_id'] = $permohonan_id;
            $detil['created'] = "GETDATE()";

            $direktorat = $this->input->post("direktorat");
            $txt = "Surat Tanda Pendaftaran Agen atau Distributor Barang atau Jasa Dalam atau Luar Negeri";
            $kd_izin = $this->input->post("kd_izin");
            $tipe_permohonan = $this->input->post("tipe_permohonan");
            $next = site_url() . 'licensing/form/third/' . $direktorat . '/' . $kd_izin . '/' . $tipe_permohonan . '/' . $permohonan_id;
            if ($act == "save") {
                $res = FALSE;
                $this->db->trans_begin();
                $this->db->insert("t_siup_agen_detil", $detil);
                if ($this->db->affected_rows() > 0) {
                    $res = TRUE;
                    $msg = "MSG||YES||Data Permohonan " . $txt . " berhasil disimpan. Silahkan lanjutkan mengisi data penanggung jawab||" . $next;
                }
                if ($this->db->trans_status() === FALSE || !$res) {
                    $this->db->trans_rollback();
                    echo $this->db->_error_message();
                    $msg = "MSG||NO||Data Permohonan " . $txt . " gagal di simpan";
                } else {
                    $this->db->trans_commit();
                }
            } elseif ($act == "update") {
                $msg = "MSG||NO||Data Permohonan " . $txt . " gagal di Update";
                $id = $this->input->post("id");
                $this->db->trans_begin();
                $res = FALSE;
                $detil = $this->input->post("detil");
                $this->db->where("id", $id);
                $this->db->update("t_siup_agen_detil", $detil);
                if ($this->db->affected_rows() > 0) {
                    $res = TRUE;
                    $msg = "MSG||YES||Data Permohonan " . $txt . " berhasil diupdate.||REFRESH";
                }
                if ($this->db->trans_status() === false || !$res) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            }
            if (!$isajax) {
                redirect(site_url());
            }
            return $msg;
        }
    }

    function get_third($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $jenis_identitas = $this->newsession->userdata('tipe_identitas');
            $id_user = $this->newsession->userdata('id');
            $idprop = $this->newsession->userdata('kdprop_pj');
            $idkab = $this->newsession->userdata('kdkab_pj');
            $idkec = $this->newsession->userdata('kdkec_pj');
            $idkel = $this->newsession->userdata('kdkel_pj');
            $jabatan_pj = $this->newsession->userdata('jabatan_pj');
            $arrdata['act'] = site_url('post/licensing/siupagen_act/third/save');
            $arrdata['direktorat'] = $dir;
            $arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '".$jenis_identitas."' and jenis = 'JENIS_IDENTITAS'","kode","uraian", TRUE);
            $arrdata['jabatan_pj'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JABATAN' AND kode = '".$jabatan_pj."' ","kode","uraian", TRUE);
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '".$idprop."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '".$idkab."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '".$idkec."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '".$idkel."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['urifirst'] = site_url('licensing/form/first/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            $query = "SELECT id, kd_izin, tipe_permohonan, identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, alamat_pj, kdprop_pj, kdkab_pj, kdkec_pj,kdkel_pj, telp_pj, fax_pj, bidang_usaha, pegawai_lokal,pegawai_asing
						FROM t_siup_agen WHERE id = '" . $id . "'";
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
                if ($row['identitas_pj']) {
                    $arrdata['act'] = site_url('post/licensing/siupagen_act/third/update');
                }
                $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            return $arrdata;
        }
    }

    function set_third($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (($act == "update") || ($act == "save")) {
                if (!$isajax) {
                    return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                    exit();
                }
                $msg = "MSG||NO||Data Penanggung Jawab gagal disimpan";
                $respj = FALSE;
                $arrpj = $this->main->post_to_query($this->input->post('pj'));
                $dir = $this->input->post('direktorat');
                $doc = $this->input->post('kd_izin');
                $type = $this->input->post('tipe_permohonan');
                $id = $this->input->post('id');
                $txt = "Surat Tanda Pendaftaran Agen atau Distributor Barang atau Jasa Dalam atau Luar Negeri";
                $this->db->where('id', $id);
                $this->db->update('t_siup_agen', $arrpj);
                if ($this->db->affected_rows() > 0) {
                    $respj = TRUE;
                    if ($act == "update") {
                        $msg = "MSG||YES||Data Penanggung Jawab berhasil diupdate.||REFRESH";
                    } else {
                        $msg = "MSG||YES||Data Penanggung Jawab berhasil disimpan. Silahkan lanjutkan mengisi data prinsipal produsen dan supplier||" . site_url() . 'licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id;
                    }
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Data Penanggung Jawab)',
                        'url' => '{c}' . site_url() . 'post/licensing/siupagen_act/second/update' . ' {m} models/licensing/siupagen_act {f} set_second($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                }
                return $msg;
            }
        }
    }

    function get_fourth($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                if ($type == '01') {
                    $addCon = "AND a.baru = '1'";
                } elseif ($type == '02') {
                    $addCon = "AND a.perpanjangan = '1'";
                } elseif ($type == '03') {
                    $addCon = "AND a.perubahan = '1'";
                }
                $query = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian, a.multi
						FROM m_dok_izin a 
						LEFT JOIN m_dok b ON b.id = a.dok_id
						LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
						WHERE a.kategori = '04' AND a.izin_id = '" . $doc . "' " . $addCon;
                $data_req = $this->main->get_result($query);
                // $arrdata['req'] = $query->result_array();
                $arrdata['id'] = $id;
                $arrdata['kd_izin'] = $doc;
                $arrdata['act'] = site_url('post/licensing/siupagen_act/fourth/save');
                $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
                $arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN'", "kode", "uraian", TRUE);//print_r($arrdata['tipe_perusahaan']);die();
                $arrdata['urisecond'] = site_url('licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
                $query = "SELECT a.id,a.kd_izin, a.tipe_permohonan, a.produksi, a.tipe_perusahaan_prod, a.nm_perusahaan_prod, a.almt_perusahaan_prod, a.tgl_pendirian_prod, a.kdprop_prod, a.kdkab_prod, a.kdkec_prod, a.kdkel_prod, a.telp_prod, a.fax_prod, a.tipe_perusahaan_supl, a.nm_perusahaan_supl, a.almt_perusahaan_supl, a.tgl_pendirian_supl, a.kdprop_supl, a.kdkab_supl, a.kdkec_supl, a.kdkel_supl, a.telp_supl, a.fax_supl
						  FROM t_siup_agen a WHERE a.id = '" . $id . "'";
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['kab_prod'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop_prod'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kec_prod'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab_prod'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel_prod'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec_prod'] . "' ORDER BY 2", "id", "nama", TRUE);

                    $arrdata['kab_supl'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop_supl'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kec_supl'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab_supl'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel_supl'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec_supl'] . "' ORDER BY 2", "id", "nama", TRUE);

                    $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
								'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
								FROM t_upload_syarat a
								LEFT join t_upload b on b.id = a.upload_id
								LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id 
								WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "' and c.kategori = '04' and a.detail_id is null";
                    //print_r($query_syarat);die();
                    $data_syarat = $this->main->get_result($query_syarat);
                    $arr = array();
                    if ($data_syarat) {
                        foreach ($query_syarat->result_array() as $keys) {
                            if (!isset($arr[$keys['dok_id']])) {
                                $arr[$keys['dok_id']] = array();
                            }
                            $arr[$keys['dok_id']][] = $keys;
                        }
                        $arrdata['sess_legal'] = $arr;
                        $arrdata['act'] = site_url('post/licensing/siupagen_act/fourth/update');
                    }
                }

                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $arrdata['nama_izin'] = $nama_izin;
                $arrdata['direktorat'] = $dir;
                return $arrdata;
            }
        }
    }

    function set_fourth($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (($act == "update") || ($act == "save")) {
                if (!$isajax) {
                    return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                    exit();
                }
                $msg = "MSG||NO||Data gagal disimpan";
                $respj = FALSE;
                $arrsiup = $this->main->post_to_query($this->input->post('prinsipal'));
                if ($this->input->post('produksi') == '02') {
                    $arrsiup['kdprop_supl'] = null;
                    $arrsiup['kdkab_supl'] = null;
                    $arrsiup['kdkec_supl'] = null;
                    $arrsiup['kdkel_supl'] = null;
                    $arrsiup['kdprop_prod'] = null;
                    $arrsiup['kdkab_prod'] = null;
                    $arrsiup['kdkec_prod'] = null;
                    $arrsiup['kdkel_prod'] = null;
                }
                $dir = $this->input->post('direktorat');
                $doc = $this->input->post('kd_izin');
                $type = $this->input->post('tipe_permohonan');
                $id = $this->input->post('id');
                //print_r($dir.'-'.$doc.'-'.$type.'-'.$id);die();
                $txt = "Surat Tanda Pendaftaran Agen atau Distributor Barang atau Jasa Dalam atau Luar Negeri";
                $arrreq = $this->input->post('REQUIREMENTS');
                $arrkeys = array_keys($arrreq);
                $id_upload_syarat = array();
                $id_upload_syarat = $arrreq['id'];
                $this->db->where('id', $id);
                $this->db->update('t_siup_agen', $arrsiup);
                if ($this->db->affected_rows() > 0) {
                    if ($act == "save") {
                        //print_r("expression");die();
                        for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                            $requirements = array('izin_id' => $doc,
                                'permohonan_id' => $id,
                                'created' => 'GETDATE()',
                                'created_user' => $this->newsession->userdata('username'));
                            for ($j = 0; $j < count($arrkeys); $j++) {
                                $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                            }
                            unset($requirements['id']);
                            //print_r($requirements);
                            $this->db->insert('t_upload_syarat', $requirements);
                        }
                        if ($this->db->affected_rows() > 0) {
                            $respj = TRUE;
                            $msg = "MSG||YES||Data Prinsipal Produsen dan Supplier berhasil disimpan. Silahkan lanjutkan mengisi data persyaratan||" . site_url() . 'licensing/form/fifth/' . $dir . '/' . $doc . '/' . $type . '/' . $id;

                            /* Log User dan Log Izin */
                            $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Data Prinsipal Produsen dan Supplier)',
                                'url' => '{c}' . site_url() . 'post/licensing/siupagen_act/third/update' . ' {m} models/licensing/siupagen_act {f} set_third($act, $isajax)');
                            $this->main->set_activity($logu);
                            /* Akhir Log User */
                        }
                        if ($this->db->trans_status() === FALSE || !$respj) {
                            $this->db->trans_rollback();
                        } else {
                            $this->db->trans_commit();
                        }
                    } else {

                        for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                            $requirements = array('izin_id' => $doc,
                                'permohonan_id' => $id,
                                'created' => 'GETDATE()',
                                'created_user' => $this->newsession->userdata('username'));
                            for ($j = 0; $j < count($arrkeys); $j++) {
                                $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                            }
                            $IDX = $requirements['id'];
                            //print_r($requirements);
                            unset($requirements['id']);
                            $this->db->where('id', $IDX);
                            $this->db->where('permohonan_id', $id);
                            $this->db->where('izin_id', $doc);
                            $this->db->update('t_upload_syarat', $requirements);
                        }//die();
                        if ($this->db->affected_rows() > 0) {
                            $respj = TRUE;
                            /* Log User dan Log Izin */
                            $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Update - Data Prinsipal Produsen dan Supplier)',
                                'url' => '{c}' . site_url() . 'post/licensing/siupagen_act/fifth/update' . ' {m} models/licensing/siupagen_act {f} set_third($act, $isajax)');
                            $this->main->set_activity($logu);
                            /* Akhir Log User */
                            $msg = "MSG||YES||Data Prinsipal Produsen dan Supplier berhasil diupdate.||REFRESH";
                        }

                        if ($this->db->trans_status() === FALSE || !$respj) {
                            $this->db->trans_rollback();
                        } else {
                            $this->db->trans_commit();
                        }
                    }
                }
                return $msg;
            }
        }
    }

    function get_fifth($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            $arrdata['izin_id'] = $doc;
            $arrdata['direktorat'] = $dir;
            $arrdata['type'] = $type;
            $arrdata['permohonan_id'] = $id;
            $arrdata['urithird'] = site_url('licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            /* $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
              '<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
              FROM t_upload_syarat a
              LEFT join t_upload b on b.id = a.upload_id
              LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id
              WHERE a.izin_id = '".$doc."' and a.permohonan_id = '".$id."' and c.kategori = '01' and a.detail_id is null"; */
            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id,
                             case when c.multi = 1 then '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor else '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor end AS nomor,
                              b.tgl_dok, b.tgl_exp, b.penerbit_dok,
                             '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
                             FROM t_upload_syarat a 
                             LEFT join t_upload b on b.id = a.upload_id
                             LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id
                             WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "' and c.kategori = '01' and a.detail_id is null";
            $data = $this->main->get_result($query_syarat);
            $arr = array();
            $query = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian, a.multi
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.kategori = '01' and a.izin_id = '" . $doc . "' " . $addCon . " order by 5 desc";
            $data_req = $this->main->get_result($query);
            $temp = $query->result_array();
			$arrdata['req'] = $temp;

            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess'] = $arr;
                $arrdata['act'] = site_url('post/licensing/siupagen_act/fifth/update');
            } else {
                 foreach ($temp as $datadt) {
                    $query_syarat2 = "  SELECT top 1 b.id as 'upload_id', b.tipe_dok,  CASE WHEN a.multi = 1 THEN ' ' + b.nomor ELSE ' ' + b.nomor END AS nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
					'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View' 
                                            FROM t_upload b left join m_dok_izin a on b.tipe_dok = a.dok_id 
					where a.izin_id = '".$doc."' and b.trader_id = '" . $this->newsession->userdata('trader_id') . "'and b.tipe_dok = '" . $datadt['dok_id'] . "'  " . $addCon . " order by tgl_dok desc;";
                
                    $data2 = $this->main->get_result($query_syarat2);
                    if ($data2) {
                        foreach ($query_syarat2->result_array() as $keys) {
                            if (!isset($arr[$keys['tipe_dok']])) {
                                $arr[$keys['tipe_dok']] = array();
                            }
                            $arr[$keys['tipe_dok']][] = $keys;
                        }
                    }
                }
                $arrdata['sess'] = $arr;
                $arrdata['baru'] = true;
                $arrdata['act'] = site_url('post/licensing/siupagen_act/fifth/save');
            }
            return $arrdata;
        }
    }

    function set_fifth($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $ressiup = FALSE;
            $arrsiup = $this->main->post_to_query($this->input->post('SIUPMB'));
            $arrreq = $this->input->post('REQUIREMENTS');
            $arrkeys = array_keys($arrreq);
            $id_upload_syarat = array();
            $id_upload_syarat = $arrreq['id'];
            $txt = "Surat Tanda Pendaftaran Agen atau Distributor Barang atau Jasa Dalam atau Luar Negeri";
            if ($act == "save") {
                for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                    $requirements = array('izin_id' => $arrsiup['izin_id'],
                        'permohonan_id' => $arrsiup['permohonan_id'],
                        'created' => 'GETDATE()',
                        'created_user' => $this->newsession->userdata('username'));
                    for ($j = 0; $j < count($arrkeys); $j++) {
                        $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                    }
                    unset($requirements['id']); //print_r($requirements);
                    if ($requirements['upload_id'] != "")
                        $this->db->insert('t_upload_syarat', $requirements);
                }//die();
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idUpload = $this->db->insert_id();
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Data Persyaratan)',
                        'url' => '{c}' . site_url() . 'post/licensing/siupagen_act/fourth/save' . ' {m} models/licensing/siupagen_act {f} set_fourth($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                    $msg = "MSG||YES||Data Persyaratan berhasil disimpan. Pengajuan Permohonan " . $txt . " selesai.||" . site_url() . 'licensing/view/status';
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            } elseif ($act == "update") {
               //  $this->db->where_in('id', $id_upload_syarat);
                $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                $this->db->where('izin_id', $arrsiup['izin_id']);
                $this->db->delete('t_upload_syarat');
                if ($this->db->affected_rows() > 0) {
                    for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                        $requirements = array('izin_id' => $arrsiup['izin_id'],
                            'permohonan_id' => $arrsiup['permohonan_id'],
                            'created' => 'GETDATE()',
                            'created_user' => $this->newsession->userdata('username'));
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                        }
                        unset($requirements['id']);
                        if ($requirements['upload_id'] != "")
                            $this->db->insert('t_upload_syarat', $requirements);
                    }
                    if ($this->db->affected_rows() > 0) {
                        $ressiup = TRUE;
                        $idUpload = $this->db->insert_id();
                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Update - Data Persyaratan)',
                            'url' => '{c}' . site_url() . 'post/licensing/siupagen_act/fourth/update' . ' {m} models/licensing/siupagen_act {f} set_fourth($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                        $msg = "MSG||YES||Data Persyaratan berhasil diupdate.||REFRESH";
                    }
                    if ($this->db->trans_status() === FALSE || !$ressiup) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }
            return $msg;
        }
    }

    function get_preview($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }
            $query = "SELECT a.id, b.direktorat_id, a.kd_izin, a.no_aju, a.trader_id, a.tipe_permohonan, c.uraian AS permohonan, g.uraian as produksi, h.uraian as jenis_agen,a.npwp, d.uraian AS tipe_perusahaan, a.nm_perusahaan, 
					a.almt_perusahaan, dbo.get_region(2, a.kdprop) AS prop_perusahaan, dbo.get_region(4, a.kdkab) AS kab_perusahaan, dbo.get_region(7, a.kdkec) AS kec_perusahaan, dbo.get_region(10, a.kdkel) AS kel_perusahaan,
					a.kdpos, a.telp, a.fax, i.uraian as identitas_pj, a.nama_pj, a.noidentitas_pj, z.uraian as jabatan_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, a.alamat_pj, 
					a.almt_perusahaan, dbo.get_region(2, a.kdprop_pj) AS prop_pj, dbo.get_region(4, a.kdkab_pj) AS kab_pj, dbo.get_region(7, a.kdkec_pj) AS kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj,
					a.telp_pj, a.fax_pj, 
					e.uraian as tipe_perusahaan_prod, a.nm_perusahaan_prod, a.almt_perusahaan_prod, a.tgl_pendirian_prod, 
					dbo.get_region(2, a.kdprop_prod) AS prop_prod, dbo.get_region(4, a.kdkab_prod) AS kab_prod, dbo.get_region(7, a.kdkec_prod) AS kec_prod, dbo.get_region(10, a.kdkel_prod) AS kel_prod,
					a.telp_prod, a.fax_prod,
					f.uraian as tipe_perusahaan_supl, a.nm_perusahaan_supl, a.almt_perusahaan_supl, a.tgl_pendirian_supl, a.telp_supl, a.fax_supl,
					dbo.get_region(2, a.kdprop_supl) AS prop_supl, dbo.get_region(4, a.kdkab_supl) AS kab_supl, dbo.get_region(7, a.kdkec_supl) AS kec_supl, dbo.get_region(10, a.kdkel_supl) AS kel_supl,
					a.bidang_usaha, a.pegawai_lokal, a.pegawai_asing, a.status, a.pemasaran, b.nama_izin, b.disposisi,j.uraian as produk,
					k.jenis, k.merk, k.hs, a.no_aju, dbo.dateIndo(a.tgl_kirim) as tgl_aju
					FROM t_siup_agen a 
					LEFT JOIN m_izin b ON b.id = a.kd_izin 
					LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
					LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff e ON e.kode = a.tipe_perusahaan_prod AND e.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff f ON f.kode = a.tipe_perusahaan_supl AND f.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff g ON g.kode = a.produksi AND g.jenis = 'PRODUKSI_KEAGENAN'
					LEFT JOIN m_reff h ON h.kode = a.jenis_agen AND h.jenis = 'JENIS_KEAGENAN'
					LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
					LEFT JOIN m_reff j ON j.kode = a.produk AND j.jenis = 'PRODUK'
					LEFT JOIN t_siup_agen_detil k on k.permohonan_id = a.id
                    LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
					WHERE a.id =" . $id;
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
                $arrdata['act'] = site_url() . 'post/proccess/siupagen_act/verification';
                $arrdata['input'] = $this->get_input($dir, $doc, $type, $id, $row['status']);
                $arrdata['proses'] = $this->main->set_proses($dir, $row['kd_izin'], $row['disposisi'], $this->newsession->userdata('role'), $row['status'], hashids_encrypt($row['id'], _HASHIDS_, 9));
                $arrdata['jmllog'] = $this->main->get_uraian("SELECT COUNT(*) AS JML FROM t_log_izin WHERE permohonan_id = '" . $row['id'] . "' AND kd_izin = '" . $row['kd_izin'] . "'", "JML");
                $arrdata['urllog'] = site_url() . 'get/log/izin/' . hashids_encrypt($row['id'], _HASHIDS_, 9) . '/' . hashids_encrypt($row['kd_izin'], _HASHIDS_, 9);
            }

            // $arrdata['cetak'] = '<button class="btn btn-sm btn-warning addon-btn m-b-10" id= "' . rand() . '" data-url = "' . site_url() . 'prints/licensing/' . hashids_encrypt($dir, _HASHIDS_, 9) . '/' . hashids_encrypt($row['kd_izin'], _HASHIDS_, 9) . '/' . hashids_encrypt($row['id'], _HASHIDS_, 9) . '/penjelasan" onclick="blank($(this));return false;"><i class="fa fa-print pull-right"></i>Cetak Penjelasan</button>';
            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id 
							LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id 
							WHERE c.kategori = '01' and a.izin_id = " . $doc . " and a.permohonan_id = '" . $id . "'";
            $data_syarat = $this->main->get_result($query_syarat);
            $arr = array();
            if ($data_syarat) {
                foreach ($query_syarat->result_array() as $keys) {
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess_syarat'] = $arr;
            }

            $query_req = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.kategori = '01' and a.izin_id = '" . $doc . "' " . $addCon . " order by a.tipe desc, a.urutan ASC";
            $data_req = $this->main->get_result($query_req);
            $arrdata['req'] = $query_req->result_array();

            $query_legal = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id 
							LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id 
							WHERE c.kategori = '04' and a.izin_id = " . $doc . " and a.permohonan_id = '" . $id . "'";
            $data_legal = $this->main->get_result($query_legal);
            $arr = array();
            if ($data_legal) {
                foreach ($query_legal->result_array() as $keys) {
                    if (!isset($arr[$keys['dok_id']])) {
                        $arrlegal[$keys['dok_id']] = array();
                    }
                    $arrlegal[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess_legal'] = $arrlegal;
            }


            $query_req_legal = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.kategori = '04' and a.izin_id = '" . $doc . "' " . $addCon;
            $data_req_legal = $this->main->get_result($query_req_legal);
            if ($data_req_legal) {
                $arrdata['req_legal'] = $query_req_legal->result_array();
            }
            $arrdata['dir'] = $dir;
            $arrdata['telaah'] = $this->main->get_telaah($doc);
            $arrdata['agrement'] = $this->main->get_agrement($doc);
            return $arrdata;
        }
    }

    function get_input($dir, $doc, $type, $id, $stts) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrstts = array('0102');
            $arrdata = array();
            if (in_array($stts, $arrstts)) {
                $query = "SELECT a.id, a.kd_izin, a.no_aju, CONVERT(VARCHAR(10), a.tgl_aju, 103) AS tgl_aju, a.no_izin, CONVERT(VARCHAR(10), a.tgl_izin, 103) as tgl_izin, CONVERT(VARCHAR(10), a.tgl_izin_exp, 103) AS tgl_izin_exp FROM t_siup_agen a WHERE a.id = '" . $id . "' AND a.kd_izin = '" . $doc . "'";
                $ret = $this->main->get_result($query);
                if ($ret) {
                    $this->ineng = $this->session->userdata('site_lang');
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['dir'] = $dir;
                    $arrdata['doc'] = $doc;
                }
                if (!$this->session->userdata('site_lang'))
                    $this->ineng = "id";
                $data = $this->load->view($this->ineng . '/backend/input/' . $dir . '/' . $stts, $arrdata, true);
            } else {
                $data = "";
            }
        }
        return $data;
    }

    function get_listagen($act, $id, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;

            $direktorat = "'" . join("','", $this->newsession->userdata('dir_id')) . "'";
            $kd_izin = "'" . join("','", $this->newsession->userdata('kode_izin')) . "'";
            $arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
            $arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
            if (implode($this->newsession->userdata('dir_id')) == "02") {
                $where = "status IN ('0100','0104')";
            } else {
                $where = "status = '0100'";
            }

            $query = "SELECT a.id, '01' + '/' + CAST(a.kd_izin AS VARCHAR) + '/' + a.tipe_permohonan + '/' + CAST(a.id AS VARCHAR) AS IDX,
                    a.no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), a.tgl_aju, 105) + '</div>' AS 'Pengajuan',
                    'Direktorat Bina Usaha Dan Pelaku Distribusi' + '<div><b>' + d.nama_izin + '</b> ( ' + c.uraian + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +a.npwp+ '</b></div>' + '<div><b>' +a.nm_perusahaan+ '</b></div>' +
                    CONVERT(VARCHAR(10), a.tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), a.created, 108) + '<div>' +a.created_user+ '</div>' AS 'Daftar Dokumen',
                    a.no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  a.tgl_izin, 105),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), a.tgl_izin_exp, 105),'-') + '</div>' AS 'Perizinan', 'Perusahaan Supplier : <div>' + CAST(a.nm_perusahaan_supl as VARCHAR) + '</div>' + 'Perusahaan Produsen : <div>' + CAST(a.nm_perusahaan_prod AS VARCHAR) + '</div>' as 'Data Prinsipal', a.kd_izin
                    FROM t_siup_agen a
                    LEFT JOIN m_reff c ON c.kode = tipe_permohonan and c.jenis = 'TIPE_PERMOHONAN'
                    LEFT JOIN m_izin d ON d.id = kd_izin
                    LEFT JOIN m_reff b ON b.kode = a.status AND b.jenis = 'STATUS' 
                    WHERE a.status = '1000'";
            //print_r($query);die();
            $table->title("");
            $table->columns(array("id", "direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
                array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'", site_url() . 'licensing/preview/{IDX}'),
                "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
                "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
                "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
                "status", "CONVERT(VARCHAR(10), tgl_aju, 120)",
                "<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>"));
            $this->newtable->width(array('Pengajuan' => 200, 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
            $this->newtable->search(array(array("a.nm_perusahaan", "Nama Perusahaan"), array("a.nm_perusahaan_supl", "Nama Perusahaan Supplier"),
                array("a.nm_perusahaan_prod", "Nama Perusahaan Produsen"),
                array("a.no_aju", "Nomor Permohonan"),
                array("CONVERT(VARCHAR(10), a.tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("a.no_aju", "No. Izin"),
                array("CONVERT(VARCHAR(10), a.tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("a.tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis))
            ));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/lst_agen");
            $table->orderby('last_proses');
            $table->sortby("ASC");
            $table->keys(array("IDX"));
            $table->hiddens(array("id", "IDX", "SORTTGL", "kd_izin"));
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            $table->menu(array(//'Tambah' => array('GET', site_url() . 'licensing/manual', '0', 'home', 'modal'),
                // 'Edit' => array('GET', site_url() . 'licensing/form/first', '1', 'fa fa-edit'),
                'Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-eye')
                    // 'Delete' => array('POST', site_url().'licensing/delete_inbox/ajax','N', 'fa fa-times','isngajax')
            ));
            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Permohonan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function set_onfly($act, $id, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($act == "update") {
                if (!$isajax) {
                    return false;
                    exit();
                }
                $msg = "MSG||NO";
                $respon = FALSE;
                $arrsiup = $this->main->post_to_query($this->input->post('dataon'));
                $id = hashids_decrypt($id, _HASHIDS_, 9);
                $this->db->where('id', $id);
                $this->db->update('t_siup_agen', $arrsiup);
                if ($this->db->affected_rows() == 1) {
                    $respon = TRUE;
                    $logu = array('aktifitas' => 'Melakukan update tanggal izin / tanggal masa berlaku',
                        'url' => '{c}' . site_url() . 'get/onfly/onfly_act/update' . ' {m} models/licensing/siupagen_act {f} set_onfly($act,  $id, $isajax)');
                    $this->main->set_activity($logu);
                }
                if ($respon)
                    $msg = "MSG||YES";
                return $msg;
            }
        }
    }

    function referensi_kbli($target, $callback, $fieldcallback) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT kode as kbli, uraian as desc_kbli, kode AS 'Kode KBLI', uraian AS 'Uraian KBLI' FROM m_kbli";
            $table->title("");
            $table->columns(array("kode", "Kode KBLI", "uraian", "Uraian KBLI"));
            $this->newtable->width(array('Kode KBLI' => 100, 'Uraian' => 300, '&nbsp;' => 5));
            $this->newtable->search(array(array("kode", "Kode KBLI"),
                array("uraian", "Uraian")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_kbli/" . $target . '/' . $callback . '/' . $fieldcallback);
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("kode"));
            $table->hiddens(array("kbli", "desc_kbli"));
            $table->use_ajax(TRUE);
            $table->show_search(TRUE);
            $table->show_chk(FALSE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);
            $table->settrid(TRUE);
            $table->attrid($target);
            if ($callback != "")
                $table->callback(site_url(str_replace(".", '/', $callback)));
            if ($fieldcallback != "")
                $table->fieldcallback($fieldcallback);
            $table->tbtarget("refkbli");
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function list_dok($target, $callback, $fieldcallback, $id, $multiple, $putin, $doc) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT id, id AS 'upload_id', nomor AS 'No. Dokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'Tgl. Dokumen', tgl_exp AS 'Tgl. Akhir', '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
			FROM t_upload WHERE tipe_dok = '" . $id . "' AND trader_id = '" . $this->newsession->userdata('trader_id') . "'";
            $table->title("");
            $table->columns(array("id", "nomor", "penerbit_dok", "tgl_dok", "tgl_exp", "<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
            $this->newtable->width(array('Nomor Dokumen' => 100, 'Penerbit' => 300, 'Tanggal Awal' => 100, 'Tanggal Akhir' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("nomor", "Nomor Penerbit"),
                array("penerbit_dok", "Penerbit")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_syarat/" . $id . '/' . $target . '/' . $callback . '/' . $fieldcallback . '/');
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id", "folder", "nama_file", "upload_id"));
            $table->show_search(TRUE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);

            if ((int) $multiple == 1) {
                $table->show_chk(TRUE);
                $table->menu(array('Pilih Data' => array('POSTGET', site_url() . 'post/document/get_requirements/' . $doc . '/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#' . $putin)));
            } else {
                $table->show_chk(FALSE);
                if ($callback != "")
                    $table->callback(site_url(str_replace(".", '/', $callback)));
                if ($fieldcallback != "")
                    $table->fieldcallback($fieldcallback);
                $table->settrid(TRUE);
                $table->attrid($target);
            }
            $table->tbtarget("refreq_" . rand());
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function get_requirements($doc) {
        $id = join("','", $this->input->post('tb_chk'));
        $data = "SELECT a.id AS dok_ids, b.id AS id_upload, b.nomor, b.penerbit_dok, b.tgl_dok, b.tgl_exp, '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS files, c.tipe FROM m_dok a LEFT JOIN t_upload B ON a.id = b.tipe_dok LEFT JOIN m_dok_izin c ON c.dok_id = a.id WHERE b.id IN('" . $id . "') AND c.izin_id = '" . $doc . "'";
        //print_r($data);die();
        return $this->db->query($data)->result_array();
    }

}

?>