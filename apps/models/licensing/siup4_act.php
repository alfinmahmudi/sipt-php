<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Siup4_act extends CI_Model {

    function get_first($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata = array();
            $kdprop = $this->newsession->userdata('kdprop');
            $kdkab = $this->newsession->userdata('kdkab');
            $idkec = $this->newsession->userdata('kdkec');
            $idkel = $this->newsession->userdata('kdkel');
            $tipe_perusahaan = $this->newsession->userdata('tipe_perusahaan'); //die($trader_id);
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $kdprop . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '" . $kdkab . "'  ORDER BY 2", "id", "nama", TRUE); //print_r($arrdata);die();
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '" . $idkec . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '" . $idkel . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['direktorat'] = $dir;
            $arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian", TRUE);
            $arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN' and kode = '" . $tipe_perusahaan . "' ", "kode", "uraian", TRUE);
            $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $kdkab . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec'] . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['jenis_waralaba'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_WARALABA' ORDER BY kode", "kode", "uraian", TRUE);
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;

            if ($id == "") {
                $arrdata['act'] = site_url('post/licensing/siup4_act/first/save');
                //$type = hashids_decrypt($type,_HASHIDS_,6);
                $arrdata['sess']['tipe_permohonan'] = $type;
                $arrdata['sess']['kd_izin'] = $doc;
            } else {
                $arrdata['act'] = site_url('post/licensing/siup4_act/first/update');
                $query = "SELECT id, kd_izin, no_aju, tgl_aju, trader_id, tipe_permohonan, npwp, tipe_perusahaan, nm_perusahaan, almt_perusahaan, 
						kdprop, kdkab, kdkec, kdkel, kdpos, telp, fax,id_permohonan_lama, no_izin_lama, tgl_izin_lama, tgl_izin_exp_lama, merk_perusahaan, jenis_waralaba, fl_waralaba
						FROM t_siup4 WHERE id = '" . $id . "'";
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec'] . "' ORDER BY 2", "id", "nama", TRUE);
                }
            }
            return $arrdata;
        }
    }

    function set_first($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data Permohonan SIUP Perantara Perdangangan Properti gagal disimpan";
            $ressiup = FALSE;
            $arrsiup = $this->main->post_to_query($this->input->post('siupmb'));
            $dir = $this->input->post('direktorat');
            $doc = $arrsiup['kd_izin'];
            $type = $arrsiup['tipe_permohonan'];
            $arrsiup['fl_waralaba'] = '0';
            if($arrsiup['jenis_waralaba'] != ''){
                    $arrsiup['fl_waralaba'] = '1';
            }
            if ($act == "save") {
                $arrsiup['no_aju'] = $this->main->set_aju();
                $arrsiup['fl_pencabutan'] = '0';
                $arrsiup['status'] = '0000';
                $arrsiup['tgl_aju'] = 'GETDATE()';
                $arrsiup['trader_id'] = $this->newsession->userdata('trader_id');
                $arrsiup['created'] = 'GETDATE()';
                $arrsiup['created_user'] = $this->newsession->userdata('username');
                if (($type != '01') && ($arrsiup['id_permohonan_lama'] != "")) {
                    if ($type == '03')
                        $arrsiup['fl_pembaharuan'] = "1";
                    $query_old_doc = "SELECT identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, 
									  alamat_pj, kdprop_pj, kdkab_pj, kdkel_pj, kdkec_pj,telp_pj, fax_pj, 
									  nilai_modal, nilai_saham
									  FROM t_siup4 WHERE id = " . $arrsiup['id_permohonan_lama'] . " AND kd_izin = " . $arrsiup['kd_izin'];
                    $data_old = $this->main->get_result($query_old_doc);
                    if ($data_old) {
                        foreach ($query_old_doc->result_array() as $keys) {
                            $jml = count($keys);
                            $array_keys = array_keys($keys);
                            $array_values = array_values($keys);
                            for ($i = 0; $i < $jml; $i++) {
                                $arrsiup[$array_keys[$i]] = $array_values[$i];
                            }
                        }
                    }
                }
                $this->db->trans_begin();
                $exec = $this->db->insert('t_siup4', $arrsiup);
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idredir = $this->db->insert_id();
                    $msg = "MSG||YES||Data Permohonan SIUP Perantara Perdangangan Properti berhasil disimpan. Silahkan lanjutkan mengisi data penanggung jawab||" . site_url() . 'licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $idredir;

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan SIUP Perantara Perdangangan Properti dengan nomor permohonan : ' . $arrsiup['no_aju'],
                        'url' => '{c}' . site_url() . 'post/licensing/siup4_act/first/save' . ' {m} models/licensing/siup4_act {f} set_first($act, $ajax)');
                    $this->main->set_activity($logu);
                    $logi = array('kd_izin' => $arrsiup['kd_izin'],
                        'permohonan_id' => $idredir,
                        'keterangan' => 'Menambahkan daftar permohonan SIUP Perantara Perdangangan Properti dengan nomor permohonan : ' . $arrsiup['no_aju'],
                        'catatan' => '',
                        'status' => '0000',
                        'selisih' => 0);
                    $this->main->set_loglicensing($logi);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } elseif ($act == "update") {
                $id = $this->input->post('id');
                $this->db->where('id', $id);
                $this->db->update('t_siup4', $arrsiup);
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $msg = "MSG||YES||Data Permohonan SIUP Perantara Perdangangan Properti berhasil diupdate.||REFRESH";
                }
                return $msg;
            }
        }
    }

    function get_second($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $id_user = $this->newsession->userdata('id');
            $idprop = $this->newsession->userdata('kdprop_pj');
            $idkab = $this->newsession->userdata('kdkab_pj');
            $idkec = $this->newsession->userdata('kdkec_pj');
            $idkel = $this->newsession->userdata('kdkel_pj');
            $jabatan_pj = $this->newsession->userdata('jabatan_pj');

            $jenis_identitas = $this->newsession->userdata('tipe_identitas'); //die($jenis_identitas);
            $arrdata['act'] = site_url('post/licensing/siup4_act/second/save');
            $arrdata['direktorat'] = $dir;
            $arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_IDENTITAS' and kode = '" . $jenis_identitas . "' ", "kode", "uraian", TRUE);
            $arrdata['jabatan_pj'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JABATAN' AND kode = '" . $jabatan_pj . "' ", "kode", "uraian", TRUE);
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $idprop . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '" . $idkab . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '" . $idkec . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '" . $idkel . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['direktorat'] = $dir;
            $arrdata['no_identitas'] = $dir;
            $arrdata['direktorat'] = $dir;
            $arrdata['direktorat'] = $dir;
            $arrdata['direktorat'] = $dir;
            $arrdata['direktorat'] = $dir;
            $arrdata['direktorat'] = $dir;
            $arrdata['direktorat'] = $dir;
            $arrdata['direktorat'] = $dir;

            $arrdata['urifirst'] = site_url('licensing/form/first/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            $query = "	SELECT id, kd_izin, tipe_permohonan, identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, 
						alamat_pj,kdprop_pj, kdkab_pj, kdkec_pj,kdkel_pj, telp_pj, fax_pj, kdprop, kdkab, kdkec, kdkel, nilai_modal, nilai_saham 
						FROM t_siup4 
						WHERE id = '" . $id . "' ";
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
                if ($row['identitas_pj']) {
                    $arrdata['act'] = site_url('post/licensing/siup4_act/second/update');
                }
                $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            return $arrdata;
        }
    }

    function set_second($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (($act == "update") || ($act == "save")) {
                if (!$isajax) {
                    return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                    exit();
                }
                $msg = "MSG||NO||Data Penanggung Jawab gagal disimpan";
                $respj = FALSE;
                $arrpj = $this->main->post_to_query($this->input->post('pj'));
                $dir = $this->input->post('direktorat');
                $doc = $this->input->post('kd_izin');
                $type = $this->input->post('tipe_permohonan');
                $id = $this->input->post('id');
                $nilai_modal = $this->input->post('nilai_modal');
                $nilai_saham = $this->input->post('nilai_saham');
                $arrpj['nilai_modal'] = str_replace(",", "", $nilai_modal);
                $arrpj['nilai_saham'] = str_replace(",", "", $nilai_saham);
                // print_r($arrpj[nilai_modal] . ' -- ' .$arrpj[nilai_saham]);die();
                $this->db->where('id', $id);
                $this->db->update('t_siup4', $arrpj);
                if ($this->db->affected_rows() > 0) {
                    $respj = TRUE;
                    if ($act == "update") {
                        $msg = "MSG||YES||Data Penanggung Jawab berhasil diupdate.||REFRESH";
                    } else {
                        $msg = "MSG||YES||Data Penanggung Jawab berhasil disimpan. Silahkan lanjutkan mengisi data tenaga ahli||" . site_url() . 'licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id;

                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan SIUP Perantara Perdangangan Properti (Data Penanggung Jawab)',
                            'url' => '{c}' . site_url() . 'post/licensing/siup4_act/second/save' . ' {m} models/licensing/siup4_act {f} set_second($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                    }
                }
                return $msg;
            }
        }
    }

    function get_lstthird($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $table = $this->newtable;
                //$arrdata['act'] = site_url('post/licensing/siupmb_act/third/save');
                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $arrdata = array('id' => $id,
                    'direktorat' => $dir,
                    'kd_izin' => $doc,
                    'tipe_permohonan' => $type,
                    'nama_izin' => $nama_izin,
                    'act' => site_url('post/licensing/siup4_act/third/save'),
                    'urisecond' => site_url('licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urithird' => site_url('licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urifourth' => site_url('licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'uripreview' => site_url('licensing/preview/' . $dir . '/' . $doc . '/' . $type . '/' . $id));

                $query = "SELECT id , nama as 'Nama', warganegara as 'Kewarganegaraan', no_sertifikat as 'No. Sertifikat', penerbit as 'Penerbit' FROM t_siup4_ahli 
						  	WHERE permohonan_id = '" . $id . "'";


                $table->title("");
                $table->columns(array("id", "nama", "no_sertifikat", "penerbit", "warganegara"));
                $this->newtable->width(array('Nama' => 200, 'No. Sertifikat' => 100, 'Penerbit' => 100, 'Kewarganegaraan' => 100));
                $this->newtable->search(array(array("nama", "Nama"),
                    array("no_sertifikat", "No. Sertifikat"),
                    array("penerbit", "Penerbit"),
                    array("warganegara", "Kewarganegaraan")));
                $table->cidb($this->db);
                $table->ciuri($this->uri->segment_array());
                $table->action(site_url() . 'licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
                //$table->orderby(1);
                $table->sortby("ASC");
                $table->keys(array("id"));
                $table->hiddens(array("id"));
                $table->show_search(TRUE);
                $table->show_chk(TRUE);
                $table->single(TRUE);
                $table->dropdown(TRUE);
                $table->use_ajax(TRUE);
                //$table->hashids(TRUE);
                $table->postmethod(TRUE);
                $table->tbtarget("refthird");
                $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/view/tenaga_ahli/' . $doc . '/' . $id, '0', 'home'),
                    'Edit' => array('GET', site_url() . 'licensing/view/tenaga_ahli/' . $doc . '/' . $id, '1', 'fa fa-edit'),
                    'Delete' => array('POST', site_url() . 'licensing/del_siup4/ajax', 'N', 'fa fa-times', 'isngajax')
                ));
                $arrdata['lstTngAhli'] = $table->generate($query);
                //print_r($arrdata);die();
                if ($this->input->post("data-post"))
                    return $table->generate($query);
                else
                    return $arrdata;
            }
        }
    }

    function get_tenaga_ahli($doc, $id, $dtl) {
        if ($this->newsession->userdata('_LOGGED')) {
            //print_r($id.$dtl);die();
            $detail_id = hashids_decrypt($dtl, _HASHIDS_, 9);
            $arrdata['permohonan_id'] = $id;
            $arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_IDENTITAS' AND kode = '01' ", "kode", "uraian", TRUE);
            $query = "SELECT a.id, a.kd_izin, b.direktorat_id, b.nama_izin, a.tipe_permohonan, c.dok_id, c.tipe, d.keterangan, e.uraian from t_siup4 a
					LEFT JOIN m_izin b on b.id = a.kd_izin
					LEFT JOIN m_dok_izin c on c.izin_id = a.kd_izin and c.kategori = '03'
					LEFT JOIN m_dok d on d.id = c.dok_id
					LEFT JOIN m_reff e on e.kode = c.tipe and e.jenis = 'TIPE_DOKIZIN'
					WHERE a.id = '" . $id . "'";
            $data_req = $this->main->get_result($query);
            $arrdata['req'] = $query->result_array();
            $arrdata['act'] = site_url('post/licensing/siup4_act/third/save');
            if ($dtl != "") {
                $query_ahli = "SELECT id, permohonan_id, nama, warganegara, jenis_identitas, no_identitas, no_sertifikat, penerbit FROM t_siup4_ahli WHERE permohonan_id = '" . $id . "' and id = '" . $dtl . "'";
                $data_ahli = $this->main->get_result($query_ahli);
                if ($data_ahli) {
                    foreach ($query_ahli->result_array() as $row) {
                        $arrdata['ahli'] = $row;
                    }
                    $arrdata['act'] = site_url('post/licensing/siup4_act/third/update');
                }

                //print_r($arrdata);die();
                $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok, a.detail_id,
							'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id 
							WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "' and a.detail_id = '" . $dtl . "'";
                //print_r($query_syarat);die();
                $data = $this->main->get_result($query_syarat);
                $arr = array();
                if ($data) {
                    foreach ($query_syarat->result_array() as $keys) {
                        $arr[$keys['dok_id']] = $keys;
                    }
                    $arrdata['sess'] = $arr;
                }
            }
            //print_r($arrdata);die();

            return $arrdata;
        }
    }

    function set_third($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $res = FALSE;
            $arrahli = $this->main->post_to_query($this->input->post('ahli'));
            $arrsiup = $this->main->post_to_query($this->input->post('SIUPMB'));
            $arrahli['permohonan_id'] = $arrsiup['permohonan_id'];
            $dir = $this->input->post('direktorat');
            $tipe_permohonan = $this->input->post('tipe_permohonan');
            $doc = $arrsiup['izin_id'];
            $arrreq = $this->input->post('REQUIREMENTS');
            $arrkeys = array_keys($arrreq);
            if ($act == "save") {
                $arrahli['created'] = 'GETDATE()';
                $this->db->insert('t_siup4_ahli', $arrahli);
                if ($this->db->affected_rows() > 0) {
                    $resahli = TRUE;
                    $idAhli = $this->db->insert_id();
                    for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                        $requirements = array('izin_id' => $arrsiup['izin_id'],
                            'detail_id' => $idAhli,
                            'permohonan_id' => $arrsiup['permohonan_id'],
                            'created' => 'GETDATE()',
                            'created_user' => $this->newsession->userdata('username'));
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                        }
                        unset($requirements['id']);
                        $this->db->insert('t_upload_syarat', $requirements);
                    }
                    if ($this->db->affected_rows() > 0) {
                        $res = TRUE;
                        $idUpload = $this->db->insert_id();
                        $msg = "MSG||YES||Data Tenaga Ahli berhasil disimpan.||" . site_url() . 'licensing/form/third/' . $dir . '/' . $doc . '/' . $tipe_permohonan . '/' . $arrahli['permohonan_id'];
                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan SIUP Perantara Perdangangan Properti (Data Tenaga Ahli)',
                            'url' => '{c}' . site_url() . 'post/licensing/siup4_act/third/save' . ' {m} models/licensing/siup4_act {f} set_third($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                    }
                    if ($this->db->trans_status() === FALSE || !$res) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            } elseif ($act == "update") {
                $ahli_id = $this->input->post('id_ahli');
                $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                $this->db->where('id', $ahli_id);
                $this->db->update('t_siup4_ahli', $arrahli);
                if ($this->db->affected_rows() > 0) {
                    for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                        $requirements = array('updated' => 'GETDATE()');
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                        }
                        $idx = $requirements['id'];
                        unset($requirements['id']);
                        $this->db->where('id', $idx);
                        $this->db->where('izin_id', $arrsiup['izin_id']);
                        $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                        $this->db->where('detail_id', $ahli_id);
                        $this->db->update('t_upload_syarat', $requirements);
                    }
                    if ($this->db->affected_rows() > 0) {
                        $ressiup = TRUE;
                        $msg = "MSG||YES||Data Tenaga Ahli berhasil diupdate.||REFRESH";
                    }
                    if ($this->db->trans_status() === FALSE || !$ressiup) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }
            //print_r($act);die();
            return $msg;
        }
    }

    function get_fourth($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            $arrdata['izin_id'] = $doc;
            $arrdata['direktorat'] = $dir;
            $arrdata['type'] = $type;
            $arrdata['permohonan_id'] = $id;
            $arrdata['urithird'] = site_url('licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id,
                            case when c.multi = 1 then '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor else '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor end AS nomor,
                             b.tgl_dok, b.tgl_exp, b.penerbit_dok,
                            '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
                            FROM t_upload_syarat a 
                            LEFT join t_upload b on b.id = a.upload_id
                            LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id
                            WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "' and a.detail_id is null";
                       
            $data = $this->main->get_result($query_syarat);
            $arr = array();

            $query2 = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian, a.multi
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.kategori = '01' and a.izin_id = '" . $doc . "' " . $addCon . " order by 5 desc";
            $this->main->get_result($query2);
            $data_dok = $query2->result_array();
            $arrdata['req'] = $data_dok;
            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
                    //$arr[$keys['dok_id']] = $keys;
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess'] = $arr;
                $arrdata['act'] = site_url('post/licensing/siup4_act/fourth/update');
            } else {
                foreach ($data_dok as $datadt) {
                    $query_syarat2 = "  SELECT top 1 b.id as 'upload_id', b.tipe_dok,  CASE WHEN a.multi = 1 THEN ' ' + b.nomor ELSE ' ' + b.nomor END AS nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
					'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View' 
                                            FROM t_upload b left join m_dok_izin a on b.tipe_dok = a.dok_id 
					where a.izin_id = '".$doc."' and b.trader_id = '" . $this->newsession->userdata('trader_id') . "'and b.tipe_dok = '" . $datadt['dok_id'] . "'  " . $addCon . " order by tgl_dok desc;";
                    $cek = $this->db->query($query_syarat2)->num_rows();
                    //print_r($cek);die();
                    $data2 = $this->main->get_result($query_syarat2);
                    if ($cek != 0) {
                        foreach ($query_syarat2->result_array() as $keys) {
                            if (!isset($arr[$keys['tipe_dok']])) {
                                $arr[$keys['tipe_dok']] = array();
                            }
                            $arr[$keys['tipe_dok']][] = $keys;
                        }
                    }
                }
                $arrdata['sess'] = $arr;
                $arrdata['baru'] = true;
                $arrdata['act'] = site_url('post/licensing/siup4_act/fourth/save');
            }
            return $arrdata;
        }
    }

    function set_fourth($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $ressiup = FALSE;
            $arrsiup = $this->main->post_to_query($this->input->post('SIUPMB'));
            $arrreq = $this->input->post('REQUIREMENTS');
            $arrkeys = array_keys($arrreq);
            if ($act == "save") {
                for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                    $requirements = array('izin_id' => $arrsiup['izin_id'],
                        'permohonan_id' => $arrsiup['permohonan_id'],
                        'created' => 'GETDATE()',
                        'created_user' => $this->newsession->userdata('username'));
                    for ($j = 0; $j < count($arrkeys); $j++) {
                        $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                    }
                    unset($requirements['id']);
                    if ($requirements['upload_id'] != "")
                        $this->db->insert('t_upload_syarat', $requirements);
                }
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idUpload = $this->db->insert_id();
                    $msg = "MSG||YES||Data Persyaratan berhasil disimpan. Pengajuan Permohonan SIUP Perantara Perdangangan Properti selesai.||" . site_url() . 'licensing/view/status';
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan SIUP Perantara Perdangangan Properti (Data Persyaratan)',
                        'url' => '{c}' . site_url() . 'post/licensing/siup4_act/fourth/save' . ' {m} models/licensing/siup4_act {f} set_fourth($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            } elseif ($act == "update") {
                $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                $this->db->where('izin_id', $arrsiup['izin_id']);
                $this->db->where('detail_id', null);
                $this->db->delete('t_upload_syarat');
                if ($this->db->affected_rows() > 0) {
                    for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                        $requirements = array('izin_id' => $arrsiup['izin_id'],
                            'permohonan_id' => $arrsiup['permohonan_id'],
                            'created' => 'GETDATE()',
                            'created_user' => $this->newsession->userdata('username'));
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                        }
                        unset($requirements['id']);
                        if ($requirements['upload_id'] != "")
                            $this->db->insert('t_upload_syarat', $requirements);
                    }

                    if ($this->db->affected_rows() > 0) {
                        $ressiup = TRUE;
                        $msg = "MSG||YES||Data Persyaratan berhasil diupdate.||REFRESH";
                    }
                    if ($this->db->trans_status() === FALSE || !$ressiup) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }
            return $msg;
        }
    }

    function get_preview($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }

            $query = "SELECT a.id, b.direktorat_id, a.kd_izin, a.no_aju, a.trader_id, a.tipe_permohonan, c.uraian AS permohonan ,a.npwp, 
					d.uraian AS tipe_perusahaan, a.nm_perusahaan, a.almt_perusahaan, 
					e.nama AS kdprop, f.nama AS kdkab, g.nama AS kdkec, h.nama AS kdkel, a.kdpos, a.telp, a.fax, 
					l.uraian AS identitas_pj, a.nama_pj, a.noidentitas_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, a.alamat_pj, 
					m.nama AS kdprop_pj, n.nama AS kdkab_pj, o.nama AS kdkec_pj, p.nama AS kdkel_pj, a.telp_pj, a.fax_pj,
					a.nilai_modal, a.nilai_saham, a.status, b.nama_izin, b.disposisi, q.uraian as 'jabatan_pj', a.no_aju, dbo.dateIndo(a.tgl_kirim) as tgl_aju, a.merk_perusahaan, r.uraian as 'jenis_waralaba'
					FROM t_siup4 a
					LEFT JOIN m_izin b ON b.id = a.kd_izin 
					LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
					LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_prop e ON e.id = a.kdprop
					LEFT JOIN m_kab f ON f.id = a.kdkab
					LEFT JOIN m_kec g ON g.id = a.kdkec
					LEFT JOIN m_kel h ON h.id = a.kdkel
					LEFT JOIN m_reff l ON l.kode = a.identitas_pj AND l.jenis = 'JENIS_IDENTITAS'
					LEFT JOIN m_prop m ON m.id = a.kdprop_pj
					LEFT JOIN m_kab n ON n.id = a.kdkab_pj
					LEFT JOIN m_kec o ON o.id = a.kdkec_pj
					LEFT JOIN m_kel p ON p.id = a.kdkel_pj
					LEFT JOIN m_reff q ON q.kode = a.jabatan_pj and q.jenis = 'JABATAN'
                                        LEFT JOIN m_reff r ON r.kode = a.jenis_waralaba AND r.jenis = 'JENIS_WARALABA'
					WHERE a.id =  '" . $id . "'";
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
                $arrdata['act'] = site_url() . 'post/proccess/siup4_act/verification';
                $arrdata['input'] = $this->get_input($dir, $doc, $type, $id, $row['status']);
                $arrdata['proses'] = $this->main->set_proses($dir, $row['kd_izin'], $row['disposisi'], $this->newsession->userdata('role'), $row['status'], hashids_encrypt($row['id'], _HASHIDS_, 9));
                $arrdata['jmllog'] = $this->main->get_uraian("SELECT COUNT(*) AS JML FROM t_log_izin WHERE permohonan_id = '" . $row['id'] . "' AND kd_izin = '" . $row['kd_izin'] . "'", "JML");
                $arrdata['urllog'] = site_url() . 'get/log/izin/' . hashids_encrypt($row['id'], _HASHIDS_, 9) . '/' . hashids_encrypt($row['kd_izin'], _HASHIDS_, 9);
                // print_r($arrdata['sess']);die();
            }

            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id 
							WHERE a.permohonan_id = '" . $id . "' AND a.izin_id = '" . $doc . "' AND a.detail_id is null";
            $data = $this->main->get_result($query_syarat);
            $arr = array();
            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
                    //$arr[$keys['dok_id']] = $keys;
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess_syarat'] = $arr;
            }
            $query = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.izin_id = '" . $doc . "' and a.kategori = '01' " . $addCon . " order by a.urutan ASC";
            $data_req = $this->main->get_result($query);
            $arrdata['req'] = $query->result_array();
            $arrdata['dir'] = $dir;
            $table = $this->newtable;

         //    $query_ahli = "SELECT a.id, a.id as 'idx', a.nama as 'Nama', a.warganegara as 'Kewarganegaraan', a.no_sertifikat as 'No. Sertifikat', a.penerbit as 'Penerbit'
						   // FROM t_siup4_ahli a
						   // WHERE a.permohonan_id = '" . $id . "'";
         //    $table->title("DATA TENAGA AHLI");
         //    $table->columns(array("a.id", "idx", array("a.nama", site_url() . 'licensing/popup_ahli/{idx}/' . $doc . '/' . $id, 'modal'), "a.no_sertifikat", "a.penerbit", "a.warganegara"));
         //    $this->newtable->width(array('Nama' => 200, 'No. Sertifikat' => 100, 'Penerbit' => 100, 'Kewarganegaraan' => 100, 'Detail' => 10));

         //    $table->cidb($this->db);
         //    $table->ciuri($this->uri->segment_array());
         //    $table->orderby(1);
         //    $table->sortby("ASC");
         //    $table->keys(array("id"));
         //    $table->hiddens(array("id", 'idx'));
         //    $table->show_search(FALSE);
         //    $table->show_chk(FALSE);
         //    $table->single(TRUE);
         //    $table->dropdown(TRUE);
         //    $table->hashids(TRUE);
         //    $table->postmethod(TRUE);
         //    $table->tbtarget("refthird" . rand());
         //    $arrdata['lstTngAhli'] = $table->generate($query_ahli);

        // $dok_pen = "SELECT a.id, c.keterangan as 'Keterangan', b.nomor as 'Nomor Dokumen', b.penerbit_dok as 'Penerbit',  dbo.dateIndo(b.tgl_dok) as 'Tanggal Awal', dbo.dateIndo(b.tgl_exp) as 'Tanggal Akhir',
        //                       '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
        //                       FROM t_upload_syarat a 
        //                       LEFT JOIN t_upload b ON b.id = a.upload_id
        //                       LEFT JOIN m_dok c ON c.id = a.dok_id
        //                       WHERE a.izin_id = '" . $doc . "' AND a.detail_id ='" . $id . "' AND a.permohonan_id = '" . $permohonan_id . "'";

            $query = "SELECT a.id, a.nama, a.warganegara, a.no_sertifikat, a.penerbit FROM t_siup4_ahli a
                    WHERE a.permohonan_id = '" . $id . "'";
            $arrdata['lstTngAhli'] = $this->db->query($query)->result_array();
            if ($arrdata['sess']['status'] != '1000') {
                $arrdata['telaah'] = $this->main->get_telaah($doc);
                $arrdata['agrement'] = $this->main->get_agrement($doc);
            }
            return $arrdata;
        }
    }

    function get_input($dir, $doc, $type, $id, $stts) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrstts = array('0102');
            $arrdata = array();
            if (in_array($stts, $arrstts)) {
                $query = "SELECT a.id, a.kd_izin, a.no_aju, CONVERT(VARCHAR(10), a.tgl_aju, 103) AS tgl_aju, a.no_izin, CONVERT(VARCHAR(10), a.tgl_izin, 103) as tgl_izin, CONVERT(VARCHAR(10), a.tgl_izin_exp, 103) AS tgl_izin_exp FROM t_siup4 a WHERE a.id = '" . $id . "' AND a.kd_izin = '" . $doc . "'";
                $ret = $this->main->get_result($query);
                if ($ret) {
                    $this->ineng = $this->session->userdata('site_lang');
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['dir'] = $dir;
                    $arrdata['doc'] = $doc;
                }
                if (!$this->session->userdata('site_lang'))
                    $this->ineng = "id";
                $data = $this->load->view($this->ineng . '/backend/input/' . $dir . '/' . $stts, $arrdata, true);
            } else {
                $data = "";
            }
        }
        return $data;
    }

    function set_onfly($act, $id, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($act == "update") {
                if (!$isajax) {
                    return false;
                    exit();
                }
                $msg = "MSG||NO";
                $respon = FALSE;
                $arrsiup = $this->main->post_to_query($this->input->post('dataon'));
                $id = hashids_decrypt($id, _HASHIDS_, 9);
                $this->db->where('id', $id);
                $this->db->update('t_siup4', $arrsiup);
                if ($this->db->affected_rows() == 1) {
                    $respon = TRUE;
                    $logu = array('aktifitas' => 'Melakukan update tanggal izin / tanggal masa berlaku',
                        'url' => '{c}' . site_url() . 'get/onfly/onfly_act/update' . ' {m} models/licensing/siup4_act {f} set_onfly($act,  $id, $isajax)');
                    $this->main->set_activity($logu);
                }
                if ($respon)
                    $msg = "MSG||YES";
                return $msg;
            }
        }
    }

    function list_dok($target, $callback, $fieldcallback, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT id, id AS 'upload_id', nomor AS 'No. Dokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'Tgl. Dokumen', tgl_exp AS 'Tgl. Akhir', '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
			FROM t_upload WHERE tipe_dok = '" . $id . "' AND trader_id = '" . $this->newsession->userdata('trader_id') . "'";
            $table->title("");
            $table->columns(array("id", "nomor", "penerbit_dok", "tgl_dok", "tgl_exp", "<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
            $this->newtable->width(array('Nomor Dokumen' => 100, 'Penerbit' => 300, 'Tanggal Awal' => 100, 'Tanggal Akhir' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("nomor", "Nomor Penerbit"),
                array("penerbit_dok", "Penerbit")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_syarat/" . $id . '/' . $target . '/' . $callback . '/' . $fieldcallback . '/');
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id", "folder", "nama_file", "upload_id"));
            $table->use_ajax(TRUE);
            $table->show_search(TRUE);
            $table->show_chk(FALSE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);
            $table->settrid(TRUE);
            $table->attrid($target);
            if ($callback != "")
                $table->callback(site_url(str_replace(".", '/', $callback)));
            if ($fieldcallback != "")
                $table->fieldcallback($fieldcallback);
            $table->tbtarget("refreq");
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function list_dok_ahli($id, $doc, $permohonan_id) {
        if ($this->newsession->userdata('_LOGGED')) {//print_r($id.' - '.$doc.' - '.$permohonan_id);die();
            $table = $this->newtable;
            $query = "SELECT a.id, c.keterangan as 'Keterangan', b.nomor as 'Nomor Dokumen', b.penerbit_dok as 'Penerbit',  dbo.dateIndo(b.tgl_dok) as 'Tanggal Awal', dbo.dateIndo(b.tgl_exp) as 'Tanggal Akhir',
					  '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
					  FROM t_upload_syarat a 
					  LEFT JOIN t_upload b ON b.id = a.upload_id
					  LEFT JOIN m_dok c ON c.id = a.dok_id
					  WHERE a.izin_id = '" . $doc . "' AND a.detail_id ='" . $id . "' AND a.permohonan_id = '" . $permohonan_id . "'";
                      //print_r($query);die();
            $table->title("");
            $table->columns(array("a.id", "c.keterangan", "b.nomor", "b.penerbit_dok", "b.tgl_dok", "b.tgl_exp", "<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
            $this->newtable->width(array('Keterangan' => 350, 'Nomor Dokumen' => 100, 'Penerbit' => 200, 'Tanggal Awal' => 100, 'Tanggal Akhir' => 100));
            $this->newtable->search(array(array("nomor", "Nomor Penerbit"),
                array("penerbit_dok", "Penerbit")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_ahli/" . hashids_encrypt($id, _HASHIDS_, 9) . "/" . $doc);
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id"));
            $table->use_ajax(TRUE);
            $table->show_search(FALSE);
            $table->show_chk(FALSE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            //$table->settrid(TRUE);
            //$table->attrid($target);
            $table->tbtarget("refreq" . rand());
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function delete_siup4($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->db->trans_begin();
            foreach ($id as $data) {
                $this->db->delete('t_upload_syarat', array('detail_id' => $data));
                $this->db->delete('t_siup4_ahli', array('id' => trim($data)));
            }
            if ($this->db->trans_status === FALSE) {
                $this->db->trans_rollback();
                $msg = "MSG#Proses Gagal#refresh";
            } else {
                $this->db->trans_commit();
                $msg = "MSG#Proses Berhasil#refresh";
            }

            return $msg;
        }
    }

}

?>