<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Pameran_act extends CI_Model{
	
	var $ineng = "";
	
	function get_first($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			$arrdata = array();
			$idprop = $this->newsession->userdata('kdprop');
			$idkab = $this->newsession->userdata('kdkab');
			$idkec = $this->newsession->userdata('kdkec');
			$idkel = $this->newsession->userdata('kdkel');//print_r($idkel);die();
			$tipe_perusahaan = $this->newsession->userdata('tipe_perusahaan');
			

			$arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '".$idprop."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '".$idkab."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '".$idkec."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '".$idkel."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['direktorat'] = $dir;
			$arrdata['tipe'] = $type;
			$arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'","kode","uraian", TRUE);
			$arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN' AND kode = '".$tipe_perusahaan."' ","kode","uraian", TRUE);
			$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
			$nama_izin = $this->main->get_uraian($sql,'nama_izin');
			$arrdata['nama_izin'] = $nama_izin;
			
			if($id==""){
				$arrdata['act'] = site_url('post/licensing/pameran_act/first/save');
				//$type = hashids_decrypt($type,_HASHIDS_,6);
				$arrdata['sess']['tipe_permohonan'] = $type;
				$arrdata['sess']['kd_izin'] = $doc;
			}else{
				$arrdata['act'] = site_url('post/licensing/pameran_act/first/update');
				$query = "SELECT id, kd_izin, no_aju, tgl_aju,trader_id, tipe_permohonan, npwp, tipe_perusahaan, nm_perusahaan, almt_perusahaan, 
						kdprop, kdkab, kdkec, kdkel, kdpos, telp, fax
						FROM t_pameran WHERE id = '".$id."'";
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['sess'] = $row;
					}
					$arrdata['readonly'] = "readonly";
					$arrdata['disabled'] = "disabled";
					$arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '".$row['kdprop']."' ORDER BY 2","id", "nama", TRUE);
					$arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '".$row['kdkab']."' ORDER BY 2","id", "nama", TRUE);
					$arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '".$row['kdkec']."' ORDER BY 2","id", "nama", TRUE);
				}
			}
			return $arrdata;
		}
	}
	
	function set_first($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			$arrPameran = $this->main->post_to_query($this->input->post('PAMERAN'));
			$dir = $this->input->post('direktorat');
			$doc = $arrPameran['kd_izin'];
			$type = $arrPameran['tipe_permohonan'];
			$id_old = $arrPameran['id_permohonan_lama'];

			$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
			$nama_izin = $this->main->get_uraian($sql,'nama_izin');

			$msg = "MSG||NO||Data Permohonan ".$nama_izin." gagal disimpan";
			$res = FALSE;

			if($act == "save"){
				$arrPameran['no_aju'] = $this->main->set_aju();
				$arrPameran['fl_pencabutan'] = '0';
				$arrPameran['status'] = '0000';
				$arrPameran['tgl_aju'] = 'GETDATE()';
				$arrPameran['trader_id'] = $this->newsession->userdata('trader_id');
				$arrPameran['created'] = 'GETDATE()';
				$arrPameran['created_user'] = $this->newsession->userdata('username');
				if(($type != '01') && ($arrPameran['id_permohonan_lama'] != "")){
					if($type == '03') $arrPameran['fl_pembaharuan'] = "1";
					$query_old_doc = "SELECT identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, 
					                  alamat_pj, kdprop_pj, kdkab_pj, kdkel_pj, kdkec_pj,telp_pj, fax_pj, 
					                  awal_pameran, akhir_pameran,lokasi_pameran, kdprop_pameran, tema_pameran									 
									  FROM t_pameran WHERE id = ".$arrPameran['id_permohonan_lama']." AND kd_izin = ".$arrPameran['kd_izin'];
					$data_old = $this->main->get_result($query_old_doc);
					if($data_old){
						foreach($query_old_doc->result_array() as $keys){
							$jml = count($keys);
							$array_keys = array_keys($keys);
							$array_values = array_values($keys);
							for($i = 0; $i < $jml; $i++){
								$arrPameran[$array_keys[$i]] = $array_values[$i];
							}
						}
					}
				}
				
				$this->db->trans_begin();
				$exec = $this->db->insert('t_pameran', $arrPameran);
				if($this->db->affected_rows() > 0){
					//$res = TRUE;
					$idredir = $this->db->insert_id();
					if(($type != '01') && ($arrPameran['id_permohonan_lama'] != "") &&($doc == '16')){
						if(!$this->insert_event($idredir, $id_old)){
							$res = FALSE;
						}else{
							$res = TRUE;
						}
					}else{
						$res = TRUE;
					}
					$msg = "MSG||YES||Data Permohonan ".$nama_izin." berhasil disimpan.\nSilahkan lanjutkan mengisi data penanggung jawab||".site_url().'licensing/form/second/'.$dir.'/'.$doc.'/'.$type.'/'.$idredir;
					
					/* Log User dan Log Izin */
					$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$nama_izin.' dengan nomor permohonan : '.$arrPameran['no_aju'],
								  'url' => '{c}'. site_url().'post/licensing/pameran_act/first/save'. ' {m} models/licensing/pameran_act {f} set_first($act, $ajax)');
					$this->main->set_activity($logu);
					$logi = array('kd_izin' => $arrPameran['kd_izin'],
								  'permohonan_id' => $idredir,
								  'keterangan' => 'Menambahkan daftar permohonan '.$nama_izin.' dengan nomor permohonan : '.$arrPameran['no_aju'],
								  'catatan' => '',
								  'status' => '0000',
								  'selisih' => 0);
					$this->main->set_loglicensing($logi);			  
					/* Akhir Log User dan Log Izin */

				}
				if($this->db->trans_status() === FALSE || !$res){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				return $msg;
			}elseif ($act == "update") {
				$id = $this->input->post('id');
				$this->db->where('id',$id);
				$this->db->update('t_pameran',$arrPameran);
				if($this->db->affected_rows() > 0){
					$res = TRUE;
					$msg = "MSG||YES||Data Permohonan ".$nama_izin." berhasil diupdate.||REFRESH";

				}
				return $msg;
			}
		}
	}

	function get_second($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			$jenis_identitas = $this->newsession->userdata('tipe_identitas');
			$id_user = $this->newsession->userdata('id');
			$idprop = $this->newsession->userdata('kdprop_pj');
			$idkab = $this->newsession->userdata('kdkab_pj');
			$idkec = $this->newsession->userdata('kdkec_pj');
			$idkel = $this->newsession->userdata('kdkel_pj');
			$jabatan_pj = $this->newsession->userdata('jabatan_pj');
			$arrdata['act'] = site_url('post/licensing/pameran_act/second/save');
			$arrdata['direktorat'] = $dir;
			$arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '".$jenis_identitas."' and jenis = 'JENIS_IDENTITAS'","kode","uraian", TRUE);
			$arrdata['jabatan_pj'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JABATAN' AND kode = '".$jabatan_pj."' ","kode","uraian", TRUE);
			$arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '".$idprop."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['propinsi_tmpt'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2","id", "nama", TRUE);
			$arrdata['prop'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2","id", "nama", TRUE);
			$arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '".$idkab."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '".$idkec."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '".$idkel."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['urifirst'] = site_url('licensing/form/first/'.$dir.'/'.$doc.'/'.$type.'/'.$id);
			$query = "SELECT id, kd_izin, tipe_permohonan, identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, alamat_pj, kdprop_pj, kdkab_pj, kdkec_pj,kdkel_pj, telp_pj, fax_pj, kdprop, kdkab, kdkec, kdkel,awal_pameran, akhir_pameran, tema_pameran,lokasi_pameran, kdprop_pameran, kdkab_pameran, kdkec_pameran, kdkel_pameran
						FROM t_pameran WHERE id = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){//print_r($row);die();
					$arrdata['sess'] = $row;
				}
				if($row['identitas_pj']){
					$arrdata['act'] = site_url('post/licensing/pameran_act/second/update');
				}
				$arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '".$row['kdprop_pameran']."' ORDER BY 2","id", "nama", TRUE);
				$arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '".$row['kdkab_pameran']."' ORDER BY 2","id", "nama", TRUE);
				$arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '".$row['kdkec_pameran']."' ORDER BY 2","id", "nama", TRUE);
			}
			$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
			$nama_izin = $this->main->get_uraian($sql,'nama_izin');
			$arrdata['nama_izin'] = $nama_izin;
			return $arrdata;
		}
	}

	function set_second($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(($act == "update") || ($act == "save")){
				if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
				}
				$msg = "MSG||NO||Data Penanggung Jawab gagal disimpan";
				$respj = FALSE;
				$arrpj = $this->main->post_to_query($this->input->post('pj'));//print_r($arrpj);die();
				$dir = $this->input->post('direktorat');
				$doc = $this->input->post('kd_izin');
				$type = $this->input->post('tipe_permohonan');
				$id = $this->input->post('id');
				
				$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
				$nama_izin = $this->main->get_uraian($sql,'nama_izin');

				$this->db->where('id',$id);
				$this->db->update('t_pameran',$arrpj);
				if($this->db->affected_rows() > 0){
					$respj = TRUE;
					if($act == "update"){
						$msg = "MSG||YES||Data Penanggung Jawab berhasil diupdate.||REFRESH";
					}else{
						if($doc=='16'){
							$msg = "MSG||YES||Data Penanggung Jawab berhasil disimpan. \nSilahkan lanjutkan mengisi data penyelengaraan pameran/acara||".site_url().'licensing/form/third/'.$dir.'/'.$doc.'/'.$type.'/'.$id;
						}elseif($doc == '15'){
							$msg = "MSG||YES||Data Penanggung Jawab berhasil disimpan. \nSilahkan lanjutkan mengisi data persyaratan||".site_url().'licensing/form/fourth/'.$dir.'/'.$doc.'/'.$type.'/'.$id;
						}
					}
					/* Log User dan Log Izin */
					$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$nama_izin.' (Data Penanggung Jawab)',
								  'url' => '{c}'. site_url().'post/licensing/pameran_act/second/update'. ' {m} models/licensing/pameran_act {f} set_second($act, $isajax)');
					$this->main->set_activity($logu);
					/* Akhir Log User */
				}
				return $msg;
			}
		}
	}

	function get_third($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			if($id==""){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}else{
				$table = $this->newtable;
				//$arrdata['act'] = site_url('post/licensing/siupmb_act/third/save');
				$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
				$nama_izin = $this->main->get_uraian($sql,'nama_izin');
				$arrdata = array('id' => $id,
							 'direktorat' =>$dir,
							 'kd_izin' =>$doc,
							 'tipe_permohonan' =>$type,
							 'nama_izin'=>$nama_izin,
							 'act'=>site_url('post/licensing/siujs_act/third/save'),
							 'urifirst' => site_url('licensing/form/first/'.$dir.'/'.$doc.'/'.$type.'/'.$id),
							 'urisecond' => site_url('licensing/form/second/'.$dir.'/'.$doc.'/'.$type.'/'.$id),
							 'urifourth' => site_url('licensing/form/fourth/'.$dir.'/'.$doc.'/'.$type.'/'.$id));
							 
				$query = "SELECT id, nama as 'Nama Pameran', organizer as 'Penyelenggara', dbo.dateIndo(mulai)as 'Tanggal Mulai', dbo.dateIndo(berakhir) as 'Tanggal Berakhir'
							FROM t_pameran_event
						  	WHERE permohonan_id = '".$id."'";
				

				$table->title("");
				$table->columns(array("id","nama", "organizer", "mulai", "berakhir"));
				$this->newtable->width(array('Nama' => 200, 'Penyelengara' => 100, 'Tanggal Mulai' => 200, 'Tanggal Berkhir' => 100));
				$this->newtable->search(array(array("nama","Nama Pameran"),
											  array("organizer","Penyelenggara")));
				$table->cidb($this->db);
				$table->ciuri($this->uri->segment_array());
				$table->action(site_url()."licensing/form/third/".$dir."/".$doc."/".$type."/".$id);
				//$table->orderby(1);
				$table->sortby("ASC");
				$table->keys(array("id"));
				$table->hiddens(array("id"));
				$table->show_search(TRUE);
				$table->show_chk(TRUE);
				$table->single(TRUE);
				$table->dropdown(TRUE);
				$table->hashids(TRUE);
				$table->use_ajax(TRUE);
				$table->postmethod(TRUE);
				$table->title(TRUE);
				$table->tbtarget("a");
				$table->menu(array('Tambah' => array('GET', site_url().'licensing/view/pameran_event/'.$doc.'/'.$id, '0', 'home','modal'),
								   'Edit' => array('BOOTSTRAPDIALOG', site_url().'licensing/view/pameran_event/'.$doc.'/'.$id.'/', '1', 'fa fa-edit'),
								   'Delete' => array('POST', site_url().'licensing/delete_pameran/ajax', 'N', 'fa fa-times','isngajax')
								   ));
				$arrdata['lstPameran'] = $table->generate($query);
				//print_r($arrdata);die();
				if($this->input->post("data-post")) return $table->generate($query);
				else return $arrdata;
			}
		}
	}

	function get_pameran_event($permohonan_id, $dtl){
		if($this->newsession->userdata('_LOGGED')){
			$detail_id = hashids_decrypt($dtl,_HASHIDS_,9);
			$arrdata['act'] = site_url('post/licensing/pameran_act/third/save');
			$arrdata['permohonan_id'] = $permohonan_id;
			$arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2","id", "nama", TRUE);
			if($detail_id){
				$query_event= "SELECT * FROM t_pameran_event WHERE permohonan_id = '".$permohonan_id."' and id = '".$detail_id."'";
				$data_event = $this->main->get_result($query_event);
				if ($data_event) {
					foreach($query_event->result_array() as $row){
						$arrdata['event'] = $row;
						$arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '".$row[kdkab]."' ORDER BY 2","id", "nama", TRUE);
						$arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '".$row[kdkec]."' ORDER BY 2","id", "nama", TRUE);
						$arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '".$row[kdkel]."' ORDER BY 2","id", "nama", TRUE);
					}
					$arrdata['act'] = site_url('post/licensing/pameran_act/third/update');
				}
			}	
			//print_r($arrdata);die();
			return $arrdata;
		}
	}

	function set_third($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			$txt = "Persetujuan Prinsip Pameran Dagang, Konvensi, Dan Atau Seminar Dagang Internasional";
			$msg = "MSG||NO||Data Penyelengaraan Pameran/Acara gagal disimpan";
			$resEvent = FALSE;
			$arrEvent = $this->main->post_to_query($this->input->post('EVENT'));//print_r($arrEvent);die();
			$permohonan_id = hashids_decrypt($this->input->post('permohonan_id'),_HASHIDS_,9);
			if($act == "save"){
				$arrEvent['permohonan_id'] = $permohonan_id;
				$this->db->trans_begin();
				$exec = $this->db->insert('t_pameran_event', $arrEvent);

				if($this->db->affected_rows() > 0){
					$resEvent = TRUE;
					$msg = "MSG||YES||Data Penyelengaraan Pameran/Acara berhasil disimpan.||REFRESH";
					
					/* Log User dan Log Izin */
					$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' (Data Penyelengaraan Pameran/Acara)',
								  'url' => '{c}'. site_url().'post/licensing/pameran_act/third/save'. ' {m} models/licensing/pameran_act {f} set_third($act, $isajax)');
					$this->main->set_activity($logu);
					/* Akhir Log User dan Log Izin */

				}
				if($this->db->trans_status() === FALSE || !$resEvent){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				return $msg;
			}elseif ($act == "update") {
				//print_r($_POST);die();
				$id = hashids_decrypt($this->input->post('id'),_HASHIDS_,9);
				$this->db->where('id',$id);
				$this->db->where('permohonan_id',$permohonan_id);
				$this->db->update('t_pameran_event',$arrEvent);
				if($this->db->affected_rows() > 0){
					$resProd = TRUE;
					$msg = "MSG||YES||Data Penyelengaraan Pameran/Acara berhasil diperbaharui.||REFRESH";

					$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' (Update Data Penyelengaraan Pameran/Acara)',
								  'url' => '{c}'. site_url().'post/licensing/pameran_act/third/update'. ' {m} models/licensing/pameran_act {f} set_third($act, $isajax)');
					$this->main->set_activity($logu);

				}
				return $msg;
			}
		}
	}

	function get_fourth($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			if ($type == '01') {
				$addCon = "AND a.baru = '1'";
			}elseif ($type=='02') {
				$addCon = "AND a.perubahan = '1'";
			}elseif ($type=='03') {
				$addCon = "AND a.perpanjangan = '1'";
			}
			$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
			$nama_izin = $this->main->get_uraian($sql,'nama_izin');
			$arrdata['nama_izin'] = $nama_izin;
			$arrdata['izin_id'] = $doc;
			$arrdata['direktorat'] = $dir;
			$arrdata['type'] = $type;
			$arrdata['permohonan_id'] = $id;
			$arrdata['urithird'] = site_url('licensing/form/third/'.$dir.'/'.$doc.'/'.$type.'/'.$id);
			$query_syarat ="SELECT a.id, a.izin_id, a.dok_id, a.upload_id,
							 case when c.multi = 1 then '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor else '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor end AS nomor,
							  b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							 '<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							 FROM t_upload_syarat a 
							 LEFT join t_upload b on b.id = a.upload_id
							 LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id
							 WHERE a.izin_id = '".$doc."' and a.permohonan_id = '".$id."' and a.detail_id is null";
			/*$query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							'<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id 
							WHERE a.izin_id = '".$doc."' and a.permohonan_id = '".$id."' and a.detail_id is null";*/
			$data = $this->main->get_result($query_syarat);
			$arr = array();

			$queryReq = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian, a.multi
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.izin_id = '".$doc."' ".$addCon;
			$data_req = $this->main->get_result($queryReq);
			$temp = $queryReq->result_array();
			$arrdata['req'] = $temp;

			if($data){
				foreach($query_syarat->result_array() as $keys){
					//$arr[$keys['dok_id']] = $keys;
					if (!isset($arr[$keys['dok_id']])){
					  $arr[$keys['dok_id']] = array();
					}
					$arr[$keys['dok_id']][] = $keys;
				}
				$arrdata['sess'] = $arr;
				$arrdata['act'] = site_url('post/licensing/pameran_act/fourth/update');

			}else{
				foreach ($temp as $datadt) {
                    $query_syarat2 = "  SELECT top 1 b.id as 'upload_id', b.tipe_dok, b.tipe_dok as  dok_id,  CASE WHEN a.multi = 1 THEN ' <a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp; ' + b.nomor ELSE ' <a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp; ' + b.nomor END AS nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
					'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View' 
                                            FROM t_upload b left join m_dok_izin a on b.tipe_dok = a.dok_id 
					where a.izin_id = '" . $doc . "' and b.trader_id = '" . $this->newsession->userdata('trader_id') . "'and b.tipe_dok = '" . $datadt['dok_id'] . "'  " . $addCon . " order by tgl_dok desc;";
                    $cek = $this->db->query($query_syarat2)->num_rows();
                    //print_r($a);die();
                    $data2 = $this->main->get_result($query_syarat2);
                    if ($cek != 0) {
                        foreach ($query_syarat2->result_array() as $keys) {
                            if (!isset($arr[$keys['dok_id']])) {
                                $arr[$keys['dok_id']] = array();
                            }
                            $arr[$keys['dok_id']][] = $keys;
                        }   
                    }
                    $arrdata['sess'] = $arr;
                }
				$arrdata['baru'] = true;
				$arrdata['act'] = site_url('post/licensing/pameran_act/fourth/save');
			}
			return $arrdata;
		}
	}

	function set_fourth($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			$msg = "MSG||NO||Data gagal disimpan";
			$ressiup = FALSE;
			$arrsiup = $this->main->post_to_query($this->input->post('SIUPMB'));
			$arrreq = $this->input->post('REQUIREMENTS');
			$arrkeys = array_keys($arrreq);

			$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$arrsiup['izin_id']."'";
			$nama_izin = $this->main->get_uraian($sql,'nama_izin');

			if($act == "save"){
				for($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++){
					$requirements = array('izin_id' => $arrsiup['izin_id'],
					     'permohonan_id' => $arrsiup['permohonan_id'],
					     'created' => 'GETDATE()',
					     'created_user' => $this->newsession->userdata('username'));
					for($j=0;$j<count($arrkeys);$j++){
						$requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
					}
					unset($requirements['id']);
					if($requirements['upload_id'] != "")
						$this->db->insert('t_upload_syarat', $requirements);
				}
				if($this->db->affected_rows() > 0){
					$ressiup = TRUE;
					$idUpload = $this->db->insert_id();
					/* Log User dan Log Izin */
					$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$nama_izin.' (Data Persyaratan)',
								  'url' => '{c}'. site_url().'post/licensing/pameran_act/fourth/save'. ' {m} models/licensing/pameran_act {f} set_fourth($act, $isajax)');
					$this->main->set_activity($logu);
					/* Akhir Log User */
					$msg = "MSG||YES||Data Persyaratan berhasil disimpan. Pengajuan Permohonan ".$nama_izin." selesai.||".site_url().'licensing/view/status';
				}
				if($this->db->trans_status() === FALSE || !$ressiup){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
			}elseif($act == "update"){
				$this->db->where('permohonan_id', $arrsiup['permohonan_id']);
				$this->db->where('izin_id', $arrsiup['izin_id']);
        		$this->db->delete('t_upload_syarat');
        		if($this->db->affected_rows() > 0){
        			for($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++){
						$requirements = array('izin_id' => $arrsiup['izin_id'],
						     'permohonan_id' => $arrsiup['permohonan_id'],
						     'created' => 'GETDATE()',
						     'created_user' => $this->newsession->userdata('username'));
						for($j=0;$j<count($arrkeys);$j++){
							$requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
						}
						unset($requirements['id']);
						if($requirements['upload_id'] != "")
							$this->db->insert('t_upload_syarat', $requirements);
					}
					if($this->db->affected_rows() > 0){
						$ressiup = TRUE;
						$idUpload = $this->db->insert_id();
						/* Log User dan Log Izin */
						$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$nama_izin.' (Update - Data Persyaratan)',
									  'url' => '{c}'. site_url().'post/licensing/pameran_act/fourth/update'. ' {m} models/licensing/pameran_act {f} set_fourth($act, $isajax)');
						$this->main->set_activity($logu);
						/* Akhir Log User */
						$msg = "MSG||YES||Data Persyaratan berhasil diupdate.||REFRESH";
					}
					if($this->db->trans_status() === FALSE || !$ressiup){
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();
					}
        		}
			}
			return $msg;
		}
	}

	function get_preview($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			if ($type == '01') {
				$addCon = "AND a.baru = '1'";
			}elseif ($type=='02') {
				$addCon = "AND a.perubahan = '1'";
			}elseif ($type=='03') {
				$addCon = "AND a.perpanjangan = '1'";
			}

			$query = "SELECT a.id, b.direktorat_id, a.kd_izin, a.no_aju, a.trader_id, a.tipe_permohonan, c.uraian AS permohonan, a.npwp, 
					d.uraian AS tipe_perusahaan, a.nm_perusahaan, 
					a.almt_perusahaan, dbo.get_region(2, a.kdprop) AS prop_perusahaan, 
					dbo.get_region(4, a.kdkab) AS kab_perusahaan, dbo.get_region(7, a.kdkec) AS kec_perusahaan, 
					dbo.get_region(10, a.kdkel) AS kel_perusahaan,
					a.kdpos, a.telp, a.fax, i.uraian as identitas_pj, a.nama_pj, a.noidentitas_pj, z.uraian as jabatan_pj, a.tmpt_lahir_pj, dbo.dateIndo(a.tgl_lahir_pj)as tgl_lahir_pj, a.alamat_pj, 
					a.almt_perusahaan, dbo.get_region(2, a.kdprop_pj) AS prop_pj, dbo.get_region(4, a.kdkab_pj) AS kab_pj, dbo.get_region(7, a.kdkec_pj) AS kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj,
					a.telp_pj, a.fax_pj, a.lokasi_pameran, dbo.dateIndo(a.awal_pameran) as awal_pameran , dbo.dateIndo(a.akhir_pameran)as akhir_pameran, a.tema_pameran,dbo.get_region(2, a.kdprop_pameran) AS kdprop_pameran, a.status, b.nama_izin, b.disposisi, a.kdkab_pameran, a.kdkec_pameran, a.kdkel_pameran, a.no_aju, dbo.dateIndo(a.tgl_kirim) as tgl_aju
					FROM t_pameran a 
					LEFT JOIN m_izin b ON b.id = a.kd_izin 
					LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
					LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
					LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN' 
					WHERE a.id = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['sess'] = $row;
				}
				

				$arrdata['act'] = site_url().'post/proccess/pameran_act/verification';
				$arrdata['input'] = $this->get_input($dir, $doc, $type, $id, $row['status']);
				$arrdata['proses'] = $this->main->set_proses($dir, $row['kd_izin'], $row['disposisi'], $this->newsession->userdata('role'), $row['status'],hashids_encrypt($row['id'],_HASHIDS_,9));
				$arrdata['jmllog'] = $this->main->get_uraian("SELECT COUNT(*) AS JML FROM t_log_izin WHERE permohonan_id = '".$row['id']."' AND kd_izin = '".$row['kd_izin']."'","JML");
				$arrdata['urllog'] = site_url().'get/log/izin/'.hashids_encrypt($row['id'],_HASHIDS_,9).'/'.hashids_encrypt($row['kd_izin'],_HASHIDS_,9);

				if($doc == '16'){
					$query = "SELECT id, nama, organizer, dbo.dateIndo(mulai)as 'tgl_mulai', dbo.dateIndo(berakhir) as 'tgl_akhir'
							FROM t_pameran_event
						  	WHERE permohonan_id = '".$id."'";//print_r($query);die();
					$arrdata['lstPameran'] = $this->db->query($query)->result_array();
					// $table = $this->newtable;
				
					// $query = "SELECT id, nama as 'Nama Pameran', organizer as 'Penyelenggara', dbo.dateIndo(mulai)as 'Tanggal Mulai', dbo.dateIndo(berakhir) as 'Tanggal Berakhir'
					// 		FROM t_pameran_event
					// 	  	WHERE permohonan_id = '".$id."'";
		
					// $table->title("");
					// $table->columns(array("id","nama", "no_sertifikat", "penerbit", "warganegara"));
					// $this->newtable->width(array('Nama' => 200, 'No. Sertifikat' => 100, 'Penerbit' => 100, 'Kewarganegaraan' => 100));
					// $this->newtable->search(array(array("nama","Nama Pameran"),
					// 							  array("organizer","Penyelenggara")));
					// $table->cidb($this->db);
					// $table->ciuri($this->uri->segment_array());
					// $table->action(site_url()."/licensing/form/third/".$dir."/".$doc."/".$type."/".$id);
					// $table->orderby(1);
					// $table->sortby("ASC");
					// $table->keys(array("id"));
					// $table->hiddens(array("id"));
					// $table->show_search(TRUE);
					// $table->show_chk(FALSE);
					// $table->single(TRUE);
					// $table->dropdown(TRUE);
					// $table->hashids(TRUE);
					// $table->postmethod(TRUE);
					// $table->tbtarget("reffourth");
					// $arrdata['lstPameran'] = $table->generate($query);
				}
			}


			

			$query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, dbo.dateIndo(b.tgl_dok) as tgl_dok, dbo.dateIndo(b.tgl_exp) as tgl_exp, b.penerbit_dok,
							'<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id
							LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id  
							WHERE a.izin_id = ".$doc." and a.permohonan_id = '".$id."'";
			$data = $this->main->get_result($query_syarat);
			$arr = array();
			if($data){
				foreach($query_syarat->result_array() as $keys){
					if (!isset($arr[$keys['dok_id']])){
					  $arr[$keys['dok_id']] = array();
					}
					$arr[$keys['dok_id']][] = $keys;
				}
				$arrdata['sess_syarat'] = $arr;

			}
			$query= "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.izin_id = '".$doc."' ".$addCon." ORDER BY a.urutan ASC";
			$data_req = $this->main->get_result($query);
			if($data_req){
				$arrdata['req'] = $query->result_array();
			}
			$arrdata['dir'] = $dir;
			$arrdata['telaah'] = $this->main->get_telaah($doc);
            $arrdata['agrement'] = $this->main->get_agrement($doc);
			return $arrdata;
		}
	}
	
	function get_input($dir, $doc, $type, $id, $stts){
        if($this->newsession->userdata('_LOGGED')){
            $arrstts = array('0102');
            $arrdata = array();
            if(in_array($stts, $arrstts)){
                $query = "SELECT a.id, a.kd_izin, a.no_aju, CONVERT(VARCHAR(10), a.tgl_aju, 103) AS tgl_aju, a.no_izin, CONVERT(VARCHAR(10), a.tgl_izin, 103) as tgl_izin, CONVERT(VARCHAR(10), a.tgl_izin_exp, 103) AS tgl_izin_exp FROM t_pameran a WHERE a.id = '".$id."' AND a.kd_izin = '".$doc."'";
                $ret = $this->main->get_result($query);
                if($ret){
					$this->ineng = $this->session->userdata('site_lang'); 
                    foreach ($query->result_array() as $row){
                        $arrdata['sess'] = $row;
                    }
					$arrdata['dir'] = $dir;
					$arrdata['doc'] = $doc;
                }
                $arrdata['pejabat_ttd'] = $this->main->set_combobox("SELECT id, nama FROM m_ttd WHERE status = 1 AND kd_izin = ".$doc,"id","nama", TRUE);
				if(!$this->session->userdata('site_lang')) $this->ineng = "id";
                $data = $this->load->view($this->ineng.'/backend/input/'.$dir.'/'.$stts, $arrdata, true);
            } else {
                $data = "";
            }
        }
        return $data;
    }

	function set_onfly($act, $id, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if($act == "update"){
				if(!$isajax){
					return false;
					exit();
				}
				$msg = "MSG||NO";
				$respon = FALSE;
				$arrsiup = $this->main->post_to_query($this->input->post('dataon'));
				$id = hashids_decrypt($id, _HASHIDS_, 9);
				$this->db->where('id',$id);
				$this->db->update('t_pameran',$arrsiup);
				if($this->db->affected_rows() == 1){
					$respon = TRUE;
					$logu = array('aktifitas' => 'Melakukan update tanggal izin / tanggal masa berlaku',
								  'url' => '{c}'. site_url().'get/onfly/onfly_act/update'. ' {m} models/licensing/pameran_act {f} set_onfly($act,  $id, $isajax)');
					$this->main->set_activity($logu);
				}
				if($respon) $msg = "MSG||YES";
				return $msg;
			}
		}
	}
	
	
	function referensi_kbli($target, $callback, $fieldcallback){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable;
			$query = "SELECT kode as kbli, uraian as desc_kbli, kode AS 'Kode KBLI', uraian AS 'Uraian KBLI' FROM m_kbli";
			$table->title("");
			$table->columns(array("kode","Kode KBLI", "uraian", "Uraian KBLI"));
			$this->newtable->width(array('Kode KBLI' => 100, 'Uraian' => 300, '&nbsp;' => 5));
			$this->newtable->search(array(array("kode","Kode KBLI"),
										  array("uraian","Uraian")));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."/licensing/popup_kbli/".$target.'/'.$callback.'/'.$fieldcallback);
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("kode"));
			$table->hiddens(array("kbli","desc_kbli"));
			$table->use_ajax(TRUE);
			$table->show_search(TRUE);
			$table->show_chk(FALSE);
			$table->single(TRUE);
			$table->dropdown(TRUE);
			$table->postmethod(TRUE);
			$table->settrid(TRUE);
			$table->attrid($target);
			if($callback!="")$table->callback(site_url(str_replace(".",'/',$callback)));
			if($fieldcallback!="") $table->fieldcallback($fieldcallback);
			$table->tbtarget("refkbli");
			$arrdata = array('tabel' => $table->generate($query));
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	function list_dok($target, $callback, $fieldcallback, $id, $multiple, $putin, $doc){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable;
			$query = "SELECT id, id AS 'upload_id', nomor AS 'No. Dokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'Tgl. Dokumen', tgl_exp AS 'Tgl. Akhir', '<a href=\"".site_url()."download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
			FROM t_upload WHERE tipe_dok = '".$id."' AND trader_id = '".$this->newsession->userdata('trader_id')."'";
			$table->title("");
			$table->columns(array("id", "nomor", "penerbit_dok", "tgl_dok", "tgl_exp", "<a href=\"".site_url()."download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
			$this->newtable->width(array('Nomor Dokumen' => 100, 'Penerbit' => 300, 'Tanggal Awal' => 100,'Tanggal Akhir' => 100,'&nbsp;' => 5));
			$this->newtable->search(array(array("nomor","Nomor Penerbit"),
										  array("penerbit_dok","Penerbit")));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."/licensing/popup_syarat/".$id.'/'.$target.'/'.$callback.'/'.$fieldcallback.'/');
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("id"));
			$table->hiddens(array("id","folder","nama_file","upload_id"));
			$table->show_search(TRUE);
			$table->single(TRUE);
			$table->dropdown(TRUE);
			$table->postmethod(TRUE);
			
			if((int)$multiple == 1){ 
				$table->show_chk(TRUE);
				$table->menu(array('Pilih Data' => array('POSTGET', site_url().'post/document/get_requirements/'.$doc.'/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#'.$putin)));
			}else{
				$table->show_chk(FALSE);
				if($callback!="")$table->callback(site_url(str_replace(".",'/',$callback)));
				if($fieldcallback!="") $table->fieldcallback($fieldcallback);
				$table->settrid(TRUE);
				$table->attrid($target);
			}
			$table->tbtarget("refreq_".rand());
			$arrdata = array('tabel' => $table->generate($query));
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}
	
	function get_requirements($doc){
		$id = join("','", $this->input->post('tb_chk'));
		$data = "SELECT a.id AS dok_ids, b.id AS id_upload, b.nomor, b.penerbit_dok, b.tgl_dok, b.tgl_exp, '<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS files, c.tipe FROM m_dok a LEFT JOIN t_upload B ON a.id = b.tipe_dok LEFT JOIN m_dok_izin c ON c.dok_id = a.id WHERE b.id IN('".$id."') AND c.izin_id = '".$doc."'";
		//print_r($data);die();
		return $this->db->query($data)->result_array();
	}

	function referensi_old_doc($target, $callback, $fieldcallback, $dir, $doc){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable;
			$query = "SELECT id, id as id_old, no_aju as 'No. Pengajuan', no_izin as 'Nomor Izin', tgl_izin as 'Tanggal Izin', tgl_izin_exp as 'Tanggal Akhir Izin'
					  FROM t_pameran  
					  WHERE kd_izin = ".$doc." AND status = '1000'";
			$table->title("");
			$table->columns(array("id","No. Pengajuan", "Nomor Izin", "Tanggal Izin", "Tanggal Akhir Izin"));
			$this->newtable->width(array('No. Pengajuan' => 100, 'Nomor Izin' => 100, 'Tanggal Izin' => 100, 'Tanggal Akhir Izin' => 100, '&nbsp;' => 5));
			$this->newtable->search(array(array("no_aju","No. Pengajuan"),
										  array("no_izin","Nomor Izin")));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."/licensing/popup_old_doc/".$target.'/'.$callback.'/'.$fieldcallback);
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("id"));
			$table->hiddens(array("id","id_old"));
			$table->use_ajax(TRUE);
			$table->show_search(TRUE);
			$table->show_chk(FALSE);
			$table->single(TRUE);
			$table->dropdown(TRUE);
			$table->postmethod(TRUE);
			$table->settrid(TRUE);
			$table->attrid($target);
			if($callback!="")$table->callback(site_url(str_replace(".",'/',$callback)));
			if($fieldcallback!="") $table->fieldcallback($fieldcallback);
			$table->tbtarget("refolddoc");
			$arrdata = array('tabel' => $table->generate($query));
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	function insert_event($id, $id_old){
		$ret = FALSE;
		$query_old_event = "SELECT nama, organizer, mulai, berakhir FROM t_pameran_event WHERE permohonan_id = '".$id_old."'";
		$data_old_event = $this->main->get_result($query_old_event);
		if($data_old_event){
			foreach($query_old_event->result_array() as $keys){
				$arrEvent['permohonan_id'] = $id;
				$jml = count($keys);
				$array_keys = array_keys($keys);
				$array_values = array_values($keys);
				for($i = 0; $i < $jml; $i++){
					$arrEvent[$array_keys[$i]] = $array_values[$i];
				}
				$this->db->insert('t_pameran_event', $arrEvent);
			}
			if ($this->db->affected_rows() > 0) {
				$ret = TRUE;
			}
		}
		return $ret;
	}
	
	function delete_pameran($data){
		if($this->newsession->userdata('_LOGGED')){
			$this->db->trans_status();
			foreach($data as $id){
				if($this->db->delete('t_pameran_event',array('id'=>hashids_decrypt($id,_HASHIDS_,9)))){
					$i++;
				}
			}
			if($this->db->trans_status === FALSE || $i == 0){
				$this->db->trans_rollback();
				$msg = "MSG#Proses Gagal#refresh";
			}else{
				$this->db->trans_commit();
				$msg = "MSG#Proses Berhasil#refresh";
			}
			return $msg;
		}
	}	
}
?>