<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Siupl_act extends CI_Model{
	var $ineng = "";
	
	function get_first($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			$arrdata = array();
			$idprop = $this->newsession->userdata('kdprop');
			$idkab = $this->newsession->userdata('kdkab');
			$idkec = $this->newsession->userdata('kdkec');
			$idkel = $this->newsession->userdata('kdkel');//print_r($idkel);die();
			$tipe_perusahaan = $this->newsession->userdata('tipe_perusahaan');
			$arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '".$idprop."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '".$idkab."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '".$idkec."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '".$idkel."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['direktorat'] = $dir;
			$arrdata['tipe'] = $type;
			$arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'","kode","uraian", TRUE);
			$arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '".$tipe_perusahaan."' AND jenis = 'TIPE_PERUSAHAAN'","kode","uraian", TRUE);
			$arrdata['kategori'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'KATEGORI_SIUPBB'","kode","uraian", TRUE);
			$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
			$nama_izin = $this->main->get_uraian($sql,'nama_izin');
			$arrdata['nama_izin'] = $nama_izin;
			
			if($id==""){
				$arrdata['act'] = site_url('post/licensing/siupbb_act/first/save');
				$arrdata['sess']['tipe_permohonan'] = $type;
				$arrdata['sess']['kd_izin'] = $doc;
			}else{
				$arrdata['act'] = site_url('post/licensing/siupbb_act/first/update');
				$query = "SELECT id, kd_izin, no_aju, tgl_aju, trader_id, tipe_permohonan, kategori, npwp, tipe_perusahaan, nm_perusahaan, almt_perusahaan, 
						kdprop, kdkab, kdkec, kdkel, kdpos, telp, fax, id_permohonan_lama, no_izin_lama, tgl_izin_lama, tgl_izin_exp_lama
						FROM t_siupbb WHERE id = '".$id."'";
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['sess'] = $row;
					}
					$arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '".$row['kdprop']."' ORDER BY 2","id", "nama", TRUE);
					$arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '".$row['kdkab']."' ORDER BY 2","id", "nama", TRUE);
					$arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '".$row['kdkec']."' ORDER BY 2","id", "nama", TRUE);
				
			}
			}
			return $arrdata;
		}
	}
	
	function set_first($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			$txt = "Surat Izin Usaha Perdagangan Bahan Berbahaya (SIUP B2) sebagai DT-B2";
			$msg = "MSG||NO||Data Permohonan ".$txt." gagal disimpan";
			$ressiup = FALSE;
			$arrsiup = $this->main->post_to_query($this->input->post('siupbb'));
			$dir = $this->input->post('direktorat');
			$doc = $arrsiup['kd_izin'];
			$type = $arrsiup['tipe_permohonan'];
			$id_old = $arrsiup['id_permohonan_lama'];
			if($act == "save"){
				$arrsiup['no_aju'] = $this->main->set_aju();
				$arrsiup['fl_pencabutan'] = '0';
				$arrsiup['status'] = '0000';
				$arrsiup['tgl_aju'] = 'GETDATE()';
				$arrsiup['trader_id'] = $this->newsession->userdata('trader_id');
				$arrsiup['created'] = 'GETDATE()';
				$arrsiup['created_user'] = $this->newsession->userdata('username');
				if(($type != '01') && ($arrsiup['id_permohonan_lama'] != "")){
					if($type == '03') $arrsiup['fl_pembaharuan'] = "1";
					$query_old_doc = "SELECT identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, nama_produksi,
									  alamat_pj, kdprop_pj, kdkab_pj, kdkel_pj, kdkec_pj,telp_pj, fax_pj, nilai_modal,produksi, perdagangan, jenis_dagangan
									  FROM t_siupbb WHERE id = ".$arrsiup['id_permohonan_lama']." AND kd_izin = ".$arrsiup['kd_izin'];
					$data_old = $this->main->get_result($query_old_doc);
					if($data_old){
						foreach($query_old_doc->result_array() as $keys){
							$jml = count($keys);
							$array_keys = array_keys($keys);
							$array_values = array_values($keys);
							for($i = 0; $i < $jml; $i++){
								$arrsiup[$array_keys[$i]] = $array_values[$i];
							}
						}
					}
				}
				//print_r($arrsiup);die();
				$this->db->trans_begin();
				$exec = $this->db->insert('t_siupbb', $arrsiup);
				//print_r($this->db->last_query());die();
				if($this->db->affected_rows() > 0){
					$idredir = $this->db->insert_id();
					if(($type != '01') && ($arrsiup['id_permohonan_lama'] != "")){
						// if(!$this->insert_bank($idredir, $id_old)){
						// 	$ressiup = FALSE;
						// }else{die("masuk");
						// 	$ressiup = TRUE;
						// }
						$ressiup = TRUE;
					}else{
						$ressiup = TRUE;
					}print_r($ressiup);
					$msg = "MSG||YES||Data Permohonan ".$txt." berhasil disimpan. Silahkan lanjutkan mengisi data penanggung jawab||".site_url().'licensing/form/second/'.$dir.'/'.$doc.'/'.$type.'/'.$idredir;
					
					/* Log User dan Log Izin */
					$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' dengan nomor permohonan : '.$arrsiup['no_aju'],
								  'url' => '{c}'. site_url().'post/licensing/siupbb_act/first/save'. ' {m} models/licensing/siupbb_act {f} set_first($act, $ajax)');
					$this->main->set_activity($logu);
					$logi = array('kd_izin' => $arrsiup['kd_izin'],
								  'permohonan_id' => $idredir,
								  'keterangan' => 'Menambahkan daftar permohonan '.$txt.' dengan nomor permohonan : '.$arrsiup['no_aju'],
								  'catatan' => '',
								  'status' => '0000',
								  'selisih' => 0);
					$this->main->set_loglicensing($logi);			  
					/* Akhir Log User dan Log Izin */

				}
				if($this->db->trans_status() === FALSE || !$ressiup){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				return $msg;
			}elseif ($act == "update") {
				$id = $this->input->post('id');
				$this->db->where('id',$id);
				$this->db->update('t_siupbb',$arrsiup);
				if($this->db->affected_rows() > 0){
					$ressiup = TRUE;
					$msg = "MSG||YES||Data Permohonan ".$txt." berhasil diupdate.||REFRESH";

				}
				return $msg;
			}
		}
	}

	function get_second($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			$jenis_identitas = $this->newsession->userdata('tipe_identitas');
			$id_user = $this->newsession->userdata('id');
			$idprop = $this->newsession->userdata('kdprop_pj');
			$idkab = $this->newsession->userdata('kdkab_pj');
			$idkec = $this->newsession->userdata('kdkec_pj');
			$idkel = $this->newsession->userdata('kdkel_pj');
			$jabatan_pj = $this->newsession->userdata('jabatan_pj');
			$arrdata['act'] = site_url('post/licensing/siupbb_act/second/save');
			$arrdata['direktorat'] = $dir;
			$arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '".$jenis_identitas."' and jenis = 'JENIS_IDENTITAS'","kode","uraian", TRUE);
			$arrdata['jabatan_pj'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JABATAN' AND kode = '".$jabatan_pj."' ","kode","uraian", TRUE);
			$arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '".$idprop."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '".$idkab."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '".$idkec."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '".$idkel."' ORDER BY 2","id", "nama", TRUE);
			$arrdata['urifirst'] = site_url('licensing/form/first/'.$dir.'/'.$doc.'/'.$type.'/'.$id);
			$query = "SELECT id, kd_izin, tipe_permohonan, identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, alamat_pj, kdprop_pj, kdkab_pj, kdkec_pj,kdkel_pj, telp_pj, fax_pj, kdprop, kdkab, kdkec, kdkel, nilai_modal
						FROM t_siupbb WHERE id = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['sess'] = $row;
				}
				if($row['identitas_pj']){
					$arrdata['act'] = site_url('post/licensing/siupbb_act/second/update');
				}
				$arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '".$row['kdprop_pj']."' ORDER BY 2","id", "nama", TRUE);
				$arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '".$row['kdkab_pj']."' ORDER BY 2","id", "nama", TRUE);
				$arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '".$row['kdkec_pj']."' ORDER BY 2","id", "nama", TRUE);
			}
			$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
			$nama_izin = $this->main->get_uraian($sql,'nama_izin');
			$arrdata['nama_izin'] = $nama_izin;
			return $arrdata;
		}
	}

	function set_second($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(($act == "update") || ($act == "save")){
				if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
				}
				$msg = "MSG||NO||Data Penanggung Jawab gagal disimpan";
				$respj = FALSE;
				$arrpj = $this->main->post_to_query($this->input->post('pj'));

				$nilai_modal = $this->input->post('nilai_modal');
				$arrpj['nilai_modal'] = str_replace(",", "", $nilai_modal);
				
				$dir = $this->input->post('direktorat');
				$doc = $this->input->post('kd_izin');
				$type = $this->input->post('tipe_permohonan');
				$id = $this->input->post('id');
				$txt = "Surat Izin Usaha Perdagangan Bahan Berbahaya (SIUP B2) sebagai DT-B2";
				$this->db->where('id',$id);
				$this->db->update('t_siupbb',$arrpj);
				//print_r($this->db->last_query());die();
				if($this->db->affected_rows() > 0){
					$respj = TRUE;
					if($act == "update"){
						$msg = "MSG||YES||Data Penanggung Jawab berhasil diupdate.||REFRESH";
					}else{
						$msg = "MSG||YES||Data Penanggung Jawab berhasil disimpan. Silahkan lanjutkan mengisi data Kegiatan Usaha||".site_url().'licensing/form/third/'.$dir.'/'.$doc.'/'.$type.'/'.$id;
					}
					/* Log User dan Log Izin */
					$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' (Data Penanggung Jawab)',
								  'url' => '{c}'. site_url().'post/licensing/siupbb_act/second/update'. ' {m} models/licensing/siupbb_act {f} set_second($act, $isajax)');
					$this->main->set_activity($logu);
					/* Akhir Log User */
				}
				return $msg;
			}
		}
	}

	function get_third($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			if($id==""){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}else{
				//print_r("expressionmodel");die();
				$arrdata['act'] = site_url('post/licensing/siupbb_act/third/save');
				$arrdata['urisecond'] = site_url('licensing/form/second/'.$dir.'/'.$doc.'/'.$type.'/'.$id);
				$arrdata['lokasi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2","id", "nama", TRUE);
				$arrdata['produksi'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff where jenis = 'PRODUKSI'","kode", "uraian", TRUE);
                                $arrdata['banyak'] = 0;
				$query = "SELECT id, kd_izin, tipe_permohonan, produksi, perdagangan, jenis_dagangan, nama_produksi
						  FROM t_siupbb WHERE id = ".$id;
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['sess'] = $row;
					}
					if ($row['produksi']) {
						$arrdata['act'] = site_url('post/licensing/siupbb_act/third/update');
					}
				}

				$query_bank = "SELECT id, permohonan_id, nama, alamat, lokasi FROM t_siupbb_bank WHERE permohonan_id = ".$id;
				$data_bank = $this->main->get_result($query_bank);
				if($data_bank){
					$arrdata['bank'] = $query_bank->result_array();
				}
				
				$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
				$nama_izin = $this->main->get_uraian($sql,'nama_izin');
				$arrdata['nama_izin'] = $nama_izin;
				$arrdata['direktorat'] = $dir;
				$arrdata['kelembagaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_kelembagaan","kode","uraian", TRUE);
				return $arrdata;
			}
		}
	}

	function set_third($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(($act == "update") || ($act == "save")){
				if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
				}
				$msg = "MSG||NO||Data gagal disimpan";
				$respj = FALSE;
				$arrsiup = $this->main->post_to_query($this->input->post('siupbb'));
				$arrbank = $this->input->post('bank');
				$arrkeys = array_keys($arrbank);
				//print_r($arrsiup);die();
				$dir = $this->input->post('direktorat');
				$doc = $this->input->post('kd_izin');
				$type = $this->input->post('tipe_permohonan');
				$id = $this->input->post('id');
				$txt = "Surat Izin Usaha Perdagangan Bahan Berbahaya (SIUP B2) sebagai DT-B2";
				$this->db->where('id',$id);
				$this->db->update('t_siupbb',$arrsiup);
				if($this->db->affected_rows() > 0){
					if($act == "save"){
						if (count($_POST['bank']['nama'])>0)  {
							for($s = 0; $s < count($_POST['bank']['nama']); $s++){
								$bank = array('permohonan_id' => $id);
								for($j=0;$j<count($arrkeys);$j++){
									$bank [$arrkeys[$j]] = $arrbank[$arrkeys[$j]][$s];
								}
								unset($bank['id']);
								if($bank['nama'] != ""){
									$this->db->insert('t_siupbb_bank', $bank);
								}
							}
						}
						
						if($this->db->affected_rows() > 0){
							$respj = TRUE;
							
							$msg = "MSG||YES||Data Kegiatan berhasil disimpan. Silahkan lanjutkan mengisi data persyaratan||".site_url().'licensing/form/fourth/'.$dir.'/'.$doc.'/'.$type.'/'.$id;
							
							/* Log User dan Log Izin */
							$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' (Data Kegiatan Usaha)',
										  'url' => '{c}'. site_url().'post/licensing/siupbb_act/third/save'. ' {m} models/licensing/siupbb_act {f} set_third($act, $isajax)');
							$this->main->set_activity($logu);
							/* Akhir Log User */
						}
					}elseif ($act == "update") {
						$jumlahBank = $this->main->get_uraian("SELECT COUNT(*) AS JMLBANK FROM t_siupbb_bank WHERE permohonan_id =".$id,"JMLBANK");
						//print_r($jumlahBank);die();
						if($jumlahBank > 0){
							$this->db->where('permohonan_id',$id);
							$this->db->delete('t_siupbb_bank');
							if($this->db->affected_rows() > 0){
								for($s = 0; $s < count($_POST['bank']['nama']); $s++){
									$bank = array('permohonan_id' => $id);
									for($j=0;$j<count($arrkeys);$j++){
										$bank [$arrkeys[$j]] = $arrbank[$arrkeys[$j]][$s];
									}
									unset($bank['id']);//print_r($bank);
									if($bank['nama'] != ""){
										$this->db->insert('t_siupbb_bank', $bank);
									}
								}
							}
						}else{
							for($s = 0; $s < count($_POST['bank']['nama']); $s++){
								$bank = array('permohonan_id' => $id);
								for($j=0;$j<count($arrkeys);$j++){
									$bank [$arrkeys[$j]] = $arrbank[$arrkeys[$j]][$s];
								}
								unset($bank['id']);//print_r($bank);
								if($bank['nama'] != ""){
									$this->db->insert('t_siupbb_bank', $bank);
								}									
							}
						}
						
						if($this->db->affected_rows() > 0){
							$respj = TRUE;
							$msg = "MSG||YES||Data Kegiatan Usaha berhasil diupdate.||REFRESH";
							/* Log User dan Log Izin */
							$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' (Data Kegiatan Usaha)',
										  'url' => '{c}'. site_url().'post/licensing/siupbb_act/third/update'. ' {m} models/licensing/siupbb_act {f} set_third($act, $isajax)');
							$this->main->set_activity($logu);
							/* Akhir Log User */
						}
					}
				}
				return $msg;
			}
		}
	}

	function get_fourth($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			if ($type == '01') {
				$addCon = "AND a.baru = '1'";
			}elseif ($type=='02') {
				$addCon = "AND a.perubahan = '1'";
			}elseif ($type=='03') {
				$addCon = "AND a.perpanjangan = '1'";
			}
			$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
			$nama_izin = $this->main->get_uraian($sql,'nama_izin');
			$arrdata['nama_izin'] = $nama_izin;
			$arrdata['izin_id'] = $doc;
			$arrdata['direktorat'] = $dir;
			$arrdata['type'] = $type;
			$arrdata['permohonan_id'] = $id;
			$arrdata['urithird'] = site_url('licensing/form/third/'.$dir.'/'.$doc.'/'.$type.'/'.$id);
			$query_syarat ="SELECT a.id, a.izin_id, a.dok_id, a.upload_id,
						 case when c.multi = 1 then '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor else '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor end AS nomor,
						  b.tgl_dok, b.tgl_exp, b.penerbit_dok,
						 '<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
						 FROM t_upload_syarat a 
						 LEFT join t_upload b on b.id = a.upload_id
						 LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id
						 WHERE a.izin_id = '".$doc."' and a.permohonan_id = '".$id."' and a.detail_id is null";

			$data = $this->main->get_result($query_syarat);
			$arr = array();

			$query= "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian, a.multi
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.kategori = '01' and a.izin_id = '".$doc."' ".$addCon." order by a.urutan  ";
			$data_req = $this->main->get_result($query);
			$temp = $query->result_array();
            $arrdata['req'] = $temp;

			if($data){
				foreach($query_syarat->result_array() as $keys){
					//$arr[$keys['dok_id']] = $keys;
					if (!isset($arr[$keys['dok_id']])){
					  $arr[$keys['dok_id']] = array();
					}
					$arr[$keys['dok_id']][] = $keys;
				}
				$arrdata['sess'] = $arr;
				$arrdata['act'] = site_url('post/licensing/siupbb_act/fourth/update');

			}else{
				foreach ($temp as $datadt) {
                    $query_syarat2 = "  SELECT top 1 b.id as 'upload_id', b.tipe_dok,  CASE WHEN a.multi = 1 THEN ' <a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp; ' + b.nomor ELSE '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor END AS nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
                    '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View' 
                                            FROM t_upload b left join m_dok_izin a on b.tipe_dok = a.dok_id 
                    where a.izin_id = '".$doc."' and b.trader_id = '" . $this->newsession->userdata('trader_id') . "'and b.tipe_dok = '" . $datadt['dok_id'] . "'  " . $addCon . " order by tgl_dok desc;";
                
                    $data2 = $this->main->get_result($query_syarat2);
                    if ($data2) {
                        foreach ($query_syarat2->result_array() as $keys) {
                            if (!isset($arr[$keys['tipe_dok']])) {
                                $arr[$keys['tipe_dok']] = array();
                            }
                            $arr[$keys['tipe_dok']][] = $keys;
                        }
                    }
                }
                $arrdata['sess'] = $arr;
				$arrdata['baru'] = true;
				$arrdata['act'] = site_url('post/licensing/siupbb_act/fourth/save');
			}
			return $arrdata;
		}
	}

	function set_fourth($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			$msg = "MSG||NO||Data gagal disimpan";
			$ressiup = FALSE;
			$arrsiup = $this->main->post_to_query($this->input->post('SIUPMB'));
			$arrreq = $this->input->post('REQUIREMENTS');
			$arrkeys = array_keys($arrreq);
			$txt = "Surat Izin Usaha Perdagangan Bahan Berbahaya (SIUP B2) sebagai DT-B2";
			if($act == "save"){
				for($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++){
					$requirements = array('izin_id' => $arrsiup['izin_id'],
					     'permohonan_id' => $arrsiup['permohonan_id'],
					     'created' => 'GETDATE()',
					     'created_user' => $this->newsession->userdata('username'));
					for($j=0;$j<count($arrkeys);$j++){
						$requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
					}
					unset($requirements['id']);
					if($requirements['upload_id'] != "")
						$this->db->insert('t_upload_syarat', $requirements);
				}
				if($this->db->affected_rows() > 0){
					$ressiup = TRUE;
					$idUpload = $this->db->insert_id();
					/* Log User dan Log Izin */
					$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' (Data Persyaratan)',
								  'url' => '{c}'. site_url().'post/licensing/siupbb_act/fourth/save'. ' {m} models/licensing/siupbb_act {f} set_fourth($act, $isajax)');
					$this->main->set_activity($logu);
					/* Akhir Log User */
					$msg = "MSG||YES||Data Persyaratan berhasil disimpan. Pengajuan Permohonan ".$txt." selesai.||".site_url().'licensing/view/status';
				}
				if($this->db->trans_status() === FALSE || !$ressiup){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
			}elseif($act == "update"){
				$this->db->where('permohonan_id', $arrsiup['permohonan_id']);
				$this->db->where('izin_id', $arrsiup['izin_id']);
        		$this->db->delete('t_upload_syarat');
        		if($this->db->affected_rows() > 0){
        			for($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++){
						$requirements = array('izin_id' => $arrsiup['izin_id'],
						     'permohonan_id' => $arrsiup['permohonan_id'],
						     'created' => 'GETDATE()',
						     'created_user' => $this->newsession->userdata('username'));
						for($j=0;$j<count($arrkeys);$j++){
							$requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
						}
						unset($requirements['id']);
						if($requirements['upload_id'] != "")
							$this->db->insert('t_upload_syarat', $requirements);
					}
					if($this->db->affected_rows() > 0){
						$ressiup = TRUE;
						$idUpload = $this->db->insert_id();
						/* Log User dan Log Izin */
						$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' (Update - Data Persyaratan)',
									  'url' => '{c}'. site_url().'post/licensing/siupbb_act/fourth/update'. ' {m} models/licensing/siupbb_act {f} set_fourth($act, $isajax)');
						$this->main->set_activity($logu);
						/* Akhir Log User */
						$msg = "MSG||YES||Data Persyaratan berhasil diupdate.||REFRESH";
					}
					if($this->db->trans_status() === FALSE || !$ressiup){
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();
					}
        		}
			}
			return $msg;
		}
	}

	function get_preview($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			if ($type == '01') {
				$addCon = "AND a.baru = '1'";
			}elseif ($type=='02') {
				$addCon = "AND a.perubahan = '1'";
			}elseif ($type=='03') {
				$addCon = "AND a.perpanjangan = '1'";
			}

			$query = "SELECT a.id, b.direktorat_id, a.kd_izin, a.no_aju, a.trader_id, a.tipe_permohonan, c.uraian AS permohonan, a.npwp, d.uraian AS tipe_perusahaan, a.nm_perusahaan, 
					a.almt_perusahaan, dbo.get_region(2, a.kdprop) AS prop_perusahaan, dbo.get_region(4, a.kdkab) AS kab_perusahaan, dbo.get_region(7, a.kdkec) AS kec_perusahaan, dbo.get_region(10, a.kdkel) AS kel_perusahaan,
					a.kdpos, a.telp, a.fax, i.uraian as identitas_pj, a.nama_pj, a.noidentitas_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, a.alamat_pj, 
					a.almt_perusahaan, dbo.get_region(2, a.kdprop_pj) AS prop_pj, dbo.get_region(4, a.kdkab_pj) AS kab_pj, dbo.get_region(7, a.kdkec_pj) AS kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj,
					a.telp_pj, a.fax_pj, a.nilai_modal, f.uraian as produksi, a.nama_produksi, a.perdagangan, a.jenis_dagangan, e.uraian as kategori,
					a.status, b.nama_izin, b.disposisi, j.uraian as 'jabatan_pj', a.no_aju, dbo.dateIndo(a.tgl_kirim) as tgl_aju
					FROM t_siupbb a 
					LEFT JOIN m_izin b ON b.id = a.kd_izin 
					LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
					LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
                                        LEFT JOIN m_reff f ON f.kode = a.produksi AND f.jenis = 'PRODUKSI'
					LEFT JOIN m_reff e ON e.kode = a.kategori AND e.jenis = 'KATEGORI_SIUPBB'
					LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS' 
					LEFT JOIN m_reff j ON j.kode = a.jabatan_pj AND j.jenis = 'JABATAN' 
					WHERE a.id = ".$id;
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){//print_r($row);die();
					$arrdata['sess'] = $row;
				}
				$arrdata['act'] = site_url().'post/proccess/siupbb_act/verification';
				$arrdata['input'] = $this->get_input($dir, $doc, $type, $id, $row['status']);
				$arrdata['proses'] = $this->main->set_proses($dir, $row['kd_izin'], $row['disposisi'], $this->newsession->userdata('role'), $row['status'],hashids_encrypt($row['id'],_HASHIDS_,9));
				$arrdata['jmllog'] = $this->main->get_uraian("SELECT COUNT(*) AS JML FROM t_log_izin WHERE permohonan_id = '".$row['id']."' AND kd_izin = '".$row['kd_izin']."'","JML");
				$arrdata['urllog'] = site_url().'get/log/izin/'.hashids_encrypt($row['id'],_HASHIDS_,9).'/'.hashids_encrypt($row['kd_izin'],_HASHIDS_,9);
			}
			$query_bank = "SELECT a.id, a.permohonan_id, a.nama, a.alamat, b.uraian as lokasi 
						   FROM t_siupbb_bank a
						   LEFT JOIN m_reff b on b.kode = a.lokasi and b.jenis = 'BANK'
						   WHERE permohonan_id = '".$id."'";
			$data_bank = $this->main->get_result($query_bank);
			if($data_bank){
				$arrdata['bank'] = $query_bank->result_array();
			}

			$query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							'<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id
							LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id  
							WHERE c.kategori = '01' and a.izin_id = ".$doc." and a.permohonan_id = '".$id."'";
			$data = $this->main->get_result($query_syarat);
			$arr = array();
			if($data){
				foreach($query_syarat->result_array() as $keys){
					if (!isset($arr[$keys['dok_id']])){
					  $arr[$keys['dok_id']] = array();
					}
					$arr[$keys['dok_id']][] = $keys;
				}
				$arrdata['sess_syarat'] = $arr;

			}
			$query= "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.izin_id = '".$doc."' ".$addCon." order by a.tipe desc, a.urutan ASC";
			$data_req = $this->main->get_result($query);
                        $arrdata['telaah'] = $this->main->get_telaah($doc);
                        $arrdata['agrement'] = $this->main->get_agrement($doc);
			$arrdata['req'] = $query->result_array();
			$arrdata['dir'] = $dir;
			//print_r($arrdata);die();
			return $arrdata;
		}
	}
	
	function get_input($dir, $doc, $type, $id, $stts){
        if($this->newsession->userdata('_LOGGED')){
            $arrstts = array('0102');
            $arrdata = array();
            if(in_array($stts, $arrstts)){
                $query = "SELECT a.id, a.kd_izin, a.no_aju, CONVERT(VARCHAR(10), a.tgl_aju, 103) AS tgl_aju, a.no_izin, CONVERT(VARCHAR(10), a.tgl_izin, 103) as tgl_izin, CONVERT(VARCHAR(10), a.tgl_izin_exp, 103) AS tgl_izin_exp FROM t_siupbb a WHERE a.id = '".$id."' AND a.kd_izin = '".$doc."'";
                $ret = $this->main->get_result($query);
                if($ret){
					$this->ineng = $this->session->userdata('site_lang'); 
                    foreach ($query->result_array() as $row){
                        $arrdata['sess'] = $row;
                    }
					$arrdata['dir'] = $dir;
					$arrdata['doc'] = $doc;
                }
				if(!$this->session->userdata('site_lang')) $this->ineng = "id";
                $data = $this->load->view($this->ineng.'/backend/input/'.$dir.'/'.$stts, $arrdata, true);
            } else {
                $data = "";
            }
        }
        return $data;
    }

	function set_onfly($act, $id, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if($act == "update"){
				if(!$isajax){
					return false;
					exit();
				}
				$msg = "MSG||NO";
				$respon = FALSE;
				$arrsiup = $this->main->post_to_query($this->input->post('dataon'));
				$id = hashids_decrypt($id, _HASHIDS_, 9);
				$this->db->where('id',$id);
				$this->db->update('t_siupbb',$arrsiup);
				if($this->db->affected_rows() == 1){
					$respon = TRUE;
					$logu = array('aktifitas' => 'Melakukan update tanggal izin / tanggal masa berlaku',
								  'url' => '{c}'. site_url().'get/onfly/onfly_act/update'. ' {m} models/licensing/siupbb_act {f} set_onfly($act,  $id, $isajax)');
					$this->main->set_activity($logu);
				}
				if($respon) $msg = "MSG||YES";
				return $msg;
			}
		}
	}
	
	
	function referensi_kbli($target, $callback, $fieldcallback){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable;
			$query = "SELECT kode as kbli, uraian as desc_kbli, kode AS 'Kode KBLI', uraian AS 'Uraian KBLI' FROM m_kbli";
			$table->title("");
			$table->columns(array("kode","Kode KBLI", "uraian", "Uraian KBLI"));
			$this->newtable->width(array('Kode KBLI' => 100, 'Uraian' => 300, '&nbsp;' => 5));
			$this->newtable->search(array(array("kode","Kode KBLI"),
										  array("uraian","Uraian")));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."/licensing/popup_kbli/".$target.'/'.$callback.'/'.$fieldcallback);
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("kode"));
			$table->hiddens(array("kbli","desc_kbli"));
			$table->use_ajax(TRUE);
			$table->show_search(TRUE);
			$table->show_chk(FALSE);
			$table->single(TRUE);
			$table->dropdown(TRUE);
			$table->postmethod(TRUE);
			$table->settrid(TRUE);
			$table->attrid($target);
			if($callback!="")$table->callback(site_url(str_replace(".",'/',$callback)));
			if($fieldcallback!="") $table->fieldcallback($fieldcallback);
			$table->tbtarget("refkbli");
			$arrdata = array('tabel' => $table->generate($query));
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	function list_dok($target, $callback, $fieldcallback, $id, $multiple, $putin, $doc){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable;
			$query = "SELECT id, id AS 'upload_id', nomor AS 'No. Dokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'Tgl. Dokumen', tgl_exp AS 'Tgl. Akhir', '<a href=\"".site_url()."download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
			FROM t_upload WHERE tipe_dok = '".$id."' AND trader_id = '".$this->newsession->userdata('trader_id')."'";
			$table->title("");
			$table->columns(array("id", "nomor", "penerbit_dok", "tgl_dok", "tgl_exp", "<a href=\"".site_url()."download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
			$this->newtable->width(array('Nomor Dokumen' => 100, 'Penerbit' => 300, 'Tanggal Awal' => 100,'Tanggal Akhir' => 100,'&nbsp;' => 5));
			$this->newtable->search(array(array("nomor","Nomor Penerbit"),
										  array("penerbit_dok","Penerbit")));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."/licensing/popup_syarat/".$id.'/'.$target.'/'.$callback.'/'.$fieldcallback.'/');
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("id"));
			$table->hiddens(array("id","folder","nama_file","upload_id"));
			$table->show_search(TRUE);
			$table->single(TRUE);
			$table->dropdown(TRUE);
			$table->postmethod(TRUE);
			
			if((int)$multiple == 1){ 
				$table->show_chk(TRUE);
				$table->menu(array('Pilih Data' => array('POSTGET', site_url().'post/document/get_requirements/'.$doc.'/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#'.$putin)));
			}else{
				$table->show_chk(FALSE);
				if($callback!="")$table->callback(site_url(str_replace(".",'/',$callback)));
				if($fieldcallback!="") $table->fieldcallback($fieldcallback);
				$table->settrid(TRUE);
				$table->attrid($target);
			}
			$table->tbtarget("refreq_".rand());
			$arrdata = array('tabel' => $table->generate($query));
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}
	
	function get_requirements($doc){
		$id = join("','", $this->input->post('tb_chk'));
		$data = "SELECT a.id AS dok_ids, b.id AS id_upload, b.nomor, b.penerbit_dok, b.tgl_dok, b.tgl_exp, '<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS files, c.tipe FROM m_dok a LEFT JOIN t_upload B ON a.id = b.tipe_dok LEFT JOIN m_dok_izin c ON c.dok_id = a.id WHERE b.id IN('".$id."') AND c.izin_id = '".$doc."'";
		//print_r($data);die();
		return $this->db->query($data)->result_array();
	}

	function insert_bank($id, $id_old){
		$ret = FALSE;
		$query_old_bank = "SELECT nama, alamat, lokasi  FROM t_siupbb_bank where permohonan_id = '".$id_old."'";print_r($query_old_bank);die();
		$data_old_bank = $this->main->get_result($query_old_bank);
		if($data_old_bank){
			foreach($query_old_bank->result_array() as $keys){
				$arrBank['permohonan_id'] = $id;
				$jml = count($keys);
				$array_keys = array_keys($keys);
				$array_values = array_values($keys);
				for($i = 0; $i < $jml; $i++){
					$arrBank[$array_keys[$i]] = $array_values[$i];
				}
				$this->db->insert('t_siupbb_bank', $arrBank);
			}
			if ($this->db->affected_rows() > 0) {
				$ret = TRUE;
			}
		}
		return $ret;
	}
}
?>