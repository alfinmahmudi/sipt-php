<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log_act extends CI_Model {

    public function get_log($a, $b) {
        if ($this->newsession->userdata('_LOGGED')) {
            $a = hashids_decrypt($a, _HASHIDS_, 9);
            $b = hashids_decrypt($b, _HASHIDS_, 9);
            $arrdata = array();
            $query = "SELECT a.id, a.kd_izin, a.permohonan_id, a.keterangan, a.catatan,  d.uraian as username, a.username as usname, a.status,(CASE WHEN DATEDIFF(MONTH, a.waktu, GETDATE()) = 0 THEN 
                        (CASE WHEN DATEDIFF(DAY, a.waktu, GETDATE()) > 0 THEN CAST(DATEDIFF(DAY, a.waktu, GETDATE()) AS VARCHAR) + ' Hari, ' ELSE '' END) + (CASE WHEN DATEDIFF(HOUR, a.waktu, GETDATE()) > 0 
                        THEN CAST(DATEDIFF(HOUR, a.waktu, GETDATE())%24 AS VARCHAR) + ' Jam, ' ELSE '' END) + 
                        CAST(DATEDIFF(MINUTE, a.waktu, GETDATE())%60 AS VARCHAR) + ' Menit Yang Lalu' 
                        ELSE (CONVERT(VARCHAR(10), a.waktu, 105) + ' ' +  CONVERT(VARCHAR(8), a.waktu, 108)) END) AS waktu 
                        FROM t_log_izin a 
			left join t_user b on a.username = b.username
			left join m_reff  d on b.role = d.kode                     
                        WHERE a.permohonan_id = '" . $a . "' AND a.kd_izin = '" . $b . "' and d.jenis = 'ROLE' ORDER BY 1 DESC"; //print_r($query);die();
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['data'][] = $row;
                }
            }
            return $arrdata;
        }
    }

}

?>