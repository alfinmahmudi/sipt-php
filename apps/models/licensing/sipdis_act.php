<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sipdis_act extends CI_Model {

    var $ineng = "";

    function get_first($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata = array();
            
            $idprop = $this->newsession->userdata('kdprop');
            $idkab = $this->newsession->userdata('kdkab');
            $idkec = $this->newsession->userdata('kdkec');
            $idkel = $this->newsession->userdata('kdkel'); //print_r($idkel);die();
            $tipe_perusahaan = $this->newsession->userdata('tipe_perusahaan');

            $get_drop = "SELECT b.nama as 'nm_prop', c.nama as 'nm_kab', d.nama as 'nm_kec', e.nama as 'nm_kel', f.uraian as 'nm_tipe'
                        FROM m_trader a
                        LEFT JOIN m_prop b ON b.id = a.kdprop
                        LEFT JOIN m_kab c ON c.id = a.kdkab
                        LEFT JOIN m_kec d ON d.id = a.kdkec
                        LEFT JOIN m_kel e ON e.id = a.kdkel
                        LEFT JOIN m_reff f ON f.kode = a.tipe_perusahaan AND f.jenis = 'TIPE_PERUSAHAAN'
                        WHERE a.id = '".$this->newsession->userdata('trader_id')."' ";
            $arrdata['drop'] = $this->db->query($get_drop)->row_array();

            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $idprop . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '" . $idkab . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '" . $idkec . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '" . $idkel . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['jenis_izin'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff where jenis = 'JENIS_DISTRIBUTOR_BAPOK'", "kode", "uraian", TRUE);
            $arrdata['propinsi_usaha'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kabupaten_usaha'] = $this->main->set_combobox("SELECT id, nama FROM m_kab ORDER BY 2", "id", "nama", TRUE);
            $arrdata['direktorat'] = $dir;
            $arrdata['tipe'] = $type;
            $arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian", TRUE);
            $arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN' and kode = '" . $tipe_perusahaan . "' ", "kode", "uraian", TRUE);
            $arrdata['lokasi'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'LOKASI_PERUSAHAAN'", "kode", "uraian", TRUE);
            $arrdata['status_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS_PERUSAHAAN'", "kode", "uraian", TRUE);
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;

            if ($id == "") {
                $arrdata['act'] = site_url('post/licensing/sipdis_act/first/save');
                //$type = hashids_decrypt($type,_HASHIDS_,6);
                $arrdata['sess']['tipe_permohonan'] = $type;
                $arrdata['sess']['kd_izin'] = $doc;
            } else {
                $arrdata['act'] = site_url('post/licensing/sipdis_act/first/update');
                $query = "	SELECT id, kd_izin, no_aju, tgl_aju, id_permohonan_lama, no_izin_lama, tgl_izin_lama, tgl_izin_exp_lama,trader_id, tipe_permohonan, npwp, tipe_perusahaan, nm_perusahaan, almt_perusahaan, kdprop, kdkab, kdkec, kdkel, kdpos, telp, fax, jenis_izin
					FROM t_bapok
					WHERE id = '" . $id . "'";
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['readonly'] = "readonly";
                    $arrdata['disabled'] = "disabled";
                    // $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id = '".$kdkab."'	 ORDER BY 2","id", "nama", TRUE);
                    // $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '".$row['kdkec']."' ORDER BY 2","id", "nama", TRUE);
                    $arrdata['kec_usaha'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab_usaha'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel_usaha'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec_usaha'] . "' ORDER BY 2", "id", "nama", TRUE);
                }
            }//print_r($arrdata);die();
            return $arrdata;
        }
    }

    function set_first($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            if ($this->newsession->userdata('role') == '01') {
                $sqlSource = "SELECT source FROM t_bapok WHERE id = " . $this->input->post('id');
                $source = $this->main->get_uraian($sqlSource, 'source');
                if ($source != '1') {
                    return "MSG||NO||Mohon maaf, pengajuan online tidak dapat diperbaharui oleh pemroses.";
                    exit();
                }
            }

            $arrsiup = $this->main->post_to_query($this->input->post('bapok'));//print_r($arrsiup);die();
            $dir = $this->input->post('direktorat');
            $doc = $arrsiup['kd_izin'];
            $type = $arrsiup['tipe_permohonan'];

            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');

            $msg = "MSG||NO||Data Permohonan " . $nama_izin . " gagal disimpan";
            $ressiup = FALSE;

            if ($act == "save") {
                $arrsiup['no_aju'] = $this->main->set_aju();
                $arrsiup['fl_pencabutan'] = '0';
                $arrsiup['status'] = '0000';
                $arrsiup['tgl_aju'] = 'GETDATE()';
                $arrsiup['trader_id'] = $this->newsession->userdata('trader_id');
                $arrsiup['created'] = 'GETDATE()';
                $arrsiup['created_user'] = $this->newsession->userdata('username');
                if (($type != '01') && ($arrsiup['id_permohonan_lama'] != "")) {
                    if ($type == '03')
                        $arrsiup['fl_pembaharuan'] = "1";
                    $query_old_doc = "SELECT identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, 
									 alamat_pj, kdprop_pj, kdkab_pj, kdkel_pj, kdkec_pj,telp_pj, fax_pj
									 FROM t_bapok WHERE id = " . $arrsiup['id_permohonan_lama'] . " AND kd_izin = " . $arrsiup['kd_izin'];
                    $data_old = $this->main->get_result($query_old_doc);
                    if ($data_old) {
                        foreach ($query_old_doc->result_array() as $keys) {
                            $jml = count($keys);
                            $array_keys = array_keys($keys);
                            $array_values = array_values($keys);
                            for ($i = 0; $i < $jml; $i++) {
                                $arrsiup[$array_keys[$i]] = $array_values[$i];
                            }
                        }
                    }
                }
                $this->db->trans_begin();
                $exec = $this->db->insert('t_bapok', $arrsiup);//print_r($this->db->last_query());die();
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idredir = $this->db->insert_id();
                    $msg = "MSG||YES||Data Permohonan " . $nama_izin . " berhasil disimpan.\nSilahkan lanjutkan mengisi data penanggung jawab||" . site_url() . 'licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $idredir;

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $nama_izin . ' dengan nomor permohonan : ' . $arrsiup['no_aju'],
                        'url' => '{c}' . site_url() . 'post/licensing/sipdis_act/first/save' . ' {m} models/licensing/sipdis_act {f} set_first($act, $ajax)');
                    $this->main->set_activity($logu);
                    $logi = array('kd_izin' => $arrsiup['kd_izin'],
                        'permohonan_id' => $idredir,
                        'keterangan' => 'Menambahkan daftar permohonan ' . $nama_izin . ' dengan nomor permohonan : ' . $arrsiup['no_aju'],
                        'catatan' => '',
                        'status' => '0000',
                        'selisih' => 0);
                    $this->main->set_loglicensing($logi);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } elseif ($act == "update") {
                $id = $this->input->post('id');
                $this->db->where('id', $id);
                $this->db->update('t_bapok', $arrsiup);
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $msg = "MSG||YES||Data Permohonan " . $nama_izin . " berhasil diupdate.||REFRESH";
                }
                return $msg;
            }
        }
    }

    function get_second($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $jenis_identitas = $this->newsession->userdata('tipe_identitas');
            $id_user = $this->newsession->userdata('id');
            $idprop = $this->newsession->userdata('kdprop_pj');
            $idkab = $this->newsession->userdata('kdkab_pj');
            $idkec = $this->newsession->userdata('kdkec_pj');
            $idkel = $this->newsession->userdata('kdkel_pj');
            $jabatan_pj = $this->newsession->userdata('jabatan_pj');

            $get_drop = "SELECT b.nama as 'nm_prop', c.nama as 'nm_kab', d.nama as 'nm_kec', e.nama as 'nm_kel', f.uraian as 'nm_iden',
                        g.uraian as 'nm_jab'
                        FROM m_trader a
                        LEFT JOIN m_prop b ON b.id = a.kdprop_pj
                        LEFT JOIN m_kab c ON c.id = a.kdkab_pj
                        LEFT JOIN m_kec d ON d.id = a.kdkec_pj
                        LEFT JOIN m_kel e ON e.id = a.kdkel_pj
                        LEFT JOIN m_reff f ON f.kode = a.identitas_pj AND f.jenis = 'JENIS_IDENTITAS'
                        LEFT JOIN m_reff g ON g.kode = a.jabatan_pj AND g.jenis = 'JABATAN'
                        WHERE a.id = '".$this->newsession->userdata('trader_id')."' ";
            $arrdata['drop'] = $this->db->query($get_drop)->row_array();

            $arrdata['act'] = site_url('post/licensing/sipdis_act/second/save');
            $arrdata['direktorat'] = $dir;
            $arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '" . $jenis_identitas . "' and jenis = 'JENIS_IDENTITAS'", "kode", "uraian", TRUE);
            $arrdata['jabatan_pj'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JABATAN' AND kode = '" . $jabatan_pj . "' ", "kode", "uraian", TRUE);
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $idprop . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '" . $idkab . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '" . $idkec . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '" . $idkel . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['urifirst'] = site_url('licensing/form/first/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            $query = "SELECT id, kd_izin, tipe_permohonan, identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, alamat_pj, kdprop_pj, kdkab_pj, kdkec_pj,kdkel_pj, telp_pj, fax_pj, kdprop, kdkab, kdkec, kdkel
						FROM t_bapok WHERE id = '" . $id . "'";
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
                if ($row['identitas_pj']) {
                    $arrdata['act'] = site_url('post/licensing/sipdis_act/second/update');
                }
                $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            return $arrdata;
        }
    }

    function set_second($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (($act == "update") || ($act == "save")) {
                if (!$isajax) {
                    return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                    exit();
                }
                if ($this->newsession->userdata('role') == '01') {
                    $sqlSource = "SELECT source FROM t_bapok WHERE id = " . $this->input->post('id');
                    $source = $this->main->get_uraian($sqlSource, 'source');
                    if ($source != '1') {
                        return "MSG||NO||Mohon maaf, pengajuan online tidak dapat diperbaharui oleh pemroses.";
                        exit();
                    }
                }
                $msg = "MSG||NO||Data Penanggung Jawab gagal disimpan";
                $respj = FALSE;
                $arrpj = $this->main->post_to_query($this->input->post('pj'));
                $dir = $this->input->post('direktorat');
                $doc = $this->input->post('kd_izin');
                $type = $this->input->post('tipe_permohonan');
                $id = $this->input->post('id');

                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');

                $this->db->where('id', $id);
                $this->db->update('t_bapok', $arrpj);
                if ($this->db->affected_rows() > 0) {
                    $respj = TRUE;
                    if ($act == "update") {
                        $msg = "MSG||YES||Data Penanggung Jawab berhasil diupdate.||REFRESH";
                    } else {
                        $msg = "MSG||YES||Data Penanggung Jawab berhasil disimpan. \nSilahkan lanjutkan mengisi data Jenis Barang Kebutuhan Pokok Yang Di Perdagangkan||" . site_url() . 'licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id;
                    }
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $nama_izin . ' (Data Penanggung Jawab)',
                        'url' => '{c}' . site_url() . 'post/licensing/sipdis_act/second/update' . ' {m} models/licensing/sipdis_act {f} set_second($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                }
                return $msg;
            }
        }
    }

    function get_third($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $arrdata['act'] = site_url('post/licensing/sipdis_act/third/save');
                $arrdata['urisecond'] = site_url('licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
                $query = "SELECT id, kd_izin, tipe_permohonan, kelembagaan, bidang_usaha, pemasaran_prop, pemasaran_kab, pemasaran
                          FROM t_bapok WHERE id = '" . $id . "'";//print_r($query);die();
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                        //$a = explode(',', $row['pemasaran']);print_r($a);die();
                        $arrdata['sess_prop'] = explode(",", $row['pemasaran_prop']);
                        $arrdata['sess_kab'] = explode(",", $row['pemasaran_kab']);
                        $arrdata['bidang_usaha'] = explode(",", $row['bidang_usaha']);
                    }
                    $get_dtl = "SELECT a.id, a.jns_bapok, a.jns_brg FROM t_bapok_brg a WHERE a.permohonan_id = '" . $id . "'";//print_r($get_dtl);die();
                	$detil = $this->main->get_result($get_dtl);
                	if ($detil) {
                		foreach ($get_dtl->result_array() as $det) {
	                     	$arrdetil[] = array(
	                     		'id_det' => $det['id'], 
	                     		'jns_bapok' => $det['jns_bapok'],
	                     		'jns_brg' => $det['jns_brg']
	                     	);
	                    }
	                    $arrdata['detil'] = $arrdetil;
	                    //print_r($arrdata['detil']);die();
                	}
                }
                // $ss = "SELECT a.nama, a.id, 1 as 'kode' FROM m_prop a
                //             UNION SELECT b.nama, b.id, 2 as 'kode' FROM m_kab b 
                //             ORDER BY kode";
                // $arrdata['propinsi'] = $this->main->set_combobox($ss, "id", "nama", TRUE);

                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $arrdata['nama_izin'] = $nama_izin;
                $arrdata['direktorat'] = $dir;
                $arrdata['kbli'] = $this->main->set_combobox("SELECT REPLACE(a.kode,' ','') as kode, CONCAT(REPLACE(a.kode,' ',''), ' -- ', a.uraian) as uraian FROM m_kbli a WHERE bapok = '1'", "kode", "uraian", TRUE);
                $arrdata['prop'] = $this->main->set_combobox("SELECT id, nama FROM m_prop", "id", "nama", TRUE);
                $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab", "id", "nama", TRUE);
                $arrdata['kelembagaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_kelembagaan", "kode", "uraian", TRUE);
                
                $query = "SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_BAPOK'";
                $arrdata['bapok'] = $this->db->query($query)->result_array();//print_r($arrdata['bapok']);die();

                $get_jenis1 = "SELECT id, jenis_bapok, uraian FROM m_jenis_bapok WHERE jenis_bapok = '01' ";
                $arrdata['urai1'] = $this->db->query($get_jenis1)->result_array();

                $get_jenis2 = "SELECT id, jenis_bapok, uraian FROM m_jenis_bapok WHERE jenis_bapok = '02' ";
                $arrdata['urai2'] = $this->db->query($get_jenis2)->result_array();

                $get_jenis3 = "SELECT id, jenis_bapok, uraian FROM m_jenis_bapok WHERE jenis_bapok = '03' ";
                $arrdata['urai3'] = $this->db->query($get_jenis3)->result_array();

                $pemasaran = "SELECT kode, uraian FROM m_reff WHERE jenis = 'PEMASARAN'";
                $arrdata['pemasaran'] = $this->db->query($pemasaran)->result_array();

        //         $query = "SELECT id, kd_izin, tipe_permohonan, nilai_modal, kegiatan_usaha, kelembagaan, bank1, alamat_bank1, bank2, alamat_bank2, kbli, desc_kbli, 
						  // fl_gol_a, fl_gol_b, fl_gol_c, gol_a, gol_b, gol_c, pemasaran, jns_gol_a, jns_gol_b, jns_gol_c
						  // FROM t_siupmb WHERE id = '" . $id . "'";//print_r($query);die();
        //         $data = $this->main->get_result($query);
        //         if ($data) {
        //             foreach ($query->result_array() as $row) {
        //                 $arrdata['sess'] = $row;
        //             }
        //             //$arrdata['w_pemasaran'] = explode(",",$row['pemasaran']);
        //             $arrdata['jns_gol_a'] = explode(",", $row['jns_gol_a']);
        //             $arrdata['jns_gol_b'] = explode(",", $row['jns_gol_b']);
        //             $arrdata['jns_gol_c'] = explode(",", $row['jns_gol_c']);
        //             $arrdata['merk_gol_a'] = explode(",", $row['gol_a']);
        //             $arrdata['merk_gol_b'] = explode(",", $row['gol_b']);
        //             $arrdata['merk_gol_c'] = explode(",", $row['gol_c']); //print_r($arrdata['jns_gol_c']);die();
        //             if ($doc == '3') {
        //                 $arrdata['penetapan'] = explode(",", $row['penetapan_itmb']);
        //             }
        //             $arrdata['pemasaran'] = explode(",", $row['pemasaran']);
        //             //print_r($arrdata);die();
        //             if ($row['nilai_modal']) {
        //                 $arrdata['act'] = site_url('post/licensing/siupmb_act/third/update');
        //             }
        //         }

                return $arrdata;
            }
        }
    }

    function get_jenis_bapok() {
        $id_jenis = $this->input->post('id_jenis');
        $flag = $this->input->post('flag');
        $value = $this->input->post('value');
        $rand = rand();
        $query = "SELECT id, jenis_bapok, uraian 
        FROM m_jenis_bapok a 
        WHERE a.jenis_bapok = '".$id_jenis."'";
        $get_jenis = $this->db->query($query)->result_array();
        $temp = '';
        // $temp .= '<tr id="jns_'.$rand.'"><td nowrap><select name="jns_'.$id_jenis.'[]" wajib="yes" class="form-control input-sm select2">';
        // foreach ($get_jenis as $key) {
        // 	if ($value == $key['id']) {
        // 		$temp .= '<option value="'.$key['id'].'" selected>'.$key['uraian'].'</option>';
        // 	}else{
        // 		$temp .= '<option value="'.$key['id'].'">'.$key['uraian'].'</option>';
        // 	}
        // }
        // if ($flag == 1) {
        //     $temp .= '</select><td><center><i class="fa fa-minus-square fa-lg"';
        //     $temp .= "onclick=rem('jns_".$rand."')>";
        //     $temp .= '</i></center></td></td></tr>';
        // }else{
        //     $temp .= '</select><td><center><i class="fa fa-plus-square fa-lg"';
        //     $temp .= "onclick=add('".$id_jenis."')";
        //     $temp .= '></i></center></td></td></tr>';
        // }
        foreach ($get_jenis as $jenis) {
            $temp .= '<input type="checkbox" id="jns_'.$jenis['id'].'" name="jns_'.$id_jenis.'[]" value="'.$jenis['id'].'" > '.$jenis['uraian'].' <br>';
        }
        $get_nama = "SELECT uraian FROM m_reff where kode = '".$id_jenis."' AND jenis = 'JENIS_BAPOK' ";
        $nama = $this->db->query($get_nama)->row_array();
        $arrayres = array(
            'nama' => $nama['uraian'],
            'temp' => $temp
        );
        return $arrayres;
        //print_r($temp);die();
    }

    function set_third($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (($act == "update") || ($act == "save")) {
                if (!$isajax) {
                    return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                    exit();
                }
                if ($this->newsession->userdata('role') == '01') {
                    $sqlSource = "SELECT source FROM t_siupmb WHERE id = " . $this->input->post('id');
                    $source = $this->main->get_uraian($sqlSource, 'source');
                    if ($source != '1') {
                        return "MSG||NO||Mohon maaf, pengajuan online tidak dapat diperbaharui oleh pemroses.";
                        exit();
                    }
                }
                $msg = "MSG||NO||Data gagal disimpan";
                $respj = FALSE;
                $get_kode_jns = "SELECT kode FROM m_reff WHERE jenis = 'JENIS_BAPOK'";
                $kode_jns = $this->db->query($get_kode_jns)->result_array();
                $arrsiup = $this->main->post_to_query($this->input->post('bapok'));

                $indo = $this->input->post('indo_01');
                $prop = $this->input->post('prop');
                $kab = $this->input->post('kab');
                $pemasaran = $this->input->post('pemasaran');

                // implode jenis dengan 3 kategori yang beda
                $jns_01 = $this->input->post('jns_01');
                // $jns_01 = implode(", ", $jns_01);
                $jns_02 = $this->input->post('jns_02');
                // $jns_02 = implode(", ", $jns_02);
                $jns_03 = $this->input->post('jns_03');
                // $jns_03 = implode(", ", $jns_03);
                $bidang_usaha = $this->input->post('bidang_usaha');
                $arrsiup['bidang_usaha'] = implode(", ", $bidang_usaha);

                $dir = $this->input->post('direktorat');
                $doc = $this->input->post('kd_izin');
                $type = $this->input->post('tipe_permohonan');
                $id = $this->input->post('id');

                $jenis_barang = $this->input->post('jns_brg');

                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');

                $arrsiup['pemasaran'] = implode(", ", $pemasaran);
                if ($pemasaran[0] == '01') {
                    $arrsiup['pemasaran'] = '01';
                    $arrsiup['pemasaran_prop'] = '';
                    $arrsiup['pemasaran_kab'] = '';
                }else{
                    $arrsiup['pemasaran_prop'] = implode(", ", array_unique($prop));
                    $arrsiup['pemasaran_kab'] = implode(", ", array_unique($kab));
                }
               	
                if ($id != '') {
                	$this->db->where('permohonan_id', $id);
	                $this->db->delete('t_bapok_brg');
	                	$arrbarang['permohonan_id'] = $id;
	                	if (count($jns_01) > 0) {
	                		$arrbarang['jns_bapok'] = '01';
	                		foreach ($jns_01 as $jn_01) {
	                			$arrbarang['jns_brg'] = $jn_01;
	                			$this->db->insert('t_bapok_brg', $arrbarang);	
	                		}
	                	}
                        if (count($jns_02) > 0) {
	                		$arrbarang['jns_bapok'] = '02';
	                		foreach ($jns_02 as $jn_02) {
	                			$arrbarang['jns_brg'] = $jn_02;
	                			$this->db->insert('t_bapok_brg', $arrbarang);	
	                		}
	                	}
                        if (count($jns_03) > 0) {
	                		$arrbarang['jns_bapok'] = '03';
	                		foreach ($jns_03 as $jn_03) {
	                			$arrbarang['jns_brg'] = $jn_03;
	                			$this->db->insert('t_bapok_brg', $arrbarang);	
	                		}
	                	}

	                $this->db->where('id', $id);
                	$this->db->update('t_bapok', $arrsiup);
                }
                //print_r($this->db->last_query());die();
                if ($this->db->affected_rows() > 0) {
                    $respj = TRUE;
                    if ($act == "update") {
                        $msg = "MSG||YES||Jenis Barang Kebutuhan Pokok Yang Di Perdagangkan berhasil diupdate.||REFRESH";
                    } else {
                        $msg = "MSG||YES||Jenis Barang Kebutuhan Pokok Yang Di Perdagangkan berhasil disimpan. \nSilahkan lanjutkan mengisi data persyaratan||" . site_url() . 'licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id;
                    }
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $nama_izin . ' (Data Jenis Barang Kebutuhan Pokok Yang Di Perdagangkan)',
                        'url' => '{c}' . site_url() . 'post/licensing/siupmb_act/third/update' . ' {m} models/licensing/siupmb_act {f} set_third($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                }
                return $msg;
            }
        }
    }

    function get_fourth($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            $arrdata['izin_id'] = $doc;
            $arrdata['direktorat'] = $dir;
            $arrdata['type'] = $type;
            $arrdata['permohonan_id'] = $id;
            $arrdata['urithird'] = site_url('licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id,
							 case when c.multi = 1 then '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor else '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor end AS nomor,
							  b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							 '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							 FROM t_upload_syarat a 
							 LEFT join t_upload b on b.id = a.upload_id
							 LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id
							 WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "' and a.detail_id is null";

            /* "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
              '<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
              FROM t_upload_syarat a
              LEFT join t_upload b on b.id = a.upload_id
              WHERE a.izin_id = '".$doc."' and a.permohonan_id = '".$id."' and a.detail_id is null"; */

            /* print_r($query_syarat);die(); */
            $data = $this->main->get_result($query_syarat);
            $arr = array();

            $query = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian, a.multi
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.kategori <> '03' and a.izin_id = '" . $doc . "' " . $addCon . " order by urutan";

            $data_req = $this->main->get_result($query);
            $temp = $query->result_array();
            $arrdata['req'] = $temp;

            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
                    //$arr[$keys['dok_id']] = $keys;
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }

                $arrdata['sess'] = $arr;
                //print_r($arrdata);die();
                $arrdata['act'] = site_url('post/licensing/sipdis_act/fourth/update');
            } else {
                foreach ($temp as $datadt) {
                    $query_syarat2 = "  SELECT top 1 b.id as 'upload_id', b.tipe_dok,  CASE WHEN a.multi = 1 THEN ' <a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp; ' + b.nomor ELSE '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor END AS nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
					'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View' 
                                            FROM t_upload b left join m_dok_izin a on b.tipe_dok = a.dok_id 
					where a.izin_id = '" . $doc . "' and b.trader_id = '" . $this->newsession->userdata('trader_id') . "'and b.tipe_dok = '" . $datadt['dok_id'] . "'  " . $addCon . " order by tgl_dok desc;";

                    $data2 = $this->main->get_result($query_syarat2);
                    if ($data2) {
                        foreach ($query_syarat2->result_array() as $keys) {
                            if (!isset($arr[$keys['tipe_dok']])) {
                                $arr[$keys['tipe_dok']] = array();
                            }
                            $arr[$keys['tipe_dok']][] = $keys;
                        }
                    }
                }
                $arrdata['sess'] = $arr;
                $arrdata['baru'] = true;
                $arrdata['act'] = site_url('post/licensing/siupmb_act/fourth/save');
            }

            //print_r($arrdata);die();
            return $arrdata;
        }
    }

    function set_fourth($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $arrsiup = $this->main->post_to_query($this->input->post('SIUPMB'));
            if ($this->newsession->userdata('role') == '01') {
                $sqlSource = "SELECT source FROM t_bapok WHERE id = " . $arrsiup['permohonan_id'];
                $source = $this->main->get_uraian($sqlSource, 'source');
                if ($source != '1') {
                    return "MSG||NO||Mohon maaf, pengajuan online tidak dapat diperbaharui oleh pemroses.";
                    exit();
                }
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $ressiup = FALSE;

            $arrreq = $this->input->post('REQUIREMENTS');
            $arrkeys = array_keys($arrreq);
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');

            if ($act == "save") {
                for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                    $requirements = array('izin_id' => $arrsiup['izin_id'],
                        'permohonan_id' => $arrsiup['permohonan_id'],
                        'created' => 'GETDATE()',
                        'created_user' => $this->newsession->userdata('username'));
                    for ($j = 0; $j < count($arrkeys); $j++) {
                        $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                    }
                    unset($requirements['id']);
                    if ($requirements['upload_id'] != "")
                        $this->db->insert('t_upload_syarat', $requirements);
                }
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idUpload = $this->db->insert_id();
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $nama_izin . ' (Data Persyaratan)',
                        'url' => '{c}' . site_url() . 'post/licensing/sipdis_act/fourth/save' . ' {m} models/licensing/sipdis_act {f} set_fourth($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                    $msg = "MSG||YES||Data Persyaratan berhasil disimpan. Pengajuan Permohonan " . $nama_izin . " selesai.||" . site_url() . 'licensing/view/status';
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            } elseif ($act == "update") {
                $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                $this->db->where('izin_id', $arrsiup['izin_id']);
                $this->db->delete('t_upload_syarat');
                if ($this->db->affected_rows() > 0) {
                    for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                        $requirements = array('izin_id' => $arrsiup['izin_id'],
                            'permohonan_id' => $arrsiup['permohonan_id'],
                            'created' => 'GETDATE()',
                            'created_user' => $this->newsession->userdata('username'));
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                        }
                        unset($requirements['id']);
                        if ($requirements['upload_id'] != "")
                            $this->db->insert('t_upload_syarat', $requirements);
                    }
                    if ($this->db->affected_rows() > 0) {
                        $ressiup = TRUE;
                        $idUpload = $this->db->insert_id();
                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $nama_izin . ' (Update - Data Persyaratan)',
                            'url' => '{c}' . site_url() . 'post/licensing/sipdis_act/fourth/update' . ' {m} models/licensing/sipdis_act {f} set_fourth($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                        $msg = "MSG||YES||Data Persyaratan berhasil diupdate.||REFRESH";
                    }
                    if ($this->db->trans_status() === FALSE || !$ressiup) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }
            return $msg;
        }
    }

    function get_preview($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }

            if ($dir == '03') {
                $query = "SELECT a.id, b.direktorat_id, a.kd_izin, a.no_aju, a.trader_id, a.tipe_permohonan, c.uraian AS permohonan ,
						dbo.npwp(a.npwp) AS npwp, d.uraian AS tipe_perusahaan, a.nm_perusahaan, a.almt_perusahaan,
						dbo.get_region(2,a.kdprop) as kdprop, dbo.get_region(4,a.kdkab) as kdkab,
						dbo.get_region(7,a.kdkec) as kdkec, dbo.get_region(10,a.kdkel) as kdkel,
						l.uraian AS identitas_pj, a.nama_pj, a.noidentitas_pj, z.uraian as jabatan_pj, a.tmpt_lahir_pj, 
						dbo.dateIndo(a.tgl_lahir_pj) as tgl_lahir_pj, a.alamat_pj,
						dbo.get_region(2,a.kdprop_pj) as kdprop_pj, dbo.get_region(4,a.kdkab_pj) as kdkab_pj, 
						dbo.get_region(7,a.kdkec_pj) as kdkec_pj, dbo.get_region(10,a.kdkel_pj) as kdkel_pj,  
						a.telp_pj, a.fax_pj, a.telp, a.kdpos, a.fax, f.uraian as 'kelembagaan',
						a.status, b.nama_izin, b.disposisi, a.no_aju, dbo.dateIndo(a.tgl_kirim) as tgl_aju, a.pemasaran, a.pemasaran_prop,
						a.pemasaran_kab, i.uraian as jenis_izin, a.bidang_usaha
						FROM t_bapok a 
						LEFT JOIN m_izin b ON b.id = a.kd_izin 
						LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
						LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
						LEFT JOIN m_reff l ON l.kode = a.identitas_pj AND l.jenis = 'JENIS_IDENTITAS'
						LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND Z.jenis = 'JABATAN'
						LEFT JOIN m_kelembagaan f ON f.kode = a.kelembagaan
                        LEFT JOIN m_reff i ON i.kode = a.jenis_izin AND i.jenis = 'JENIS_DISTRIBUTOR_BAPOK'
						WHERE a.id = " . $this->db->escape($id) . "";//print_r($query);die();
                $data = $this->main->get_result($query);
                
                if ($data) {
                    $tmp = array();
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                        $pemasaran = explode(", ", $row['pemasaran']);
                        foreach ($pemasaran as $pem) {
                        	$get_urai = "SELECT kode, uraian FROM m_reff WHERE jenis = 'PEMASARAN' AND kode = '".$pem."' ";
                        	$temp = $this->db->query($get_urai)->row_array();
                            if ($pem == '01') {
                                $arrdata['pemasaran_ind'][] = array(
                                                        'urai' => $temp['uraian'],
                                                        'kode' => $pem
                                                    );   
                            }
                            if ($pem == '02') {
                                $pem_prop = explode(", ", $row['pemasaran_prop']);//print_r($pem_prop);die();
                                if (count($pem_prop) > 0) {
                                    foreach ($pem_prop as $prop) {
                                        $get_prop = "SELECT id, nama FROM m_prop where id = '".$prop."' ";
                                        $temp_prop = $this->db->query($get_prop)->row_array();
                                        $arrtemp[] = $temp_prop['nama'];
                                    }
                                    $arrdata['pemasaran_prop'][] = array(
                                                        'urai' => $temp['uraian'],
                                                        'detil' => $arrtemp,
                                                        'kode' => $pem);   
                                }//print_r($arrdata['pem_prop']);die();
                            }
                            if ($pem == '03') {
                                $pem_kab = explode(", ", $row['pemasaran_kab']);
                                if (count($pem_kab) > 0) {
                                    foreach ($pem_kab as $kab) {
                                        $get_kab = "SELECT id, nama FROM m_kab where id = '".$kab."' ";
                                        $temp_kab = $this->db->query($get_kab)->row_array();
                                        $arrtempk[] = $temp_kab['nama'];
                                    }
                                    $arrdata['pemasaran_kab'][] = array(
                                                        'urai' => $temp['uraian'],
                                                        'detil' => $arrtempk,
                                                        'kode' => $pem);
                                }
                            }
                            $kode_bid = explode(",", $row['bidang_usaha']);
                            if (count($kode_bid) > 0) {
                                foreach ($kode_bid as $kd) {
                                    $get_bid = "SELECT CONCAT(REPLACE(kode, ' ',''),' -- ', e.uraian) AS 'kbli' FROM m_kbli e WHERE REPLACE(kode, ' ','') = '".trim($kd)."'";
                                    $temp_kd = $this->db->query($get_bid)->row_array();
                                    $arrdata['kbli'][] = $temp_kd['kbli'];
                                }
                            }
                        } // print_r($arrdata['kbli']);die();

	                    $get_bapok1 = "SELECT uraian FROM m_reff WHERE jenis = 'JENIS_BAPOK' AND kode = '01' ";
                    	$bapok1 = $this->db->query($get_bapok1)->row_array();
                    	$arrdata['bapok_01'] = $bapok1['uraian'];
	                    $get_01 = "SELECT jns_brg FROM t_bapok_brg WHERE permohonan_id = ".$this->db->escape($id)." AND jns_bapok = '01' ";
	                    $res_01 = $this->db->query($get_01)->result_array();
	                    foreach ($res_01 as $res1) {
	                    	$get_urai1 = "SELECT uraian FROM m_jenis_bapok WHERE id = '".$res1['jns_brg']."' ";
	                    	$urai1[] = $this->db->query($get_urai1)->row_array();
	                    }
                        $arrdata['urai_01'] = $urai1;

	                    $get_bapok2 = "SELECT uraian FROM m_reff WHERE jenis = 'JENIS_BAPOK' AND kode = '02' ";
                    	$bapok2 = $this->db->query($get_bapok2)->row_array();
                    	$arrdata['bapok_02'] = $bapok2['uraian'];
	                    $get_02 = "SELECT jns_brg FROM t_bapok_brg WHERE permohonan_id = ".$this->db->escape($id)." AND jns_bapok = '02' ";
	                    $res_02 = $this->db->query($get_02)->result_array();
	                    foreach ($res_02 as $res2) {
	                    	$get_urai2 = "SELECT uraian FROM m_jenis_bapok WHERE id = '".$res2['jns_brg']."' ";
	                    	$urai2[] = $this->db->query($get_urai2)->row_array();
	                    }
                        $arrdata['urai_02'] = $urai2;

	                    $get_bapok3 = "SELECT uraian FROM m_reff WHERE jenis = 'JENIS_BAPOK' AND kode = '03' ";
                    	$bapok3 = $this->db->query($get_bapok3)->row_array();
                    	$arrdata['bapok_03'] = $bapok3['uraian'];
	                    $get_03 = "SELECT jns_brg FROM t_bapok_brg WHERE permohonan_id = ".$this->db->escape($id)." AND jns_bapok = '03' ";
	                    $res_03 = $this->db->query($get_03)->result_array();
	                    foreach ($res_03 as $res3) {
	                    	$get_urai3 = "SELECT uraian FROM m_jenis_bapok WHERE id = '".$res3['jns_brg']."' ";
	                    	$urai3[] = $this->db->query($get_urai3)->row_array();
	                    }
                        $arrdata['urai_03'] = $urai3;

	                    // $arrdata['detil'] = array(
	                    // 		'bapok' => $bapok,
	                    // 		'jenis' =>
	                    // 	);

	                    // $get_detil = "SELECT id, jns_bapok, jns_brg FROM t_bapok_brg WHERE permohonan_id = '".$id."' ";
	                    // $detil = $this->db->query($get_detil)->result_array();

	                    // foreach ($detil as $dtl) {
	                    // 	$get_bapok = "SELECT uraian FROM m_reff WHERE jenis = 'JENIS_BAPOK' AND kode = '".$dtl['jns_bapok']."' ";
	                    // 	$bapok = $this->db->query($get_bapok)->row_array();
	                    // 	$bapok = $bapok['uraian'];

	                    // 	$get_jns = "SELECT jns_brg 
				                 //    	FROM t_bapok_brg 
				                 //    	WHERE permohonan_id = '".$id."' AND jns_bapok = '".$dtl['jns_bapok']."' ";
	                    // 	$arrjns = $this->db->query($get_jns)->result_array();
	                    // 	// foreach ($arrjns as $jns) {
	                    // 	// 	$get_urai = "SELECT uraian FROM m_jenis_bapok WHERE jenis_bapok = '".$jns['jns_brg']."' ";
	                    // 	// 	$urai[] = $this->db->query($get_urai)->row_array();
	                    // 	// }
	                    // 	$res_detil[] = array(
	                    // 			'bapok' => $bapok,
	                    // 			// 'jenis' => $urai
	                    // 		);
	                    // }
	                    // print_r($res_detil);die();

                    }
                    $arrdata['pejabat_ttd'] = $this->main->set_combobox("SELECT id, nama FROM m_ttd WHERE status = 1 AND kd_izin = " . $doc, "id", "nama", TRUE);
                    $arrdata['act'] = site_url() . 'post/proccess/sipdis_act/verification';
                    $arrdata['input'] = $this->get_input($dir, $doc, $type, $id, $row['status']);
                    $arrdata['proses'] = $this->main->set_proses($dir, $row['kd_izin'], $row['disposisi'], $this->newsession->userdata('role'), $row['status'], hashids_encrypt($row['id'], _HASHIDS_, 9));
                    $arrdata['jmllog'] = $this->main->get_uraian("SELECT COUNT(*) AS JML FROM t_log_izin WHERE permohonan_id = '" . $row['id'] . "' AND kd_izin = '" . $row['kd_izin'] . "'", "JML");
                    $arrdata['urllog'] = site_url() . 'get/log/izin/' . hashids_encrypt($row['id'], _HASHIDS_, 9) . '/' . hashids_encrypt($row['kd_izin'], _HASHIDS_, 9);
                    //print_r($arrdata['proses']);die();
                }
                //print_r($arrdata);die('de');
            }

            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, dbo.dateIndo(b.tgl_dok) as tgl_dok, dbo.dateIndo(b.tgl_exp) as tgl_exp, b.penerbit_dok,
							'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id 
							WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "'";
            $data = $this->main->get_result($query_syarat);
            $arr = array();
            if ($data) {
                /* foreach($query_syarat->result_array() as $keys){
                  $arr[$keys['dok_id']] = $keys;
                  }
                 */
                foreach ($query_syarat->result_array() as $keys) {
                    //$arr[$keys['dok_id']] = $keys;
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess_syarat'] = $arr;
            }
            $query = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.izin_id = '" . $doc . "' " . $addCon . " order by a.tipe desc, a.urutan ASC";
            $data_req = $this->main->get_result($query);
            $arrdata['req'] = $query->result_array();
            $arrdata['dir'] = $dir;
            $arrdata['telaah'] = $this->main->get_telaah($doc);
            $arrdata['agrement'] = $this->main->get_agrement($doc);
            return $arrdata;
        }
    }

    function get_input($dir, $doc, $type, $id, $stts) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrstts = array('0102');
            $arrdata = array();
            if (in_array($stts, $arrstts)) {
                $query = "SELECT a.id, a.kd_izin, a.no_aju, CONVERT(VARCHAR(10), a.tgl_aju, 103) AS tgl_aju, a.no_izin, CONVERT(VARCHAR(10), a.tgl_izin, 103) as tgl_izin, CONVERT(VARCHAR(10), a.tgl_izin_exp, 103) AS tgl_izin_exp FROM t_siupmb a WHERE a.id = '" . $id . "' AND a.kd_izin = '" . $doc . "'";
                $ret = $this->main->get_result($query);
                if ($ret) {
                    $this->ineng = $this->session->userdata('site_lang');
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['dir'] = $dir;
                    $arrdata['doc'] = $doc;
                }
                $arrdata['pejabat_ttd'] = $this->main->set_combobox("SELECT id, nama FROM m_ttd WHERE status = 1 AND kd_izin = " . $doc, "id", "nama", TRUE);
                if (!$this->session->userdata('site_lang'))
                    $this->ineng = "id";
                $data = $this->load->view($this->ineng . '/backend/input/' . $dir . '/' . $stts, $arrdata, true);
            } else {
                $data = "";
            }
        }
        return $data;
    }

    function set_onfly($act, $id, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($act == "update") {
                if (!$isajax) {
                    return false;
                    exit();
                }
                $msg = "MSG||NO";
                $respon = FALSE;
                $arrsiup = $this->main->post_to_query($this->input->post('dataon'));
                $id = hashids_decrypt($id, _HASHIDS_, 9);
                $this->db->where('id', $id);
                $this->db->update('t_siupmb', $arrsiup);
                if ($this->db->affected_rows() == 1) {
                    $respon = TRUE;
                    $logu = array('aktifitas' => 'Melakukan update tanggal izin / tanggal masa berlaku',
                        'url' => '{c}' . site_url() . 'get/onfly/onfly_act/update' . ' {m} models/licensing/siupmb_act {f} set_onfly($act,  $id, $isajax)');
                    $this->main->set_activity($logu);
                }
                if ($respon)
                    $msg = "MSG||YES";
                return $msg;
            }
        }
    }

    public function list_minol() {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT a.id as ID, a.gol as Golongan, a.jenis as 'Jenis Minuman' FROM m_jenis_minol a ";
            //$SQL = "SELECT kode, uraian FROM m_reff WHERE jenis = 'GOL_MINOL'";
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'GOL_MINOL'", "kode", "uraian");

            //print_r($query);die();
            $this->newtable->search(array(array("a.gol", 'Jenis Golongan Minuman Beralkohol', array('ARRAY', $arrstatus)), array("a.jenis", "Jenis Minuman Beralkohol")));
            $table->title("");
            $table->columns(array("a.id", "a.gol", "a.jenis"));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/jns_minol");
            $table->keys(array("ID"));
            $table->hiddens(array("ID"));
            $table->sortby("ASC");
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/add_minol', '0', 'home', 'modal'),
                'Edit' => array('BOOTSTRAPDIALOG', site_url() . 'licensing/dialog_minol/' . $id . '/', '1', 'fa fa-pencil-square-o'),
                'Hapus' => array('POST', site_url() . 'licensing/delete_minol', 'N', 'fa fa-trash-o', 'isngajax')));

            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Laporan Perizinan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    public function add_minol($id, $post) {
        if ($this->newsession->userdata('_LOGGED')) {
            // $id = hashids_decrypt($id, _HASHIDS_, 9);
            $arrdata['gol'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'GOL_MINOL'", "kode", "uraian", TRUE);
            if ($id == '') {
                $arrdata['act'] = site_url('post/licensing/add_minol/save');
            } else {
                $sql = "SELECT id, gol, jenis FROM m_jenis_minol WHERE id = '" . $id . "' ";
                $arrdata['sess'] = $this->db->query($sql)->row_array();
                $arrdata['act'] = site_url('post/licensing/add_minol/update/');
            }
            return $arrdata;
        }
    }

    public function save_minol($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
                exit();
            }
            $msg = "MSG||NO||Data Penambahan Gagal disimpan||REFRESH";
            $resminol = FALSE;
            $arrminol = $this->input->post('minol');
            $arrsave['gol'] = $arrminol['gol'];
            $arrsave['jenis'] = $arrminol['jenis'];
            $arrsave['created'] = 'GETDATE()';
            $this->db->trans_begin();
            $this->db->insert('m_jenis_minol', $arrsave);
            if ($this->db->affected_rows() > 0) {
                $resminol = TRUE;
                $msg = "MSG||YES||Data Penambahan Berhasil Disimpan||REFRESH";
                /* Log User dan Log Izin */
                $logu = array('aktifitas' => 'Menambahkan Master Jenis Minuman Beralkohol dengan Golongan : ' . $arrminol['gol'] . 'dan Jenis : ' . $arrminol['jenis'],
                    'url' => '{c}' . site_url() . 'post/licensing/add_minol/save' . ' {m} models/licensing/siupmb_act {f} add_minol($act)');
                $this->main->set_activity($logu);
                /* Akhir Log User dan Log Izin */
            }
            if ($this->db->trans_status() === FALSE || !$resminol) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
            return $msg;
        }
    }

    public function update_minol($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
                exit();
            }
            $msg = "MSG||NO||Data Gagal Diupdate||REFRESH";
            $resminol = FALSE;
            $arrminol = $this->input->post('minol'); //print_r($arrminol);die();
            $arrsave['gol'] = $arrminol['gol'];
            $arrsave['jenis'] = $arrminol['jenis'];
            $arrsave['created'] = 'GETDATE()';
            $this->db->trans_begin();
            $this->db->where('id', $arrminol['id']);
            $this->db->update('m_jenis_minol', $arrsave); //print_r($this->db->last_query());die();
            if ($this->db->affected_rows() > 0) {
                $resminol = TRUE;
                $msg = "MSG||YES||Data Berhasil Diupdate||REFRESH";
                /* Log User dan Log Izin */
                $logu = array('aktifitas' => 'Mengupdate Master Jenis Minuman Beralkohol dengan Golongan : ' . $arrminol['gol'] . 'dan Jenis : ' . $arrminol['jenis'],
                    'url' => '{c}' . site_url() . 'post/licensing/add_minol/update' . ' {m} models/licensing/siupmb_act {f} update_minol($isajax)');
                $this->main->set_activity($logu);
                /* Akhir Log User dan Log Izin */
            }
            if ($this->db->trans_status() === FALSE || !$resminol) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
            return $msg;
        }
    }

    public function del_minol($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            // $id = hashids_decrypt($id, _HASHIDS_, 9);print_r($id);die();
            $this->db->trans_begin();
            foreach ($id as $data) {
                $this->db->delete('m_jenis_minol', array('id' => $data));
            }
            if ($this->db->affected_rows() > 0) {
                $logu = array('aktifitas' => 'Mendelete Master Minol',
                    'url' => '{c}' . site_url() . 'licensing/delete_minol' . ' {m} models/licensing/siupmb_act {f} del_minol($id)');
                $this->main->set_activity($logu);
            }
            if ($this->db->trans_status === FALSE) {
                $this->db->trans_rollback();
                $msg = "MSG#Proses Gagal#refresh";
            } else {
                $this->db->trans_commit();
                $msg = "MSG#Proses Berhasil#refresh";
            }
            return $msg;
        }
    }

    function referensi_kbli($target, $callback, $fieldcallback) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT kode as kbli, uraian as desc_kbli, kode AS 'Kode KBLI', uraian AS 'Uraian KBLI' FROM m_kbli";
            $table->title("");
            $table->columns(array("kode", "Kode KBLI", "uraian", "Uraian KBLI"));
            $this->newtable->width(array('Kode KBLI' => 100, 'Uraian' => 300, '&nbsp;' => 5));
            $this->newtable->search(array(array("kode", "Kode KBLI"),
                array("uraian", "Uraian")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_kbli/" . $target . '/' . $callback . '/' . $fieldcallback);
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("kode"));
            $table->hiddens(array("kbli", "desc_kbli"));
            $table->use_ajax(TRUE);
            $table->show_search(TRUE);
            $table->show_chk(FALSE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);
            $table->settrid(TRUE);
            $table->attrid($target);
            if ($callback != "")
                $table->callback(site_url(str_replace(".", '/', $callback)));
            if ($fieldcallback != "")
                $table->fieldcallback($fieldcallback);
            $table->tbtarget("refkbli");
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function list_dok($target, $callback, $fieldcallback, $id, $multiple, $putin, $doc, $permohonan_id) {
        if ($this->newsession->userdata('_LOGGED')) {
            //print_r($permohonan_id);die();
            if ($this->newsession->userdata('role') == '01') {
                $sql_trader = "SELECT trader_id FROM dbo.view_permohonan WHERE id = " . $permohonan_id;
                $trader_id = $this->main->get_uraian($sql_trader, "trader_id");
            } else {
                $trader_id = $this->newsession->userdata('trader_id');
            }
            $table = $this->newtable;
            $query = "SELECT id, id AS 'upload_id', nomor AS 'No. Dokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'Tgl. Dokumen', tgl_exp AS 'Tgl. Akhir', '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
			FROM t_upload WHERE tipe_dok = '" . $id . "' AND trader_id = '" . $trader_id . "'";
            //print_r($query);die();
            $table->title("");
            $table->columns(array("id", "nomor", "penerbit_dok", "tgl_dok", "tgl_exp", "<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
            $this->newtable->width(array('Nomor Dokumen' => 100, 'Penerbit' => 300, 'Tanggal Awal' => 100, 'Tanggal Akhir' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("nomor", "Nomor Penerbit"),
                array("penerbit_dok", "Penerbit")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_syarat/" . $id . '/' . $target . '/' . $callback . '/' . $fieldcallback .'/'.$id.'/'.$multiple.'/'.$putin.'/'.$doc.'/'.$permohonan_id);
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id", "folder", "nama_file", "upload_id"));
            $table->show_search(TRUE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);

            if ((int) $multiple == 1) {
                $table->show_scroll(TRUE);
                $table->show_chk(TRUE);
                $table->menu(array('Pilih Data' => array('POSTGET', site_url() . 'post/document/get_requirements/' . $doc . '/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#' . $putin)));
            } else {
                $table->show_chk(FALSE);
                if ($callback != "")
                    $table->callback(site_url(str_replace(".", '/', $callback)));
                if ($fieldcallback != "")
                    $table->fieldcallback($fieldcallback);
                $table->settrid(TRUE);
                $table->attrid($target);
            }
            $table->tbtarget("refreq_" . rand());
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function list_spp($target, $callback, $fieldcallback, $id, $multiple, $putin, $doc) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT id, id AS 'upload_id', nomor AS 'No. Dokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'Tgl. Dokumen', tgl_exp AS 'Tgl. Akhir', '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
			FROM t_upload WHERE tipe_dok = '" . $id . "' AND trader_id = '" . $this->newsession->userdata('trader_id') . "'";
            $table->title("");
            $table->columns(array("id", "nomor", "penerbit_dok", "tgl_dok", "tgl_exp", "<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
            $this->newtable->width(array('Nomor Dokumen' => 100, 'Penerbit' => 300, 'Tanggal Awal' => 100, 'Tanggal Akhir' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("nomor", "Nomor Penerbit"),
                array("penerbit_dok", "Penerbit")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_syarat/" . $id . '/' . $target . '/' . $callback . '/' . $fieldcallback . '/');
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id", "folder", "nama_file", "upload_id"));
            $table->show_search(TRUE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);

            if ((int) $multiple == 1) {
                $table->show_chk(TRUE);
                $table->menu(array('Pilih Data' => array('POSTGET', site_url() . 'post/document/get_requirements/' . $doc . '/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#' . $putin)));
            } else {
                $table->show_chk(FALSE);
                if ($callback != "")
                    $table->callback(site_url(str_replace(".", '/', $callback)));
                if ($fieldcallback != "")
                    $table->fieldcallback($fieldcallback);
                $table->settrid(TRUE);
                $table->attrid($target);
            }
            $table->tbtarget("refreq_" . rand());
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function get_requirements($doc) {
        $id = join("','", $this->input->post('tb_chk'));
        $data = "SELECT DISTINCT a.id AS dok_ids, b.id AS id_upload, '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor AS nomor, b.penerbit_dok, b.tgl_dok, b.tgl_exp, '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS files, c.tipe FROM m_dok a LEFT JOIN t_upload b ON a.id = b.tipe_dok LEFT JOIN m_dok_izin c ON c.dok_id = a.id WHERE b.id IN('" . $id . "') AND c.izin_id = '" . $doc . "'";
        //print_r($data);die();
        return $this->db->query($data)->result_array();
    }

    function referensi_old_doc($target, $callback, $fieldcallback, $dir, $doc) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT id, id as id_old, no_aju as 'No. Pengajuan', no_izin as 'Nomor Izin', tgl_izin as 'Tanggal Izin', tgl_izin_exp as 'Tanggal Akhir Izin'
					  FROM t_siupmb  
					  WHERE kd_izin = " . $doc . " AND status = '1000'";
            $table->title("");
            $table->columns(array("id", "No. Pengajuan", "Nomor Izin", "Tanggal Izin", "Tanggal Akhir Izin"));
            $this->newtable->width(array('No. Pengajuan' => 100, 'Nomor Izin' => 100, 'Tanggal Izin' => 100, 'Tanggal Akhir Izin' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("no_aju", "No. Pengajuan"),
                array("no_izin", "Nomor Izin")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_old_doc/" . $target . '/' . $callback . '/' . $fieldcallback);
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id", "id_old"));
            $table->use_ajax(TRUE);
            $table->show_search(TRUE);
            $table->show_chk(FALSE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);
            $table->settrid(TRUE);
            $table->attrid($target);
            if ($callback != "")
                $table->callback(site_url(str_replace(".", '/', $callback)));
            if ($fieldcallback != "")
                $table->fieldcallback($fieldcallback);
            $table->tbtarget("refolddoc");
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

}

?>