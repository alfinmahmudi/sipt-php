<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email_act extends CI_Model {

    function get_data_permohonan($status, $direktorat, $kd_izin, $permohonan_id) {
        $arrsiupmb = array("3", "4", "5", "6", "13");
        $arrpameran = array("15", "16");
        $arrstpw = array("17", "18", "19", "20", "21");
        $field = "";
        if (in_array($kd_izin, $arrsiupmb)) {
            $dbTable = "t_siupmb";
        } elseif ($kd_izin == "1") {
            $dbTable = "t_siujs";
        } elseif ($kd_izin == "2") {
            $dbTable = "t_siup4";
        } elseif ($kd_izin == "12") {
            $dbTable = "t_siup_agen";
        } elseif ($kd_izin == "22") {
            $dbTable = "t_bapok";
        } elseif ($kd_izin == "23") {
            $dbTable = "t_label";
        }elseif ($kd_izin == "7") {
            $dbTable = "t_pkapt";
        } elseif ($kd_izin == "8") {
            $dbTable = "t_pgapt";
        } elseif ($kd_izin == "9") {
            $dbTable = "t_sppgapt";
        } elseif ($kd_izin == "10") {
            $dbTable = "t_sppgrapt";
        } elseif ($kd_izin == "11") {
            $dbTable = "t_siupbb";
        } elseif ($kd_izin == "14") {
            $dbTable = "t_garansi";
        } elseif (in_array($kd_izin, $arrpameran)) {
            $dbTable = "t_pameran";
        } elseif (in_array($kd_izin, $arrstpw)) {
            $dbTable = "t_stpw";
        }
        $query = "SELECT top 1 a.no_aju, dbo.dateIndo(a.tgl_aju) AS tgl_aju, dbo.npwp(a.npwp)as npwp, e.uraian as direktorat, d.nama_izin, 
				dbo.dateIndo(a.tgl_izin) AS tgl_izin, dbo.dateIndo(a.tgl_terima) AS tgl_terima, a.no_izin, a.nm_perusahaan, 
				a.nama_pj, b.email_pj, c.catatan, a.source, a.trader_id, a.tgl_izin as tgl_dok, a.tgl_izin_exp as tgl_exp, f.email as koor_email
				FROM " . $dbTable . " a
				LEFT JOIN m_trader b on b.id = a.trader_id
				LEFT JOIN t_log_izin c on c.permohonan_id = a.id and c.kd_izin = a.kd_izin 
				LEFT JOIN m_izin d on d.id = a.kd_izin
				LEFT JOIN m_reff e on e.kode = d.direktorat_id AND e.jenis = 'DIREKTORAT'
                LEFT JOIN t_user f ON f.trader_id = a.trader_id
				WHERE a.id = " . $permohonan_id . " AND a.kd_izin = " . $kd_izin . "  order by waktu desc"; //print_r($query);die();
        if($kd_izin == "23"){
            $query = "SELECT top 1 a.no_aju, dbo.dateIndo(a.tgl_aju) AS tgl_aju, dbo.npwp(a.npwp)as npwp, e.uraian as direktorat, d.nama_izin, 
				dbo.dateIndo(a.tgl_izin) AS tgl_izin, dbo.dateIndo(a.tgl_terima) AS tgl_terima, a.no_izin, a.nm_perusahaan, 
				a.nama_pj, b.email_pj, c.catatan, a.trader_id, a.tgl_izin as tgl_dok, a.tgl_izin_exp as tgl_exp, f.email as koor_email
				FROM " . $dbTable . " a
				LEFT JOIN m_trader b on b.id = a.trader_id
				LEFT JOIN t_log_izin c on c.permohonan_id = a.id and c.kd_izin = a.kd_izin 
				LEFT JOIN m_izin d on d.id = a.kd_izin
				LEFT JOIN m_reff e on e.kode = d.direktorat_id AND e.jenis = 'DIREKTORAT'
                LEFT JOIN t_user f ON f.trader_id = a.trader_id
				WHERE a.id = " . $permohonan_id . " AND a.kd_izin = " . $kd_izin . "  order by waktu desc"; //print_r($query);die();
        }

        $data = $this->main->get_result($query);
        if ($data) {
            foreach ($query->result_array() as $row) {
                $arrdata = $row;
            }
        }
        $query = "SELECT TOP 1 folder, nama_file, ekstensi
						  FROM t_upload
					  where trader_id = '" . $arrdata['trader_id'] . "' and nomor = '" . $arrdata['no_izin'] . "' and tgl_dok = '" . $arrdata['tgl_dok'] . "' and tgl_exp = '" . $arrdata['tgl_exp'] . "'  order by id desc  ";

        $data = $this->main->get_result($query);
        if ($data) {
            foreach ($query->result_array() as $row) {
                $arrfile = $row;
            }
        }
        $arrdata['file'] = $arrfile;
        return $arrdata;
    }

    public function email_pelaku_usaha_beras($status, $direktorat, $kd_izin, $permohonan_id, $tipe, $resend = "") {
        $data_permohonan = $this->get_data_permohonan($status, $direktorat, $kd_izin, $permohonan_id);
        $emails = $data_permohonan['email_pj'] . ',' . $data_permohonan['koor_email'];
        $send_email = 0;
        $subject = 'Pengajuan Permohonan ' . $tipe . ' SIPT PDN';
        $message = '<html><head><title>' . $subject . '</title></head><body>';
        $message .= '<p>Yth Bapak/Ibu ' . $data_permohonan['nama_pj'] . '</p>';
        $message .= '<p>Bersama ini disampaikan bahwa Pendaftaran Label Beras dengan: <br> 
						Nomor Registrasi : <b>' . $data_permohonan['no_aju'] . '</b><br> 
						Tanggal Registrasi : <b>' . $data_permohonan['tgl_aju'] . '</b><br>
						NPWP Perusahaan : <b>' . $data_permohonan['npwp'] . '</b><br>
						Nama Perusahaan : <b>' . $data_permohonan['nm_perusahaan'] . '</b><br> Telah berhasil Terdaftar<br><br>Demikian disampaikan, atas perhatian dan kerjasama Bapak/Ibu diucapkan terima kasih.<br><br>Admin</p>';
        $message .= '<p>* This is a system-generated email, please do not reply.</p>';
        $message .= '</body></html>';
        $arrdata['emails'] = $emails;
        $arrdata['subject'] = $subject;
        $arrdata['message'] = $message;

        if (($message != "") && ($subject) && ($emails) && ($status)) {
            $send_email = $this->main->send_email($emails, $message, $subject, $attachment);
        }
        return $send_email;
    }

    public function email_pelaku_usaha($status, $direktorat, $kd_izin, $permohonan_id, $tipe, $resend = "") {
        $data_permohonan = $this->get_data_permohonan($status, $direktorat, $kd_izin, $permohonan_id);
        $emails = $data_permohonan['email_pj'] . ',' . $data_permohonan['koor_email'];
        $send_email = 0;
        if ($status == '0100') { #dikirim
            $subject = 'Pengajuan Permohonan ' . $tipe . ' SIPT PDN';
            $message = '<html><head><title>' . $subject . '</title></head><body>';
            $message .= '<p>Yth Bapak/Ibu ' . $data_permohonan['nama_pj'] . '</p>';
            $message .= '<p>Bersama ini kami sampaikan bahwa permohonan pelayanan perizinan ' . $data_permohonan['nama_izin'] . ' dengan: <br> 
						Nomor Registrasi : <b>' . $data_permohonan['no_aju'] . '</b><br> 
						Tanggal Registrasi : <b>' . $data_permohonan['tgl_aju'] . '</b><br>
						NPWP Perusahaan : <b>' . $data_permohonan['npwp'] . '</b><br>
						Nama Perusahaan : <b>' . $data_permohonan['nm_perusahaan'] . '</b><br> Telah berhasil terkirim, Silahkan mengecek pada histori permohonan untuk mengetahui status permohonan anda.<br><br>Demikian, untuk menjadi perhatian Saudara.<br><br>Admin</p>';
            $message .= '<p>* This is a system-generated email, please do not reply.</p>';
            $message .= '</body></html>';
        }

        if ($status == '0700') { #diterima
            $subject = 'Terima Permohonan ' . $tipe . ' SIPT PDN';
            $message = '<html><head><title>' . $subject . '</title></head><body>';
            $message .= '<p>Yth Bapak/Ibu ' . $data_permohonan['nama_pj'] . '</p>';
            $message .= '<p>Bersama ini kami sampaikan bahwa permohonan pelayanan perizinan ' . $data_permohonan['nama_izin'] . ' 
						dengan nomor registrasi <b>' . $data_permohonan['no_aju'] . '</b> tanggal registrasi <b>' . $data_permohonan['tgl_aju'] . '</b> 
						atas nama <b>' . $data_permohonan['nm_perusahaan'] . '</b> pada tanggal ' . $data_permohonan['tgl_terima'] . ' 
						telah diterima dan akan diproses lebih lanjut.<br><br>
						Demikian, untuk menjadi perhatian Saudara.<br><br>Admin</p>';
            $message .= '<p>* This is a system-generated email, please do not reply.</p>';
            $message .= '</body></html>';
        }

        if ($status == '0501') { #ditolak
            $subject = 'Penolakan Permohonan ' . $tipe . ' SIPT PDN';
            $message = '<html><head><title>' . $subject . '</title></head><body>';
            $message .= '<p>Yth Bapak/Ibu ' . $data_permohonan['nama_pj'] . '</p>';
            $message .= '<p>Bersama ini kami sampaikan bahwa berdasarkan hasil pemeriksaan atas permohonan izin ' . $data_permohonan['nama_izin'] . ', 
						dengan nomor registrasi <b>' . $data_permohonan['no_aju'] . '</b> tanggal registrasi <b>' . $data_permohonan['tgl_aju'] . '</b> 
						atas nama <b>' . $data_permohonan['nm_perusahaan'] . '</b>, ternyata data tidak sesuai dan benar 
						karena <br><b>' . $data_permohonan['catatan'] . '</b>.<br><br>
						Berdasarkan hal-hal diatas, kami menolak untuk menerbitkan permohonan saudara.<br><br>
						Demikian, untuk menjadi perhatian Saudara.<br><br>
						Direktur ' . $data_permohonan['direktorat'] . '</p><br>';
            $message .= '<p>Tembusan:</p>';
            $message .= '<p>1. Dirjen PDN;</p>';
            $message .= '<p>2. Irjen Kemendag;</p><br><br>';
            $message .= '<p>* This is a system-generated email, please do not reply.</p>';
            $message .= '</body></html>';
        }

        if ($status == '0001') { #dirollback
            $subject = 'Pengembalian Dokumen Permohonan ' . $tipe . ' SIPT PDN';
            $message = '<html><head><title>' . $subject . '</title></head><body>';
            $message .= '<p>Yth Bapak/Ibu ' . $data_permohonan['nama_pj'] . '</p>';
            $message .= '<p>Bersama ini kami sampaikan bahwa berdasarkan  hasil pemeriksaan atas permohonan izin ' . $data_permohonan['nama_izin'] . ', 
						dengan nomor registrasi <b>' . $data_permohonan['no_aju'] . '</b> tanggal registrasi <b>' . $data_permohonan['tgl_aju'] . '</b> 
						atas nama <b>' . $data_permohonan['nm_perusahaan'] . '</b>, 
						ternyata data tidak sesuai dan benar karena <br><b>' . $data_permohonan['catatan'] . '</b>.<br><br>
						Berdasarkan hal – hal diatas, silakahkan untuk memperbaiki data permohonan dan mengirimkannya kembali.<br><br>
						Demikian, untuk menjadi perhatian Saudara.<br><br>Admin</p>';
            $message .= '<p>* This is a system-generated email, please do not reply.</p>';
            $message .= '</body></html>';
        }

        if ($status == '1000' || $status == '0500' || $status == 'resend_mail') { #diterbitkan 
            if ($data_permohonan['source'] != '1') {

                $attachment = $data_permohonan['file']['folder'] . "/" . $data_permohonan['file']['nama_file'];
                $subject = 'Penerbitan Permohonan ' . $tipe . ' SIPT PDN';
                $message = '<html><head><title>' . $subject . '</title></head><body>';
                $message .= '<p>Yth Bapak/Ibu ' . $data_permohonan['nama_pj'] . '</p>';
                $message .= '<p>Bersama ini kami sampaikan bahwa permohonan pelayanan perizinan ' . $data_permohonan['nama_izin'] . ' 
							dengan nomor registrasi <b>' . $data_permohonan['no_aju'] . '</b> tanggal registrasi <b>' . $data_permohonan['tgl_aju'] . '</b> 
							atas nama <b>' . $data_permohonan['nm_perusahaan'] . '</b> pada tanggal <b>' . $data_permohonan['tgl_izin'] . '</b> 
							telah selesai di proses. Adapun Surat Izin <b>' . $data_permohonan['nama_izin'] . '</b> 
							memiliki Nomor sertifikat <b>' . $data_permohonan['no_izin'] . '</b> tertanggal <b>' . $data_permohonan['tgl_izin'] . '</b> sebagaimana terlampir.<br><br>
							Demikian, untuk menjadi perhatian Saudara.<br><br>Admin</p>';
                $message .= '<p>* This is a system-generated email, please do not reply.</p>';
                if ($kd_izin == '22') {
                    $message .= '<p>Harap Melakuka Pelaporan Setiap Bulannya Sebelum Tanggal 15 di Bulan Izin Terbit</p>';
                }
                $message .= '</body></html>';
            } else { #tarik dari upp
                $subject = 'Penerbitan Permohonan ' . $tipe . ' SIPT PDN';
                $message = '<html><head><title>' . $subject . '</title></head><body>';
                $message .= '<p>Yth Bapak/Ibu ' . $data_permohonan['nama_pj'] . '</p>';
                $message .= '<p>Bersama ini kami sampaikan bahwa permohonan pelayanan perizinan ' . $data_permohonan['nama_izin'] . ' 
							dengan nomor registrasi <b>' . $data_permohonan['no_aju'] . '</b> tanggal registrasi <b>' . $data_permohonan['tgl_aju'] . '</b> 
							atas nama <b>' . $data_permohonan['nm_perusahaan'] . '</b> pada tanggal <b>' . $data_permohonan['tgl_izin'] . '</b> 
							telah selesai di proses. Untuk itu, Saudara dapat mengambil <b>' . $data_permohonan['nama_izin'] . '</b> 
							di Unit Pelayanan Terpadu Perdagangan,   Gedung Utama Lantai 2 . Jl. MI. Ridwan Rais No. 5 Jakarta Pusat. 10110.<br><br>
							Demikian, untuk menjadi perhatian Saudara.<br><br>Admin</p>';
                $message .= '<p>* This is a system-generated email, please do not reply.</p>';
                $message .= '</body></html>';
            }
        }
        $arrdata['emails'] = $emails;
        $arrdata['subject'] = $subject;
        $arrdata['message'] = $message;

        if (($message != "") && ($subject) && ($emails) && ($status)) {
            $send_email = $this->main->send_email($emails, $message, $subject, $attachment);
        }
        return $send_email;
    }

    public function email_pemroses($status, $direktorat, $kd_izin, $permohonan_id, $tipe) {
        $send_email = 0;
        $query_email = "SELECT a.email, c.uraian as jabatan, b.role
						FROM t_user a
						LEFT JOIN t_user_role b ON b.user_id = a.id
						LEFT JOIN m_reff c on c.kode = b.role and jenis ='ROLE'
						WHERE b.role in (SUBSTRING( '" . $status . "', 1, 2 )) AND a.status = '00' AND b.izin_id = " . $kd_izin;
        $data = $this->main->get_result($query_email);


        $arrRollback = array("0105", "0701", "0601");
        $arrApprove = array("0100", "0700", "0600", "0200", "0102");

        $data_permohonan = $this->get_data_permohonan($status, $direktorat, $kd_izin, $permohonan_id);
        if (in_array($status, $arrRollback)) { #rollback
            $lbl = 'rollback';
            $catatan = 'Catatan : <b>' . $data_permohonan['catatan'] . '</b><br> ';
        } elseif (in_array($status, $arrApprove)) { #approve
            $lbl = $tipe;
        }
        if ($data) {
            $email = $query_email->result_array();
            for ($i = 0; $i < count($email); $i++) {
                $emails = $email[$i]['email'];
                $jabatan = $email[$i]['jabatan'];
                $role = $email[$i]['role'];
                if ($role == '01') {
                    $subject = 'Pengajuan Permohonan ' . $tipe . ' SIPT PDN';
                    $message = '<html><head><title>' . $subject . '</title></head><body>';
                    $message .= '<p>Yth Bapak/Ibu ' . $jabatan[0] . '</p>';
                    $message .= '<p>Terdapat permohonan  ' . $lbl . '  perizinan ' . $data_permohonan['nama_izin'] . ' dengan: <br> 
					Nomor Registrasi : <b>' . $data_permohonan['no_aju'] . '</b><br> 
					Tanggal Registrasi : <b>' . $data_permohonan['tgl_aju'] . '</b><br>
					NPWP Perusahaan : <b>' . $data_permohonan['npwp'] . '</b><br>
					Nama Perusahaan : <b>' . $data_permohonan['nm_perusahaan'] . '</b><br> 
					' . $catatan . '
					Silahkan untuk melakukan validasi dan pengecekan dengan mengakses aplikasi SIPT PDN.<br>
					<a href="' . site_url() . 'internal' . '">Link Aplikasi</a><br><br><br>
					Demikian, untuk menjadi perhatian Saudara.<br><br>Admin</p>';
                    $message .= '<p>* This is a system-generated email, please do not reply.</p>';
                    $message .= '</body></html>';
                } else {
                    $subject = 'Pengajuan Permohonan ' . $tipe . ' SIPT PDN';
                    $message = '<html><head><title>' . $subject . '</title></head><body>';
                    $message .= '<p>Yth Bapak/Ibu ' . $jabatan[0] . '</p>';
                    $message .= '<p>Terdapat permohonan  ' . $lbl . '  perizinan ' . $data_permohonan['nama_izin'] . ' dengan: <br> 
					Nomor Registrasi : <b>' . $data_permohonan['no_aju'] . '</b><br> 
					Tanggal Registrasi : <b>' . $data_permohonan['tgl_aju'] . '</b><br>
					NPWP Perusahaan : <b>' . $data_permohonan['npwp'] . '</b><br>
					Nama Perusahaan : <b>' . $data_permohonan['nm_perusahaan'] . '</b><br> 
					' . $catatan . '
					<br><br>Admin</p>';
                    $message .= '<p>* This is a system-generated email, please do not reply.</p>';
                    $message .= '</body></html>';
                }
                $arrdata['emails'] = $emails;
                $arrdata['subject'] = $subject;
                $arrdata['message'] = $message;

                if (($message != "") && ($subject) && ($emails) && ($status)) {
                    $send_email = $this->main->send_email($emails, $message, $subject);
                }
            }
        }
        return $send_email;
    }

}

?>