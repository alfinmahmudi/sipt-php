<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Label_act extends CI_Model {

    var $ineng = "";

    function get_first($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata = array();

            $idprop = $this->newsession->userdata('kdprop');
            $idkab = $this->newsession->userdata('kdkab');
            $idkec = $this->newsession->userdata('kdkec');
            $idkel = $this->newsession->userdata('kdkel'); //print_r($idkel);die();
            $tipe_perusahaan = $this->newsession->userdata('tipe_perusahaan');

            $get_drop = "SELECT b.nama as 'nm_prop', c.nama as 'nm_kab', d.nama as 'nm_kec', e.nama as 'nm_kel', f.uraian as 'nm_tipe'
                        FROM m_trader a
                        LEFT JOIN m_prop b ON b.id = a.kdprop
                        LEFT JOIN m_kab c ON c.id = a.kdkab
                        LEFT JOIN m_kec d ON d.id = a.kdkec
                        LEFT JOIN m_kel e ON e.id = a.kdkel
                        LEFT JOIN m_reff f ON f.kode = a.tipe_perusahaan AND f.jenis = 'TIPE_PERUSAHAAN'
                        WHERE a.id = '" . $this->newsession->userdata('trader_id') . "' ";
            $arrdata['drop'] = $this->db->query($get_drop)->row_array();
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $idprop . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '" . $idkab . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '" . $idkec . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '" . $idkel . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['propinsi_usaha'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kabupaten_usaha'] = $this->main->set_combobox("SELECT id, nama FROM m_kab ORDER BY 2", "id", "nama", TRUE);
            $arrdata['direktorat'] = $dir;
            $arrdata['tipe'] = $type;
            $arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian", TRUE);
            $arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN' and kode = '" . $tipe_perusahaan . "' ", "kode", "uraian", TRUE);
            $arrdata['lokasi'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'LOKASI_PERUSAHAAN'", "kode", "uraian", TRUE);
            $arrdata['impor'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'IMPORTIR_BERAS'", "kode", "uraian", TRUE);
            $arrdata['status_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS_PERUSAHAAN'", "kode", "uraian", TRUE);
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;

            if ($id == "") {
                $arrdata['act'] = site_url('post/licensing/label_act/first/save');
//$type = hashids_decrypt($type,_HASHIDS_,6);
                $arrdata['sess']['tipe_permohonan'] = $type;
                $arrdata['sess']['kd_izin'] = $doc;
            } else {
                $arrdata['act'] = site_url('post/licensing/label_act/first/update');
                $query = "SELECT id, kd_izin, no_aju, dbo.dateIndo(tgl_aju), id_permohonan_lama, no_izin_lama, dbo.dateIndo(tgl_izin_lama), dbo.dateIndo(tgl_izin_exp_lama),trader_id, tipe_permohonan, npwp, tipe_perusahaan, nm_perusahaan, almt_perusahaan, kdprop, kdkab, kdkec, kdkel, kdpos, telp, fax
					FROM t_label
					WHERE id = '" . $id . "'";
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['readonly'] = "readonly";
                    $arrdata['disabled'] = "disabled";
// $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id = '".$kdkab."'	 ORDER BY 2","id", "nama", TRUE);
// $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '".$row['kdkec']."' ORDER BY 2","id", "nama", TRUE);
                    $arrdata['kec_usaha'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab_usaha'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel_usaha'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec_usaha'] . "' ORDER BY 2", "id", "nama", TRUE);
                }
            }
            // print_r($arrdata);die();
            return $arrdata;
        }
    }

    function set_first($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            if ($this->newsession->userdata('role') == '01') {
                $sqlSource = "SELECT source FROM t_bapok WHERE id = " . $this->input->post('id');
                $source = $this->main->get_uraian($sqlSource, 'source');
                if ($source != '1') {
                    return "MSG||NO||Mohon maaf, pengajuan online tidak dapat diperbaharui oleh pemroses.";
                    exit();
                }
            }

            $arrsiup = $this->main->post_to_query($this->input->post('bapok'));
            // print_r($arrsiup);die();
            $dir = $this->input->post('direktorat');
            $doc = $arrsiup['kd_izin'];
            $type = $arrsiup['tipe_permohonan'];

            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');

            $msg = "MSG||NO||Data Permohonan " . $nama_izin . " gagal disimpan";
            $ressiup = FALSE;

            if ($act == "save") {
                $arrsiup['no_aju'] = $this->main->set_aju();
                $arrsiup['fl_pencabutan'] = '0';
                $arrsiup['status'] = '0000';
                $arrsiup['tgl_aju'] = 'GETDATE()';
                $arrsiup['trader_id'] = $this->newsession->userdata('trader_id');
                $arrsiup['created'] = 'GETDATE()';
                $arrsiup['created_user'] = $this->newsession->userdata('username');
                if (($type != '01') && ($arrsiup['id_permohonan_lama'] != "")) {
                    if ($type == '03')
                        $arrsiup['fl_pembaharuan'] = "1";
                    $query_old_doc = "SELECT identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, 
									 alamat_pj, kdprop_pj, kdkab_pj, kdkel_pj, kdkec_pj,telp_pj, fax_pj
									 FROM t_label WHERE id = " . $arrsiup['id_permohonan_lama'] . " AND kd_izin = " . $arrsiup['kd_izin'];
                    $data_old = $this->main->get_result($query_old_doc);
                    if ($data_old) {
                        foreach ($query_old_doc->result_array() as $keys) {
                            $jml = count($keys);
                            $array_keys = array_keys($keys);
                            $array_values = array_values($keys);
                            for ($i = 0; $i < $jml; $i++) {
                                $arrsiup[$array_keys[$i]] = $array_values[$i];
                            }
                        }
                    }
                }
                $this->db->trans_begin();
                $exec = $this->db->insert('t_label', $arrsiup); //print_r($this->db->last_query());die();
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idredir = $this->db->insert_id();
                    $msg = "MSG||YES||Data Permohonan " . $nama_izin . " berhasil disimpan.\nSilahkan lanjutkan mengisi data penanggung jawab||" . site_url() . 'licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $idredir;

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $nama_izin . ' dengan nomor permohonan : ' . $arrsiup['no_aju'],
                        'url' => '{c}' . site_url() . 'post/licensing/label_act/first/save' . ' {m} models/licensing/label_act {f} set_first($act, $ajax)');
                    $this->main->set_activity($logu);
                    $logi = array('kd_izin' => $arrsiup['kd_izin'],
                        'permohonan_id' => $idredir,
                        'keterangan' => 'Menambahkan daftar permohonan ' . $nama_izin . ' dengan nomor permohonan : ' . $arrsiup['no_aju'],
                        'catatan' => '',
                        'status' => '0000',
                        'selisih' => 0);
                    $this->main->set_loglicensing($logi);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } elseif ($act == "update") {
                $id = $this->input->post('id');
                $this->db->where('id', $id);
                $this->db->update('t_label', $arrsiup);
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $msg = "MSG||YES||Data Permohonan " . $nama_izin . " berhasil diupdate.||REFRESH";
                }
                return $msg;
            }
        }
    }

    function get_second($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $jenis_identitas = $this->newsession->userdata('tipe_identitas');
            $id_user = $this->newsession->userdata('id');
            $idprop = $this->newsession->userdata('kdprop_pj');
            $idkab = $this->newsession->userdata('kdkab_pj');
            $idkec = $this->newsession->userdata('kdkec_pj');
            $idkel = $this->newsession->userdata('kdkel_pj');
            $jabatan_pj = $this->newsession->userdata('jabatan_pj');

            $get_drop = "SELECT b.nama as 'nm_prop', c.nama as 'nm_kab', d.nama as 'nm_kec', e.nama as 'nm_kel', f.uraian as 'nm_iden',
                        g.uraian as 'nm_jab'
                        FROM m_trader a
                        LEFT JOIN m_prop b ON b.id = a.kdprop_pj
                        LEFT JOIN m_kab c ON c.id = a.kdkab_pj
                        LEFT JOIN m_kec d ON d.id = a.kdkec_pj
                        LEFT JOIN m_kel e ON e.id = a.kdkel_pj
                        LEFT JOIN m_reff f ON f.kode = a.identitas_pj AND f.jenis = 'JENIS_IDENTITAS'
                        LEFT JOIN m_reff g ON g.kode = a.jabatan_pj AND g.jenis = 'JABATAN'
                        WHERE a.id = '" . $this->newsession->userdata('trader_id') . "' ";
            $arrdata['drop'] = $this->db->query($get_drop)->row_array();

            $arrdata['act'] = site_url('post/licensing/label_act/second/save');
            $arrdata['direktorat'] = $dir;
            $arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '" . $jenis_identitas . "' and jenis = 'JENIS_IDENTITAS'", "kode", "uraian", TRUE);
            $arrdata['jabatan_pj'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JABATAN' AND kode = '" . $jabatan_pj . "' ", "kode", "uraian", TRUE);
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $idprop . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '" . $idkab . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '" . $idkec . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '" . $idkel . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['urifirst'] = site_url('licensing/form/first/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            $query = "SELECT id, kd_izin, tipe_permohonan, nama_pj, identitas_pj, jabatan_pj, tmpt_lahir_pj, CONVERT(VARCHAR(11), tgl_lahir_pj, 105) AS tgl_lahir_pj, alamat_pj, kdprop_pj, kdkab_pj, kdkec_pj,kdkel_pj, telp_pj, fax_pj, kdprop, kdkab, kdkec, kdkel
						FROM t_label WHERE id = '" . $id . "'"; //die($query);
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
                if ($row['identitas_pj']) {
                    $arrdata['act'] = site_url('post/licensing/label_act/second/update');
                }
                $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            return $arrdata;
        }
    }

    function set_second($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (($act == "update") || ($act == "save")) {
                if (!$isajax) {
                    return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                    exit();
                }
                if ($this->newsession->userdata('role') == '01') {
                    $sqlSource = "SELECT source FROM t_bapok WHERE id = " . $this->input->post('id');
                    $source = $this->main->get_uraian($sqlSource, 'source');
                    if ($source != '1') {
                        return "MSG||NO||Mohon maaf, pengajuan online tidak dapat diperbaharui oleh pemroses.";
                        exit();
                    }
                }
                $msg = "MSG||NO||Data Penanggung Jawab gagal disimpan";
                $respj = FALSE;
                $arrpj = $this->main->post_to_query($this->input->post('pj'));
                $dir = $this->input->post('direktorat');
                $doc = $this->input->post('kd_izin');
                $type = $this->input->post('tipe_permohonan');
                $id = $this->input->post('id');

                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');

                $this->db->where('id', $id);
                $this->db->update('t_label', $arrpj);
                if ($this->db->affected_rows() > 0) {
                    $respj = TRUE;
                    if ($act == "update") {
                        $msg = "MSG||YES||Data Penanggung Jawab berhasil diupdate.||REFRESH";
                    } else {
                        $msg = "MSG||YES||Data Penanggung Jawab berhasil disimpan. \nSilahkan lanjutkan mengisi data nilai modal dan kekayaan||" . site_url() . 'licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id;
                    }
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $nama_izin . ' (Data Penanggung Jawab)',
                        'url' => '{c}' . site_url() . 'post/licensing/label_act/second/update' . ' {m} models/licensing/label_act {f} set_second($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                }
                return $msg;
            }
        }
    }

    function get_third($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $arrdata['act'] = site_url('post/licensing/label_act/third/save');
                $arrdata['urisecond'] = site_url('licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $id);

                $query = "SELECT id, kd_izin, tipe_permohonan
                          FROM t_label WHERE id = '" . $id . "'";
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                }

                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $arrdata['nama_izin'] = $nama_izin;
                $arrdata['direktorat'] = $dir;


                $arrdata['kd_jenis_beras'] = $this->main->set_combobox("SELECT id, uraian FROM m_beras", "id", "uraian", TRUE);
                $arrdata['kd_jenis_khusus'] = $this->main->set_combobox("SELECT id, uraian FROM m_beras_khusus", "id", "uraian", TRUE);
                $arrdata['kd_campuran'] = $this->main->set_combobox("SELECT id, uraian FROM m_beras_campuran", "id", "uraian", TRUE);
                $arrdata['kd_kemasan'] = $this->main->set_combobox("SELECT id, uraian FROM m_beras_berat", "id", "uraian", TRUE);

                $query = "select e.id as merk_id, e.merk, e.kd_jns_beras, e.kd_jns_khusus, h.uraian as 'beras', i.uraian as 'khusus', e.campuran
						FROM t_label_merek e 
                                                LEFT JOIN m_beras h ON h.id = e.kd_jns_beras
                                                LEFT JOIN m_beras_khusus i ON i.id = e.kd_jns_khusus where e.permohonan_id = '" . $id . "'"; //print_r($query);die();
                $array = $this->db->query($query)->result_array();
                if ($array) {
                    $arrdata['act'] = site_url('post/licensing/label_act/third/update');

                    $a = 0;
                    foreach ($array as $value) {
                        $arrdata['label'][] = $value;
                        $query = "SELECT a.kd_kemasan, uraian FROM t_label_kemasan a LEFT JOIN m_beras_berat b ON a.kd_kemasan = b.id  where merk_id = " . $value['merk_id']; //die($query);
                        $kemasan = $this->db->query($query)->result_array();
                        foreach ($kemasan as $row) {
                            $arrdata['label'][$a]['kemasan'] .= $row['uraian'] . ',';
                            $arrdata['label'][$a]['kemval'] .= $row['kd_kemasan'] . ',';
                        }
                        $a++;
                    }
                }
                return $arrdata;
            }
        }
    }

    function set_third($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (($act == "update") || ($act == "save")) {
                if (!$isajax) {
                    return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                    exit();
                }

                $msg = "MSG||NO||Data gagal disimpan";
                $respj = FALSE;
                $arrdet = $this->input->post('dtl');
                if (!$arrdet) {
                    return $msg;
                }
                $dir = $this->input->post('direktorat');
                $doc = $this->input->post('kd_izin');
                $type = $this->input->post('tipe_permohonan');
                $id = $this->input->post('id');
                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                if ($act == 'save') {
                    for ($i = 0; $i < count($arrdet['merek']); $i++) {

                        $arrins['permohonan_id'] = $id;
                        $arrins['merk'] = $arrdet['merek'][$i];
                        $arrins['kd_jns_beras'] = $arrdet['kd_jenis_beras'][$i];
                        $arrins['kd_jns_khusus'] = $arrdet['kd_khusus'][$i];
                        $arrins['created'] = 'GETDATE()';
                        $arrins['campuran'] = $arrdet['kd_campuran'][$i];
                        $arrins['created_user'] = $this->newsession->userdata('username');
                        $this->db->insert('t_label_merek', $arrins);
//                         return "MSG||NO||".$this->db->last_query();
                        $merk_id = $this->db->insert_id();
//                        return "MSG||NO||".$this->db->insert_id();
                        $kd_kemasan = explode(',', substr($arrdet['kd_kemasan'][$i], 1));
                        for ($j = 0; $j < count($kd_kemasan); $j++) {
                            $arrkem['kd_kemasan'] = $kd_kemasan[$j];
                            $arrkem['merk_id'] = $merk_id;
                            $arrkem['created_user'] = $this->newsession->userdata('username');
                            $this->db->insert('t_label_kemasan', $arrkem);
                        }
//                        return "MSG||NO||".$this->db->last_query();
//                        
                        /*  $kd_campuran = explode(',', substr($arrdet['kd_campuran'][$i], 1));
                          for ($k = 0; $k < count($kd_campuran); $k++) {
                          $arrcamp['kd_campuran'] = $kd_campuran[$k];
                          $arrcamp['merk_id'] = $merk_id;
                          $arrcamp['created_user'] = $this->newsession->userdata('username');
                          $this->db->insert('t_label_campuran', $arrcamp);
                          } */
                    }
                } else {
                    if (!$arrdet) {
                        return $msg;
                    }
                    $where = array('permohonan_id' => $id);
                    $this->db->where($where);
                    $this->db->delete('t_label_merek');
                    $sql = "SELECT id FROM t_label_merek WHERE permohonan_id = '" . $id . "'"; //die($query);
                    $merk_id = $this->db->query($sql)->result_array();
//return "MSG||NO||" . $merk_id['id'];
                    foreach ($merk_id as $value) {
                        $where = array('merk_id' => $value['id']);
                        $this->db->where($where);
                        $this->db->delete('t_label_kemasan');
                        $this->db->where($where);
                        $this->db->delete('t_label_campuran');
                    }


                    for ($i = 0; $i < count($arrdet['merek']); $i++) {
                        $arrins['permohonan_id'] = $id;
                        $arrins['merk'] = $arrdet['merek'][$i];
                        $arrins['kd_jns_beras'] = $arrdet['kd_jenis_beras'][$i];
                        $arrins['kd_jns_khusus'] = $arrdet['kd_khusus'][$i];
                        $arrins['created'] = 'GETDATE()';
                        $arrins['campuran'] = $arrdet['kd_campuran'][$i];
                        $arrins['created_user'] = $this->newsession->userdata('username');
                        $this->db->insert('t_label_merek', $arrins);
//                         return "MSG||NO||".$this->db->last_query();
                        $merk_id = $this->db->insert_id();
//                        return "MSG||NO||".$this->db->insert_id();
                        $kd_kemasan = explode(',', $arrdet['kd_kemasan'][$i]);
//                        return "MSG||NO||".var_dump($arrdet['kd_kemasan'][$i]);
                        for ($j = 0; $j < count($kd_kemasan); $j++) {
                            $arrkem['kd_kemasan'] = $kd_kemasan[$j];
                            $arrkem['merk_id'] = $merk_id;
                            $arrkem['created_user'] = $this->newsession->userdata('username');
                            $this->db->insert('t_label_kemasan', $arrkem);
                        }
//                        return "MSG||NO||".$this->db->last_query();
//                        
                    }
                }



                if ($this->db->affected_rows() > 0) {
                    $respj = TRUE;
                    if ($act == "update") {
                        $msg = "MSG||YES||Data Lebel Beras Berhasil diupdate||REFRESH";
                    } else {
                        $msg = "MSG||YES||Data Lebel Beras Berhasil disimpan. \nSilahkan lanjutkan mengisi data persyaratan||" . site_url() . 'licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id;
                    }
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $nama_izin . ' (Data Label Beras)',
                        'url' => '{c}' . site_url() . 'post/licensing/label_act/third/update' . ' {m} models/licensing/label_act {f} set_third($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                }
                return $msg;
            }
        }
    }

    function get_fourth($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            $arrdata['izin_id'] = $doc;
            $arrdata['direktorat'] = $dir;
            $arrdata['type'] = $type;
            $arrdata['permohonan_id'] = $id;
            $arrdata['urithird'] = site_url('licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id,
							 case when c.multi = 1 then '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor else '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor end AS nomor,
							  dbo.dateIndo(b.tgl_dok) as tgl_dok, dbo.dateIndo(b.tgl_exp) as tgl_exp, b.penerbit_dok,
							 '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							 FROM t_upload_syarat a 
							 LEFT join t_upload b on b.id = a.upload_id
							 LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id
							 WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "' and a.detail_id is null";
            $data = $this->main->get_result($query_syarat);
            $arr = array();

            $sql = "select count(*) as khusus from t_label_merek a where a.permohonan_id = '" . $id . "' and  kd_jns_beras = 3";
            $jum_khusus = $this->main->get_uraian($sql, 'khusus');

            if ($jum_khusus >= 1) {
                $query = " SELECT a.id, a.dok_id, concat(e.id,f.id,a.dok_id) as dok_id_temp, concat(b.keterangan,' ',CONVERT(text,e.merk),' ',CONVERT(text,f.merk)) as keterangan , b.kode , a.tipe, c.uraian, a.multi
			FROM m_dok_izin a 
			LEFT JOIN m_dok b ON b.id = a.dok_id
			LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
			LEFT JOIN (select a.id, a.kd_jns_beras, '145' as kode , a.merk  from t_label_merek a where a.permohonan_id = '" . $id . "' ) e on e.kode = a.dok_id
                        LEFT JOIN (select a.id, a.kd_jns_beras, '146' as kode , a.merk  from t_label_merek a where a.permohonan_id = '" . $id . "' and a.kd_jns_beras = 3 ) f 
                        on f.kode = a.dok_id WHERE a.kategori <> '03' and a.izin_id = '" . $doc . "'  " . $addCon . "  order by urutan;";
            } else {
                $query = " SELECT a.id, a.dok_id, concat(e.id,f.id,a.dok_id) as dok_id_temp, concat(b.keterangan,' ',CONVERT(text,e.merk),' ',CONVERT(text,f.merk)) as keterangan , b.kode , a.tipe, c.uraian, a.multi
			FROM m_dok_izin a 
			LEFT JOIN m_dok b ON b.id = a.dok_id
			LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
			LEFT JOIN (select a.id, a.kd_jns_beras, '145' as kode , a.merk  from t_label_merek a where a.permohonan_id = '" . $id . "' ) e on e.kode = a.dok_id
                        LEFT JOIN (select a.id, a.kd_jns_beras, '146' as kode , a.merk  from t_label_merek a where a.permohonan_id = '" . $id . "' and a.kd_jns_beras = 3 ) f 
                        on f.kode = a.dok_id WHERE a.kategori <> '03' and a.izin_id = '" . $doc . "'  " . $addCon . " and a.dok_id <> 146 order by urutan;";
            }

            $data_req = $this->main->get_result($query);
            $temp = $query->result_array();
            $arrdata['req'] = $temp;

            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
//$arr[$keys['dok_id']] = $keys;
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }

                $arrdata['sess'] = $arr;
//print_r($arrdata);die();
                $arrdata['act'] = site_url('post/licensing/sipdis_act/fourth/update');
            } else {
                /*  foreach ($temp as $datadt) {
                  $query_syarat2 = "  SELECT top 1 b.id as 'upload_id', b.tipe_dok,  CASE WHEN a.multi = 1 THEN ' <a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp; ' + b.nomor ELSE '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor END AS nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
                  '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
                  FROM t_upload b left join m_dok_izin a on b.tipe_dok = a.dok_id
                  where a.izin_id = '" . $doc . "' and b.trader_id = '" . $this->newsession->userdata('trader_id') . "'and b.tipe_dok = '" . $datadt['dok_id'] . "'  " . $addCon . " order by tgl_dok desc;";

                  $data2 = $this->main->get_result($query_syarat2);
                  if ($data2) {
                  foreach ($query_syarat2->result_array() as $keys) {
                  if (!isset($arr[$keys['tipe_dok']])) {
                  $arr[$keys['tipe_dok']] = array();
                  }
                  $arr[$keys['tipe_dok']][] = $keys;
                  }
                  }
                  } */
                $arrdata['sess'] = $arr;
                $arrdata['baru'] = true;
                $arrdata['act'] = site_url('post/licensing/siupmb_act/fourth/save');
            }

//print_r($arrdata);die();
            return $arrdata;
        }
    }

    function set_fourth($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $arrsiup = $this->main->post_to_query($this->input->post('SIUPMB'));
            if ($this->newsession->userdata('role') == '01') {
                $sqlSource = "SELECT source FROM t_bapok WHERE id = " . $arrsiup['permohonan_id'];
                $source = $this->main->get_uraian($sqlSource, 'source');
                if ($source != '1') {
                    return "MSG||NO||Mohon maaf, pengajuan online tidak dapat diperbaharui oleh pemroses.";
                    exit();
                }
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $ressiup = FALSE;

            $arrreq = $this->input->post('REQUIREMENTS');
            $arrkeys = array_keys($arrreq);
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');

            if ($act == "save") {
                for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                    $requirements = array('izin_id' => $arrsiup['izin_id'],
                        'permohonan_id' => $arrsiup['permohonan_id'],
                        'created' => 'GETDATE()',
                        'created_user' => $this->newsession->userdata('username'));
                    for ($j = 0; $j < count($arrkeys); $j++) {
                        $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                    }
                    unset($requirements['id']);
                    if ($requirements['upload_id'] != "")
                        $this->db->insert('t_upload_syarat', $requirements);
                }
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idUpload = $this->db->insert_id();
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $nama_izin . ' (Data Persyaratan)',
                        'url' => '{c}' . site_url() . 'post/licensing/sipdis_act/fourth/save' . ' {m} models/licensing/sipdis_act {f} set_fourth($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                    $msg = "MSG||YES||Data Persyaratan berhasil disimpan. Pengajuan Permohonan " . $nama_izin . " selesai.||" . site_url() . 'licensing/view/status';
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            } elseif ($act == "update") {
                $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                $this->db->where('izin_id', $arrsiup['izin_id']);
                $this->db->delete('t_upload_syarat');
                if ($this->db->affected_rows() > 0) {
                    for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                        $requirements = array('izin_id' => $arrsiup['izin_id'],
                            'permohonan_id' => $arrsiup['permohonan_id'],
                            'created' => 'GETDATE()',
                            'created_user' => $this->newsession->userdata('username'));
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                        }
                        unset($requirements['id']);
                        if ($requirements['upload_id'] != "")
                            $this->db->insert('t_upload_syarat', $requirements);
                    }
                    if ($this->db->affected_rows() > 0) {
                        $ressiup = TRUE;
                        $idUpload = $this->db->insert_id();
                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $nama_izin . ' (Update - Data Persyaratan)',
                            'url' => '{c}' . site_url() . 'post/licensing/sipdis_act/fourth/update' . ' {m} models/licensing/sipdis_act {f} set_fourth($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                        $msg = "MSG||YES||Data Persyaratan berhasil diupdate.||REFRESH";
                    }
                    if ($this->db->trans_status() === FALSE || !$ressiup) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }
            return $msg;
        }
    }

    function get_preview($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }

//             $query = "select a.id, e.merk, h.uraian as 'beras', i.uraian as 'khusus', 
//            berat = STUFF((
//                    SELECT ', ' +  jns.uraian
//                    FROM dbo.t_label_kemasan bpk  
//            inner JOIN m_beras_berat jns ON jns.id = bpk.kd_kemasan where bpk.merk_id =  e.id 
//            FOR XML PATH('')), 1, 2, ''),
//            campuran = STUFF((
//                    SELECT ', ' + jns.uraian
//                    FROM dbo.t_label_campuran bpk  
//            inner JOIN m_beras_campuran jns ON jns.id = bpk.kd_campuran where bpk.merk_id =  e.id 
//            FOR XML PATH('')), 1, 2, '')
//						FROM t_label a 
//                LEFT JOIN t_label_merek e ON e.permohonan_id = a.id
//                                                LEFT JOIN m_beras h ON h.id = e.kd_jns_beras
//                                                LEFT JOIN m_beras_khusus i ON i.id = e.kd_jns_khusus where a.id = '".$id."'";//print_r($query);die();
//
//            $arrdata['label'] = $this->db->sqlsrv_query($query)->result_array();
            $query = "select a.id, e.id as merk_id, e.merk, h.uraian as 'beras', i.uraian as 'khusus', e.campuran
						FROM t_label a 
                LEFT JOIN t_label_merek e ON e.permohonan_id = a.id
                                                LEFT JOIN m_beras h ON h.id = e.kd_jns_beras
                                                LEFT JOIN m_beras_khusus i ON i.id = e.kd_jns_khusus where a.id = '" . $id . "'"; //print_r($query);die();
            $array = $this->db->query($query)->result_array();
            $a = 0;
            foreach ($array as $value) {
                if($value['merk_id']){
                $arrdata['label'][] = $value;
                // $query = "SELECT uraian FROM t_label_campuran a LEFT JOIN m_beras_campuran b ON a.kd_campuran = b.id  where merk_id = " . $value['merk_id']; //die($query);
                // $camp = $this->db->query($query)->result_array();
                // foreach ($camp as $row) {
                //     $arrdata['label'][$a]['campuran'] .= $row['uraian'] . ',';
                // }
                
                $query = "SELECT uraian FROM t_label_kemasan a LEFT JOIN m_beras_berat b ON a.kd_kemasan = b.id  where merk_id = " . $value['merk_id']; //die($query);
                $kemasan = $this->db->query($query)->result_array();
                foreach ($kemasan as $row) {
                    $arrdata['label'][$a]['kemasan'] .= $row['uraian'] . ',';
                }
                $a++;
                }
            }

            if ($dir == '03') {
                $query = "SELECT a.id, b.direktorat_id, a.kd_izin, a.no_aju, a.trader_id, a.tipe_permohonan, c.uraian AS permohonan ,
						dbo.npwp(a.npwp) AS npwp, d.uraian AS tipe_perusahaan, a.nm_perusahaan, a.almt_perusahaan, e.uraian as jns_usaha,
						dbo.get_region(2,a.kdprop) as kdprop, dbo.get_region(4,a.kdkab) as kdkab,
						dbo.get_region(7,a.kdkec) as kdkec, dbo.get_region(10,a.kdkel) as kdkel,
						a.nama_pj, z.uraian as jabatan_pj, a.tmpt_lahir_pj, f.uraian AS identitas_pj, a.noidentitas_pj,
						dbo.dateIndo(a.tgl_lahir_pj) as tgl_lahir_pj, a.alamat_pj,
						dbo.get_region(2,a.kdprop_pj) as kdprop_pj, dbo.get_region(4,a.kdkab_pj) as kdkab_pj, 
						dbo.get_region(7,a.kdkec_pj) as kdkec_pj, dbo.get_region(10,a.kdkel_pj) as kdkel_pj,  
						a.telp_pj, a.fax_pj, a.telp, a.kdpos, a.fax,
						a.status, b.nama_izin, b.disposisi, a.no_aju, dbo.dateIndo(a.tgl_aju) as tgl_aju
						FROM t_label a 
						LEFT JOIN m_izin b ON b.id = a.kd_izin 
						LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
						LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
                        LEFT JOIN m_reff e ON e.kode = a.jns_usaha AND e.jenis = 'IMPORTIR_BERAS'
						LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND Z.jenis = 'JABATAN'
                                                LEFT JOIN m_reff f ON f.kode = a.identitas_pj AND f.jenis = 'JENIS_IDENTITAS'
						WHERE a.id = '" . $id . "'"; //print_r($query);die();
                $data = $this->main->get_result($query);

                if ($data) {
                    $tmp = array();
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                        $pemasaran = explode(", ", $row['pemasaran']);
                        foreach ($pemasaran as $pem) {
                            $get_urai = "SELECT kode, uraian FROM m_reff WHERE jenis = 'PEMASARAN' AND kode = '" . $pem . "' ";
                            $temp = $this->db->query($get_urai)->row_array();
                            if ($pem == '01') {
                                $arrdata['pemasaran'][] = array(
                                    'urai' => $temp['uraian'],
                                    'kode' => $pem
                                );
                            } elseif ($pem == '02') {
                                $pem_prop = explode(", ", $row['pemasaran_prop']); //print_r($pem_prop);die();
                                if (count($pem_prop) > 0) {
                                    foreach ($pem_prop as $prop) {
                                        $get_prop = "SELECT id, nama FROM m_prop where id = '" . $prop . "' ";
                                        $temp_prop = $this->db->query($get_prop)->row_array();
                                        $arrtemp[] = $temp_prop['nama'];
                                    }
                                    $arrdata['pemasaran'][] = array(
                                        'urai' => $temp['uraian'],
                                        'detil' => $arrtemp,
                                        'kode' => $pem);
                                }//print_r($arrdata['pem_prop']);die();
                            } elseif ($pem == '03') {
                                $pem_kab = explode(", ", $row['pemasaran_kab']);
                                if (count($pem_kab) > 0) {
                                    foreach ($pem_kab as $kab) {
                                        $get_kab = "SELECT id, nama FROM m_kab where id = '" . $kab . "' ";
                                        $temp_kab = $this->db->query($get_kab)->row_array();
                                        $arrtempk[] = $temp_kab['nama'];
                                    }
                                    $arrdata['pemasaran'][] = array(
                                        'urai' => $temp['uraian'],
                                        'detil' => $arrtempk,
                                        'kode' => $pem);
                                }
                            }
                        }//print_r($arrdata['pemasaran']);die();

                        $get_bapok1 = "SELECT uraian FROM m_reff WHERE jenis = 'JENIS_BAPOK' AND kode = '01' ";
                        $bapok1 = $this->db->query($get_bapok1)->row_array();
                        $arrdata['bapok_01'] = $bapok1['uraian'];
                        $get_01 = "SELECT jns_brg FROM t_bapok_brg WHERE permohonan_id = '" . $id . "' AND jns_bapok = '01' ";
                        $res_01 = $this->db->query($get_01)->result_array();
                        foreach ($res_01 as $res1) {
                            $get_urai1 = "SELECT uraian FROM m_jenis_bapok WHERE id = '" . $res1['jns_brg'] . "' ";
                            $urai1[] = $this->db->query($get_urai1)->row_array();
                        }
                        $arrdata['urai_01'] = $urai1;

                        $get_bapok2 = "SELECT uraian FROM m_reff WHERE jenis = 'JENIS_BAPOK' AND kode = '02' ";
                        $bapok2 = $this->db->query($get_bapok2)->row_array();
                        $arrdata['bapok_02'] = $bapok2['uraian'];
                        $get_02 = "SELECT jns_brg FROM t_bapok_brg WHERE permohonan_id = '" . $id . "' AND jns_bapok = '02' ";
                        $res_02 = $this->db->query($get_02)->result_array();
                        foreach ($res_02 as $res2) {
                            $get_urai2 = "SELECT uraian FROM m_jenis_bapok WHERE id = '" . $res2['jns_brg'] . "' ";
                            $urai2[] = $this->db->query($get_urai2)->row_array();
                        }
                        $arrdata['urai_02'] = $urai2;

                        $get_bapok3 = "SELECT uraian FROM m_reff WHERE jenis = 'JENIS_BAPOK' AND kode = '03' ";
                        $bapok3 = $this->db->query($get_bapok3)->row_array();
                        $arrdata['bapok_03'] = $bapok3['uraian'];
                        $get_03 = "SELECT jns_brg FROM t_bapok_brg WHERE permohonan_id = '" . $id . "' AND jns_bapok = '03' ";
                        $res_03 = $this->db->query($get_03)->result_array();
                        foreach ($res_03 as $res3) {
                            $get_urai3 = "SELECT uraian FROM m_jenis_bapok WHERE id = '" . $res3['jns_brg'] . "' ";
                            $urai3[] = $this->db->query($get_urai3)->row_array();
                        }
                        $arrdata['urai_03'] = $urai3;

// $arrdata['detil'] = array(
// 		'bapok' => $bapok,
// 		'jenis' =>
// 	);
// $get_detil = "SELECT id, jns_bapok, jns_brg FROM t_bapok_brg WHERE permohonan_id = '".$id."' ";
// $detil = $this->db->query($get_detil)->result_array();
// foreach ($detil as $dtl) {
// 	$get_bapok = "SELECT uraian FROM m_reff WHERE jenis = 'JENIS_BAPOK' AND kode = '".$dtl['jns_bapok']."' ";
// 	$bapok = $this->db->query($get_bapok)->row_array();
// 	$bapok = $bapok['uraian'];
// 	$get_jns = "SELECT jns_brg 
//    	FROM t_bapok_brg 
//    	WHERE permohonan_id = '".$id."' AND jns_bapok = '".$dtl['jns_bapok']."' ";
// 	$arrjns = $this->db->query($get_jns)->result_array();
// 	// foreach ($arrjns as $jns) {
// 	// 	$get_urai = "SELECT uraian FROM m_jenis_bapok WHERE jenis_bapok = '".$jns['jns_brg']."' ";
// 	// 	$urai[] = $this->db->query($get_urai)->row_array();
// 	// }
// 	$res_detil[] = array(
// 			'bapok' => $bapok,
// 			// 'jenis' => $urai
// 		);
// }
// print_r($res_detil);die();
                    }
                    $arrdata['pejabat_ttd'] = $this->main->set_combobox("SELECT id, nama FROM m_ttd WHERE status = 1 AND kd_izin = " . $doc, "id", "nama", TRUE);
                    $arrdata['act'] = site_url() . 'post/proccess/label_act/verification';
                    $arrdata['input'] = $this->get_input($dir, $doc, $type, $id, $row['status']);
                    $arrdata['proses'] = $this->main->set_proses($dir, $row['kd_izin'], $row['disposisi'], $this->newsession->userdata('role'), $row['status'], hashids_encrypt($row['id'], _HASHIDS_, 9));
                    $arrdata['jmllog'] = $this->main->get_uraian("SELECT COUNT(*) AS JML FROM t_log_izin WHERE permohonan_id = '" . $row['id'] . "' AND kd_izin = '" . $row['kd_izin'] . "'", "JML");
                    $arrdata['urllog'] = site_url() . 'get/log/izin/' . hashids_encrypt($row['id'], _HASHIDS_, 9) . '/' . hashids_encrypt($row['kd_izin'], _HASHIDS_, 9);
//print_r($arrdata['proses']);die();
                }
//print_r($arrdata);die('de');
            }

            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, dbo.dateIndo(b.tgl_dok) as tgl_dok, dbo.dateIndo(b.tgl_exp) as tgl_exp, b.penerbit_dok,
							'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id 
							WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "'";
            $data = $this->main->get_result($query_syarat);
            $arr = array();
            if ($data) {
                /* foreach($query_syarat->result_array() as $keys){
                  $arr[$keys['dok_id']] = $keys;
                  }
                 */
                foreach ($query_syarat->result_array() as $keys) {
//$arr[$keys['dok_id']] = $keys;
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess_syarat'] = $arr;
            }
            $query = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.izin_id = '" . $doc . "' " . $addCon . " order by a.tipe desc, a.urutan ASC";
            $data_req = $this->main->get_result($query);
            $arrdata['req'] = $query->result_array();
            $arrdata['dir'] = $dir;
            $arrdata['telaah'] = $this->main->get_telaah($doc);
            $arrdata['agrement'] = $this->main->get_agrement($doc);
            return $arrdata;
        }
    }

    function get_input($dir, $doc, $type, $id, $stts) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrstts = array('0102');
            $arrdata = array();
            if (in_array($stts, $arrstts)) {
                $query = "SELECT a.id, a.kd_izin, a.no_aju, CONVERT(VARCHAR(10), a.tgl_aju, 103) AS tgl_aju, a.no_izin, CONVERT(VARCHAR(10), a.tgl_izin, 103) as tgl_izin, CONVERT(VARCHAR(10), a.tgl_izin_exp, 103) AS tgl_izin_exp FROM t_siupmb a WHERE a.id = '" . $id . "' AND a.kd_izin = '" . $doc . "'";
                $ret = $this->main->get_result($query);
                if ($ret) {
                    $this->ineng = $this->session->userdata('site_lang');
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['dir'] = $dir;
                    $arrdata['doc'] = $doc;
                }
                $arrdata['pejabat_ttd'] = $this->main->set_combobox("SELECT id, nama FROM m_ttd WHERE status = 1 AND kd_izin = " . $doc, "id", "nama", TRUE);
                if (!$this->session->userdata('site_lang'))
                    $this->ineng = "id";
                $data = $this->load->view($this->ineng . '/backend/input/' . $dir . '/' . $stts, $arrdata, true);
            } else {
                $data = "";
            }
        }
        return $data;
    }

    function set_onfly($act, $id, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($act == "update") {
                if (!$isajax) {
                    return false;
                    exit();
                }
                $msg = "MSG||NO";
                $respon = FALSE;
                $arrsiup = $this->main->post_to_query($this->input->post('dataon'));
                $id = hashids_decrypt($id, _HASHIDS_, 9);
                $this->db->where('id', $id);
                $this->db->update('t_siupmb', $arrsiup);
                if ($this->db->affected_rows() == 1) {
                    $respon = TRUE;
                    $logu = array('aktifitas' => 'Melakukan update tanggal izin / tanggal masa berlaku',
                        'url' => '{c}' . site_url() . 'get/onfly/onfly_act/update' . ' {m} models/licensing/siupmb_act {f} set_onfly($act,  $id, $isajax)');
                    $this->main->set_activity($logu);
                }
                if ($respon)
                    $msg = "MSG||YES";
                return $msg;
            }
        }
    }

    public function list_minol() {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT a.id as ID, a.gol as Golongan, a.jenis as 'Jenis Minuman' FROM m_jenis_minol a ";
//$SQL = "SELECT kode, uraian FROM m_reff WHERE jenis = 'GOL_MINOL'";
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'GOL_MINOL'", "kode", "uraian");

//print_r($query);die();
            $this->newtable->search(array(array("a.gol", 'Jenis Golongan Minuman Beralkohol', array('ARRAY', $arrstatus)), array("a.jenis", "Jenis Minuman Beralkohol")));
            $table->title("");
            $table->columns(array("a.id", "a.gol", "a.jenis"));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/jns_minol");
            $table->keys(array("ID"));
            $table->hiddens(array("ID"));
            $table->sortby("ASC");
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/add_minol', '0', 'home', 'modal'),
                'Edit' => array('BOOTSTRAPDIALOG', site_url() . 'licensing/dialog_minol/' . $id . '/', '1', 'fa fa-pencil-square-o'),
                'Hapus' => array('POST', site_url() . 'licensing/delete_minol', 'N', 'fa fa-trash-o', 'isngajax')));

            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Laporan Perizinan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    public function add_minol($id, $post) {
        if ($this->newsession->userdata('_LOGGED')) {
// $id = hashids_decrypt($id, _HASHIDS_, 9);
            $arrdata['gol'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'GOL_MINOL'", "kode", "uraian", TRUE);
            if ($id == '') {
                $arrdata['act'] = site_url('post/licensing/add_minol/save');
            } else {
                $sql = "SELECT id, gol, jenis FROM m_jenis_minol WHERE id = '" . $id . "' ";
                $arrdata['sess'] = $this->db->query($sql)->row_array();
                $arrdata['act'] = site_url('post/licensing/add_minol/update/');
            }
            return $arrdata;
        }
    }

    public function save_minol($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
                exit();
            }
            $msg = "MSG||NO||Data Penambahan Gagal disimpan||REFRESH";
            $resminol = FALSE;
            $arrminol = $this->input->post('minol');
            $arrsave['gol'] = $arrminol['gol'];
            $arrsave['jenis'] = $arrminol['jenis'];
            $arrsave['created'] = 'GETDATE()';
            $this->db->trans_begin();
            $this->db->insert('m_jenis_minol', $arrsave);
            if ($this->db->affected_rows() > 0) {
                $resminol = TRUE;
                $msg = "MSG||YES||Data Penambahan Berhasil Disimpan||REFRESH";
                /* Log User dan Log Izin */
                $logu = array('aktifitas' => 'Menambahkan Master Jenis Minuman Beralkohol dengan Golongan : ' . $arrminol['gol'] . 'dan Jenis : ' . $arrminol['jenis'],
                    'url' => '{c}' . site_url() . 'post/licensing/add_minol/save' . ' {m} models/licensing/siupmb_act {f} add_minol($act)');
                $this->main->set_activity($logu);
                /* Akhir Log User dan Log Izin */
            }
            if ($this->db->trans_status() === FALSE || !$resminol) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
            return $msg;
        }
    }

    public function update_minol($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
                exit();
            }
            $msg = "MSG||NO||Data Gagal Diupdate||REFRESH";
            $resminol = FALSE;
            $arrminol = $this->input->post('minol'); //print_r($arrminol);die();
            $arrsave['gol'] = $arrminol['gol'];
            $arrsave['jenis'] = $arrminol['jenis'];
            $arrsave['created'] = 'GETDATE()';
            $this->db->trans_begin();
            $this->db->where('id', $arrminol['id']);
            $this->db->update('m_jenis_minol', $arrsave); //print_r($this->db->last_query());die();
            if ($this->db->affected_rows() > 0) {
                $resminol = TRUE;
                $msg = "MSG||YES||Data Berhasil Diupdate||REFRESH";
                /* Log User dan Log Izin */
                $logu = array('aktifitas' => 'Mengupdate Master Jenis Minuman Beralkohol dengan Golongan : ' . $arrminol['gol'] . 'dan Jenis : ' . $arrminol['jenis'],
                    'url' => '{c}' . site_url() . 'post/licensing/add_minol/update' . ' {m} models/licensing/siupmb_act {f} update_minol($isajax)');
                $this->main->set_activity($logu);
                /* Akhir Log User dan Log Izin */
            }
            if ($this->db->trans_status() === FALSE || !$resminol) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
            return $msg;
        }
    }

    public function del_minol($id) {
        if ($this->newsession->userdata('_LOGGED')) {
// $id = hashids_decrypt($id, _HASHIDS_, 9);print_r($id);die();
            $this->db->trans_begin();
            foreach ($id as $data) {
                $this->db->delete('m_jenis_minol', array('id' => $data));
            }
            if ($this->db->affected_rows() > 0) {
                $logu = array('aktifitas' => 'Mendelete Master Minol',
                    'url' => '{c}' . site_url() . 'licensing/delete_minol' . ' {m} models/licensing/siupmb_act {f} del_minol($id)');
                $this->main->set_activity($logu);
            }
            if ($this->db->trans_status === FALSE) {
                $this->db->trans_rollback();
                $msg = "MSG#Proses Gagal#refresh";
            } else {
                $this->db->trans_commit();
                $msg = "MSG#Proses Berhasil#refresh";
            }
            return $msg;
        }
    }

    function referensi_kbli($target, $callback, $fieldcallback) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT kode as kbli, uraian as desc_kbli, kode AS 'Kode KBLI', uraian AS 'Uraian KBLI' FROM m_kbli";
            $table->title("");
            $table->columns(array("kode", "Kode KBLI", "uraian", "Uraian KBLI"));
            $this->newtable->width(array('Kode KBLI' => 100, 'Uraian' => 300, '&nbsp;' => 5));
            $this->newtable->search(array(array("kode", "Kode KBLI"),
                array("uraian", "Uraian")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_kbli/" . $target . '/' . $callback . '/' . $fieldcallback);
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("kode"));
            $table->hiddens(array("kbli", "desc_kbli"));
            $table->use_ajax(TRUE);
            $table->show_search(TRUE);
            $table->show_chk(FALSE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);
            $table->settrid(TRUE);
            $table->attrid($target);
            if ($callback != "")
                $table->callback(site_url(str_replace(".", '/', $callback)));
            if ($fieldcallback != "")
                $table->fieldcallback($fieldcallback);
            $table->tbtarget("refkbli");
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function list_dok($target, $callback, $fieldcallback, $id, $multiple, $putin, $doc, $permohonan_id) {
        if ($this->newsession->userdata('_LOGGED')) {
//print_r($permohonan_id);die();
            if ($this->newsession->userdata('role') == '01') {
                $sql_trader = "SELECT trader_id FROM dbo.view_permohonan WHERE id = " . $permohonan_id;
                $trader_id = $this->main->get_uraian($sql_trader, "trader_id");
            } else {
                $trader_id = $this->newsession->userdata('trader_id');
            }
            $table = $this->newtable;
            $query = "SELECT id, id AS 'upload_id', nomor AS 'No. Dokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'Tgl. Dokumen', tgl_exp AS 'Tgl. Akhir', '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
			FROM t_upload WHERE tipe_dok = '" . $id . "' AND trader_id = '" . $trader_id . "'";
//print_r($query);die();
            $table->title("");
            $table->columns(array("id", "nomor", "penerbit_dok", "tgl_dok", "tgl_exp", "<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
            $this->newtable->width(array('Nomor Dokumen' => 100, 'Penerbit' => 300, 'Tanggal Awal' => 100, 'Tanggal Akhir' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("nomor", "Nomor Penerbit"),
                array("penerbit_dok", "Penerbit")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_syarat/" . $id . '/' . $target . '/' . $callback . '/' . $fieldcallback . '/' . $id . '/' . $multiple . '/' . $putin . '/' . $doc . '/' . $permohonan_id);
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id", "folder", "nama_file", "upload_id"));
            $table->show_search(TRUE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);

            if ((int) $multiple == 1) {
                $table->show_scroll(TRUE);
                $table->show_chk(TRUE);
                $table->menu(array('Pilih Data' => array('POSTGET', site_url() . 'post/document/get_requirements/' . $doc . '/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#' . $putin)));
            } else {
                $table->show_chk(FALSE);
                if ($callback != "")
                    $table->callback(site_url(str_replace(".", '/', $callback)));
                if ($fieldcallback != "")
                    $table->fieldcallback($fieldcallback);
                $table->settrid(TRUE);
                $table->attrid($target);
            }
            $table->tbtarget("refreq_" . rand());
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function list_spp($target, $callback, $fieldcallback, $id, $multiple, $putin, $doc) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT id, id AS 'upload_id', nomor AS 'No. Dokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'Tgl. Dokumen', tgl_exp AS 'Tgl. Akhir', '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
			FROM t_upload WHERE tipe_dok = '" . $id . "' AND trader_id = '" . $this->newsession->userdata('trader_id') . "'";
            $table->title("");
            $table->columns(array("id", "nomor", "penerbit_dok", "tgl_dok", "tgl_exp", "<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
            $this->newtable->width(array('Nomor Dokumen' => 100, 'Penerbit' => 300, 'Tanggal Awal' => 100, 'Tanggal Akhir' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("nomor", "Nomor Penerbit"),
                array("penerbit_dok", "Penerbit")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_syarat/" . $id . '/' . $target . '/' . $callback . '/' . $fieldcallback . '/');
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id", "folder", "nama_file", "upload_id"));
            $table->show_search(TRUE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);

            if ((int) $multiple == 1) {
                $table->show_chk(TRUE);
                $table->menu(array('Pilih Data' => array('POSTGET', site_url() . 'post/document/get_requirements/' . $doc . '/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#' . $putin)));
            } else {
                $table->show_chk(FALSE);
                if ($callback != "")
                    $table->callback(site_url(str_replace(".", '/', $callback)));
                if ($fieldcallback != "")
                    $table->fieldcallback($fieldcallback);
                $table->settrid(TRUE);
                $table->attrid($target);
            }
            $table->tbtarget("refreq_" . rand());
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function get_requirements($doc) {
        $id = join("','", $this->input->post('tb_chk'));
        $data = "SELECT DISTINCT a.id AS dok_ids, b.id AS id_upload, '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor AS nomor, b.penerbit_dok, b.tgl_dok, b.tgl_exp, '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS files, c.tipe FROM m_dok a LEFT JOIN t_upload b ON a.id = b.tipe_dok LEFT JOIN m_dok_izin c ON c.dok_id = a.id WHERE b.id IN('" . $id . "') AND c.izin_id = '" . $doc . "'";
//print_r($data);die();
        return $this->db->query($data)->result_array();
    }

    function referensi_old_doc($target, $callback, $fieldcallback, $dir, $doc) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT id, id as id_old, no_aju as 'No. Pengajuan', no_izin as 'Nomor Izin', tgl_izin as 'Tanggal Izin', tgl_izin_exp as 'Tanggal Akhir Izin'
					  FROM t_siupmb  
					  WHERE kd_izin = " . $doc . " AND status = '1000'";
            $table->title("");
            $table->columns(array("id", "No. Pengajuan", "Nomor Izin", "Tanggal Izin", "Tanggal Akhir Izin"));
            $this->newtable->width(array('No. Pengajuan' => 100, 'Nomor Izin' => 100, 'Tanggal Izin' => 100, 'Tanggal Akhir Izin' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("no_aju", "No. Pengajuan"),
                array("no_izin", "Nomor Izin")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_old_doc/" . $target . '/' . $callback . '/' . $fieldcallback);
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id", "id_old"));
            $table->use_ajax(TRUE);
            $table->show_search(TRUE);
            $table->show_chk(FALSE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);
            $table->settrid(TRUE);
            $table->attrid($target);
            if ($callback != "")
                $table->callback(site_url(str_replace(".", '/', $callback)));
            if ($fieldcallback != "")
                $table->fieldcallback($fieldcallback);
            $table->tbtarget("refolddoc");
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

}

?>