<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sppgrap_act extends CI_Model {

    var $ineng = "";

    function get_first($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata = array();
            $idprop = $this->newsession->userdata('kdprop');
            $idkab = $this->newsession->userdata('kdkab');
            $idkec = $this->newsession->userdata('kdkec');
            $idkel = $this->newsession->userdata('kdkel'); //print_r($idkel);die();
            $tipe_perusahaan = $this->newsession->userdata('tipe_perusahaan');


            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $idprop . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '" . $idkab . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '" . $idkec . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '" . $idkel . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['direktorat'] = $dir;
            $arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian", TRUE);
            $arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN' AND kode = '" . $tipe_perusahaan . "' ", "kode", "uraian", TRUE);
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = "Surat Persetujuan Perdagangan Gula Rafinasi Antar Pulau (SPPAGKR)";
            $queryNpwp = "select npwp from m_trader_refinasi";
            $sqlNpwp = $this->main->get_result($queryNpwp);
            if ($sqlNpwp) {
                foreach ($queryNpwp->result_array() as $v) {
                    $np[] = $v['npwp'];
                }
            }
            $arrdata['npwp'] = $np;
            if ($id == "") {
                $arrdata['act'] = site_url('post/licensing/sppgrap_act/first/save');
                $arrdata['sess']['tipe_permohonan'] = $type;
                $arrdata['sess']['kd_izin'] = $doc;
            } else {
                $arrdata['act'] = site_url('post/licensing/sppgrap_act/first/update');
                $query = "SELECT id, kd_izin, no_aju, tgl_aju, trader_id, tipe_permohonan, npwp, tipe_perusahaan, nm_perusahaan, almt_perusahaan, 
						kdprop, kdkab, kdkec, kdkel, kdpos, telp, fax, id_pgapt, no_pgapt, tgl_pgapt, tgl_exp_pgapt
						FROM t_sppgrapt WHERE id = " . $id;
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec'] . "' ORDER BY 2", "id", "nama", TRUE);
                }
            }
            return $arrdata;
        }
    }

    function set_first($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $txt = "Surat Persetujuan Perdagangan Gula Rafinasi Antar Pulau (SPPAGKR)";
            $msg = "MSG||NO||Data Permohonan " . $txt . " gagal disimpan";
            $ressiup = FALSE;
            $arrsppgap = $this->main->post_to_query($this->input->post('SPPGAP'));
            $dir = $this->input->post('direktorat');
            $doc = $arrsppgap['kd_izin'];
            $type = $arrsppgap['tipe_permohonan'];
            if ($act == "save") {
                $arrsppgap['no_aju'] = $this->main->set_aju();
                $arrsppgap['fl_pencabutan'] = '0';
                $arrsppgap['status'] = '0000';
                $arrsppgap['tgl_aju'] = 'GETDATE()';
                $arrsppgap['trader_id'] = $this->newsession->userdata('trader_id');
                $arrsppgap['created'] = 'GETDATE()';
                $arrsppgap['created_user'] = $this->newsession->userdata('username');
                $this->db->trans_begin();
                $exec = $this->db->insert('t_sppgrapt', $arrsppgap);
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idredir = $this->db->insert_id();
                    
                    $sqlpj = "update a set 
                                    a.identitas_pj = b.identitas_pj
                                   ,a.noidentitas_pj = b.noidentitas_pj
                                   ,a.nama_pj =  b.na_pj
                                   ,a.jabatan_pj =  b.jabatan_pj
                                   ,a.tmpt_lahir_pj =  b.tmpt_lahir_pj
                                   ,a.tgl_lahir_pj =  b.tgl_lahir_pj
                                   ,a.alamat_pj =  b.alamat_pj
                                   ,a.kdprop_pj = b.kdprop_pj
                                   ,a.kdkab_pj = b.kdkab_pj
                                   ,a.kdkec_pj = b.kdkec_pj
                                   ,a.kdkel_pj = b.kdkel_pj
                                   ,a.telp_pj = b.telp_pj
                             from t_sppgrapt a
                             inner join m_trader b on a.trader_id = b.id
                             where a.id = ".$idredir." ;";
                    $this->db->query($sqlpj);      
                    $msg = "MSG||YES||Data Permohonan " . $txt . " berhasil disimpan. Silahkan lanjutkan mengisi data komoditi||" . site_url() . 'licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $idredir;

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' dengan nomor permohonan : ' . $arrsppgap['no_aju'],
                        'url' => '{c}' . site_url() . 'post/licensing/sppgrap_act/first/save' . ' {m} models/licensing/spprgap_act {f} set_first($act, $ajax)');
                    $this->main->set_activity($logu);
                    $logi = array('kd_izin' => $arrsppgap['kd_izin'],
                        'permohonan_id' => $idredir,
                        'keterangan' => 'Menambahkan daftar permohonan ' . $txt . ' dengan nomor permohonan : ' . $arrsppgap['no_aju'],
                        'catatan' => '',
                        'status' => '0000',
                        'selisih' => 0);
                    $this->main->set_loglicensing($logi);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                    $arrprodusen['tipe_perusahaan'] = $arrsppgap['tipe_perusahaan'];
                    $arrprodusen['nama_produsen'] = $arrsppgap['nm_perusahaan'];
                    $arrprodusen['alamat'] = $arrsppgap['almt_perusahaan'];
                    $arrprodusen['kdprop'] = $arrsppgap['kdprop'];
                    $arrprodusen['kdkab'] = $arrsppgap['kdkab'];
                    $arrprodusen['kdkec'] = $arrsppgap['kdkec'];
                    $arrprodusen['kdkel'] = $arrsppgap['kdkel'];
                    $arrprodusen['permohonan_id'] = $idredir;
                    $this->db->insert('t_sppgrapt_produksi', $arrprodusen);
                }
                return $msg;
            } elseif ($act == "update") {
                $id = $this->input->post('id');
                $this->db->where('id', $id);
                $this->db->update('t_sppgrapt', $arrsppgap);
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $arrprodusen['tipe_perusahaan'] = $arrsppgap['tipe_perusahaan'];
                    $arrprodusen['nama_produsen'] = $arrsppgap['nm_perusahaan'];
                    $arrprodusen['alamat'] = $arrsppgap['almt_perusahaan'];
                    $arrprodusen['kdprop'] = $arrsppgap['kdprop'];
                    $arrprodusen['kdkab'] = $arrsppgap['kdkab'];
                    $arrprodusen['kdkec'] = $arrsppgap['kdkec'];
                    $arrprodusen['kdkel'] = $arrsppgap['kdkel'];
                    $this->db->where('permohonan_id', $id);
                    $this->db->update('t_sppgrapt_produksi', $arrprodusen);
                    $msg = "MSG||YES||Data Permohonan " . $txt . " berhasil diupdate.||REFRESH";
                }
                return $msg;
            }
        }
    }

    function get_second($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata['act'] = site_url('post/licensing/sppgrap_act/second/save');
            $arrdata['direktorat'] = $dir;
            $arrdata['kd_izin'] = $doc;
            $arrdata['type'] = $type;
            $arrdata['id'] = $id;
            $arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_IDENTITAS'", "kode", "uraian", TRUE);
            $arrdata['asal_gula'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '01' AND jenis = 'PRODUKSI'", "kode", "uraian", TRUE);
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            $arrdata['pelabuhan'] = $this->main->set_combobox("SELECT kode, kode +' - '+ uraian as uraian FROM m_peldn ORDER BY 2", "kode", "uraian", TRUE);
            $arrdata['urifirst'] = site_url('licensing/form/first/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            $arrdata['urifourth'] = site_url('licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            $query = "SELECT jns_gula, jml_gula, asal_gula, kdprop_asal_gula, kdprop_tujuan_gula, loading_gula, discharge_gula FROM t_sppgrapt WHERE id = " . $id;
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
                if ($row['jns_gula']) {
                    $arrdata['act'] = site_url('post/licensing/sppgrap_act/second/update');
                }
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = "Surat Persetujuan Perdagangan Gula Rafinasi Antar Pulau (SPPAGKR)";
            return $arrdata;
        }
    }

    function set_second($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (($act == "update") || ($act == "save")) {
                if (!$isajax) {
                    return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                    exit();
                }
                $msg = "MSG||NO||Data Komoditi gagal disimpan";
                $reskomoditi = FALSE;
                $arrkomoditi = $this->main->post_to_query($this->input->post('KOMODITI'));
                $dir = $this->input->post('direktorat');
                $doc = $this->input->post('kd_izin');
                $type = $this->input->post('tipe_permohonan');
                $id = $this->input->post('id');
                $txt = "Surat Persetujuan Perdagangan Gula Rafinasi Antar Pulau (SPPAGKR)";
                //print_r($arrkomoditi);die();
                $this->db->where('id', $id);
                $this->db->update('t_sppgrapt', $arrkomoditi);

                if ($this->db->affected_rows() > 0) {
                    $reskomoditi = TRUE;
                    if ($act == "update") {
                        $msg = "MSG||YES||Data Komoditi berhasil diupdate.||REFRESH";
                    } else {
                        $msg = "MSG||YES||Data Komoditi berhasil disimpan. Silahkan lanjutkan mengisi data produsen dan distributor.||" . site_url() . 'licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id;
                    }
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Data Komoditi)',
                        'url' => '{c}' . site_url() . 'post/licensing/sppgrap_act/second/update' . ' {m} models/licensing/sppgrap_act {f} set_second($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                }
                return $msg;
            }
        }
    }

    function get_third($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $arrdata = array('id' => $id,
                    'direktorat' => $dir,
                    'kd_izin' => $doc,
                    'tipe_permohonan' => $type,
                    'nama_izin' => 'Surat Persetujuan Perdagangan Gula Rafinasi Antar Pulau (SPPAGKR)',
                    'act' => site_url('post/licensing/sppgap_act/fourth/save'),
                    'urifirst' => site_url('licensing/form/first/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urisecond' => site_url('licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urithird' => site_url('licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urifourth' => site_url('licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id));

                $queryPermintaan = "SELECT a.id, a.nomor_po as 'Nomor PO', a.tgl_po as 'Tanggal PO', a.jml_permintaaan as 'Jumlah Permintaan', a.jml_pengiriman as 'Jumlah Pengiriman', 
									a.realiasi as 'Jumlah Realisasi'
									FROM t_sppgrapt_permintaan a
									WHERE a.permohonan_id = " . $id;
                //print_r($queryProdusen);die();
                $arrdata['banyakPermintaan'] = $this->db->query($queryPermintaan)->num_rows(); //$this->db->query($queryProdusen)->num_rows();
                // $queryProdusen = "SELECT id, nama_produsen AS 'Nama Produsen', alamat AS 'Alamat' FROM t_sppgrapt_produksi
                // 			  	WHERE permohonan_id = ".$id;
                // //print_r($queryProdusen);die();
                // $arrdata['banyakProdusen'] = 1;//$this->db->query($queryProdusen)->num_rows();
                $queryDistributor = "SELECT id, nama_produsen AS 'Nama Distributor', alamat AS 'Alamat' FROM t_sppgrapt_distributor
							  	WHERE permohonan_id = " . $id;
                $arrdata['banyakDistributor'] = $this->db->query($queryDistributor)->num_rows();

                $arrdata['lstProdusen'] = $this->get_lst_po($id);
                $arrdata['lstDistributor'] = $this->get_lst_distributor($id);
                return $arrdata;
            }
        }
    }

    function get_lst_produsen($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $table = $this->newtable;
                $queryProdusen = "SELECT id, nama_produsen AS 'Nama Produsen', alamat AS 'Alamat' FROM t_sppgrapt_produksi
							  	WHERE permohonan_id = " . $id;
                $table->title("");
                $table->columns(array("id", "nama_produsen", "alamat"));
                $this->newtable->width(array('Nama Produsen' => 100, 'Alamat' => 200));
                $this->newtable->search(array(array("nama_produsen", "Nama Produsen")));
                $table->cidb($this->db);
                $table->ciuri($this->uri->segment_array());
                $table->action(site_url() . "licensing/get_produsen/" . $id);
                $table->orderby(1);
                $table->sortby("ASC");
                $table->keys(array("id"));
                $table->hiddens(array("id"));
                $table->show_search(FALSE);
                $table->show_chk(FALSE);
                $table->single(TRUE);
                $table->dropdown(TRUE);
                $table->hashids(TRUE);
                $table->postmethod(TRUE);
                $table->clear();
                $table->tbtarget("refProdusen");
                $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/view/produsen/save/' . $id, '0', 'home', 'modal'),
                    'Edit' => array('BOOTSTRAPDIALOG', site_url() . 'licensing/view/produsen/update/' . $id . '/', '1', 'fa fa-edit'),
                    'DELETE' => array('POST', site_url() . 'licensing/del_produsen/produsen/ajax', 'N', 'fa fa-times', 'isngajax')
                ));
                $table->show_chk(TRUE);
                return $table->generate($queryProdusen);
            }
        }
    }

    function get_lst_po($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $table = $this->newtable;
                $queryProdusen = "SELECT a.id, a.nomor_po as 'Nomor PO', a.tgl_po as 'Tanggal PO', a.jml_permintaaan as 'Jumlah Permintaan', a.jml_pengiriman as 'Jumlah Pengiriman', 
									a.realiasi as 'Jumlah Realisasi'
									FROM t_sppgrapt_permintaan a
									WHERE a.permohonan_id = " . $id;
                $table->title("");
                $table->columns(array("id", "nama_produsen", "alamat"));
                $this->newtable->width(array('Nama Produsen' => 100, 'Alamat' => 200));
                $this->newtable->search(array(array("nama_produsen", "Nama Produsen")));
                $table->cidb($this->db);
                $table->ciuri($this->uri->segment_array());
                $table->action(site_url() . "licensing/get_produsen/" . $id);
                $table->orderby(1);
                $table->sortby("ASC");
                $table->keys(array("id"));
                $table->hiddens(array("id"));
                $table->show_search(FALSE);
                $table->show_chk(FALSE);
                $table->single(TRUE);
                $table->dropdown(TRUE);
                $table->hashids(TRUE);
                $table->postmethod(TRUE);
                $table->clear();
                $table->tbtarget("refProdusen");
                //if($this->db->query($queryProdusen)->num_rows() == 0){
                //	$table->menu(array('Tambah' => array('GET', site_url().'licensing/view/po/save/'.$id, '0', 'home', 'modal')));
                //	$table->show_chk(TRUE);
                //}else{
                $table->menu(array('Edit' => array('BOOTSTRAPDIALOG', site_url() . 'licensing/view/po/update/' . $id . '/', '1', 'fa fa-edit'),
                    'DELETE' => array('POST', site_url() . 'licensing/del_produsen/po/ajax', 'N', 'fa fa-times', 'isngajax'),
                    'Tambah' => array('GET', site_url() . 'licensing/view/po/save/' . $id, '0', 'home', 'modal')
                ));
                $table->show_chk(TRUE);
                //}				   
                return $table->generate($queryProdusen);
            }
        }
    }

    function get_lst_distributor($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $table = $this->newtable;
                $queryDistributor = "SELECT id, nama_produsen AS 'Nama Distributor', alamat AS 'Alamat' FROM t_sppgrapt_distributor
							  	WHERE permohonan_id = " . $id;
                $table->title("");
                $table->columns(array("id", "nama_produsen", "alamat"));
                $this->newtable->width(array('Nama Distributor' => 100, 'Alamat' => 200));
                $this->newtable->search(array(array("nama_produsen", "Nama Distributor")));
                $table->cidb($this->db);
                $table->ciuri($this->uri->segment_array());
                $table->action(site_url() . "licensing/get_distributor/" . $id);
                $table->orderby(1);
                $table->sortby("ASC");
                $table->keys(array("id"));
                $table->hiddens(array("id"));
                $table->show_search(TRUE);
                $table->show_chk(TRUE);
                $table->single(TRUE);
                $table->dropdown(TRUE);
                $table->hashids(TRUE);
                $table->postmethod(TRUE);
                $table->clear();
                $table->tbtarget("refDistributor");
                $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/view/distributor/save/' . $id, '0', 'home', 'modal'),
                    'Edit' => array('BOOTSTRAPDIALOG', site_url() . 'licensing/view/distributor/update/' . $id . '/', '1', 'fa fa-edit'),
                    'DELETE' => array('POST', site_url() . 'licensing/del_produsen/distributor/ajax', 'N', 'fa fa-times', 'isngajax')
                ));
                /* $arrdata['lstDistributor'] = $table->generate($queryDistributor); */


                return $table->generate($queryDistributor);
            }
        }
    }

    function get_produsen($id, $dtl) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
                $arrdata['permohonan_id'] = $id;
                $arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN'", "kode", "uraian", TRUE);
                if ($dtl == "") {
                    $arrdata['act'] = site_url('post/licensing/sppgrap_act/produsen/save');
                } else {
                    $dtlx = hashids_decrypt($dtl, _HASHIDS_, 9);
                    $arrdata['act'] = site_url('post/licensing/spprgap_act/produsen/update');
                    $query = "SELECT id, permohonan_id, nama_produsen, tipe_perusahaan, alamat, kdprop, kdkab, kdkec, kdkel
							FROM t_sppgrapt_produksi WHERE id = " . $dtlx . " AND permohonan_id = " . $id;
                    //print_r($query);die();
                    $data = $this->main->get_result($query);
                    if ($data) {
                        foreach ($query->result_array() as $row) {
                            $arrdata['sess'] = $row;
                        }
                        $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                        $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab'] . "' ORDER BY 2", "id", "nama", TRUE);
                        $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec'] . "' ORDER BY 2", "id", "nama", TRUE);
                    }
                }
                return $arrdata;
            }
        }
    }

    function set_produsen($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $txt = "Surat Persetujuan Perdagangan Gula Rafinasi Antar Pulau (SPPAGKR)";
            $msg = "MSG||NO||Data Produsen gagal disimpan";
            $resProd = FALSE;
            $arrProd = $this->main->post_to_query($this->input->post('PRODUSEN'));
            if ($act == "save") {
                $arrProd['permohonan_id'] = hashids_decrypt($this->input->post('permohonan_id'), _HASHIDS_, 9);
                $arrProd['created'] = 'GETDATE()';
                $arrProd['created_user'] = $this->newsession->userdata('username');
                //print_r($arrProd);die();
                $this->db->trans_begin();
                $exec = $this->db->insert('t_sppgrapt_produksi', $arrProd);

                if ($this->db->affected_rows() > 0) {
                    $resProd = TRUE;
                    $msg = "MSG||YES||Data Produsen berhasil disimpan.||REFRESH";

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Data Produsen)',
                        'url' => '{c}' . site_url() . 'post/licensing/sppgrap_act/produsen/save' . ' {m} models/licensing/sppgrap_act {f} set_produsen($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$resProd) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } elseif ($act == "update") {
                //print_r($_POST);die();
                $id = hashids_decrypt($this->input->post('id'), _HASHIDS_, 9);
                $permohonan_id = hashids_decrypt($this->input->post('permohonan_id'), _HASHIDS_, 9);
                $this->db->where('id', $id);
                $this->db->where('permohonan_id', $permohonan_id);
                $this->db->update('t_sppgrapt_produksi', $arrProd);
                if ($this->db->affected_rows() > 0) {
                    $resProd = TRUE;
                    $msg = "MSG||YES||Data Produsen berhasil diperbaharui.||REFRESH";

                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Update Data Produsen)',
                        'url' => '{c}' . site_url() . 'post/licensing/sppgrap_act/produsen/update' . ' {m} models/licensing/sppgap_act {f} set_produsen($act, $isajax)');
                    $this->main->set_activity($logu);
                }
                return $msg;
            }
        }
    }

    function get_po($id, $dtl) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $sql = "SELECT a.tipe_permohonan FROM t_sppgrapt a WHERE id = '" . $id . "' ";
                $tipe_permohonan = $this->db->query($sql)->row_array();
                if ($tipe_permohonan['tipe_permohonan'] == '03') {
                    $a = "SELECT nomor_po, tgl_po FROM t_sppgrapt_permintaan WHERE trader_id = '" . $this->newsession->userdata('trader_id') . "' ";
                    $arrpermohonan = $this->db->query($a)->result_array(); //print_r($arrpermohonan);die();
                    $arrdata['tipe_permohonan'] = $tipe_permohonan['tipe_permohonan'];
                    $arrdata['permintaan'] = $this->main->set_combobox("SELECT a.id, a.nomor_po, a.nomor_po +'<nbsp>'+ convert(varchar(12),a.tgl_po) as PO  from t_sppgrapt_permintaan a group by a.id, a.nomor_po, a.nomor_po +'<nbsp>'+ convert(varchar(12),a.tgl_po)", "id", "PO", TRUE);
                }
                $arrdata['permohonan_id'] = $id;
                if ($dtl == "") {
                    $arrdata['act'] = site_url('post/licensing/sppgrap_act/po/save');
                } else {
                    $dtlx = hashids_decrypt($dtl, _HASHIDS_, 9);
                    $arrdata['act'] = site_url('post/licensing/spprgap_act/po/update');
                    $query = "	SELECT a.nomor_po, a.tgl_po, a.jml_permintaaan, a.jml_pengiriman, a.realiasi
								FROM t_sppgrapt_permintaan a
								WHERE a.id = '" . $dtlx . "' AND a.permohonan_id = '" . $id . "' ";
                    //print_r($query);die();
                    $data = $this->main->get_result($query);
                    if ($data) {
                        foreach ($query->result_array() as $row) {
                            $arrdata['sess'] = $row;
                        }
                    }
                }
                return $arrdata;
            }
        }
    }

    function set_po($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $txt = "Surat Persetujuan Perdagangan Gula Rafinasi Antar Pulau (SPPAGKR)";
            $msg = "MSG||NO||Data Permintaan gagal disimpan";
            $resProd = FALSE;
            $id_permintaan = $this->input->post('id');
            $arrProd = $this->main->post_to_query($this->input->post('PO'));
            if ($arrProd['nomor_po'] == '' && $arrProd['tgl_po'] == '') {
                $s = "SELECT nomor_po, tgl_po FROM t_sppgrapt_permintaan WHERE id = '" . $id_permintaan . "' ";
                $arrpermintaan = $this->db->query($s)->row_array();
                $arrProd['nomor_po'] = $arrpermintaan['nomor_po'];
                $arrProd['tgl_po'] = $arrpermintaan['tgl_po'];
            }
            if ($act == "save") {
                $arrProd['permohonan_id'] = hashids_decrypt($this->input->post('permohonan_id'), _HASHIDS_, 9);
                $arrProd['trader_id'] = $this->newsession->userdata('trader_id');
                //print_r($arrProd);die();
                $this->db->trans_begin();
                $exec = $this->db->insert('t_sppgrapt_permintaan', $arrProd);
                //print_r($this->db->last_query());die();
                if ($this->db->affected_rows() > 0) {
                    $resProd = TRUE;
                    $msg = "MSG||YES||Data Permintaan berhasil disimpan.||REFRESH";

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permintaan (PO) ' . $txt . ' (Data Permintaan (PO))',
                        'url' => '{c}' . site_url() . 'post/licensing/sppgrap_act/po/save' . ' {m} models/licensing/sppgrap_act {f} set_po($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$resProd) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } elseif ($act == "update") {
                //print_r($_POST);die();
                $id = hashids_decrypt($this->input->post('id'), _HASHIDS_, 9);
                $permohonan_id = hashids_decrypt($this->input->post('permohonan_id'), _HASHIDS_, 9);
                $this->db->where('id', $id);
                $this->db->where('permohonan_id', $permohonan_id);
                $this->db->update('t_sppgrapt_permintaan', $arrProd);
                if ($this->db->affected_rows() > 0) {
                    $resProd = TRUE;
                    $msg = "MSG||YES||Data Permintaan berhasil diperbaharui.||REFRESH";

                    $logu = array('aktifitas' => 'Menambahkan daftar permintaan (PO) ' . $txt . ' (Update Data Permintaan (PO))',
                        'url' => '{c}' . site_url() . 'post/licensing/sppgrap_act/po/update' . ' {m} models/licensing/sppgap_act {f} set_po($act, $isajax)');
                    $this->main->set_activity($logu);
                }
                return $msg;
            }
        }
    }

    function get_distributor($id, $dtl) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
                $arrdata['permohonan_id'] = $id;
                $arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN'", "kode", "uraian", TRUE);
                if ($dtl == "") {
                    $arrdata['act'] = site_url('post/licensing/sppgrap_act/distributor/save');
                } else {
                    $dtlx = hashids_decrypt($dtl, _HASHIDS_, 9);
                    $arrdata['act'] = site_url('post/licensing/sppgrap_act/distributor/update');
                    $query = "SELECT id, permohonan_id, nama_produsen, tipe_perusahaan, alamat, kdprop, kdkab, kdkec, kdkel
							FROM t_sppgrapt_distributor WHERE id = " . $dtlx . " AND permohonan_id = " . $id;
                    //print_r($query);die();
                    $data = $this->main->get_result($query);
                    if ($data) {
                        foreach ($query->result_array() as $row) {
                            $arrdata['sess'] = $row;
                        }
                        $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                        $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab'] . "' ORDER BY 2", "id", "nama", TRUE);
                        $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec'] . "' ORDER BY 2", "id", "nama", TRUE);
                    }
                }
                return $arrdata;
            }
        }
    }

    function set_distributor($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $txt = "Surat Persetujuan Perdagangan Gula Rafinasi Antar Pulau (SPPAGKR)";
            $msg = "MSG||NO||Data Distributor gagal disimpan";
            $resDist = FALSE;
            $arrDist = $this->main->post_to_query($this->input->post('DISTRIBUTOR'));
            if ($act == "save") {
                $arrDist['permohonan_id'] = hashids_decrypt($this->input->post('permohonan_id'), _HASHIDS_, 9);
                $arrDist['created'] = 'GETDATE()';
                $arrDist['created_user'] = $this->newsession->userdata('username');
                //print_r($arrDist);die();
                $this->db->trans_begin();
                $exec = $this->db->insert('t_sppgrapt_distributor', $arrDist);
                //print_r($this->db->last_query());die();
                if ($this->db->affected_rows() > 0) {
                    $resDist = TRUE;
                    $msg = "MSG||YES||Data Distributor berhasil disimpan.||REFRESH";

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Data Distributor)',
                        'url' => '{c}' . site_url() . 'post/licensing/sppgrap_act/distributor/save' . ' {m} models/licensing/sppgrap_act {f} set_distributor($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$resDist) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } elseif ($act == "update") {
                //print_r($_POST);die();
                $id = hashids_decrypt($this->input->post('id'), _HASHIDS_, 9);
                $permohonan_id = hashids_decrypt($this->input->post('permohonan_id'), _HASHIDS_, 9);
                $this->db->where('id', $id);
                $this->db->where('permohonan_id', $permohonan_id);
                $this->db->update('t_sppgrapt_distributor', $arrDist);
                if ($this->db->affected_rows() > 0) {
                    $resDist = TRUE;
                    $msg = "MSG||YES||Data Distributor berhasil diperbaharui.||REFRESH";

                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Update Data Distributor)',
                        'url' => '{c}' . site_url() . 'post/licensing/sppgrap_act/distributor/update' . ' {m} models/licensing/sppgrap_act {f} set_distributor($act, $isajax)');
                    $this->main->set_activity($logu);
                }
                return $msg;
            }
        }
    }

    function get_fourth($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            $arrdata['izin_id'] = $doc;
            $arrdata['direktorat'] = $dir;
            $arrdata['type'] = $type;
            $arrdata['permohonan_id'] = $id;
            $arrdata['urithird'] = site_url('licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id,
							 case when c.multi = 1 then '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor else '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor end AS nomor,
							  b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							 '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							 FROM t_upload_syarat a 
							 LEFT join t_upload b on b.id = a.upload_id
							 LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id
							 WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "' and a.detail_id is null";
            /* $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
              '<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
              FROM t_upload_syarat a
              LEFT join t_upload b on b.id = a.upload_id
              WHERE a.izin_id = '".$doc."' and a.permohonan_id = '".$id."' and a.detail_id is null"; */
            $data = $this->main->get_result($query_syarat);
            $arr = array();

            $query = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian, a.multi
                    FROM m_dok_izin a 
                    LEFT JOIN m_dok b ON b.id = a.dok_id
                    LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
                    WHERE a.kategori = '01' and a.izin_id = '" . $doc . "' " . $addCon;
            $data_req = $this->main->get_result($query);
            $temp = $query->result_array();
            $arrdata['req'] = $temp;

            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
                    //$arr[$keys['dok_id']] = $keys;
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess'] = $arr;
                $arrdata['act'] = site_url('post/licensing/sppgrap_act/fourth/update');
            } else {
                foreach ($temp as $datadt) {
                    $query_syarat2 = "  SELECT top 1 b.id as 'upload_id', b.tipe_dok,  CASE WHEN a.multi = 1 THEN ' <a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp; ' + b.nomor ELSE '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor END AS nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
                    '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View' 
                                            FROM t_upload b left join m_dok_izin a on b.tipe_dok = a.dok_id 
                    where a.izin_id = '".$doc."' and b.trader_id = '" . $this->newsession->userdata('trader_id') . "'and b.tipe_dok = '" . $datadt['dok_id'] . "'  " . $addCon . " order by tgl_dok desc;";
                
                    $data2 = $this->main->get_result($query_syarat2);
                    if ($data2) {
                        foreach ($query_syarat2->result_array() as $keys) {
                            if (!isset($arr[$keys['tipe_dok']])) {
                                $arr[$keys['tipe_dok']] = array();
                            }
                            $arr[$keys['tipe_dok']][] = $keys;
                        }
                    }
                }
                $arrdata['sess'] = $arr;
                $arrdata['baru'] = true;
                $arrdata['act'] = site_url('post/licensing/sppgrap_act/fourth/save');
            }

            return $arrdata;
        }
    }

    function set_fourth($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $ressiup = FALSE;
            $arrsiup = $this->main->post_to_query($this->input->post('SIUPMB'));
            $arrreq = $this->input->post('REQUIREMENTS');
            $arrkeys = array_keys($arrreq);
            $txt = "Surat Persetujuan Perdagangan Gula Rafinasi Antar Pulau (SPPAGKR)";
            if ($act == "save") {
                for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                    $requirements = array('izin_id' => $arrsiup['izin_id'],
                        'permohonan_id' => $arrsiup['permohonan_id'],
                        'created' => 'GETDATE()',
                        'created_user' => $this->newsession->userdata('username'));
                    for ($j = 0; $j < count($arrkeys); $j++) {
                        $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                    }
                    unset($requirements['id']);
                    if ($requirements['upload_id'] != "")
                        $this->db->insert('t_upload_syarat', $requirements);
                }
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idUpload = $this->db->insert_id();
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Data Persyaratan)',
                        'url' => '{c}' . site_url() . 'post/licensing/sppgrap_act/fourth/save' . ' {m} models/licensing/sppgrap_act {f} set_fourth($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                    $msg = "MSG||YES||Data Persyaratan berhasil disimpan. Pengajuan Permohonan " . $txt . " selesai.||" . site_url() . 'licensing/view/status';
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            } elseif ($act == "update") {
                $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                $this->db->where('izin_id', $arrsiup['izin_id']);
                $this->db->delete('t_upload_syarat');
                if ($this->db->affected_rows() > 0) {
                    for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                        $requirements = array('izin_id' => $arrsiup['izin_id'],
                            'permohonan_id' => $arrsiup['permohonan_id'],
                            'created' => 'GETDATE()',
                            'created_user' => $this->newsession->userdata('username'));
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                        }
                        unset($requirements['id']);
                        if ($requirements['upload_id'] != "")
                            $this->db->insert('t_upload_syarat', $requirements);
                    }
                    if ($this->db->affected_rows() > 0) {
                        $ressiup = TRUE;
                        $idUpload = $this->db->insert_id();
                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Update - Data Persyaratan)',
                            'url' => '{c}' . site_url() . 'post/licensing/sppgrap_act/fourth/update' . ' {m} models/licensing/spprgap_act {f} set_fourth($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                        $msg = "MSG||YES||Data Persyaratan berhasil diupdate.||REFRESH";
                    }
                    if ($this->db->trans_status() === FALSE || !$ressiup) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }
            return $msg;
        }
    }

    function get_preview($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }

            $query = "SELECT a.id, b.direktorat_id, a.kd_izin, a.no_aju, a.trader_id, a.tipe_permohonan, c.uraian AS permohonan, a.npwp, d.uraian AS tipe_perusahaan, a.nm_perusahaan, 
					a.almt_perusahaan, dbo.get_region(2, a.kdprop) AS prop_perusahaan, dbo.get_region(4, a.kdkab) AS kab_perusahaan, dbo.get_region(7, a.kdkec) AS kec_perusahaan, dbo.get_region(10, a.kdkel) AS kel_perusahaan,
					a.kdpos, a.telp, a.fax, a.id_pgapt, a.no_pgapt, a.tgl_pgapt, a.tgl_exp_pgapt, 
					a.jns_gula, a.jml_gula, f.uraian as discharge_gula, e.uraian as loading_gula, g.uraian as asal_gula, 
					dbo.get_region(2, a.kdprop_asal_gula) as kdprop_asal_gula, dbo.get_region(2, a.kdprop_tujuan_gula) as kdprop_tujuan_gula,
					a.status, b.nama_izin, b.disposisi, a.no_aju, a.tgl_aju
					FROM t_sppgrapt a 
					LEFT JOIN m_izin b ON b.id = a.kd_izin 
					LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
					LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_peldn e ON e.kode = a.loading_gula
					LEFT JOIN m_peldn f ON f.kode = a.discharge_gula
					LEFT JOIN m_reff g ON g.kode = a.asal_gula AND g.jenis = 'PRODUKSI'
					WHERE a.id = " . $id;
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
                $arrdata['act'] = site_url() . 'post/proccess/sppgrap_act/verification';
                $arrdata['input'] = $this->get_input($dir, $doc, $type, $id, $row['status']);
                $arrdata['proses'] = $this->main->set_proses($dir, $row['kd_izin'], $row['disposisi'], $this->newsession->userdata('role'), $row['status'], hashids_encrypt($row['id'], _HASHIDS_, 9));
                //print_r("d".$dir .$row['kd_izin']. $row['disposisi']. $this->newsession->userdata('role'). $row['status']);die();
                $arrdata['jmllog'] = $this->main->get_uraian("SELECT COUNT(*) AS JML FROM t_log_izin WHERE permohonan_id = '" . $row['id'] . "' AND kd_izin = '" . $row['kd_izin'] . "'", "JML");
                $arrdata['urllog'] = site_url() . 'get/log/izin/' . hashids_encrypt($row['id'], _HASHIDS_, 9) . '/' . hashids_encrypt($row['kd_izin'], _HASHIDS_, 9);
                //	if($this->newsession->userdata('role') == '01'){
                // $arrdata['cetak'] = '<button type="submit" class="btn btn-sm btn-warning addon-btn m-b-10" id= "'.rand().'" data-url = "'.site_url().'prints/licensing/'.hashids_encrypt($dir,_HASHIDS_,9).'/'.hashids_encrypt($row['kd_izin'],_HASHIDS_,9).'/'.hashids_encrypt($row['id'],_HASHIDS_,9).'/penjelasan" onclick="blank($(this));return false;"><i class="fa fa-print pull-right"></i>Cetak Penjelasan</button>';
                //}
                // $table = $this->newtable;
                // $queryProdusen = "SELECT id, nama_produsen AS 'Nama Produsen', alamat AS 'Alamat' FROM t_sppgrapt_produksi
                // 			  	WHERE permohonan_id = ".$id;
                // $table->title("");
                // $table->columns(array("id","nama_produsen", "alamat"));
                // $this->newtable->width(array('Nama Produsen' => 100, 'Alamat' => 200));
                // $this->newtable->search(array(array("nama_produsen","Nama Produsen")));
                // $table->cidb($this->db);
                // $table->ciuri($this->uri->segment_array());
                // $table->action(site_url()."licensing/get_produsen/".$id);
                // $table->orderby(1);
                // $table->sortby("ASC");
                // $table->keys(array("id"));
                // $table->hiddens(array("id"));
                // $table->show_search(TRUE);
                // $table->show_chk(FALSE);
                // $table->single(TRUE);
                // $table->dropdown(TRUE);
                // $table->hashids(TRUE);
                // $table->postmethod(TRUE);
                // $table->tbtarget("refProdusen");
                // $arrdata['lstProdusen'] = $table->generate($queryProdusen);
                // $table->clear();

                $table = $this->newtable; 
                $queryPermintaan = "SELECT a.id, a.nomor_po as 'Nomor PO', dbo.dateIndo( a.tgl_po) as 'Tanggal PO', a.jml_permintaaan as 'Jumlah Permintaan', a.jml_pengiriman as 'Jumlah Pengiriman', 
									a.realiasi as 'Jumlah Realisasi'
									FROM t_sppgrapt_permintaan a
									WHERE a.permohonan_id = " . $id;
                $table->title("");
                $table->columns(array("id", "nama_produsen", "alamat"));
                $this->newtable->width(array('Nama Produsen' => 100, 'Alamat' => 200));
                $this->newtable->search(array(array("nama_produsen", "Nama Produsen")));
                $table->cidb($this->db);
                $table->ciuri($this->uri->segment_array());
                $table->action(site_url() . "licensing/get_produsen/" . $id);
                $table->orderby(1);
                $table->sortby("ASC");
                $table->keys(array("id"));
                $table->hiddens(array("id"));
                $table->show_search(TRUE);
                $table->show_chk(FALSE);
                $table->single(TRUE);
                $table->dropdown(TRUE);
                $table->hashids(TRUE);
                $table->postmethod(TRUE);
                $table->tbtarget("refProdusen");
                $arrdata['lstPermintaan'] = $table->generate($queryPermintaan);
                $table->clear();

                $table = $this->newtable;
                $queryDistributor = "SELECT id, nama_produsen AS 'Nama Distributor', alamat AS 'Alamat' FROM t_sppgrapt_distributor
							  	WHERE permohonan_id = " . $id;
                $table->title("");
                $table->columns(array("id", "nama_produsen", "alamat"));
                $this->newtable->width(array('Nama Distributor' => 100, 'Alamat' => 200));
                $this->newtable->search(array(array("nama_produsen", "Nama Distributor")));
                $table->cidb($this->db);
                $table->ciuri($this->uri->segment_array());
                $table->action(site_url() . "licensing/get_distributor/" . $id);
                $table->orderby(1);
                $table->sortby("ASC");
                $table->keys(array("id"));
                $table->hiddens(array("id"));
                $table->show_search(TRUE);
                $table->show_chk(FALSE);
                $table->single(TRUE);
                $table->dropdown(TRUE);
                $table->hashids(TRUE);
                $table->postmethod(TRUE);
                $table->tbtarget("refDistributor");
                $table->clear();
                $arrdata['lstDistributor'] = $table->generate($queryDistributor);
            }



            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id
							LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id  
							WHERE c.kategori = '01' and a.izin_id = " . $doc . " and a.permohonan_id = '" . $id . "'";
            $data = $this->main->get_result($query_syarat);
            $arr = array();
            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess_syarat'] = $arr;
            }
            $queryReq = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.kategori = '01' and a.izin_id = '" . $doc . "' " . $addCon." ORDER BY a.urutan ASC ";
            $data_req = $this->main->get_result($queryReq);
            $arrdata['req'] = $queryReq->result_array();
            $arrdata['telaah'] = $this->main->get_telaah($doc);
            $arrdata['agrement'] = $this->main->get_agrement($doc);
            $arrdata['dir'] = $dir;
            //print_r($arrdata);die();
            return $arrdata;
        }
    }

    function get_input($dir, $doc, $type, $id, $stts) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrstts = array('0102');
            $arrdata = array();
            if (in_array($stts, $arrstts)) {
                $query = "SELECT a.id, a.kd_izin, a.no_aju, CONVERT(VARCHAR(10), a.tgl_aju, 103) AS tgl_aju, a.no_izin, CONVERT(VARCHAR(10), a.tgl_izin, 103) as tgl_izin, CONVERT(VARCHAR(10), a.tgl_izin_exp, 103) AS tgl_izin_exp 
                			FROM t_sppgrapt a WHERE a.id = '" . $id . "' AND a.kd_izin = '" . $doc . "'";
                $ret = $this->main->get_result($query);
                if ($ret) {
                    $this->ineng = $this->session->userdata('site_lang');
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['dir'] = $dir;
                    $arrdata['doc'] = $doc;
                }
                if (!$this->session->userdata('site_lang'))
                    $this->ineng = "id";
                $data = $this->load->view($this->ineng . '/backend/input/' . $dir . '/' . $stts, $arrdata, true);
            } else {
                $data = "";
            }
        }
        return $data;
    }

    function set_onfly($act, $id, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($act == "update") {
                if (!$isajax) {
                    return false;
                    exit();
                }
                $msg = "MSG||NO";
                $respon = FALSE;
                $arrsiup = $this->main->post_to_query($this->input->post('dataon'));
                $id = hashids_decrypt($id, _HASHIDS_, 9);
                $this->db->where('id', $id);
                $this->db->update('t_sppgrapt', $arrsiup);
                if ($this->db->affected_rows() == 1) {
                    $respon = TRUE;
                    $logu = array('aktifitas' => 'Melakukan update tanggal izin / tanggal masa berlaku',
                        'url' => '{c}' . site_url() . 'get/onfly/onfly_act/update' . ' {m} models/licensing/spprgap_act {f} set_onfly($act,  $id, $isajax)');
                    $this->main->set_activity($logu);
                }
                if ($respon)
                    $msg = "MSG||YES";
                return $msg;
            }
        }
    }

    function referensi_spp($target, $callback, $fieldcallback) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT id as idt,  no_izin as 'NO IZIN' , tgl_izin AS 'TANGGAL IZIN' , tgl_izin_exp AS 'TANGGAL EXPIRED' FROM t_pgapt where status = '1000' and trader_id = '" . $this->newsession->userdata('trader_id') . "'";
            $table->title("");
            $table->columns(array("no_izin", "tgl_izin", "tgl_izin_exp"));
            $this->newtable->width(array('NO IZIN' => 200, 'TANGGAL IZIN' => 100, 'TANGGAL EXPIRED' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("no_izin", "NO IZIN"),
                array("tgl_izin", "TANGGAL IZIN")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_spp/" . $target . '/' . $callback . '/' . $fieldcallback);
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("idt"));
            $table->use_ajax(TRUE);
            $table->show_search(TRUE);
            $table->show_chk(FALSE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);
            $table->settrid(TRUE);
            $table->attrid($target);
            if ($callback != "")
                $table->callback(site_url(str_replace(".", '/', $callback)));
            if ($fieldcallback != "")
                $table->fieldcallback($fieldcallback);
            $table->tbtarget("refkbli");
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function list_dok($target, $callback, $fieldcallback, $id, $multiple, $putin, $doc) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT id, id AS 'upload_id', nomor AS 'No. Dokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'Tgl. Dokumen', tgl_exp AS 'Tgl. Akhir', '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
			FROM t_upload WHERE tipe_dok = '" . $id . "' AND trader_id = '" . $this->newsession->userdata('trader_id') . "'";
            $table->title("");
            $table->columns(array("id", "nomor", "penerbit_dok", "tgl_dok", "tgl_exp", "<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
            $this->newtable->width(array('Nomor Dokumen' => 100, 'Penerbit' => 300, 'Tanggal Awal' => 100, 'Tanggal Akhir' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("nomor", "Nomor Penerbit"),
                array("penerbit_dok", "Penerbit")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_syarat/" . $id . '/' . $target . '/' . $callback . '/' . $fieldcallback . '/');
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id", "folder", "nama_file", "upload_id"));
            $table->show_search(TRUE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);

            if ((int) $multiple == 1) {
                $table->show_chk(TRUE);
                $table->menu(array('Pilih Data' => array('POSTGET', site_url() . 'post/document/get_requirements/' . $doc . '/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#' . $putin)));
            } else {
                $table->show_chk(FALSE);
                if ($callback != "")
                    $table->callback(site_url(str_replace(".", '/', $callback)));
                if ($fieldcallback != "")
                    $table->fieldcallback($fieldcallback);
                $table->settrid(TRUE);
                $table->attrid($target);
            }
            $table->tbtarget("refreq_" . rand());
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function get_requirements($doc) {
        $id = join("','", $this->input->post('tb_chk'));
        $data = "SELECT a.id AS dok_ids, b.id AS id_upload, b.nomor, b.penerbit_dok, b.tgl_dok, b.tgl_exp, '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS files, c.tipe FROM m_dok a LEFT JOIN t_upload B ON a.id = b.tipe_dok LEFT JOIN m_dok_izin c ON c.dok_id = a.id WHERE b.id IN('" . $id . "') AND c.izin_id = '" . $doc . "'";
        //print_r($data);die();
        return $this->db->query($data)->result_array();
    }

    function delete_produsen($data, $menu) {
        $this->db->trans_begin();
        $i = 0;
        foreach ($data as $id) {
            $hasid = hashids_decrypt($id, _HASHIDS_, 9); //print_r($hasid);die();
            $this->db->where("id", $hasid);
            if ($menu == "distributor") {
                $tabel = "t_sppgrapt_distributor";
            } elseif ($menu == "po") {
                $tabel = "t_sppgrapt_permintaan";
            } else {
                $tabel = "t_sppgrapt_produksi";
            }
            $this->db->delete($tabel);

            if ($this->db->affected_rows() == 0) {
                $i++;
                break;
            }
        }
        if ($this->db->trans_status() == FALSE || $i > 0) {
            $msg = "MSG#Proses Gagal#refresh";
            $this->db->trans_rollback();
        } else {
            $msg = "MSG#Proses Berhasil#refresh";
            $this->db->trans_commit();
        }
        return $msg;
    }

}

?>