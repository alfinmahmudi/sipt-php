<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Garansi_act extends CI_Model {

    var $ineng = "";

    function get_first($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata = array();
            $idprop = $this->newsession->userdata('kdprop');
            $idkab = $this->newsession->userdata('kdkab');
            $idkec = $this->newsession->userdata('kdkec');
            $idkel = $this->newsession->userdata('kdkel');//print_r($idkel);die();
            $tipe_perusahaan = $this->newsession->userdata('tipe_perusahaan');
            
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '".$idprop."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '".$idkab."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '".$idkec."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '".$idkel."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['jenis_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff where jenis='JENIS_GARANSI'", "kode", "uraian", TRUE);
            $arrdata['negara_perusahaan'] = $this->main->set_combobox("SELECT kode, nama FROM m_negara ORDER BY 2", "kode", "nama", TRUE);
            $arrdata['direktorat'] = $dir;
            $arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian", TRUE);
            $arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN' AND kode = '".$tipe_perusahaan."' ", "kode", "uraian", TRUE);
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            if ($id == "") {
                $arrdata['act'] = site_url('post/licensing/garansi_act/first/save');
                $arrdata['sess']['tipe_permohonan'] = $type;
                $arrdata['sess']['kd_izin'] = $doc;
            } else {
                $arrdata['act'] = site_url('post/licensing/garansi_act/first/update');
                $query = "SELECT id, kd_izin,jensi_garansi,tipe_perusahaan, no_aju, tgl_aju, trader_id, tipe_permohonan, npwp, tipe_perusahaan, nm_perusahaan, almt_perusahaan, 
						kdprop, kdkab, kdkec, kdkel, kdpos, telp, fax
						FROM t_garansi
						WHERE id = " . $id;
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec'] . "' ORDER BY 2", "id", "nama", TRUE);
                }
                //print_r($arrdata['kab']);die();
            }

            return $arrdata;
        }
    }

    function set_first($act, $isajax, $s = null) {
        if ($this->newsession->userdata('_LOGGED')) {     
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $arrgaransi = $this->main->post_to_query($this->input->post('GARANSI'));
            //print_r($arrgaransi);die();
            //print_r($arrstpw);die();
            $dir = $this->input->post('direktorat');
            $doc = $arrgaransi['kd_izin'];
            $type = $arrgaransi['tipe_permohonan'];
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $txt = $nama_izin;
            $msg = "MSG||NO||Data Permohonan " . $txt . " gagal disimpan";
            $ressiup = FALSE;
            if ($act == "save") {
          
                $arrgaransi['no_aju'] = $this->main->set_aju();
                $arrgaransi['fl_pencabutan'] = '0';
                $arrgaransi['status'] = '0000';
                $arrgaransi['tgl_aju'] = 'GETDATE()';
                $arrgaransi['trader_id'] = $this->newsession->userdata('trader_id');
                $arrgaransi['created'] = 'GETDATE()';
                $arrgaransi['created_user'] = $this->newsession->userdata('username');
                $this->db->trans_begin();
                $exec = $this->db->insert('t_garansi', $arrgaransi);
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idredir = $this->db->insert_id();
                    $msg = "MSG||YES||Data Permohonan " . $txt . " berhasil disimpan. Silahkan lanjutkan mengisi data penanggung jawab||" . site_url() . 'licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $idredir . '/' . $s;

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' dengan nomor permohonan : ' . $arrgaransi['no_aju'],
                        'url' => '{c}' . site_url() . 'post/licensing/garansi_act/first/save' . ' {m} models/licensing/garansi_act {f} set_first($act, $ajax)');
                    $this->main->set_activity($logu);
                    $logi = array('kd_izin' => $arrgaransi['kd_izin'],
                        'permohonan_id' => $idredir,
                        'keterangan' => 'Menambahkan daftar permohonan ' . $txt . ' dengan nomor permohonan : ' . $arrgaransi['no_aju'],
                        'catatan' => '',
                        'status' => '0000',
                        'selisih' => 0);
                    $this->main->set_loglicensing($logi);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } elseif ($act == "update") {
                //print_r($arrgaransi);die();
                $id = $this->input->post('id');
                $this->db->where('id', $id);
                $this->db->update('t_garansi', $arrgaransi);
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $msg = "MSG||YES||Data Permohonan " . $txt . " berhasil diupdate.||" . site_url() . 'licensing/form/first/' . $dir . '/' . $doc . '/' . $type . '/' . $id . '/' . $s;
                }
                //echo $msg;die();
                return $msg;
            }
        }
    }

    function get_second($dir, $doc, $type, $id, $s = null) {
        if ($this->newsession->userdata('_LOGGED')) {
            $jenis_identitas = $this->newsession->userdata('tipe_identitas');
            $id_user = $this->newsession->userdata('id');
            $idprop = $this->newsession->userdata('kdprop_pj');
            $idkab = $this->newsession->userdata('kdkab_pj');
            $idkec = $this->newsession->userdata('kdkec_pj');
            $idkel = $this->newsession->userdata('kdkel_pj');
            $jabatan_pj = $this->newsession->userdata('jabatan_pj');
            $arrdata['act'] = site_url('post/licensing/garansi_act/second/save');
            $arrdata['direktorat'] = $dir;
            $arrdata['kd_izin'] = $doc;
            $arrdata['tipe_permohonan'] = $type;
            $arrdata['id'] = $id;
            $arrdata['identitas_pj'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '".$jenis_identitas."' and jenis = 'JENIS_IDENTITAS'","kode","uraian", TRUE);
            $arrdata['jabatan_pj'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JABATAN' AND kode = '".$jabatan_pj."' ","kode","uraian", TRUE);
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '".$idprop."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '".$idkab."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '".$idkec."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '".$idkel."' ORDER BY 2","id", "nama", TRUE);
            $arrdata['urifirst'] = site_url('licensing/form/first/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            $arrdata['urifourth'] = site_url('licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            if ($s != NULL) {
                $npwp = $this->main->get_uraian("select npwp from t_garansi where id='" . $id . "'", "npwp");
                $sqlNPWP = "select TOP(1) identitas_pj, noidentitas_pj, nama_pj,jabatan_pj, 
				tmpt_lahir_pj, tgl_lahir_pj, alamat_pj, kdprop_pj, kdkab_pj, kdkec_pj, kdkel_pj, telp_pj, jensi_garansi,
				fax_pj FROM t_garansi WHERE npwp='" . $npwp . "' AND no_izin IS NOT NULL ORDER BY tgl_izin DESC";
                //print_r($sqlNPWP);die();
                $dataNPWP = $this->main->get_result($sqlNPWP);
                if ($dataNPWP) {
                    foreach ($sqlNPWP->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['act'] = site_url('post/licensing/garansi_act/second/update/ajax/search');


                    //die($npwp);
                }
            } else {
                $query = "SELECT  identitas_pj, noidentitas_pj, nama_pj,jabatan_pj, 
				tmpt_lahir_pj, tgl_lahir_pj, alamat_pj, kdprop_pj, kdkab_pj, kdkec_pj, kdkel_pj, telp_pj, jensi_garansi,
				fax_pj FROM t_garansi WHERE id = " . $id;
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    if ($row['identitas_pj']) {
                        $arrdata['act'] = site_url('post/licensing/garansi_act/second/update');
                    }
                    $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                }
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            return $arrdata;
        }
    }

    function set_second($act, $isajax, $s = null) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (($act == "update") || ($act == "save")) {
                if (!$isajax) {
                    return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                    exit();
                }
                $arrgaransi = $this->main->post_to_query($this->input->post('GARANSI'));
                $dir = $this->input->post('direktorat');
                $doc = $this->input->post('kd_izin');
                $type = $this->input->post('tipe_permohonan');
                $id = $this->input->post('id');
                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $txt = $nama_izin;
                $msg = "MSG||NO||Data Penanggung Jawab gagal disimpan";
                $reskomoditi = FALSE;
                $this->db->where('id', $id);
                $this->db->update('t_garansi', $arrgaransi);
                if ($this->db->affected_rows() > 0) {
                    $reskomoditi = TRUE;
                    if ($act == "update") {
                        $msg = "MSG||YES||Data " . $txt . " berhasil diupdate.||REFRESH";
                    } else {
                        $msg = "MSG||YES||Data " . $txt . " berhasil disimpan. Silahkan lanjutkan mengisi data produk.||" . site_url() . 'licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id;
                    }
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Data Penanggung Jawab)',
                        'url' => '{c}' . site_url() . 'post/licensing/garansi_act/second/update' . ' {m} models/licensing/garansi_act {f} set_second($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                }
                return $msg;
            }
        }
    }

    function get_third($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $arrdata['direktorat'] = $dir;
                $arrdata['kd_izin'] = $doc;
                $arrdata['tipe_permohonan'] = $type;
                $arrdata['id'] = $id;
                $permohonan_id = $this->uri->segment(7);
                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $table = $this->newtable;

                $query = "SELECT a.id AS 'NO',  b.produk as 'Jenis Produk' , a.merk AS 'Merek' , a.tipe AS 'Type Model' FROM t_garansi_produk a
						LEFT JOIN m_produk b on b.id = a.jenis_produk
						where a.permohonan_id ='" . $permohonan_id . "'";
                //	echo $query;die();
                $q = "SELECT jensi_garansi FROM t_garansi WHERE id = '".$id."'";
                $jns_garansi = $this->db->query($q)->row_array();
                if ($jns_garansi['jensi_garansi'] == '02') {
                    $act = site_url() . 'licensing/form/importir/' . $dir . '/' . $doc . '/' . $type . '/' . $id;
                }else{
                    $act = site_url() . 'licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id;    
                }
                
                $table->title("");
                $table->columns(array("a.id", "b.produk", "a.merk", "a.tipe"));
                $this->newtable->width(array('JENIS PRODUK' => 100, 'MERK' => 100, 'TYPE MODEL' => 110, '&nbsp;' => 5));
                $this->newtable->search(array(array("b.produk", "JENIS PRODUK"),
                    array("a.merk", "MERK")));
                $table->cidb($this->db);
                $table->ciuri($this->uri->segment_array());
                $table->action(site_url() . 'licensing/garansi/ /' . $dir . '/' . $doc . '/' . $type . '/' . $id);
                $table->sortby("ASC");
                $table->keys(array('NO'));
                $table->hiddens(array("NO"));
                $table->show_search(FALSE);
                $table->show_chk(TRUE);
                $b = $this->db->query($query)->num_rows();
                $table->single(TRUE);
                $table->dropdown(TRUE);
                $table->use_ajax(TRUE);
                //$table->hashids(TRUE);
                $table->postmethod(TRUE);
                $table->title(TRUE);
                //$table->tbtarget("refkbli");
                //$table->attrid($target);
                if($this->db->query($query)->num_rows() == 0){
                    $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/garansi/' . $permohonan_id, '0', 'home', 'modal'),
                  //      'Upload' => array('GET', site_url(), '0', 'fa fa-cloud-upload'),
                        'Delete' => array('POST', site_url() . 'licensing/del_garansi', 'N', 'fa fa-times', 'isngajax'),
                        'Edit' => array('BOOTSTRAPDIALOG', site_url() . 'licensing/dialog_garansi/', '1', 'fa fa-edit')
                    ));
                }else{
                    $table->menu(array(
                  //      'Upload' => array('GET', site_url(), '0', 'fa fa-cloud-upload'),
                        'Delete' => array('POST', site_url() . 'licensing/del_garansi', 'N', 'fa fa-times', 'isngajax'),
                        'Edit' => array('BOOTSTRAPDIALOG', site_url() . 'licensing/dialog_garansi/', '1', 'fa fa-edit')
                    ));
                }

                //$table->menu(array('Pilih Data' => array('POSTGET', site_url().'post/document/get_requirements/'.$doc.'/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#')));	
                $get_jenis = "SELECT jensi_garansi FROM t_garansi WHERE id = '".$id."'";
                $jenis = $this->db->query($get_jenis)->row_array();
                $table->tbtarget("refkbli");
                $arrdata = array('tabel' => $table->generate($query),
                    'nama_izin' => $nama_izin,
                    'act' => $act,
                    'ts' => $ts,
                    'direktorat' => $dir,
                    'kd_izin' => $doc,
                    'tipe_permohonan' => $type,
                    'id' => $permohonan_id,
                    'jenis' => $jenis['jensi_garansi']
                );
                // print_r($arrdata);die();
                if ($this->input->post("data-post"))
                    return $table->generate($query);
                else
                    return $arrdata;
            }
        }
    }

    function set_third($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (($act == "update") || ($act == "save")) {
                if (!$isajax) {
                    return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                    exit();
                }
                $msg = "MSG||NO||Data gagal disimpan";
                $respj = FALSE;
                $arrbanyak = $this->input->post("GARANSI_1");//print_r($arrbanyak);die();
                $arrproduk = $this->input->post("GARANSI");//print_r($arrproduk);die();
                $merk = implode("<br>", $arrbanyak['merek']);
                $type = implode("<br>", $arrbanyak['tipe']);
                // $tipe = explode(",", $arrproduk['tipe']);
                // $jml_type = count($tipe);
                // $jns_garansi = $this->main->get_uraian("select jensi_garansi as jns_garansi from t_garansi where id='" . $arrproduk['permohonan_id'] . "'", "jns_garansi");
                // $nm_perusahaan = $this->main->get_uraian("select nm_perusahaan from t_garansi where id='" . $arrproduk['permohonan_id'] . "'", "nm_perusahaan");
                // $expl = explode(" ", $nm_perusahaan);
                // $cn = count($expl);
                // $ccode = "";
                // if ($cn < 2) {
                //     $ccode.=strtoupper(substr($nm_perusahaan, 0, 1));
                // } else {
                //     for ($i = 0; $i < $cn; $i++) {
                //         $h = substr($expl[$i], 0, 1);
                //         $ccode.=strtoupper($h);
                //     }
                // }
                // $max = $this->main->get_uraian("select MAX(id) AS 'idx' from m_produk where kode='" . $arrproduk['jenis_produk'] . "'", "idx");
                // $daftar = $this->main->get_uraian("select count(*) AS 'max' from t_garansi where npwp='" . $this->newsession->userdata('npwp') . "'", "max");
                // $blnthn = date('m') . date('y');

                // $um_garansi = array(
                //     "permits" => ($jns_garansi == '01') ? 'P' : 'I',
                //     "pid" => $arrproduk['jenis_produk'],
                //     "cproduct" => $max,
                //     "ccode" => $ccode,
                //     "item" => $jml_type,
                //     "car" => $max . $daftar . $blnthn
                // );
                // $no_izin = $this->main->um_garansi($um_garansi);
                //print_r($no_izin);die();
                //$this->db->where('id',$arrproduk['permohonan_id']);
                //	$this->db->update('t_garansi',array('no_izin'=>$no_izin));
                $txt = "Informasi Pendaftaran Petunjuk Penggunaan (Manual) dan Kartu Garansi (Garansi)/Purna Jual Dalam Bahasa Indonesia Bagi Produk Telematika/Elektronika";
                $arrproduk['created'] = 'GETDATE()';
                // $arrproduk['no_izin'] = $no_izin;
                // $arrproduk['tgl_izin'] = date('Y-m-d');
                $arrproduk['tipe'] = $type;
                $arrproduk['merk'] = $merk;
                $id = $this->input->post('id');
                
                //print_r($arrproduk);die();
                if ($act == "save") {
                    //$arrproduk['no']
                    $this->db->insert('t_garansi_produk', $arrproduk);
                    if ($this->db->affected_rows() > 0) {
                        $respj = TRUE;

                        $msg = "MSG||YES||Data berhasil disimpan.||REFRESH";

                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' ',
                            'url' => '{c}' . site_url() . 'post/licensing/garansi_act/third/save' . ' {m} models/licensing/garansi_act {f} set_third($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                    }
                } else {
                    $id_prod = $arrbanyak['id'][0];
                    $this->db->where('id', $id_prod);
                    $this->db->update('t_garansi_produk', $arrproduk);
                    if ($this->db->affected_rows() > 0) {
                        $respj = TRUE;
                        $msg = "MSG||YES||Data " . $txt . " berhasil diupdate.||REFRESH";
                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Update Data Perusahaan Pemberi Waralaba)',
                            'url' => '{c}' . site_url() . 'post/licensing/garansi_act/third/update' . ' {m} models/licensing/garansi_act {f} set_third($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                    }
                }

                return $msg;
            }
        }
    }

    function get_importir($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $arrdata['direktorat'] = $dir;
                $arrdata['kd_izin'] = $doc;
                $arrdata['tipe_permohonan'] = $type;
                $arrdata['id'] = $id;
                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $table = $this->newtable;

                $query = "SELECT a.id as NO, a.nama_pabrik as 'Nama Perusahaan Asal / Pabrik', a.alamat as 'Alamat', a.telp as Telepon, a.fax as Fax, a.merk as 'Merk dan Tipe' FROM t_garansi_pabrik a WHERE a.permohonan_id ='" . $id . "'";
                //  echo $query;die();
                
                $act = site_url() . 'licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id;    
                $table->title("");
                $table->columns(array("a.id", "b.produk", "a.merk", "a.tipe"));
                $this->newtable->width(array('JENIS PRODUK' => 100, 'MERK' => 100, 'TYPE MODEL' => 110, '&nbsp;' => 5));
                $this->newtable->search(array(array("b.produk", "JENIS PRODUK"),
                    array("a.merk", "MERK")));
                $table->cidb($this->db);
                $table->ciuri($this->uri->segment_array());
                $table->action(site_url() . 'licensing/garansi/ /' . $dir . '/' . $doc . '/' . $type . '/' . $id);
                $table->sortby("ASC");
                $table->keys(array('NO'));
                $table->hiddens(array("NO"));
                $table->show_search(FALSE);
                $table->show_chk(TRUE);
                $b = $this->db->query($query)->num_rows();
                $table->single(TRUE);
                $table->dropdown(TRUE);
                $table->use_ajax(TRUE);
                //$table->hashids(TRUE);
                $table->postmethod(TRUE);
                $table->title(TRUE);
                //$table->tbtarget("refkbli");
                //$table->attrid($target);

                $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/importir/'.$id, '0', 'home', 'modal'),
              //      'Upload' => array('GET', site_url(), '0', 'fa fa-cloud-upload'),
                    'Edit' => array('BOOTSTRAPDIALOG', site_url() . 'licensing/dialog_importir/'.$id.'/', '1', 'fa fa-edit'),
                    'Delete' => array('POST', site_url() . 'licensing/del_importir', 'N', 'fa fa-times', 'isngajax')
                ));

                //$table->menu(array('Pilih Data' => array('POSTGET', site_url().'post/document/get_requirements/'.$doc.'/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#')));   
                $table->tbtarget("refkbli");
                $arrdata = array('tabel' => $table->generate($query),
                    'nama_izin' => $nama_izin,
                    'act' => $act,
                    'ts' => $ts,
                    'direktorat' => $dir,
                    'kd_izin' => $doc,
                    'tipe_permohonan' => $type,
                    'id' => $id,
                );
                // print_r($arrdata);die();
                if ($this->input->post("data-post"))
                    return $table->generate($query);
                else
                    return $arrdata;
            }
        }
    }

    function set_importir($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (($act == "update") || ($act == "save")) {
                if (!$isajax) {
                    return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                    exit();
                }
                $msg = "MSG||NO||Data gagal disimpan";
                $respj = FALSE;
                $arrimpor = $this->main->post_to_query($this->input->post("IMPOR"));
                $id = $this->input->post('id');
                //print_r($arrproduk);die();
                if ($act == "save") {
                    //$arrproduk['no']
                    $this->db->insert('t_garansi_pabrik', $arrimpor);
                    if ($this->db->affected_rows() > 0) {
                        $respj = TRUE;

                        $msg = "MSG||YES||Data berhasil disimpan.||REFRESH";

                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' ',
                            'url' => '{c}' . site_url() . 'post/licensing/garansi_act/importir/save' . ' {m} models/licensing/garansi_act {f} set_importir($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                    }
                } else {
                    $this->db->where('id', $id);
                    $this->db->update('t_garansi_pabrik', $arrimpor);
                    if ($this->db->affected_rows() > 0) {
                        $respj = TRUE;
                        $msg = "MSG||YES||Data " . $txt . " berhasil diupdate.||REFRESH";
                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Update Data Perusahaan Pemberi Waralaba)',
                            'url' => '{c}' . site_url() . 'post/licensing/garansi_act/importir/update' . ' {m} models/licensing/garansi_act {f} set_importir($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                    }
                }

                return $msg;
            }
        }
    }

    function get_fourth($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }
            $arrdata['nama_izin'] = "Informasi Pendaftaran Petunjuk Penggunaan (Manual) dan Kartu Garansi (Garansi)/Purna Jual Dalam Bahasa Indonesia Bagi Produk Telematika/Elektronika";
            $arrdata['izin_id'] = $doc;
            $arrdata['direktorat'] = $dir;
            $arrdata['type'] = $type;
            $arrdata['permohonan_id'] = $id;
            $sql_jenis_izin = $this->db->query("SELECT jensi_garansi FROM t_garansi WHERE id='".$id."'")->row();
            $jenis_izin = $sql_jenis_izin->jensi_garansi;
            //echo $key;die();
           $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id,
							 case when c.multi = 1 then '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor else '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor end AS nomor,
							  b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							 '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							 FROM t_upload_syarat a 
							 LEFT join t_upload b on b.id = a.upload_id
							 LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id
							 WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "' AND c.tipe_izin='".$jenis_izin."' and a.detail_id is null";
            /* $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
              '<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
              FROM t_upload_syarat a
              LEFT join t_upload b on b.id = a.upload_id
              WHERE a.izin_id = '".$doc."' and a.permohonan_id = '".$id."' and a.detail_id is null"; */
            $data = $this->main->get_result($query_syarat);
            $arr = array();

            $query= "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian, a.multi,a.tipe_izin
                    FROM m_dok_izin a 
                    LEFT JOIN m_dok b ON b.id = a.dok_id
                    LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
                    WHERE a.kategori <> '03' and a.izin_id = '".$doc."' ".$addCon." AND a.tipe_izin='".$jenis_izin."' order by 5 desc";

            $data_req = $this->main->get_result($query);
            $temp = $query->result_array();
            $arrdata['req'] = $temp;

            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
                    //$arr[$keys['dok_id']] = $keys;
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess'] = $arr;
                $arrdata['act'] = site_url('post/licensing/garansi_act/fourth/update');
            } else {
                foreach ($temp as $datadt) {
                    $query_syarat2 = "  SELECT top 1 b.id as 'upload_id', b.tipe_dok, b.tipe_dok as  dok_id,  CASE WHEN a.multi = 1 THEN ' <a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp; ' + b.nomor ELSE ' <a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp; ' + b.nomor END AS nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
                    '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View' 
                                            FROM t_upload b left join m_dok_izin a on b.tipe_dok = a.dok_id 
                    where a.izin_id = '" . $doc . "' and b.trader_id = '" . $this->newsession->userdata('trader_id') . "'and b.tipe_dok = '" . $datadt['dok_id'] . "'  " . $addCon . " order by tgl_dok desc;";
                    $cek = $this->db->query($query_syarat2)->num_rows();
                    //print_r($a);die();
                    $data2 = $this->main->get_result($query_syarat2);
                    if ($cek != 0) {
                        foreach ($query_syarat2->result_array() as $keys) {
                            if (!isset($arr[$keys['dok_id']])) {
                                $arr[$keys['dok_id']] = array();
                            }
                            $arr[$keys['dok_id']][] = $keys;
                        }   
                    }
                    $arrdata['sess'] = $arr;
                }
                $arrdata['baru'] = true;
                $arrdata['act'] = site_url('post/licensing/garansi_act/fourth/save');
            }
            $get_jenis = "SELECT jensi_garansi FROM t_garansi WHERE id = '".$id."'";
            $jenis = $this->db->query($get_jenis)->row_array();
            $arrdata['jenis'] = $jenis['jensi_garansi'];
            return $arrdata;
        }
    }

    function set_fourth($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $ressiup = FALSE;
            $arrsiup = $this->main->post_to_query($this->input->post('GARANSI'));
            $arrreq = $this->input->post('REQUIREMENTS');
            $arrkeys = array_keys($arrreq);
            $txt = "Informasi Pendaftaran Petunjuk Penggunaan (Manual) dan Kartu Garansi (Garansi)/Purna Jual Dalam Bahasa Indonesia Bagi Produk Telematika/Elektronika";
            if ($act == "save") {
                for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                    $requirements = array('izin_id' => $arrsiup['izin_id'],
                        'permohonan_id' => $arrsiup['permohonan_id'],
                        'created' => 'GETDATE()',
                        'created_user' => $this->newsession->userdata('username'));
                    for ($j = 0; $j < count($arrkeys); $j++) {
                        $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                    }
                    unset($requirements['id']);
                    if ($requirements['upload_id'] != "")
                        $this->db->insert('t_upload_syarat', $requirements);
                }
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idUpload = $this->db->insert_id();
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Data Persyaratan)',
                        'url' => '{c}' . site_url() . 'post/licensing/garansi_act/fifth/save' . ' {m} models/licensing/garansi_act {f} set_third($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                    $msg = "MSG||YES||" . $txt . " selesai.||" . site_url() . 'licensing/view/status';
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            } elseif ($act == "update") {
                $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                $this->db->where('izin_id', $arrsiup['izin_id']);
                $this->db->delete('t_upload_syarat');
                if ($this->db->affected_rows() > 0) { //die('masuk');
                    for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                        $requirements = array('izin_id' => $arrsiup['izin_id'],
                            'permohonan_id' => $arrsiup['permohonan_id'],
                            'created' => 'GETDATE()',
                            'created_user' => $this->newsession->userdata('username'));
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                        }
                        unset($requirements['id']);
                        if ($requirements['upload_id'] != "")
                            $this->db->insert('t_upload_syarat', $requirements);
                    }
                    if ($this->db->affected_rows() > 0) {
                        $ressiup = TRUE;
                        $idUpload = $this->db->insert_id();
                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Update - Data Persyaratan)',
                            'url' => '{c}' . site_url() . 'post/licensing/garansi_act/fourth/update' . ' {m} models/licensing/garansi_act {f} set_fourth($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                        $msg = "MSG||YES||Data Persyaratan berhasil diupdate.||". site_url() . 'licensing/view/status';
                    }
                    if ($this->db->trans_status() === FALSE || !$ressiup) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }
            return $msg;
        }
    }

    function get_fifth($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }
            $arrdata['nama_izin'] = $nama_izin;
            $arrdata['izin_id'] = $doc;
            $arrdata['direktorat'] = $dir;
            $arrdata['type'] = $type;
            $arrdata['permohonan_id'] = $id;
            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id 
							WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "' and a.detail_id is null";
            $data = $this->main->get_result($query_syarat);
            $arr = array();
            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
                    //$arr[$keys['dok_id']] = $keys;
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess'] = $arr;
                $arrdata['act'] = site_url('post/licensing/stpw_act/fifth/update');
            } else {
                $arrdata['act'] = site_url('post/licensing/stpw_act/fifth/save');
            }
            $query = "SELECT dok_id,tipe from m_dok_izin a
					WHERE a.kategori = '01' and a.izin_id = '" . $doc . "' " . $addCon;
            $data_req = $this->main->get_result($query);
            if ($data_req) {
                $arrdata['req'] = $query->result_array();
            }
            return $arrdata;
        }
    }

    function set_fifth($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $ressiup = FALSE;
            $arrsiup = $this->main->post_to_query($this->input->post('STPW'));
            $arrreq = $this->input->post('REQUIREMENTS');
            $arrkeys = array_keys($arrreq);
            $txt = "Surat Tanda Pendaftaran Waralaba (Pemberi Waralaba Lanjutan)";
            if ($act == "save") {
                for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                    $requirements = array('izin_id' => $arrsiup['izin_id'],
                        'permohonan_id' => $arrsiup['permohonan_id'],
                        'created' => 'GETDATE()',
                        'created_user' => $this->newsession->userdata('username'));
                    for ($j = 0; $j < count($arrkeys); $j++) {
                        $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                    }
                    unset($requirements['id']);
                    if ($requirements['upload_id'] != "")
                        $this->db->insert('t_upload_syarat', $requirements);
                }
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idUpload = $this->db->insert_id();
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Data Persyaratan)',
                        'url' => '{c}' . site_url() . 'post/licensing/stpw_act/fifth/save' . ' {m} models/licensing/stpw_act {f} set_third($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                    $msg = "MSG||YES||Data Persyaratan berhasil disimpan. Pengajuan Permohonan " . $txt . " selesai.||" . site_url() . 'licensing/view/status';
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            } elseif ($act == "update") {
                $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                $this->db->where('izin_id', $arrsiup['izin_id']);
                $this->db->delete('t_upload_syarat');
                if ($this->db->affected_rows() > 0) {
                    for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                        $requirements = array('izin_id' => $arrsiup['izin_id'],
                            'permohonan_id' => $arrsiup['permohonan_id'],
                            'created' => 'GETDATE()',
                            'created_user' => $this->newsession->userdata('username'));
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                        }
                        unset($requirements['id']);
                        if ($requirements['upload_id'] != "")
                            $this->db->insert('t_upload_syarat', $requirements);
                    }
                    if ($this->db->affected_rows() > 0) {
                        $ressiup = TRUE;
                        $idUpload = $this->db->insert_id();
                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan ' . $txt . ' (Update - Data Persyaratan)',
                            'url' => '{c}' . site_url() . 'post/licensing/stpw_act/fifth/update' . ' {m} models/licensing/stpw_act {f} set_third($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                        $msg = "MSG||YES||Data Persyaratan berhasil diupdate.||REFRESH";
                    }
                    if ($this->db->trans_status() === FALSE || !$ressiup) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }
            return $msg;
        }
    }

    function get_preview($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }

            $query = "SELECT a.id, b.direktorat_id, a.kd_izin, a.no_aju, a.trader_id,almt_perusahaan, a.tipe_permohonan, c.uraian AS permohonan, a.npwp, d.uraian AS tipe_perusahaan, a.nm_perusahaan, a.kdprop,a.kdkab,a.kdkec,a.kdkel,a.kdpos,a.fax,a.telp, 
				      dbo.get_region(2, a.kdprop) AS prop_perusahaan,dbo.get_region(4, a.kdkab) AS kab_perusahaan, dbo.get_region(7, a.kdkec) AS kec_perusahaan, dbo.get_region(10, a.kdkel) AS kel_perusahaan,
					c.uraian as warganegara,a.noidentitas_pj,k.uraian AS identitas_pj,a.nama_pj,z.uraian AS jabatan_pj,a.tmpt_lahir_pj,a.tgl_lahir_pj,a.alamat_pj,
					 dbo.get_region(2, a.kdprop_pj) AS prop_pj,dbo.get_region(4, a.kdkab_pj) AS kab_pj, dbo.get_region(7, a.kdkec_pj) AS kec_pj, dbo.get_region(10, a.kdkel_pj) AS kel_pj,a.telp_pj,fax_pj,
					b.disposisi, a.status,j.uraian AS jenis_garansi, a.no_aju, dbo.dateIndo(a.tgl_kirim) as tgl_aju
					 
					FROM t_garansi a 
					LEFT JOIN m_izin b ON b.id = a.kd_izin 
					LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
					LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff k ON k.kode = a.identitas_pj AND k.jenis = 'JENIS_IDENTITAS'
					LEFT JOIN m_reff j ON j.kode = a.jensi_garansi AND j.jenis = 'JENIS_GARANSI'
					LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
					where a.id='".$id."'";
            $data = $this->main->get_result($query);
            //$ary = array();
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }

                $arrdata['act'] = site_url() . 'post/proccess/garansi_act/verification';
                $arrdata['input'] = $this->get_input($dir, $doc, $type, $id, $row['status']);
                $arrdata['proses'] = $this->main->set_proses($dir, $row['kd_izin'], $row['disposisi'], $this->newsession->userdata('role'), $row['status'], hashids_encrypt($row['id'], _HASHIDS_, 9));
                $arrdata['jmllog'] = $this->main->get_uraian("SELECT COUNT(*) AS JML FROM t_log_izin WHERE permohonan_id = '" . $row['id'] . "' AND kd_izin = '" . $row['kd_izin'] . "'", "JML");
                $arrdata['urllog'] = site_url() . 'get/log/izin/' . hashids_encrypt($row['id'], _HASHIDS_, 9) . '/' . hashids_encrypt($row['kd_izin'], _HASHIDS_, 9);
                $arrdata['cetak'] = '<button class="btn btn-sm btn-warning addon-btn m-b-10" id= "' . rand() . '" data-url = "' . site_url() . 'prints/licensing/' . hashids_encrypt($dir, _HASHIDS_, 9) . '/' . hashids_encrypt($row['kd_izin'], _HASHIDS_, 9) . '/' . hashids_encrypt($row['id'], _HASHIDS_, 9) . '/penjelasan" onclick="blank($(this));return false;"><i class="fa fa-print pull-right"></i>Cetak Penjelasan</button>';
                
                $queryGaransi = "SELECT  b.produk, a.merk, a.tipe FROM t_garansi_produk a
							  	LEFT JOIN m_produk b on b.id = a.jenis_produk WHERE permohonan_id = " . $id;
                $produk = $this->db->query($queryGaransi)->row_array();
                $arrdata['list_produk'] = $produk;

                $queryImportir = "SELECT a.id, a.nama_pabrik, a.alamat, a.telp, a.fax FROM t_garansi_pabrik a WHERE a.permohonan_id = " . $id;
                $importir = $this->db->query($queryImportir)->result_array();
                $arrdata['list_impor'] = $importir;
 
                // $table->title("");
                // $table->columns(array("jenis_produk", "merk", "tipe"));
                // $this->newtable->width(array('Jenis Produk' => 250, 'Merk' => 300, 'Type' => 10));
                // $this->newtable->search(array(array("jenis_produk", "Jenis Produk")));
                // $table->cidb($this->db);
                // $table->ciuri($this->uri->segment_array());
                // $table->action(site_url() . "licensing/get_garansi/" . $id);
                // $table->orderby(1);
                // $table->sortby("ASC");
                // $table->keys(array("id"));
                // $table->hiddens(array("id"));
                // $table->show_search(TRUE);
                // $table->show_chk(FALSE);
                // $table->single(TRUE);
                // $table->dropdown(TRUE);
                // $table->hashids(TRUE);
                // $table->postmethod(TRUE);
                // $table->tbtarget("refProdusen");
                // $arrdata['lstGaransi'] = $table->generate($queryGaransi);
                // $table->clear();

                // $table = $this->newtable;
                // $queryImportir = "SELECT a.id, a.nama_pabrik as 'Nama Perusahaan Asal / Pabrik', a.alamat as 'Alamat', a.telp as Telepon, a.fax as Fax FROM t_garansi_pabrik a WHERE a.permohonan_id = " . $id;
                // $table->title("");
                // $table->columns(array("nama_pabrik", "alamat", "telp", "fax"));
                // $this->newtable->width(array('Nama Perusahaan Asal / Pabrik' => 250, 'Alamat' => 300, 'Telepon' => 10, 'Fax' => 10));
                // $this->newtable->search(array(array("nama_pabrik", "Nama Perusahaan Asal / Pabrik")));
                // $table->cidb($this->db);
                // $table->ciuri($this->uri->segment_array());
                // $table->action(site_url() . "licensing/get_importir/" . $id);
                // $table->orderby(1);
                // $table->sortby("ASC");
                // $table->keys(array("id"));
                // $table->hiddens(array("id"));
                // $table->show_search(TRUE);
                // $table->show_chk(FALSE);
                // $table->single(TRUE);
                // $table->dropdown(TRUE);
                // $table->hashids(TRUE);
                // $table->postmethod(TRUE);
                // $table->tbtarget("refProdusen");
                // $arrdata['lstImportir'] = $table->generate($queryImportir);
                // $table->clear();
            }
            $sJenis_izin = $this->db->query("SELECT jensi_garansi from t_garansi WHERE id='".$id."'")->row();
            $oJenis_garansi = $sJenis_izin->jensi_garansi;
            // /die($oJenis_garansi);
            $arrdata['nama_izin'] = "Informasi Pendaftaran Petunjuk Penggunaan (Manual) dan Kartu Garansi (Garansi)/Purna Jual Dalam Bahasa Indonesia Bagi Produk Telematika/Elektronika";
            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id
							LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id  
							WHERE a.izin_id = " . $doc . " and a.permohonan_id = '" . $id . "' AND c.tipe_izin='".$oJenis_garansi."' ORDER BY c.tipe desc,  c.urutan";
                            //print_r($query_syarat);die();
            $data = $this->main->get_result($query_syarat);
            $arr = array();

            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess_syarat'] = $arr;
            }
            
            $queryReq = "SELECT a.id, a.dok_id,a.urutan, b.keterangan, b.kode , a.tipe, c.uraian
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.izin_id = '" . $doc . "' " . $addCon." AND a.tipe_izin='".$oJenis_garansi."' ORDER BY a.tipe desc, a.urutan ASC";
                    //echo $queryReq;die();
            $data_req = $this->main->get_result($queryReq);
            if ($data_req) {
                $arrdata['req'] = $queryReq->result_array();
                //print_r($arrdata['req']);die();
            }
            $arrdata['dir'] = $dir;
            $arrdata['telaah'] = $this->main->get_telaah($doc);
            $arrdata['agrement'] = $this->main->get_agrement($doc);

            //print_r($arrdata);die();

            return $arrdata;
        }
    }

    function get_input($dir, $doc, $type, $id, $stts) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrstts = array('0102');
            $arrdata = array();
            if (in_array($stts, $arrstts)) {
                $query = "SELECT a.id, a.kd_izin, a.no_aju, CONVERT(VARCHAR(10), a.tgl_aju, 103) AS tgl_aju, a.no_izin, CONVERT(VARCHAR(10), a.tgl_izin, 103) as tgl_izin, CONVERT(VARCHAR(10), a.tgl_izin_exp, 103) AS tgl_izin_exp 
                			FROM t_stpw a WHERE a.id = '" . $id . "' AND a.kd_izin = '" . $doc . "'";
                $ret = $this->main->get_result($query);
                if ($ret) {
                    $this->ineng = $this->session->userdata('site_lang');
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['dir'] = $dir;
                    $arrdata['doc'] = $doc;
                }
                if (!$this->session->userdata('site_lang'))
                    $this->ineng = "id";
                $data = $this->load->view($this->ineng . '/backend/input/' . $dir . '/' . $stts, $arrdata, true);
            } else {
                $data = "";
            }
        }
        return $data;
    }

    function set_onfly($act, $id, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($act == "update") {
                if (!$isajax) {
                    return false;
                    exit();
                }
                $msg = "MSG||NO";
                $respon = FALSE;
                $arrsiup = $this->main->post_to_query($this->input->post('dataon'));
                $id = hashids_decrypt($id, _HASHIDS_, 9);
                $this->db->where('id', $id);
                $this->db->update('t_stpw', $arrsiup);
                if ($this->db->affected_rows() == 1) {
                    $respon = TRUE;
                    $logu = array('aktifitas' => 'Melakukan update tanggal izin / tanggal masa berlaku',
                        'url' => '{c}' . site_url() . 'get/onfly/onfly_act/update' . ' {m} models/licensing/sppgap_act {f} set_onfly($act,  $id, $isajax)');
                    $this->main->set_activity($logu);
                }
                if ($respon)
                    $msg = "MSG||YES";
                return $msg;
            }
        }
    }

    function referensi_spp($target, $callback, $fieldcallback) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT id as idt,  no_izin as 'NO IZIN' , tgl_izin AS 'TANGGAL IZIN' , tgl_izin_exp AS 'TANGGAL EXPIRED' FROM t_pgapt where status = '1000' and trader_id = '" . $this->newsession->userdata('trader_id') . "'";
            $table->title("");
            $table->columns(array("no_izin", "tgl_izin", "tgl_izin_exp"));
            $this->newtable->width(array('NO IZIN' => 200, 'TANGGAL IZIN' => 100, 'TANGGAL EXPIRED' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("no_izin", "NO IZIN"),
                array("tgl_izin", "TANGGAL IZIN")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_spp/" . $target . '/' . $callback . '/' . $fieldcallback);
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("idt"));
            $table->use_ajax(TRUE);
            $table->show_search(TRUE);
            $table->show_chk(FALSE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);
            $table->settrid(TRUE);
            $table->attrid($target);
            if ($callback != "")
                $table->callback(site_url(str_replace(".", '/', $callback)));
            if ($fieldcallback != "")
                $table->fieldcallback($fieldcallback);
            $table->tbtarget("refkbli");
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function list_dok($target, $callback, $fieldcallback, $id, $multiple, $putin, $doc) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT id, id AS 'upload_id', nomor AS 'No. Dokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'Tgl. Dokumen', tgl_exp AS 'Tgl. Akhir', '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
			FROM t_upload WHERE tipe_dok = '" . $id . "' AND trader_id = '" . $this->newsession->userdata('trader_id') . "'";
            $table->title("");
            $table->columns(array("id", "nomor", "penerbit_dok", "tgl_dok", "tgl_exp", "<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
            $this->newtable->width(array('Nomor Dokumen' => 100, 'Penerbit' => 300, 'Tanggal Awal' => 100, 'Tanggal Akhir' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("nomor", "Nomor Penerbit"),
                array("penerbit_dok", "Penerbit")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_syarat/" . $id . '/' . $target . '/' . $callback . '/' . $fieldcallback . '/');
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id", "folder", "nama_file", "upload_id"));
            $table->show_search(TRUE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);

            if ((int) $multiple == 1) {
                $table->show_chk(TRUE);
                $table->menu(array('Pilih Data' => array('POSTGET', site_url() . 'post/document/get_requirements/' . $doc . '/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#' . $putin)));
            } else {
                $table->show_chk(FALSE);
                if ($callback != "")
                    $table->callback(site_url(str_replace(".", '/', $callback)));
                if ($fieldcallback != "")
                    $table->fieldcallback($fieldcallback);
                $table->settrid(TRUE);
                $table->attrid($target);
            }
            $table->tbtarget("refreq_" . rand());
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function get_requirements($doc) {
        $id = join("','", $this->input->post('tb_chk'));
        $data = "SELECT a.id AS dok_ids, b.id AS id_upload, b.nomor, b.penerbit_dok, b.tgl_dok, b.tgl_exp, '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS files, c.tipe FROM m_dok a LEFT JOIN t_upload B ON a.id = b.tipe_dok LEFT JOIN m_dok_izin c ON c.dok_id = a.id WHERE b.id IN('" . $id . "') AND c.izin_id = '" . $doc . "'";
        //print_r($data);die();
        return $this->db->query($data)->result_array();
    }

    function get_garansi($id, $post) {
        if ($this->newsession->userdata('_LOGGED')) {
            $sql = "SELECT a.id,a.jenis_produk,a.merk,a.tipe from t_garansi_produk a LEFT JOIN m_produk b on a.jenis_produk = b.id where a.id='" . $id . "'";
            //echo $sql;die();
            $data = $this->main->get_result($sql);
            $arrdata['act'] = site_url() . "post/licensing/garansi_act/third/save";
            if ($data) {
                foreach ($sql->result_array() as $row) {
                    $merk = explode("<br>", $row['merk']);
                    $tipe = explode("<br>", $row['tipe']);
                    $arrdata['merk'] = $merk;//print_r($arrdata['merk']);die();
                    $arrdata['tipe'] = $tipe;//print_r($banyak);die();
                    $arrdata['sess'] = $row;
                }
                $arrdata['act'] = site_url() . "post/licensing/garansi_act/third/update";
            }
            $arrdata['permohonan_id'] = $id;
            $arrdata['jenis_produk'] = $this->main->set_combobox("SELECT id, produk FROM m_produk", "id", "produk", TRUE);
            return $arrdata;
        }
    }

    function get_importir_1($permohonan_id, $id_pabrik) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata['permohonan_id'] = $permohonan_id;
            $sql = "SELECT id, merk, tipe FROM t_garansi_produk WHERE permohonan_id = '".$permohonan_id."' ";
            $arrprod = $this->db->query($sql)->row_array();
            $merk = explode("<br>", $arrprod['merk']);
            $tipe = explode("<br>", $arrprod['tipe']);
            for ($i=0; $i < count($merk) ; $i++) {
                $mix[] = $merk[$i].' - '.$tipe[$i];
            }
            $arrdata['id_mix'] = $arrprod['id'];
            $arrdata['mix'] = $mix;
            //print_r($mix);die();
            if ($id_pabrik == '') {
                $arrdata['act'] = site_url() . "post/licensing/garansi_act/importir/save";
            }else{
                $sql = "SELECT a.id, a.nama_pabrik, a.alamat, a.telp, a.fax, a.merk FROM t_garansi_pabrik a WHERE a.id ='" . $id_pabrik . "' AND a.permohonan_id = '".$permohonan_id."' ";
                $data = $this->main->get_result($sql);
                if ($data) {
                    foreach ($sql->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['act'] = site_url() . "post/licensing/garansi_act/importir/update";
                }
            }
            return $arrdata;
        }
    }

    function delete_garansi($id) {
        $this->db->trans_begin();
        foreach ($id as $data) {
            $this->db->delete('t_garansi_produk', array('id' => $data));
        }
        if ($this->db->trans_status === FALSE) {
            $this->db->trans_rollback();
            $msg = "MSG#Proses Gagal#refresh";
        } else {
            $this->db->trans_commit();
            $msg = "MSG#Proses Berhasil#refresh";
        }
        return $msg;
    }

    function delete_importir($id) {
        $this->db->trans_begin();
        foreach ($id as $data) {
            $this->db->delete('t_garansi_pabrik', array('id' => $data));
        }
        if ($this->db->trans_status === FALSE) {
            $this->db->trans_rollback();
            $msg = "MSG#Proses Gagal#refresh";
        } else {
            $this->db->trans_commit();
            $msg = "MSG#Proses Berhasil#refresh";
        }
        return $msg;
    }

    function get_lst_garansi($id) {
        $table = $this->newtable;
        $queryGaransi = "SELECT id, jenis_produk AS 'Jenis Produk', merk AS 'Merk', tipe AS 'Type' FROM t_garansi_produk
					  	WHERE permohonan_id = " . $id;
        $table->title("");
        $table->columns(array("jenis_produk", "merk", "tipe"));
        $this->newtable->width(array('Jenis Produk' => 250, 'Merk' => 300, 'Type' => 10));
        $this->newtable->search(array(array("jenis_produk", "Jenis Produk")));
        $table->cidb($this->db);
        $table->ciuri($this->uri->segment_array());
        $table->action(site_url() . "licensing/get_garansi/" . $id);
        $table->orderby(1);
        $table->sortby("ASC");
        $table->keys(array("id"));
        $table->hiddens(array("id"));
        $table->show_search(TRUE);
        $table->show_chk(FALSE);
        $table->single(TRUE);
        $table->dropdown(TRUE);
        $table->hashids(TRUE);
        $table->postmethod(TRUE);
        $table->tbtarget("refProdusen");
        return $table->generate($queryGaransi);
    }

    function set_produk($dir, $doc, $type, $id) {
        $npwp = $this->main->get_uraian("select npwp from t_garansi where id='" . $id . "'", "npwp");
        $r = $this->db->query("select a.jenis_produk, a.merk, a.tipe from t_garansi_produk a left join t_garansi b on b.id = a.permohonan_id where b.npwp='" . $npwp . "'")->result_array();
        $arr = array();
        foreach ($r as $d) {
            $this->db->insert("t_garansi_produk", array('permohonan_id' => $id));
            if ($this->db->affected_rows() > 0) {
                $id_produk = $this->db->insert_id();
                $this->db->where('id', $id_produk);
                $this->db->update('t_garansi_produk', array('created' => 'GETDATE()'));
                $this->db->update('t_garansi_produk', $d);
            }
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
                return site_url() . 'licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id;
            }
        }
    }

}

?>