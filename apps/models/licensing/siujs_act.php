<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Siujs_act extends CI_Model {

    var $ineng = "";

    function get_first($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata = array();
            $idprop = $this->newsession->userdata('kdprop');
            $idkab = $this->newsession->userdata('kdkab');
            $idkec = $this->newsession->userdata('kdkec');
            $idkel = $this->newsession->userdata('kdkel'); //print_r($idkel);die();
            $tipe_perusahaan = $this->newsession->userdata('tipe_perusahaan');


            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $idprop . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '" . $idkab . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '" . $idkec . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '" . $idkel . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['direktorat'] = $dir;
            $arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian", TRUE);
            $arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '" . $tipe_perusahaan . "' and jenis = 'TIPE_PERUSAHAAN'", "kode", "uraian", TRUE);
            $arrdata['lokasi'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'LOKASI_PERUSAHAAN'", "kode", "uraian", TRUE);
            $arrdata['status_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS_PERUSAHAAN'", "kode", "uraian", TRUE);
            $arrdata['jenis_siup'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_SIUP'", "kode", "uraian", TRUE);
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;

            if ($id == "") {
                $arrdata['act'] = site_url('post/licensing/siujs_act/first/save');
                //$type = hashids_decrypt($type,_HASHIDS_,6);
                $arrdata['sess']['tipe_permohonan'] = $type;
                $arrdata['sess']['kd_izin'] = $doc;
            } else {
                $arrdata['act'] = site_url('post/licensing/siujs_act/first/update');
                $query = "SELECT id, kd_izin, no_aju, tgl_aju, trader_id, tipe_permohonan, npwp, tipe_perusahaan, nm_perusahaan, almt_perusahaan, 
						kdprop, kdkab, kdkec, kdkel, kdpos, telp, fax,id_permohonan_lama, no_izin_lama, tgl_izin_lama, tgl_izin_exp_lama
						FROM t_siujs WHERE id = '" . $id . "'";
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec'] . "' ORDER BY 2", "id", "nama", TRUE);
                }
            }
            return $arrdata;
        }
    }

    function set_first($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data Permohonan Surat Izin Usaha Jasa Survey gagal disimpan";
            $ressiup = FALSE;
            $arrsiup = $this->main->post_to_query($this->input->post('siupmb'));
            $dir = $this->input->post('direktorat');
            $doc = $arrsiup['kd_izin'];
            $type = $arrsiup['tipe_permohonan'];
            $id_old = $arrsiup['id_permohonan_lama'];

            if ($act == "save") {
                $arrsiup['no_aju'] = $this->main->set_aju();
                $arrsiup['fl_pencabutan'] = '0';
                $arrsiup['status'] = '0000';
                $arrsiup['tgl_aju'] = 'GETDATE()';
                $arrsiup['trader_id'] = $this->newsession->userdata('trader_id');
                $arrsiup['created'] = 'GETDATE()';
                $arrsiup['created_user'] = $this->newsession->userdata('username');
                if (($type != '01') && ($arrsiup['id_permohonan_lama'] != "")) {
                    if ($type == '03')
                        $arrsiup['fl_pembaharuan'] = "1";
                    $query_old_doc = "SELECT pegawai_tetap, pegawai_honor, pegawai_asing, pegawai_lokal, relasi, kegiatan_usaha,
									 identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, 
									 alamat_pj, kdprop_pj, kdkab_pj, kdkel_pj, kdkec_pj,telp_pj, fax_pj
									 FROM t_siujs WHERE id = " . $arrsiup['id_permohonan_lama'] . " AND kd_izin = " . $arrsiup['kd_izin'];
                    //print_r($query_old_doc);die();
                    $data_old = $this->main->get_result($query_old_doc);
                    if ($data_old) {
                        foreach ($query_old_doc->result_array() as $keys) {
                            $jml = count($keys);
                            $array_keys = array_keys($keys);
                            $array_values = array_values($keys);
                            for ($i = 0; $i < $jml; $i++) {
                                $arrsiup[$array_keys[$i]] = $array_values[$i];
                            }
                        }
                    }
                }
                //print_r($arrsiup);die();
                $this->db->trans_begin();
                $exec = $this->db->insert('t_siujs', $arrsiup);
                if ($this->db->affected_rows() > 0) {
                    $idredir = $this->db->insert_id();
                    if (($type != '01') && ($arrsiup['id_permohonan_lama'] != "")) {
                        $res_bid = $this->insert_bidang($idredir, $id_old);
                        $res_cab = $this->insert_cabang($idredir, $id_old);
                        $ressiup = TRUE;
                    } else {
                        $ressiup = TRUE;
                    }
                    $msg = "MSG||YES||Data Permohonan Surat Izin Usaha Jasa Survey berhasil disimpan. Silahkan lanjutkan mengisi data penanggung jawab.||" . site_url() . 'licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $idredir;

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan Surat Izin Usaha Jasa Survey dengan nomor permohonan : ' . $arrsiup['no_aju'],
                        'url' => '{c}' . site_url() . 'post/licensing/siujs_act/first/save' . ' {m} models/licensing/siujs_act {f} set_first($act, $ajax)');
                    $this->main->set_activity($logu);
                    $logi = array('kd_izin' => $arrsiup['kd_izin'],
                        'permohonan_id' => $idredir,
                        'keterangan' => 'Menambahkan daftar permohonan Surat Izin Usaha Jasa Survey dengan nomor permohonan : ' . $arrsiup['no_aju'],
                        'catatan' => '',
                        'status' => '0000',
                        'selisih' => 0);
                    $this->main->set_loglicensing($logi);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } elseif ($act == "update") {
                $id = $this->input->post('id');
                $this->db->where('id', $id);
                $this->db->update('t_siujs', $arrsiup);
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $msg = "MSG||YES||Data Permohonan Surat Izin Usaha Jasa Survey berhasil diupdate.||REFRESH";
                }
                return $msg;
            }
        }
    }

    function get_second($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $jenis_identitas = $this->newsession->userdata('tipe_identitas');
            $id_user = $this->newsession->userdata('id');
            $idprop = $this->newsession->userdata('kdprop_pj');
            $idkab = $this->newsession->userdata('kdkab_pj');
            $idkec = $this->newsession->userdata('kdkec_pj');
            $idkel = $this->newsession->userdata('kdkel_pj');
            $jabatan_pj = $this->newsession->userdata('jabatan_pj');
            $arrdata['act'] = site_url('post/licensing/siujs_act/second/save');
            $arrdata['direktorat'] = $dir;
            $arrdata['id'] = $id;
            $arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '" . $jenis_identitas . "' and jenis = 'JENIS_IDENTITAS'", "kode", "uraian", TRUE);
            $arrdata['jabatan_pj'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JABATAN' AND kode = '" . $jabatan_pj . "' ", "kode", "uraian", TRUE);
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $idprop . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '" . $idkab . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '" . $idkec . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '" . $idkel . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['urifirst'] = site_url('licensing/form/first/' . $dir . '/' . $doc . '/' . $type . '/' . $id); //print_r($arrdata);die();
            $query = "SELECT id, kd_izin, tipe_permohonan, identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, alamat_pj, kdprop_pj, kdkab_pj, kdkec_pj,kdkel_pj, telp_pj, fax_pj, kdprop, kdkab, kdkec, kdkel, kdpos_pj
						FROM t_siujs WHERE id = '" . $id . "'";
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
                if ($row['identitas_pj']) {
                    $arrdata['act'] = site_url('post/licensing/siujs_act/second/update');
                }
                $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            return $arrdata;
        }
    }

    function set_second($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (($act == "update") || ($act == "save")) {
                if (!$isajax) {
                    return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                    exit();
                }
                $msg = "MSG||NO||Data Penanggung Jawab gagal disimpan";
                $respj = FALSE;
                $arrpj = $this->main->post_to_query($this->input->post('pj'));
                $dir = $this->input->post('direktorat');
                $doc = $this->input->post('kd_izin');
                $type = $this->input->post('tipe_permohonan');
                $id = $this->input->post('id');
                $this->db->where('id', $id);
                $this->db->update('t_siujs', $arrpj);
                if ($this->db->affected_rows() > 0) {
                    $respj = TRUE;
                    if ($act == "update") {
                        $msg = "MSG||YES||Data Penanggung Jawab berhasil diupdate.||REFRESH";
                    } else {
                        $msg = "MSG||YES||Data Penanggung Jawab berhasil disimpan. Silahkan lanjutkan mengisi daftar tenaga ahli.||" . site_url() . 'licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id;
                    }
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan Surat Izin Usaha Jasa Survey (Data Penanggung Jawab)',
                        'url' => '{c}' . site_url() . 'post/licensing/siujs_act/second/update' . ' {m} models/licensing/siujs_act {f} set_second($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                }
                return $msg;
            }
        }
    }

    function get_third($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $arrdata['act'] = site_url('post/licensing/siujs_act/third/save');
                $arrdata['urisecond'] = site_url('licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
                $query = "SELECT a.id, a.kd_izin, a.tipe_permohonan, a.pegawai_tetap, a.pegawai_honor, a.pegawai_asing, a.pegawai_lokal, a.relasi, a.kegiatan_usaha,
						a.kategori_survey as kategori
						  FROM t_siujs a WHERE a.id = '" . $id . "'";
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['kategori_survey'] = explode(";", $row['kategori']);
                    if ($row['pegawai_tetap']) {
                        $arrdata['act'] = site_url('post/licensing/siujs_act/third/update');
                        $query_bidang = "SELECT id, permohonan_id, keahlian, jumlah FROM t_siujs_bidang WHERE permohonan_id = '" . $id . "'";
                        $data_bidang = $this->main->get_result($query_bidang);
                        if ($data_bidang) {
                            $arrdata['bidang'] = $query_bidang->result_array();
                        }
                    }
                }
                $arrdata['survey'] = $this->main->set_combobox("SELECT id, kegiatan FROM m_survey", "id", "kegiatan", TRUE);
                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $arrdata['nama_izin'] = $nama_izin;
                $arrdata['direktorat'] = $dir;
                return $arrdata;
            }
        }
    }

    function set_third($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $respj = FALSE;
            $arrsiujs = $this->main->post_to_query($this->input->post('siujs'));
            $arrahli = $this->input->post('ahli');
            $arrkeys = array_keys($arrahli);
            //print_r($arrsiujs);
            //print_r($arrahli);die();
            $dir = $this->input->post('direktorat');
            $doc = $this->input->post('kd_izin');
            $type = $this->input->post('tipe_permohonan');
            $survey = implode(';', $this->input->post('kategori_survey'));

            // $jml_survey = count(explode(';',$survey));
            // if(strlen($jml_survey) == 1){
            // 	$res="S0".$jml_survey."-SIUJS.".date('my');
            // }else{
            // 	$res="S".$jml_survey."-SIUJS.".date('my');
            // }
            // $arrsiujs['no_izin'] = $res;
            $arrsiujs['kategori_survey'] = $survey;
            $id = $this->input->post('id');
            if ($act == "save") {
                $this->db->where('id', $id);
                $this->db->update('t_siujs', $arrsiujs);
                if ($this->db->affected_rows() > 0) {
                    for ($s = 0; $s < count($_POST['ahli']['keahlian']); $s++) {
                        $keahlian = array('permohonan_id' => $id,
                            'created' => 'GETDATE()');
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $keahlian [$arrkeys[$j]] = $arrahli[$arrkeys[$j]][$s];
                        }
                        unset($keahlian['id']);
                        $this->db->insert('t_siujs_bidang', $keahlian);
                    }

                    if ($this->db->affected_rows() > 0) {
                        $ressiup = TRUE;
                        $idUpload = $this->db->insert_id();
                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan Surat Izin Usaha Jasa Survey (Daftar Tenaga Ahli)',
                            'url' => '{c}' . site_url() . 'post/licensing/siujs_act/third/save' . ' {m} models/licensing/siujs_act {f} set_third($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                        $msg = "MSG||YES||Daftar Tenaga Ahli berhasil disimpan. Silahkan lanjutkan mengisi data tenaga ahli.||" . site_url() . 'licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id;
                    }
                    if ($this->db->trans_status() === FALSE || !$ressiup) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            } elseif ($act == "update") {
                $this->db->where('id', $id);
                $this->db->update('t_siujs', $arrsiujs);
                if ($this->db->affected_rows() > 0) {
                    $this->db->where('permohonan_id', $id);
                    $this->db->delete('t_siujs_bidang');
                    if ($this->db->affected_rows() > 0) {
                        for ($s = 0; $s < count($_POST['ahli']['keahlian']); $s++) {
                            $keahlian = array('permohonan_id' => $id,
                                'created' => 'GETDATE()');
                            for ($j = 0; $j < count($arrkeys); $j++) {
                                $keahlian [$arrkeys[$j]] = $arrahli[$arrkeys[$j]][$s];
                            }
                            unset($keahlian['id']);
                            $this->db->insert('t_siujs_bidang', $keahlian);
                        }
                        if ($this->db->affected_rows() > 0) {
                            $ressiup = TRUE;
                            $idUpload = $this->db->insert_id();
                            /* Log User dan Log Izin */
                            $logu = array('aktifitas' => 'Menambahkan daftar permohonan Surat Izin Usaha Jasa Survey (Update - Daftar Tenaga Ahli)',
                                'url' => '{c}' . site_url() . 'post/licensing/siujs_act/third/save' . ' {m} models/licensing/siujs_act {f} set_third($act, $isajax)');
                            $this->main->set_activity($logu);
                            /* Akhir Log User */
                            $msg = "MSG||YES||Daftar Tenaga Ahli berhasil diupdate.||REFRESH";
                        }
                        if ($this->db->trans_status() === FALSE || !$ressiup) {
                            $this->db->trans_rollback();
                        } else {
                            $this->db->trans_commit();
                        }
                    }
                }
            }

            return $msg;
        }
    }

    function get_lstfourth($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $table = $this->newtable;
                //$arrdata['act'] = site_url('post/licensing/siupmb_act/third/save');
                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $arrdata = array('id' => $id,
                    'direktorat' => $dir,
                    'kd_izin' => $doc,
                    'tipe_permohonan' => $type,
                    'nama_izin' => $nama_izin,
                    'act' => site_url('post/licensing/siujs_act/third/save'),
                    'urifirst' => site_url('licensing/form/first/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urisecond' => site_url('licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urithird' => site_url('licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urifourth' => site_url('licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urififth' => site_url('licensing/form/fifth/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urisixth' => site_url('licensing/form/fifth/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'uripreview' => site_url('licensing/preview/' . $dir . '/' . $doc . '/' . $type . '/' . $id));

                $query = "SELECT a.id, a.nama as 'Nama', a.warganegara as 'Kewarganegaraan', a.no_sertifikat as 'No. Sertifikat', a.penerbit as 'Penerbit' FROM t_siujs_ahli a
						  	WHERE a.permohonan_id = '" . $id . "'";


                $table->title("");
                $table->columns(array("a.id", "a.nama", "a.no_sertifikat", "a.penerbit", "a.warganegara"));
                $this->newtable->width(array('Nama' => 200, 'No. Sertifikat' => 100, 'Penerbit' => 100, 'Kewarganegaraan' => 100));
                $this->newtable->search(array(array("nama", "Nama"),
                    array("a.no_sertifikat", "No. Sertifikat"),
                    array("a.penerbit", "Penerbit"),
                    array("a.warganegara", "Kewarganegaraan")));
                $table->cidb($this->db);
                $table->ciuri($this->uri->segment_array());
                $table->action(site_url() . 'licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
                //$table->orderby(1);
                $table->sortby("ASC");
                $table->keys(array("id"));
                $table->hiddens(array("id"));
                $table->show_search(TRUE);
                $table->show_chk(TRUE);
                $table->single(TRUE);
                $table->dropdown(TRUE);
                $table->hashids(TRUE);
                $table->postmethod(TRUE);
                $table->tbtarget("a");
                $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/view/tenaga_ahli/' . $doc . '/' . $id, '0', 'home'),
                    'Edit' => array('GET', site_url() . 'licensing/view/tenaga_ahli/' . $doc . '/' . $id, '1', 'fa fa-edit'),
                    'Delete' => array('POST', site_url() . 'licensing/delete_tenaga_ahli/ajax', 'N', 'fa fa-times', 'isngajax')
                ));
                $arrdata['lstTngAhli'] = $table->generate($query);
                //print_r($arrdata);die();
                if ($this->input->post("data-post"))
                    return $table->generate($query);
                else
                    return $arrdata;
            }
        }
    }

    function get_tenaga_ahli($doc, $id, $dtl) {
        if ($this->newsession->userdata('_LOGGED')) {
            $detail_id = hashids_decrypt($dtl, _HASHIDS_, 9);
            $arrdata['permohonan_id'] = $id;
            $arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_IDENTITAS' and kode = '01' ", "kode", "uraian", TRUE);
            $query = "SELECT a.id, a.kd_izin, b.direktorat_id, b.nama_izin, a.tipe_permohonan, c.dok_id, c.tipe, d.keterangan, c.multi, e.uraian from t_siujs a
					LEFT JOIN m_izin b on b.id = a.kd_izin
					LEFT JOIN m_dok_izin c on c.izin_id = a.kd_izin and c.kategori = '03'
					LEFT JOIN m_dok d on d.id = c.dok_id
					LEFT JOIN m_reff e on e.kode = c.tipe and e.jenis = 'TIPE_DOKIZIN'
					WHERE a.id = '" . $id . "'";//print_r($query);die();
            $data_req = $this->main->get_result($query);
            $arrdata['req'] = $query->result_array();
            $s = $this->main->get_uraian("SELECT keahlian from t_siujs_ahli where id = '".$detail_id."' ", "keahlian");
            $arrdata['kategori_survey'] = explode(",", $s);
            $arrdata['act'] = site_url('post/licensing/siujs_act/fourth/save');
            $arrdata['survey'] = $this->main->set_combobox("SELECT id, kegiatan FROM m_survey", "id", "kegiatan", TRUE);
            if ($detail_id) {
                $query_ahli = "SELECT id, permohonan_id, nama, warganegara, jenis_identitas, no_identitas, no_sertifikat, penerbit FROM t_siujs_ahli WHERE permohonan_id = '" . $id . "' and id = '" . $detail_id . "'";
                $data_ahli = $this->main->get_result($query_ahli);
                if ($data_ahli) {
                    foreach ($query_ahli->result_array() as $row) {
                        $arrdata['ahli'] = $row;
                    }
                    $arrdata['act'] = site_url('post/licensing/siujs_act/fourth/update');
                }
                $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok, a.detail_id,
							'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id 
							WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "' and a.detail_id = '" . $detail_id . "'";
                //print_r($query_syarat);die();
                $data = $this->main->get_result($query_syarat);
                $arr = array();
                if ($data) {
                    foreach ($query_syarat->result_array() as $keys) {
                        $arr[$keys['dok_id']][] = $keys;
                    }
                    $arrdata['sess'] = $arr;
                }
            }
            //print_r($arrdata);die();
            return $arrdata;
        }
    }

    function set_fourth($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $res = FALSE;
            $arrsurvey = $this->input->post('kat_survey');
            $arrahli = $this->main->post_to_query($this->input->post('ahli'));
            $arrsiup = $this->main->post_to_query($this->input->post('SIUPMB'));
            $arrahli['permohonan_id'] = $arrsiup['permohonan_id'];
            $arrahli['keahlian'] = implode(",", $arrsurvey);
            $dir = $this->input->post('direktorat');
            $tipe_permohonan = $this->input->post('tipe_permohonan');
            $doc = $arrsiup['izin_id'];
            $arrreq = $this->input->post('REQUIREMENTS');
            $arrkeys = array_keys($arrreq);
            if ($act == "save") {
                $arrahli['created'] = 'GETDATE()';
                $this->db->insert('t_siujs_ahli', $arrahli);//print_r($this->db->last_query());die();
                if ($this->db->affected_rows() > 0) {
                    $resahli = TRUE;
                    $idAhli = $this->db->insert_id();
                    for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                        $requirements = array('izin_id' => $arrsiup['izin_id'],
                            'detail_id' => $idAhli,
                            'permohonan_id' => $arrsiup['permohonan_id'],
                            'created' => 'GETDATE()',
                            'created_user' => $this->newsession->userdata('username'));
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                        }
                        unset($requirements['id']);
                        $this->db->insert('t_upload_syarat', $requirements);
                    }
                    if ($this->db->affected_rows() > 0) {
                        $res = TRUE;
                        $idUpload = $this->db->insert_id();
                        $msg = "MSG||YES||Data Tenaga Ahli berhasil disimpan.||" . site_url() . 'licensing/form/fourth/' . $dir . '/' . $doc . '/' . $tipe_permohonan . '/' . $arrahli['permohonan_id'];
                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan Surat Izin Usaha Jasa Survey (Data Tenaga Ahli)',
                            'url' => '{c}' . site_url() . 'post/licensing/siujs_act/fourth/save' . ' {m} models/licensing/siujs_act {f} set_fourth($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                    }
                    if ($this->db->trans_status() === FALSE || !$res) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            } elseif ($act == "update") {
                $ahli_id = $this->input->post('id_ahli');
                $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                $this->db->where('detail_id', $ahli_id);
                $this->db->update('t_siujs_ahli', $arrahli);

                $this->db->where('izin_id', $arrsiup['izin_id']);
                $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                $this->db->where('detail_id', $ahli_id);
                $this->db->delete('t_upload_syarat');
                // for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                //     $requirements = array('updated' => 'GETDATE()');
                //     for ($j = 0; $j < count($arrkeys); $j++) {
                //         $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                //     }
                //     $idx = $requirements['id'];
                //     unset($requirements['id']);
                //     $this->db->where('id', $idx);
                //     $this->db->where('izin_id', $arrsiup['izin_id']);
                //     $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                //     $this->db->where('detail_id', $ahli_id);
                //     $this->db->update('t_upload_syarat', $requirements);print_r($this->db->last_query());die();
                //print_r($_POST['REQUIREMENTS']['dok_id']);die();
                 for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                    $requirements = array('izin_id' => $arrsiup['izin_id'],
                        'detail_id' => $ahli_id,
                        'permohonan_id' => $arrsiup['permohonan_id'],
                        'created' => 'GETDATE()',
                        'created_user' => $this->newsession->userdata('username'));
                    for ($j = 0; $j < count($arrkeys); $j++) {
                        $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                    }
                    unset($requirements['id']);
                    $this->db->insert('t_upload_syarat', $requirements);
                }
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $msg = "MSG||YES||Data Tenaga Ahli berhasil diupdate.||REFRESH";
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            }
            //print_r($act);die();
            return $msg;
        }
    }

    function get_lstfifth($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($id == "") {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            } else {
                $table = $this->newtable;
                //$arrdata['act'] = site_url('post/licensing/siupmb_act/third/save');
                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $arrdata = array('id' => $id,
                    'direktorat' => $dir,
                    'kd_izin' => $doc,
                    'tipe_permohonan' => $type,
                    'nama_izin' => $nama_izin,
                    'act' => site_url('post/licensing/siujs_act/third/save'),
                    'urifirst' => site_url('licensing/form/first/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urisecond' => site_url('licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urithird' => site_url('licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urifourth' => site_url('licensing/form/fourth/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'urisixth' => site_url('licensing/form/sixth/' . $dir . '/' . $doc . '/' . $type . '/' . $id),
                    'uripreview' => site_url('licensing/preview/' . $dir . '/' . $doc . '/' . $type . '/' . $id));

                $query = "SELECT a.id, a.nama_cabang AS 'Nama Cabang', a.lokasi AS 'Lokasi Cabang', b.nama as 'Negara', a.alamat AS 'Alamat' FROM t_siujs_cabang a
							LEFT JOIN m_negara b ON b.kode = a.negara
							WHERE permohonan_id = '" . $id . "'";


                $table->title("");
                $table->columns(array("id", array("nama_cabang", site_url() . 'licensing/append/{id}/' . $doc . '/' . $id, 'append'), "lokasi", "negara", "alamat"));
                $this->newtable->width(array('Nama Cabang' => 300, 'Lokasi' => 50, 'Negara' => 200, 'Alamat' => 400));
                $this->newtable->search(array(array("a.nama_cabang", "Nama Cabang"),
                    array("a.lokasi", "Lokasi"),
                    array("b.nama", "Negara"),
                    array("a.alamat", "Alamat")));
                $table->cidb($this->db);
                $table->ciuri($this->uri->segment_array());
                $table->action(site_url() . "/licensing/form/fifth/" . $dir . "/" . $doc . "/" . $type . "/" . $id);
                $table->orderby(1);
                $table->sortby("ASC");
                $table->keys(array("id"));
                $table->hiddens(array("id"));
                $table->show_search(TRUE);
                $table->show_chk(TRUE);
                $table->single(TRUE);
                $table->dropdown(TRUE);
                $table->hashids(TRUE);
                $table->postmethod(TRUE);
                $table->tbtarget("reffifth");
                $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/view/cabang/' . $doc . '/' . $id, '0', 'home'),
                    'Edit' => array('GET', site_url() . 'licensing/view/cabang/' . $doc . '/' . $id, '1', 'fa fa-edit'),
                    'Delete' => array('POST', site_url() . 'licensing/delete_data_cabang/ajax', 'N', 'fa fa-times', 'isngajax')));
                $arrdata['lstCabang'] = $table->generate($query);
                //print_r($arrdata);die();
                if ($this->input->post("data-post"))
                    return $table->generate($query);
                else
                    return $arrdata;
            }
        }
    }

    function get_cabang($id, $dtl) {
        if ($this->newsession->userdata('_LOGGED')) {
            $detail_id = hashids_decrypt($dtl, _HASHIDS_, 9);
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            $arrdata['negara'] = $this->main->set_combobox("SELECT kode, nama FROM m_negara ORDER BY 2", "kode", "nama", TRUE);
            $arrdata['act'] = site_url('post/licensing/siujs_act/fifth/save');
            $query = "SELECT b.direktorat_id, a.kd_izin, a.tipe_permohonan, b.nama_izin, a.id FROM t_siujs a
					LEFT JOIN m_izin b ON b.id = a.kd_izin
					WHERE a.id = " . $id;
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $rowx) {
                    $arrdata['sess'] = $rowx;
                }
                $arrdata['urififth'] = site_url('licensing/form/fifth/' . $rowx['direktorat_id'] . '/' . $rowx['kd_izin'] . '/' . $rowx['tipe_permohonan'] . '/' . $id);
            }

            if ($detail_id) {
                $query_cabang = "SELECT id, permohonan_id, nama_cabang, lokasi, negara, alamat, kdprop, kdkab, kdkec, kdkel, telp, fax FROM t_siujs_cabang WHERE permohonan_id = '" . $id . "' and id = '" . $detail_id . "'";
                $data_cabang = $this->main->get_result($query_cabang);
                if ($data_cabang) {
                    foreach ($query_cabang->result_array() as $row) {
                        $arrdata['cabang'] = $row;
                    }
                    $arrdata['act'] = site_url('post/licensing/siujs_act/fifth/update');
                    $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec'] . "' ORDER BY 2", "id", "nama", TRUE);
                }
            }
            //print_r($arrdata);die();

            return $arrdata;
        }
    }

    function set_fifth($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $res = FALSE;
            $arrcabang = $this->main->post_to_query($this->input->post('cabang'));
            if ($arrcabang['lokasi'] == 'Dalam Negeri') {
                $arrcabang['negara'] = 'ID';
            } else {
                unset($arrcabang['kdprop']);
                unset($arrcabang['kdkab']);
                unset($arrcabang['kdkec']);
                unset($arrcabang['kdkel']);
            }
            $siujs = $this->input->post('siujs');
            if ($act == "save") {
                $arrcabang['created'] = 'GETDATE()';
                $arrcabang['created_user'] = $this->newsession->userdata('username');
                //print_r($arrcabang);die();
                $this->db->insert('t_siujs_cabang', $arrcabang);
                if ($this->db->affected_rows() > 0) {
                    $res = TRUE;
                    $idCabang = $this->db->insert_id();
                    $msg = "MSG||YES||Data Cabang berhasil disimpan.||" . site_url() . 'licensing/form/fifth/' . $siujs['dir'] . '/' . $siujs['doc'] . '/' . $siujs['type'] . '/' . $arrcabang['permohonan_id'];
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan Surat Izin Usaha Jasa Survey (Data Cabang)',
                        'url' => '{c}' . site_url() . 'post/licensing/siujs_act/fifth/save' . ' {m} models/licensing/siujs_act {f} set_fifth($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                }
                if ($this->db->trans_status() === FALSE || !$res) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            } elseif ($act == "update") {
                $idCabang = $this->input->post('id');
                $this->db->where('permohonan_id', $arrcabang['permohonan_id']);
                $this->db->where('id', $idCabang);
                $this->db->update('t_siujs_cabang', $arrcabang);
                if ($this->db->affected_rows() > 0) {
                    $res = TRUE;
                    $msg = "MSG||YES||Data Cabang berhasil diupdate.||REFRESH";
                }
                if ($this->db->trans_status() === FALSE || !$res) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            }
            //print_r($act);die();
            return $msg;
        }
    }

    function get_sixth($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $arrdata['nama_izin'] = $nama_izin;
            $arrdata['izin_id'] = $doc;
            $arrdata['direktorat'] = $dir;
            $arrdata['type'] = $type;
            $arrdata['permohonan_id'] = $id;
            $arrdata['urithird'] = site_url('licensing/form/third/' . $dir . '/' . $doc . '/' . $type . '/' . $id);
            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id,
							 case when c.multi = 1 then '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor else '<a href=\"javascript:void(0);\" class=\"removetr\" id=\"" . rand() . "\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp;' + b.nomor end AS nomor,
							  b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							 '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							 FROM t_upload_syarat a 
							 LEFT join t_upload b on b.id = a.upload_id
							 LEFT JOIN m_dok_izin c on c.dok_id = a.dok_id and c.izin_id = a.izin_id
							 WHERE a.izin_id = '" . $doc . "' and a.permohonan_id = '" . $id . "' and a.detail_id is null";

            /* $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
              '<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
              FROM t_upload_syarat a
              LEFT join t_upload b on b.id = a.upload_id
              WHERE a.izin_id = '".$doc."' and a.permohonan_id = '".$id."' and a.detail_id is null"; */


            $data = $this->main->get_result($query_syarat);
            $arr = array();

            $query = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian, a.multi
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.kategori <> '03' and a.izin_id = '" . $doc . "' " . $addCon . " order by 5 desc";
            //print_r($query);die();
            $data_req = $this->main->get_result($query);
            $temp = $query->result_array();
            $arrdata['req'] = $temp;

            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
                    //$arr[$keys['dok_id']] = $keys;
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess'] = $arr;
                $arrdata['act'] = site_url('post/licensing/siujs_act/sixth/update');
            } else {
                foreach ($temp as $datadt) {
                    $query_syarat2 = "  SELECT top 1 b.id as 'upload_id', b.tipe_dok, b.tipe_dok as  dok_id,  CASE WHEN a.multi = 1 THEN ' <a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this), true); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp; ' + b.nomor ELSE ' <a href=\"javascript:void(0);\" class=\"removetr\" id=\"".rand()."\" onClick=\"rmtrreq($(this)); return false;\"><i class=\"fa fa-minus-square text-danger\"></i></a>&nbsp; ' + b.nomor END AS nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
					'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View' 
                                            FROM t_upload b left join m_dok_izin a on b.tipe_dok = a.dok_id 
					where a.izin_id = '" . $doc . "' and b.trader_id = '" . $this->newsession->userdata('trader_id') . "'and b.tipe_dok = '" . $datadt['dok_id'] . "'  " . $addCon . " order by tgl_dok desc;";
                    $cek = $this->db->query($query_syarat2)->num_rows();
                    //print_r($a);die();
                    $data2 = $this->main->get_result($query_syarat2);
                    if ($cek != 0) {
                        foreach ($query_syarat2->result_array() as $keys) {
                            if (!isset($arr[$keys['dok_id']])) {
                                $arr[$keys['dok_id']] = array();
                            }
                            $arr[$keys['dok_id']][] = $keys;
                        }   
                    }
                    $arrdata['sess'] = $arr;
                }
                $arrdata['baru'] = true;
                $arrdata['act'] = site_url('post/licensing/siujs_act/sixth/save');
            }
            return $arrdata;
        }
    }

    function set_sixth($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $ressiup = FALSE;
            $arrsiup = $this->main->post_to_query($this->input->post('SIUPMB'));
            $arrreq = $this->input->post('REQUIREMENTS');
            $arrkeys = array_keys($arrreq);
            if ($act == "save") {
                for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                    $requirements = array('izin_id' => $arrsiup['izin_id'],
                        'permohonan_id' => $arrsiup['permohonan_id'],
                        'created' => 'GETDATE()',
                        'created_user' => $this->newsession->userdata('username'));
                    for ($j = 0; $j < count($arrkeys); $j++) {
                        $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                    }
                    unset($requirements['id']);
                    if ($requirements['upload_id'] != "")
                        $this->db->insert('t_upload_syarat', $requirements);
                }
                if ($this->db->affected_rows() > 0) {
                    $ressiup = TRUE;
                    $idUpload = $this->db->insert_id();
                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan daftar permohonan Surat Izin Usaha Jasa Survey (Data Persyaratan)',
                        'url' => '{c}' . site_url() . 'post/licensing/siujs_act/fourth/save' . ' {m} models/licensing/siujs_act {f} set_fourth($act, $isajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User */
                    $msg = "MSG||YES||Data Persyaratan berhasil disimpan. Pengajuan Permohonan Surat Izin Usaha Jasa Survey selesai.||" . site_url() . 'licensing/view/status';
                }
                if ($this->db->trans_status() === FALSE || !$ressiup) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            } elseif ($act == "update") {
                $this->db->where('permohonan_id', $arrsiup['permohonan_id']);
                $this->db->where('izin_id', $arrsiup['izin_id']);
                $this->db->where('detail_id', null);
                $this->db->delete('t_upload_syarat');
                if ($this->db->affected_rows() > 0) {
                    for ($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++) {
                        $requirements = array('izin_id' => $arrsiup['izin_id'],
                            'permohonan_id' => $arrsiup['permohonan_id'],
                            'created' => 'GETDATE()',
                            'created_user' => $this->newsession->userdata('username'));
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
                        }
                        unset($requirements['id']);
                        if ($requirements['upload_id'] != "")
                            $this->db->insert('t_upload_syarat', $requirements);
                    }
                    if ($this->db->affected_rows() > 0) {
                        $ressiup = TRUE;
                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan daftar permohonan Surat Izin Usaha Jasa Survey (Update - Data Persyaratan)',
                            'url' => '{c}' . site_url() . 'post/licensing/siujs_act/update/save' . ' {m} models/licensing/siujs_act {f} set_fourth($act, $isajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User */
                        $msg = "MSG||YES||Data Persyaratan berhasil diupdate.||REFRESH";
                    }
                    if ($this->db->trans_status() === FALSE || !$ressiup) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }
            return $msg;
        }
    }

    function get_preview($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($type == '01') {
                $addCon = "AND a.baru = '1'";
            } elseif ($type == '02') {
                $addCon = "AND a.perubahan = '1'";
            } elseif ($type == '03') {
                $addCon = "AND a.perpanjangan = '1'";
            }


            $query = "SELECT a.id, b.direktorat_id, a.kd_izin,b.nama_izin, a.no_aju, a.trader_id, a.tipe_permohonan, c.uraian AS permohonan ,a.npwp, d.uraian AS tipe_perusahaan, 
						a.nm_perusahaan, a.almt_perusahaan, 
						e.nama AS kdprop, f.nama AS kdkab, g.nama AS kdkec, h.nama AS kdkel, a.kdpos, a.telp, a.fax, 
						a.pegawai_tetap, a.pegawai_honor, a.pegawai_asing, a.pegawai_lokal, a.relasi, a.kegiatan_usaha,
						l.uraian AS identitas_pj, a.nama_pj, a.noidentitas_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, a.alamat_pj,
						m.nama AS kdprop_pj, n.nama AS kdkab_pj, o.nama AS kdkec_pj, p.nama AS kdkel_pj, a.telp_pj, a.fax_pj, a.status, b.disposisi,
						a.kategori_survey, q.uraian as 'jabatan_pj', a.no_aju, dbo.dateIndo(a.tgl_kirim) as tgl_aju
						FROM t_siujs a 
						LEFT JOIN m_izin b ON b.id = a.kd_izin 
						LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
						LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
						LEFT JOIN m_prop e ON e.id = a.kdprop
						LEFT JOIN m_kab f ON f.id = a.kdkab
						LEFT JOIN m_kec g ON g.id = a.kdkec
						LEFT JOIN m_kel h ON h.id = a.kdkel
						LEFT JOIN m_reff l ON l.kode = a.identitas_pj AND l.jenis = 'JENIS_IDENTITAS'
						LEFT JOIN m_prop m ON m.id = a.kdprop_pj
						LEFT JOIN m_kab n ON n.id = a.kdkab_pj
						LEFT JOIN m_kec o ON o.id = a.kdkec_pj
						LEFT JOIN m_kel p ON p.id = a.kdkel_pj
						LEFT JOIN m_reff q ON q.kode = a.jabatan_pj AND q.jenis = 'JABATAN'						
						WHERE a.id = '" . $id . "'";
            $data = $this->main->get_result($query);

            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
                $survey = explode(";", $row['kategori_survey']);
                foreach ($survey as $id_survey) {
                    $sql_survey = "select kegiatan from m_survey where id='" . $id_survey . "'";
                    if ($this->main->get_result($sql_survey)) {
                        foreach ($sql_survey->result_array() as $data) {
                            $arrdata['kegiatan_survey'][] = $data['kegiatan'];
                        }
                    }
                }
                //print_r($arrdata);die();

                $query_bidang = "SELECT id, permohonan_id, keahlian, jumlah FROM t_siujs_bidang WHERE permohonan_id = '" . $id . "'";
                $data_bidang = $this->main->get_result($query_bidang);
                if ($data_bidang) {
                    $arrdata['bidang'] = $query_bidang->result_array();
                }

          //       $table = $this->newtable;
          //       $query = "SELECT a.id, a.nama as 'Nama', a.warganegara as 'Kewarganegaraan', a.no_sertifikat as 'No. Sertifikat', a.penerbit as 'Penerbit' FROM t_siujs_ahli a
							  	// WHERE a.permohonan_id = '" . $id . "'";
          //       $table->title("");
          //       $table->columns(array("a.id", array("a.nama", site_url() . 'licensing/popup_ahli/{id}/' . $doc . '/' . $id, 'modal'), "a.no_sertifikat", "a.penerbit", "a.warganegara"));
          //       $this->newtable->width(array('Nama' => 200, 'No. Sertifikat' => 100, 'Penerbit' => 100, 'Kewarganegaraan' => 100));
          //       $table->cidb($this->db);
          //       $table->ciuri($this->uri->segment_array());
          //       $table->action(site_url() . "/licensing/form/fourth/" . $dir . "/" . $doc . "/" . $type . "/" . $id);
          //       $table->orderby(1);
          //       $table->sortby("ASC");
          //       $table->keys(array("id"));
          //       $table->hiddens(array("id"));
          //       $table->show_search(FALSE);
          //       $table->show_chk(FALSE);
          //       $table->single(TRUE);
          //       $table->dropdown(TRUE);
          //       $table->hashids(TRUE);
          //       $table->postmethod(TRUE);
          //       $table->tbtarget("reffourth");
          //       $arrdata['lstTngAhli'] = $table->generate($query);
          //       $table->clear();
                $query = "SELECT a.id, a.nama, a.warganegara, a.no_sertifikat, a.penerbit FROM t_siujs_ahli a
                    WHERE a.permohonan_id = '" . $id . "'";
                $arrdata['lstTngAhli'] = $this->db->query($query)->result_array();

                $arrdata['act'] = site_url() . 'post/proccess/siujs_act/verification';
                $arrdata['input'] = $this->get_input($dir, $doc, $type, $id, $row['status']);
                $arrdata['proses'] = $this->main->set_proses($dir, $row['kd_izin'], $row['disposisi'], $this->newsession->userdata('role'), $row['status'], hashids_encrypt($row['id'], _HASHIDS_, 9));
                $arrdata['jmllog'] = $this->main->get_uraian("SELECT COUNT(*) AS JML FROM t_log_izin WHERE permohonan_id = '" . $row['id'] . "' AND kd_izin = '" . $row['kd_izin'] . "'", "JML");
                $arrdata['urllog'] = site_url() . 'get/log/izin/' . hashids_encrypt($row['id'], _HASHIDS_, 9) . '/' . hashids_encrypt($row['kd_izin'], _HASHIDS_, 9);
                // $arrdata['cetak'] = '<button class="btn btn-sm btn-warning addon-btn m-b-10" id= "'.rand().'" data-url = "'.site_url().'prints/licensing/'.hashids_encrypt($dir,_HASHIDS_,9).'/'.hashids_encrypt($row['kd_izin'],_HASHIDS_,9).'/'.hashids_encrypt($row['id'],_HASHIDS_,9).'/penjelasan" onclick="blank($(this));return false;"><i class="fa fa-print pull-right"></i>Cetak Penjelasan</button>';
            }

       //      $query_cabang = "SELECT a.id, a.nama_cabang AS 'Nama Cabang', a.lokasi AS 'Lokasi Cabang', b.nama as 'Negara', a.alamat AS 'Alamat' FROM t_siujs_cabang a
							// LEFT JOIN m_negara b ON b.kode = a.negara
							// WHERE permohonan_id = '" . $id . "'";
       //      $table->title("");
       //      $table->columns(array("id", array("nama_cabang", site_url() . 'licensing/append/{id}/' . $row['kd_izin'] . '/' . $id, 'append'), "lokasi", "negara", "alamat"));
       //      $this->newtable->width(array('Nama Cabang' => 300, 'Lokasi' => 50, 'Negara' => 200, 'Alamat' => 400));
       //      $this->newtable->search(array(array("a.nama_cabang", "Nama Cabang"),
       //          array("a.lokasi", "Lokasi"),
       //          array("b.nama", "Negara"),
       //          array("a.alamat", "Alamat")));
       //      $table->cidb($this->db);
       //      $table->ciuri($this->uri->segment_array());
       //      $table->action(site_url() . "/licensing/preview/01/1/01/1");
       //      $table->orderby(1);
       //      $table->sortby("ASC");
       //      $table->keys(array("id"));
       //      $table->hiddens(array("id"));
       //      $table->show_search(FALSE);
       //      $table->show_chk(FALSE);
       //      $table->single(TRUE);
       //      $table->dropdown(TRUE);
       //      $table->hashids(TRUE);
       //      $table->expandrow(FALSE);
       //      $table->postmethod(TRUE);
       //      $table->tbtarget("reffifth");
       //      $arrdata['lstCabang'] = $table->generate($query_cabang);
            $query_cabang = "SELECT a.id, a.nama_cabang, a.lokasi, b.nama as negara, a.alamat FROM t_siujs_cabang a
                LEFT JOIN m_negara b ON b.kode = a.negara
                WHERE permohonan_id = '" . $id . "'";
            $arrdata['lstCabang'] = $this->db->query($query_cabang)->result_array();

            $query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							'<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id 
							WHERE a.permohonan_id = '" . $id . "' AND a.izin_id = '" . $doc . "' AND a.detail_id is null";
            //print_r($query_syarat);die();
            $data = $this->main->get_result($query_syarat);
            $arr = array();
            if ($data) {
                foreach ($query_syarat->result_array() as $keys) {
                    //$arr[$keys['dok_id']] = $keys;
                    if (!isset($arr[$keys['dok_id']])) {
                        $arr[$keys['dok_id']] = array();
                    }
                    $arr[$keys['dok_id']][] = $keys;
                }
                $arrdata['sess_syarat'] = $arr;
            }
            $query = "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.kategori <> '03' and a.izin_id = '" . $doc . "' " . $addCon . " order by a.urutan ASC";
            $data_req = $this->main->get_result($query);
            $arrdata['req'] = $query->result_array();
            $arrdata['dir'] = $dir;
            $arrdata['telaah'] = $this->main->get_telaah($doc);
            $arrdata['agrement'] = $this->main->get_agrement($doc);
            return $arrdata;
        }
    }

    function get_dtl_cabang($id, $permohonan_id) {
        $idx = hashids_decrypt($id, _HASHIDS_, 9);
        $query = "SELECT a.id, a.permohonan_id, a.nama_cabang, a.lokasi, telp, fax,
				CASE WHEN a.lokasi = 'Dalam Negeri' THEN CAST(a.alamat AS VARCHAR) +isnull((( ', ' +d.nama)+(', '+c.nama)+(', '+b.nama)+(', '+e.nama)),'') ELSE a.alamat END as alamat 
				FROM t_siujs_cabang a 
				LEFT JOIN m_kab b ON b.id = a.kdkab 
				LEFT JOIN m_kec c ON c.id = a.kdkec 
				LEFT JOIN m_kel d ON d.id = a.kdkel 
				LEFT JOIN m_prop e ON e.id = a.kdprop 
				WHERE a.id = " . $idx . " AND a.permohonan_id = " . $permohonan_id;
        //print_r($query);die();
        $data = $this->main->get_result($query);
        if ($data) {
            foreach ($query->result_array() as $row) {
                $arrdata['sess'] = $row;
            }
        }
        return $arrdata;
    }

    function get_input($dir, $doc, $type, $id, $stts) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrstts = array('0102');
            $arrdata = array();
            if (in_array($stts, $arrstts)) {
                $query = "SELECT a.id, a.kd_izin, a.no_aju, CONVERT(VARCHAR(10), a.tgl_aju, 103) AS tgl_aju, a.no_izin, CONVERT(VARCHAR(10), a.tgl_izin, 103) as tgl_izin, CONVERT(VARCHAR(10), a.tgl_izin_exp, 103) AS tgl_izin_exp FROM t_siujs a WHERE a.id = '" . $id . "' AND a.kd_izin = '" . $doc . "'";
                $ret = $this->main->get_result($query);
                if ($ret) {
                    $this->ineng = $this->session->userdata('site_lang');
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['dir'] = $dir;
                    $arrdata['doc'] = $doc;
                }
                if (!$this->session->userdata('site_lang'))
                    $this->ineng = "id";
                $data = $this->load->view($this->ineng . '/backend/input/' . $dir . '/' . $stts, $arrdata, true);
            } else {
                $data = "";
            }
        }
        return $data;
    }

    function set_onfly($act, $id, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($act == "update") {
                if (!$isajax) {
                    return false;
                    exit();
                }
                $msg = "MSG||NO";
                $respon = FALSE;
                $arrsiup = $this->main->post_to_query($this->input->post('dataon'));
                $id = hashids_decrypt($id, _HASHIDS_, 9);
                $this->db->where('id', $id);
                $this->db->update('t_siujs', $arrsiup);
                if ($this->db->affected_rows() == 1) {
                    $respon = TRUE;
                    $logu = array('aktifitas' => 'Melakukan update tanggal izin / tanggal masa berlaku',
                        'url' => '{c}' . site_url() . 'get/onfly/onfly_act/update' . ' {m} models/licensing/siujs_act {f} set_onfly($act,  $id, $isajax)');
                    $this->main->set_activity($logu);
                }
                if ($respon)
                    $msg = "MSG||YES";
                return $msg;
            }
        }
    }

    function referensi_kbli($target, $callback, $fieldcallback) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT kode as kbli, uraian as desc_kbli, kode AS 'Kode KBLI', uraian AS 'Uraian KBLI' FROM m_kbli";
            $table->title("");
            $table->columns(array("kode", "Kode KBLI", "uraian", "Uraian KBLI"));
            $this->newtable->width(array('Kode KBLI' => 100, 'Uraian' => 300, '&nbsp;' => 5));
            $this->newtable->search(array(array("kode", "Kode KBLI"),
                array("uraian", "Uraian")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_kbli/" . $target . '/' . $callback . '/' . $fieldcallback);
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("kode"));
            $table->hiddens(array("kbli", "desc_kbli"));
            $table->use_ajax(TRUE);
            $table->show_search(TRUE);
            $table->show_chk(FALSE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);
            $table->settrid(TRUE);
            $table->attrid($target);
            if ($callback != "")
                $table->callback(site_url(str_replace(".", '/', $callback)));
            if ($fieldcallback != "")
                $table->fieldcallback($fieldcallback);
            $table->tbtarget("refkbli");
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function list_dok($target, $callback, $fieldcallback, $id, $multiple, $putin, $doc) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT id, id AS 'upload_id', nomor AS 'No. Dokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'Tgl. Dokumen', tgl_exp AS 'Tgl. Akhir', '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
			FROM t_upload WHERE tipe_dok = '" . $id . "' AND trader_id = '" . $this->newsession->userdata('trader_id') . "'";
            $table->title("");
            $table->columns(array("id", "nomor", "penerbit_dok", "tgl_dok", "tgl_exp", "<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
            $this->newtable->width(array('Nomor Dokumen' => 100, 'Penerbit' => 300, 'Tanggal Awal' => 100, 'Tanggal Akhir' => 100, '&nbsp;' => 5));
            $this->newtable->search(array(array("nomor", "Nomor Penerbit"),
                array("penerbit_dok", "Penerbit")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_syarat/" . $id . '/' . $target . '/' . $callback . '/' . $fieldcallback . '/');
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id", "folder", "nama_file", "upload_id"));
            $table->show_search(TRUE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->postmethod(TRUE);

            if ((int) $multiple == 1) {
                $table->show_chk(TRUE);
                $table->menu(array('Pilih Data' => array('POSTGET', site_url() . 'post/document/get_requirements/' . $doc . '/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#' . $putin)));
            } else {
                $table->show_chk(FALSE);
                if ($callback != "")
                    $table->callback(site_url(str_replace(".", '/', $callback)));
                if ($fieldcallback != "")
                    $table->fieldcallback($fieldcallback);
                $table->settrid(TRUE);
                $table->attrid($target);
            }
            $table->tbtarget("refreq_" . rand());
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function list_dok_ahli($id, $doc, $permohonan_id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT a.id, c.keterangan as 'Keterangan', b.nomor as 'Nomor Dokumen', b.penerbit_dok as 'Penerbit',  b.tgl_dok as 'Tanggal Awal', b.tgl_exp as 'Tanggal Akhir',
					  '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
					  FROM t_upload_syarat a 
					  LEFT JOIN t_upload b ON b.id = a.upload_id
					  LEFT JOIN m_dok c ON c.id = a.dok_id
					  WHERE a.izin_id = '" . $doc . "' AND a.detail_id ='" . $id . "' AND a.permohonan_id = '" . $permohonan_id . "'";
            $table->title("");
            $table->columns(array("a.id", "c.keterangan", "b.nomor", "b.penerbit_dok", "b.tgl_dok", "b.tgl_exp", "<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
            $this->newtable->width(array('Keterangan' => 350, 'Nomor Dokumen' => 100, 'Penerbit' => 200, 'Tanggal Awal' => 50, 'Tanggal Akhir' => 50));
            $this->newtable->search(array(array("nomor", "Nomor Penerbit"),
                array("penerbit_dok", "Penerbit")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "/licensing/popup_ahli/" . hashids_encrypt($id, _HASHIDS_, 9) . "/" . $doc);
            $table->orderby(1);
            $table->sortby("ASC");
            $table->keys(array("id"));
            $table->hiddens(array("id"));
            $table->use_ajax(TRUE);
            $table->show_search(FALSE);
            $table->show_chk(FALSE);
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            //$table->settrid(TRUE);
            //$table->attrid($target);
            $table->tbtarget("refreq" . rand());
            $arrdata = array('tabel' => $table->generate($query));
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function get_requirements($doc) {
        $id = join("','", $this->input->post('tb_chk'));
        $data = "SELECT a.id AS dok_ids, b.id AS id_upload, b.nomor, b.penerbit_dok, b.tgl_dok, b.tgl_exp, '<a href=\"" . site_url() . "download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS files, c.tipe FROM m_dok a LEFT JOIN t_upload B ON a.id = b.tipe_dok LEFT JOIN m_dok_izin c ON c.dok_id = a.id WHERE b.id IN('" . $id . "') AND c.izin_id = '" . $doc . "'";
        //print_r($data);die();
        return $this->db->query($data)->result_array();
    }

    function insert_bidang($id, $id_old) {
        $ret = FALSE;
        $query_old_bidang = "SELECT keahlian, jumlah FROM t_siujs_bidang where permohonan_id = '" . $id_old . "'";
        $data_old_bidang = $this->main->get_result($query_old_bidang);
        if ($data_old_bidang) {
            foreach ($query_old_bidang->result_array() as $keys) {
                $arrBidang['permohonan_id'] = $id;
                $arrBidang['created'] = 'GETDATE()';
                $jml = count($keys);
                $array_keys = array_keys($keys);
                $array_values = array_values($keys);
                for ($i = 0; $i < $jml; $i++) {
                    $arrBidang[$array_keys[$i]] = $array_values[$i];
                }
                $this->db->insert('t_siujs_bidang', $arrBidang);
            }
            if ($this->db->affected_rows() > 0) {
                $ret = TRUE;
            }
        }
        return $ret;
    }

    function insert_cabang($id, $id_old) {
        $ret = FALSE;
        $query_old_cabang = "SELECT nama_cabang, lokasi, negara, alamat, kdprop, kdkab, kdkec, kdkel, telp, fax FROM t_siujs_cabang where permohonan_id = '" . $id_old . "'";
        $data_old_cabang = $this->main->get_result($query_old_cabang);
        if ($data_old_cabang) {
            foreach ($query_old_cabang->result_array() as $keys) {
                $arrCabang['permohonan_id'] = $id;
                $arrCabang['created'] = 'GETDATE()';
                $arrCabang['created_user'] = $this->newsession->userdata('username');
                $jml = count($keys);
                $array_keys = array_keys($keys);
                $array_values = array_values($keys);
                for ($i = 0; $i < $jml; $i++) {
                    $arrCabang[$array_keys[$i]] = $array_values[$i];
                }
                $this->db->insert('t_siujs_cabang', $arrCabang);
            }
            if ($this->db->affected_rows() > 0) {
                $ret = TRUE;
            }
        }
        return $ret;
    }

    function delete_ahli($data) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->db->trans_begin();
            $i = 0;
            foreach ($data as $id) {
                $hasid = hashids_decrypt($id, _HASHIDS_, 9);
                $this->db->where("id", $hasid);
                $this->db->delete("t_siujs_ahli");
                if ($this->db->affected_rows() == 0) {
                    $i++;
                    break;
                }
            }
            if ($this->db->trans_status === FALSE || $i > 0) {
                $msg = "MSG#Proses Gagal#refresh";
                $this->db->trans_rollback();
            } else {
                $msg = "MSG#Proses Berhasil#refresh";
                $this->db->trans_commit();
            }
            return $msg;
        }
    }

    function delete_cabang($data) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->db->trans_begin();
            $i = 0;
            foreach ($data as $id) {
                $hasid = hashids_decrypt($id, _HASHIDS_, 9);
                $this->db->where("id", $hasid);
                $this->db->delete("t_siujs_cabang");
                if ($this->db->affected_rows() == 0) {
                    $i++;
                    break;
                }
            }
            if ($this->db->trans_status === FALSE || $i > 0) {
                $msg = "MSG#Proses Gagal#refresh";
                $this->db->trans_rollback();
            } else {
                $msg = "MSG#Proses Berhasil#refresh";
                $this->db->trans_commit();
            }
            return $msg;
        }
    }

}

?>