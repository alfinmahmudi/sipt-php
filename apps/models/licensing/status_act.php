<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Status_act extends CI_Model {

    public function get_choice($pelaporan) {
        if ($this->newsession->userdata('_LOGGED')) {
            //$arrdata['direktorat'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT'","kode","uraian", TRUE);
            $sql = "SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT'";
            $arrdata['direktorat'] = $this->main->set_combobox($sql, "kode", "uraian", TRUE);
            $m = $this->main->get_result($sql);
            if ($m) {
                foreach ($sql->result_array() as $r) {
                    $dir[] = $r;
                }
            }
            //print_r($dir);die();
            $i = 0;
            foreach ($dir as $b) {
                $q = "select a.id,a.nama_izin, b.uraian from m_izin a left JOIN m_reff b on a.direktorat_id= b.kode where a.direktorat_id='" . $b['kode'] . "' AND b.jenis='DIREKTORAT' AND a.aktif = 1 ";
                $j = $this->main->get_result($q);
                if ($j) {
                    foreach ($q->result_array() as $w) {
                        $n[$b['kode']][] = $w;
                    }
                }
                $i++;
            }
            if ($pelaporan == '') {
                $arrdata['act'] = site_url('post/licensing/choice');
            } else {
                $arrdata['act'] = site_url('post/licensing/choice/pelaporan');
            }
            $arrdata['dir'] = $dir;
            $arrdata['izin'] = $n;
            $arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian", TRUE);
            $arrdata['garansi'] = $this->db->query("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_GARANSI'")->result_array();
            $queryNpwp = "select npwp from m_trader_refinasi";
            $sqlNpwp = $this->main->get_result($queryNpwp);
            if ($sqlNpwp) {
                foreach ($queryNpwp->result_array() as $v) {
                    $np[] = $v['npwp'];
                }
            }
            $arrdata['npwp'] = $np;
            return $arrdata;
        }
    }

    public function cek_edit() {
        if ($this->newsession->userdata('_LOGGED')) {
            $lemparan = $this->input->post('tb_chk');
            $data = explode('/', $lemparan[0]); //print_r($data);die();
            $query = "  SELECT COUNT(a.id) as res, a.nama_izin 
                        FROM VIEW_PERMOHONAN a 
                        WHERE a.id = '" . $data[3] . "' AND a.kd_izin = '" . $data[1] . "' AND a.direktorat_id = '" . $data[0] . "' AND a.status_id <> '0000' AND a.status_id <> '0501'
                        GROUP BY a.nama_izin";
            $result = $this->db->query($query)->row_array();
            if ($result['res'] != 0) {
                $msg = "MSG#Data Permohonan " . $result['nama_izin'] . " Tidak Dapat Di Edit. Karena Permohonan Sedang Dalam Proses#refresh";
            } else {
                // $msg = "MSG||YES||Data Permohonan " . $txt . " berhasil disimpan. Silahkan lanjutkan mengisi data penanggung jawab||" . site_url() . 'licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $idredir . '/' . $s;
                //redirect(site_url() . 'licensing/form/first/' . $data[0] . '/' . $data[1] . '/' . $data[2] . '/' . $data[3], 'refresh()');
                $msg = "MSG#Silahkan Menunggu#redirect#" . site_url() . 'licensing/form/first/' . $data[0] . '/' . $data[1] . '/' . $data[2] . '/' . $data[3];
            }
            return $msg;
        }
    }

    public function list_status() {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
            $arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
            // $arrizin = $this->main->array_cb("SELECT ")
            $query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
						no_aju + '<div>Tanggal kirim : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
						direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan',
						CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
						no_izin + '<div>' + CONVERT(VARCHAR(10),  tgl_izin, 105) + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 105),'-') + '</div>' AS 'Perizinan',
						status AS 'Status', created AS SORTTGL, 
						'<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log, kd_izin
						FROM VIEW_PERMOHONAN ";

            if ($this->newsession->userdata('role') == "99")
                $query .= "";
            else
                $query .= " WHERE trader_id = '" . $this->newsession->userdata('trader_id') . "'";



            $table->title("");
            $table->columns(array("id", "direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
                array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'", site_url() . 'licensing/preview/{IDX}'),
                "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
                "CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
                "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
                "status", "CONVERT(VARCHAR(10), tgl_aju, 120)",
                "<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>"));
            $this->newtable->width(array('Pengajuan' => 200, 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100, "Log" => 75));
            $this->newtable->search(array(array("no_aju", "Nomor Permohonan"),
                array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("no_izin", "No. Izin"),
                array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url() . 'get/cb/set_dokumen2/')),
                array("nama_izin", 'Dokumen Perizinan', array('ARRAY', array())),
                array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
                array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
            ));
            if ($this->input->post('data-post')) {
                // print_r($this->newtable->search(array(array("no_aju", "Nomor Permohonan"),
                // array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                // array("no_ijin", "No. Izin"),
                // array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                // array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url() . 'get/cb/set_dokumen/')),
                // array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
                // array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
                // array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
                // )));die();
            }
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/status");
            $table->orderby(8);
            $table->sortby("DESC");
            $table->keys(array("IDX"));
            $table->hiddens(array("id", "IDX", "SORTTGL", "kd_izin"));
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status_permohonan");

            if ($this->newsession->userdata('role') == '05') {
                $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/choice', '0', 'home', 'modal'),
                    'Edit' => array('POST', site_url() . 'licensing/check/ajax', '1', 'fa fa-edit', 'isngajax'),
                    // 'Edit' => array('GET', site_url() . 'licensing/check', '1', 'fa fa-edit', 'isngajax'),
                    'Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-eye'),
                    'Delete' => array('POST', site_url() . 'licensing/delete_status/ajax', 'N', 'fa fa-times', 'isngajax')
                ));
            } else {
                $table->menu(array('Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-pencil-square-o')));
            }
            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Permohonan');

            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    public function list_inbox() {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;

            $direktorat = "'" . join("','", $this->newsession->userdata('dir_id')) . "'";
            $kd_izin = "'" . join("','", $this->newsession->userdata('kode_izin')) . "'";
            $arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
            $arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
            if (implode($this->newsession->userdata('dir_id')) == "02") {
                $where = "status_id IN ('0100','0104')";
            } else {
                $where = "status_id = '0100'";
            }

            $query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
                        no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' + ur_status_dok AS 'Pengajuan',
                        direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
                        CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
                         no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
                        status AS 'Status', last_proses AS SORTTGL, '<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log, 
                        kd_izin,  case 
						when (((DATEDIFF(minute, last_proses, GETDATE()) - (DATEDIFF(WK, last_proses,  GETDATE()) * 2) - 
   CASE 
		WHEN DATEPART(DW, last_proses) = 1 
		THEN 1 
		ELSE 0 
	END 
	+ 
	CASE 
		WHEN DATEPART(DW,  GETDATE()) = 1 
		THEN 1 
		ELSE 0 
	END) / sla_waktu) * 100) > 100 or last_proses = NULL 
								then '<div class=\"pulse pulse5 bg-danger light-color\" \>  '+ convert(varchar(5),(DATEDIFF(day, last_proses, GETDATE()) - (DATEDIFF(WK, last_proses,  GETDATE()) * 2) - 
   CASE 
		WHEN DATEPART(DW, last_proses) = 1 
		THEN 1 
		ELSE 0 
	END 
	+ 
	CASE 
		WHEN DATEPART(DW,  GETDATE()) = 1 
		THEN 1 
		ELSE 0 
	END)) + ' Hari '+ convert(varchar(5),(DATEDIFF(minute, last_proses, GETDATE()) - (DATEDIFF(WK, last_proses,  GETDATE()) * 2) - 
   CASE 
		WHEN DATEPART(DW, last_proses) = 1 
		THEN 1 
		ELSE 0 
	END 
	+ 
	CASE 
		WHEN DATEPART(DW,  GETDATE()) = 1 
		THEN 1 
		ELSE 0 
	END)%24) + ' Jam  </div>' 
						when (((DATEDIFF(minute, last_proses, GETDATE()) - (DATEDIFF(WK, last_proses,  GETDATE()) * 2) - 
   CASE 
		WHEN DATEPART(DW, last_proses) = 1 
		THEN 1 
		ELSE 0 
	END 
	+ 
	CASE 
		WHEN DATEPART(DW,  GETDATE()) = 1 
		THEN 1 
		ELSE 0 
	END) / sla_waktu) * 100) > 75 and (((DATEDIFF(minute, last_proses, GETDATE()) - (DATEDIFF(WK, last_proses,  GETDATE()) * 2) - 
   CASE 
		WHEN DATEPART(DW, last_proses) = 1 
		THEN 1 
		ELSE 0 
	END 
	+ 
	CASE 
		WHEN DATEPART(DW,  GETDATE()) = 1 
		THEN 1 
		ELSE 0 
	END) / sla_waktu) * 100) <= 100 
								then '<div class=\"pulse pulse5 bg-warning light-color\" \>  '+ convert(varchar(5),(DATEDIFF(minute, last_proses, GETDATE()) - (DATEDIFF(WK, last_proses,  GETDATE()) * 2) - 
   CASE 
		WHEN DATEPART(DW, last_proses) = 1 
		THEN 1 
		ELSE 0 
	END 
	+ 
	CASE 
		WHEN DATEPART(DW,  GETDATE()) = 1 
		THEN 1 
		ELSE 0 
	END)/60) + ' Jam '+ convert(varchar(5),(DATEDIFF(minute, last_proses, GETDATE()) - (DATEDIFF(WK, last_proses,  GETDATE()) * 2) - 
   CASE 
		WHEN DATEPART(DW, last_proses) = 1 
		THEN 1 
		ELSE 0 
	END 
	+ 
	CASE 
		WHEN DATEPART(DW,  GETDATE()) = 1 
		THEN 1 
		ELSE 0 
	END)%60) + ' Menit  </div>' 
					    else '<div class=\"pulse pulse2 bg-success light-color\" \>  '+ convert(varchar(5),(DATEDIFF(minute, last_proses, GETDATE()) - (DATEDIFF(WK, last_proses,  GETDATE()) * 2) - 
   CASE 
		WHEN DATEPART(DW, last_proses) = 1 
		THEN 1 
		ELSE 0 
	END 
	+ 
	CASE 
		WHEN DATEPART(DW,  GETDATE()) = 1 
		THEN 1 
		ELSE 0 
	END)/60) + ' Jam '+ convert(varchar(5),(DATEDIFF(minute, last_proses, GETDATE()) - (DATEDIFF(WK, last_proses,  GETDATE()) * 2) - 
   CASE 
		WHEN DATEPART(DW, last_proses) = 1 
		THEN 1 
		ELSE 0 
	END 
	+ 
	CASE 
		WHEN DATEPART(DW,  GETDATE()) = 1 
		THEN 1 
		ELSE 0 
	END)%60) + ' Menit  </div>' end  as SLA
                        FROM VIEW_PERMOHONAN  LEFT JOIN  m_waktu_proses b on kd_izin = sla_izin and status_id = sla_status
                        WHERE " . $where . "  AND direktorat_id IN (" . $direktorat . ") AND kd_izin IN (" . $kd_izin . ")";
            //print_r($query);die();
            $table->title("");
            $table->columns(array("id", "direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
                array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'", site_url() . 'licensing/preview/{IDX}'),
                "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
                "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
                "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
                "status", "CONVERT(VARCHAR(10), tgl_aju, 120)",
                "<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>"));
            $this->newtable->width(array('Pengajuan' => 200, 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
            $this->newtable->search(array(array("nm_perusahaan", "Nama Perusahaan"), array("no_aju", "Nomor Permohonan"),
                array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("no_aju", "No. Izin"),
                array("nm_perusahaan", "Nama Perusahaan"),
                array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url() . 'get/cb/set_dokumen/')),
                array("nama_izin", 'Dokumen Perizinan', array('ARRAY', array())),
                array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
                array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
            ));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/inbox");
            $table->orderby('last_proses');
            $table->sortby("ASC");
            $table->keys(array("IDX"));
            $table->hiddens(array("id", "IDX", "SORTTGL", "kd_izin"));
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            $table->menu(array(//'Tambah' => array('GET', site_url() . 'licensing/manual', '0', 'home', 'modal'),
                // 'Edit' => array('GET', site_url() . 'licensing/form/first', '1', 'fa fa-edit'),
                'Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-eye')
                    // 'Delete' => array('POST', site_url().'licensing/delete_inbox/ajax','N', 'fa fa-times','isngajax')
            ));
            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Permohonan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    public function statusAll() {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;

            $direktorat = "'" . join("','", $this->newsession->userdata('dir_id')) . "'";
            $kd_izin = "'" . join("','", $this->newsession->userdata('kode_izin')) . "'";
            $where = "status_id != '0000'";
            $arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
            $arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
            $query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
			no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
			direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
			CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
			 no_izin + '<div>' + CONVERT(VARCHAR(10),  tgl_izin, 105) + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 105),'-') + '</div>' AS 'Perizinan',
			status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL, '<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log, kd_izin
                FROM VIEW_PERMOHONAN  WHERE " . $where . "  AND direktorat_id IN (" . $direktorat . ") AND kd_izin IN (" . $kd_izin . ")";
            //print_r($query);die();
            $table->title("");
            $table->columns(array("id", "direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
                array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'", site_url() . 'licensing/preview/{IDX}'),
                "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
                "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
                "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
                "status", "CONVERT(VARCHAR(10), tgl_aju, 120)",
                "<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>"));
            $this->newtable->width(array('Pengajuan' => 200, 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
            $this->newtable->search(array(array("no_aju", "Nomor Permohonan"),
                array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("no_aju", "No. Izin"),
                array("nm_perusahaan", "Nama Perusahaan"),
                array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url() . 'get/cb/set_dokumen2/')),
                array("nama_izin", 'Dokumen Perizinan', array('ARRAY', array())),
                array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
                array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
            ));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/all");
            $table->orderby(8);
            $table->sortby("DESC");
            $table->keys(array("IDX"));
            $table->hiddens(array("id", "IDX", "SORTTGL", "kd_izin"));
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            $table->menu(array(//'Tambah' => array('GET', site_url().'licensing/manual', '0', 'home', 'modal'),
                // 'Edit' => array('GET', site_url() . 'licensing/form/first', '1', 'fa fa-edit'),
                'Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-eye')
                    // 'Delete' => array('POST', site_url().'licensing/delete_inbox/ajax','N', 'fa fa-times','isngajax')
            ));
            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Permohonan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    public function list_report() {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
            $arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
            $query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 105),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 105),'-') + '</div>' AS 'Perizinan',
status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 105) AS SORTTGL, '<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log, kd_izin
FROM VIEW_PERMOHONAN ";
            if ($this->newsession->userdata('role') == "99") {
                $query .= "WHERE 1=1 ";
                $query .= " And status_id in ('1000')";
            } elseif ($this->newsession->userdata('role') == "05") {
                $query .= " WHERE trader_id = '" . $this->newsession->userdata('trader_id') . "'";
                $query .= " And status_id in ('1000')";
            } elseif ($this->newsession->userdata('role') == "01") {
                $dir = join("','", $this->newsession->userdata('dir_id'));
                $query .= " WHERE direktorat_id='" . $dir . "' AND status_id in ('1000')";
            }
            //print_r($query);die();
            $table->title("");
            $table->columns(array("id", "direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
                array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'", site_url() . 'licensing/report/{IDX}'),
                "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
                "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
                "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
                "status", "CONVERT(VARCHAR(10), tgl_aju, 120)",
                "<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>"));
            $this->newtable->width(array('Pengajuan' => 200, 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
            $this->newtable->search(array(array("no_aju", "Nomor Permohonan"), array("nm_perusahaan", "Nama Perusahaan"),
                array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("no_izin", "No. Izin"),
                array("CONVERT(VARCHAR(10), tgl_izin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url() . 'get/cb/set_dokumen/')),
                array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array()))
            ));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/report");
            $table->orderby(8);
            $table->sortby("ASC");
            $table->keys(array("IDX"));
            $table->hiddens(array("id", "IDX", "SORTTGL", "kd_izin"));
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status" . rand());
            // $table->menu(array(//'Tambah' => array('GET', site_url() . 'licensing/choice/pelaporan', '0', 'home', 'modal'),
            //     'Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-pencil-square-o'),
            //     'Pelaporan' => array('GET', site_url() . 'licensing/report', '1', 'fa fa-pencil-square-o')));

            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Laporan Perizinan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    public function list_proccess() {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;

            $direktorat = "'" . join("','", $this->newsession->userdata('dir_id')) . "'";
            $kd_izin = "'" . join("','", $this->newsession->userdata('kode_izin')) . "'";

            $arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
            $arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
            $query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL, '<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log, kd_izin,  
    case when ((DATEDIFF(minute, updated, GETDATE()) / sla_waktu) * 100) > 100 or updated = NULL 
								then '<div class=\"pulse pulse5 bg-danger light-color\" \>  '+ convert(varchar(5),DATEDIFF(minute,  updated, GETDATE())/60) + ' Jam '+ convert(varchar(5),DATEDIFF(minute,  updated, GETDATE())%60) + ' Menit  </div>' 
							when ((DATEDIFF(minute, updated, GETDATE()) / sla_waktu) * 100) > 75 and ((DATEDIFF(minute, updated, GETDATE()) / sla_waktu) * 100) <= 100 
								then '<div class=\"pulse pulse5 bg-warning light-color\" \>  '+ convert(varchar(5),DATEDIFF(minute,  updated, GETDATE())/60) + ' Jam '+ convert(varchar(5),DATEDIFF(minute,  updated, GETDATE())%60) + ' Menit  </div>' 
								else '<div class=\"pulse pulse2 bg-success light-color\" \>  '+ convert(varchar(5),DATEDIFF(minute,  updated, GETDATE())/60) + ' Jam '+ convert(varchar(5),DATEDIFF(minute,  updated, GETDATE())%60) + ' Menit  </div>' end  as SLA
                        FROM VIEW_PERMOHONAN 
                        LEFT JOIN  m_waktu_proses b on kd_izin = sla_izin and status_id = sla_status
WHERE direktorat_id IN (" . $direktorat . ") AND kd_izin IN (" . $kd_izin . ")";

            if ($this->newsession->userdata('role') == '07') { #Kasie Subdit
                $query .= $this->main->find_where($query);
                if (implode($this->newsession->userdata("dir_id")) == "02") {
                    $query .= "status_id IN ('0700','0706')";
                } else {
                    $query .= "status_id = '0700'";
                }
            } else if ($this->newsession->userdata('role') == '06') { #Kasubdit
                $query .= $this->main->find_where($query);
                if (implode($this->newsession->userdata("dir_id")) == "02") {
                    $query .= "status_id IN ('0600','0602')";
                } else {
                    $query .= "status_id = '0600'";
                }
            } else if ($this->newsession->userdata('role') == '02') { #Direktur
                $query .= $this->main->find_where($query);
                if (implode($this->newsession->userdata("dir_id")) == "02") {
                    $query .= "status_id IN ('0200','0202')";
                } else {
                    $query .= "status_id = '0200'";
                }
            }

            $table->title("");
            $table->columns(array("id", "direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
                array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'", site_url() . 'licensing/preview/{IDX}'),
                "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
                "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
                "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
                "status", "CONVERT(VARCHAR(10), tgl_aju, 120)",
                "<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>"));
            $this->newtable->width(array('Pengajuan' => 200, 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
            $this->newtable->search(array(array("nm_perusahaan", "Nama Perusahaan"), array("no_aju", "Nomor Permohonan"),
                array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("no_aju", "No. Izin"),
                array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url() . 'get/cb/set_dokumen/')),
                array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
                array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
                array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
            ));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/proccess");
            $table->orderby(8);
            $table->sortby("ASC");
            $table->keys(array("IDX"));
            $table->hiddens(array("id", "IDX", "SORTTGL", "kd_izin"));
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            if ($this->newsession->userdata('role') == '05') {
                $table->menu(array('Edit' => array('GET', site_url() . 'licensing/form/first', '1', 'fa fa-edit'),
                    'Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-eye')));
            } else {
                $table->menu(array('Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-eye')));
            }
            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Permohonan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    public function list_rollback() {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;

            $direktorat = "'" . join("','", $this->newsession->userdata('dir_id')) . "'";
            $kd_izin = "'" . join("','", $this->newsession->userdata('kode_izin')) . "'";

            $arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
            $arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
            $query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
					no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
					direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
					CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
					no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
					status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL,'<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log, kd_izin,  case when ((DATEDIFF(minute, updated, GETDATE()) / sla_waktu) * 100) > 100 or updated = NULL 
                                then '<div class=\"pulse pulse5 bg-danger light-color\" \>  '+ convert(varchar(3),DATEDIFF(minute,  updated, GETDATE())) + ' Menit </div>' 
                            when ((DATEDIFF(minute, updated, GETDATE()) / sla_waktu) * 100) > 75 and ((DATEDIFF(minute, updated, GETDATE()) / sla_waktu) * 100) <= 100 
                                then '<div class=\"pulse pulse5 bg-warning light-color\" \>  '+ convert(varchar(3),DATEDIFF(minute,  updated, GETDATE())) + ' Menit </div>' 
                                else '<div class=\"pulse pulse2 bg-success light-color\" \>  '+ convert(varchar(3),DATEDIFF(minute,  updated, GETDATE())) + ' Menit </div>' end  as SLA
                        FROM VIEW_PERMOHONAN 
                        LEFT JOIN  m_waktu_proses b on kd_izin = sla_izin and status_id = sla_status
                    WHERE direktorat_id IN (" . $direktorat . ") AND kd_izin IN (" . $kd_izin . ")";

            if ($this->newsession->userdata('role') == '01') { #Pemroses Subdit
                $query .= $this->main->find_where($query);
                $query .= " status_id IN('0105') ";
                $query .= " AND pemroses = " . $this->newsession->userdata('id');
            } else if ($this->newsession->userdata('role') == '07') { #Kasie
                $query .= $this->main->find_where($query);
                $query .= " status_id IN('0701') ";
            } else if ($this->newsession->userdata('role') == '06') { #Kasubdit
                $query .= $this->main->find_where($query);
                $query .= " status_id = '0601' ";
            }
            //print_r($query);die();
            $table->title("");
            $table->columns(array("id", "direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
                array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'", site_url() . 'licensing/preview/{IDX}'),
                "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
                "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
                "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
                "status", "CONVERT(VARCHAR(10), tgl_aju, 120)",
                "<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>"));
            $this->newtable->width(array('Pengajuan' => 200, 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
            $this->newtable->search(array(array("no_aju", "Nomor Permohonan"),
                array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("no_aju", "No. Izin"),
                array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url() . 'get/cb/set_dokumen/')),
                array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
                array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
                array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
            ));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/proccess");
            $table->orderby(8);
            $table->sortby("ASC");
            $table->keys(array("IDX"));
            $table->hiddens(array("id", "IDX", "SORTTGL", "kd_izin"));
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            $table->menu(array('Edit' => array('GET', site_url() . 'licensing/form/first', '1', 'fa fa-edit'),
                'Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-eye')));
            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Permohonan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    public function list_issued() {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;

            $direktorat = "'" . join("','", $this->newsession->userdata('dir_id')) . "'";
            $kd_izin = "'" . join("','", $this->newsession->userdata('kode_izin')) . "'";

            $arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
            $arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
            $query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen', 
no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL, '<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log, kd_izin,  case when ((DATEDIFF(minute, updated, GETDATE()) / sla_waktu) * 100) > 100 or updated = NULL 
                                then '<div class=\"pulse pulse5 bg-danger light-color\" \>  '+ convert(varchar(3),DATEDIFF(minute,  updated, GETDATE())) + ' Menit </div>' 
                            when ((DATEDIFF(minute, updated, GETDATE()) / sla_waktu) * 100) > 75 and ((DATEDIFF(minute, updated, GETDATE()) / sla_waktu) * 100) <= 100 
                                then '<div class=\"pulse pulse5 bg-warning light-color\" \>  '+ convert(varchar(3),DATEDIFF(minute,  updated, GETDATE())) + ' Menit </div>' 
                                else '<div class=\"pulse pulse2 bg-success light-color\" \>  '+ convert(varchar(3),DATEDIFF(minute,  updated, GETDATE())) + ' Menit </div>' end  as SLA
                        FROM VIEW_PERMOHONAN 
                        LEFT JOIN  m_waktu_proses b on kd_izin = sla_izin and status_id = sla_status
WHERE status_id = '0102' AND direktorat_id IN (" . $direktorat . ") AND kd_izin IN (" . $kd_izin . ")";
            $table->title("");
            $table->columns(array("id", "direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
                array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'", site_url() . 'licensing/preview/{IDX}'),
                "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
                "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
                "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
                "status", "CONVERT(VARCHAR(10), tgl_aju, 120)",
                "<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>"));
            $this->newtable->width(array('Pengajuan' => 200, 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
            $this->newtable->search(array(array("no_aju", "Nomor Permohonan"),
                array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("no_aju", "No. Izin"),
                array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url() . 'get/cb/set_dokumen/')),
                array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
                array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
                array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
            ));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/issued");
            $table->orderby(8);
            $table->sortby("ASC");
            $table->keys(array("IDX"));
            $table->hiddens(array("id", "IDX", "SORTTGL", "kd_izin"));
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            $table->menu(array('Edit' => array('GET', site_url() . 'licensing/form/first', '1', 'fa fa-edit'),
                'Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-pencil-square-o')));
            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Permohonan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    public function list_released() {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
            $arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");

            $direktorat = "'" . join("','", $this->newsession->userdata('dir_id')) . "'";
            $kd_izin = "'" . join("','", $this->newsession->userdata('kode_izin')) . "'";

            $query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' + ur_status_dok AS 'Pengajuan',
direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 105),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 105),'-') + '</div>' AS 'Perizinan',
status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL, '<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log, kd_izin
FROM VIEW_PERMOHONAN WHERE status_id = '1000' AND direktorat_id IN (" . $direktorat . ") AND kd_izin IN (" . $kd_izin . ")";
            $table->title("");
            $table->columns(array("id", "direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
                array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'", site_url() . 'licensing/preview/{IDX}'),
                "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
                "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
                "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
                "status", "CONVERT(VARCHAR(10), tgl_aju, 120)",
                "<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>"));
            $this->newtable->width(array('Pengajuan' => 200, 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
            $this->newtable->search(array(array("no_aju", "Nomor Permohonan"),
                array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("no_aju", "No. Izin"),
                array("nm_perusahaan", "Nama Perusahaan"),
                array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url() . 'get/cb/set_dokumen/')),
                array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
                array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
                array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
            ));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/released");
            $table->orderby(8);
            $table->sortby("ASC");
            $table->keys(array("IDX"));
            $table->hiddens(array("id", "IDX", "SORTTGL", "kd_izin"));
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            $table->menu(array('Edit' => array('GET', site_url() . 'licensing/form/first', '1', 'fa fa-edit'),
                'Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-eye')));
            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Permohonan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function tembusan($izin_id, $permohonan_id) {
        $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $izin_id . "'";
        $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
        $arrdata['nama_izin'] = $nama_izin;
        $arrdata['act'] = site_url('post/licensing/tembusan/save');
        $arrdata['izin_id'] = $izin_id;
        $arrdata['permohonan_id'] = $permohonan_id;
        $query_tembusan = "SELECT * FROM m_tembusan WHERE kd_izin = " . $izin_id . " AND permohonan_id =" . hashids_decrypt($permohonan_id, _HASHIDS_, 9);
        $data = $this->main->get_result($query_tembusan);
        if ($data) {
            foreach ($query_tembusan->result_array() as $row) {
                $arrdata['tembusan'][] = $row;
                $arrdata['act'] = site_url('post/licensing/tembusan/update');
            }
        }
        return $arrdata;
    }

    function set_tembusan($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }

            $msg = "MSG||NO||Data Tembusan gagal disimpan";
            $res = FALSE;
            $arrdata = $this->main->post_to_query($this->input->post('data'));
            $arrtembusan = $this->input->post('tembusan');
            $arrkeys = array_keys($arrtembusan);
            $kd_izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
            $permohonan_id = hashids_decrypt($arrdata['permohonan_id'], _HASHIDS_, 9);
            $sql_aju = "SELECT no_aju FROM dbo.view_permohonan WHERE id = " . $permohonan_id . " and kd_izin = " . $kd_izin;
            $no_aju = $this->main->get_uraian($sql_aju, "no_aju");

            if ($act == "save") {
                for ($s = 0; $s < count($_POST['tembusan']['urutan']); $s++) {
                    $tembusan = array('kd_izin' => $kd_izin,
                        'permohonan_id' => $permohonan_id,
                        'created_user' => $this->newsession->userdata('username'),
                        'created' => 'GETDATE()');
                    for ($j = 0; $j < count($arrkeys); $j++) {
                        $tembusan [$arrkeys[$j]] = $arrtembusan[$arrkeys[$j]][$s];
                    }
                    unset($tembusan['id']); //print_r($tembusan);
                    $this->db->insert('m_tembusan', $tembusan);
                }//die();
                if ($this->db->affected_rows() > 0) {
                    $res = TRUE;
                    $logu = array('aktifitas' => 'Menambahkan daftar tembusan untuk nomor aju ' . $no_aju,
                        'url' => '{c}' . site_url() . 'post/licensing/tembusan/save' . ' {m} models/licensing/status_act {f} set_tembusan($act, $isajax)');
                    $this->main->set_activity($logu);

                    $msg = "MSG||YES||Data Tembusan Berhasil disimpan||REFRESH";
                }
                if ($this->db->trans_status() === FALSE || !$res) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } elseif ($act == "update") {
                $this->db->where('kd_izin', $kd_izin);
                $this->db->where('permohonan_id', $permohonan_id);
                $this->db->delete('m_tembusan');
                if ($this->db->affected_rows() > 0) {
                    for ($s = 0; $s < count($_POST['tembusan']['urutan']); $s++) {
                        $tembusan = array('kd_izin' => $kd_izin,
                            'permohonan_id' => $permohonan_id,
                            'created_user' => $this->newsession->userdata('username'),
                            'created' => 'GETDATE()');
                        for ($j = 0; $j < count($arrkeys); $j++) {
                            $tembusan [$arrkeys[$j]] = $arrtembusan[$arrkeys[$j]][$s];
                        }
                        unset($tembusan['id']);
                        $this->db->insert('m_tembusan', $tembusan);
                    }
                    if ($this->db->affected_rows() > 0) {
                        $res = TRUE;
                        $logu = array('aktifitas' => 'Memperbaharui daftar tembusan untuk nomor aju ' . $no_aju,
                            'url' => '{c}' . site_url() . 'post/licensing/tembusan/update' . ' {m} models/licensing/status_act {f} set_tembusan($act, $isajax)');
                        $this->main->set_activity($logu);

                        $msg = "MSG||YES||Data Tembusan Berhasil diperbaharui||REFRESH";
                    }
                    if ($this->db->trans_status() === FALSE || !$res) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
                return $msg;
            }
        }
    }

    public function del_status($id) {
        //print_r($id);die();
        /*
          $exp[0]->Direktorat_id
          $exp[1]->kode izin
          $exp[2]->tipe_permohonan
          $exp[3]->id
         */
        $this->db->trans_begin();
        $i = 0;
        foreach ($id as $data) {
            $tabel = "";
            $exp = explode("/", $data);
            $arrstpw = array("17", "18", "19", "20");
            $arrsiupmb = array("3", "4", "5", "6", "13");

            if (in_array($exp[1], $arrstpw)) {
                $tabel = "t_stpw";
            } else if (in_array($exp[1], $arrsiupmb)) {
                $tabel = "t_siubmb";
            } elseif ($exp[1] == "7") {
                $tabel = "t_pkapt";
            } elseif ($exp[1] == "8") {
                $tabel = "t_pgapt";
            } elseif ($exp[1] == "9") {
                $tabel = "t_sppgapt";
            } elseif ($exp[1] == "10") {
                $tabel = "t_sppgrapt";
            } elseif ($exp[1] == "11") {
                $tabel = "t_siupbb";
            } elseif ($exp[1] == "12") {
                $tabel = "t_siup_agen";
            } elseif ($exp[1] == "2") {
                $tabel = "t_siup4";
            } elseif ($exp[1] == "1") {
                $tabel = "t_siujs";
            } elseif ($exp[1] == "14") {
                $tabel = "t_garansi";
            } elseif ($exp[1] == "22") {
                $tabel = "t_bapok";
            } elseif (($exp[1] == "15") || ($exp[1] == "16")) {
                $tabel = "t_pameran";
            } else {
                break;
            }
            $status = $this->main->get_uraian("select status from " . $tabel . " where kd_izin='" . $exp[1] . "' AND id='" . $exp[3] . "'", "status");
            if ($status == "0000") {
                $this->db->where('kd_izin', $exp[1]);
                $this->db->where('status', $status);
                $this->db->where('id', $exp[3]);
                $this->db->delete($tabel);
            } else {
                $i++;
                break;
            }
        }

        if ($this->db->trans_status() === FALSE || $i > 0) {
            $this->db->trans_rollback();
            $msg = "MSG#Proses Gagal#refresh";
        } else {
            $this->db->trans_commit();
            $msg = "MSG#Proses Berhasil#refresh";
        }
        return $msg;
    }

    public function get_notif() {
        $get_kdizin = "SELECT izin_id FROM t_user_role WHERE user_id = '" . $this->newsession->userdata('id') . "' ";
        $arrkdizin = $this->db->query($get_kdizin)->result_array();
        foreach ($arrkdizin as $key) {
            $kdizin[] = $key['izin_id'];
        }
        $kdizin = implode(",", $kdizin);
        $notif = "SELECT top 3 CONCAT('licensing/preview/',a.direktorat_id,'/',a.kd_izin,'/',a.tipe_permohonan,'/',a.id) as 'path', a.nm_perusahaan, b.aka, (CASE WHEN DATEDIFF(MONTH, a.updated, GETDATE()) = 0 THEN 
            (CASE WHEN DATEDIFF(DAY, a.updated, GETDATE()) > 0 THEN CAST(DATEDIFF(DAY, a.updated, GETDATE()) AS VARCHAR) + ' Hari, ' ELSE '' END) + (CASE WHEN DATEDIFF(HOUR, a.updated, GETDATE()) > 0 
            THEN CAST(DATEDIFF(HOUR, a.updated, GETDATE())%24 AS VARCHAR) + ' Jam, ' ELSE '' END) + 
            CAST(DATEDIFF(MINUTE, a.updated, GETDATE())%60 AS VARCHAR) + ' Menit Yang Lalu' 
            ELSE (CONVERT(VARCHAR(10), a.updated, 105) + ' ' +  CONVERT(VARCHAR(8), a.updated, 108)) END) AS waktu
            FROM VIEW_PERMOHONAN a
            LEFT JOIN m_izin b ON b.id = a.kd_izin
            WHERE a.kd_izin IN(" . $kdizin . ") AND a.status_id = '" . $this->newsession->userdata('role') . "00'
            ORDER BY a.updated DESC";
        $arrnotif = $this->db->query($notif)->result_array();
        $all = "SELECT count(*) as 'jml' 
                FROM view_permohonan 
                WHERE kd_izin IN(" . $kdizin . ") AND status_id = '" . $this->newsession->userdata('role') . "00'";
        $resall = $this->db->query($all)->row_array();
        $arrdata = array(
            'data' => $arrnotif,
            'jml' => $resall['jml']
        );
        return $arrdata;
    }

    public function get_table() {
        $jns = $this->input->post('jns');
        $get_kdizin = "SELECT izin_id FROM t_user_role WHERE user_id = '" . $this->newsession->userdata('id') . "' ";
        $arrkdizin = $this->db->query($get_kdizin)->result_array();
        foreach ($arrkdizin as $key) {
            $kdizin[] = $key['izin_id'];
        }
        $kdizin = implode(",", $kdizin);
        $get_next = "SELECT a.tolak, a.setujui 
                    FROM m_proses a 
                    WHERE a.posisi = '" . $this->newsession->userdata('role') . "00' AND a.kd_izin IN(" . $kdizin . ")";
        $arrstatus = $this->db->query($get_next)->row_array();
        if ($jns == 'verif') {
            $addwhr = "AND a.status = '" . $arrstatus['setujui'] . "' AND a.username like '" . $this->newsession->userdata('username') . "'";
        } elseif ($jns == 'tolak') {
            $addwhr = "AND a.status = '" . $arrstatus['tolak'] . "' AND a.username like '" . $this->newsession->userdata('username') . "'";
        }

        if ($jns == 'baru') {
            $notif = "SELECT top 10 CONCAT('licensing/preview/',a.direktorat_id,'/',a.kd_izin,'/01/',a.id) as 'path', a.nm_perusahaan, b.aka, (CASE WHEN DATEDIFF(MONTH, a.updated, GETDATE()) = 0 THEN 
            (CASE WHEN DATEDIFF(DAY, a.updated, GETDATE()) > 0 THEN CAST(DATEDIFF(DAY, a.updated, GETDATE()) AS VARCHAR) + ' Hari, ' ELSE '' END) + (CASE WHEN DATEDIFF(HOUR, a.updated, GETDATE()) > 0 
            THEN CAST(DATEDIFF(HOUR, a.updated, GETDATE())%24 AS VARCHAR) + ' Jam, ' ELSE '' END) + 
            CAST(DATEDIFF(MINUTE, a.updated, GETDATE())%60 AS VARCHAR) + ' Menit Yang Lalu' 
            ELSE (CONVERT(VARCHAR(10), a.updated, 105) + ' ' +  CONVERT(VARCHAR(8), a.updated, 108)) END) AS waktu,
            '<button data-url = \"" . site_url() . "get/popup/log/' + CAST(a.id AS VARCHAR) + '/' + CAST(a.kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log
            FROM VIEW_PERMOHONAN a
            LEFT JOIN m_izin b ON b.id = a.kd_izin
            WHERE a.kd_izin IN(" . $kdizin . ") AND a.status_id = '" . $this->newsession->userdata('role') . "00'
            ORDER BY a.updated DESC";
        } else {
            $notif = "SELECT top 10 b.nm_perusahaan, b.nama_izin as 'aka', CONCAT('licensing/preview/',b.direktorat_id,'/',b.kd_izin,'/01/',b.id) 
                    as 'path', (CASE WHEN DATEDIFF(MONTH, a.created, GETDATE()) = 0 THEN 
                    (CASE WHEN DATEDIFF(DAY, a.created, GETDATE()) > 0 THEN CAST(DATEDIFF(DAY, a.created, GETDATE()) AS VARCHAR) + ' Hari, ' ELSE '' END) + (CASE WHEN DATEDIFF(HOUR, a.created, GETDATE()) > 0 
                    THEN CAST(DATEDIFF(HOUR, a.created, GETDATE())%24 AS VARCHAR) + ' Jam, ' ELSE '' END) + 
                    CAST(DATEDIFF(MINUTE, a.created, GETDATE())%60 AS VARCHAR) + ' Menit Yang Lalu' 
                    ELSE (CONVERT(VARCHAR(10), a.created, 105) + ' ' +  CONVERT(VARCHAR(8), a.created, 108)) END) AS waktu, 
                    '<button data-url = \"" . site_url() . "get/popup/log/' + CAST(b.id AS VARCHAR) + '/' + CAST(b.kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log
                    FROM t_log_izin a
                    LEFT JOIN VIEW_PERMOHONAN b ON b.id = a.permohonan_id AND b.kd_izin = a.kd_izin
                    WHERE a.kd_izin IN(" . $kdizin . ") " . $addwhr . "
                    ORDER BY a.created DESC";
        }
        // print_r($notif);die();
        // '<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log,
        //print_r($notif);die();
        $all = "SELECT count(a.id) as 'jml' 
                FROM t_log_izin a
                LEFT JOIN VIEW_PERMOHONAN b ON b.id = a.permohonan_id
                WHERE a.status = '" . $stat . "' AND a.username like '" . $this->newsession->userdata('username') . "'  
                AND b.kd_izin IN(" . $kdizin . ")";
        $arrnotif = $this->db->query($notif)->result_array();
        $resall = $this->db->query($all)->row_array();
        $arrdata = array(
            'data' => $arrnotif,
            'jml' => $resall['jml']
        );
        return $arrdata;
    }

    public function get_petugas() {
        $status = $this->input->post('status');
        $notif = "SELECT TOP 10 a.id, CONCAT(c.uraian, '. ', a.nm_perusahaan) as nm_perusahaan, a.almt_perusahaan, a.usr_aktivasi, 
                (CASE WHEN DATEDIFF(MONTH, a.created, GETDATE()) = 0 THEN 
                (CASE WHEN DATEDIFF(DAY, a.created, GETDATE()) > 0 THEN CAST(DATEDIFF(DAY, a.created, GETDATE()) AS VARCHAR) + ' Hari, ' ELSE '' END) + (CASE WHEN DATEDIFF(HOUR, a.created, GETDATE()) > 0 
                THEN CAST(DATEDIFF(HOUR, a.created, GETDATE())%24 AS VARCHAR) + ' Jam, ' ELSE '' END) + 
                CAST(DATEDIFF(MINUTE, a.created, GETDATE())%60 AS VARCHAR) + ' Menit Yang Lalu' 
                ELSE (CONVERT(VARCHAR(10), a.created, 105) + ' ' +  CONVERT(VARCHAR(8), a.created, 108)) END) AS created, 
                (CASE WHEN DATEDIFF(MONTH, a.tgl_aktifasi, GETDATE()) = 0 THEN 
                (CASE WHEN DATEDIFF(DAY, a.tgl_aktifasi, GETDATE()) > 0 THEN CAST(DATEDIFF(DAY, a.tgl_aktifasi, GETDATE()) AS VARCHAR) + ' Hari, ' ELSE '' END) + (CASE WHEN DATEDIFF(HOUR, a.tgl_aktifasi, GETDATE()) > 0 
                THEN CAST(DATEDIFF(HOUR, a.tgl_aktifasi, GETDATE())%24 AS VARCHAR) + ' Jam, ' ELSE '' END) + 
                CAST(DATEDIFF(MINUTE, a.tgl_aktifasi, GETDATE())%60 AS VARCHAR) + ' Menit Yang Lalu' 
                ELSE (CONVERT(VARCHAR(10), a.tgl_aktifasi, 105) + ' ' +  CONVERT(VARCHAR(8), a.tgl_aktifasi, 108)) END) AS tgl_aktivasi, a.keterangan as ket
                FROM m_trader a
                LEFT JOIN m_reff b ON b.kode = a.tipe_registrasi AND b.jenis = 'TIPE_REGISTRASI'
                LEFT JOIN m_reff c ON c.kode = a.tipe_perusahaan AND c.jenis = 'KDTIPE_PERUSAHAAN'
                WHERE a.status = '" . $status . "'
                ORDER BY a.created DESC";
        $arrnotif = $this->db->query($notif)->result_array();
        foreach ($arrnotif as $key) {
            $arrid[] = hashids_encrypt($key['id'], _HASHIDS_, 9);
        }
        $arrdata = array(
            'id' => $arrid,
            'data' => $arrnotif
        );
        return $arrdata;
    }

    public function get_jml() {
        $verif = array('99', '09');
        if (in_array($this->newsession->userdata('role'), $verif)) {
            $sql = "SELECT sum(case when a.status = '00' then 1 else 0 end ) as 'baru',
                    sum(case when a.status = '01' then 1 else 0 end ) as 'terima',
                    sum(case when a.status = '02' then 1 else 0 end ) as 'tolak',
                    sum(case when a.status = '03' then 1 else 0 end ) as 'perubahan',
                    sum(case when a.status = '04' then 1 else 0 end ) as 'tdk_valid'
                    from m_trader a";
            $arrdata = $this->db->query($sql)->row_array();
        } else {
            $get_kdizin = "SELECT izin_id FROM t_user_role WHERE user_id = '" . $this->newsession->userdata('id') . "' ";
            $arrkdizin = $this->db->query($get_kdizin)->result_array();
            foreach ($arrkdizin as $key) {
                $kdizin[] = $key['izin_id'];
            }
            $kdizin = implode(",", $kdizin);
            $all = "SELECT sum(case when ((DATEDIFF(minute, a.last_proses, GETDATE()) / b.sla_waktu) * 100) > 100 then 1 else 0 end ) as 'lebih_sla',
                sum(case when ((DATEDIFF(minute, a.last_proses, GETDATE()) / b.sla_waktu) * 100) <= 100 then 1 else 0 end ) as 'mendekati_sla',
                sum(case when ((DATEDIFF(minute, a.last_proses, GETDATE()) / b.sla_waktu) * 100) = 100 then 1 else 0 end ) as 'sesuai_sla'
                from VIEW_PERMOHONAN a
                LEFT JOIN m_waktu_proses b ON b.sla_izin = a.kd_izin AND a.status_id = b.sla_status
                WHERE a.kd_izin IN(" . $kdizin . ") AND a.status_id = '" . $this->newsession->userdata('role') . "00'";
            $arrdata = $this->db->query($all)->row_array();
        }
        return $arrdata;
    }

    public function get_regis() {
        $notif = "SELECT top 3 a.id, CONCAT(b.uraian, '. ', a.nm_perusahaan) as nm_perusahaan, c.uraian as tipe, (CASE WHEN DATEDIFF(MONTH, a.created, GETDATE()) = 0 THEN (CASE WHEN DATEDIFF(DAY, a.created, GETDATE()) > 0 THEN CAST(DATEDIFF(DAY, a.created, GETDATE()) AS VARCHAR) + ' Hari, ' ELSE '' END) + (CASE WHEN DATEDIFF(HOUR, a.created, GETDATE()) > 0 THEN CAST(DATEDIFF(HOUR, a.created, GETDATE())%24 AS VARCHAR) + ' Jam, ' ELSE '' END) + CAST(DATEDIFF(MINUTE, a.created, GETDATE())%60 AS VARCHAR) + ' Menit Yang Lalu' ELSE (CONVERT(VARCHAR(10), a.created, 105) + ' ' + CONVERT(VARCHAR(8), a.created, 108)) END) AS waktu
                FROM m_trader a
                LEFT JOIN m_reff b ON b.kode = a.tipe_perusahaan AND b.jenis = 'KDTIPE_PERUSAHAAN'
                LEFT JOIN m_reff c ON c.kode = a.tipe_registrasi AND c.jenis = 'TIPE_REGISTRASI'
                WHERE a.status = '00'";
        $arrnotif = $this->db->query($notif)->result_array();
        foreach ($arrnotif as $key) {
            $arrid[] = hashids_encrypt($key['id'], _HASHIDS_, 9);
        }
        $all = "SELECT count(*) as 'jml' 
                FROM m_trader a
                LEFT JOIN m_reff b ON b.kode = a.tipe_perusahaan AND b.jenis = 'KDTIPE_PERUSAHAAN'
                LEFT JOIN m_reff c ON c.kode = a.tipe_registrasi AND c.jenis = 'TIPE_REGISTRASI'
                WHERE a.status = '00'";
        $resall = $this->db->query($all)->row_array();
        $arrdata = array(
            'id' => $arrid,
            'data' => $arrnotif,
            'jml' => $resall['jml']
        );
        return $arrdata;
    }

    public function del_inbox($id) {

        /*
          $exp[0]->Direktorat_id
          $exp[1]->kode izin
          $exp[2]->tipe_permohonan
          $exp[3]->id
         */
        $this->db->trans_begin();
        $i = 0;
        foreach ($id as $data) {
            $tabel = "";
            $exp = explode("/", $data);
            $arrstpw = array("17", "18", "19", "20");
            $arrsiupmb = array("3", "4", "5", "6", "13");

            if (in_array($exp[1], $arrstpw)) {
                $tabel = "t_stpw";
            } else if (in_array($exp[1], $arrsiupmb)) {
                $tabel = "t_siubmb";
            } elseif ($exp[1] == "7") {
                $tabel = "t_pkapt";
            } elseif ($exp[1] == "8") {
                $tabel = "t_pgapt";
            } elseif ($exp[1] == "9") {
                $tabel = "t_sppgapt";
            } elseif ($exp[1] == "11") {
                $tabel = "t_siupbb";
            } elseif ($exp[1] == "10") {
                $tabel = "t_sppgrap";
            } elseif ($exp[1] == "12") {
                $tabel = "t_siup_agen";
            } elseif ($exp[1] == "2") {
                $tabel = "t_siup4";
            } elseif ($exp[1] == "1") {
                $tabel = "t_siujs";
            } elseif ($exp[1] == "14") {
                $tabel = "t_garansi";
            } elseif (($exp[1] == "15") || ($exp[1] == "16")) {
                $tabel = "t_pameran";
            } else {
                break;
            }
            $no_aju = $this->main->get_uraian("select no_aju from view_permohonan where kd_izin='" . $exp[1] . "' AND id='" . $exp[3] . "' AND tipe_permohonan='" . $exp[2] . "' AND direktorat_id='" . $exp[0] . "'", "no_aju");
            $source = $this->main->get_uraian("select source from " . $tabel . " where kd_izin='" . $exp[1] . "' AND no_aju='" . $no_aju . "' AND source='1'", "source");
            //print_r($no_aju.$source);die();
            if ($source == "1") {
                $this->db->where('kd_izin', $exp[1]);
                $this->db->where('source', $source);
                $this->db->where('no_aju', $no_aju);
                $this->db->where('tipe_permohonan', $exp[2]);
                $this->db->delete($tabel);
                if ($this->db->affected_rows() != 1) {
                    $i++;
                    break;
                }
            } else {
                $i++;
                break;
            }
        }

        if ($this->db->trans_status() === FALSE || $i > 0) {
            $this->db->trans_rollback();
            $msg = "MSG#Proses Gagal#refresh";
        } else {
            $this->db->trans_commit();
            $msg = "MSG#Proses Berhasil#refresh";
        }
        return $msg;
    }

}

?>