<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Status_act extends CI_Model{
	public function get_choice(){
		if($this->newsession->userdata('_LOGGED')){
			$arrdata['direktorat'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT'","kode","uraian", TRUE);
			$arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'","kode","uraian", TRUE);
			return $arrdata;
		}
	}
	
	public function list_status(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
			$arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
			$arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
			$arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
			$query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan',
CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL, '[Log]' AS Log, kd_izin
FROM VIEW_PERMOHONAN ";
			if($this->newsession->userdata('role') == "99")
			$query .= "";
			else
			$query .= " WHERE trader_id = '".$this->newsession->userdata('trader_id')."'";
			
			$table->title("");
			$table->columns(array("id","direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
								  array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'",site_url().'licensing/preview/{IDX}'),
								  "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
								  "CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
								  "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
								  "status","CONVERT(VARCHAR(10), tgl_aju, 120)",
								  array("'[Log]'",site_url().'licensing/popup_log/{kd_izin}/{id}')));
			$this->newtable->width(array('Pengajuan' => 200 , 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
			$this->newtable->search(array(array("no_aju","Nomor Permohonan"),
										  array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("no_ijin","No. Izin"), 
										  array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url().'get/cb/set_dokumen/')),
										  array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
										  array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
										  array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
										  
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/view/status");
			$table->orderby(8);
			$table->sortby("DESC");
			$table->keys(array("IDX"));
			$table->hiddens(array("id","IDX","SORTTGL","kd_izin"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_status_permohonan");
			
			if($this->newsession->userdata('role') == '05'){
				$table->menu(array('Tambah' => array('GET', site_url().'licensing/choice', '0', 'home', 'modal'),
							   'Edit' => array('GET', site_url().'licensing/form/first', '1', 'fa fa-edit'),
							   'Preview' => array('GET', site_url().'licensing/preview', '1', 'fa fa-pencil-square-o')));
			}else{
				$table->menu(array('Preview' => array('GET', site_url().'licensing/preview', '1', 'fa fa-pencil-square-o')));
			}
			
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Data Permohonan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}
	
	public function list_inbox(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
			
			$direktorat = "'".join("','", $this->newsession->userdata('dir_id'))."'";
			$kd_izin = "'".join("','", $this->newsession->userdata('kode_izin'))."'";

			$arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
			$arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
			$arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
			$query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
 no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL
FROM VIEW_PERMOHONAN WHERE status_id = '0100' AND direktorat_id IN (".$direktorat.") AND kd_izin IN (".$kd_izin.")";
			//print_r($query);die();
			$table->title("");
			$table->columns(array("id","direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
								  array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'",site_url().'licensing/preview/{IDX}'),
								  "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
								  "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
								  "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
								  "status","CONVERT(VARCHAR(10), tgl_aju, 120)"));
			$this->newtable->width(array('Pengajuan' => 200 , 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
			$this->newtable->search(array(array("no_aju","Nomor Permohonan"),
										  array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("no_aju","No. Izin"), 
										  array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url().'get/cb/set_dokumen/')),
										  array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
										  array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
										  array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
										  
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/view/inbox");
			$table->orderby(8);
			$table->sortby("ASC");
			$table->keys(array("IDX"));
			$table->hiddens(array("id","IDX","SORTTGL"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_status");
			$table->menu(array('Tambah' => array('GET', site_url().'licensing/choice', '0', 'home', 'modal'),
							   'Edit' => array('GET', site_url().'licensing/form/first', '1', 'fa fa-edit'),
							   'Preview' => array('GET', site_url().'licensing/preview', '1', 'fa fa-pencil-square-o')));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Data Permohonan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}
	
	public function list_report(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
			$arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
			$arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
			$arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
			$query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL
FROM VIEW_PERMOHONAN ";
			if($this->newsession->userdata('role') == "99")
			$query .= "WHERE 1=1 ";
			else
			$query .= " WHERE trader_id = '".$this->newsession->userdata('trader_id')."'";
			
			$query .= " And status_id in ('0500')";
			
			$table->title("");
			$table->columns(array("id","direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
								  array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'",site_url().'licensing/preview/{IDX}'),
								  "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
								  "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
								  "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
								  "status","CONVERT(VARCHAR(10), tgl_aju, 120)"));
			$this->newtable->width(array('Pengajuan' => 200 , 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
			$this->newtable->search(array(array("no_aju","Nomor Permohonan"),
										  array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("no_ijin","No. Izin"), 
										  array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url().'get/cb/set_dokumen/')),
										  array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array()))										  
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/view/status");
			$table->orderby(8);
			$table->sortby("ASC");
			$table->keys(array("IDX"));
			$table->hiddens(array("id","IDX","SORTTGL"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_status");
			$table->menu(array( 'Preview' => array('GET', site_url().'licensing/preview', '1', 'fa fa-pencil-square-o'), 
								'Pelaporan' => array('GET', site_url().'licensing/report', '1', 'fa fa-pencil-square-o')));
			
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Data Laporan Perizinan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}
	
	public function list_proccess(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable;

			$direktorat = "'".join("','", $this->newsession->userdata('dir_id'))."'";
			$kd_izin = "'".join("','", $this->newsession->userdata('kode_izin'))."'";

			$arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
			$arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
			$arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
			$query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL
FROM VIEW_PERMOHONAN WHERE direktorat_id IN (".$direktorat.") AND kd_izin IN (".$kd_izin.")";
			
			if($this->newsession->userdata('role') == '07'){ #Kasie Subdit
				$query .= $this->main->find_where($query);
				$query .= " status_id ='0101' ";
			}else if($this->newsession->userdata('role') == '06'){ #Kasubdit
				$query .= $this->main->find_where($query);
				$query .= " status_id = '0600' ";
			}else if($this->newsession->userdata('role') == '02'){ #Direktur
				$query .= $this->main->find_where($query);
				$query .= " status_id = '0200'";
			}
			
			$table->title("");
			$table->columns(array("id","direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
								  array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'",site_url().'licensing/preview/{IDX}'),
								  "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
								  "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
								  "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
								  "status","CONVERT(VARCHAR(10), tgl_aju, 120)"));
			$this->newtable->width(array('Pengajuan' => 200 , 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
			$this->newtable->search(array(array("no_aju","Nomor Permohonan"),
										  array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("no_aju","No. Izin"), 
										  array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url().'get/cb/set_dokumen/')),
										  array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
										  array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
										  array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
										  
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/view/proccess");
			$table->orderby(8);
			$table->sortby("ASC");
			$table->keys(array("IDX"));
			$table->hiddens(array("id","IDX","SORTTGL"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_status");
			$table->menu(array('Tambah' => array('GET', site_url().'licensing/choice', '0', 'home', 'modal'),
							   'Edit' => array('GET', site_url().'licensing/form/first', '1', 'fa fa-edit'),
							   'Preview' => array('GET', site_url().'licensing/preview', '1', 'fa fa-pencil-square-o')));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Data Permohonan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	public function list_rollback(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable;

			$direktorat = "'".join("','", $this->newsession->userdata('dir_id'))."'";
			$kd_izin = "'".join("','", $this->newsession->userdata('kode_izin'))."'";

			$arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
			$arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
			$arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
			$query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL
FROM VIEW_PERMOHONAN WHERE direktorat_id IN (".$direktorat.") AND kd_izin IN (".$kd_izin.")";
			
			if($this->newsession->userdata('role') == '01'){ #Pemroses Subdit
				$query .= $this->main->find_where($query);
				$query .= " status_id = '0105'";
				$query .= " AND pemroses = ".$this->newsession->userdata('id');
			}else if($this->newsession->userdata('role') == '06'){ #Kasubdit
				$query .= $this->main->find_where($query);
				$query .= " status_id = '0601' ";
			}
			
			$table->title("");
			$table->columns(array("id","direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
								  array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'",site_url().'licensing/preview/{IDX}'),
								  "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
								  "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
								  "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
								  "status","CONVERT(VARCHAR(10), tgl_aju, 120)"));
			$this->newtable->width(array('Pengajuan' => 200 , 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
			$this->newtable->search(array(array("no_aju","Nomor Permohonan"),
										  array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("no_aju","No. Izin"), 
										  array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url().'get/cb/set_dokumen/')),
										  array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
										  array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
										  array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
										  
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/view/proccess");
			$table->orderby(8);
			$table->sortby("ASC");
			$table->keys(array("IDX"));
			$table->hiddens(array("id","IDX","SORTTGL"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_status");
			$table->menu(array('Tambah' => array('GET', site_url().'licensing/choice', '0', 'home', 'modal'),
							   'Edit' => array('GET', site_url().'licensing/form/first', '1', 'fa fa-edit'),
							   'Preview' => array('GET', site_url().'licensing/preview', '1', 'fa fa-pencil-square-o')));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Data Permohonan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	public function list_issued(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 

			$direktorat = "'".join("','", $this->newsession->userdata('dir_id'))."'";
			$kd_izin = "'".join("','", $this->newsession->userdata('kode_izin'))."'";

			$arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
			$arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
			$arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
			$query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen', 
no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL
FROM VIEW_PERMOHONAN WHERE status_id = '0102' AND direktorat_id IN (".$direktorat.") AND kd_izin IN (".$kd_izin.")";
			$table->title("");
			$table->columns(array("id","direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
								  array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'",site_url().'licensing/preview/{IDX}'),
								  "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
								  "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
								  "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
								  "status","CONVERT(VARCHAR(10), tgl_aju, 120)"));
			$this->newtable->width(array('Pengajuan' => 200 , 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
			$this->newtable->search(array(array("no_aju","Nomor Permohonan"),
										  array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("no_aju","No. Izin"), 
										  array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url().'get/cb/set_dokumen/')),
										  array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
										  array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
										  array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
										  
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/view/issued");
			$table->orderby(8);
			$table->sortby("ASC");
			$table->keys(array("IDX"));
			$table->hiddens(array("id","IDX","SORTTGL"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_status");
			$table->menu(array('Tambah' => array('GET', site_url().'home/test', '0', 'home', 'append'),
							   'Edit' => array('GET', site_url().'licensing/form/first', '1', 'fa fa-edit'),
							   'Preview' => array('GET', site_url().'licensing/preview', '1', 'fa fa-pencil-square-o')));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Data Permohonan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	public function list_released(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
			$arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
			$arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
			$arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
			
			$direktorat = "'".join("','", $this->newsession->userdata('dir_id'))."'";
			$kd_izin = "'".join("','", $this->newsession->userdata('kode_izin'))."'";

			$query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' AS 'Pengajuan',
direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL
FROM VIEW_PERMOHONAN WHERE status_id = '1000' AND direktorat_id IN (".$direktorat.") AND kd_izin IN (".$kd_izin.")";
			$table->title("");
			$table->columns(array("id","direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
								  array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'",site_url().'licensing/preview/{IDX}'),
								  "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
								  "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
								  "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
								  "status","CONVERT(VARCHAR(10), tgl_aju, 120)"));
			$this->newtable->width(array('Pengajuan' => 200 , 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
			$this->newtable->search(array(array("no_aju","Nomor Permohonan"),
										  array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("no_aju","No. Izin"), 
										  array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
										  array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url().'get/cb/set_dokumen/')),
										  array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
										  array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
										  array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
										  
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/view/released");
			$table->orderby(8);
			$table->sortby("ASC");
			$table->keys(array("IDX"));
			$table->hiddens(array("id","IDX","SORTTGL"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_status");
			$table->menu(array('Tambah' => array('GET', site_url().'licensing/choice', '0', 'home', 'modal'),
							   'Edit' => array('GET', site_url().'licensing/form/first', '1', 'fa fa-edit'),
							   'Preview' => array('GET', site_url().'licensing/preview', '1', 'fa fa-pencil-square-o')));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Data Permohonan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	function tembusan($izin_id,$permohonan_id){
		//print_r($izin_id."-".$permohonan_id);die();
		$query = "";
		$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$izin_id."'";
		$nama_izin = $this->main->get_uraian($sql,'nama_izin');
		$arrdata['nama_izin'] = $nama_izin;
		$arrdata['act'] = site_url('post/licensing/tembusan/save');
		$arrdata['izin_id'] = $izin_id;
		$arrdata['permohonan_id'] = $permohonan_id;
		$query_tembusan = "SELECT * FROM m_tembusan WHERE kd_izin = ".$izin_id." AND permohonan_id =".hashids_decrypt($permohonan_id,_HASHIDS_,9);
		//print_r($query_tembusan);die();
		$data = $this->main->get_result($query_tembusan);
		if($data){
			foreach($query_tembusan->result_array() as $row){
				$arrdata['tembusan'][] = $row;
				$arrdata['act'] = site_url('post/licensing/tembusan/update');
			}
		}
		return $arrdata;
	}

	function set_tembusan($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			
			$msg = "MSG||NO||Data Tembusan gagal disimpan";
			$res = FALSE;
			$arrdata = $this->main->post_to_query($this->input->post('data'));
			$arrtembusan = $this->input->post('tembusan');
			$arrkeys = array_keys($arrtembusan);
			$kd_izin = hashids_decrypt($arrdata['kd_izin'],_HASHIDS_,9);
			$permohonan_id = hashids_decrypt($arrdata['permohonan_id'],_HASHIDS_,9);
			$sql_aju = "SELECT no_aju FROM dbo.view_permohonan WHERE id = ".$permohonan_id." and kd_izin = ".$kd_izin;
			$no_aju = $this->main->get_uraian($sql_aju,"no_aju");

			if($act == "save"){
				for($s = 0; $s < count($_POST['tembusan']['urutan']); $s++){
					$tembusan = array('kd_izin' => $kd_izin,
						'permohonan_id' => $permohonan_id,
						'created_user' => $this->newsession->userdata('username'),
					    'created' => 'GETDATE()');
					for($j=0;$j<count($arrkeys);$j++){
						$tembusan [$arrkeys[$j]] = $arrtembusan[$arrkeys[$j]][$s];
					}
					unset($tembusan['id']);//print_r($tembusan);
					$this->db->insert('m_tembusan', $tembusan);
				}//die();
				if($this->db->affected_rows() > 0){
					$res = TRUE;
					$logu = array('aktifitas' => 'Menambahkan daftar tembusan untuk nomor aju '.$no_aju,
								  'url' => '{c}'. site_url().'post/licensing/tembusan/save'. ' {m} models/licensing/status_act {f} set_tembusan($act, $isajax)');
					$this->main->set_activity($logu);
					
					$msg = "MSG||YES||Data Tembusan Berhasil disimpan||REFRESH";
				}
				if($this->db->trans_status() === FALSE || !$res){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				return $msg;
			}elseif ($act == "update") {
				$this->db->where('kd_izin',$kd_izin);
				$this->db->where('permohonan_id',$permohonan_id);
				$this->db->delete('m_tembusan');
				if($this->db->affected_rows() > 0){
					for($s = 0; $s < count($_POST['tembusan']['urutan']); $s++){
						$tembusan = array('kd_izin' => $kd_izin,
							'permohonan_id' => $permohonan_id,
							'created_user' => $this->newsession->userdata('username'),
						    'created' => 'GETDATE()');
						for($j=0;$j<count($arrkeys);$j++){
							$tembusan [$arrkeys[$j]] = $arrtembusan[$arrkeys[$j]][$s];
						}
						unset($tembusan['id']);
						$this->db->insert('m_tembusan', $tembusan);
					}
					if($this->db->affected_rows() > 0){
						$res = TRUE;
						$logu = array('aktifitas' => 'Memperbaharui daftar tembusan untuk nomor aju '.$no_aju,
									  'url' => '{c}'. site_url().'post/licensing/tembusan/update'. ' {m} models/licensing/status_act {f} set_tembusan($act, $isajax)');
						$this->main->set_activity($logu);
						
						$msg = "MSG||YES||Data Tembusan Berhasil diperbaharui||REFRESH";
					}
					if($this->db->trans_status() === FALSE || !$res){
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();
					}

				}
				return $msg;
			}
		}
	}

	
}
?>