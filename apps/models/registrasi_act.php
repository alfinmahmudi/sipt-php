<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Registrasi_act extends CI_Model {

    var $arrext = array('.jpg', '.JPG', '.jpeg', '.JPEG', '.pdf', ".PDF", ".png", ".PNG");

    public function get_registrasi() {
        // die('de');
        // if ($id == null) {
        $sql = "SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ";
        $arrdata['direktorat'] = $this->main->set_combobox($sql, "kode", "uraian", TRUE);

        $get_usaha = "SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN' ";
        $arrdata['usaha'] = $this->main->set_combobox($get_usaha, "kode", "uraian", TRUE);

        $get_prop = "SELECT id, nama from m_prop";
        $arrdata['prop'] = $this->main->set_combobox($get_prop, "id", "nama", TRUE);
        $arrdata['negara'] = $this->main->set_combobox("SELECT kode, nama FROM m_negara", "kode", "nama", TRUE);

        $arrdata['jabatan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JABATAN' ", "kode", "uraian", TRUE);
        $arrdata['pendaftaran'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_REGISTRASI' ", "kode", "uraian", TRUE);
        $arrdata['identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_IDENTITAS' ", "kode", "uraian", TRUE);

        // $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2","id", "nama", TRUE);
        // $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab ORDER BY 2","id", "nama", TRUE);
        // $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec ORDER BY 2","id", "nama", TRUE);
        // $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel ORDER BY 2","id", "nama", TRUE);

        $arrdata['act'] = site_url('post/registrasi/regis_act/');

        $pass = 'sipt' . date('YmdH') . '-' . date('YmdHis');
        $arrdata['addGets'] = $this->main->encrypt($pass);
        // } else {
        //     $get_kab = "SELECT id, nama from m_kab where id_prov = '" . $id . "' ";
        //     $arrdata['kab'] = $this->main->set_combobox($get_kab, "id", "nama", TRUE);
        // }
        return $arrdata;
    }

    public function edit_regis($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $id = hashids_decrypt($id, _HASHIDS_, 9);
            $sql = "SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ";
            $arrdata['direktorat'] = $this->main->set_combobox($sql, "kode", "uraian", TRUE);

            $get_usaha = "SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN' ";
            $arrdata['usaha'] = $this->main->set_combobox($get_usaha, "kode", "uraian", TRUE);

            $get_prop = "SELECT id, nama from m_prop";
            $arrdata['prop'] = $this->main->set_combobox($get_prop, "id", "nama", TRUE);
            $arrdata['identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_IDENTITAS' ", "kode", "uraian", TRUE);

            $arrdata['jabatan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JABATAN' ", "kode", "uraian", TRUE);
            $arrdata['pendaftaran'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_REGISTRASI' ", "kode", "uraian", TRUE);
            if ($id != '') {
                $sql = " SELECT a.id, a.npwp, a.nm_perusahaan, a.tipe_registrasi, a.tipe_perusahaan, a.almt_perusahaan, a.kdprop as 'kdprop_per', a.kdkab as 'kdkab_per',
						a.kdkec as 'kdkec_per', a.kdkel as 'kdkel_per', a.kdpos as 'kdpos_per', a.telp as 'telp_per', a.fax as 'fax_per', a.jns_usaha, a.no_tdp, 
						a.tgl_tdp, a.file_tdp, a.file_npwp, a.na_pj, a.jabatan_pj, a.tgl_lahir_pj, a.tgl_lahir_pj, a.tmpt_lahir_pj, a.identitas_pj, a.noidentitas_pj,
						a.alamat_pj, a.kdprop_pj, a.kdkab_pj, a.kdkec_pj, a.kdkel_pj, a.telp_pj, a.fax_pj, a.email_pj, a.direktorat, b.id as 'user_id', b.username,
						b.nama as 'nama_koor', b.alamat as 'alamat_koor', b.telp as 'telp_koor', b.fax as 'fax_koor', b.email as 'email_koor', b.no_identitas, 
						b.tipe_identitas, b.file_identitas, b.file_kuasa, '<a href=\"" . site_url() . "download/doc' + substring(a.file_tdp, 19, (LEN(a.file_tdp)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewTDP', '<a href=\"" . site_url() . "download/doc' + substring(a.file_npwp, 19, (LEN(a.file_npwp)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewNpwp', '<a href=\"" . site_url() . "download/doc' + substring(b.file_identitas, 19, (LEN(b.file_identitas)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewKTP', '<a href=\"" . site_url() . "download/doc' + substring(b.file_kuasa, 19, (LEN(b.file_kuasa)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewSK', '<a href=\"" . site_url() . "download/doc' + substring(a.file_ktp, 19, (LEN(a.file_ktp)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewKTP_PJ'
						FROM m_trader a
						LEFT JOIN t_user b ON b.trader_id = a.id
						WHERE a.id = " . $this->db->escape($id);
                $arrsess = $this->db->query($sql)->row_array();
                $arrdata['sess'] = $arrsess;
                $arrdata['kab_per'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $arrsess['kdprop_per'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kec_per'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $arrsess['kdkab_per'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kel_per'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $arrsess['kdkec_per'] . "' ORDER BY 2", "id", "nama", TRUE);

                $arrdata['kab_pj'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $arrsess['kdprop_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kec_pj'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $arrsess['kdkab_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kel_pj'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $arrsess['kdkec_pj'] . "' ORDER BY 2", "id", "nama", TRUE);
                //print_r($arrdata);die();
            }

            $arrdata['act'] = site_url('licensing/update_regis');
            return $arrdata;
        }
    }

    public function get_vaidate_tdp() {
        $trader_id = $this->newsession->userdata('trader_id');
        $get_validate = "SELECT DATEDIFF(day,getdate(),tgl_exp_tdp) AS 'hari', tgl_exp_tdp, status, tipe_registrasi, keterangan from m_trader where id = " . $this->db->escape($trader_id);
        $validate = $this->db->query($get_validate)->row_array();
        if ($validate['hari'] <= 7 && $validate['tipe_registrasi'] == '01') {
            $res['status'] = '0';
            $res['hari'] = $validate['hari'];
        } else {
            if ($validate['status'] == '03') {
                $res['status'] = '0';
                $res['tdk_val'] = '0';
            } elseif ($validate['status'] == '02' || $validate['status'] == '04') {
                $res['status'] = '0';
                $res['ket'] = $validate['keterangan'];
                $res['tdk_val'] = '1';
            } else {
                $res['status'] = '1';
            }
        }
        return $res;
    }

    public function get_vaidate_nib() {
        $trader_id = $this->newsession->userdata('trader_id');
        $get_validate = "SELECT nib from m_trader where id = " . $this->db->escape($trader_id);
        $validate = $this->db->query($get_validate)->row_array();
        if ($validate['nib'] == "") {
            $res['status'] = '0';
        } else {
            $res['status'] = '1';
        }
        return $res;
    }

    public function update_regis($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
                exit();
            }
            $msg = "MSG||NO||Proses Gagal.||REFRESH";
            $resreg = FALSE;
            $arrregis = $this->main->post_to_query($this->input->post('REG'));
            $arrkoor = $this->main->post_to_query($this->input->post('KOOR'));
            $arrnpwp = $this->input->post('NPWP');
            $npwp = array($arrnpwp['1'] . $arrnpwp['2'] . $arrnpwp['3'] . $arrnpwp['4'] . $arrnpwp['5'] . $arrnpwp['6']);
            //print_r($npwp);die();
            $arrregis['npwp'] = $npwp[0];
            $this->db->trans_begin();
            $this->db->where('id', $arrregis['id']);
            unset($arrregis['id']);
            $this->db->update('m_trader', $arrregis);

            $this->db->where('id', $arrkoor['id']);
            unset($arrkoor['id']);
            $this->db->update('t_user', $arrkoor);

            if ($this->db->affected_rows() > 0) {
                $resreg = TRUE;
                $msg = "MSG||YES||Data Berhasil Di Update.||REFRESH";
            }

            if ($this->db->trans_status() === FALSE || $resreg == FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
            return $msg;
        }
    }

    public function get_preview($id) {
        $id_trader = hashids_decrypt($id, _HASHIDS_, 9);
        //print_r($id_trader);exit();
        $getDataTrader = "	SELECT a.* ,a.nib, b.nama 'prop', c.nama 'kab', z.nama 'kec', x.nama 'kel', d.uraian 'registrasi', e.uraian 'direk', f.uraian 'bentuk',
							'<a href=\"" . site_url() . "download/doc' + substring(a.file_npwp, 19, (LEN(a.file_npwp)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewNpwp', '<a href=\"" . site_url() . "download/doc' + substring(a.file_tdp, 19, (LEN(a.file_tdp)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewTDP', a.jabatan_pj, h.uraian as identitas_pj, m.nama AS kdprop_pj, n.nama AS kdkab_pj, o.nama AS kdkec_pj, p.nama AS kdkel_pj, '<a href=\"" . site_url() . "download/doc' + substring(a.file_ktp, 19, (LEN(a.file_ktp)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewKTP_PJ', dbo.dateIndo(a.tgl_tdp) as tgl_tdp, dbo.dateIndo(a.tgl_exp_tdp) as tgl_exp_tdp, dbo.dateIndo(a.tgl_lahir_pj) as tgl_lahir_pj, a.keterangan, CAST(a.created AS varchar(30)) as created, CAST(a.tgl_aktifasi AS varchar(30)) as tgl_aktifasi,
                                a.usr_aktivasi
							from m_trader a 
							left join m_prop b on b.id = a.kdprop
							left join m_kab c on c.id = a.kdkab
							LEFT JOIN m_kec z ON z.id = a.kdkec
							LEFT JOIN m_kel x ON x.id = a.kdkel 
							left join m_reff d on d.kode = a.tipe_registrasi and d.jenis = 'TIPE_REGISTRASI'
							left join m_reff e on e.kode = a.direktorat and e.jenis = 'DIREKTORAT'
							left join m_reff f on f.kode = a.tipe_perusahaan and f.jenis = 'TIPE_PERUSAHAAN'
							--left join m_reff g on g.kode = a.jabatan_pj and g.jenis = 'JABATAN'
							left join m_reff h on h.kode = a.identitas_pj and h.jenis = 'JENIS_IDENTITAS'
							LEFT JOIN m_prop m ON m.id = a.kdprop_pj
							LEFT JOIN m_kab n ON n.id = a.kdkab_pj
							LEFT JOIN m_kec o ON o.id = a.kdkec_pj
							LEFT JOIN m_kel p ON p.id = a.kdkel_pj
							where a.id = " . $this->db->escape($id_trader); //print_r($getDataTrader);die();
        $dataTrader = $this->db->query($getDataTrader)->row_array();

        if ($dataTrader['status'] == '03') {
            $action['act'] = site_url('post/registrasi/pembaharuan_act/');
        } elseif ($dataTrader['status'] == '01') {
            $action['act'] = site_url('post/registrasi/resend_mail/');
        } elseif ($dataTrader['status'] == '02' || $dataTrader['status'] == '04') {
            $action['act'] = site_url('post/registrasi/resend_tolak/');
        } else {
            $action['act'] = site_url('post/registrasi/preview_act/');
        }

//        $getDataUser = "SELECT a.id, a.nama, a.alamat, a.telp, a.fax, a.email, a.no_identitas, a.file_kuasa,
//						'<a href=\"" . site_url() . "download/doc' + substring(a.file_identitas, 19, (LEN(a.file_identitas)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewKTP', '<a href=\"" . site_url() . "download/doc' + substring(a.file_kuasa, 19, (LEN(a.file_kuasa)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewSK'
//						from t_user a
//						left join m_trader b on b.id = a.trader_id
//						where b.id = " . $this->db->escape($id_trader);
        $getDataUser = "SELECT a.id, a.nama, a.alamat, a.telp, a.fax, a.email, a.no_identitas, a.file_kuasa,
						'<a href=\"" . site_url() . "download/doc' + substring(a.file_identitas, 19, (LEN(a.file_identitas)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewKTP', '<a href=\"" . site_url() . "download/doc' + substring(a.file_kuasa, 19, (LEN(a.file_kuasa)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewSK',
						'<a href=\"" . site_url() . "download/doc' + substring(b.file_npwp, 19, (LEN(b.file_npwp)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'ViewNpwp'
						from t_user a
						left join m_trader b on b.id = a.trader_id
						where b.id = " . $this->db->escape($id_trader);
        $dataUser = $this->db->query($getDataUser)->row_array();
        $result = array('DataTrader' => $dataTrader,
            'DataUser' => $dataUser,
            'action' => $action);
        return $result;
    }

    public function set_mail_tolak($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
                exit();
            }
            $arrdata = $this->main->post_to_query($this->input->post('SET')); //print_r($arrdata);die();
            $id_trader = hashids_decrypt($arrdata['id_trader'], _HASHIDS_, 9);
            $id_user = hashids_decrypt($arrdata['id_user'], _HASHIDS_, 9);

            $getDataUser = "SELECT email from t_user where id = " . $this->db->escape($id_user);
            $dataUser = $this->db->query($getDataUser)->row_array();

            $sql = "SELECT a.id, a.npwp, a.file_npwp, a.no_tdp, a.file_tdp, a.noidentitas_pj, a.file_ktp, a.tipe_registrasi, a.tgl_tdp, a.tgl_exp_tdp, a.nm_perusahaan, a.na_pj, a.email_pj, a.keterangan
                    FROM m_trader a
                    WHERE a.id = " . $this->db->escape($id_trader);
            $arrtrader = $this->db->query($sql)->row_array(); //print_r($arrtrader);die();

            $queryData = "select username from t_user where id = " . $this->db->escape($id_user);
            $data = $this->db->query($queryData)->row_array();

            //mail from system part!
            if ($arrtrader['status'] == '03') {
                $mail_user .= "Yth Bapak/Ibu " . $arrtrader['na_pj'] . "<br>
                            Pimpinan " . $arrtrader['nm_perusahaan'] . "<br>
                           Pembaharuan Hak Akses SIPT PDN anda tidak disetujui dengan catatan.
               <br>
                           " . $arrtrader['keterangan'] . "
                           <br>    
                           Silahkan untuk mengakses website di http://sipt.kemendag.go.id
               <br>
               <br>
               <br>
               Admin SIPT PDN";
                $judul = "Pengiriman Kembali Penolakan Pembaharuan SIPT";
            } else {
                $mail_user .= "Yth Bapak/Ibu " . $arrtrader['na_pj'] . "<br>
                            Pimpinan " . $arrtrader['nm_perusahaan'] . "<br>
                           Pendaftaran Hak Akses SIPT PDN anda tidak disetujui dengan catatan.
               <br>
                           " . $arrtrader['keterangan'] . "
                           <br>    
                           Silahkan untuk mengakses website di http://sipt.kemendag.go.id
               <br>
               <br>
               <br>
               Admin SIPT PDN";
                $judul = "Pengiriman Kembali Penolakan Pendaftaran SIPT";
            }

            $mail = $dataUser['email'] . ',' . $arrtrader['email_pj'];
            $this->main->send_email($mail, $mail_user, $judul);

            $msg = "MSG||YES||Pengiriman Berhasil.||REFRESH";

            return $msg;
        }
    }

    public function set_mail($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
                exit();
            }
            $msg = "MSG||NO||Pengiriman Gagal.||REFRESH";
            $resreg = FALSE;
            $arrdata = $this->main->post_to_query($this->input->post('SET')); //print_r($arrdata);die();
            $id_trader = hashids_decrypt($arrdata['id_trader'], _HASHIDS_, 9);
            $id_user = hashids_decrypt($arrdata['id_user'], _HASHIDS_, 9);

            $getDataUser = "SELECT email, username, password from t_user where id = " . $this->db->escape($id_user);
            $dataUser = $this->db->query($getDataUser)->row_array();
            $uname = $dataUser['username'];

            $Caracteres = '23456789';
            $QuantidadeCaracteres = strlen($Caracteres);
            $QuantidadeCaracteres--;

            $Hash = NULL;
            for ($x = 1; $x <= 8; $x++) {
                $Posicao = rand(0, $QuantidadeCaracteres);
                $Hash .= substr($Caracteres, $Posicao, 1);
            }

            $pass = $Hash;

            $user = base64_encode(trim(strtolower($uname)));
            $genPassword = md5($pass . $user); //print_r($genPassword);die();

            $sql = "SELECT a.id, a.npwp, a.file_npwp, a.no_tdp, a.file_tdp, a.noidentitas_pj, a.file_ktp, a.tipe_registrasi, a.tgl_tdp, a.tgl_exp_tdp, a.nm_perusahaan, a.na_pj, a.email_pj, a.keterangan
                    FROM m_trader a
                    WHERE a.id = " . $this->db->escape($id_trader);
            $arrtrader = $this->db->query($sql)->row_array(); //print_r($arrtrader);die();

            $this->db->trans_begin();

            $updateUser = array('password' => $genPassword);
            $this->db->where('id', $id_user);
            $this->db->update('t_user', $updateUser);

            if ($this->db->affected_rows() > 0) {
                $resreg = TRUE;
            }

            if ($this->db->trans_status() === FALSE || $resreg == FALSE) {
                $this->db->trans_rollback();
                $msg = "MSG||NO||Pengiriman Gagal.||REFRESH";
            } else {
                $this->db->trans_commit();

                $queryData = "select username from t_user where id = " . $this->db->escape($id_user);
                $data = $this->db->query($queryData)->row_array();
                //mail from system part!                                
                $mail_user .= "Yth Bapak/Ibu " . $arrtrader['na_pj'] . "<br>
                                    Pimpinan " . $arrtrader['nm_perusahaan'] . "<br>
                                   Berikut Kami Kirimkan Kembali Hak Akses yang Sudah Diterima.
                   <br>
                   Username : " . $uname . "
                   <br>
                   Password : " . $pass . "
                   <br>
                   Catatan  :" . $arrtrader['keterangan'] . "
                               <br>    
                               Silahkan untuk mengakses website di http://sipt.kemendag.go.id
                   <br>
                   <br>
                   <br>
                   Admin SIPT PDN";
                $mail = $dataUser['email'] . ',' . $arrtrader['email_pj'];
                $this->main->send_email($mail, $mail_user, 'Pengiriman Ulang Hak Akses SIPT PDN');
                $msg = "MSG||YES||Pengiriman Berhasil.||REFRESH";
            }
            return $msg;
        }
    }

    public function list_request() {
        $sql = "SELECT sum(case a.status when '00' then 1 else 0 end ) as 'baru',
                sum(case a.status when '01' then 1 else 0 end ) as 'terima',
                sum(case a.status when '02' then 1 else 0 end ) as 'tolak',
                sum(case a.status when '03' then 1 else 0 end ) as 'pembaharuan',
                sum(case a.status when '04' then 1 else 0 end ) as 'tdk_val'
                from m_trader a;";
        $permohonan = $this->db->query($sql)->row_array(); // print_r($permohonan);die();
        $table = $this->newtable;
        $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS_TRADER' ORDER BY 1", "uraian", "uraian");
        $query = "SELECT a.id as 'ID', b.uraian as 'Tipe Registrasi', a.nm_perusahaan as 'Nama', a.npwp as 'NPWP' , 
                a.almt_perusahaan as 'Alamat', d.uraian as Status, dbo.dateIndo(a.created) as 'Tanggal Permohonan', 
                CONVERT(varchar,a.tgl_aktifasi,113) as 'Tanggal Proses',
                CASE 
                WHEN a.status NOT IN('00', '03') and (   DATEDIFF(MINUTE, a.created, a.tgl_aktifasi)
    - ( DATEDIFF(wk, a.created,a.tgl_aktifasi)*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 7 THEN DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
) < 1440 --tolak/trima kurang 1hari
                    THEN convert(varchar(5), (   DATEDIFF(MINUTE, a.created, a.tgl_aktifasi)
    - ( DATEDIFF(wk, a.created,a.tgl_aktifasi)*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 7 THEN DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
)/60) + ' Jam ' + convert(varchar(5), (   DATEDIFF(MINUTE, a.created, a.tgl_aktifasi)
    - ( DATEDIFF(wk, a.created,a.tgl_aktifasi)*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 7 THEN DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
)%60) + ' Menit '
WHEN a.status NOT IN('00', '03') and (   DATEDIFF(MINUTE, a.created, a.tgl_aktifasi)
    - ( DATEDIFF(wk, a.created,a.tgl_aktifasi)*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 7 THEN DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
) > 1440 --tolak/trima lebih 1hari
                    THEN convert(varchar(5), (   DATEDIFF(MINUTE, a.created, a.tgl_aktifasi)
    - ( DATEDIFF(wk, a.created,a.tgl_aktifasi)*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 7 THEN DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
)/1440) + ' Hari ' + convert(varchar(5), (   DATEDIFF(MINUTE, a.created, a.tgl_aktifasi)
    - ( DATEDIFF(wk, a.created,a.tgl_aktifasi)*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 7 THEN DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
)%1440/60) + ' Jam '
WHEN a.status NOT IN('00', '03') --tolak/trima
                    THEN convert(varchar(5), (   DATEDIFF(MINUTE, a.created, a.tgl_aktifasi)
    - ( DATEDIFF(wk, a.created,a.tgl_aktifasi)*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, a.tgl_aktifasi)  = 7 THEN DATEDIFF(minute,CONVERT(date,a.tgl_aktifasi),a.tgl_aktifasi) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
)) + ' Menit '
                WHEN (((   DATEDIFF(MINUTE, a.created, getdate())
    - ( DATEDIFF(wk, a.created,getdate())*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, getdate())  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, getdate())  = 7 THEN DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
) / 2880) * 100) > 100 or a.created = NULL --baru lebih dari sla
                    THEN '<div class=\"pulse pulse5 bg-danger light-color\" \>  '+ convert(varchar(5),(   DATEDIFF(MINUTE, a.created, getdate())
    - ( DATEDIFF(wk, a.created,getdate())*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, getdate())  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, getdate())  = 7 THEN DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
)) + ' Menit </div>' 
                WHEN (   DATEDIFF(MINUTE, a.created, getdate())
    - ( DATEDIFF(wk, a.created,getdate())*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, getdate())  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, getdate())  = 7 THEN DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
) > 2160 -- baru lebih dari 2160 menit
                    THEN '<div class=\"pulse pulse5 bg-danger light-color\" \>  '+ convert(varchar(5),(   DATEDIFF(MINUTE, a.created, getdate())
    - ( DATEDIFF(wk, a.created,getdate())*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, getdate())  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, getdate())  = 7 THEN DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
)/1440) + ' Hari '+ convert(varchar(5),(   DATEDIFF(MINUTE, a.created, getdate())
    - ( DATEDIFF(wk, a.created,getdate())*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, getdate())  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, getdate())  = 7 THEN DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
)%1440/60) + ' Jam </div>' 
                ELSE '<div class=\"pulse pulse2 bg-success light-color\" \> '+ convert(varchar(5),(   DATEDIFF(MINUTE, a.created, getdate())
    - ( DATEDIFF(wk, a.created,getdate())*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, getdate())  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, getdate())  = 7 THEN DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
)/60) + ' Jam '+ convert(varchar(5),(   DATEDIFF(MINUTE, a.created, getdate())
    - ( DATEDIFF(wk, a.created,getdate())*(2*24*60)
        -- End on Sunday
        -(CASE WHEN DATEPART(dw, getdate())  = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        -(CASE WHEN DATEPART(dw, a.created) = 7 THEN DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
        -- End on Saturday
        +(CASE WHEN DATEPART(dw, getdate())  = 7 THEN DATEDIFF(minute,CONVERT(date,getdate()),getdate()) ELSE 0 END)
        -- Start on Saturday
        +(CASE WHEN DATEPART(dw, a.created) = 1 THEN 24*60-DATEDIFF(minute,CONVERT(date,a.created),a.created) ELSE 0 END)
    )
)%60) + ' Menit </div>' 
                END AS SLA
                from m_trader a
                left join m_reff b on b.kode = a.tipe_registrasi and b.jenis = 'TIPE_REGISTRASI' 
                left join t_user c on c.trader_id = a.id
                left join m_reff d on d.kode = a.status and d.jenis = 'STATUS_TRADER'
                WHERE c.role = '05' ";

        //print_r($query);die();
        $this->newtable->search(array(array("d.uraian", 'Status Permohonan', array('ARRAY', $arrstatus)), array("CONVERT(VARCHAR(10), a.created, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))), array("b.uraian", "Tipe Registrasi"), array("a.nm_perusahaan", "Nama Perusahaan"), array("a.npwp", "NPWP"), array("a.almt_perusahaan", "Alamat")));
        $table->title("");
        $table->columns(array("a.id", "b.uraian", "a.almt_perusahaan", "c.nama", "a.na_pj"));
        $table->cidb($this->db);
        $table->ciuri($this->uri->segment_array());
        $table->action(site_url() . "licensing/view/request");
        $table->keys(array("ID"));
        $table->hiddens(array("ID"));
        $table->sortby("DESC");
        $table->show_search(TRUE);
        $table->show_chk(TRUE);
        $table->single(FALSE);
        $table->dropdown(TRUE);
        $table->hashids(TRUE);
        $table->postmethod(TRUE);
        $table->title(TRUE);
        $table->tbtarget("tb_status");
        $table->menu(array('Preview' => array('GET', site_url() . 'licensing/preview_regis', '1', 'fa fa-check'),
            'Edit' => array('GET', site_url() . 'licensing/edit_regis', '1', 'fa fa-edit')));

        $arrdata = array('tabel' => $table->generate($query),
            'judul' => 'Data Pengguna Hak Akses',
            'result' => $permohonan);
        if ($this->input->post("data-post"))
            return $table->generate($query);
        else
            return $arrdata;
    }

    public function list_pemroses() {
        $table = $this->newtable;
        $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS_TRADER' ORDER BY 1", "uraian", "uraian");
        $arrrole = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'ROLE' ORDER BY 1", "kode", "uraian");
        $query = "SELECT a.id as ID, a.username AS Username, a.nip AS NIP, a.nama AS Nama, a.alamat AS Alamat, a.telp AS Telepon, a.email AS Email, b.uraian AS Role
                FROM t_user a 
                LEFT JOIN m_reff b ON b.kode = a.role AND b.jenis = 'ROLE'
                WHERE a.role <> '05'";

        //print_r($query);die();
        $this->newtable->search(array(array("a.username", "Username"), array("a.nama", "Nama"), array("a.nip", "NIP"), array("a.role", 'Role', array('ARRAY', $arrrole))));
        $table->title("");
        $table->columns(array("a.id", "b.uraian", "a.nm_perusahaan", "a.npwp", "a.almt_perusahaan"));
        $table->cidb($this->db);
        $table->ciuri($this->uri->segment_array());
        $table->action(site_url() . "licensing/view/pemroses");
        $table->keys(array("ID"));
        $table->hiddens(array("ID"));
        $table->sortby("DESC");
        $table->show_search(TRUE);
        $table->show_chk(TRUE);
        $table->single(FALSE);
        $table->dropdown(TRUE);
        $table->hashids(FALSE);
        $table->postmethod(TRUE);
        $table->title(TRUE);
        $table->tbtarget("tb_status");
        $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/new_pemroses', '0', 'home'),
            'Edit' => array('GET', site_url() . 'licensing/edit_pemroses', '1', 'fa fa-edit'),
            'Hapus' => array('POST', site_url() . 'licensing/delete_pemroses', 'N', 'fa fa-trash-o', 'isngajax')));

        $arrdata = array('tabel' => $table->generate($query),
            'judul' => 'Data Petugas');
        if ($this->input->post("data-post"))
            return $table->generate($query);
        else
            return $arrdata;
    }

    public function del_pemroses($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->db->trans_begin();
            foreach ($id as $data) {
                $this->db->delete('t_user', array('id' => $data));
                $this->db->delete('t_user_role', array('user_id' => $data));
            }
            if ($this->db->affected_rows() > 0) {
                $logu = array('aktifitas' => 'Mendelete Master Petugas',
                    'url' => '{c}' . site_url() . 'licensing/delete_pemroses' . ' {m} models/registrasi_act{f} del_pemroses($id)');
                $this->main->set_activity($logu);
            }
            if ($this->db->trans_status === FALSE) {
                $this->db->trans_rollback();
                $msg = "MSG#Proses Gagal#refresh";
            } else {
                $this->db->trans_commit();
                $msg = "MSG#Proses Berhasil#refresh";
            }
            return $msg;
        }
    }

    public function get_pemroses($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian from m_reff a where a.jenis = 'JENIS_IDENTITAS'", "kode", "uraian", TRUE);
            $arrdata['direktorat'] = $this->main->set_combobox("SELECT kode, uraian from m_reff a where a.jenis = 'DIREKTORAT'", "kode", "uraian", TRUE);
            $arrdata['role'] = $this->main->set_combobox("SELECT kode, uraian from m_reff a where a.jenis = 'ROLE' and a.kode <> '05'", "kode", "uraian", TRUE);
            if ($id != '') {
                $sql = "SELECT a.id, a.username, a.nip, a.nama, a.alamat, a.telp, a.fax, a.email, a.no_identitas, a.tipe_identitas, a.role, a.status
                    FROM t_user a 
                    WHERE a.id = " . $this->db->escape($id);
                $arrdata['sess'] = $this->db->query($sql)->row_array();
                $query = "SELECT direktorat FROM t_user_role a WHERE a.user_id = " . $this->db->escape($id);
                $dir = $this->db->query($query)->row_array();
                $arrdata['dir'] = $dir['direktorat'];
                $arrdata['act'] = site_url('post/registrasi/pemroses_act/update');
                $arrdata['edit'] = "edit";
            } else {
                $arrdata['act'] = site_url('post/registrasi/pemroses_act/save');
            }
            return $arrdata;
        }
    }

    public function get_izin() {
        if ($this->newsession->userdata('_LOGGED')) {
            $id = $this->input->post('id');
            $dir = $this->input->post('dir');
            $sql = "SELECT a.id, a.nama_izin FROM m_izin a WHERE a.direktorat_id = " . $this->db->escape($dir) . " AND a.aktif = '1' ";
            $arrizin = $this->db->query($sql)->result_array();
            $query = "SELECT a.izin_id from t_user_role a WHERE a.user_id = " . $this->db->escape($id);
            $arrrole = $this->db->query($query)->result_array();
            $i = 1;
            $a = 0;
            $arrdata = '';
            $arrdata .= '<section class="panel">';
            $arrdata .= '<header class="panel-heading head-border"> Pilih Izin </header>';
            $arrdata .= '<table class="table table-striped custom-table table-hover"><thead>';
            $arrdata .= '<tr style="background: #383838; color: white;">';
            $arrdata .= '<th> NO </th>';
            $arrdata .= '<th><i class="fa fa-paperclip"></i> Nama Izin </th>';
            $arrdata .= '<th><i class="fa fa-check"></i> Pilih </th>';
            $arrdata .= '</tr></thead><tbody>';
            foreach ($arrizin as $key) {
                $arrdata .= '<tr><td>' . $i . '</td>';
                $arrdata .= '<td>' . $key['nama_izin'] . '</td>';
                foreach ($arrrole as $izn) {
                    if ($izn['izin_id'] == $key['id']) {
                        $check = "checked";
                        break;
                    } else {
                        $check = "";
                    }
                }
                $arrdata .= '<td><input type="checkbox" ' . $check . ' name="izin_id[]" value="' . $key['id'] . '"></td></tr>';
                $a++;
                $i++;
            }
            $arrdata .= '</tbody></table></section>';

            return $arrdata;
        }
    }

    public function set_pemroses($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
                exit();
            }
            $msg = "MSG||NO||Pendaftaran Gagal.||REFRESH";
            $respro = FALSE;
            $arrdata = $this->main->post_to_query($this->input->post('PROFIL'));
            $dir = $this->input->post('direktorat');
            $izin_id = $this->input->post('izin_id');
            $status = $this->input->post('stat');
            //print_r($status);die();
            //generate Password
            $Caracteres = 'ABCDEFGHJKLMPQRSTUVXWYZabcdefghjklmnpqrstuvwxyz23456789';
            $QuantidadeCaracteres = strlen($Caracteres);
            $QuantidadeCaracteres--;

            $Hash = NULL;
            for ($x = 1; $x <= 8; $x++) {
                $Posicao = rand(0, $QuantidadeCaracteres);
                $Hash .= substr($Caracteres, $Posicao, 1);
            }

            $pass = $Hash;

            $user = base64_encode(trim(strtolower($arrdata['username'])));
            $genPassword = md5($pass . $user);

            $arrbinus = array("1", "2", "14", "15", "17", "18", "19", "20", "21", "12", "16");
            $arrlogis = array("13", "3", "4", "5", "6");
            $arrbapok = array("7", "8", "9", "10", "11");
            $special_role = array('02', '09', '99', '08', '04');
            $bnyk_dir = array('01', '02', '03');

            $arrrole['direktorat'] = $dir;
            $arrrole['role'] = $arrdata['role'];

            $arrdata['password'] = $genPassword;
            $arrdata['trader_id'] = '1';
            $arrdata['status'] = '00';
            $arrdata['created'] = 'GETDATE()';
            $this->db->trans_begin();
            $this->db->insert('t_user', $arrdata);
            $id_user = $this->db->insert_id();
            if ($arrdata['role'] == '02') {
                if ($dir == '01') {
                    foreach ($arrbinus as $key) {
                        $arrrole['user_id'] = $id_user;
                        $arrrole['izin_id'] = $key;
                        $this->db->insert('t_user_role', $arrrole);
                    }
                } elseif ($dir == '02') {
                    foreach ($arrbapok as $key) {
                        $arrrole['user_id'] = $id_user;
                        $arrrole['izin_id'] = $key;
                        $this->db->insert('t_user_role', $arrrole);
                    }
                } elseif ($dir == '03') {
                    foreach ($arrbapok as $key) {
                        $arrrole['user_id'] = $id_user;
                        $arrrole['izin_id'] = $key;
                        $this->db->insert('t_user_role', $arrrole);
                    }
                }
            } elseif (in_array($arrdata['role'], $special_role)) {
                foreach ($bnyk_dir as $direkto) {
                    if ($direkto == '01') {
                        foreach ($arrbinus as $key) {
                            $arrrole['direktorat'] = $direkto;
                            $arrrole['user_id'] = $id_user;
                            $arrrole['izin_id'] = $key;
                            $this->db->insert('t_user_role', $arrrole);
                        }
                    } elseif ($direkto == '02') {
                        foreach ($arrbapok as $key) {
                            $arrrole['direktorat'] = $direkto;
                            $arrrole['user_id'] = $id_user;
                            $arrrole['izin_id'] = $key;
                            $this->db->insert('t_user_role', $arrrole);
                        }
                    } elseif ($direkto == '03') {
                        foreach ($arrbapok as $key) {
                            $arrrole['direktorat'] = $direkto;
                            $arrrole['user_id'] = $id_user;
                            $arrrole['izin_id'] = $key;
                            $this->db->insert('t_user_role', $arrrole);
                        }
                    }
                }
            } else {
                foreach ($izin_id as $key) {
                    $arrrole['user_id'] = $id_user;
                    $arrrole['izin_id'] = $key;
                    $this->db->insert('t_user_role', $arrrole);
                }
            }

            if ($this->db->affected_rows() > 0) {
                $logu = array('aktifitas' => 'Menambahkan Master Petugas',
                    'url' => '{c}' . site_url() . 'post/registrasi/pemroses_act/save' . ' {m} models/registrasi_act{f} set_pemroses($isajax)');
                $this->main->set_activity($logu);
                $respro = TRUE;
            }

            if ($this->db->trans_status() === FALSE || $respro == FALSE) {
                $this->db->trans_rollback();
                $msg = "MSG||NO||Pendaftaran Gagal.||REFRESH";
            } else {
                $this->db->trans_commit();
                //mail from system part!                                
                $mail_user .= "Yth Bapak/Ibu <br><br>
                               Pendaftaran Hak Akses SIPT PDN anda sudah disetujui dengan,
                               <br>
                               Username : " . $arrdata['username'] . "
                               <br>
                               Password : " . $pass . "
                               <br>    
                               Silahkan untuk mengakses website di http://sipt.kemendag.go.id/internal
                   <br>
                   <br>
                   <br>
                   Admin SIPT PDN";
                $mail = $arrdata['email'];
                $this->main->send_email($mail, $mail_user, 'Pendaftaran Baru SIPT');
                $msg = "MSG||YES||Pendaftaran Berhasil.||REFRESH";
            }

            return $msg;
        }
    }

    public function set_edit($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
                exit();
            }
            $msg = "MSG||NO||Update Gagal.||REFRESH";
            $respro = FALSE;
            $arrdata = $this->main->post_to_query($this->input->post('PROFIL'));
            $id = $this->input->post('id');
            $pass = $this->input->post('password');
            $dir = $this->input->post('direktorat');
            $izin_id = $this->input->post('izin_id');
            $status = $this->input->post('stat');

            $user = base64_encode(trim(strtolower($arrdata['username'])));
            $genPassword = md5($pass . $user);

            $arrdata['password'] = $genPassword;
            $arrrole['direktorat'] = $dir;
            $arrrole['role'] = $arrdata['role'];
            $arrdata['status'] = $status[0];

            $this->db->trans_begin();
            $this->db->where('id', $id);
            $this->db->update('t_user', $arrdata);

            $this->db->where('user_id', $id);
            $this->db->delete('t_user_role');
            foreach ($izin_id as $key) {
                $arrrole['user_id'] = $id;
                $arrrole['izin_id'] = $key;
                $this->db->insert('t_user_role', $arrrole);
            }

            if ($this->db->affected_rows() > 0) {
                $logu = array('aktifitas' => 'Mengupdate Master Petugas',
                    'url' => '{c}' . site_url() . 'post/registrasi/pemroses_act/update' . ' {m} models/registrasi_act{f} set_edit($isajax)');
                $this->main->set_activity($logu);
                $respro = TRUE;
            }

            if ($this->db->trans_status() === FALSE || $respro == FALSE) {
                $this->db->trans_rollback();
                $msg = "MSG||NO||Update Gagal.||REFRESH";
            } else {
                $this->db->trans_commit();
                $mail_user .= "Yth Bapak/Ibu <br><br>
                               Pendaftaran Hak Akses SIPT PDN anda sudah diperbaharui dengan,
                               <br>
                               Username : " . $arrdata['username'] . "
                               <br>";
                if ($pass == '') {
                    $mail_user .= "Password Tidak Mengalami Perubahan";
                } else {
                    $mail_user .= "Password : " . $pass . "";
                }
                $mail_user .= "<br>    
                               Silahkan untuk mengakses website di http://sipt.kemendag.go.id/internal
                   <br>
                   <br>
                   <br>
                   Admin SIPT PDN";
                $mail = $arrdata['email'];
                $this->main->send_email($mail, $mail_user, 'Pendaftaran Baru SIPT');
                $msg = "MSG||YES||Update Berhasil.||REFRESH";
            }
            return $msg;
        }
    }

    public function set_tolak($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
                exit();
            }
            $msg = "MSG||NO||Penolakan Gagal.||REFRESH";
            $resreg = FALSE;
            $arrdata = $this->main->post_to_query($this->input->post('SET'));
            $id_trader = hashids_decrypt($arrdata['id_trader'], _HASHIDS_, 9);
            $id_user = hashids_decrypt($arrdata['id_user'], _HASHIDS_, 9);

            $getDataUser = "SELECT email from t_user where id = " . $this->db->escape($id_user);
            $dataUser = $this->db->query($getDataUser)->row_array();

            $sql = "SELECT a.id, a.npwp, a.file_npwp, a.no_tdp, a.file_tdp, a.noidentitas_pj, a.file_ktp, a.tipe_registrasi, a.tgl_tdp, a.tgl_exp_tdp, a.nm_perusahaan, a.na_pj, a.email_pj
					FROM m_trader a
                    WHERE a.id = " . $this->db->escape($id_trader);
            $arrtrader = $this->db->query($sql)->row_array(); //print_r($arrtrader);die();

            if ($arrtrader['status'] == '03') {
                $status = '04';
            } else {
                $status = '02';
            }

            $this->db->trans_begin();
            $updateTrader = array(
                'status' => $status,
                'keterangan' => $arrdata['catatan'],
                'usr_aktivasi' => $this->newsession->userdata('nama'));

            $updateUser = array('status' => '01');

            $this->db->where('id', $id_trader);
            $this->db->update('m_trader', $updateTrader);

            if ($this->db->affected_rows() > 0) {
                $resreg = TRUE;
            }

            if ($this->db->trans_status() === FALSE || $resreg == FALSE) {
                $this->db->trans_rollback();
                $msg = "MSG||NO||Penolakan Gagal.||REFRESH";
            } else {
                $this->db->trans_commit();

                $queryData = "select username from t_user where id = " . $this->db->escape($id_user);
                $data = $this->db->query($queryData)->row_array();

                $update = "UPDATE m_trader SET tgl_aktifasi = getdate()
                        WHERE id = " . $this->db->escape($id_trader);
                $this->db->query($update);

                //mail from system part!
                if ($arrtrader['status'] == '03') {
                    $mail_user .= "Yth Bapak/Ibu " . $arrtrader['na_pj'] . "<br>
                                Pimpinan " . $arrtrader['nm_perusahaan'] . "<br>
                               Pembaharuan Hak Akses SIPT PDN anda tidak disetujui dengan catatan.
                   <br>
                               " . $arrdata['catatan'] . "
                               <br>    
                               Silahkan untuk mengakses website di http://sipt.kemendag.go.id
                   <br>
                   <br>
                   <br>
                   Admin SIPT PDN";
                    $judul = "Penolakan Pembaharuan SIPT";
                } else {
                    $mail_user .= "Yth Bapak/Ibu " . $arrtrader['na_pj'] . "<br>
                                Pimpinan " . $arrtrader['nm_perusahaan'] . "<br>
                               Pendaftaran Hak Akses SIPT PDN anda tidak disetujui dengan catatan.
                   <br>
                               " . $arrdata['catatan'] . "
                               <br>    
                               Silahkan untuk mengakses website di http://sipt.kemendag.go.id
                   <br>
                   <br>
                   <br>
                   Admin SIPT PDN";
                    $judul = "Penolakan Pendaftaran SIPT";
                }

                $mail = $dataUser['email'] . ',' . $arrtrader['email_pj'];
                $this->main->send_email($mail, $mail_user, $judul);

                $msg = "MSG||YES||Permohonan Ditolak.||REFRESH";
            }
            return $msg;
        }
    }

    public function set_preview($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
                exit();
            }
            $msg = "MSG||NO||Verifikasi Gagal.||REFRESH";
            $resreg = FALSE;
            $arrdata = $this->main->post_to_query($this->input->post('SET')); //print_r($arrdata);die();
            $id_trader = hashids_decrypt($arrdata['id_trader'], _HASHIDS_, 9);
            $id_user = hashids_decrypt($arrdata['id_user'], _HASHIDS_, 9);

            $getDataUser = "SELECT email, username, password from t_user where id = " . $this->db->escape($id_user);
            $dataUser = $this->db->query($getDataUser)->row_array();
            $uname = trim($dataUser['username']);

            $Caracteres = '23456789';
            $QuantidadeCaracteres = strlen($Caracteres);
            $QuantidadeCaracteres--;

            $Hash = NULL;
            for ($x = 1; $x <= 8; $x++) {
                $Posicao = rand(0, $QuantidadeCaracteres);
                $Hash .= substr($Caracteres, $Posicao, 1);
            }

            $pass = $Hash;

            $user = base64_encode(trim(strtolower($uname)));
            $genPassword = md5($pass . $user); //print_r($genPassword);die();

            $sql = "SELECT a.id, a.npwp, a.file_npwp, a.no_tdp, a.file_tdp, a.noidentitas_pj, a.file_ktp, a.tipe_registrasi, a.tgl_tdp, a.tgl_exp_tdp, a.nm_perusahaan, a.na_pj, a.email_pj
					FROM m_trader a
					WHERE a.id = " . $this->db->escape($id_trader);
            $arrtrader = $this->db->query($sql)->row_array(); //print_r($arrtrader);die();

            $this->db->trans_begin();
            $updateTrader = array(
                'status' => '01',
                'keterangan' => $arrdata['catatan'],
                'usr_aktivasi' => $this->newsession->userdata('nama'));
            if ($arrtrader['tipe_registrasi'] == 02) {
                $updateTrader['tipe_perusahaan'] = '05';
            }

            $updateUser = array('status' => '00',
                'password' => $genPassword);

            if ($arrtrader['npwp'] != '' && $arrtrader['file_npwp'] != '') {
                $insNPWP = array(
                    'trader_id' => $arrtrader['id'],
                    'tipe_dok' => '6',
                    'nomor' => $arrtrader['npwp'],
                    'nama_file' => substr($arrtrader['file_npwp'], 38),
                    'folder' => $arrtrader['file_npwp'],
                    'ekstensi' => '.' . pathinfo($arrtrader['file_npwp'], PATHINFO_EXTENSION),
                    // 'created' => date("Y-m-d h:i:s"),
                    'created_user' => $this->newsession->userdata('nama')
                );
                $this->db->insert('t_upload', $insNPWP);
            }
            if ($arrtrader['no_tdp'] != '' && $arrtrader['file_tdp'] != '') {
                $insTDP = array(
                    'trader_id' => $arrtrader['id'],
                    'tipe_dok' => '31',
                    'nomor' => $arrtrader['no_tdp'],
                    'tgl_dok' => $arrtrader['tgl_tdp'],
                    'tgl_exp' => $arrtrader['tgl_exp_tdp'],
                    'nama_file' => substr($arrtrader['file_tdp'], 34),
                    'folder' => $arrtrader['file_tdp'],
                    'ekstensi' => '.' . pathinfo($arrtrader['file_tdp'], PATHINFO_EXTENSION),
                    //'created' => date("Y-m-d h:i:s"),
                    'created_user' => $this->newsession->userdata('nama')
                );
                $this->db->insert('t_upload', $insTDP);
            }
            if ($arrtrader['noidentitas_pj'] != '' && $arrtrader['file_ktp'] != '') {
                $insKTP_PJ = array(
                    'trader_id' => $arrtrader['id'],
                    'tipe_dok' => '15',
                    'nomor' => $arrtrader['noidentitas_pj'],
                    'nama_file' => substr($arrtrader['file_ktp'], 37),
                    'folder' => $arrtrader['file_ktp'],
                    'ekstensi' => '.' . pathinfo($arrtrader['file_ktp'], PATHINFO_EXTENSION),
                    //'created' => date("Y-m-d h:i:s"),
                    'created_user' => $this->newsession->userdata('nama')
                );
                $this->db->insert('t_upload', $insKTP_PJ);
            }

            $this->db->where('id', $id_trader);
            $this->db->update('m_trader', $updateTrader);

            $this->db->where('id', $id_user);
            $this->db->update('t_user', $updateUser);

            if ($this->db->affected_rows() > 0) {
                $resreg = TRUE;
            }

            if ($this->db->trans_status() === FALSE || $resreg == FALSE) {
                $this->db->trans_rollback();
                $msg = "MSG||NO||Verifikasi Gagal.||REFRESH";
            } else {
                $this->db->trans_commit();

                $update = "UPDATE m_trader SET tgl_aktifasi = getdate()
                        WHERE id = " . $this->db->escape($id_trader);
                $this->db->query($update);

                $queryData = "select username from t_user where id = " . $this->db->escape($id_user);
                $data = $this->db->query($queryData)->row_array();
                //mail from system part!								
                $mail_user .= "Yth Bapak/Ibu " . $arrtrader['na_pj'] . "<br>
                                Pimpinan " . $arrtrader['nm_perusahaan'] . "<br>
                               Pendaftaran Hak Akses SIPT PDN anda telah disetujui.
			       <br>
			       Username : " . $uname . "
			       <br>
			       Password : " . $pass . "
			       <br>
			       Catatan  :" . $arrdata['catatan'] . "
                               <br>    
                               Silahkan untuk mengakses website di http://sipt.kemendag.go.id
			       <br>
			       <br>
			       <br>
			       Admin SIPT PDN";
                $mail = $dataUser['email'] . ',' . $arrtrader['email_pj'];
                $this->main->send_email($mail, $mail_user, 'Penerimaan Pendaftaran SIPT PDN');

                /*  $mail_admin .="Hai Admin! Thank for approval, keep calm and stay cool!
                  <br>
                  Regrads,
                  <br>
                  SIPT Online Service";
                  $mail = "mahmudialfin@gmail.com";
                  $this->main->send_email($mail, $mail_admin, 'You Just Approved a Request');
                 */
                $msg = "MSG||YES||Permohonan Diterima.||REFRESH";
            }
            return $msg;
        }
    }

    public function set_pembaharuan($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
                exit();
            }
            $msg = "MSG||NO||Verifikasi Gagal.||REFRESH";
            $resreg = FALSE;
            $arrdata = $this->main->post_to_query($this->input->post('SET')); //print_r($arrdata);die();
            $id_trader = hashids_decrypt($arrdata['id_trader'], _HASHIDS_, 9);
            $id_user = hashids_decrypt($arrdata['id_user'], _HASHIDS_, 9);

            $sql = "SELECT a.id, a.nib, a.npwp, a.file_npwp, a.no_tdp, a.file_tdp, a.noidentitas_pj, a.file_ktp, a.tipe_registrasi, a.nm_perusahaan,
                    a.email_pj, a.tgl_exp_tdp, a.tgl_tdp
                    FROM m_trader a
                    WHERE a.id = " . $this->db->escape($id_trader);
            $arrtrader = $this->db->query($sql)->row_array(); //print_r($arrtrader);die();

            $getDataUser = "SELECT email, username, password from t_user where id = " . $this->db->escape($id_user);
            $dataUser = $this->db->query($getDataUser)->row_array();

            $this->db->trans_begin();
            $updateTrader = array(
                'status' => '01',
                'keterangan' => $arrdata['catatan'],
                'usr_aktivasi' => $this->newsession->userdata('nama'));

            if ($arrtrader['no_tdp'] != '' && $arrtrader['file_tdp'] != '') {
                $insTDP = array(
                    'trader_id' => $arrtrader['id'],
                    'tipe_dok' => '31',
                    'nomor' => $arrtrader['no_tdp'],
                    'tgl_dok' => $arrtrader['tgl_tdp'],
                    'tgl_exp' => $arrtrader['tgl_exp_tdp'],
                    'nama_file' => substr($arrtrader['file_tdp'], 34),
                    'folder' => $arrtrader['file_tdp'],
                    'ekstensi' => '.' . pathinfo($arrtrader['file_tdp'], PATHINFO_EXTENSION),
                    //'created' => date("Y-m-d h:i:s"),
                    'created_user' => $this->newsession->userdata('nama')
                );
                $this->db->insert('t_upload', $insTDP);
            }

            if ($arrtrader['noidentitas_pj'] != '' && $arrtrader['file_ktp'] != '') {
                $insKTP_PJ = array(
                    'trader_id' => $arrtrader['id'],
                    'tipe_dok' => '15',
                    'nomor' => $arrtrader['noidentitas_pj'],
                    'tgl_dok' => 'GETDATE()',
                    'nama_file' => substr($arrtrader['file_ktp'], 37),
                    'folder' => $arrtrader['file_ktp'],
                    'ekstensi' => '.' . pathinfo($arrtrader['file_ktp'], PATHINFO_EXTENSION),
                    //'created' => date("Y-m-d h:i:s"),
                    'created_user' => $this->newsession->userdata('nama')
                );
                $this->db->insert('t_upload', $insKTP_PJ);
            }

            $this->db->where('id', $id_trader);
            $this->db->update('m_trader', $updateTrader); //print_r($this->db->last_query());die();

            if ($this->db->affected_rows() > 0) {
                $resreg = TRUE;
            }

            if ($this->db->trans_status() === FALSE || $resreg == FALSE) {
                $this->db->trans_rollback();
                $msg = "MSG||NO||Verifikasi Gagal.||REFRESH";
            } else {
                $this->db->trans_commit();
                $update = "UPDATE m_trader SET tgl_aktifasi = getdate()
                        WHERE id = " . $this->db->escape($id_trader);
                $this->db->query($update);

                $queryData = "select username from t_user where id = " . $this->db->escape($id_user);
                $data = $this->db->query($queryData)->row_array();
                if($data['username'] != $arrtrader['nib']){
                    $Caracteres = '23456789';
                    $QuantidadeCaracteres = strlen($Caracteres);
                    $QuantidadeCaracteres--;

                    $Hash = NULL;
                    for ($x = 1; $x <= 8; $x++) {
                        $Posicao = rand(0, $QuantidadeCaracteres);
                        $Hash .= substr($Caracteres, $Posicao, 1);
                    }

                    $pass = $Hash;

                    $user = base64_encode(trim(strtolower($arrtrader['nib'])));
                    $genPassword = md5($pass . $user); //print_r($genPassword);die();
                    $updateUser = array(
                        'password' => $genPassword,
                        'username' => $arrtrader['nib']);
                    $this->db->where('id', $id_user);
                    $this->db->update('t_user', $updateUser);
                    $pass_e = 'Password           = '.$pass.'<br>';
                    $user_e = 'Username           = '.$arrtrader['nib'].'<br>';
                }
                //mail from system part!                                
                $html = "";
                $domain = "sipt.kemendag.go.id";
                $html .= "Terima kasih telah Mengupdate profile perusahaan Anda dalam rangka mengaktifkan Kembali Hak Akses SIPT PDN <br>";
                $html .= "NPWP                  = " . $arrtrader['npwp'] . "<br>";
                $html .= "Nama Perushaaan       = " . $arrtrader['nm_perusahaan'] . "<br>";
                $html .= $user_e;
                $html .= $pass_e;
                $html .= "Data anda telah diverifikasi dan sudah dapat mengajukan permohonan perizinan di SIPT PDN<br>";
                $html .= "Silahkan mengunjungi website kami di http://sipt.kemendag.go.id<br>";
                $html .= "Direktorat Jenderal Perdagangan Dalam Negeri<br>";
                $html .= "Kementerian Perdagangan Republik Indonesia<br>";
                $mail = $arrtrader['email_pj'];
                $email = $dataUser['email'];
                $this->main->send_email($mail, $html, 'Verifikasi Pembaharuan Data Hak Akses SIPT PDN');
                $this->main->send_email($email, $html, 'Verifikasi Pembaharuan Data Hak Akses SIPT PDN');
                $msg = "MSG||YES||Permohonan Diterima.||REFRESH";
            }
            return $msg;
        }
    }

    public function get_vaidate($a) {
        $get_validate = "SELECT id from t_user where username = " . $this->db->escape($a) . " AND status = '00' ";
        $validate = $this->db->query($get_validate)->num_rows;
        return $validate;
    }

    // function set_registrasi($isajax, $keycode){
    //     if (!$this->newsession->userdata('_LOGGED')) {
    //         if ($this->newsession->userdata('keycode_ses') != str_replace(' ', '', $keycode)) {
    //             $msg = "MSG||NO||Captcha tidak sesuai.";
    //             return $msg;
    //         }
    //         if (!$isajax) {
    //             return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
    //         }
    //         $msg = "MSG||NO||Registrasi Gagal.";
    //         $resreg = FALSE;
    //         $arrnpwp = $this->main->post_to_query($this->input->post('NPWP'));
    //         $arrregis = $this->main->post_to_query($this->input->post('REG'));
    //         $arrkoor = $this->main->post_to_query($this->input->post('KOOR'));
    //     }
    // }

    public function set_registrasi($isajax, $keycode) {
        //echo $this->main->allowed($_FILES['filektp']['name']);die();
        if (!$this->newsession->userdata('_LOGGED')) {
            if ($this->newsession->userdata('keycode_ses') != str_replace(' ', '', $keycode)) {
                return "MSG||NO||Captcha tidak sesuai.";
            }
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
            }
            //print_r($this->main->allowed($_FILES['filenpwp']['name']));exit();
            $msg = "MSG||NO||Registrasi Gagal.";
            $resreg = FALSE;
            $arrnpwp = $this->main->post_to_query($this->input->post('NPWP'));
            $arrregis = $this->main->post_to_query($this->input->post('REG'));
            $arrkoor = $this->main->post_to_query($this->input->post('KOOR'));

            // $Caracteres = 'ABCDEFGHIJKLMOPQRSTUVXWYZabcdefghijklmnopqrstuvwxyz0123456789'; 
            // $QuantidadeCaracteres = strlen($Caracteres); 
            // $QuantidadeCaracteres--; 
            // $Hash=NULL; 
            //     for($x=1; $x<=8; $x++){ 
            //         $Posicao = rand(0,$QuantidadeCaracteres); 
            //         $Hash .= substr($Caracteres,$Posicao,1); 
            //     } 
            if ($arrregis['tipe_registrasi'] != '03') {
                $npwp = array($arrnpwp['1'] . $arrnpwp['2'] . $arrnpwp['3'] . $arrnpwp['4'] . $arrnpwp['5'] . $arrnpwp['6']);
                $qnpwp = "SELECT COUNT(a.id) as res, a.status FROM m_trader a 
                            WHERE a.npwp = " . $this->db->escape($npwp[0]) . " and a.status IN('00','01','03','04')
                            GROUP BY a.status"; //print_r($qnpwp);die();
                $resnpwp = $this->db->query($qnpwp)->result_array();
                foreach ($resnpwp as $hsl) {
                    if ($hsl['status'] == '00') {
                        $msg = "MSG||NO||Perusahaan Dengan No NPWP " . $npwp[0] . " Sedang Dalam Proses Verifikasi";
                        return $msg;
                    } elseif ($hsl['status'] == '01' || $hsl['status'] == '03' || $hsl['status'] == '03') {
                        $msg = "MSG||NO||Perusahaan Sudah Terdaftar";
                        return $msg;
                    }
                }
            }
            $genPassword = "-";
            //print_r($this->input->post('checkbox'));die();
            //print_r($_FILES['filektp_pj']);die();
            //directory file
            $dirNPWP = './upL04d5/document/NPWPPER/' . date("Y") . "/" . date("m") . "/" . date("d") . "/";
            $dirKTP = './upL04d5/document/IDENTITAS/' . date("Y") . "/" . date("m") . "/" . date("d") . "/";
            $dirSK = './upL04d5/document/SURATKUASA/' . date("Y") . "/" . date("m") . "/" . date("d") . "/";
            $dirTDP = './upL04d5/document/TDP/' . date("Y") . "/" . date("m") . "/" . date("d") . "/";
            $dirAkta = './upL04d5/document/AKTA/' . date("Y") . "/" . date("m") . "/" . date("d") . "/";
            $dirKTP_PJ = './upL04d5/document/KTP_PJ/' . date("Y") . "/" . date("m") . "/" . date("d") . "/";

            //for validation, if registrasi peroragan, because there's no upload file tdp in perorangan registration
//            if ($arrregis['tipe_registrasi'] == 01) {
//                if (!empty($_FILES['filetdp']['error'])) {
//                    switch ($_FILES['filetdp']['error']) {
//                        case '1':
//                            $msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
//                            return $msg;
//                            break;
//                        case '2':
//                            $msg = 'MSG||NO||The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
//                            return $msg;
//                            break;
//                        case '3':
//                            $msg = 'MSG||NO||The uploaded file was only partially uploaded';
//                            return $msg;
//                            break;
//                        case '4':
//                            $msg = 'MSG||NO||The uploded file was not found';
//                            return $msg;
//                            break;
//                        case '6':
//                            $msg = 'MSG||NO||Missing a temporary folder';
//                            return $msg;
//                            break;
//                        case '7':
//                            $msg = 'MSG||NO||Failed to write file to disk';
//                            return $msg;
//                            break;
//                        case '8':
//                            $msg = 'MSG||NO||File upload stopped by extension';
//                            return $msg;
//                            break;
//                        default:
//                            return "MSG||NO||File upload Error";
//                   }
//                } elseif (!in_array($this->main->allowed($_FILES['filetdp']['name']), $this->arrext)) {
//                    $msg = "MSG||NO||Upload File Surat TDP hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png";
//                    return $msg;
//                    break;
//                } elseif (empty($_FILES['filetdp']['tmp_name']) || $_FILES['filetdp']['tmp_name'] == 'none') {
//                    $msg = 'MSG||NO||File TDP Upload tidak di temukan';
//                    return $msg;
//                    break;
//                }elseif ($_FILES['filetdp']['size'] > 3000000) {
//                    $msg = 'MSG||NO||File Upload Maksimal Hanya 2 MB';
//                    return $msg;
//                    break;
//                }
//                if (file_exists($dirTDP) && is_dir($dirTDP)) {
//                    $config['upload_path'] = $dirTDP;
//                } else {
//                    if (mkdir($dirTDP, 0777, true)) {
//                        if (chmod($dirTDP, 0777)) {
//                            $config['upload_path'] = $dirTDP;
//                        }
//                    }
//                }
//                if ($_FILES['filetdp'] != null) {
//                    $config4['upload_path'] = $dirTDP;
//                    $config4['allowed_types'] = 'jpg|jpeg|pdf|png';
//                    $config4['remove_spaces'] = TRUE;
//                    $ext4 = pathinfo($_FILES['filetdp']['name'], PATHINFO_EXTENSION);
//                    $config4['file_name'] = "TDP-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext4;
//                    $this->load->library('upload');
//                    $this->upload->initialize($config4);
//                    $this->upload->display_errors('', '');
//
//                    $this->upload->do_upload("filetdp");
//                    $dataTDP = $this->upload->data("filetdp");
//                }
//                $tmptdp = $dirTDP . $config4['file_name'];
//                $arrregis['file_tdp'] = $tmptdp;
//            }
            //VALIDATE FOR AKTA
            // if ($arrregis['tipe_registrasi'] == 01) {
            //     if (!empty($_FILES['fileakta']['error'])) {
            //         switch ($_FILES['fileakta']['error']) {
            //             case '1':
            //                 $msg = 'MSG||NO||The uploaded file AKTA exceeds the upload_max_filesize directive in php.ini';
            //                 return $msg;
            //                 break;
            //             case '2':
            //                 $msg = 'MSG||NO||The uploaded file AKTA exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
            //                 return $msg;
            //                 break;
            //             case '3':
            //                 $msg = 'MSG||NO||The uploaded file AKTA was only partially uploaded';
            //                 return $msg;
            //                 break;
            //             case '4':
            //                 $msg = 'MSG||NO||The uploded file AKTA was not found';
            //                 return $msg;
            //                 break;
            //             case '6':
            //                 $msg = 'MSG||NO||Missing a temporary folder on AKTA file';
            //                 return $msg;
            //                 break;
            //             case '7':
            //                 $msg = 'MSG||NO||Failed to write file to disk on AKTA file';
            //                 return $msg;
            //                 break;
            //             case '8':
            //                 $msg = 'MSG||NO||File upload stopped by extension on AKTA file';
            //                 return $msg;
            //                 break;
            //             default:
            //                 return "MSG||NO||File upload Error";
            //         }
            //     } elseif (!in_array($this->main->allowed($_FILES['fileakta']['name']), $this->arrext)) {
            //         $msg = "MSG||NO||Upload File Surat AKTA hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png";
            //         return $msg;
            //         break;
            //     } else if (empty($_FILES['fileakta']['tmp_name']) || $_FILES['fileakta']['tmp_name'] == 'none') {
            //         $msg = 'MSG||NO||File AKTA Upload tidak di temukan';
            //         return $msg;
            //         break;
            //     }
            //     if (file_exists($dirAkta) && is_dir($dirAkta)) {
            //         $config['upload_path'] = $dirAkta;
            //     } else {
            //         if (mkdir($dirAkta, 0777, true)) {
            //             if (chmod($dirAkta, 0777)) {
            //                 $config['upload_path'] = $dirAkta;
            //             }
            //         }
            //     }
            //     if ($_FILES['fileakta'] != null) {
            //         $configakta['upload_path'] = $dirAkta;
            //         $configakta['allowed_types'] = 'jpg|jpeg|pdf|png';
            //         $configakta['remove_spaces'] = TRUE;
            //         $ext4 = pathinfo($_FILES['fileakta']['name'], PATHINFO_EXTENSION);
            //         $configakta['file_name'] = "AKTA-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext4;
            //         $this->load->library('upload');
            //         $this->upload->initialize($configakta);
            //         $this->upload->display_errors('', '');
            //         $this->upload->do_upload("fileakta");
            //         $dataAkta = $this->upload->data("fileakta");
            //     }
            //     $tmpAkta = $dirAkta . $configakta['file_name'];
            //     $arrregis['file_akta'] = $tmpAkta;
            // }
            //for npwp
//             if ($arrregis['tipe_registrasi'] == '01' || $arrregis['tipe_registrasi'] == '02') {
//             	if (!empty($_FILES['filenpwp']['error'])) {
//                     switch ($_FILES['filenpwp']['error']) {
//                         case '1':
//                             $msg = 'MSG||NO||The uploaded file NPWP exceeds the upload_max_filesize directive in php.ini';
//                             return $msg;
//                             break;
//                         case '2':
//                             $msg = 'MSG||NO||The uploaded file NPWP exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
//                             return $msg;
//                             break;
//                         case '3':
//                             $msg = 'MSG||NO||The uploaded file NPWP was only partially uploaded';
//                             return $msg;
//                             break;
//                         case '4':
//                             $msg = 'MSG||NO||The uploded file NPWP was not found';
//                             return $msg;
//                             break;
//                         case '6':
//                             $msg = 'MSG||NO||Missing a temporary folder on NPWP file';
//                             return $msg;
//                             break;
//                         case '7':
//                             $msg = 'MSG||NO||Failed to write file to disk on NPWP file';
//                             return $msg;
//                             break;
//                         case '8':
//                             $msg = 'MSG||NO||File upload stopped by extension on NPWP file';
//                             return $msg;
//                             break;
//                     }
//                 } elseif (!in_array($this->main->allowed($_FILES['filenpwp']['name']), $this->arrext)) {
//                     $msg = "MSG||NO||Upload File NPWP hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png";
//                     return $msg;
//                     break;
//                 } else if (empty($_FILES['filenpwp']['tmp_name']) || $_FILES['filenpwp']['tmp_name'] == 'none') {
//                     $msg = 'MSG||NO||File NPWP Upload tidak di temukan';
//                     return $msg;
//                     break;
//                 }
//
//                 if (file_exists($dirNPWP) && is_dir($dirNPWP)) {
//                     $config['upload_path'] = $dirNPWP;
//                 } else {
//                     if (mkdir($dirNPWP, 0777, true)) {
//                         if (chmod($dirNPWP, 0777)) {
//                             $config['upload_path'] = $dirNPWP;
//                         }
//                     }
//                 }
//                 if ($_FILES['filenpwp'] != null) {
//                     $config4['upload_path'] = $dirNPWP;
//                     $config4['allowed_types'] = 'jpg|jpeg|pdf|png';
//                     $config4['remove_spaces'] = TRUE;
//                     $ext4 = pathinfo($_FILES['filenpwp']['name'], PATHINFO_EXTENSION);
//                     $config4['file_name'] = "NPWP-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext4;
//                     $this->load->library('upload');
//                     $this->upload->initialize($config4);
//                     $this->upload->display_errors('', '');
//
//                     $this->upload->do_upload("filenpwp");
//                     $datanpwp = $this->upload->data("filenpwp");
//                 }
//                 $tmptnpwp = $dirNPWP . $config4['file_name'];
//                 $arrregis['file_npwp'] = $tmptnpwp;
//             }


            $cek = $this->input->post('checkbox');
            if ($cek == "") {
                if (!empty($_FILES['filesk']['error'])) {
                    if ($_FILES['filektp']['error'] == 1 || $_FILES['filesk']['error'] == 1) {
                        $msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
                        return $msg;
                        break;
                    } elseif ($_FILES['filektp']['error'] == 2 || $_FILES['filesk']['error'] == 2) {
                        $msg = 'MSG||NO||The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                        return $msg;
                        break;
                    } elseif ($_FILES['filektp']['error'] == 3 || $_FILES['filesk']['error'] == 3) {
                        $msg = 'MSG||NO||The uploaded file was only partially uploaded';
                        return $msg;
                        break;
                    } elseif ($_FILES['filektp']['error'] == 4 || $_FILES['filesk']['error'] == 4) {
                        $msg = 'MSG||NO||The uploded file was not found ininininin';
                        return $msg;
                        break;
                    } elseif ($_FILES['filektp']['error'] == 5 || $_FILES['filesk']['error'] == 5) {
                        $msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
                        return $msg;
                        break;
                    } elseif ($_FILES['filektp']['error'] == 6 || $_FILES['filesk']['error'] == 6) {
                        $msg = 'MSG||NO||Missing a temporary folder';
                        return $msg;
                        break;
                    } elseif ($_FILES['filektp']['error'] == 7 || $_FILES['filesk']['error'] == 7) {
                        $msg = 'MSG||NO||Failed to write file to disk';
                        return $msg;
                        break;
                    } elseif ($_FILES['filektp']['error'] == 8 || $_FILES['filesk']['error'] == 8) {
                        $msg = 'MSG||NO||File upload stopped by extension';
                        return $msg;
                        break;
                    }
                } elseif (!in_array($this->main->allowed($_FILES['filesk']['name']), $this->arrext)) {
                    $msg = "MSG||NO||Upload File KTP hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png";
                    return $msg;
                    break;
                } elseif (!in_array($this->main->allowed($_FILES['filesk']['name']), $this->arrext)) {
                    $msg = "MSG||NO||Upload File Surat Kuasa hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png";
                    return $msg;
                    break;
                } elseif (empty($_FILES['filesk']['tmp_name']) || $_FILES['filesk']['tmp_name'] == 'none') {
                    $msg = 'MSG||NO||File Surat Kuasa Upload tidak di temukan';
                    return $msg;
                    break;
                } elseif (empty($_FILES['filektp']['tmp_name']) || $_FILES['filektp']['tmp_name'] == 'none') {
                    $msg = 'MSG||NO||File KTP Upload tidak di temukan';
                    return $msg;
                    break;
                } elseif (empty($_FILES['filesk']['tmp_name']) || $_FILES['filesk']['tmp_name'] == 'none') {
                    $msg = 'MSG||NO||File Surat Kuasa Upload tidak di temukan';
                    return $msg;
                    break;
                } elseif (empty($_FILES['filektp']['tmp_name']) || $_FILES['filektp']['tmp_name'] == 'none') {
                    $msg = 'MSG||NO||File KTP Upload tidak di temukan';
                    return $msg;
                    break;
                } elseif ($_FILES['filektp']['size'] > 3000000) {
                    $msg = 'MSG||NO||File Upload Maksimal Hanya 2 MB';
                    return $msg;
                    break;
                } elseif ($_FILES['filesk']['size'] > 3000000) {
                    $msg = 'MSG||NO||File Upload Maksimal Hanya 2 MB';
                    return $msg;
                    break;
                }
                if (file_exists($dirKTP) && is_dir($dirKTP)) {
                    $config['upload_path'] = $dirKTP;
                } else {
                    if (mkdir($dirKTP, 0777, true)) {
                        if (chmod($dirKTP, 0777)) {
                            $config['upload_path'] = $dirKTP;
                        }
                    }
                }

                if (file_exists($dirSK) && is_dir($dirSK)) {
                    $config['upload_path'] = $dirSK;
                } else {
                    if (mkdir($dirSK, 0777, true)) {
                        if (chmod($dirSK, 0777)) {
                            $config['upload_path'] = $dirSK;
                        }
                    }
                }
                if ($_FILES['filektp'] != null) {
                    $config2['upload_path'] = $dirKTP;
                    $config2['allowed_types'] = 'jpg|jpeg|pdf|png';
                    $config2['remove_spaces'] = TRUE;
                    $ext2 = pathinfo($_FILES['filektp']['name'], PATHINFO_EXTENSION);
                    $config2['file_name'] = "KTP-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext2;
                    $this->load->library('upload');
                    $this->upload->initialize($config2);
                    $this->upload->display_errors('', '');

                    $this->upload->do_upload("filektp");
                    $dataKTP = $this->upload->data("filektp");
                    $tmptktp = $dirKTP . $config2['file_name'];
                    $arrregis['file_npwp'] = $tmptnpwp;
                }


                if ($_FILES['filesk'] != null) {
                    $config3['upload_path'] = $dirSK;
                    $config3['allowed_types'] = 'jpg|jpeg|pdf|png';
                    $config3['remove_spaces'] = TRUE;
                    $ext3 = pathinfo($_FILES['filesk']['name'], PATHINFO_EXTENSION);
                    $config3['file_name'] = "SK-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext3;
                    $this->load->library('upload');
                    $this->upload->initialize($config3);
                    $this->upload->display_errors('', '');

                    $this->upload->do_upload("filesk");
                    $dataSK = $this->upload->data("filesk");
                    $tmptsk = $dirSK . $config3['file_name'];
                    $arrregis['file_kuasa'] = $tmptsk;
                }
            }


//        //     //validation if when upload show the error code
//            if (!empty($_FILES['filektp_pj']['error'])) {
//                if ($_FILES['filektp_pj']['error'] == 1) {
//                    $msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
//                    return $msg;
//                    break;
//                } elseif ($_FILES['filektp_pj']['error'] == 2) {
//                    $msg = 'MSG||NO||The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
//                    return $msg;
//                    break;
//                } elseif ($_FILES['filektp_pj']['error'] == 3) {
//                    $msg = 'MSG||NO||The uploaded file was only partially uploaded';
//                    return $msg;
//                    break;
//                } elseif ($_FILES['filektp_pj']['error'] == 4) {
//                    $msg = 'MSG||NO||The uploded file was not found ininininin';
//                    return $msg;
//                    break;
//                } elseif ($_FILES['filektp_pj']['error'] == 5) {
//                    $msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
//                    return $msg;
//                    break;
//                } elseif ($_FILES['filektp_pj']['error'] == 6) {
//                    $msg = 'MSG||NO||Missing a temporary folder';
//                    return $msg;
//                    break;
//                } elseif ($_FILES['filektp_pj']['error'] == 7) {
//                    $msg = 'MSG||NO||Failed to write file to disk';
//                    return $msg;
//                    break;
//                } elseif ($_FILES['filektp_pj']['error'] == 8) {
//                    $msg = 'MSG||NO||File upload stopped by extension';
//                    return $msg;
//                    break;
//                }
//                //if success validate again if file null and not compatible format with jpg, pdf, or png
//            } elseif (!in_array($this->main->allowed($_FILES['filektp_pj']['name']), $this->arrext)) {
//                $msg = "MSG||NO||Upload File KTP PJ hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png";
//                return $msg;
//                break;
//            } elseif (empty($_FILES['filektp_pj']['tmp_name']) || $_FILES['filektp_pj']['tmp_name'] == 'none') {
//                $msg = 'MSG||NO||File Surat Kuasa Upload tidak di temukan';
//                return $msg;
//                break;
//            }elseif ($_FILES['filektp_pj']['size'] > 3000000) {
//                    $msg = 'MSG||NO||File Upload Maksimal Hanya 2 MB';
//                    return $msg;
//                    break;
//                }
//                //make a directory if not have a directory 
//                if (file_exists($dirKTP_PJ) && is_dir($dirKTP_PJ)) {
//                    $config['upload_path'] = $dirKTP_PJ;
//                } else {
//                    if (mkdir($dirKTP_PJ, 0777, true)) {
//                        if (chmod($dirKTP_PJ, 0777)) {
//                            $config['upload_path'] = $dirKTP_PJ;
//                        }
//                    }
//                }
//
//                //if all validation success, this is uploading process! with upload library
//                if ($_FILES['filektp_pj'] != null) {
//                    $configpj['upload_path'] = $dirKTP_PJ;
//                    $configpj['allowed_types'] = 'jpg|jpeg|pdf|png';
//                    $configpj['remove_spaces'] = TRUE;
//                    $ext1 = pathinfo($_FILES['filektp_pj']['name'], PATHINFO_EXTENSION);
//                    $configpj['file_name'] = "KTP_PJ-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext1;
//                    $this->load->library('upload');
//                    $this->upload->initialize($configpj);
//                    $this->upload->display_errors('', '');
//
//                    $this->upload->do_upload("filektp_pj");
//                    $dataKTP_PJ = $this->upload->data("filektp_pj");
//                }
//
//            //trans begin to make sure your transaction safe and sound util transaction commit.
//             $this->db->trans_begin();
//        //     //to combine NPWP 
//        //     //for temp to combine directory and file name
//            $tmptktp_pj = $dirKTP_PJ . $configpj['file_name'];
            // $tmptakta = $dirAkta . $configakta['file_name'];

            $arrregis['file_ktp'] = $tmptktp_pj;
            $arrregis['npwp'] = $npwp[0];
            $arrregis['status'] = "00";
            //print_r($arrregis);exit();
            $this->db->insert('m_trader', $arrregis); //print_r($this->db->last_query());die();

            if ($this->db->affected_rows() > 0) {
                $arrkoor['password'] = $genPassword;
                $arrkoor['trader_id'] = $this->db->insert_id();
                $arrkoor['status'] = "01";
                $arrkoor['role'] = "05";
                if ($cek == 1) {
                    $arrkoor['file_identitas'] = $tmptktp_pj;
                } else {
                    $arrkoor['file_identitas'] = $tmptktp;
                    $arrkoor['file_kuasa'] = $tmptsk;
                }
                $arrkoor['created'] = date("Y-m-d h:i:s");
                $arrkoor['tipe_identitas'] = "01";
                //print_r($arrkoor);exit();
                $this->db->insert('t_user', $arrkoor);
                // echo ;
                if ($this->db->affected_rows() > 0) {
                    $resreg = TRUE;
                }
            }

            if ($this->db->trans_status() === FALSE || $resreg == FALSE) {
                // die("rollback");
                $this->db->trans_rollback();
                $msg = "MSG||NO||Registrasi Gagal.";
            } else {

                $this->db->trans_commit();
                $log = $this->input->post('log');
                $arrlog['json'] = $log;
                $arrlog['nib'] = $arrregis['nib'];
                $arrlog['date_request'] = 'GETDATE()';
                $this->db->insert('m_oss', $arrlog);

                //mail from system part!								
                $html = "";
                $domain = "sipt.kemendag.go.id";
                $html .= "Terima kasih telah mendaftarkan profile perusahaan Anda dalam rangka mendapatkan Hak Akses SIPT PDN <br>";
                $html .= "NPWP            = " . implode('', $npwp) . "<br>";
                $html .= "Nama Perushaaan = " . $arrregis['nm_perusahaan'] . "<br>";
                $html .= "Nama Pimpinan   = " . $arrregis['na_pj'] . "<br>";
                $html .= "Data anda akan diverifikasi untuk selanjutnya diberikan hak akses SIPT PDN<br>";
                $html .= "Silahkan mengunjungi website kami di http://sipt.kemendag.go.id<br>";
                $html .= "Direktorat Jenderal Perdagangan Dalam Negeri<br>";
                $html .= "Kementerian Perdagangan Republik Indonesia<br>";
                $mail = $arrregis['email_pj'] . ',' . $arrkoor['email_koor'];
                $this->main->send_email($mail, $html, 'Pendaftaran SIPT PDN');

                $mail_admin = "Admin SIPT PDN<br><br";
                $mail_admin .= "Terdapat pendaftaran Baru Has Akses SIPT PDN<br>";
                $mail_admin .= "NPWP            = " . implode('', $npwp) . "<br>";
                $mail_admin .= "Nama Perushaaan = " . $arrregis['nm_perusahaan'] . "<br>";
                $mail_admin .= "Nama Pimpinan   = " . $arrregis['na_pj'] . "<br>";
                $mail_admin .= "Silahkan mengunjungi website di http://sipt.kemendag.go.id<br>";
                $mail = $arrregis['email_koor'];
                $this->main->send_email($mail, $mail_admin, 'Pendaftaran SIPT PDN');
                $msg = "MSG||YES||Registrasi Berhasil.||site_url('portal/regis')";
            }
            return $msg;
        }
    }

}

?>