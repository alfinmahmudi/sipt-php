<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Services_act extends CI_Model {

    public function get_data_izin($arr) {
        if ($arr['app'] == 'K3L' and $arr['token'] == 'd98db8d3947c9848fc67b30db9e3f407') {
            if ($arr['kd_izin'] == 12) {
                $sql = "select a.no_izin, a.tgl_izin, a.npwp, a.nm_perusahaan, a.almt_perusahaan, a.kdprop, a.kdkab, a.telp, a.nama_pj
                    from t_siup_agen a 
                    where a.no_izin = " . $this->db->escape($arr['nomor']) . " and a.status = '1000'";
            }
            return $this->data_result($sql);
        } else {
            $return['hasil'] = 0;
            $return['data'] = 'Filed Access';
            return $return;
        }
    }

    function data_result($sql) {
        $data = $this->db->query($sql);
        $return['hasil'] = $data->num_rows();
        $return['data'] = $data->row();
        return $return;
    }

    function data_result_array($sql) {
        $data = $this->db->query($sql);
        $return['hasil'] = $data->num_rows();
        $return['data'] = $data->result_array();
        return $return;
    }

}

?>