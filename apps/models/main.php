<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Model {

    function get_uraian($query, $select) {
        $data = $this->db->query($query);
        if ($data->num_rows() > 0) {
            $row = $data->row();
            return $row->$select;
        } else {
            return "";
        }
        return 1;
    }

    function get_pemasaran_bapok($data, $tipe) {
        $hsl = array();
        if ($tipe == 1) {
            if (trim($data != '')) {
                $temp_pemasaran = explode(",", $data);
                foreach ($temp_pemasaran as $pemasaran) {
                    if (trim($pemasaran) != '') {
                        $query_pemasaran = " select uraian from m_reff where jenis = 'PEMASARAN' and kode = '" . trim($pemasaran) . "'";
                        $dt_pemasaran = $this->db->query($query_pemasaran)->row_array();
                        $hsl[] = $dt_pemasaran['uraian'];
                    }
                }
            }
        } else if ($tipe == 2) {
            if (trim($data != '')) {
                $temp_kab = explode(",", $data);
                foreach ($temp_kab as $kab) {
                    if (trim($kab) != '') {
                        $query_kab = " SELECT a.nama FROM m_kab a where a.id = '" . trim($kab) . "'";
                        $dt_kab = $this->db->query($query_kab)->row_array();
                        $hsl[] = $dt_kab['nama'];
                    }
                }
            }
        } else if ($tipe == 3) {
            if (trim($data != '')) {
                $temp_prop = explode(",", $data);
                foreach ($temp_prop as $prop) {
                    if (trim($prop) != '') {
                        $query_prop = "SELECT a.nama FROM m_prop a where a.id = '" . trim($prop) . "'";
                        $dt_prop = $this->db->query($query_prop)->row_array();
                        $hsl[] = $dt_prop['nama'];
                    }
                }
            }
        }
        return implode(', ', $hsl);
    }

    public function get_selisih($kd_izin, $id, $table) {
        $query = " SELECT convert(varchar,last_proses,120) FROM " . $table . " WHERE id = '" . $id . "' AND kd_izin = '" . $kd_izin . "' ";
        $data = $this->db->query($query);
        $last_proses = $data->row_array();
        if ($data->num_rows() > 0) {
            date_default_timezone_set("Asia/Jakarta");
            $itunghari = 0;
            $selasi = 0;
            $selasihari = 0;
            $selasijam_kir = 0;
            $selasijam_pro = 0;
            $jampulang = "17:00:00";
            $jammasuk = "08:00:00";
            $dateskrg = date('Y-m-d');
            $jamskrg = date('H:i:s');
            $date = substr($last_proses['computed'], 0, 10); //tanggal sebelumnya
            $jamkirim = substr($last_proses['computed'], 10, 10); //jam status sebelumya
            $jamproses = date('H:i:s'); //saat kirim
            $harikirim = strtolower(date("l", strtotime($date)));
            $hariproses = strtolower(date("l", strtotime($dateskrg))); //print_r($hariproses);exit();
            $hari_itung = $date;
            //calculate date
            $hasilhari = round(abs(strtotime($dateskrg) - strtotime($date)) / 86400);
            $selasihari = $selasihari + $hasilhari;
            //check for sunday and saturday
            for ($i = 0; $i < $selasihari; $i++) {
                $tambah_tanggal = date('Y-m-d', strtotime($hari_itung . ' +1 day'));
                $hari_itung = $tambah_tanggal;
                $hari = date("l", strtotime($hari_itung));
                $hari = strtolower($hari);
                if ($hari != "saturday" && $hari != "sunday") {
                    $itunghari++;
                }
            }
            if (strtotime($date) < strtotime($dateskrg)) {
                //cek bedanya lebih dari sehari atau engga
                if ($itunghari > 1) {//lebih dari satu hari
                    if (strtotime($jammasuk) <= strtotime($jamkirim) && strtotime($jampulang) >= strtotime($jamkirim)) {//dikirim saat jam oprasional
                        if ($harikirim != "saturday" && $harikirim != "sunday") {
                            $hasiljam_kir = round(abs(strtotime($jampulang) - strtotime($jamkirim)));
                            $selasijam_kir = $selasijam_kir + $hasiljam_kir;
                            //print_r($selasijam_kir);print_r("..");
                            //echo " dikirim saat jam kantor ";	
                        } else {
                            $selasijam_kir = 0;
                            $itunghari--;
                            //echo "kirim hari saabt atau minggu";
                        }
                    } else {//di luar jam oprasiaonal
                        $selasijam_kir = 0;
                        //echo " dikirim luar jam kantor! ";
                    }
                    if (strtotime($jammasuk) <= strtotime($jamproses) && strtotime($jampulang) >= strtotime($jamproses)) {//diproses saat jam kantor
                        if ($hariproses != "saturday" && $hariproses != "sunday") {
                            $hasiljam_pro = round(abs(strtotime($jamproses) - strtotime($jammasuk)));
                            $selasijam_pro = $selasijam_pro + $hasiljam_pro;
                            //print_r($selasijam_pro);print_r("..");
                            //echo " diproses saat jam kantor ";
                        } else {//diproses di luar jam kantor
                            $selasijam_pro = 0;
                            $itunghari--;
                            //echo "diproses hari sabtu atau minggu";
                        }
                    } else {
                        $selasijam_pro = 0;
                        //echo " diproses luar jam kantor! ";
                    }
                    if (strtotime($jampulang) > strtotime($jamkirim) && strtotime($jammasuk) > strtotime($jamproses)) {//kalo di kirim lebih dari jam pulang
                        if ($hariproses != "saturday" && $hariproses != "sunday") {
                            $kepagian = 9;
                            //echo " kirim atau proses nya kepagian jadi gak masuk waktu kantor";
                        } elseif ($harikirim != "saturday" && $harikirim != "sunday") {
                            $kepagian = 9;
                            //echo " kirim atau proses nya kepagian jadi gak masuk waktu kantor";
                        } else {
                            $kepagian = 9;
                            //echo " Kirim atau proses nya sabtu minggu ";
                        }
                    }
                    //karena udah ada selisi di hari pertama
                    $itunghari = $itunghari - 1;
                    //kalo kirim atau proses sabtu minggu
                    //print_r($itunghari);
                    $totalhari = $itunghari * 9 * 3600;
                    $selasi = $selasijam_kir + $selasijam_pro + $totalhari + $kepagian;
                    $selasi = round((int) $selasi / 60);
                    //print_r(" jadi selisih nya itu ".$selasi);
                    //echo " lebih dari sehari nih! ";
                    return $selasi;
                } elseif ($itunghari <= 1) {
                    if (strtotime($jammasuk) <= strtotime($jamkirim) && strtotime($jampulang) >= strtotime($jamkirim)) {
                        if ($harikirim != "saturday" && $harikirim != "sunday") {
                            $hasiljam_kir = round(abs(strtotime($jampulang) - strtotime($jamkirim)));
                            $selasijam_kir = $selasijam_kir + $hasiljam_kir;
                            // print_r($selasijam_kir);print_r("..");
                            // echo " dikirim saat jam kantor ";
                        } else {
                            $selasijam_kir = 0;
                            //echo "dikirim hari sabtu atau minggu";
                        }
                    } else {
                        $selasijam_kir = 0;
                        //echo " ,dikirim luar jam kantor! ";
                    }
                    if (strtotime($jammasuk) <= strtotime($jamproses) && strtotime($jampulang) >= strtotime($jamproses)) {
                        if ($hariproses != "saturday" && $hariproses != "sunday") {
                            $hasiljam_pro = round(abs(strtotime($jamproses) - strtotime($jammasuk)));
                            $selasijam_pro = $selasijam_pro + $hasiljam_pro;
                            // print_r($selasijam_pro);print_r("..");
                            //echo " ,diproses saat jam kantor ";
                        } else {
                            $selasijam_pro = 0;
                            //echo "diproses hari sabtu atau minggu";
                        }
                    } else {
                        $selasijam_pro = 0;
                        //echo " ,diproses luar jam kantor! ";
                    }
                    //kepagian
                    if (strtotime($jampulang) > strtotime($jamkirim) && strtotime($jammasuk) > strtotime($jamproses)) {
                        $kepagian = 9;
                        //echo " ,kirim atau proses kepagian jadi tambah 1 hari kerja ";
                    }
                    if ($harikirim == "sunday" || $harikirim == "saturday") {
                        $kepagian = 0;
                    } elseif ($hariproses == "sunday" || $hariproses == "saturday") {
                        $kepagian = 0;
                    }
                    $selasi = $selasijam_kir + $selasijam_pro + $kepagian;
                    $selasi = round((int) $selasi / 60);
                    // print_r(" ,jadi selisih nya itu ".$selasi);
                    // echo " ,sehari doang! ";
                    return $selasi;
                } else {
                    return "";
                    //echo "ada yang salah pas itung hari";
                }
                //echo " beda hari men!";
            } elseif (strtotime($date) == strtotime($dateskrg)) {
                //kepagian
                if (strtotime($jammasuk) > strtotime($jamkirim)) {
                    if ($harikirim != "sunday" && $harikirim != "saturday") {
                        $jamkirim = $jammasuk;
                        // print_r($jamkirim);
                        // echo " ,kirim kepagian jadi default ke jam masuk ";
                    } else {
                        $jamkirim = 0;
                        //echo "kirim sabtu atau minggu";
                    }
                }
                // kirim kesorean
                if (strtotime($jampulang) < strtotime($jamkirim)) {
                    if ($harikirim != "sunday" && $harikirim != "saturday") {
                        $jamkirim = $jampulang;
                        // print_r($jamkirim);
                        // echo " ,kirim kepagian jadi default ke jam masuk ";
                    } else {
                        $jamkirim = 0;
                        //echo "kirim sabtu atau minggu";
                    }
                }
                // proses kesorean
                if (strtotime($jampulang) < strtotime($jamproses)) {
                    if ($harikirim != "sunday" && $harikirim != "saturday") {
                        $jamproses = $jampulang;
                        // print_r($jamproses);
                        // echo " ,proses nya kesorean jadi default ke jam pulang ";
                    } else {
                        $jamproses = 0;
                        // echo "proses sabtu minggu";
                    }
                }
                // proses kepagian
                if (strtotime($jammasuk) > strtotime($jamproses)) {
                    if ($harikirim != "sunday" && $harikirim != "saturday") {
                        $jamproses = $jammasuk;
                        // print_r($jamproses);
                        // echo " ,proses nya kesorean jadi default ke jam pulang ";
                    } else {
                        $jamproses = 0;
                        // echo "proses sabtu minggu";
                    }
                }
                // print_r($jamproses.' -- '.$jamkirim);die();
                $hasiljam = abs(strtotime($jamproses) - strtotime($jamkirim));
                $hasiljam = round((int) $hasiljam / 60);
                $selasi = $hasiljam;
                // print_r(" ,jadi selisih nya itu ".$selasi);
                // echo " harinya sama ";
                return $selasi;
            } else {
                return "";
                // echo "ada yang salah hari nya!";
            }
        } else {
            return "";
        }
    }

    // public function test(){
    // 		date_default_timezone_set("Asia/Bangkok");
    // $itunghari = 0;
    // $selasi = 0;
    // $selasihari = 0;
    // $selasijam_kir = 0;
    // $selasijam_pro = 0;
    // $jampulang = "16:00:00";
    // $jammasuk = "07:00:00";
    // $dateskrg = "2016-08-02";
    // $jamskrg = date('H:i:s');
    // $date = "2016-08-01";//tanggal sebelumnya
    // $jamkirim = "20:00:00";//jam status sebelumya
    // $jamproses = "05:00:00";//saat kirim
    // $harikirim = strtolower(date("l", strtotime($date)));
    // $hariproses = strtolower(date("l", strtotime($dateskrg)));//print_r($hariproses);exit();
    // $hari_itung = $date;
    // //calculate date
    // $hasilhari = round(abs(strtotime($dateskrg) - strtotime($date)) / 86400);
    // $selasihari = $selasihari + $hasilhari;
    // //check for sunday and saturday
    // for ($i=0; $i < $selasihari; $i++) { 
    // 	$tambah_tanggal = date('Y-m-d', strtotime($hari_itung . ' +1 day'));
    // 	$hari_itung = $tambah_tanggal;
    // 	$hari = date("l", strtotime($hari_itung));
    // 	$hari = strtolower($hari);
    // 	if ($hari != "saturday" || $hari != "sunday") {
    // 		$itunghari++;
    // 	}
    // }
    // //check
    // if (strtotime($date) < strtotime($dateskrg)) {//die("masuk");//beda hari
    // 	// print_r($itunghari);die();
    // 	//cek bedanya lebih dari sehari atau engga
    // 	if ($itunghari > 1) {//lebih dari sehari
    // 		//cek apakah di kirim pas jam operasional
    // 		if (strtotime($jammasuk) <= strtotime($jamkirim) && strtotime($jampulang) >= strtotime($jamkirim)) {//dikirim saat jam oprasional
    // 			if ($harikirim != "saturday" && $harikirim != "sunday") {
    // 				$hasiljam_kir = round(abs(strtotime($jampulang) - strtotime($jamkirim)) / 3600);
    // 				$selasijam_kir = $selasijam_kir + $hasiljam_kir;
    // 				print_r($selasijam_kir);print_r("..");
    // 				echo " dikirim saat jam kantor ";	
    // 			}else{
    // 				$selasijam_kir = 0;
    // 				$itunghari--;
    // 				echo "kirim hari saabt atau minggu";
    // 			}
    // 		}else{//di luar jam oprasiaonal
    // 			$selasijam_kir = 0;
    // 			echo " dikirim luar jam kantor! ";
    // 		}
    // 		if (strtotime($jammasuk) <= strtotime($jamproses) && strtotime($jampulang) >= strtotime($jamproses)) {
    // 			if ($hariproses != "saturday" && $hariproses != "sunday") {
    // 				$hasiljam_pro = round(abs(strtotime($jamproses) - strtotime($jammasuk)) / 3600);
    // 				$selasijam_pro = $selasijam_pro + $hasiljam_pro;
    // 				print_r($selasijam_pro);print_r("..");
    // 				echo " diproses saat jam kantor ";
    // 			}else{
    // 				$selasijam_pro = 0;
    // 				$itunghari--;
    // 				echo "diproses hari sabtu atau minggu";
    // 			}
    // 		}else{
    // 			$selasijam_pro = 0;
    // 			echo " diproses luar jam kantor! ";
    // 		}
    // 		if (strtotime($jampulang) > strtotime($jamkirim) && strtotime($jammasuk) > strtotime($jamproses)) {
    // 			if ($hariproses != "saturday" && $hariproses != "sunday"){
    // 				$kepagian = 9;
    // 				echo " kirim atau proses nya kepagian jadi gak masuk waktu kantor";
    // 			}elseif ($harikirim != "saturday" && $harikirim != "sunday") {
    // 				$kepagian = 9;
    // 				echo " kirim atau proses nya kepagian jadi gak masuk waktu kantor";
    // 			}else{
    // 				$kepagian = 9;
    // 				echo " Kirim atau proses nya sabtu minggu ";
    // 			}
    // 		}
    // 		//karena udah ada selisi di hari pertama
    // 		$itunghari = $itunghari - 1;
    // 		//kalo kirim atau proses sabtu minggu
    // 		print_r($itunghari);
    // 		$totalhari = $itunghari * 9;
    // 		$selasi = $selasijam_kir + $selasijam_pro + $totalhari + $kepagian;
    // 		print_r(" jadi selisih nya itu ".$selasi);
    // 		echo " lebih dari sehari nih! ";
    // 	}elseif ($itunghari <= 1){
    // 		if (strtotime($jammasuk) <= strtotime($jamkirim) && strtotime($jampulang) >= strtotime($jamkirim)) {
    // 			if ($harikirim != "saturday" && $harikirim != "sunday") {
    // 				$hasiljam_kir = round(abs(strtotime($jampulang) - strtotime($jamkirim)) / 3600);
    // 				$selasijam_kir = $selasijam_kir + $hasiljam_kir;
    // 				print_r($selasijam_kir);print_r("..");
    // 				echo " dikirim saat jam kantor ";
    // 			}else{
    // 				$selasijam_kir = 0;
    // 				echo "dikirim hari sabtu atau minggu";
    // 			}					
    // 		}else{
    // 			$selasijam_kir = 0;
    // 			echo " ,dikirim luar jam kantor! ";
    // 		}
    // 		if (strtotime($jammasuk) <= strtotime($jamproses) && strtotime($jampulang) >= strtotime($jamproses)) {
    // 			if ($hariproses != "saturday" && $hariproses != "sunday") {
    // 				$hasiljam_pro = round(abs(strtotime($jamproses) - strtotime($jammasuk)) / 3600);
    // 				$selasijam_pro = $selasijam_pro + $hasiljam_pro;
    // 				print_r($selasijam_pro);print_r("..");
    // 				echo " ,diproses saat jam kantor ";
    // 			}else{
    // 				$selasijam_pro = 0;
    // 				echo "diproses hari sabtu atau minggu";
    // 			}
    // 		}else{
    // 			$selasijam_pro = 0;
    // 			echo " ,diproses luar jam kantor! ";
    // 		}
    // 		//kepagian
    // 		if (strtotime($jampulang) > strtotime($jamkirim) && strtotime($jammasuk) > strtotime($jamproses)) {
    // 			$kepagian = 9;
    // 			echo " ,kirim atau proses kepagian jadi tambah 1 hari kerja ";
    // 		}
    // 		if ($harikirim == "sunday" || $harikirim == "saturday") {
    // 			$kepagian = 0;
    // 		}elseif ($hariproses == "sunday" || $hariproses == "saturday") {
    // 			$kepagian = 0;
    // 		}
    // 		$selasi = $selasijam_kir + $selasijam_pro + $kepagian;
    // 		print_r(" ,jadi selisih nya itu ".$selasi);
    // 		echo " ,sehari doang! ";
    // 	}else{
    // 		echo "ada yang salah pas itung hari";
    // 	}
    // 	echo " beda hari men!";
    // }elseif (strtotime($date) == strtotime($dateskrg)) { //echo '='.strtotime($date).'-'. strtotime($dateskrg);//die("iya");
    // 	//kepagian
    // 	if (strtotime($jammasuk) > strtotime($jamkirim)) {
    // 		if ($harikirim != "sunday" && $harikirim != "saturday") {
    // 			$jamkirim = $jammasuk;
    // 			print_r($jamkirim);
    // 			echo " ,kirim kepagian jadi default ke jam masuk ";
    // 		}else{
    // 			$jamkirim = 0;
    // 			echo "kirim sabtu atau minggu";
    // 		}
    // 	}
    // 	if (strtotime($jampulang) < strtotime($jamproses)) {
    // 		if ($harikirim != "sunday" && $harikirim != "saturday") {
    // 			$jamproses = $jampulang;
    // 			print_r($jamproses);
    // 			echo " ,proses nya kesorean jadi default ke jam pulang ";
    // 		}else{
    // 			$jamproses = 0;
    // 			echo "proses sabtu minggu";
    // 		}
    // 	}
    // 	$hasiljam = round(abs(strtotime($jamproses) - strtotime($jamkirim)) / 3600);
    // 	$selasi = $hasiljam;
    // 	print_r(" ,jadi selisih nya itu ".$selasi);
    // 	echo " harinya sama ";
    // }else{
    // 	echo "ada yang salah hari nya!";
    // }
    // }
    function get_result(&$query) {
        $data = $this->db->query($query);
        if ($data->num_rows() > 0) {
            $query = $data;
        } else {
            return false;
        }
        return true;
    }

    function array_cb($query, $key, $value) {
        $data = $this->db->query($query);
        $arraycb = array();
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                if (!array_key_exists($row[$key], $arraycb))
                    $arraycb[$row[$key]] = $row[$value];
            }
        }else {
            return false;
        }
        return $arraycb;
    }

    function set_combobox($query, $key, $value, $empty = FALSE, &$disable = "") {
        $combobox = array();
        $data = $this->db->query($query);
        if ($empty)
            $combobox[""] = "&nbsp;";
        if ($data->num_rows() > 0) {
            $kodedis = "";
            $arrdis = array();
            foreach ($data->result_array() as $row) {
                if (is_array($disable)) {
                    if ($kodedis == $row[$disable[0]]) {
                        if (!array_key_exists($row[$key], $combobox))
                            $combobox[$row[$key]] = "&nbsp; &nbsp;&nbsp;&nbsp;" . $row[$value];
                    }else {
                        if (!array_key_exists($row[$disable[0]], $combobox))
                            $combobox[$row[$disable[0]]] = $row[$disable[1]];
                        if (!array_key_exists($row[$key], $combobox))
                            $combobox[$row[$key]] = "&nbsp; &nbsp;&nbsp;&nbsp;" . $row[$value];
                    }
                    $kodedis = $row[$disable[0]];
                    if (!in_array($kodedis, $arrdis))
                        $arrdis[] = $kodedis;
                }else {
                    $combobox[$row[$key]] = str_replace("'", "\'", $row[$value]);
                }
            }
            $disable = $arrdis;
        }
        return $combobox;
    }

    function post_to_query($array, $except = "") {
        $data = array();
        foreach ($array as $a => $b) {
            if (is_array($except)) {
                if (!in_array($a, $except))
                    $data[$a] = $b;
            }else {
                $data[$a] = $b;
            }
        }
        return $data;
    }

    function find_where($query) {
        if (strpos($query, "WHERE") === FALSE) {
            $query = " WHERE ";
        } else {
            $query = " AND ";
        }
        return $query;
    }

    function allowed($ext) {
        for ($i = -1; $i > -(strlen($ext)); $i--) {
            if (substr($ext, $i, 1) == '.')
                return (substr($ext, $i));
        }
    }

    function get_telaah($izin) {
        $role = $this->newsession->userdata('role');
        $query = "SELECT A.telaah FROM m_telaah A WHERE A.role = '" . $role . "' AND A.izin_id = '" . $izin . "' AND A.jenis = 1";
        if ($this->get_result($query)) {
            foreach ($query->result_array() as $row) {
                $result = $row['telaah'];
            }
        }
        return $result;
    }

    function get_agrement($izin) {
        $role = $this->newsession->userdata('role');
        $query = "SELECT A.telaah FROM m_telaah A WHERE A.role = '" . $role . "' AND A.izin_id = '" . $izin . "' AND A.jenis = 2";
        if ($this->get_result($query)) {
            foreach ($query->result_array() as $row) {
                $result = $row['telaah'];
            }
        }
        return $result;
    }

    function set_menu($role) {
        $direktorat = join("','", $this->newsession->userdata('dir_id'));
        $addWhr = "";
        // add special menu with special condition :
        // use 'if' with kd_izin in session and use array to add new menu
        // ex array : $result[$row['MENU_ID']] = array($row['SUB_MENU'], $row['MENU_NAME'], $row['MENU_CLASS'], $row['MENU_BG'], str_replace('{urisegment}#', site_url(), $row['MENU_URL']));
        if (($direktorat != '03')) {
            $addWhr = "AND left(B.menu_id,2) != '66'";
        }
        $kd_izin = $this->newsession->userdata('kode_izin');
        $query = "SELECT LTRIM(RTRIM(A.menu_id)) AS MENU_ID, A.menu_name AS MENU_NAME, A.menu_class AS MENU_CLASS, A.menu_url AS MENU_URL, (SELECT COUNT(*) FROM m_menu WHERE LEFT(m_menu.menu_id, LEN(RTRIM(LTRIM(A.menu_id)))) = RTRIM(LTRIM(A.menu_id)) AND m_menu.menu_for = '0') AS SUB_MENU
                 FROM m_menu A LEFT JOIN m_menu_role B ON A.menu_id = B.menu_id
                 WHERE B.role_id = '" . $role . "' AND A.menu_status = 1 AND A.menu_for = '0' " . $addWhr . " ORDER BY 1";
        if ($this->get_result($query)) {
            foreach ($query->result_array() as $row) {
                if ($row['MENU_ID'] == '99') {
                    if (in_array('03', $this->newsession->userdata('dir_id'))) {
                        $result['0328'] = array('1', 'Rekap Label Beras', 'fa fa-file-archive-o', '', site_url() . 'reference/view/rekap_label');
                    }
                    if (in_array('02', $this->newsession->userdata('dir_id'))) {
                        if ($this->newsession->userdata('role') != '98')
                            $result['98'] = array('1', 'Rekap Pelaporan Antar Pulau', 'fa fa-file-archive-o', '', site_url() . 'reference/view/rekap_antarpulau');
                    }
                    if ($row['MENU_ID'] == '0306' && in_array("12", $kd_izin)) {
                        $result['0308'] = array('1', 'Penerbitan SIUP Agen', '', '', site_url() . 'licensing/view/lst_agen');
                    }
                    if (in_array('03', $this->newsession->userdata('dir_id'))) {
                        $result['08'] = array('3', 'Rekap TDPUD', 'fa fa-sliders', '', '#');
                        $result['0801'] = array('1', 'Rekap Tabulasi TDPUD', '', '', site_url() . 'reference/view/rekap_bapokstra');
                        $result['0802'] = array('1', 'Rekap Pelaporan TDPUD', '', '', site_url() . 'reference/view/rekap_bapok');
                    if ($this->newsession->userdata('role') != '98')
                            $result['0803'] = array('1', 'Rekap Perusahaan TDPUD', '', '', site_url() . 'reference/view/rekap_bapok_prshn');
                    }
                } //5502  
                if ($row['MENU_ID'] == '5502' and in_array('02', $this->newsession->userdata('dir_id')) == false and $this->newsession->userdata('role') != '05') {
                    // ini buat ngapus menu pelaporan antar pulau di direktorat lain 
                } else {
                    $result[$row['MENU_ID']] = array($row['SUB_MENU'], $row['MENU_NAME'], $row['MENU_CLASS'], $row['MENU_BG'], str_replace('{urisegment}#', site_url(), $row['MENU_URL']));
                }
            }
//            print_r($result);die();
        }
        return $result;
    }

    function set_notif($role) {
        $direktorat = "'" . join("','", $this->newsession->userdata('dir_id')) . "'";
        $kd_izin = "'" . join("','", $this->newsession->userdata('kode_izin')) . "'";
        if ($role == '01') {
            $role = "'01'";
        } else if ($role == '07') {
            $role = "'01','07'";
        } elseif ($role == '06') {
            $role = "'01','06','07'";
        } elseif ($role == '02') {
            $role = "'01','06','07','02'";
        }
        $arrnotif1 = array();
        $arrnotif2 = array();
        $arrnotif3 = array();
        $query = "SELECT a.status_id, replace(a.status,b.uraian+' - ', '') as status, a.status as status_desc,  count(*) as jml,
				CASE
					WHEN a.status_id = '0100' THEN '<a href=\"" . site_url() . "licensing/view/inbox\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>' 
					WHEN a.status_id = '0105' THEN '<a href=\"" . site_url() . "licensing/view/rollback\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>'
					WHEN a.status_id = '0102' THEN '<a href=\"" . site_url() . "licensing/view/issued\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>'
					WHEN a.status_id = '0200' THEN '<a href=\"" . site_url() . "licensing/view/proccess\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>'
					WHEN a.status_id = '0700' THEN '<a href=\"" . site_url() . "licensing/view/proccess\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>'
					WHEN a.status_id = '0701' THEN '<a href=\"" . site_url() . "licensing/view/rollback\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>'
					WHEN a.status_id = '0600' THEN '<a href=\"" . site_url() . "licensing/view/proccess\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>'
					WHEN a.status_id = '0601' THEN '<a href=\"" . site_url() . "licensing/view/rollback\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>'
					WHEN a.status_id = '0103' THEN '<a href=\"" . site_url() . "licensing/view/proccess\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>'
					WHEN a.status_id = '0104' THEN '<a href=\"" . site_url() . "licensing/view/inbox\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>'
					WHEN a.status_id = '0706' THEN '<a href=\"" . site_url() . "licensing/view/proccess\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>'
					WHEN a.status_id = '0602' THEN '<a href=\"" . site_url() . "licensing/view/proccess\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>'
					WHEN a.status_id = '0202' THEN '<a href=\"" . site_url() . "licensing/view/proccess\" class=\"single-mail\"><span class=\"icon bg-primary\"><i class=\"fa\">'+convert(varchar(10),count(*))+'</i></span><strong>'+replace(a.status,b.uraian+' - ', '')+'</strong>'
				END AS 'url', c.sla_waktu, left(a.status_id,2) as role, 
				case when ((DATEDIFF(minute, a.updated, GETDATE()) / c.sla_waktu) * 100) > 100 or a.updated = NULL 
				then   1  end  as SLA, d.nama_izin
				FROM dbo.view_permohonan a
				LEFT JOIN m_reff b ON b.kode = left(a.status_id,2) and b.jenis = 'ROLE'
                                LEFT JOIN m_waktu_proses c on c.sla_izin =  a.kd_izin and c.sla_status = a.status_id
                                LEFT JOIN m_izin d on d.id = a.kd_izin
				WHERE left(a.status_id,2) in (" . $role . ")
				AND a.direktorat_id IN (" . $direktorat . ")
				AND a.kd_izin IN (" . $kd_izin . ")
				AND a.status_id NOT IN ('0000','0500','0105','0601','0701','0102','0101')
				GROUP BY a.status_id, a.status,a.kd_izin, b.uraian , c.sla_waktu, a.updated,  d.nama_izin
				order by left(a.status_id,2) desc, status_id, a.kd_izin ;";
        // $query = "	SELECT a.kd_izin, a.nama_izin, count(*) as jml, b.sla_waktu, c.uraian as 'status'
        // 			FROM dbo.view_permohonan a 
        // 			LEFT JOIN m_reff c ON c.kode = a.status_id AND c.jenis = 'STATUS'
        // 			LEFT JOIN m_waktu_proses b ON b.sla_izin = a.kd_izin AND b.sla_status IN (".$status.")
        // 			WHERE a.status_id IN (".$status.") AND a.direktorat_id IN ('01') AND a.kd_izin IN ('1','2','12','16','15','21','17','18','19','20','14') 
        // 			GROUP BY a.kd_izin, a.nama_izin, a.status_id, a.status, b.sla_waktu, c.uraian order by kd_izin";
        if ($this->get_result($query)) {
            $arrnotif3 = $query->result_array();
        }
        $arrnotif = array_merge($arrnotif1, $arrnotif2, $arrnotif3);
        $total = 0;
        foreach ($arrnotif as $row) {
            $total = $total + $row['jml'];
            $result['notif'][] = $row;
        }
        $result['total'] = $total;
        return $result;
    }

    function get_sla($role) {
        $direktorat = "'" . join("','", $this->newsession->userdata('dir_id')) . "'";
        $kd_izin = "'" . join("','", $this->newsession->userdata('kode_izin')) . "'"; //print_r($kd_izin);die();
        if ($this->newsession->userdata('role') == '01') {
            $status = "'0100'";
        } elseif ($this->newsession->userdata('role') == '07') {
            $status = "'0100', '0700'";
        } elseif ($this->newsession->userdata('role') == '06') {
            $status = "'0100', '0700', '0600'";
        } elseif ($this->newsession->userdata('role') == '02') {
            $status = "'0100', '0700', '0600','0200'";
        } elseif ($this->newsession->userdata('role') == '99') {
            $status = "1000";
        } elseif ($this->newsession->userdata('role') == '09') {
            $status = "1000";
        } elseif ($this->newsession->userdata('role') == '04') {
            $status = "1000";
        } elseif ($this->newsession->userdata('role') == '08') {
            $status = "1000";
        } elseif ($this->newsession->userdata('role') == '98') {
            $status = "0000";
        }
        $sql = "SELECT a.kd_izin, a.nama_izin, a.id, DATEDIFF(minute,a.updated,GETDATE()) as 'selisih', a.status_id
				FROM dbo.view_permohonan a
				where a.kd_izin IN (" . $kd_izin . ") and a.status_id IN (" . $status . ")";
        if ($this->get_result($sql)) {
            $selisih = $sql->result_array();
        }
        $query = "SELECT * FROM m_waktu_proses a
					WHERE a.sla_izin IN(" . $kd_izin . ") AND a.sla_status IN(" . $status . ")";
        if ($this->get_result($query)) {
            $sla = $query->result_array();
        }
        $pas = 0;
        $lebih = 0;
        foreach ($selisih as $key) {
            foreach ($sla as $kuy) {
                if ($key['status_id'] == $kuy['sla_status'] && $key['kd_izin'] == $kuy['sla_izin']) {
                    if ($key['selisih'] >= $kuy['sla_waktu']) {
                        $lebih++;
                    }
                    $pas++;
                    $result['data'][] = array('nama_izin' => $key['nama_izin'],
                        'lebih' => $lebih,
                        'pas' => $pas,
                        'status' => $key['status_id']);
                }
            }
        }
        return $result;
    }

    function set_content($priv, $breadcumbs, $content, $message = "") {
        $ineng = $this->session->userdata('site_lang') ? $this->session->userdata('site_lang') : "id";
        $appname = 'Sistem Informasi Perizinan Terpadu - Direktorat Jenderal Perdaganan Dalam Negeri | Kementrian Perdagangan Republik Indonesia';
        $header = ($priv == "portal" ? $this->load->view($ineng . '/header/portal', '', true) : $this->load->view($ineng . '/header/logged', '', true));
        $navmenu = ($priv == "portal" ? '' : $this->set_menu($this->newsession->userdata('role')));
        if ($this->newsession->userdata('role') != '05') {
            $notif = ($priv == "portal" ? '' : $this->set_notif($this->newsession->userdata('role')));
        }
        if ($breadcumbs == "")
            $breadcumbs = "";
        $data = array('_appname_' => $appname,
            '_header_' => $header,
            '_navmenu_' => $navmenu,
            '_notif_' => $notif,
            '_content_' => $content,
            '_message_' => $message);
        return $data;
    }

    function set_aju() {
        $this->db->simple_query("IF NOT EXISTS (SELECT nomor, updated FROM m_aju WHERE (CASE [RESET] WHEN 'D' THEN CONVERT(VARCHAR, updated, 112) WHEN 'M' THEN LEFT(CONVERT(VARCHAR, updated, 112), 6) + RIGHT('0' + CAST(DAY(GETDATE()) AS VARCHAR), 2) WHEN 'Y' THEN LEFT(CONVERT(VARCHAR, updated, 112), 4) + RIGHT('0' + CAST(MONTH(GETDATE()) AS VARCHAR), 2) + RIGHT('0' + CAST(DAY(GETDATE()) AS VARCHAR), 2) END) = CONVERT(VARCHAR, GETDATE(), 112)) UPDATE m_aju SET nomor = 1, updated = GETDATE() ELSE UPDATE m_aju SET nomor = nomor + 1, updated = GETDATE()");
        $query = "SELECT uraian AS FORMAT FROM m_reff WHERE JENIS = 'nomor' AND KODE = '01'";
        $res = $this->get_result($query);
        if ($res) {
            $row = $query->row();
            $nomor = $this->get_uraian("SELECT nomor FROM m_aju", "nomor");
            $format = $row->FORMAT;
            $arrfor = explode("%", $format);
            foreach ($arrfor as $a) {
                $b = explode(";", $a);
                if (substr($b[0], 0, 1) == "_") {
                    $digit = (int) $b[1];
                    if ($b[0] == "_#") {
                        $tmp = substr(str_repeat("0", $digit) . (int) $nomor, -$digit);
                        $tmp .= '/';
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_F") {
                        $tmp = $b[1];
                        $tmp .= '/';
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_M") {
                        $tmp = substr(str_repeat("0", $digit) . date("m"), -$digit);
                        $tmp .= '/';
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_Y") {
                        $tmp = substr(str_repeat("0", $digit) . date("Y"), -$digit);
                        $format = str_replace($a, $tmp, $format);
                    }
                } else {
                    $format = str_replace("%" . $b[0], $b[0], $format);
                }
            }
            return $format;
        }
    }

    function prev_nomor($i) {
        $query = "SELECT nomor, format FROM m_nomor WHERE kd_izin = '" . $i . "'";
        $res = $this->get_result($query);
        if ($res) {
            $row = $query->row();
            $nomor = $row->nomor;
            $format = $row->format;
            $arrfor = explode("%", $format);
            $arr01 = array(1, 2, 12); #Direktorat Binus
            $arr02 = array(3, 4, 5, 6, 13); #Direktorat Logistik
            $arr03 = array(7, 8, 9, 10, 11); #Direktorat Bapokstra
            $dotslash = "";
            if (in_array($i, $arr01)) {
                $dotslash .= '.';
            } else if (in_array($i, $arr02)) {
                $dotslash .= '/';
            } else if (in_array($i, $arr03)) {
                $dotslash .= '/';
            }
            foreach ($arrfor as $a) {
                $b = explode(";", $a);
                if (substr($b[0], 0, 1) == "_") {
                    $digit = (int) $b[1];
                    if ($b[0] == "_#") {
                        $tmp = substr(str_repeat("x", $digit), -$digit);
                        $tmp .= $dotslash;
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_U") {
                        $tmp = $b[1];
                        $tmp .= (in_array($i, $arr01) ? '-' : $dotslash);
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_I") {
                        $tmp = $b[1];
                        $tmp .= $dotslash;
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_M") {
                        $tmp = substr(str_repeat("0", $digit) . date("m"), -$digit);
                        $tmp .= (in_array($i, $arr01) ? '' : $dotslash);
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_Y") {
                        $tmp = substr(str_repeat("0", $digit) . date("Y"), -$digit);
                        $format = str_replace($a, $tmp, $format);
                    }
                } else {
                    $format = str_replace("%" . $b[0], $b[0], $format);
                }
            }
            return $format;
        }
    }

    function set_nomor($i, $dir = "", $permohonan_id = "") {
        $this->db->simple_query("IF NOT EXISTS (SELECT nomor, updated FROM m_nomor WHERE kd_izin = '" . $i . "' AND (CASE [RESET] WHEN 'D' THEN CONVERT(VARCHAR, updated, 112) WHEN 'M' THEN LEFT(CONVERT(VARCHAR, updated, 112), 6) + RIGHT('0' + CAST(DAY(GETDATE()) AS VARCHAR), 2) WHEN 'Y' THEN LEFT(CONVERT(VARCHAR, updated, 112), 4) + RIGHT('0' + CAST(MONTH(GETDATE()) AS VARCHAR), 2) + RIGHT('0' + CAST(DAY(GETDATE()) AS VARCHAR), 2) END) = CONVERT(VARCHAR, GETDATE(), 112)) UPDATE m_nomor SET nomor = 1, updated = GETDATE() WHERE kd_izin = '" . $i . "' ELSE UPDATE m_nomor SET nomor = nomor + 1, updated = GETDATE() WHERE kd_izin = '" . $i . "'");
        $query = "SELECT nomor, format FROM m_nomor WHERE kd_izin = '" . $i . "'";
        $res = $this->get_result($query);
        if ($res) {
            $row = $query->row();
            $nomor = $row->nomor;
            $format = $row->format;
            if ($i == '1') {
                $get_survey = "SELECT kategori_survey FROM t_siujs WHERE id = '" . $permohonan_id . "' ";
                $katsur = $this->db->query($get_survey)->row_array();
                $katsur = explode(";", $katsur['kategori_survey']);
                if (trim($katsur['0']) == '') {
                    $katsur = count($katsur) - 1;
                } else {
                    $katsur = count($katsur);
                }
                $format = trim($nomor) . '.S' . str_pad($katsur, 2, "0", STR_PAD_LEFT) . '-SIUJS.' . date('m') . date('y');
            } else {
                $arrfor = explode("%", $format);
                //   print_r($arrfor);die();
                //print_r($arrfor);die();
                $arr01 = array(1, 2, 12, 14, 15, 16); #Direktorat Binus
                $arr02 = array(3, 4, 5, 6, 13); #Direktorat Logistik
                $arr03 = array(7, 8, 9, 10, 11, 17, 18, 19, 20); #Direktorat Bapokstra
                //print_r($i.$dir);die();
                $dotslash = "";
                if (in_array($i, $arr01)) {
                    $dotslash .= '.';
                } else if (in_array($i, $arr02)) {
                    $dotslash .= '/';
                } else if (in_array($i, $arr03)) {
                    $dotslash .= '/';
                } else {
                    $dotslash .= '/';
                }
                foreach ($arrfor as $a) {
                    $b = explode(";", $a);
                    if (substr($b[0], 0, 1) == "_") {
                        $digit = (int) $b[1];
                        //		  % _#5  ;  % _U;SIPT %  ;   _I  ;  DIR % % _M; 2 %  _Y  ;  4   _#5;_U;SIPT;_I;DIR_M;2_Y;4
                        if ($b[0] == "_#") {
                            $tmp = (int) $nomor;
                            $tmp .= $dotslash;
                            // die($tmp);
                            $format = str_replace($a, $tmp, $format);
                        } else if ($b[0] == "_U") {
                            $tmp = $b[1];
                            $tmp .= (in_array($i, $arr01) ? '-' : $dotslash);
                            $format = str_replace($a, $tmp, $format);
                        } else if ($b[0] == "_I") {
                            $tmp = $b[1];
                            $tmp .= (in_array($i, $arr01) ? $dotslash : $dir . $dotslash);
                            $format = str_replace($a, $tmp, $format);
                            if ($dir != "") {
                                $format = str_replace("DIR", "", $format);
                            }
                        } else if ($b[0] == "_M") {
                            $tmp = substr(str_repeat("0", $digit) . date("m"), -$digit);
                            $tmp .= (in_array($i, $arr01) ? '' : $dotslash);
                            $format = str_replace($a, $tmp, $format);
                        } else if ($b[0] == "_Y") {
                            $tmp = substr(str_repeat("0", $digit) . date("Y"), -$digit);
                            $format = str_replace($a, $tmp, $format);
                        } else if ($b[0] == "DIR") {
                            $format = str_replace($a, $dir, $format);
                        }
                    } else {
                        $format = str_replace("%" . $b[0], $b[0], $format);
                        //die($format);
                    }
                }
            }
            return $format;
        }
    }

    function um_garansi($args) {
        if (!is_array($args)) {
            return false;
        }
        /*
          JENIS IZIN 					= $args['permits']; jenis_permohonan
          KODE PRODUK					= $args['cproduct'];
          KODE PERUSAHAAN 			= $args['ccode'];
          JUMLAH ITEM					= $args['item'];
          NOMOR URUT PERUSAHAAN		= MAX(urut) FROM m_produk WHERE id = $args['cproduct'];
          MAX TRADER CAR				= $args['car'];
          MY (Month Years - 2 digits)	= date("m").date("Y");
         */
        #'%_I;{PERMITS}%_P;{PRODUCT}%_C;{CCODE}%_Q#;{QTY}%_INC;{INQUERY}%_MAX;{MAXC}%_M;2%_Y;4'
        $this->db->simple_query("IF NOT EXISTS (SELECT urut, updated FROM m_produk WHERE id = '" . $args['pid'] . "' AND (CASE [reset] WHEN 'D' THEN CONVERT(VARCHAR, updated, 112) WHEN 'M' THEN LEFT(CONVERT(VARCHAR, updated, 112), 6) + RIGHT('0' + CAST(DAY(GETDATE()) AS VARCHAR), 2) WHEN 'Y' THEN LEFT(CONVERT(VARCHAR, updated, 112), 4) + RIGHT('0' + CAST(MONTH(GETDATE()) AS VARCHAR), 2) + RIGHT('0' + CAST(DAY(GETDATE()) AS VARCHAR), 2) END) = CONVERT(VARCHAR, GETDATE(), 112)) UPDATE m_produk SET urut = 1, updated = GETDATE() WHERE id = '" . $args['pid'] . "' ELSE UPDATE m_produk SET urut = urut + 1, updated = GETDATE() WHERE id = '" . $args['pid'] . "'");
        $query = "SELECT uraian AS format FROM m_reff WHERE JENIS = 'nomor' AND KODE = '02'";
        $res = $this->get_result($query);
        if ($res) {
            $row = $query->row();
            $format = $row->format;
            $nomor = (int) $this->get_uraian("SELECT urut FROM m_produk WHERE id = '" . $args['pid'] . "'", "urut");
            $arrfor = explode("%", $format);
            foreach ($arrfor as $a) {
                $b = explode(";", $a);
                if (substr($b[0], 0, 1) == "_") {
                    $digit = (int) $b[1];
                    if ($b[0] == "_I") {
                        $tmp = (is_array($args) ? (str_replace($b[1], $args['permits'], $b[1])) : $b[1]);
                        $tmp .= ".";
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_P") {
                        $tmp = (is_array($args) ? (str_replace($b[1], $args['kode'], $b[1])) : $b[1]);
                        $tmp .= ".";
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_C") {
                        $tmp = (is_array($args) ? (str_replace($b[1], $args['ccode'], $b[1])) : $b[1]);
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_#") {
                        $tmp = (is_array($args) ? (str_replace($b[1], $args['item'], $b[1])) : $b[1]);
                        $tmp .= ".";
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_INC") {
                        $max = (int) $b[2];
                        $tmp = (is_array($args) ? substr(str_repeat("0", $max) . (int) (str_replace($b[1], $nomor, $b[1])), -$max) : $b[1]);
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_MAX") {
                        $inc = (int) $b[2];
                        $tmp = (is_array($args) ? substr(str_repeat("0", $inc) . (int) (str_replace($b[1], $args['car'], $b[1])), -$inc) : $b[1]);
                        $tmp .= ".";
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_M") {
                        $tmp = substr(str_repeat("0", $digit) . date("m"), -$digit);
                        $format = str_replace($a, $tmp, $format);
                    } else if ($b[0] == "_Y") {
                        $tmp = substr(str_repeat("0", $digit) . date("Y"), -$digit);
                        $format = str_replace($a, $tmp, $format);
                    }
                } else {
                    $format = str_replace("%" . $b[0], $b[0], $format);
                }
            }
            return $format;
        }
    }

    function cek_kswp($kd_izin) {
        $npwp = $this->newsession->userdata('npwp');
//        $url = "http://www.kemendag.go.id/addon/api/website_api/kswp/" . $kd_izin . "/" . $npwp;
        $url = "http://hub.kemendag.go.id/index.php/website_api/kswp/" . $kd_izin . "/" . $npwp;
//        die('asdasd');
        $ch = curl_init();
        // set URL and other appropriate options  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:4agROXxoNEhMC5meU78Al1nZWwcqGIJDzvpPL6K0', 'User-Agent:Website-Demo'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // grab URL and pass it to the browser  
        $output = curl_exec($ch);
        return json_decode($output);
    }
    
    function cek_nib() {
        $nib = $this->newsession->userdata('nib');
//      $url = "http://www.kemendag.go.id/addon/api/website_api/kswp/" . $kd_izin . "/" . $npwp;
        $url = "http://hub.kemendag.go.id/index.php/oss_services/checknib/";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
//      curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:MQbTaGOivtnc1y4ldD2JVf3EZWUhKsBYe6gHw5Lj'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:4agROXxoNEhMC5meU78Al1nZWwcqGIJDzvpPL6K0', 'User-Agent:Website-Demo'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'nib=' . $nib);
        curl_setopt($ch, CURLOPT_USERAGENT, 'WEB-SIPT');
        $output1 = curl_exec($ch);
         return json_decode($output1);
    }
    
    function data_nib() {
        $nib = $this->newsession->userdata('nib');
//      $url = "http://www.kemendag.go.id/addon/api/website_api/kswp/" . $kd_izin . "/" . $npwp;
        $url = "http://hub.kemendag.go.id/index.php/oss_services/inqueryniball/";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
//                curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:MQbTaGOivtnc1y4ldD2JVf3EZWUhKsBYe6gHw5Lj'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:4agROXxoNEhMC5meU78Al1nZWwcqGIJDzvpPL6K0', 'User-Agent:Website-Demo'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'nib=' . $nib);
        curl_setopt($ch, CURLOPT_USERAGENT, 'WEB-SIPT');
        $output1 = curl_exec($ch);
        return $output1;
    }

    function set_expired($e) {
        $query = "SELECT berlaku, berlaku_by FROM m_nomor WHERE kd_izin = '" . $e . "'";
        $res = $this->get_result($query);
        if ($res) {
            $row = $query->row();
            switch ($row->berlaku_by) {
                case 'Y':
                    $q = "SELECT CONVERT(VARCHAR(10), (DATEADD(year," . $row->berlaku . ",GETDATE())), 103) AS EXPIRED";
                    break;
                case 'M':
                    $q = "SELECT CONVERT(VARCHAR(10), (DATEADD(month," . $row->berlaku . ",GETDATE())), 103) AS EXPIRED";
                    break;
                case 'D':
                    $q = "SELECT CONVERT(VARCHAR(10), (DATEADD(day," . $row->berlaku . ",GETDATE())), 103) AS EXPIRED";
                    break;
            }
            if ($q) {
                $exp = $this->get_uraian($q, "EXPIRED");
                return $exp;
            } else {
                return false;
            }
        }
    }

    function set_proses($a, $b, $c, $d, $e, $f) {
        /*
          @ $a = Kode Direktorat
          @ $b = Kode Izin
          @ $c = Rekomendasi / Disposisi
          @ $d = Role
          @ $e = Status Existing
         */
//        $reskswp = $this->cek_nib('8120004752265');
//        var_dump($reskswp->responcheckNIB->dataNIB->status);die();
        //print_r(implode($this->newsession->userdata('dir_id')));die();
        $query = "SELECT a.tolak, a.setujui, a.kembali FROM m_proses a WHERE a.direktorat = '" . $a . "' AND a.kd_izin = '" . $b . "' AND a.disposisi = '" . $c . "' AND role = '" . $d . "' AND a.posisi = '" . $e . "'";
        //print_r($query);die();
        //print_r($e);die();
        $data = $this->db->query($query);
        $proses = array();
        $arrCetak = array("0700", "0102", "0600", "0200", "0100", "1000");
        $get_validate = "SELECT DATEDIFF(day,getdate(),tgl_exp_tdp) AS 'hari', tgl_exp_tdp, status, tipe_registrasi from m_trader where id = '" . $this->newsession->userdata('trader_id') . "' ";
        $validate = $this->db->query($get_validate)->row_array();
        //untuk tombol rollback  $arrRollback = array("0600", "0200","0100","0700","0105","0701","0601");
        $qs = "SELECT id_ksw FROM m_izin WHERE id = '" . $b . "'";
        $arrkswp = $this->db->query($qs)->row_array();
        if ($data->num_rows() > 0) {
            foreach ($data->result_array() as $row) {
                $proses[] = '<button class="btn btn-sm btn-info addon-btn m-b-10" id="' . rand() . '" onclick="javascript:window.history.back(); return false;"><i class="fa fa-arrow-left pull-left"></i>Kembali</button>';
//                echo $this->newsession->userdata('trader_id');die();
                $reskswp = $this->cek_nib();
//                $reskswp->responcheckNIB->dataNIB->status = 2;
                $siup = array('3', '4', '5', '1', '2', '11');
                if ($this->newsession->userdata('role') == '05') {
                    if ($this->newsession->userdata('tipe_registrasi') != '03') {
                        if ($reskswp->responcheckNIB->dataNIB->status != '03' && (!in_array($b, $siup))) {
                            $proses[] = '<p> NIB anda belum diizinkan berusaha.</p> <p>Silakan proses melalui OSS</p>';
                            return $proses;
                        } else {
                            if($reskswp->responcheckNIB->dataNIB->status != '01' && (in_array($b, $siup))){
                                $proses[] = '<p> NIB anda belum aktif .</p> <p>Silakan proses melalui OSS</p>';
                                return $proses;
                            }else if ($this->newsession->userdata('nib') == "") {
                                $proses[] = '<p> NIB anda tidak valid.</p> <p>Silakan Update Profile Perusahaan atau klik link <a href="' . site_url() . 'setting/edit_per">Edit Profile Perusahaan.</a></p>';
                                return $proses;
                            }
                        }
                    }
                }
                if ($e == '0000' || $e == '0501') {
                    // $proses[] = '<button class="btn btn-sm btn-success addon-btn m-b-10" id= "' . rand() . '" onclick="proccess(\'#fpreview\',$(this)); return false;" data-status = "' . $row['setujui'] . '"><i class="fa fa-check-square pull-right"></i>Kirim Permohonan</button>';
                    if (trim($arrkswp['id_ksw']) != '') {
                        if ($b == '17') {
                            $id_permohonan = $this->uri->segment('6');
                            $cek_waralaba = "SELECT produk_waralaba FROM t_stpw WHERE id='" . $id_permohonan . "' ";
                            $prod = $this->db->query($cek_waralaba)->row();
                            if ($prod->produk_waralaba == '01') {
                                $reskswp = $this->cek_kswp($b);
                                if ($reskswp->status != 'VALID') {
                                    $proses[] = '<p> NPWP anda tidak valid KSWP.</p> <p>
Silakan menghubungi Kantor Pelayanan Pajak terdekat atau menghubungi 1500200 untuk melakukan validasi KSWP (Valid NPWP dan Telah melaporkan SPT 2 tahun terakhir).</p>
<p>Jika telah melakukan validasi di KPP setempat dan memperoleh Surat Keterangan Validasi NPWP, mohon juga diingatkan kepada petugas KPP agar status valid KSWP diperbaharui di Database Pajak agar proses perizinan dapat dilanjutkan.</p>';
                                    return $proses;
                                }
                            }
                        } else {
                            $reskswp = $this->cek_kswp($b);
                            if ($reskswp->status != 'VALID') {
                                $proses[] = '<p> NPWP anda tidak valid KSWP.</p> <p>
Silakan menghubungi Kantor Pelayanan Pajak terdekat atau menghubungi 1500200 untuk melakukan validasi KSWP (Valid NPWP dan Telah melaporkan SPT 2 tahun terakhir).</p>
<p>Jika telah melakukan validasi di KPP setempat dan memperoleh Surat Keterangan Validasi NPWP, mohon juga diingatkan kepada petugas KPP agar status valid KSWP diperbaharui di Database Pajak agar proses perizinan dapat dilanjutkan.</p>';
                                return $proses;
                            }
                        }
                    }
                    if ($validate['status'] == '01') {
                        $proses[] = '<button class="btn btn-sm btn-success addon-btn m-b-10" id= "' . rand() . '" onclick="proccess(\'#fpreview\',$(this)); return false;" data-status = "' . $row['setujui'] . '"><i class="fa fa-check-square pull-right"></i>Kirim Permohonan</button>';
                    } elseif ($validate['status'] == '03') {
                        $proses[] = '<p> Data Perusahaan Masih Dalam Proses Verifikasi </p>';
//                    } 
//                    elseif ($validate['status'] == '04') {
//                        $proses[] = '<p> Data Perusahaan Tidak Valid, Harap Perbaharui TDP Perusahaan</p>';
                    } elseif ($validate['tipe_registrasi'] == '04') {
                        $proses[] = '<button class="btn btn-sm btn-success addon-btn m-b-10" id= "' . rand() . '" onclick="proccess(\'#fpreview\',$(this)); return false;" data-status = "' . $row['setujui'] . '"><i class="fa fa-check-square pull-right"></i>Kirim Permohonan</button>';
                    } else {
                        $proses[] = '<p> Harap Memperbaharui Data Perusahaan klik <a href="' . site_url() . '/setting/edit_per">Edit Profile Perusahaan</a> </p>';
                    }
                } else {
                    if (in_array($e, $arrRollback)) {
                        // $proses[] = '<button class="btn btn-sm btn-primary addon-btn m-b-10" id= "' . rand() . '" onclick="proccess(\'#fpreview\',$(this)); return false;" data-status = "' . $row['kembali'] . '"><i class="fa fa-undo pull-right"></i>Rollback Permohonan</button>';
                    }
                    $arrterima = array('0100', '0105');
                    if (in_array($e, $arrterima)) {
                        $lbl = "Terima Permohonan";
                    } else {
                        if ($e == '0102')
                            $lbl = "Proses Penerbitan";
                        else
                            $lbl = "Proses Permohonan";
                    }
                    $proses[] = '<button class="btn btn-sm btn-success addon-btn m-b-10" id= "' . rand() . '" onclick="proccess(\'#fpreview\',$(this)); return false;" data-status = "' . $row['setujui'] . '"><i class="fa fa-tags pull-right"></i>' . $lbl . '</button>';
                    if ($e != '0102') {
                        $proses[] = '<button class="btn btn-sm btn-danger addon-btn m-b-10" id= "' . rand() . '" onclick="proccess(\'#fpreview\',$(this),\'tolak\'); return false;" data-status = "' . $row['tolak'] . '"><i class="fa fa-arrow-down pull-right"></i>Tolak Permohonan</button>';
                    }
                    if ($e == '0100') {
                        $proses[] = '<button class="btn btn-sm btn-default addon-btn m-b-10" id= "' . rand() . '" data-url = "' . site_url() . 'licensing/popup_tembusan/' . $b . '/' . $f . '" onclick="popup($(this));return false;" ><i class="fa fa-bars pull-right"></i> Set Tembusan </button>';
                    }
                    if (in_array($e, $arrCetak)) {
                        $proses[] = '<button class="btn btn-sm btn-warning addon-btn m-b-10" id= "' . rand() . '" data-url = "' . site_url() . 'prints/licensing/' . hashids_encrypt($a, _HASHIDS_, 9) . '/' . hashids_encrypt($b, _HASHIDS_, 9) . '/' . $f . '" onclick="redirectblank($(this)); return false;"><i class="fa fa-print pull-right"></i>Cetak Izin</button>';
                    }
                }
            }
        } else {
            if ($e == '1000') {
                $proses[] = '<button class="btn btn-sm btn-info addon-btn m-b-10" id="' . rand() . '" onclick="javascript:window.history.back(); return false;"><i class="fa fa-arrow-left pull-left"></i>Kembali</button>';
                //$proses[] = '<button class="btn btn-sm btn-warning addon-btn m-b-10" id= "' . rand() . '" data-url = "' . site_url() . 'prints/direk/' . hashids_encrypt($a, _HASHIDS_, 9) . '/' . hashids_encrypt($b, _HASHIDS_, 9) . '/' . $f . '" onclick="redirectblank($(this)); return false;"><i class="fa fa-print pull-right"></i>Lihat File Izin</button>';
                $proses[] = '<button class="btn btn-sm btn-warning addon-btn m-b-10" id= "' . rand() . '" data-url = "' . site_url() . 'prints/licensing/' . hashids_encrypt($a, _HASHIDS_, 9) . '/' . hashids_encrypt($b, _HASHIDS_, 9) . '/' . $f . '" onclick="redirectblank($(this)); return false;"><i class="fa fa-print pull-right"></i>Cetak Izin</button>';
                if ($this->newsession->userdata('role') != '05') {
                    $proses[] = '<button class="btn btn-sm btn-success addon-btn m-b-10" id= "' . rand() . '" onclick="proccess(\'#fpreview\',$(this)); return false;" data-status = "resend_email"><i class="fa fa-check-square pull-right"></i>Kirim Email</button>';
                }
            }
        }
        return $proses;
    }

    function set_activity($data) {
        if (!is_array($data)) {
            return false;
        }
        
        $data['waktu'] = 'GETDATE()';
        $data['username'] = $this->newsession->userdata('username');
        $this->db->insert('t_log_user', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }
    
    function set_log_oss() {
       
        
        $resnib = $this->data_nib();    
        $data['json'] = 'kirim permohonan '.$resnib;
        $data['date_request'] = 'GETDATE()';
        $data['nib'] = $this->newsession->userdata('nib');
        $this->db->insert('m_oss', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }else{
            return false;
        }
    }

    function set_loglicensing($data) {
        if (!is_array($data)) {
            return false;
        }
        $data['username'] = $this->newsession->userdata('username');
        $data['waktu'] = 'GETDATE()';
        $data['created'] = 'GETDATE()';
        $this->db->insert('t_log_izin', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }

    function send_email($email, $message, $subject, $attachment = "") {
        $this->load->helper('email');
        $email_success = 0;
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => '10.30.30.248',
            'smtp_port' => 25,
            'smtp_user' => '',
            'smtp_pass' => '',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wrapchars' => 100,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'start_tls' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('sipt@kemendag.go.id', 'SIPT PDN Kemendag');
        $email = str_replace(';', ',', $email);
        $this->email->to($email);
        $array_bcc = array('sipt.kemendag@gmail.com', 'sipt@kemendag.go.id');
        $this->email->bcc($array_bcc);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($attachment != "") {
            $this->email->attach($attachment);
        }
        if ($this->email->send()) {
            $email_success = 1;
        }
        //}
        return $email_success;
    }

    function requirements($a, $b, $c) {
        /*
          @ $a => Permohonan ID
          @ $b => Kode Izin
          @ $c => Type Permohonan;
         */
        $ret = FALSE;
        if ($a != "" && $b != "" && $c != "") {
            if ($c == "Baru") {
                $addWhr .= "baru = 1";
            } else if ($c == "Perubahan") {
                $addWhr .= "perubahan = 1";
            } else if ($c == "Perpanjangan") {
                $addWhr .= "perpanjangan = 1";
            }
            if ($b == '14') {
                $tipe_izin = $this->get_uraian("SELECT a.jensi_garansi FROM t_garansi a WHERE id = '" . $a . "'", 'jensi_garansi');
                $qtipe_izin = "AND tipe_izin = '" . $tipe_izin . "'";
            }
            $qsyarat = "SELECT COUNT(*) AS JMLSYARAT FROM m_dok_izin WHERE izin_id = '" . $b . "' AND tipe = 1 AND kategori NOT IN('03','04') AND " . $addWhr . " " . $qtipe_izin . " ";
            $qupload = "SELECT COUNT(*) AS JMLUPLOAD FROM(SELECT a.dok_id FROM t_upload_syarat a LEFT JOIN m_dok_izin b ON b.izin_id = a.izin_id AND b.dok_id = a.dok_id WHERE a.izin_id = '" . $b . "' AND a.permohonan_id = '" . $a . "' AND detail_id is null AND b.tipe = 1 AND b.kategori NOT IN('03','04') " . $qtipe_izin . " AND b." . $addWhr . " GROUP BY a.dok_id)a";
            //print_r($qupload);die();
            $jmlsyarat = (int) $this->get_uraian($qsyarat, "JMLSYARAT");
            $jmlupload = (int) $this->get_uraian($qupload, "JMLUPLOAD");
            //print_r($jmlsyarat."-".$jmlupload);die();
            if ($jmlupload < $jmlsyarat) {
                $ret = FALSE;
            } else {
                $ret = TRUE;
            }
        }
        return $ret;
    }

    function uridirect($message, $url, $segment = NULL) {
        $data = array("message" => $message,
            "url" => $url,
            "seg" => $segment);
        echo $this->load->view('redirect', $data);
    }

    function company_code($string) {
        if ($string == "") {
            return false;
            exit();
        }
        $kata = explode(" ", strtoupper($kalimat));
        $huruf = "";
        foreach ($kata as $a) {
            $huruf .= substr($a, 0, 1);
        }
        return $huruf;
    }

    function encrypt($pass) {
        $config = $this->config->item('login_key');
        $key = pack('H*', $config);
        $plaintext = $pass;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plaintext, MCRYPT_MODE_CBC, $iv);
        $ciphertext = $iv . $ciphertext;
        $ciphertext_base64 = base64_encode($ciphertext);
        return $ciphertext_base64;
    }

    function dencrypt($ciphertext_base64) {
        $config = $this->config->item('login_key');
        $key = pack('H*', $config);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $ciphertext_dec = base64_decode($ciphertext_base64);
        $iv_dec = substr($ciphertext_dec, 0, $iv_size);
        $ciphertext_dec = substr($ciphertext_dec, $iv_size);
        $plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);
        return $plaintext_dec;
    }

}

?>