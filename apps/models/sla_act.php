<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Sla_act extends CI_Model{

	public function get_sla(){
		if($this->newsession->userdata('_LOGGED')){
			$izin_id = explode('|', $this->input->post('dokumen'));
			$sql = "SELECT a.sla_id, a.sla_waktu, a.sla_status FROM m_waktu_proses a WHERE a.sla_izin = ".$this->db->escape($izin_id[1]);
			$sess = $this->db->query($sql)->result_array();
			$arrdata['nama_izin'] = $this->db->query("SELECT nama_izin FROM m_izin where id = ".$this->db->escape($izin_id[1]))->row_array();
			$query = "	SELECT a.kode, a.uraian 
						FROM m_reff a 
						WHERE a.jenis = 'STATUS' 
						AND a.kode IN('0700', '0100', '0600', '0200', '0701', '0601', '0105')";
			$arrdata['status'] = $this->main->set_combobox($query,"kode","uraian", TRUE);
			$arrdata['sess'] = $sess;
			$arrdata['act'] = site_url('sla/set_sla');
			return $arrdata;
		}
	}

	public function set_sla($isajax){
		if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $ressla = FALSE;
			$arrsla = $this->input->post('sla');
			//print_r($arrsla);die();
			$i = 0;
			foreach ($arrsla['sla_id'] as $key) {
				$arrsave['sla_waktu'] = $arrsla['sla_waktu'][$i];
				$arrsave['sla_status'] = $arrsla['sla_status'][$i];
				$this->db->trans_begin();
				$this->db->where('sla_id', $key);
				$this->db->update('m_waktu_proses', $arrsave);
				if ($this->db->affected_rows() > 0) {
					$logu = array('aktifitas' => 'Mengupdate SLA ke m_waktu_izin',
                    'url' => '{c}' . site_url() . 'sla/set_sla' . ' {m} models/sla_act/set_sla {f} set_sla($isajax)');
                	$this->main->set_activity($logu);
                	$ressla = TRUE;
                	$msg = "MSG||YES||Data berhasil disimpan||REFRESH";
				}
				if ($this->db->trans_status() === FALSE || $ressla == FALSE) {
                	$this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                $i++;
			}
			return $msg;
		}
	}

}
?>