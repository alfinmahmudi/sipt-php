<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pelaporan_act extends CI_Model {

    var $ineng = "";
    var $arrext = array('.jpg', '.JPG', '.jpeg', '.JPEG', '.pdf', ".PDF", ".png", ".PNG");

    function get_preview($dir, $doc, $type, $permohonan_id) {
        if ($this->newsession->userdata('_LOGGED')) {
            // $table = $this->newtable; 
            // $query = "SELECT a.id, a.permohonan_id, a.tgl_lap AS 'Tanggl Pelaporan', '<a href=\"" . site_url() . "download/doc' + substring(a.file_lap, 19, (LEN(a.file_lap)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'File Pelaporan'
            // 			FROM t_lap a 
            // 			WHERE a.permohonan_id = '".$permohonan_id."'  ";
            // $table->title("");
            // $table->columns(array("a.id", "a.permohonan_id", "a.permintaan_id", "a.tgl_lap","a.file_lap"));
            // // $this->newtable->width(array('Nama Pengguna' => 100 , 'Nama Lengkap' => 300, 'Media Komunikasi' => 200, "Akses" => 170, "Status" => 100));
            // $this->newtable->search(array(array("a.tgl_lap","Tanggal Pelaporan")
            // 							  ));
            // $table->cidb($this->db);
            // $table->ciuri($this->uri->segment_array());
            // $table->action(site_url()."licensing/report/".$dir."/".$doc."/".$type."/".$permohonan_id."");
            // $table->orderby(1);
            // $table->sortby("DESC");
            // $table->keys(array("a.id"));
            // $table->hiddens(array("id", "permohonan_id"));
            // $table->show_search(TRUE);
            // $table->show_chk(TRUE);
            // $table->single(FALSE);
            // $table->dropdown(TRUE);
            // $table->hashids(TRUE);
            // $table->postmethod(TRUE);
            // $table->title(TRUE);
            // $table->tbtarget("tb_users");
            // $table->menu(array('Tambah' => array('GET', site_url().'licensing/add_report/form/'.$dir.'/'.$doc.'/'.$permohonan_id, '0', 'home', 'modal'),
            // 			   'Edit' => array('BOOTSTRAPDIALOG', site_url().'licensing/add_report/form/'.$dir.'/'.$doc.'/'.$permohonan_id.'/', '1', 'fa fa-edit'),
            // 			   'Delete' => array('GET', site_url().'licensing/delete_laporan/laporan/ajax/t_lap'.$id, '1', 'fa fa-times','isngajax'),
            // 			   'Pelaporan' => array('POST', site_url().'licensing/laporan/sppgrapt/ajax', 'N', 'fa fa-edit', 'isngajax')
            // 			   ));
            // $arrdata['tblPengadaan'] = $table->generate($query);
            // return $arrdata;

            $table = $this->newtable;
            $query = "SELECT a.id, a.permohonan_id, dbo.dateIndo(a.tgl_lap) AS 'Tanggl Pelaporan', b.uraian as 'Status','<a href=\"" . site_url() . "download/doc' + substring(a.file_lap, 19, (LEN(a.file_lap)-8)) + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'File Pelaporan'
						FROM t_lap a 
						LEFT JOIN m_reff b ON b.kode = a.status AND b.jenis = 'STATUS_LAP'
						WHERE a.permohonan_id = '" . $permohonan_id . "' ";
            //print_r($query);die();
            $table->columns(array("a.id", "a.permohonan_id", "a.permintaan_id", "a.tgl_lap", "a.file_lap"));
            $this->newtable->search(array(array("a.tgl_lap", "Tanggal Pelaporan")
            ));
            $table->title("");
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/report/" . $dir . "/" . $doc . "/" . $type . "/" . $permohonan_id . "");
            $table->keys(array("id"));
            $table->hiddens(array("id", "permohonan_id"));
            $table->sortby("ASC");
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(TRUE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            if ($this->newsession->userdata('role') == '05') {
                $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/add_report/form/' . $dir . '/' . $doc . '/' . $permohonan_id, '0', 'home', 'modal'),
                    'Edit' => array('BOOTSTRAPDIALOG', site_url() . 'licensing/add_report/form/' . $dir . '/' . $doc . '/' . $permohonan_id . '/', '1', 'fa fa-edit'),
                    'Delete' => array('POST', site_url() . 'licensing/delete_laporan/laporan/ajax/t_lap', 'N', 'fa fa-times', 'isngajax'),
                    'Pelaporan' => array('POST', site_url() . 'licensing/laporan/all/ajax', 'N', 'fa fa-edit', 'isngajax')
                ));
            }

            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Laporan Perizinan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function get_data($menu, $dir, $doc, $permohonan_id, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $idx = hashids_decrypt($id, _HASHIDS_, 9);
            $arrdata['id'] = $id;
            $arrdata['permohonan_id'] = $permohonan_id;
            $arrdata['izin_id'] = $doc;
            $arrdata['dir'] = $dir;

            if ($id == "") {
                $arrdata['act'] = site_url('post/report/pelaporan_act/save');
            } else {
                $arrdata['act'] = site_url('post/report/pelaporan_act/update');
                $sql = "SELECT id, tgl_lap FROM t_lap WHERE id = '" . $idx . "' ";
                $arrdata['sess'] = $this->db->query($sql)->row_array();
                $arrdata['update'] = 'update';
            }
            //print_r($arrdata);die();
            return $arrdata;
        }
    }

    function set_data($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $txt = 'Data Pelaporan';
            $msg = "MSG||NO||" . $txt . " gagal disimpan";
            $resgula = FALSE;
            $arrgula = $this->main->post_to_query($this->input->post('gula'));
            $permohonan_id = $this->input->post('permohonan_id');
            $izin_id = $this->input->post('izin_id');
            $dir = $this->input->post('dir');

            $dirPO = './upL04d5/document/PELAPORAN-' . $dir . '-' . $izin_id . '/' . date("Y") . "/" . date("m") . "/" . date("d") . "/";
            //print_r($_FILES['filepo']);die();
            if ($_FILES['filepo']['name'] != "") {
                if (!empty($_FILES['filepo']['error'])) {
                    switch ($_FILES['filepo']['error']) {
                        case '1':
                            $msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
                            return $msg;
                            break;
                        case '2':
                            $msg = 'MSG||NO||The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                            return $msg;
                            break;
                        case '3':
                            $msg = 'MSG||NO||The uploaded file was only partially uploaded';
                            return $msg;
                            break;
                        case '4':
                            $msg = 'MSG||NO||The uploded file was not found';
                            return $msg;
                            break;
                        case '6':
                            $msg = 'MSG||NO||Missing a temporary folder';
                            return $msg;
                            break;
                        case '7':
                            $msg = 'MSG||NO||Failed to write file to disk';
                            return $msg;
                            break;
                        case '8':
                            $msg = 'MSG||NO||File upload stopped by extension';
                            return $msg;
                            break;
                        default:
                            return "MSG||NO||File upload Error";
                    }
                } elseif (!in_array($this->main->allowed($_FILES['filepo']['name']), $this->arrext)) {
                    $msg = "MSG||NO||Upload File PO hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png";
                    return $msg;
                } else if (empty($_FILES['filepo']['tmp_name']) || $_FILES['filepo']['tmp_name'] == 'none') {
                    $msg = 'MSG||NO||File PO Upload tidak di temukan';
                    return $msg;
                }
                if (file_exists($dirPO) && is_dir($dirPO)) {
                    $config['upload_path'] = $dirTDP;
                } else {
                    if (mkdir($dirPO, 0777, true)) {
                        if (chmod($dirPO, 0777)) {
                            $config['upload_path'] = $dirPO;
                        }
                    }
                }
                if ($_FILES['filepo'] != null) {
                    $config4['upload_path'] = $dirPO;
                    $config4['allowed_types'] = 'jpg|jpeg|pdf|png';
                    $config4['remove_spaces'] = TRUE;
                    $ext4 = pathinfo($_FILES['filepo']['name'], PATHINFO_EXTENSION);
                    $config4['file_name'] = "PL-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext4;
                    $this->load->library('upload');
                    $this->upload->initialize($config4);
                    $this->upload->display_errors('', '');

                    $this->upload->do_upload("filepo");
                    $dataTDP = $this->upload->data("filepo");
                }
                $tmpPO = $dirPO . $config4['file_name'];
                $arrgula['file_lap'] = $tmpPO;
            }

            if ($act == "save") {
                $arrgula['permohonan_id'] = $permohonan_id;
                $arrgula['status'] = '0000';
                $arrgula['created'] = 'GETDATE()';
                $arrgula['created_by'] = $this->newsession->userdata('username');
                //print_r($arrgula);die();
                $this->db->trans_begin();
                $exec = $this->db->insert('t_lap', $arrgula); //print_r($this->db->last_query());die();
                if ($this->db->affected_rows() > 0) {
                    $resgula = TRUE;
                    // $msg = "MSG||YES||".$txt." berhasil disimpan.||REFRESH";
                    $msg = "MSG||YES||" . $txt . " berhasil disimpan.||site_url('licensing/report/" . $dir . "/" . $doc . "/" . $type . "/" . $id . "')";
                    $logu = array('aktifitas' => 'Menambahkan Pelaporan dengan Tanggal PO :  ' . $arrgula['tgl_lap'],
                        'url' => '{c}' . site_url() . 'post/report/pelaporan_act/save' . ' {m} models/report/pelaporan_act {f} set_data($act, $isajax)');
                    $this->main->set_activity($logu);
                }
                if ($this->db->trans_status() === FALSE || !$resgula) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } elseif ($act == "update") {
                $id = $this->input->post('id');
                $idx = hashids_decrypt($id, _HASHIDS_, 9);
                $this->db->where('id', $id);
                $this->db->trans_begin();
                $this->db->update('t_lap', $arrgula);
                if ($this->db->affected_rows() > 0) {
                    $resgula = TRUE;
                    $msg = "MSG||YES||" . $txt . " berhasil diupdate.||REFRESH";
                }
                if ($this->db->trans_status() === FALSE || !$resgula) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            }
        }
    }

    function del_laporan($data, $tabel) {
        $this->db->trans_begin();
        $i = 0;
        foreach ($data as $id) {
            $hasid = hashids_decrypt($id, _HASHIDS_, 9);
            $this->db->where('id', $hasid);
            $this->db->delete($tabel);
            if ($this->db->affected_rows() == 0) {
                $i++;
                break;
            }
        }
        if ($this->db->trans_status === FALSE || $i > 0) {
            $this->db->trans_rollback();
            $msg = "MSG#Proses Gagal#refresh";
        } else {
            $this->db->trans_commit();
            $msg = "MSG#Proses Berhasil#refresh";
        }
        return $msg;
    }

    function set_laporan($data) {
        $this->db->trans_begin();
        $i = 0;
        foreach ($data as $id) {
            $idx = hashids_decrypt($id, _HASHIDS_, 9);
            $this->db->where("id", $idx);
            $this->db->update('t_lap', array("status" => "0100"));
            if ($this->db->affected_rows() == 0) {
                $i++;
                break;
            }
        }
        if ($this->db->trans_status() === FALSE || $i > 0) {
            $this->db->trans_rollback();
            $msg = "MSG#Proses Gagal#refresh";
        } else {
            $this->db->trans_commit();
            $msg = "MSG#Proses Berhasil#refresh";
        }
        return $msg;
    }

}

?>