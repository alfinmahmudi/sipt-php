<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Siupmb_act extends CI_Model{
	
	var $ineng = "";
	
	function get_lstPengadaan($dir, $doc, $type, $permohonan_id){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable;
			if($this->newsession->userdata('role') == "01"){
				$sql = "AND a.status = '0100'";
			}
			$query = "SELECT id, permohonan_id, triwulan AS Triwulan, lap_tahun AS Tahun, golongan AS Golongan, a.jenis AS Jenis, jml_dn AS 'Jumlah Dalam Negeri', jml_imp AS 'Jumlah Impor', b.nama AS 'Negara Impor', m_reff.uraian AS Status
						FROM t_lap_pengadaanmb a 
						LEFT JOIN m_negara b on a.negara_imp=b.kode
						LEFT JOIN m_reff on a.status = m_reff.kode AND m_reff.jenis='STATUS'
						WHERE permohonan_id = ".$permohonan_id. $sql;
			$table->title("");
			$table->columns(array("triwulan","lap_tahun", "golongan", "a.jenis", "jml_dn", "jml_imp", "negara_imp", "status"));
			$this->newtable->width(array('Triwulan' => 10, 'Tahun' => 10, 'Golongan' => 50, 'Jenis' => 50, 'Jumlah' => 50, 'Jumlah Dalam Negeri' => 80, 'Jumlah Impor' => 50, 'Negara Impor'=>100, 'Status' => 20));
			$this->newtable->search(array(array("lap_tahun","Tahun"),
										  array("golongan","Golongan"),
										  array("status","Status")
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/report/".$dir."/".$doc."/".$type."/".$permohonan_id.".pengadaan/");		
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("id"));
			$table->hiddens(array("id","permohonan_id"));
			$table->show_search(TRUE);
			$table->single(TRUE);
			$table->dropdown(TRUE);
			$table->hashids(TRUE);
			$table->postmethod(TRUE);
			$table->tbtarget("tb_lstPengadaan".rand());
			if($this->newsession->userdata('role') == "05"){
			$table->menu(array('Tambah' => array('GET', site_url().'licensing/add_report/pengadaanmb/'.$dir.'/'.$doc.'/'.$permohonan_id, '0', 'home', 'modal'),
						   'Edit' => array('BOOTSTRAPDIALOG', site_url().'licensing/add_report/pengadaanmb/'.$dir.'/'.$doc.'/'.$permohonan_id.'/', '1', 'fa fa-edit'),
						   'Delete' => array('POST', site_url().'licensing/delete_laporan/siupmb/ajax/t_lap_pengadaanmb', 'N', 'fa fa-times','isngajax'),
						   'Pelaporan' => array('POST', site_url().'licensing/laporan/siupmb/ajax/pengadaan', 'N', 'fa fa-edit', 'isngajax')
						   ));
			
			$table->show_chk(TRUE);			   
			}else{
			$table->show_chk(FALSE);
			}
			
			$arrdata['tblPengadaan'] = $table->generate($query);
			$table->clear();
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	function get_lstPenyaluran($dir, $doc, $type, $permohonan_id){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable;
			if($this->newsession->userdata('role') == "01"){
				$sql = " AND a.status = '0100'";
			}
			$query = "SELECT a.id, a.permohonan_id, a.triwulan AS Triwulan, a.lap_tahun AS Tahun, a.golongan AS Golongan, a.jenis AS Jenis, a.jml AS Jumlah, m_reff.uraian AS Status FROM t_lap_penyaluranmb a
						LEFT JOIN m_reff on a.status = m_reff.kode AND m_reff.jenis='STATUS'
						WHERE a.permohonan_id = ".$permohonan_id.$sql;
			$table->title("");
			$table->columns(array("triwulan","lap_tahun", "golongan", "a.jenis", "jml", "status"));
			$this->newtable->width(array('Triwulan' => 20, 'Tahun' => 20, 'Golongan' => 50, 'Jenis' => 50, 'Jumlah' => 50, 'Status' => 50));
			$this->newtable->search(array(array("a.lap_tahun","Tahun"),
										  array("a.jenis","Jenis"),
										  array("a.status","Status")
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/report/".$dir."/".$doc."/".$type."/".$permohonan_id.".penyaluran/");
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("id"));
			$table->hiddens(array("id","permohonan_id"));
			$table->show_search(TRUE);
			$table->single(TRUE);
			$table->dropdown(TRUE);
			$table->hashids(TRUE);
			$table->postmethod(TRUE);
			$table->tbtarget("tb_lstPenyaluran".rand());
			if($this->newsession->userdata('role') == "05"){
			$table->menu(array('Tambah' => array('GET', site_url().'licensing/add_report/penyaluranmb/'.$dir.'/'.$doc.'/'.$permohonan_id, '0', 'home', 'modal'),
						   'Edit' => array('BOOTSTRAPDIALOG', site_url().'licensing/add_report/penyaluranmb/'.$dir.'/'.$doc.'/'.$permohonan_id.'/', '1', 'fa fa-edit'),
						   'Delete' => array('POST', site_url().'licensing/delete_laporan/siupmb/ajax/t_lap_penyaluranmb', 'N', 'fa fa-times','isngajax'),
						   'Pelaporan' => array('POST', site_url().'licensing/laporan/siupmb/ajax/penyaluran', 'N', 'fa fa-edit','isngajax')
						   ));
			$table->show_chk(TRUE);			   
			}else{
			$table->show_chk(FALSE);
			}
			
			$arrdata['tblPenyaluran'] = $table->generate($query);
			$table->clear();
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	function get_data($menu, $dir, $doc, $permohonan_id,$id){
		if($this->newsession->userdata('_LOGGED')){
			$idx = hashids_decrypt($id,_HASHIDS_,9);
			$arrdata['id'] = $id;
			$arrdata['permohonan_id'] = $permohonan_id;
			$arrdata['negara_imp'] =  $this->main->set_combobox("SELECT kode, nama FROM m_negara ORDER BY 2","kode","nama", TRUE);
			$arrdata['triwulan'] = array("1"=>"1","2"=>"2","3"=>"3","4"=>"4");
			$arrdata['golongan'] = array("Golonga A"=>"Golonga A","Golonga B"=>"Golonga B","Golonga C"=>"Golonga C");

			if($id==""){
				$arrdata['act'] = site_url('post/report/siupmb_act/'.$menu.'/save');				
			}else{
				$arrdata['act'] = site_url('post/report/siupmb_act/'.$menu.'/update');
				if($menu == 'pengadaanmb'){
					$query = "SELECT triwulan, lap_tahun, golongan, jenis, jml_dn, jml_imp, negara_imp 
							  FROM t_lap_pengadaanmb 
							  WHERE permohonan_id = '".$permohonan_id."' and id = '".$idx."'";
				}elseif ($menu == 'penyaluranmb') {
					$query = "SELECT triwulan, lap_tahun, golongan, jenis, jml
							  FROM t_lap_penyaluranmb 
							  WHERE permohonan_id = '".$permohonan_id."' and id = '".$idx."'";
				}
                              //  die($query);
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['sess'] = $row;
					}
				}
			}
			//print_r($arrdata);die();
			return $arrdata;
		}

	}

	function set_pengadaanmb($act,$isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			$txt ='Data Laporan Pengadaan Minuman Beralkohol';
			$msg = "MSG||NO||".$txt." gagal disimpan";
			$ressiup = FALSE;
			$arrsiup = $this->main->post_to_query($this->input->post('siupmb'));
			$permohonan_id = $this->input->post('permohonan_id');
			if($act == "save"){
				$arrsiup['permohonan_id'] = $permohonan_id;
				$arrsiup['status'] = '0000';
				$arrsiup['created'] = 'GETDATE()';
				$arrsiup['created_user'] = $this->newsession->userdata('username');
				//print_r($arrsiup);die();
				$this->db->trans_begin();
				$exec = $this->db->insert('t_lap_pengadaanmb', $arrsiup);
				if($this->db->affected_rows() > 0){
					$idredir = $this->db->insert_id();
					$ressiup = TRUE;
					
					$msg = "MSG||YES||".$txt." berhasil disimpan.||REFRESH";
					
					/* Log User dan Log Izin */
					/* Akhir Log User dan Log Izin */

				}
				if($this->db->trans_status() === FALSE || !$ressiup){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				return $msg;
			}elseif ($act == "update") {
				$id = $this->input->post('id');
				$idx = hashids_decrypt($id,_HASHIDS_,9);
				$this->db->where('id',$idx);
				$this->db->where('permohonan_id',$permohonan_id);
				$this->db->update('t_lap_pengadaanmb',$arrsiup);
				if($this->db->affected_rows() > 0){
					$ressiup = TRUE;
					$msg = "MSG||YES||".$txt." berhasil diupdate.||REFRESH";

				}
				return $msg;
			}
		}
	}

	function set_penyaluranmb($act,$isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			$txt ='Data Laporan Penyaluran Minuman Berlkohol';
			$msg = "MSG||NO||".$txt." gagal disimpan";
			$ressiup = FALSE;
			$arrsiup = $this->main->post_to_query($this->input->post('siupmb'));
			$permohonan_id = $this->input->post('permohonan_id');
			if($act == "save"){
				$arrsiup['permohonan_id'] = $permohonan_id;
				$arrsiup['status'] = '0000';
				$arrsiup['created'] = 'GETDATE()';
				$arrsiup['created_user'] = $this->newsession->userdata('username');
				
				$this->db->trans_begin();
				$exec = $this->db->insert('t_lap_penyaluranmb', $arrsiup);
				if($this->db->affected_rows() > 0){
					$idredir = $this->db->insert_id();
					$ressiup = TRUE;
					
					$msg = "MSG||YES||".$txt." berhasil disimpan.||REFRESH";
					
					/* Log User dan Log Izin */
					/* Akhir Log User dan Log Izin */

				}
				if($this->db->trans_status() === FALSE || !$ressiup){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				return $msg;
			}elseif ($act == "update") {
				$id = $this->input->post('id');
				$idx = hashids_decrypt($id,_HASHIDS_,9);
				$this->db->where('id',$idx);
				$this->db->where('permohonan_id',$permohonan_id);
				$this->db->update('t_lap_penyaluranmb',$arrsiup);
				if($this->db->affected_rows() > 0){
					$ressiup = TRUE;
					$msg = "MSG||YES||".$txt." berhasil diupdate.||REFRESH";

				}
				return $msg;
			}
		}
	}
	
	function del_laporan($data,$tabel){
		$this->db->trans_begin();
		$i=0;
		foreach($data as $id){
			$hasid = hashids_decrypt($id,_HASHIDS_,9);
			$this->db->where('id',$hasid);
			$this->db->delete($tabel);
			if($this->db->affected_rows() == 0){
				$i++;
				break;
			}
		}
		if($this->db->trans_status === FALSE || $i > 0){
			$this->db->trans_rollback();
			$msg = "MSG#Proses Gagal#refresh";
		}else{
			$this->db->trans_commit();
			$msg = "MSG#Proses Berhasil#refresh";
		}
		return $msg;
		
	}
	
	function set_laporan($data,$dtl){
		$this->db->trans_begin();
			$i=0;
			foreach($data as $id){
				$idx = hashids_decrypt($id,_HASHIDS_,9);
				$this->db->where("id",$idx);
				if($dtl == "pengadaan"){
					$tbl = "t_lap_pengadaanmb";
				}else{
					$tbl = "t_lap_penyaluranmb";
				}
				$this->db->update($tbl,array("status"=>"0100"));
				if($this->db->affected_rows() == 0){
					$i++;
					break;
				}
			}
			if($this->db->trans_status() === FALSE || $i > 0){
				$this->db->trans_rollback();
				$msg = "MSG#Proses Gagal#refresh";
			}else{
				$this->db->trans_commit();
				$msg = "MSG#Proses Berhasil#refresh";
			}
		return $msg;
	}

}
?>