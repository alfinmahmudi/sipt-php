<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sipdis_act extends CI_Model {

    var $ineng = "";
    var $arrext = array('.jpg', '.JPG', '.jpeg', '.JPEG', '.pdf', ".PDF", ".png", ".PNG");

    function get_preview($dir, $doc, $type, $permohonan_id) {
//        print_r($dir ."/". $doc."/". $type."/". $permohonan_id);die;
        if ($this->newsession->userdata('_LOGGED')) {
            $addwrh = '';
            // if ($this->newsession->userdata('role') != '05') {
            //     $addwrh = "AND a.fl_kirim = '0100'";
            // }
            $table = $this->newtable;
            $query = "SELECT a.id, a.permohonan_id, 
					CASE
					  WHEN LEN(a.bulan) = 1 THEN dbo.get_bulan(CONCAT('0', a.bulan))
					  ELSE dbo.get_bulan(a.bulan)
					END AS 'Bulan', 
					a.tahun AS 'Tahun', a.pengadaan AS 'Pemasukan', a.penyaluran AS 'Penyaluran', 
					a.stok_awl AS 'Stock Awal', a.stok_akhr AS 'Stock Akhir', b.urai_satuan AS 'Satuan', 
					b.uraian AS 'Jenis'
					FROM t_lap_bapok a
					LEFT JOIN m_jenis_bapok b ON b.id = a.bapok
					LEFT JOIN m_reff c ON c.kode = a.fl_kirim AND c.jenis = 'STATUS_TDPUD'
                    WHERE a.permohonan_id = '" . $permohonan_id . "' ".$addwrh." ";
            //print_r($query);die();
            $table->columns(array("a.id", "a.permohonan_id", "a.permintaan_id", "a.tgl_lap", "a.file_lap"));
            // $this->newtable->search(array(array("a.permintaan_id", "Nomor PO"),
            //     array("a.tgl_lap", "Tanggal PO")));
            $table->title("");
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/report/" . $dir . "/" . $doc . "/" . $type . "/" . $permohonan_id . "");
            $table->detail(site_url()."pelaporan/get_detil");
            $table->keys(array("id"));
            $table->hiddens(array("id", "permohonan_id"));
            $table->sortby("ASC");
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(TRUE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->expandrow(TRUE);
            $table->tbtarget("tb_status");
            if ($this->newsession->userdata('role') != '05') {
                $table->menu(array('Rollback' => array('POST', site_url() . 'pelaporan/rollback_rep/ajax', 'N', 'fa fa-edit', 'isngajax')
                ));
            }else {
                $table->menu(array('Tambah' => array('GET', site_url() . 'licensing/add_report/form/' . $dir . '/' . $doc . '/' . $permohonan_id, '0', 'home', 'isngajax'),
                // 'Edit' => array('GET', site_url() . 'licensing/add_report/form/' . $dir . '/' . $doc . '/' . $permohonan_id, '1', 'fa fa-edit'),
                'Edit' => array('POST', site_url() . 'pelaporan/check/' . $dir . '/' . $doc . '/' . $permohonan_id.'/ajax', '1', 'fa fa-edit', 'isngajax'),
                'Delete' => array('POST', site_url() . 'licensing/delete_laporan/sipdis/ajax/t_lap_bapok', 'N', 'fa fa-times', 'isngajax')
                // 'Kirim' => array('POST', site_url() . 'licensing/laporan/sipdis/ajax', 'N', 'fa fa-edit', 'isngajax')
                ));
            }
            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Laporan Perizinan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function get_data($menu, $dir, $doc, $permohonan_id, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $idx = hashids_decrypt($id, _HASHIDS_, 9);
            $bln = '';
            $tahun = '';
            for ($i=0; $i <= 12; $i++) { 
                if ($i == 0) {
                    $bln[] = '&nbsp;';
                }else{
                	if ($i <= 9) {
                		$get_urai = "select dbo.get_bulan('0".$i."') as 'bln'";
                	}else{
                		$get_urai = "select dbo.get_bulan('".$i."') as 'bln'";
                	}
                	$urai_bulan = $this->db->query($get_urai)->row_array();
                    $bln[] = $urai_bulan['bln'];
                }
            }
            $plus = (int)date('Y') + 5;
//            if(date('M') == 'Jan'){
//                $tahun[] = '&nbsp;';
//                $tahun[date('Y')-1] = date('Y')-1;
//            }
            for ($i=(int)date('Y')-1; $i <= $plus; $i++) {
                if ($i == date('Y')-1) {
                    $tahun[] = '&nbsp;';
                    $tahun[date('Y')-1] = date('Y')-1;
                }else{
                    $tahun[$i] = $i;
                }
            }
//            print_r(date('M'));die();
            // print_r($tahun);
            // print_r($bln);
            // die();
            $get_jns = "SELECT c.id, c.uraian
						FROM t_bapok a
						LEFT JOIN t_bapok_brg b ON b.permohonan_id = a.id
						LEFT JOIN m_jenis_bapok c ON c.id = b.jns_brg 
						WHERE a.id = '".$permohonan_id."'";
			$sql_get = "SELECT pemasaran_kab, pemasaran_prop FROM t_bapok WHERE id = '".$permohonan_id."' ";
			$masdat = $this->db->query($sql_get)->row_array();
            $whr_prop = '';
            $whr_kab = '';
            // print_r(substr($masdat['pemasaran_prop'], 0, 1));die();
            if (substr($masdat['pemasaran_prop'], 0, 1) == ',') {
                $masdat['pemasaran_prop'] = substr($masdat['pemasaran_prop'], 2);
            }
            if (substr($masdat['pemasaran_kab'], 0, 1) == ',') {
                $masdat['pemasaran_kab'] = substr($masdat['pemasaran_kab'], 2);
            }
            //print_r($masdat['pemasaran_kab']);die();
			if (trim($masdat['pemasaran_prop']) != '' && trim($masdat['pemasaran_kab']) != '') {
				$whr_prop = "WHERE a.id IN(".$masdat['pemasaran_prop'].")";
				$whr_kab = "WHERE b.id IN(".$masdat['pemasaran_kab'].")";
			}elseif (trim($masdat['pemasaran_kab']) != '') {
                $whr_prop = "WHERE a.id IN('')";
				$whr_kab = "WHERE b.id IN(".$masdat['pemasaran_kab'].")";
			}elseif (trim($masdat['pemasaran_prop']) != '') {
				$whr_prop = "WHERE a.id IN(".$masdat['pemasaran_prop'].")";
                $whr_kab = "WHERE b.id_prov IN(".$masdat['pemasaran_prop'].")";
			}
			$get_prop = "SELECT a.nama, a.id, 1 as 'kode' 
						FROM m_prop a
						".$whr_prop."
						UNION SELECT b.nama, b.id, 2 as 'kode' 
						FROM m_kab b
						".$whr_kab."
						ORDER BY kode";//print_r($get_prop);die();
			$arrdata['kab_pem'] = $this->main->set_combobox($get_prop, "id", "nama", TRUE);
			$get_bis = "SELECT a.nama, a.id, 1 as 'kode' FROM m_prop a UNION SELECT b.nama, b.id, 2 as 'kode' FROM m_kab b ORDER BY kode";
			$arrdata['kab'] = $this->main->set_combobox($get_bis, "id", "nama", TRUE);
			$arrdata['negara'] = $this->main->set_combobox("SELECT kode, nama FROM m_negara", "kode", "nama", TRUE);
            $arrdata['bulan'] = $bln;
            $arrdata['tahun'] = $tahun;
            $arrdata['id'] = $id;
            $arrdata['permohonan_id'] = $permohonan_id;
            $arrdata['jns_bapok'] = $this->main->set_combobox($get_jns, "id", "uraian", TRUE);
            $arrdata['sat_bapok'] = $this->main->set_combobox("SELECT a.id, a.urai_satuan FROM m_jenis_bapok a", "id", "urai_satuan", TRUE);
            // $arrdata['kab'] = $this->main->set_combobox("SELECT a.nama, a.id, 1 as 'kode' FROM m_prop a UNION SELECT b.nama, b.id, 2 as 'kode' FROM m_kab b ORDER BY kode", "id", "nama", TRUE);
            // $arrdata['kab_pem'] = $this->main->set_combobox($get_prop, "id", "nama", TRUE);print_r($arrdata['kab_pem']);die();
            if ($id == "") {
                // $cek_attemp = "SELECT id FROM t_lap_bapok WHERE permohonan_id = '".$permohonan_id."'";
                // $attemp = $this->db->query($cek_attemp)->num_rows();
                // if ($attemp > 0) {
                //     $baru = "1";
                // }
                $arrdata['act'] = site_url('pelaporan/set_detil/save');
            } else {
                $arrdata['act'] = site_url('pelaporan/set_detil/update');
                $sql = "SELECT a.bulan, a.tahun, a.stok_awl, a.stok_akhr,a.bapok
                        FROM t_lap_bapok a
                        WHERE a.permohonan_id = '".$permohonan_id."' AND a.id = '" . $idx . "' ";
                $arrdata['sess'] = $this->db->query($sql)->row_array();
                $get_det = "SELECT a.jumlah, a.daerah, a.tipe, b.nama as kab, c.nama as prop, d.nama as ngr, a.negara, a.fl_impor
                            FROM t_lap_bapok_dtl a
                            LEFT JOIN m_kab b ON b.id = a.daerah
                            LEFT JOIN m_prop c ON c.id = a.daerah
                            LEFT JOIN m_negara d on d.kode = a.negara
                            WHERE a.lap_id = '".$idx."'";//print_r($get_det);die();
                $arrdata['detil'] = $this->db->query($get_det)->result_array();
                $arrdata['update'] = 'update';
            }
            //print_r($arrdata);die();
            return $arrdata;
        }
    }

    function get_detil($id){
        $id_news = hashids_decrypt($id,_HASHIDS_,9);                    
        $sql = "SELECT a.jumlah, a.daerah, a.tipe, 
                CASE
                    WHEN LEN(a.daerah) = 2 THEN c.nama
                    WHEN a.daerah = '' THEN d.nama
                    ELSE b.nama
                END AS 'nama'
                FROM t_lap_bapok_dtl a
                LEFT JOIN m_kab b ON b.id = a.daerah
                LEFT JOIN m_prop c ON c.id = a.daerah
                LEFT JOIN m_negara d ON d.kode = a.negara
                WHERE a.lap_id = '".$id_news."' ";
        $data = $this->db->query($sql)->result_array();
        $arrdata = array('data' => $data);
        return $arrdata;
    }

    function rollback_rep($id){
        if ($this->newsession->userdata('_LOGGED')) {
            $resudate = FALSE;
            $msg = "MSG#Laporan Gagal Rollback#ref";
            foreach ($id as $key) {
                $idx = hashids_decrypt($key, _HASHIDS_, 9);
                $up_stat = array(
                        'fl_kirim' => '0000' 
                    );
                $this->db->trans_begin();
                $this->db->where('id', $idx);
                $this->db->update('t_lap_bapok', $up_stat);
                if ($this->db->affected_rows() > 0) {
                    $resudate = TRUE;
                    $msg = "MSG#Laporan Berhasil Di Rollback#refresh";
                }

                if ($this->db->trans_status() === FALSE || !$resudate) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
            }
            return $msg;
        }
    }

    function val_edit($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $permohonan_id = $this->input->post('permohonan_id');
            $bapok = $this->input->post('bapok');
           	$cek = "SELECT COUNT(a.id) as jml
					FROM t_lap_bapok a
					WHERE a.permohonan_id = '".$permohonan_id."' AND a.bapok = '".$bapok."'";
			$jum = $this->db->query($cek)->row_array();
			if ($jum['jml'] > 1) {
				$ret = 'TRUE';
			}else{
				$ret = 'FALSE';
			}
			return $ret;
        }
    }

    function cek_sess($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $permohonan_id = $this->input->post('permohonan_id');
            $bapok = $this->input->post('bapok');
            $lap_id = $this->input->post('id');
   //          $cek = "SELECT COUNT(a.id) as jml
			// 		FROM t_lap_bapok a
			// 		WHERE a.permohonan_id = '".$permohonan_id."' AND a.bapok = '".$bapok."'";
			// $jum = $this->db->query($cek)->row_array();
			$get_data = "SELECT top 1 a.bulan, a.tahun, a.pengadaan, a.penyaluran, a.stok_akhr, a.stok_awl, 
						CASE
                          WHEN LEN(a.bulan) = 1 THEN dbo.get_bulan(CONCAT('0', a.bulan))
                          ELSE dbo.get_bulan(a.bulan)
                        END AS 'ref_bulan'
                        FROM t_lap_bapok a
                        WHERE a.permohonan_id = '".$permohonan_id."' AND a.bapok = '".$bapok."'
                        ORDER BY CONVERT(int, a.tahun) DESC, CONVERT(int, a.bulan) DESC";
            $arrdata = $this->db->query($get_data)->row_array();
            if (trim($lap_id) == '') {
	            if ($arrdata['bulan'] == 1) {
	                $arrdata['ref_bulan'] = 'Februari';
	            }elseif ($arrdata['bulan'] == 2) {
	                $arrdata['ref_bulan'] = 'Maret';
	            }elseif ($arrdata['bulan'] == 3) {
	                $arrdata['ref_bulan'] = 'April';
	            }elseif ($arrdata['bulan'] == 4) {
	                $arrdata['ref_bulan'] = 'Mei';
	            }elseif ($arrdata['bulan'] == 5) {
	                $arrdata['ref_bulan'] = 'Juni';
	            }elseif ($arrdata['bulan'] == 6) {
	                $arrdata['ref_bulan'] = 'Juli';
	            }elseif ($arrdata['bulan'] == 7) {
	                $arrdata['ref_bulan'] = 'Agustus';
	            }elseif ($arrdata['bulan'] == 8) {
	                $arrdata['ref_bulan'] = 'September';
	            }elseif ($arrdata['bulan'] == 9) {
	                $arrdata['ref_bulan'] = 'Oktober';
	            }elseif ($arrdata['bulan'] == 10) {
	                $arrdata['ref_bulan'] = 'November';
	            }elseif ($arrdata['bulan'] == 11) {
	                $arrdata['ref_bulan'] = 'Desember';
	            }elseif ($arrdata['bulan'] == 12) {
	                $arrdata['ref_bulan'] = 'Januari';
	            }
	        }
            $get_satuan = "SELECT urai_satuan FROM m_jenis_bapok WHERE id = '".$bapok."' ";
            $res_sat = $this->db->query($get_satuan)->row_array();
            $data = array('satuan' => $res_sat['urai_satuan'],
            			'res' => $arrdata);
            // print_r($data);die();
            return $data;  
        }
    }    

    function set_detil($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }

            $txt = 'Data Laporan Tanda Daftar Pelaku Usaha Distribusi Barang Kebutuhan Pokok';
            $msg = "MSG||NO||" . $txt . " gagal disimpan";
            $resgula = FALSE;

            $id = $this->input->post('id');
            $idx = hashids_decrypt($id, _HASHIDS_, 9);
            $permohonan_id = $this->input->post('permohonan_id');
            $arrmas = $this->input->post('gula');//print_r($arrmas['tahun']);die();
            $arrdet = $this->input->post('dtl');//print_r($arrdet);die();

            $arrmas['stok_awl'] = str_replace(",", ".", $arrmas['stok_awl']);
            $tamp = explode(".", $arrmas['stok_awl']);
            if (count($tamp) <= 2) {
                foreach ($tamp as $key) {
                    if (!is_numeric($key)) {
                        $msg = "MSG||NO||Inputan Jumlah Harus Angka";
                        return $msg;
                    }
                }
            }else{
                $msg = "MSG||NO||Inputan Jumlah Tidak Sesuai";
                return $msg;
            }

            // foreach ($arrdet['jum'] as $det) {
            //     $det = str_replace(",", ".", $det);
            //     $tamp = explode(".", $det);
            //     if (count($tamp) <= 2) {
            //         foreach ($tamp as $key) {
            //             if (!is_numeric($key)) {
            //                 $msg = "MSG||NO||Inputan Jumlah Harus Angka";
            //                 return $msg;
            //             }
            //         }
            //     }else{
            //         $msg = "MSG||NO||Inputan Jumlah Tidak Sesuai";
            //         return $msg;
            //     }
            // }

            $pengadaan = 0;
            $penyaluran = 0;

            for ($i=0; $i < count($arrdet['tipe']) ; $i++) {
                if ($arrdet['tipe'][$i] == '1') {
                    $pengadaan = $pengadaan + (float)$arrdet['jum'][$i]; 
                }elseif ($arrdet['tipe'][$i] == '2') {
                    $penyaluran = $penyaluran + (float)$arrdet['jum'][$i]; 
                }
            }

            $get_satuan = "SELECT urai_satuan FROM m_jenis_bapok WHERE id = '".$arrmas['bapok']."' ";
            $res_sat = $this->db->query($get_satuan)->row_array();
            $satuan = $res_sat['urai_satuan'];

            $arrmas['fl_kirim'] = '0000';
            $arrmas['permohonan_id'] = $permohonan_id;
            $arrmas['pengadaan'] = $pengadaan;
            $arrmas['penyaluran'] = $penyaluran;
            $arrmas['created'] = 'GETDATE()';
            $arrmas['createdby'] = $this->newsession->userdata('username');

            if ($act == 'save') {
                $this->db->trans_begin();
                $exec = $this->db->insert('t_lap_bapok', $arrmas); //print_r($this->db->last_query());die();
                if ($this->db->affected_rows() > 0) {
                    $lap_id = $this->db->insert_id();
                    for ($i=0; $i < count($arrdet['jum']) ; $i++) {
                        $arrins['lap_id'] = $lap_id;
                        $arrins['jumlah'] = $arrdet['jum'][$i];
                        $arrins['daerah'] = $arrdet['asal'][$i];
                        $arrins['negara'] = $arrdet['negara'][$i];
                        $arrins['fl_impor'] = $arrdet['fl_impor'][$i];
                        $arrins['satuan'] = $satuan;
                        $arrins['tipe'] = $arrdet['tipe'][$i];
                        $arrins['createdby'] = $this->newsession->userdata('username');
                        $this->db->insert('t_lap_bapok_dtl', $arrins); //print_r($this->db->last_query());die();
                    }
                    $resgula = TRUE;
                }
                if ($this->db->trans_status() === FALSE || !$resgula) {
                    $this->db->trans_rollback();
                } else {
                	// $msg = "MSG||YES||".$txt." berhasil disimpan.||REFRESH";
                    $msg = "MSG||YES||" . $txt . " berhasil disimpan.||REFRESH";
                    $logu = array('aktifitas' => 'Menambahkan Pelaporan TDPUD dengan periode Bulan :  ' . $arrmas['bulan']. ' Tahun : '.$arrmas['tahun'],
                        'url' => '{c}' . site_url() . 'post/report/sipdis_act/save' . ' {m} models/report/sipdis_act {f} set_detil($act, $isajax)');
                    $this->main->set_activity($logu);
                    $this->db->trans_commit();
                }
            }else{
                $this->db->trans_begin();
                $this->db->where('id', $idx);
                $this->db->where('permohonan_id', $permohonan_id);
                $exec = $this->db->update('t_lap_bapok', $arrmas); //print_r($this->db->last_query());die();
                if ($this->db->affected_rows() > 0) {
                    $cek_det = "SELECT id FROM t_lap_bapok_dtl WHERE lap_id = '".$idx."'";
                    $jml_det = $this->db->query($cek_det)->num_rows();
                    if ($jml_det > 0) {
                        $this->db->where('lap_id', $idx);
                        $this->db->delete('t_lap_bapok_dtl');
                    }

                    for ($i=0; $i < count($arrdet['jum']) ; $i++) {
                        $arrins['lap_id'] = $idx;
                        $arrins['jumlah'] = $arrdet['jum'][$i];
                        $arrins['daerah'] = $arrdet['asal'][$i];
                        $arrins['negara'] = $arrdet['negara'][$i];
                        $arrins['fl_impor'] = $arrdet['fl_impor'][$i];
                        $arrins['satuan'] = $satuan;
                        $arrins['tipe'] = $arrdet['tipe'][$i];
                        $arrins['createdby'] = $this->newsession->userdata('username');
                        $this->db->insert('t_lap_bapok_dtl', $arrins); //print_r($this->db->last_query());die();
                    }
                    $resgula = TRUE;
                }
                if ($this->db->trans_status() === FALSE || !$resgula) {
                    $this->db->trans_rollback();
                } else {
                	// $msg = "MSG||YES||".$txt." berhasil disimpan.||REFRESH";
                    $msg = "MSG||YES||" . $txt . " berhasil disimpan.||REFRESH";
                    $logu = array('aktifitas' => 'Mengedit Pelaporan TDPUD dengan periode Bulan :  ' . $arrmas['bulan']. ' Tahun : '.$arrmas['tahun'],
                        'url' => '{c}' . site_url() . 'post/report/sipdis_act/update' . ' {m} models/report/sipdis_act {f} set_detil($act, $isajax)');
                    $this->main->set_activity($logu);
                    $this->db->trans_commit();
                }
            }
            return $msg;
        }
    }    

    function del_laporan($data, $tabel) {
        $this->db->trans_begin();
        $i = 0;
        foreach ($data as $id) {
            $hasid = hashids_decrypt($id, _HASHIDS_, 9);
            $cek_access = "SELECT fl_kirim FROM t_lap_bapok where id = '".$hasid."'";
	        $access = $this->db->query($cek_access)->row_array();
	        if ($access['fl_kirim'] != 0000) {
            	$msg = "MSG#Data Pelaporan Tidak Dapat Di Hapus. Karena Sudah Dikirim#refresh";
            	return $msg;
            }
            $this->db->where('id', $hasid);
            $this->db->delete($tabel);
            if ($this->db->affected_rows() > 0) {
                $this->db->where('lap_id', $hasid);
                $this->db->delete('t_lap_bapok_dtl');//print_r($this->db->last_query());die();
            }
        }
        if ($this->db->trans_status === FALSE || $i > 0) {
            $this->db->trans_rollback();
            $msg = "MSG#Proses Gagal#refresh";
        } else {
            $msg = "MSG||YES||" . $txt . " berhasil disimpan.||REFRESH";
            $logu = array('aktifitas' => 'Menghapus Pelaporan TDPUD dengan periode Bulan :  ' . $arrmas['bulan']. ' Tahun : '.$arrmas['tahun'],
            'url' => '{c}' . site_url() . 'post/report/sipdis_act/del_laporan' . ' {m} models/report/sipdis_act {f} set_detil($act, $isajax)');
            $this->main->set_activity($logu);
            $this->db->trans_commit();
            $msg = "MSG#Proses Berhasil#refresh";
        }
        return $msg;
    }

    function set_laporan($data) {
        $this->db->trans_begin();
        $i = 0;
        foreach ($data as $id) {
            $idx = hashids_decrypt($id, _HASHIDS_, 9);
            $cek_access = "SELECT fl_kirim FROM t_lap_bapok where id = '".$idx."'";
	        $access = $this->db->query($cek_access)->row_array();
	        if ($access['fl_kirim'] != 0000) {
            	$msg = "MSG#Data Pelaporan Tidak Dapat Di Kirim. Karena Sudah Dikirim#refresh";
            	return $msg;
            }
            $arrlap = array("fl_kirim" => "0100",
                            "sendby" => $this->newsession->userdata('username'),
                            "send" => date('Y-m-d H-i-s'));
            $this->db->where("id", $idx);
            $this->db->update('t_lap_bapok', $arrlap);
            if ($this->db->affected_rows() == 0) {
                $i++;
                break;
            }
        }
        if ($this->db->trans_status() === FALSE || $i > 0) {
            $this->db->trans_rollback();
            $msg = "MSG#Proses Gagal#refresh";
        } else {
            $this->db->trans_commit();
            $msg = "MSG#Proses Berhasil#refresh";
        }
        return $msg;
    }

    public function cek_edit($dir, $doc, $permohonan_id, $isajax){
        if ($this->newsession->userdata('_LOGGED')) {
            $data = $this->input->post('tb_chk');
            foreach ($data as $id) {
            	$ecn = $id;
            	$idx = hashids_decrypt($id, _HASHIDS_, 9);
	            $cek_access = "SELECT fl_kirim FROM t_lap_bapok where id = '".$idx."'";
	            $access = $this->db->query($cek_access)->row_array();
            }
            if ($access['fl_kirim'] != 0000) {
            	$msg = "MSG#Data Pelaporan Tidak Dapat Di Edit. Karena Sudah Dikirim#refresh";
            }else{
                // $msg = "MSG||YES||Data Permohonan " . $txt . " berhasil disimpan. Silahkan lanjutkan mengisi data penanggung jawab||" . site_url() . 'licensing/form/second/' . $dir . '/' . $doc . '/' . $type . '/' . $idredir . '/' . $s;
                //redirect(site_url() . 'licensing/form/first/' . $data[0] . '/' . $data[1] . '/' . $data[2] . '/' . $data[3], 'refresh()');
                $msg = "MSG#Silahkan Menunggu#redirect#". site_url() . 'licensing/add_report/form/' . $dir . '/' . $doc . '/' . $permohonan_id .'/'.$ecn;
            }
            return $msg;
        }
    }

    function begin_upload($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            //$file_upl = $_FILES['file_upload'];print_r($file_upl);die();

            $dirdok = './upL04d5/document/PEL_BPK/' . date("Y") . "/" . date("m") . "/";
            $kode = "PEL_BPK";
            require APPPATH.'libraries/class.uploader.php';

		    $uploader = new Uploader();
		    $data = $uploader->upload($_FILES['file_upload'], array(
		        'limit' => 1, //Maximum Limit of files. {null, Number}
		        'maxSize' => 2, //Maximum Size of files {null, Number(in MB's)}
		        'extensions' => array('xlsx'), //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
		        'required' => false, //Minimum one file is required for upload {Boolean}
		        'uploadDir' => $dirdok, //Upload directory {String}
		        'title' => $kode.'-'.date('Y-m-d').'-'.rand(1, 100), //New file name {null, String, Array} *please read documentation in README.md
		        'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
		        'replace' => true, //Replace the file if it already exists  {Boolean}
		        'perms' => null, //Uploaded file permisions {null, Number}
		        'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
		        'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
		        'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
		        'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
		        'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
		        'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
		    ));

		    if($data['isComplete']){
		        $info = $data['data']['metas'];
		        $ex = '<button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="read_excel(\''.trim($info['0']['name']).'\'); return false;"><i class="fa fa-file-excel-o"></i> Read Excel </button>';
                $res = array('err_status' => '0',
                            'err_desc' => $ex);
		    }else{
                $ex = "Upload Gagal";
                $res = array('err_status' => '1',
                            'err_desc' => $ex);
		    }

		    if($data['hasErrors']){
		        $errors = $data['errors'][0][0];
		        $res = array('err_status' => '1',
                            'err_desc' => $errors);
		    }
            // print_r($ex);die();
	        return $res;

        }
    }

    function read_excel($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }

            $nama = $this->input->post('path');

            $dirdok = './upL04d5/document/PEL_BPK/' . date("Y") . "/" . date("m") . "/" . $nama;

            //  Include PHPExcel_IOFactory
            require APPPATH.'libraries/PHPExcel/IOFactory.php';

            $inputFileName = $dirdok;

            //  Read your Excel workbook
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $tamp = array();            
            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0); 
            $highestRow = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 

            $no = 0;
            //  Loop through each row of the worksheet in turn
            for ($row = 1; $row <= $highestRow; ++$row) {
              for ($col = 0; $col <= $highestColumnIndex; ++$col) {
                if ($row != '1') {
                    if ($col == '3') {
                    	break;
                    }else{
                    	$tamp[$no][] = $sheet->getCellByColumnAndRow($col, $row)->getValue();
                    }
                }
              }
              if ($row != '1') {
              	$no++;
              }
            }
            // print_r($temp);die();
            $tipe1 = '';
            $tipe2 = '';
            $jum_pem = 0;
            $jum_pen = 0;
            foreach ($tamp as $key) {
            	$rand_remove = rand();
            	$get_kab = "SELECT a.nama FROM m_kab a where id = '".$key['1']."'";
            	$kab = $this->db->query($get_kab)->row_array();
            	if ($key['2'] == 1) {
            		$tipe1 .= '<tr id="tr_'.$rand_remove.'">';
			        $tipe1 .= '<td ><button type="button" class="btn btn-danger fa fa-times" onclick="remove_det('.$rand_remove.', '.$key['0'].', '.$key['2'].')"></button></td>';
			        $tipe1 .= '<td ><p>'.$key['0'].'</p><input type="hidden" name="dtl[jum][]" value='.$key['0'].'></td>';
			        $tipe1 .= '<td ><p>'.$kab['nama'].'</p><input type="hidden" name="dtl[asal][]" value='.$key['1'].'><input type="hidden" name="dtl[tipe][]" value='.$key['2'].'></td>';
			        $tipe1 .= '</tr>';
			        $jum_pem = $jum_pem + (float)$key['0'];
            	}elseif ($key['2'] == 2) {
            		$tipe2 .= '<tr id="tr_'.$rand_remove.'">';
			        $tipe2 .= '<td ><button type="button" class="btn btn-danger fa fa-times" onclick="remove_det('.$rand_remove.', '.$key['0'].', '.$key['2'].')"></button></td>';
			        $tipe2 .= '<td ><p>'.$key['0'].'</p><input type="hidden" name="dtl[jum][]" value='.$key['0'].'></td>';
			        $tipe2 .= '<td ><p>'.$kab['nama'].'</p><input type="hidden" name="dtl[asal][]" value='.$key['1'].'><input type="hidden" name="dtl[tipe][]" value='.$key['2'].'></td>';
			        $tipe2 .= '</tr>';
			        $jum_pen = (float)$jum_pen + (float)$key['0'];
            	}
            }
            $resjum = (float)$jum_pem - (float)$jum_pen;
            $arrres = array(
            	'tipe1' => $tipe1,
            	'tipe2' => $tipe2,
            	'resjum' => $resjum
            );
            // print_r($arrres);die();
            return $arrres;
        }
    }

}

?>