<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pulau_act extends CI_Model {

    public function get_choice() {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata['act'] = site_url('post/report/jenis_pelapor_pulau');
            $arrdata['dir'] = $dir;
            $arrdata['izin'] = $n;
            $arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_PELAPOR'", "kode", "uraian", TRUE);
            $queryNpwp = "select npwp from m_trader_refinasi";
            $sqlNpwp = $this->main->get_result($queryNpwp);
            if ($sqlNpwp) {
                foreach ($queryNpwp->result_array() as $v) {
                    $np[] = $v['npwp'];
                }
            }
            $arrdata['npwp'] = $np;
            return $arrdata;
        }
    }

    public function list_report() {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $role = $this->newsession->userdata('role');
            $trader_id = $this->newsession->userdata('trader_id');
            $addwhr = '';
            if ($role == "05") {
                $addwhr = "WHERE a.trader_id = '" . $trader_id . "'";
            } else {
                $addwhr = "WHERE a.status = '0100'";
            }
            $query = "SELECT a.id, a.no_aju as 'No Pelaporan', a.trader_id, CONCAT(a.npwp, '<br>' ,a.nama) as 'Pengirim', CONCAT(a.npwp_penerima, '<br>' ,a.nama_penerima) as 'Penerima', 
				    a.nama_kapal as 'Nama Kapal', b.uraian as 'Pelabuhan Muat', c.uraian as 'Pelabuhan Bongkar', 
                                    case when DATEDIFF(DAY, a.eta, GETDATE()) > 7 then '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class=\"fa fa-lock\" alt=\"Tidak Bisa di Edit/Hapus\"></i>'
 else '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class=\"fa fa-unlock\"></i>' end as 'Status'
					FROM t_lap_pulau a 
					LEFT JOIN m_peldn b ON b.kode = a.pel_asal
                    LEFT JOIN m_peldn c ON c.kode = a.pel_tujuan
                    LEFT JOIN m_reff d ON a.status = d.kode and d.jenis = 'STATUS_LAP'
                    " . $addwhr . "";
            // print_r($query);die();
            $table->columns(array("a.id", "a.permohonan_id", "a.permintaan_id", "a.tgl_lap", "a.file_lap"));
            $this->newtable->search(array(array("no_aju", "No Pelaporan"), array("nama", "Nama Pengirim"),
                array("nama_penerima", "Nama Penerima")));
            $table->title("");
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "pulau/home");
            $table->keys(array("id"));
            $table->hiddens(array("id", "trader_id"));
            $table->sortby("DESC");
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            if ($role == '05') {
                $table->menu(array(
                    'Tambah' => array('GET', site_url() . 'pulau/choice', '0', 'home', 'modal'),
                    // 'Tambah' => array('GET', site_url().'pulau/add_report/first', '0', 'home'),
                    'Preview' => array('GET', site_url() . 'pulau/preview_report', 'N', 'fa fa-check', 'isngajax'),
                    'Edit' => array('POST', site_url() . 'pulau/cek_edit', 'N', 'fa fa-edit', 'isngajax'),
                    'Copy' => array('POST', site_url() . 'pulau/cek_copy', 'N', 'fa fa-files-o', 'isngajax'),
                    // 'Edit' => array('GET', site_url().'pulau/add_report/first', '1', 'fa fa-edit'),
                    'Delete' => array('POST', site_url() . 'pulau/delete_report/ajax', 'N', 'fa fa-times', 'isngajax')
                        // 'Pelaporan' => array('POST', site_url().'licensing/laporan/all/ajax', 'N', 'fa fa-edit', 'isngajax')
                ));
            } else {
                $table->menu(array(
                    'Preview' => array('GET', site_url() . 'pulau/preview_report', 'N', 'fa fa-check', 'isngajax')
                ));
            }

            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Pelaporan Perdagangan Antar Pulau');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function cek($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            date_default_timezone_set('Asia/Jakarta');
            $now = '';
            $tgl = '';
            if (count($id) > 1) {
                $msg = "MSG#Hanya dapat mengedit satu permohonan#refresh";
                return $msg;
            }
            $cek_q = "SELECT status, jenis_pelapor, eta FROM t_lap_pulau WHERE id = " . $id[0];
            $res = $this->db->query($cek_q)->row_array();

            $tgl = strtotime('7 day', strtotime($res['eta']));
            $tgl = date('Y-m-d', $tgl);
            $now = date('Y-m-d');
//                        $tgl = '2017-12-04';
//                        print_r($tgl.' '.$now);die();
            if ($now >= $tgl) {
//                        print_r($tgl.' '.$now);die();
                //$msg = "MSG#Mohon Menunggu#redirect#".site_url().'pulau/add_report/first/'.$res['jenis_pelapor'].'/'.$id[0];
                $msg = "MSG#Data Sudah Dikirim Tidak Dapat Di Edit#refresh";
            } else {
                $msg = "MSG#Mohon Menunggu#redirect#" . site_url() . 'pulau/add_report/first/' . $res['jenis_pelapor'] . '/' . $id[0];
                // $msg = "MSG#Data Sudah Dikirim Tidak Dapat Di Edit#refresh";
            }
            if ($res['eta'] == '') {
                $msg = "MSG#Mohon Menunggu#redirect#" . site_url() . 'pulau/add_report/first/' . $res['jenis_pelapor'] . '/' . $id[0];
            }
            return $msg;
        }
    }

    function cek_copy($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (count($id) > 1) {
                $msg = "MSG#Hanya dapat mengedit satu permohonan#refresh";
                return $msg;
            }
            $cek_q = "SELECT trader_id, jenis_pelapor, npwp, nama, tipe_perushaan, alamat, kdprop, kdkab, 
					kdkec, kdkel, telp, email, npwp_penerima, nama_penerima, tipe_perushaan_penerima, 
					alamat_penerima, kdprop_penerima, kdkab_penerima, kdkec_penerima, kdkel_penerima, 
					telp_penerima, fax_penerima, email_penerima, fax, pel_asal, pel_tujuan, jenis_asal,
					negara_asal, alamat_asal, alamat_tujuan, prop_asal, prop_tujuan, jenis_tujuan, negara_tujuan, 
					kab_asal, kab_tujuan, nama_kapal, eta, etd, status
					FROM t_lap_pulau 
					WHERE id = " . $id[0];
            $res = $this->db->query($cek_q)->row_array();
            // print_r($res);die();
            if ($res['trader_id'] != '') {
                $query = "INSERT INTO t_lap_pulau (trader_id, jenis_pelapor, npwp, nama, tipe_perushaan, alamat, kdprop, kdkab, 
						kdkec, kdkel, telp, email, npwp_penerima, nama_penerima, tipe_perushaan_penerima, 
						alamat_penerima, kdprop_penerima, kdkab_penerima, kdkec_penerima, kdkel_penerima, 
						telp_penerima, fax_penerima, email_penerima, fax, pel_asal, pel_tujuan, jenis_asal,
						negara_asal, alamat_asal, alamat_tujuan, prop_asal, prop_tujuan, jenis_tujuan, negara_tujuan, 
						kab_asal, kab_tujuan, nama_kapal, status) 
						SELECT trader_id, jenis_pelapor, npwp, nama, tipe_perushaan, alamat, kdprop, kdkab, 
						kdkec, kdkel, telp, email, npwp_penerima, nama_penerima, tipe_perushaan_penerima, 
						alamat_penerima, kdprop_penerima, kdkab_penerima, kdkec_penerima, kdkel_penerima, 
						telp_penerima, fax_penerima, email_penerima, fax, pel_asal, pel_tujuan, jenis_asal,
						negara_asal, alamat_asal, alamat_tujuan, prop_asal, prop_tujuan, jenis_tujuan, negara_tujuan, 
						kab_asal, kab_tujuan, nama_kapal, status 
						FROM t_lap_pulau 
						WHERE id = " . $id[0];
                $result = $this->db->query($query);
                $id_now = $this->db->insert_id();
                $query_eks = "SELECT a.npwp, a.tipe_perushaan, a.nama, a.alamat, a.kdprop, a.kdkab,
							a.kdkel, a.kdkec, a.telp, a.email, a.nama_pj
							FROM t_lap_pulau_eks a
							WHERE a.id_lap = " . $id[0];
                $result_eks = $this->db->query($query_eks)->result_array(); //print_r($result_eks);die();
                if (count($result) > 0) {
                    foreach ($result_eks as $eks) {
                        $eks['id_lap'] = $id_now;
                        $eks['created'] = 'GETDATE()';
                        $eks['createdby'] = $this->newsession->userdata('username');
                        $res_eks = $this->db->insert('t_lap_pulau_eks', $eks);
                        // print_r($this->db->last_query());die();
                        if ($res_eks != 1) {
                            $msg = "MSG#Gagal Mengcopy Pelaporan Ekspedisi#refresh";
                        }
                    }
                }
                $query_dtl = "SELECT a.hs, a.uraian, a.jumlah, a.satuan, a.jns_komoditi
						FROM t_lap_pulau_dtl a
						WHERE a.id_lap = " . $id[0];
                $result_dtl = $this->db->query($query_dtl)->result_array();
                if (count($result_dtl) > 0) {
                    foreach ($result_dtl as $dtl) {
                        $dtl['id_lap'] = $id_now;
                        $res_dtl = $this->db->insert('t_lap_pulau_dtl', $dtl);
                        if ($res_dtl != 1) {
                            $msg = "MSG#Gagal Mengcopy Pelaporan Komoditi#refresh";
                        }
                    }
                }
                if ($result == 1) {
                    $msg = "MSG#Copy Pelaporan Berhasil#refresh";
                    $no_aju = $this->main->set_aju();
                    $arrupdate = array(
                        'created' => 'GETDATE()',
                        'cretaed_by' => $this->newsession->userdata('username'),
                        'tgl_kirim' => 'GETDATE()',
                        'status' => '0100',
                        'no_aju' => $no_aju
                    );
                    $this->db->where('id', $id_now);
                    $this->db->update('t_lap_pulau', $arrupdate);
                } else {
                    $msg = "MSG#Gagal Mengcopy Pelaporan#refresh";
                }
            } else {
                $msg = "MSG#Data Tidak Dapat Di Copy#refresh";
            }
            return $msg;
        }
    }

    function get_first($jenis, $id = '') {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata = array();
            $arrdata['id'] = $id;
            $arrdata['jenis'] = $jenis;
            $idprop = $this->newsession->userdata('kdprop');
            $idkab = $this->newsession->userdata('kdkab');
            $idkec = $this->newsession->userdata('kdkec');
            $idkel = $this->newsession->userdata('kdkel'); //print_r($idkel);die();
            $tipe_perusahaan = $this->newsession->userdata('tipe_perusahaan');
            $arrdata['propinsi_ter'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $idprop . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id = '" . $idkab . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec where id = '" . $idkec . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel where id = '" . $idkel . "' ORDER BY 2", "id", "nama", TRUE);
            $arrdata['direktorat'] = $dir;
            $arrdata['tipe'] = $type;
            $arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian", TRUE);
            $arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE kode = '" . $tipe_perusahaan . "' AND jenis = 'TIPE_PERUSAHAAN'", "kode", "uraian", TRUE);
            $arrdata['tipe_perusahaan_pen'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN'", "kode", "uraian", TRUE);
            $nama_izin = "Form Pelaporan Perdagangan Antar Pulau";
            $arrdata['nama_izin'] = $nama_izin;

            if ($id == "") {
                $arrdata['act'] = site_url('post/licensing/pulau/first/save');
            } else {
                $arrdata['act'] = site_url('post/licensing/pulau/first/update');
                $query = "SELECT a.id, a.npwp, a.nama, a.tipe_perushaan, a.alamat, a.kdprop, a.kdkab, a.kdkec, a.kdkel,
						a.telp, a.email, a.nama_penerima, a.npwp_penerima, a.tipe_perushaan_penerima, a.alamat_penerima, a.kdprop_penerima, 
						a.kdkab_penerima, a.kdkec_penerima, a.kdkel_penerima, a.telp_penerima, a.email_penerima, a.fax_penerima,
						a.nama_ekspedisi, a.alamat_ekspedisi, a.telp_ekspedisi, a.email_ekspedisi
						FROM t_lap_pulau a
						WHERE a.id = '" . $id . "'";
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    if ($jenis == '02') {
                        $arrdata['kab_pengirim'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                        $arrdata['kec_pengirim'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab'] . "' ORDER BY 2", "id", "nama", TRUE);
                        $arrdata['kel_pengirim'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec'] . "' ORDER BY 2", "id", "nama", TRUE);
                    }
                    $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop_penerima'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab_penerima'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec_penerima'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['detil'] = $this->form_ekspedisi($id);
                    if (trim($arrdata['detil']['temp']) != '') {
                        $arrdata['flag'] = 1;
                    }
                    // print_r($arrdata);die();
                    // print_r($detil);die();
                }
            }
            return $arrdata;
        }
    }

    function form_ekspedisi($id = '') {
        $num_form = $this->input->post('num_form');
        $jenis = $this->input->post('jenis');
        // print_r($_POST);die();
        if ($num_form == '') {
            $num_form = 0;
        }

        if ($jenis == '02' && $num_form == '0') {
            $num_form = 1;
        }

        $jml_form = 1;
        $arrdata[] = '';
        if ($id != '') {
            $query = "SELECT * from t_lap_pulau_eks WHERE id_lap = " . $id;
            // print_r($query);die();
            $data = $this->db->query($query);
            if ($data) {
                $query_per = "SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN'";
                $tipe_per = $this->db->query($query_per)->result_array();

                $query_prop = "SELECT id, nama FROM m_prop";
                $arrprop = $this->db->query($query_prop)->result_array();
                $jml_form = $data->num_rows();
                $arrdata = $data->result_array();
                // print_r($arrdata);die();
            }
        } else {
            $query_per = "SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN'";
            $tipe_per = $this->db->query($query_per)->result_array();

            $query_prop = "SELECT id, nama FROM m_prop";
            $arrprop = $this->db->query($query_prop)->result_array();
        }
        // print_r($jml_form);die();
        for ($i = 0; $i < $jml_form; $i++) {
            $temp .= '';
            $temp .= '<div class="row" id="eks_' . $num_form . '">';
            $temp .= '<div class="col-lg-12">';
            $temp .= '<section class="panel" id="control-group">';
            $temp .= '<div class="panel-body">';

            // npwp
            $temp .= '<div class="row">';
            $temp .= '<label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>';
            $temp .= '<div class="col-sm-9">';
            $temp .= '<input type="text" class="form-control mb-10" value="' . $arrdata[$i]['npwp'] . '" id="npwp_' . $num_form . '" name="eks[' . $num_form . '][npwp]" wajib="yes"/>';
            $temp .= '</div>';
            $temp .= '</div>';

            // tipe
            $temp .= '<div style="height:5px;"></div>';
            $temp .= '<div class="row">';
            $temp .= '<label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>';
            $temp .= '<div class="col-sm-9">';

            $temp .= '<select id="tipe_perusahaan_' . $num_form . '" name="eks[' . $num_form . '][tipe_perushaan]" wajib="yes" class="form-control mb-10">';

            foreach ($tipe_per as $tipe) {
                if ($tipe['kode'] == $arrdata[$i]['tipe_perushaan']) {
                    $temp .= '<option value="' . $tipe['kode'] . '" selected>' . $tipe['uraian'] . '</option>';
                } else {
                    $temp .= '<option value="' . $tipe['kode'] . '">' . $tipe['uraian'] . '</option>';
                }
            }

            $temp .= '</select>';

            $temp .= '</div>';
            $temp .= '</div>';

            // nama
            $temp .= '<div style="height:5px;"></div>';
            $temp .= '<div class="row">';
            $temp .= '<label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>';
            $temp .= '<div class="col-sm-9">';
            $temp .= '<input type="text" class="form-control mb-10" id="nama_' . $num_form . '" name="eks[' . $num_form . '][nama]" value="' . $arrdata[$i]['nama'] . '"/>';
            $temp .= '</div>';
            $temp .= '</div>';

            // alamat
            $temp .= '<div style="height:5px;"></div>';
            $temp .= '<div class="row">';
            $temp .= '<label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>';
            $temp .= '<div class="col-sm-9">';
            $temp .= '<textarea class="form-control mb-10" id="almt_perusahaan_' . $num_form . '" name="eks[' . $num_form . '][alamat]"> ' . $arrdata[$i]['alamat'] . ' </textarea>';
            $temp .= '</div>';
            $temp .= '</div>';

            // prop
            $temp .= '<div style="height:5px;"></div>';
            $temp .= '<div class="row">';
            $temp .= '<label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>';
            $temp .= '<div class="col-sm-9">';

            $temp .= '<select id="kdprop_' . $num_form . '" name="eks[' . $num_form . '][kdprop]" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab_' . $num_form . '\', 1); return false;">';

            foreach ($arrprop as $prop) {
                if ($prop['id'] == $arrdata[$i]['kdprop']) {
                    $temp .= '<option value="' . $prop['id'] . '" selected>' . $prop['nama'] . '</option>';
                } else {
                    $temp .= '<option value="' . $prop['id'] . '">' . $prop['nama'] . '</option>';
                }
            }

            $temp .= '</select>';

            $temp .= '</div>';
            $temp .= '</div>';

            // kab
            $temp .= '<div style="height:5px;"></div>';
            $temp .= '<div class="row">';
            $temp .= '<label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>';
            $temp .= '<div class="col-sm-9">';

            $temp .= '<select id="kdkab_' . $num_form . '" name="eks[' . $num_form . '][kdkab]" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec_' . $num_form . '\', 1);">';

            if ($id != '') {
                $query_kab = "SELECT id, nama FROM m_kab WHERE id_prov = '" . $arrdata[$i]['kdprop'] . "' ORDER BY 2";
                $arrkab = $this->db->query($query_kab)->result_array();

                foreach ($arrkab as $kab) {
                    if ($kab['id'] == $arrdata[$i]['kdkab']) {
                        $temp .= '<option value="' . $kab['id'] . '" selected>' . $kab['nama'] . '</option>';
                    } else {
                        $temp .= '<option value="' . $kab['id'] . '">' . $kab['nama'] . '</option>';
                    }
                }
            }

            $temp .= '</select>';

            $temp .= '</div>';
            $temp .= '</div>';

            // kec
            $temp .= '<div style="height:5px;"></div>';
            $temp .= '<div class="row">';
            $temp .= '<label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>';
            $temp .= '<div class="col-sm-9">';

            $temp .= '<select id="kdkec_' . $num_form . '" name="eks[' . $num_form . '][kdkec]" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel_' . $num_form . '\', 1);">';

            if ($id != '') {
                $query_kec = "SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $arrdata[$i]['kdkab'] . "' ORDER BY 2";
                $arrkec = $this->db->query($query_kec)->result_array();

                foreach ($arrkec as $kec) {
                    if ($kec['id'] == $arrdata[$i]['kdkec']) {
                        $temp .= '<option value="' . $kec['id'] . '" selected>' . $kec['nama'] . '</option>';
                    } else {
                        $temp .= '<option value="' . $kec['id'] . '">' . $kec['nama'] . '</option>';
                    }
                }
            }

            $temp .= '</select>';

            $temp .= '</div>';
            $temp .= '</div>';

            // kel
            $temp .= '<div style="height:5px;"></div>';
            $temp .= '<div class="row">';
            $temp .= '<label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>';
            $temp .= '<div class="col-sm-9">';

            $temp .= '<select id="kdkel_' . $num_form . '" name="eks[' . $num_form . '][kdkel]" wajib="yes" class="form-control input-sm select2">';

            if ($id != '') {
                $query_kel = "SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $arrdata[$i]['kdkec'] . "' ORDER BY 2";
                $arrkel = $this->db->query($query_kel)->result_array();

                foreach ($arrkel as $kel) {
                    if ($kel['id'] == $arrdata[$i]['kdkel']) {
                        $temp .= '<option value="' . $kel['id'] . '" selected>' . $kel['nama'] . '</option>';
                    } else {
                        $temp .= '<option value="' . $kel['id'] . '">' . $kel['nama'] . '</option>';
                    }
                }
            }

            $temp .= '</select>';

            $temp .= '</div>';
            $temp .= '</div>';

            // email
            $temp .= '<div style="height:5px;"></div>';
            $temp .= '<div class="row">';
            $temp .= '<label class="col-sm-3 control-label">Email</label>';
            $temp .= '<div class="col-sm-9">';
            $temp .= '<input type="text" class="form-control mb-10" id="email_' . $num_form . '" name="eks[' . $num_form . '][email]" value="' . $arrdata[$i]['email'] . '" />';
            $temp .= '</div>';
            $temp .= '</div>';

            // telp
            $temp .= '<div style="height:5px;"></div>';
            $temp .= '<div class="row">';
            $temp .= '<label class="col-sm-3 control-label">Telp/Hp</label>';
            $temp .= '<div class="col-sm-2">';
            $temp .= '<input type="text" class="form-control mb-10" id="telp_' . $num_form . '" name="eks[' . $num_form . '][telp]" value="' . $arrdata[$i]['telp'] . '" />';
            $temp .= '</div>';
            $temp .= '</div>';

            // pj
            $temp .= '<div style="height:5px;"></div>';
            $temp .= '<div class="row">';
            $temp .= '<label class="col-sm-3 control-label">Nama Pemilik / Penanggung Jawab <font size ="2" color="red">*</font></label>';
            $temp .= '<div class="col-sm-9">';
            $temp .= '<input type="text" class="form-control mb-10" id="nama_pj" name="eks[' . $num_form . '][nama_pj]" value="' . $arrdata[$i]['nama_pj'] . '"/>';
            $temp .= '</div>';
            $temp .= '</div>';

            if ($num_form != '0') {
                // button hapus
                $temp .= '<div class="row">';
                $temp .= '<div class="col-lg-12">';
                $temp .= '<span class="pull-left"><button class="btn btn-sm btn-warning addon-btn m-b-10" onclick="remove_eks(eks_' . $num_form . '); return false;"><i class="fa fa-minus"></i>Hapus</button></span>';
                $temp .= '</div>';
                $temp .= '</div>';
            }

            $temp .= '</div>';
            $temp .= '</section>';
            $temp .= '</div>';
            $temp .= '</div>';

            $num_form++;
        }

        // print_r($temp);die();
        $arrret['temp'] = $temp;
        return $arrret;
    }

    function set_first($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            // print_r($_POST);die();
            $resfirst = FALSE;
            $msg = "MSG||NO||Data Pelaporan gagal disimpan";
            $arrfirst = $this->main->post_to_query($this->input->post('siupmb'));
            $arreks = $this->input->post('eks');
            $jenis = $this->input->post('jenis');
            // print_r($jenis);die();

            if ($act == 'save') {
                $arrfirst['trader_id'] = $this->newsession->userdata('trader_id');
                $arrfirst['jenis_pelapor'] = $this->input->post('jenis');
//				$arrfirst['created'] = 'GETDATE()';
//                $arrfirst['created_by'] = $this->newsession->userdata('username');
                $arrfirst['status'] = '0000';
                $arrfirst['no_aju'] = $this->main->set_aju();
                $this->db->trans_begin();
                $exec = $this->db->insert('t_lap_pulau', $arrfirst);
                if ($this->db->affected_rows() > 0) {
                    $resfirst = TRUE;
                    $idredir = $this->db->insert_id();
                    $no = 1;
                    foreach ($arreks as $key) {
                        $key['id_lap'] = $idredir;
                        $key['npwp'] = str_replace('.', '', str_replace('-', '', $key['npwp']));
                        $key['no_urut'] = $no;
                        $key['created'] = 'GETDATE()';
                        $key['createdby'] = $this->newsession->userdata('username');
                        $exec = $this->db->insert('t_lap_pulau_eks', $key);
                        $no++;
                        // print_r($this->db->last_query());die();
                    }
                    $msg = "MSG||YES||Data Pelaporan Antar Pulau berhasil disimpan. Silahkan lanjutkan mengisi data barang||" . site_url() . 'pulau/add_report/second/' . $jenis . '/' . $idredir;

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan data Pelaporan Antar Pulau (Data Pengirim Penerima Ekspedisi) dengan id Pelaporan : ' . hashids_encrypt($idredir, _HASHIDS_, 9),
                        'url' => '{c}' . site_url() . 'pulau/add_report/second/' . ' {m} models/report/pulau_act {f} set_first($act, $ajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$resfirst) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } else {
                $id_lap = $this->input->post('id_lap');
                if ($id_lap == '') {
                    return $msg;
                }
                $arrfirst['updated'] = 'GETDATE()';
                $arrfirst['updated_by'] = $this->newsession->userdata('username');
                $this->db->trans_begin();
                $this->db->where('id', $id_lap);
                $this->db->update('t_lap_pulau', $arrfirst);

                if ($this->db->affected_rows() > 0) {
                    $resfirst = TRUE;
                    $idredir = $this->db->insert_id();
                    $where = array('id_lap' => $id_lap);
                    $this->db->where($where);
                    $this->db->delete('t_lap_pulau_eks');

                    foreach ($arreks as $key) {
                        $key['id_lap'] = $id_lap;
                        $key['created'] = 'GETDATE()';
                        $key['createdby'] = $this->newsession->userdata('username');
                        $exec = $this->db->insert('t_lap_pulau_eks', $key);
                    }
                    $msg = "MSG||YES||Data Pelaporan Antar Pulau berhasil disimpan. Silahkan lanjutkan mengisi data barang||" . site_url() . 'pulau/add_report/second/' . $jenis . '/' . $id_lap;

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan data Pelaporan Antar Pulau (Data Pengirim Penerima Ekspedisi) dengan id Pelaporan : ' . hashids_encrypt($idredir, _HASHIDS_, 9),
                        'url' => '{c}' . site_url() . 'pulau/add_report/second/' . ' {m} models/report/pulau_act {f} set_first($act, $ajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$resfirst) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            }

            return $msg;
        }
    }

    function get_second($jenis, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $jenis_identitas = $this->newsession->userdata('tipe_identitas');
            $id_user = $this->newsession->userdata('id');
            $arrdata['act'] = site_url('post/licensing/pulau/second/save');
            $arrdata['id'] = $id;
            $arrdata['jenis'] = $jenis;
            $arrdata['direktorat'] = $dir;
            $arrdata['asal'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_ASAL'", "kode", "uraian", TRUE);
            $arrdata['tujuan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_TUJUAN'", "kode", "uraian", TRUE);
            $arrdata['negara'] = $this->main->set_combobox("SELECT kode, nama FROM m_negara", "kode", "nama", TRUE);
            $arrdata['pelabuhan_muat'] = $this->main->set_combobox("SELECT a.kode, a.uraian FROM m_peldn a WHERE a.tipe = '1'", "kode", "uraian", TRUE);
            $arrdata['pelabuhan_bongkar'] = $this->main->set_combobox("SELECT a.kode, a.uraian FROM m_peldn a WHERE a.tipe = '1'", "kode", "uraian", TRUE);
            $arrdata['pelabuhan_kayu'] = $this->main->set_combobox("SELECT a.kode, a.uraian FROM m_peldn a WHERE a.tipe = '3'", "kode", "uraian", TRUE);
            $arrdata['propinsi_asal'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            $arrdata['propinsi_tujuan'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            $arrdata['urifirst'] = site_url('pulau/add_report/first/' . $id);
            $arrdata['btn'] = "Selanjutnya";
            $kayu = array("HUPKH", "HUPKA", "HUPKI", "HUPTB", "HUPTO", "HULOG");
            $query = "SELECT a.pel_asal, a.pel_tujuan, a.alamat_asal, a.alamat_tujuan, a.prop_asal, a.prop_tujuan,
					a.kab_asal, a.kab_tujuan, a.nama_kapal, a.eta, a.etd, jenis_asal, jenis_tujuan, negara_asal, negara_tujuan
					FROM t_lap_pulau a
					WHERE a.id = '" . $id . "'";
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
                if ($row['pel_asal'] != '' || $row['pel_tujuan'] != '' || $row['alamat_tujuan'] != '' || $row['alamat_asal'] != '') {
                    $arrdata['act'] = site_url('post/licensing/pulau/second/update');
                    $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['btn'] = "Update";
                }
            }
            // print_r($arrdata['sess']);die();
            if (in_array($row['pel_asal'], $kayu))
                $arrdata['flag'] = '1';
            $nama_izin = 'Pelaporan Perdagangan Antar Pulau';
            $arrdata['nama_izin'] = $nama_izin;
            return $arrdata;
        }
    }

    function set_second($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            // print_r($_POST);die();
            $ressec = FALSE;
            $msg = "MSG||NO||Data Pelaporan gagal disimpan";
            $arrsec = $this->main->post_to_query($this->input->post('pj'));
            $id_lap = $this->input->post('id_lap');
            $jenis = $this->input->post('jenis');
            // print_r($jenis);die();

            if ($id_lap == '') {
                $msg = "MSG||NO||Harap Mengajukan Ulang Pelaporan Baru||" . site_url() . 'pulau/home';
                return $msg;
            }

            $this->db->trans_begin();
            $this->db->where('id', $id_lap);
            $this->db->update('t_lap_pulau', $arrsec);
            // print_r($this->db->last_query());die();
            if ($this->db->affected_rows() > 0) {
                $ressec = TRUE;
                if ($act == 'save') {
                    $msg = "MSG||YES||Data Pelaporan Antar Pulau berhasil disimpan. Silahkan lanjutkan mengisi data Komoditi||" . site_url() . 'pulau/add_report/third/' . $jenis . '/' . $id_lap;
                } else {
                    $arrkom['updated'] = 'GETDATE()';
                    $arrkom['updated_by'] = $this->newsession->userdata('username');
                    $this->db->where('id', $id_lap);
                    $this->db->update('t_lap_pulau', $arrkom);
                    $msg = "MSG||YES||Data Pelaporan Antar Pulau berhasil diupdate.||" . site_url() . 'pulau/add_report/third/' . $jenis . '/' . $id_lap;
                }

                /* Log User dan Log Izin */
                $logu = array('aktifitas' => 'Menambahkan data Pelaporan Antar Pulau (Data Barang) dengan id Pelaporan : ' . hashids_encrypt($idredir, _HASHIDS_, 9),
                    'url' => '{c}' . site_url() . 'pulau/add_report/second/' . ' {m} models/report/pulau_act {f} set_first($act, $ajax)');
                $this->main->set_activity($logu);
                /* Akhir Log User dan Log Izin */
            }
            if ($this->db->trans_status() === FALSE || !$ressec) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
            return $msg;
        }
    }

    function get_third($jenis, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
//            die($id);
            $permohonan_id = $this->uri->segment(7);
            $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
            $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
            $table = $this->newtable;

            $query = "SELECT CONCAT(b.jenis_pelapor, '/', a.id_lap, '/', a.id) AS idx, CONCAT(a.hs, '
					', a.uraian) AS 'HS',   a.uraian_user AS 'Uraian Barang', a.nama_container as 'Nomor Container', a.jumlah AS 'Jumlah', c.uraian AS 'Satuan'
					FROM t_lap_pulau_dtl a
					LEFT JOIN t_lap_pulau b ON b.id = a.id_lap
					LEFT JOIN m_satuan c ON c.satuan = a.satuan
					WHERE a.id_lap = '" . $id . "'";
            // echo $query;die();

            $table->title("");
            $table->columns(array("a.id", "a.id_lap", "a.hs", "a.uraian", "b.uraian",));
            $this->newtable->width(array('JENIS PRODUK' => 100, 'MERK' => 100, 'TYPE MODEL' => 110, '&nbsp;' => 5));
            // $this->newtable->search(array(array("b.produk", "JENIS PRODUK"),
            //     array("a.merk", "MERK")));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . 'pulau/add_report/third/' . $jenis . '/' . $id);
            $table->sortby("ASC");
            $table->keys(array('idx'));
            $table->hiddens(array("idx"));
            $table->show_search(FALSE);
            $table->show_chk(TRUE);
//            $b = $this->db->query($query)->num_rows();
            $table->single(TRUE);
            $table->dropdown(TRUE);
            $table->use_ajax(TRUE);
            //$table->hashids(TRUE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            //$table->tbtarget("refkbli");
            //$table->attrid($target);
            $table->menu(array('Tambah' => array('GET', site_url() . 'pulau/add_komoditi/' . $jenis . '/' . $id, '0', 'home'),
                'Edit' => array('GET', site_url() . 'pulau/add_komoditi', '1', 'fa fa-edit'),
                'Delete' => array('POST', site_url() . 'pulau/del_komoditi', 'N', 'fa fa-times', 'isngajax')
            ));
            $table->tbtarget("fpreview");
//            if($id == ''){
//                $link = 'pulau/verif';
//            }else{
//                $link = 'pulau/home';
//            }
            $arrdata = array('tabel' => $table->generate($query),
                'id' => $id,
                'jenis' => $jenis,
                'act' => site_url('pulau/verif')
            );
            //print_r($arrdata);die();
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function add_komoditi($jenis, $id_lap, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata['id_lap'] = $id_lap;
            $arrdata['id'] = $id;
            $arrdata['jenis'] = $jenis;
            $arrdata['act'] = site_url('post/licensing/pulau/komoditi/save');
            $arrdata['komoditi'] = $this->main->set_combobox("SELECT a.id, a.uraian FROM m_jenis_bapok a", "id", "uraian", TRUE);
            $arrdata['hs'] = $this->main->set_combobox("SELECT hs as id, CONCAT (hs,' ', uraiInd) as nama FROM m_hs WHERE LEN(hs) = 4 ORDER BY hs", "id", "nama", TRUE);
            $arrdata['satuan'] = $this->main->set_combobox("SELECT satuan as id, uraian as nama FROM m_satuan", "id", "nama", TRUE);
            $arrdata['urifirst'] = site_url('pulau/add_report/third/' . $id);
            $query = "SELECT a.hs, a.uraian, a.uraian_user,  a.jumlah, a.satuan, a.nama_container
					FROM t_lap_pulau_dtl a
					WHERE a.id = '" . $id . "'";
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrdata['sess'] = $row;
                }
                $arrdata['sess']['hs_4'] = substr($arrdata['sess']['hs'], 0, 4);
                $query_hs6 = "SELECT hs AS id, CONCAT (hs,' ', uraiInd) AS nama
			                FROM m_hs
			                WHERE hs LIKE '%" . $arrdata['sess']['hs_4'] . "%' AND LEN(hs) = 6
			                ORDER BY hs";
                $arrdata['hs_6'] = $this->main->set_combobox($query_hs6, "id", "nama");
                $arrdata['sess']['hs_6'] = substr($arrdata['sess']['hs'], 0, 6);
                $query_hs8 = "SELECT hs AS id, CONCAT (hs,' ', uraiInd) AS nama
			                FROM m_hs
			                WHERE hs LIKE '%" . $arrdata['sess']['hs_6'] . "%' AND LEN(hs) = 8
			                ORDER BY hs";
                $arrdata['hs_8'] = $this->main->set_combobox($query_hs8, "id", "nama");
                $arrdata['act'] = site_url('post/licensing/pulau/komoditi/update');
                // print_r($arrdata['sess']);die();
            }
            return $arrdata;
        }
    }

    function set_komoditi($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            // print_r($_POST);die();
            $reskom = FALSE;
            $msg = "MSG||NO||Data Pelaporan gagal disimpan";
            $arrkom = $this->main->post_to_query($this->input->post('pj'));
            $id_lap = $this->input->post('id_lap');
            $id = $this->input->post('id');
            $jenis = $this->input->post('jenis');
            // print_r($arrkom);
            // print_r($id_lap);die();

            if ($act == 'save') {
                $arrkom['id_lap'] = $id_lap;
                $this->db->trans_begin();
                $exec = $this->db->insert('t_lap_pulau_dtl', $arrkom); //print_r($this->db->last_query());die();
                if ($this->db->affected_rows() > 0) {
                    $reskom = TRUE;
                    $msg = "MSG||YES||Data Komoditi berhasil disimpan||" . site_url() . 'pulau/add_report/third/' . $jenis . '/' . $id_lap;

                    /* Log User dan Log Izin */
                    $logu = array('aktifitas' => 'Menambahkan data Pelaporan Antar Pulau (Data Komoditi) dengan id Pelaporan : ' . hashids_encrypt($idredir, _HASHIDS_, 9),
                        'url' => '{c}' . site_url() . 'pulau/add_komoditi' . ' {m} models/report/pulau_act {f} set_komoditi($act, $ajax)');
                    $this->main->set_activity($logu);
                    /* Akhir Log User dan Log Izin */
                }
                if ($this->db->trans_status() === FALSE || !$reskom) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                return $msg;
            } elseif ($act == 'update') {
                if ($id == '') {
                    return $msg;
                } else {
                    $this->db->trans_begin();
                    $this->db->where('id', $id);
                    $this->db->update('t_lap_pulau_dtl', $arrkom); //print_r($this->db->last_query());die();
                    if ($this->db->affected_rows() > 0) {
                        $reskom = TRUE;
                        $msg = "MSG||YES||Data Komoditi berhasil disimpan||" . site_url() . 'pulau/add_report/third/' . $jenis . '/' . $id_lap;

                        /* Log User dan Log Izin */
                        $logu = array('aktifitas' => 'Menambahkan data Pelaporan Antar Pulau (Data Komoditi) dengan id Pelaporan : ' . hashids_encrypt($idredir, _HASHIDS_, 9),
                            'url' => '{c}' . site_url() . 'pulau/add_komoditi' . ' {m} models/report/pulau_act {f} set_komoditi($act, $ajax)');
                        $this->main->set_activity($logu);
                        /* Akhir Log User dan Log Izin */
                    }
                    if ($this->db->trans_status() === FALSE || !$reskom) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                    return $msg;
                }
            }
        }
    }

    function delete_komoditi($id) {

        $this->db->trans_begin();
        foreach ($id as $data) {
            $data = explode("/", $data);
            // print_r($data);die();
            $this->db->delete('t_lap_pulau_dtl', array('id' => $data[2]));
        }
        if ($this->db->trans_status === FALSE) {
            $this->db->trans_rollback();
            $msg = "MSG#Proses Gagal#refresh";
        } else {
            $this->db->trans_commit();
            $msg = "MSG#Proses Berhasil#refresh";
        }
        return $msg;
    }

    function del_report($id, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            date_default_timezone_set('Asia/Jakarta');
            $this->db->trans_begin();
            foreach ($id as $data) {
                $cek_q = "SELECT status, eta FROM t_lap_pulau WHERE id = " . $data;
                $res = $this->db->query($cek_q)->row_array();
                if ($res['eta']) {
                    $tgl = strtotime('7 day', strtotime($res['eta']));
                    $tgl = date('Y-m-d', $tgl);
                    $now = date('Y-m-d');
                    if ($now >= $tgl) {
                        $msg = "MSG#Pelaporan tidak bisa dihapus#refresh";
                        return $msg;
                    }
                }
                $this->db->delete('t_lap_pulau', array('id' => $data));
                if ($this->db->affected_rows() > 0) {
                    $this->db->where('id_lap', $data);
                    $this->db->delete('t_lap_pulau_dtl');
                }
            }
            if ($this->db->trans_status === FALSE) {
                $this->db->trans_rollback();
                $msg = "MSG#Proses Gagal#refresh";
            } else {
                $this->db->trans_commit();
                $msg = "MSG#Proses Berhasil#refresh";
            }
            return $msg;
        }
    }

    function preview_report($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $query = "SELECT a.id, a.npwp, a.nama, d.uraian as 'tipe_perusahaan', a.alamat, fa.nama as 'kdprop', ga.nama as 'kdkab', 
					ha.nama as 'kdkec', ia.nama as 'kdkel', a.fax, a.status, j.uraian as 'jenis_asal', k.uraian as 'jenis_tujuan', 
					la.nama as 'negara_asal', lt.nama as 'negara_tujuan',
					a.telp, a.email, a.nama_penerima, a.npwp_penerima, e.uraian as 'tipe_perusahaan_penerima', a.alamat_penerima, 
					fb.nama as 'kdprop_penerima', a.email_penerima, a.email_ekspedisi, a.email,
					gb.nama as 'kdkab_penerima', hb.nama as 'kdkec_penerima', ib.nama as 'kdkel_penerima', a.telp_penerima, 
					a.email_penerima, a.fax_penerima, a.nama_ekspedisi, a.alamat_ekspedisi, a.telp_ekspedisi, a.email_ekspedisi, 
					c.uraian as 'pel_tujuan', b.uraian as 'pel_asal', 
					a.alamat_asal, a.alamat_tujuan, fc.nama as 'prop_asal', fd.nama as 'prop_tujuan',
					gc.nama as 'kab_asal', gd.nama as 'kab_tujuan', a.nama_kapal, a.eta, a.etd
					FROM t_lap_pulau a
					LEFT JOIN m_reff d ON d.kode = a.tipe_perushaan AND d.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_reff e ON e.kode = a.tipe_perushaan AND e.jenis = 'TIPE_PERUSAHAAN'
					LEFT JOIN m_peldn b ON b.kode = a.pel_asal
					LEFT JOIN m_peldn c ON c.kode = a.pel_tujuan
					LEFT JOIN m_prop fa ON fa.id = a.kdprop
					LEFT JOIN m_prop fb ON fb.id = a.kdprop_penerima
					LEFT JOIN m_prop fc ON fc.id = a.prop_asal
					LEFT JOIN m_prop fd ON fd.id = a.prop_tujuan					
					LEFT JOIN m_kab ga ON ga.id = a.kdkab
					LEFT JOIN m_kab gb ON gb.id = a.kdkab_penerima
					LEFT JOIN m_kab gc ON gc.id = a.kab_asal
					LEFT JOIN m_kab gd ON gd.id = a.kab_tujuan
					LEFT JOIN m_kec ha ON ha.id = a.kdkec
					LEFT JOIN m_kec hb ON hb.id = a.kdkec_penerima
					
					LEFT JOIN m_kel ia ON ia.id = a.kdkel
					LEFT JOIN m_kel ib ON ib.id = a.kdkel_penerima

					LEFT JOIN m_reff j ON j.kode = a.jenis_asal AND j.jenis = 'JENIS_ASAL'
					LEFT JOIN m_reff k ON k.kode = a.jenis_tujuan AND k.jenis = 'JENIS_TUJUAN'
					LEFT JOIN m_negara la ON la.kode = a.negara_asal
					LEFT JOIN m_negara lt ON lt.kode = a.negara_tujuan
					WHERE a.id = '" . $id . "'";
            $row = $this->db->query($query)->row_array();

            $detil = "SELECT CONCAT(a.hs, '<br>', a.uraian) AS 'HS', a.uraian as 'Uraian_Barang', 
					a.jumlah AS 'Jumlah', c.uraian AS 'Satuan', b.uraian as 'Jenis_Komoditi', a.nama_container
					FROM t_lap_pulau_dtl a
					LEFT JOIN m_jenis_bapok b ON b.id = a.jns_komoditi
					LEFT JOIN m_satuan c ON c.satuan = a.satuan
					WHERE a.id_lap = '" . $id . "'";
            $row_detil = $this->db->query($detil)->result_array();
            $ekspedisi = "SELECT a.npwp, d.uraian as 'tipe_perushaan', a.nama, a.alamat, a.telp, a.email, 
            			fa.nama as 'kdprop', ga.nama as 'kdkab', 
            			ha.nama as 'kdkec', ia.nama as 'kdkel', a.nama_pj
            			FROM t_lap_pulau_eks a 
            			LEFT JOIN m_reff d ON d.kode = a.tipe_perushaan AND d.jenis = 'TIPE_PERUSAHAAN'
            			LEFT JOIN m_prop fa ON fa.id = a.kdprop
						LEFT JOIN m_kab ga ON ga.id = a.kdkab
						LEFT JOIN m_kec ha ON ha.id = a.kdkec
						LEFT JOIN m_kel ia ON ia.id = a.kdkel
            			WHERE a.id_lap = '" . $id . "'";
            $row_ekspedisi = $this->db->query($ekspedisi)->result_array();
            $arrdata = array('sess' => $row,
                'detil' => $row_detil,
                'ekspedisi' => $row_ekspedisi,
                'act' => site_url('pulau/verif/resend'));
            return $arrdata;
        }
    }

    function verif_act() {
        if ($this->newsession->userdata('_LOGGED')) {
            $id = $this->input->post('data');
            $id = $id['id'];
            $query = $this->db->query("SELECT no_aju from t_lap_pulau where id = '" . $id . "'");
            $hasil = $query->result_array();
//            print_r($hasil[0]['no_aju']);die();
//            die($id);
            if ($hasil[0]['no_aju'] == '') {
                $no_aju = $this->main->set_aju();
                $arrupdate = array(
                    'created' => 'GETDATE()',
                    'cretaed_by' => $this->newsession->userdata('username'),
                    'tgl_kirim' => 'GETDATE()',
                    'status' => '0100',
                    'no_aju' => $no_aju
                );
            } else {

                $arrupdate = array(
                    'created' => 'GETDATE()',
                    'cretaed_by' => $this->newsession->userdata('username'),
                    'tgl_kirim' => 'GETDATE()',
                    'status' => '0100'
                );
            }
            $this->db->trans_begin();
            $this->db->where('id', $id);
            $this->db->update('t_lap_pulau', $arrupdate);
            if ($this->db->trans_status === FALSE) {
                $this->db->trans_rollback();
                $msg = "MSG||NO||Data Pelaporan Gagal Dikirim||REFRESH";
            } else {
                $this->db->trans_commit();
                $msg = "MSG||YES||Data Pelaporan berhasil Dikirim||" . site_url() . 'pulau/home';
            }
            return $msg;
        }
    }

    function set_prints($id, $cetak = "") {
        if ($this->newsession->userdata('_LOGGED')) {
            $query = "SELECT a.id, a.npwp, a.nama, d.uraian as 'tipe_perushaan', a.alamat, fa.nama as 'kdprop', ga.nama as 'kdkab', 
					ha.nama as 'kdkec', ia.nama as 'kdkel', a.fax, a.status, j.uraian as 'jenis_asal', a.jenis_tujuan as kd_tujuan,
					k.uraian as 'jenis_tujuan', a.jenis_asal as 'kd_asal', dbo.dateIndo(a.eta) as 'sampai', 
					la.nama as 'negara_asal', lt.nama as 'negara_tujuan', dbo.dateIndo(a.etd) as 'berangkat', z.uraian as 'jenis_pelapor',
					a.telp, a.email, a.nama_penerima, a.npwp_penerima, e.uraian as 'tipe_perushaan_penerima', a.alamat_penerima, 
					fb.nama as 'kdprop_penerima', a.email_penerima, a.email_ekspedisi, a.email,
					gb.nama as 'kdkab_penerima', hb.nama as 'kdkec_penerima', ib.nama as 'kdkel_penerima', a.telp_penerima, 
					a.email_penerima, a.fax_penerima, a.nama_ekspedisi, a.alamat_ekspedisi, a.telp_ekspedisi, a.email_ekspedisi, 
					c.uraian as 'pel_tujuan', b.uraian as 'pel_asal', a.no_aju,
					a.alamat_asal, a.alamat_tujuan, fc.nama as 'prop_asal', fd.nama as 'prop_tujuan',
					gc.nama as 'kab_asal', gd.nama as 'kab_tujuan', a.nama_kapal, a.eta, a.etd
					FROM t_lap_pulau a
					LEFT JOIN m_reff d ON d.kode = a.tipe_perushaan AND d.jenis = 'KDTIPE_PERUSAHAAN'
					LEFT JOIN m_reff e ON e.kode = a.tipe_perushaan_penerima AND e.jenis = 'KDTIPE_PERUSAHAAN'
					LEFT JOIN m_peldn b ON b.kode = a.pel_asal
					LEFT JOIN m_peldn c ON c.kode = a.pel_tujuan
					LEFT JOIN m_prop fa ON fa.id = a.kdprop
					LEFT JOIN m_prop fb ON fb.id = a.kdprop_penerima
					LEFT JOIN m_prop fc ON fc.id = a.prop_asal
					LEFT JOIN m_prop fd ON fd.id = a.prop_tujuan					
					LEFT JOIN m_kab ga ON ga.id = a.kdkab
					LEFT JOIN m_kab gb ON gb.id = a.kdkab_penerima
					LEFT JOIN m_kab gc ON gc.id = a.kab_asal
					LEFT JOIN m_kab gd ON gd.id = a.kab_tujuan
					LEFT JOIN m_kec ha ON ha.id = a.kdkec
					LEFT JOIN m_kec hb ON hb.id = a.kdkec_penerima
					LEFT JOIN m_reff z ON z.kode = a.jenis_pelapor AND z.jenis = 'JENIS_PELAPOR'

					LEFT JOIN m_kel ia ON ia.id = a.kdkel
					LEFT JOIN m_kel ib ON ib.id = a.kdkel_penerima

					LEFT JOIN m_reff j ON j.kode = a.jenis_asal AND j.jenis = 'JENIS_ASAL'
					LEFT JOIN m_reff k ON k.kode = a.jenis_tujuan AND k.jenis = 'JENIS_TUJUAN'
					LEFT JOIN m_negara la ON la.kode = a.negara_asal
					LEFT JOIN m_negara lt ON lt.kode = a.negara_tujuan
					WHERE a.id = '" . $id . "'";
            $row = $this->db->query($query)->row_array();
            $arrdata['row'] = $row;

            $query_eks = "SELECT b.id, b.id_lap, b.NPWP, b.nama, b.alamat, fa.nama AS 'kdprop', ga.nama AS 'kdkab', ha.nama AS 'kdkec', ia.nama AS 'kdkel', b.telp, b.email, d.uraian as 'tipe_perushaan'
                            FROM t_lap_pulau a
                            LEFT JOIN t_lap_pulau_eks b ON a.id = b.id_lap
                            LEFT JOIN m_prop fa ON fa.id = b.kdprop
                            LEFT JOIN m_kab ga ON ga.id = b.kdkab
                            LEFT JOIN m_kec ha ON ha.id = b.kdkec
                            LEFT JOIN m_kel ia ON ia.id = b.kdkel
                            LEFT JOIN m_reff d ON d.kode = a.tipe_perushaan AND d.jenis = 'KDTIPE_PERUSAHAAN' 
                            WHERE b.id_lap = '" . $id . "'";
            $row_eks = $this->db->query($query_eks)->result();

            $detil = "SELECT a.hs AS 'HS', a.uraian as 'Uraian_Barang', 
					a.jumlah AS 'Jumlah', c.uraian AS 'Satuan', b.uraian as 'Jenis_Komoditi', a.nama_container
					FROM t_lap_pulau_dtl a
					LEFT JOIN m_jenis_bapok b ON b.id = a.jns_komoditi
					LEFT JOIN m_satuan c ON c.satuan = a.satuan
					WHERE a.id_lap = '" . $id . "'";
            $row_detil = $this->db->query($detil)->result_array();

            $arrdata['detil'] = $row_detil;
            $arrdata['eks'] = $row_eks;
            $arrdata['catatan'] = $_POST['catatan'];
            //print_r(hashids_encrypt((int)$arrdata['row']['direktorat_id'], _HASHIDS_, 9)."-".hashids_encrypt(2, _HASHIDS_, 9));die();
            $arrdata['link'] = url_sipt . 'verifikasi/' . hashids_encrypt((int) $arrdata['row']['direktorat_id'], _HASHIDS_, 9) . '/' . hashids_encrypt($a, _HASHIDS_, 9) . '/' . $b;
            if ($cetak != '') {
                $kode = '136';
                $folder = 'LAPAN';
                $arrdata['namafile'] = "DOCUMENT-" . $row['id'] . '-' . $row['nama'] . '.pdf';
                $arrdata['dir'] = './upL04d5/document/' . $folder . '/' . date("Y") . "/" . date("m") . "/" . date("d");
                $sqlInsertPendukung = "INSERT INTO t_upload (trader_id, tipe_dok, nomor, tgl_dok, tgl_exp, penerbit_dok, nama_file, ori_file, folder, ekstensi, ukuran, created_user, created) 
						VALUES('" . $arrdata['row']['trader_id'] . "','" . $kode . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['row']['tgl_dok'] . "','" . $arrdata['row']['tgl_exp'] . "','Direktorat Jenderal Perdagangan Dalam Negeri','" . $arrdata['namafile'] . "','" . $arrdata['row']['no_izin'] . "','" . $arrdata['dir'] . "','.pdf','0','system',GETDATE())";
                $insert = $this->db->query($sqlInsertPendukung);
            }
            return $arrdata;
        }
    }

    function get_data_permohonan($status, $id) {
        $query = "SELECT top 1 a.id, a.nama as nama_pengirim, a.nama_penerima, a.pel_asal, a.pel_tujuan,
				b.email_pj, f.email as 'email_koor', a.email, a.email_penerima
				FROM t_lap_pulau a
				LEFT JOIN m_trader b ON b.id = a.trader_id
				LEFT JOIN t_user f ON f.trader_id = a.trader_id
				WHERE a.id = " . $id . "";
        // print_r($query);die();
        $data = $this->main->get_result($query);
        if ($data) {
            foreach ($query->result_array() as $row) {
                $arrdata = $row;
            }
        }
        $arrdata['file'] = '';
        if ($status == '0000') {
            $query = "SELECT TOP 1 folder, nama_file, ekstensi
						  FROM t_upload
					  where nama_file like '%DOCUMENT-" . $row['id'] . "-" . $row['nama_pengirim'] . ".pdf%'  order by id desc  ";
            // print_r($query);die();
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $row) {
                    $arrfile = $row;
                }
            }
            $arrdata['file'] = $arrfile;
        }
        return $arrdata;
    }

    function email_pelaku_usaha($status, $id) {
        $data_permohonan = $this->get_data_permohonan($status, $id);
        $emails = $data_permohonan['email_pj'] . ',' . $data_permohonan['koor_email'] . ',' . $data_permohonan['email_penerima'];
        $send_email = 0;

        if ($status == '0000') {

            if ($data_permohonan['file'] != '') {
                $attachment = $data_permohonan['file']['folder'] . "/" . $data_permohonan['file']['nama_file'];
            }

            $subject = 'Pelaporan Perdagangan Antar Pulau SIPT PDN';
            $message = '<html><head><title>' . $subject . '</title></head><body>';
            $message .= '<p>Yth Bapak/Ibu Pimpinan Perusahaan Terkait</p>';
            $message .= '<p>Bersama ini kami sampaikan dan lampirkan bahwa Pelaporan Perdagangan Antar Pulau 
						dengan nomor data sebagai berikut:<br><br>';
            $message .= '<p>Perusahaan Pengirim : ' . $data_permohonan['nama_pengirim'] . '</p>';
            $message .= '<p>dengan Pelabuhan Asal : ' . $data_permohonan['pel_asal'] . '</p>';
            $message .= '<p>Perusahaan Penerima : ' . $data_permohonan['nama_penerima'] . '</p>';
            $message .= '<p>dengan Pelabuhan Tujuan : ' . $data_permohonan['pel_tujuan'] . '</p><br>';

            $message .= 'Demikian, untuk menjadi perhatian Saudara.<br><br>Admin</p>';
            $message .= '<p>* This is a system-generated email, please do not reply.</p>';
            $message .= '</body></html>';
        }

        $arrdata['emails'] = $emails;
        $arrdata['subject'] = $subject;
        $arrdata['message'] = $message;

        // print_r($arrdata);die();
        if (($message != "") && ($subject) && ($emails)) {
            $send_email = $this->main->send_email($emails, $message, $subject, $attachment);
        }
        return $send_email;
    }

    function search_hs() {
        if ($this->newsession->userdata('_LOGGED')) {
            // print_r($_POST);die();
            $by = $this->input->post('by');
            $name = $this->input->post('name');
            $addwhr = '';
            $temp = '';
            $fl = 0;

            if ($by == '1') {
                $addwhr = " and a.hs like '%" . $name . "%'";
            } elseif ($by == '2') {
                $exp = explode(' ', $name);
                foreach ($exp as $val) {
                    $addwhr .= " and a.uraiInd like '%" . $val . "%' ";
                }
            } elseif ($by == '3') {
                $exp = explode(' ', $name);
                foreach ($exp as $val) {
                    $addwhr .= " and a.uraiEng like '%" . $val . "%' ";
                }
            }

            $query = "  select a.hs,   CONCAT(a.uraiInd, ' <b>=></b> ',  b.uraiInd ) as  uraiInd, CONCAT(a.uraiEng, ' <b>=></b> ',  b.uraiEng ) as  uraiEng   from m_hs a 
						left join m_hs b on SUBSTRING(a.hs,1,4) = b.hs where LEN(a.hs) = 8
					" . $addwhr;
            $res = $this->db->query($query)->result_array();

            if (count($res) > 0) {
                $temp .= '<table class="table responsive-data-table data-table">';
                $temp .= '<thead>';
                $temp .= '<tr style="background: #383838; color: white;">';
                $temp .= '<th>  </th>';
                $temp .= '<th> No HS </th>';
                $temp .= '<th> Uraian Indonesia </th>';
                $temp .= '<th> Uraian English </th>';
                $temp .= '</tr>';
                $temp .= '</thead>';
                $temp .= '<tbody>';

                foreach ($res as $data) {
                    $temp .= '<tr>';
                    $temp .= '<td><input type="radio" onclick="set_search(' . $fl . ')" id="res_search"></td>';
                    $temp .= '<td>' . $data['hs'] . '<input type="hidden" id="hs_' . $fl . '" value="' . $data['hs'] . '"></td>';
                    $temp .= '<td>' . $data['uraiInd'] . '<input type="hidden" id="uraiInd_' . $fl . '" value="' . $data['uraiInd'] . '"></td>';
                    $temp .= '<td>' . $data['uraiEng'] . '<input type="hidden" id="uraiEng_' . $fl . '" value="' . $data['uraiEng'] . '"></td>';
                    $temp .= '</tr>';
                    $fl++;
                }

                $temp .= '</tbody>';
                $temp .= '</table>';
            } else {
                if ($by == '1') {
                    $temp .= '<p style="color: red;"> HS Code tidak ditemukan </p>';
                } elseif ($by == '2') {
                    $temp .= '<p style="color: red;"> Uraian Indonesia HS tidak ditemukan </p>';
                } elseif ($by == '3') {
                    $temp .= '<p style="color: red;"> Uraian English HS tidak ditemukan </p>';
                }
            }

            return $temp;
        }
    }

}

?>