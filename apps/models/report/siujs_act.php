<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Siujs_act extends CI_Model{
	
	var $ineng = "";
	
	function get_lstLapSiujs($dir, $doc, $type, $permohonan_id){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable;
			if($this->newsession->userdata('role') == "01"){
				$sql = "AND t_lap_siujs.status='0100'";
			}
			$query = "SELECT id, permohonan_id, 
					lap_tahun AS Tahun, 
					pemberi_tipe AS 'Pemberi Tipe',
					pemberi_nama AS 'Pemberi Nama',
					tgl_order AS 'Tanggal Order',
					-- lokasi_kab AS 'Kabupaten',
					-- lokasi_prop AS 'Provinsi',
					lokasi_alamat AS 'Alamat',
					kegiatan AS 'Kegiatan',
					ket AS 'Keterangan',
					m_reff.uraian AS 'Status'
					FROM t_lap_siujs 
					LEFT JOIN m_reff on t_lap_siujs.status = m_reff.kode AND m_reff.jenis='STATUS'
					WHERE permohonan_id = ".$permohonan_id. $sql;
			$table->title("");
			$table->columns(array("lap_tahun", "pemberi_tipe", "pemberi_nama", "tgl_order", "lokasi_alamat", "kegiatan", "ket", "status"));
			$this->newtable->width(array('Tahun' => 10, 'Pemberi Tipe' => 50, 'Pemberi Nama' => 50, 'Tanggal Order' => 50, 'Alamat' => 80, 'Kegiatan' => 50, 'Keterangan'=>100, 'Status' => 20));
			$this->newtable->search(array(array("lap_tahun","Tahun"),
										  array("pemberi_nama","Pemberi Nama"),
										  array("status","Status")
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/report/".$dir."/".$doc."/".$type."/".$permohonan_id."/");		
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("id"));
			$table->hiddens(array("id","permohonan_id"));
			$table->show_search(TRUE);
			$table->single(TRUE);
			$table->dropdown(TRUE);
			$table->hashids(TRUE);
			$table->postmethod(TRUE);
			$table->tbtarget("tb_lstLapSiujs".rand());
			if($this->newsession->userdata('role') == '05'){
			$table->menu(array('Tambah' => array('GET', site_url().'licensing/add_report/laporansiujs/'.$dir.'/'.$doc.'/'.$permohonan_id, '0', 'home', 'modal'),
						   'Edit' => array('BOOTSTRAPDIALOG', site_url().'licensing/add_report/laporansiujs/'.$dir.'/'.$doc.'/'.$permohonan_id.'/', '1', 'fa fa-edit'),
						   'Delete' => array('POST', site_url().'licensing/delete_laporan/siujs/ajax', 'N', 'fa fa-times','isngajax'),
						   'Pelaporan' => array('POST', site_url().'licensing/laporan/siujs/ajax/','N','fa  fa fa-edit','isngajax')
						   ));
			$table->show_chk(TRUE);			   
			}else{
			$table->show_chk(FALSE);
			}
			$arrdata['tblLapSiujs'] = $table->generate($query);
			$table->clear();
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	function get_data($menu, $dir, $doc, $permohonan_id,$id){
		if($this->newsession->userdata('_LOGGED')){
			$idx = hashids_decrypt($id,_HASHIDS_,9);
			$arrdata['id'] = $id;
			$arrdata['permohonan_id'] = $permohonan_id;
			$arrdata['propinsi'] =  $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2","id","nama", TRUE);

			if($id==""){
				$arrdata['act'] = site_url('post/report/siujs_act/'.$menu.'/save');				
			}else{
				$arrdata['act'] = site_url('post/report/siujs_act/'.$menu.'/update');
				$query = "SELECT a.lap_tahun, a.pemberi_tipe, a.pemberi_nama, a.tgl_order, a.lokasi_kab, a.lokasi_prop, a.lokasi_alamat, a.kegiatan, a.ket
						  FROM t_lap_siujs a 
						  WHERE a.permohonan_id = '".$permohonan_id."' and a.id = '".$idx."'";
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['sess'] = $row;
					}
					$arrdata['kab'] =  $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = ".$row['lokasi_prop']." ORDER BY 2","id","nama", TRUE);
				}
			}
			return $arrdata;
		}

	}

	function set_lap_siujs($act,$isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			$txt ='Data Laporan Usaha Jasa Survey';
			$msg = "MSG||NO||".$txt." gagal disimpan";
			$ressiup = FALSE;
			$arrsiup = $this->main->post_to_query($this->input->post('siujs'));
			$permohonan_id = $this->input->post('permohonan_id');
			if($act == "save"){
				$arrsiup['permohonan_id'] = $permohonan_id;
				$arrsiup['status'] = '0000';
				$arrsiup['created'] = 'GETDATE()';
				$arrsiup['created_user'] = $this->newsession->userdata('username');
				//print_r($arrsiup);die();
				$this->db->trans_begin();
				$exec = $this->db->insert('t_lap_siujs', $arrsiup);
				if($this->db->affected_rows() > 0){
					$idredir = $this->db->insert_id();
					$ressiup = TRUE;
					
					$msg = "MSG||YES||".$txt." berhasil disimpan.||REFRESH";
					
					/* Log User dan Log Izin */
					/* Akhir Log User dan Log Izin */

				}
				if($this->db->trans_status() === FALSE || !$ressiup){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				return $msg;
			}elseif ($act == "update") {
				$id = $this->input->post('id');
				$idx = hashids_decrypt($id,_HASHIDS_,9);
				$this->db->where('id',$idx);
				$this->db->where('permohonan_id',$permohonan_id);
				$this->db->update('t_lap_siujs',$arrsiup);
				if($this->db->affected_rows() > 0){
					$ressiup = TRUE;
					$msg = "MSG||YES||".$txt." berhasil diupdate.||REFRESH";

				}
				return $msg;
			}
		}
	}
	
	function del_laporan($data){	
			$this->db->trans_begin();
			$i=0;
			foreach($data as $id){
				$hasid = hashids_decrypt($id,_HASHIDS_,9);
				$status = $this->main->get_uraian("select status from t_lap_siujs where id='".$hasid."'","status");
				if($status == "0000"){
					$this->db->where('id',$hasid);
					$this->db->delete('t_lap_siujs');
					if($this->db->affected_rows() == 0){
						$i++;
						break;
					}
				}else{
					$i++;
					break;
				}
			}
			if($this->db->trans_status() === FALSE || $i > 0){
				$this->db->trans_rollback();
				$msg = "MSG#Proses Gagal#refresh";
			}else{
				$this->db->trans_commit();
				$msg = "MSG#Proses Berhasil#refresh";
			}
			return $msg;
	}
	
	function set_laporan($data){
			$this->db->trans_begin();
			$i=0;
			foreach($data as $id){
				$idx = hashids_decrypt($id,_HASHIDS_,9);
				$this->db->where("id",$idx);
				$this->db->update("t_lap_siujs",array("status"=>"0100"));
				if($this->db->affected_rows() == 0){
					$i++;
					break;
				}
			}
			if($this->db->trans_status() === FALSE || $i > 0){
				$this->db->trans_rollback();
				$msg = "MSG#Proses Gagal#refresh";
			}else{
				$this->db->trans_commit();
				$msg = "MSG#Proses Berhasil#refresh";
			}
			return $msg;
		}
}
?>