<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Siup4_act extends CI_Model{
	
	var $ineng = "";
	
	function get_lstLapSiup4($dir, $doc, $type, $permohonan_id){
		if($this->newsession->userdata('_LOGGED')){
			//print_r($dir.$doc.$type.$id);die();
			if($this->newsession->userdata('role') == "01"){
				$sql = "AND t_lap_siup4.status='0100'";
			}
			$table = $this->newtable;
			$query = "SELECT id, permohonan_id, 
					lap_tahun AS Tahun, 
					lap_omset AS Omset, 
					lap_omset_sebelum AS 'Omset Sebelum', 
					jml_ahli AS 'Jumlah Tenaga Ahli', 
					jml_karyawan AS 'Jumlah Karyawan',
					m_reff.uraian AS 'Status'
					FROM t_lap_siup4 
					LEFT JOIN m_reff on t_lap_siup4.status = m_reff.kode AND m_reff.jenis='STATUS'
					WHERE permohonan_id = ".$permohonan_id.$sql;
			$table->title("");
			$table->columns(array("lap_tahun", "lap_omset", "lap_omset_sebelum", "jml_ahli", "jml_karyawan","status"));
			$this->newtable->width(array('Tahun' => 10, 'Omset' => 50, 'Omset Sebelum' => 50, 'Jumlah Tenaga Ahli' => 50, 'Jumlah Karyawan' => 80, 'Status' => 20));
			$this->newtable->search(array(array("lap_tahun","Tahun"),
										  array("lap_omset","Omset")));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/report/".$dir."/".$doc."/".$type."/".$permohonan_id."/");		
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("id"));
			$table->hiddens(array("id","permohonan_id"));
			$table->show_search(TRUE);
			$table->single(TRUE);
			$table->dropdown(TRUE);
			$table->hashids(TRUE);
			$table->postmethod(TRUE);
			$table->tbtarget("tb_lstLapSiup4".rand());
			if($this->newsession->userdata('role') == "05"){
			$table->menu(array('Tambah' => array('GET', site_url().'licensing/add_report/laporansiup4/'.$dir.'/'.$doc.'/'.$permohonan_id, '0', 'home', 'modal'),
						   'Edit' => array('BOOTSTRAPDIALOG', site_url().'licensing/add_report/laporansiup4/'.$dir.'/'.$doc.'/'.$permohonan_id.'/', '1', 'fa fa-edit'),
						   'Delete' => array('POST', site_url().'licensing/delete_laporan/siup4/ajax', 'N', 'fa fa-times','isngajax'),
						   'Pelaporan' => array('POST', site_url().'licensing/laporan/siup4/ajax', 'N', 'fa  fa-edit ','isngajax')
						   ));
			$table->show_chk(TRUE);			   
			}else{
			$table->show_chk(FALSE);
			}
			
			$arrdata['tblLapSiup4'] = $table->generate($query);
			$table->clear();
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	function get_data($menu, $dir, $doc, $permohonan_id,$id){
		if($this->newsession->userdata('_LOGGED')){
			$idx = hashids_decrypt($id,_HASHIDS_,9);
			$arrdata['id'] = $id;
			$arrdata['permohonan_id'] = $permohonan_id;

			if($id==""){
				$arrdata['act'] = site_url('post/report/siup4_act/'.$menu.'/save');				
			}else{
				$arrdata['act'] = site_url('post/report/siup4_act/'.$menu.'/update');
				$query = "SELECT lap_tahun, lap_omset, lap_omset_sebelum, jml_ahli, jml_karyawan, kar_s3, kar_s2, kar_s1, kar_d3, kar_sma,kar_lain
						  FROM t_lap_siup4 
						  WHERE permohonan_id = '".$permohonan_id."' and id = '".$idx."'";
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['sess'] = $row;
					}
				}
			}
			return $arrdata;
		}

	}

	function set_lap_siup4($act,$isajax){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			$txt ='Data Laporan Perusahaan Perantara Perdagangan Properti';
			$msg = "MSG||NO||".$txt." gagal disimpan";
			$ressiup = FALSE;
			$arrsiup = $this->main->post_to_query($this->input->post('siup4'));
			$permohonan_id = $this->input->post('permohonan_id');
			if($act == "save"){
				$arrsiup['permohonan_id'] = $permohonan_id;
				$arrsiup['status'] = '0000';
				$arrsiup['created'] = 'GETDATE()';
				$arrsiup['created_user'] = $this->newsession->userdata('username');
				//print_r($arrsiup);die();
				$this->db->trans_begin();
				$exec = $this->db->insert('t_lap_siup4', $arrsiup);
				if($this->db->affected_rows() > 0){
					$idredir = $this->db->insert_id();
					$ressiup = TRUE;
					
					$msg = "MSG||YES||".$txt." berhasil disimpan.||REFRESH";
					
					/* Log User dan Log Izin */
					/* Akhir Log User dan Log Izin */

				}
				if($this->db->trans_status() === FALSE || !$ressiup){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				return $msg;
			}elseif ($act == "update") {
				$id = $this->input->post('id');
				$idx = hashids_decrypt($id,_HASHIDS_,9);
				$this->db->where('id',$idx);
				$this->db->where('permohonan_id',$permohonan_id);
				$this->db->update('t_lap_siup4',$arrsiup);
				if($this->db->affected_rows() > 0){
					$ressiup = TRUE;
					$msg = "MSG||YES||".$txt." berhasil diupdate.||REFRESH";

				}
				return $msg;
			}
	}
	
	function del_laporan($data){
		$this->db->trans_begin();
		$i=0;
		foreach($data as $id){
			$hasid = hashids_decrypt($id,_HASHIDS_,9);
			$this->db->where('id',$hasid);
			$this->db->delete('t_lap_siup4');
			if($this->db->affected_rows() == 0){
				$i++;
				break;
			}
		}
		if($this->db->trans_status() === FALSE || $i > 0){
			$this->db->trans_rollback();
			$msg = "MSG#Proses Gagal#refresh";
		}else{
			$this->db->trans_commit();
			$msg = "MSG#Proses Berhasil#refresh";
		}
		return $msg;
		
	}
	
	function set_laporan($data){
			$this->db->trans_begin();
			$i=0;
			foreach($data as $id){
				$idx = hashids_decrypt($id,_HASHIDS_,9);
				$this->db->where("id",$idx);
				$this->db->update("t_lap_siup4",array("status"=>"0100"));
				if($this->db->affected_rows() == 0){
					$i++;
					break;
				}
			}
			if($this->db->trans_status() === FALSE || $i > 0){
				$this->db->trans_rollback();
				$msg = "MSG#Proses Gagal#refresh";
			}else{
				$this->db->trans_commit();
				$msg = "MSG#Proses Berhasil#refresh";
			}
			return $msg;
		}
}
?>