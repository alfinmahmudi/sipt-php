<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Formating_act extends CI_Model{
	public function list_number(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
			$query = "SELECT a.kd_izin, case when a.nomor = '49' then 'Informasi Pendaftaran Petunjuk Penggunaan (Manual) dan Kartu Garansi (Garansi)/Purna Jual Dalam Bahasa Indonesia Bagi Produk Telematika/Elektronika' else b.nama_izin end AS 'Perizinan', 
a.nomor AS 'Nomor Terakhir', CASE WHEN a.reset = 'Y' THEN 'Tahun' WHEN a.reset = 'M' THEN 'Bulan' WHEN a.reset = 'D' THEN 'Hari' END AS 'Reset', CAST(a.berlaku AS VARCHAR) + (CASE WHEN a.berlaku_by = 'Y' THEN ' Tahun' WHEN a.berlaku_by = 'M' THEN ' Bulan' END) AS 'Berlaku'
FROM m_nomor a LEFT JOIN m_izin b ON a.kd_izin = b.id";
			$table->title("");
			$table->columns(array("a.kd_izin", "b.nama_izin", "a.nomor", "CASE WHEN a.reset = 'Y' THEN 'Tahun' WHEN a.reset = 'M' THEN 'Bulan' WHEN a.reset = 'D' THEN 'Hari' END", "CAST(a.berlaku AS VARCHAR) + (CASE WHEN a.berlaku_by = 'Y' THEN ' Tahun' WHEN a.berlaku_by = 'M' THEN ' Bulan' END)"));
			$this->newtable->width(array('Perizinan' => 300 , 'Format Penomoran' => 150, 'Nomor Terakhir' => 75, "Reset" => 60, "Berlaku" => 70));
			$this->newtable->search(array(array("b.nama_izin","Perizinan")));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."reference/view/formating");
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("kd_izin"));
			$table->hiddens(array("kd_izin"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(TRUE);
			$table->dropdown(TRUE);
			$table->hashids(TRUE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_formating_".rand());
			$table->menu(array('Edit' => array('BOOTSTRAPDIALOG', site_url().'reference/formating_edit/', '1','fa fa-external-link')
							   ));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Format Penomoran Surat Perizinan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}
	
	public function get_user($id){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			$arrdata = array();
			if($id == ""){
				$arrdata['act'] = site_url() . 'post/reference/users_ac/save';
			}else{
				$arrdata['act'] = site_url() . 'post/reference/users_ac/update';
			}
			$arrdata['role'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'ROLE' AND kode NOT IN ('99','05') ORDER BY 2 ASC","kode","uraian", TRUE);
			$arrdata['direktorat'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT'","kode","uraian", TRUE);
			$arrdata['status'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS_USER'","kode","uraian", TRUE);
			return $arrdata;
		}
	}
	
	public function set_format($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			if($act == "update"){
				if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
				}
				$ret = "MSG||NO||Data pengguna gagal diupdate";
				$resuser = FALSE;
				$arrusers = $this->main->post_to_query($this->input->post('format'));
				$idx = hashids_decrypt($this->input->post('id'), _HASHIDS_, 9);
				$arrusers['updated'] = 'GETDATE()';
				unset($arrusers['nama_izin']);
				$this->db->trans_begin();
				$this->db->where("kd_izin",$idx);
				$this->db->update('m_nomor', $arrusers);
				if($this->db->affected_rows() > 0){
					$resuser = TRUE;
				}
				if($this->db->trans_status() === FALSE || !$resuser){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$ret = "MSG||YES||Data berhasil diupdate||REFRESH";
				}
			}
			return $ret;
		}
	}
	
	public function get_format($id){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			$id = hashids_decrypt($id, _HASHIDS_, 9);
			$arrdata = array();
			$arrdata['arr_cmb'] = array(""=>"","Y"=>"Tahun","M"=>"Bulan","D"=>"Hari"); 
			$arrdata['act'] = site_url() . 'post/reference/formating_ac/update';
			$query = "SELECT a.kd_izin, b.nama_izin AS 'Perizinan', a.nomor,  a.reset, CAST(a.berlaku AS VARCHAR) + (CASE WHEN a.berlaku_by = 'Y' THEN ' Tahun' WHEN a.berlaku_by = 'M' THEN ' Bulan' END) AS 'berlaku', a.berlaku, a.berlaku_by
			FROM m_nomor a LEFT JOIN m_izin b ON a.kd_izin = b.id WHERE a.kd_izin = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['sess'] = $row;
				}
			}
			
			return $arrdata;
		}
	}
	
}
?>