<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 259200);

class Rekap_pulau_act extends CI_Model {

    function excel_antrpl() {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrpost = $this->input->post('antar_pulau');
            $whr = '';
            if ($arrpost['tgl_awal'] != '' && $arrpost['tgl_akhir'] != '') {
                $whr = " AND a.etd BETWEEN '" . $arrpost['tgl_awal'] . "' AND '" . $arrpost['tgl_akhir'] . "'";
            }
            if ($arrpost['tgl_awal2'] != '' && $arrpost['tgl_akhir2'] != '') {
                $whr .= " AND a.eta BETWEEN '" . $arrpost['tgl_awal2'] . "' AND '" . $arrpost['tgl_akhir2'] . "'";
            }
            if ($arrpost['pel_muat'] != '') {
                $whr .= " AND a.pel_asal = '" . $arrpost['pel_muat'] . "'";
            }
            if ($arrpost['pel_bongkar'] != '') {
                $whr .= " AND a.pel_tujuan = '" . $arrpost['pel_bongkar'] . "'";
            }
            if ($arrpost['npwp'] != '') {
                $whr .= " AND a.npwp = '" . $arrpost['npwp'] . "'";
            }
            if ($arrpost['nama'] != '') {
                $whr .= " AND a.nama_penerima = '" . $arrpost['nama'] . "'";
            }
            if ($arrpost['prop_asal'] != '') {
                $whr .= " AND a.prop_asal = '" . $arrpost['prop_asal'] . "'";
            }
            if ($arrpost['prop_tujuan'] != '') {
                $whr .= " AND a.prop_tujuan = '" . $arrpost['prop_tujuan'] . "'";
            }
            if ($arrpost['kapal'] != '') {
                $whr .= " AND a.nama_kapal = '" . $arrpost['kapal'] . "'";
            }
            if ($arrpost['hs'] != '') {
                $iddtl = $this->get_hs_id($arrpost['hs']);
                if (trim($iddtl) != '') {
                    $whr .= ' AND a.id in (' . $iddtl . ')';
                } else {
                    die("Pencarian Komoditi HS Tersebut Tidak ada");
                }
            }
            if ($whr == '') {
                die("You Must Add Some Filters");
            }
            $header = array("no_aju", "created", "jenis_pelapor", "npwp_pengirim", "nama_pengirim", "tp_perusahaan_pengirim", "alamat_pengirim", "prop_pengirim", "kab_pengirim", "kec_pengirim", "kel_pengirim", "telp", "email", "fax", "npwp_penerima", "nama_penerima", "tp_perusanaan_penerima", "alamat_penerima", "prop_penerima", "kab_penerima", "kec_penerima", "kel_penerima", "telp_penerima", "fax_penerima", "email_penerima", "pel_asal", "pel_tujuan", "jenis_asal", "negara_tujuan", "kab_asal", "kab_tujuan", "nama_kapal", "eta", "etd", "jumlah_ekspedisi", "tp_perusaaan_ekspedisi1", "nama_ekspedisi1", "pj_ekspedisi1", "npwp_ekspedisi1", "alamat_ekspedisi1", "prop_ekspedisi1", "kab_ekspedisi1", "kec_ekspedisi1", "kel_ekspedisi1", "telp_ekspedisi1", "email_ekspedisi1", "tp_perusaaan_ekspedisi2", "nama_ekspedisi2", "pj_ekspedisi2", "npwp_ekspedisi2", "alamat_ekspedisi2", "prop_ekspedisi2", "kab_ekspedisi2", "kec_ekspedisi2", "kel_ekspedisi2", "telp_ekspedisi2", "email_ekspedisi2");

            $sql = "SELECT a.id, a.no_aju as no_aju, a.created as created, b.uraian as jenis_pelapor,
              '''' + a.npwp as npwp_pengirim, a.nama as nama_pengirim, c.uraian as tp_perusahaan_pengirim, a.alamat as alamat_pengirim, d.nama as prop_pengirim,
              e.nama as kab_pengirim, f.nama as kec_pengirim, g.nama as kel_pengirim, '''' +  a.telp as telp, a.email as email, '''' +  a.fax as fax,
             '''' + a.npwp_penerima as npwp_penerima, a.nama_penerima as nama_penerima, h.uraian as tp_perusanaan_penerima, a.alamat_penerima as alamat_penerima,  i.nama as prop_penerima, j.nama as kab_penerima,
              k.nama as kec_penerima, l.nama as kel_penerima, '''' + a.telp_penerima as telp_penerima, '''' + a.fax_penerima as fax_penerima, a.email_penerima as email_penerima, m.kode + ' - ' +m.uraian as pel_asal,
              n.kode + ' - ' +n.uraian as pel_tujuan, o.uraian as jenis_asal , p.nama as negara_tujuan, q.nama as kab_asal , r.nama as kab_tujuan,
              a.nama_kapal, dbo.dateIndo(a.eta) as eta, dbo.dateIndo(a.etd) as etd , (select COUNT(*) from t_lap_pulau_eks where id_lap = a.id) as jumlah_ekspedisi,
              t.uraian as tp_perusaaan_ekspedisi1, s1.nama as nama_ekspedisi1, s1.nama_pj as pj_ekspedisi1, '''' + s1.npwp as npwp_ekspedisi1,
              s1.alamat as alamat_ekspedisi1, u.nama as prop_ekspedisi1, v.nama as kab_ekspedisi1, w.nama as kec_ekspedisi1 , x.nama as kel_ekspedisi1,
              '''' + s1.telp as telp_ekspedisi1, s1.email as email_ekspedisi1,
              y.uraian as tp_perusaaan_ekspedisi2, s2.nama as nama_ekspedisi2, s2.nama_pj as pj_ekspedisi2, '''' + s2.npwp as npwp_ekspedisi2,
              s2.alamat as alamat_ekspedisi2, z.nama as prop_ekspedisi2, aa.nama as kab_ekspedisi2, ab.nama as kec_ekspedisi2 , ac.nama as kel_ekspedisi2,
              '''' + s2.telp as telp_ekspedisi2, s2.email as email_ekspedisi2
              from t_lap_pulau a
              left join m_reff b on a.jenis_pelapor = b.kode and b.jenis = 'JENIS_PELAPOR'
              left join m_reff c on a.tipe_perushaan = c.kode and c.jenis = 'TIPE_PERUSAHAAN'
              left join m_prop d on a.kdprop = d.id
              left join m_kab e on a.kdkab = e.id
              left join m_kec f on a.kdkec = f.id
              left join m_kel g on a.kdkel = g.id
              left join m_reff h on a.tipe_perushaan_penerima = h.kode and h.jenis = 'TIPE_PERUSAHAAN'
              left join m_prop i on a.kdprop_penerima = i.id
              left join m_kab j on a.kdkab_penerima = j.id
              left join m_kec k on a.kdkec_penerima = k.id
              left join m_kel l on a.kdkel_penerima = l.id
              left join m_peldn m on a.pel_asal = m.kode
              left join m_peldn n on a.pel_tujuan = n.kode
              left join m_reff o on a.jenis_asal = o.kode and o.jenis = 'JENIS_ASAL'
              left join m_negara p on a.negara_tujuan = p.kode
              left join m_kab q on a.kab_asal = q.id
              left join m_kab r on a.kab_tujuan = r.id
              left join t_lap_pulau_eks s1 on s1.id_lap = a.id  and s1.no_urut = 1
              left join t_lap_pulau_eks s2 on s2.id_lap = a.id  and s2.no_urut = 2
              left join m_reff t on s1.tipe_perushaan = t.kode and t.jenis = 'TIPE_PERUSAHAAN'
              left join m_prop u on s1.kdprop = u.id
              left join m_kab v on s1.kdkab = v.id
              left join m_kec w on s1.kdkec = w.id
              left join m_kel x on s1.kdkel = x.id
              left join m_reff y on s2.tipe_perushaan = y.kode and y.jenis = 'TIPE_PERUSAHAAN'
              left join m_prop z on s2.kdprop = z.id
              left join m_kab aa on s2.kdkab = aa.id
              left join m_kec ab on s2.kdkec = ab.id
              left join m_kel ac on s2.kdkel = ac.id
              where DATEDIFF(day,a.etd, getdate()) > 7  and a.trader_id <> 140 " . $whr;

            $data_det = $this->db->query($sql)->result_array();
        }
        $arrdata['header'] = $header;
        $arrdata['detil'] = $data_det;
        $arrdata['header_ex'] = $header_ex;
        $arrdata['jum_data'] = $jum_data;
        $arrdata['hs'] = $arrpost['hs'];
        return $arrdata;
    }

    function get_detil($id, $hs = "") {
        $arrdet['header'] = array("HS", "HS_URAI", "HS_JUM", "SAT_URAI");
        $where = "";
        if (trim($hs) != "") {
            $where = " And a.hs = " . $this->db->escape($hs);
        }
        $query = "SELECT '''' + a.hs as 'HS', a.uraian as 'HS_URAI', a.jumlah as 'HS_JUM', b.uraian  as 'SAT_URAI'
                FROM t_lap_pulau_dtl a
                LEFT JOIN m_satuan b ON b.satuan = a.satuan
                WHERE a.id_lap = " . $id . $where;
        $arrdet['detil'] = $this->db->query($query)->result_array();
        return $arrdet;
    }

    function get_hs_id($hs) {
        $sql = "select a.id_lap from t_lap_pulau_dtl a where a.hs = " . $this->db->escape($hs) . " group by a.id_lap";
        $result = $this->db->query($sql)->result_array();
        $arrdata = array();
        foreach ($result as $row) {
            $arrdata[] = $row['id_lap'];
        }
        return implode(',', $arrdata);
    }

    function validateDate($date, $format = 'Y-m-d') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

}

?>