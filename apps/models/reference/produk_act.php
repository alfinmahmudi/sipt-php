<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Produk_act extends CI_Model{
	public function list_produk(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
			$query = "SELECT a.id as ID, a.kode as 'Kode Produk', a.produk as 'Nama Produk' , a.urut as 'Nomor Urut' FROM m_produk a";
			$table->title("");
			$table->columns(array("a.id", "a.kode", "a.produk"));
			$this->newtable->width(array('Nama Pengguna' => 100 , 'Nama Lengkap' => 300, 'Media Komunikasi' => 200, "Akses" => 170, "Status" => 100));
			$this->newtable->search(array(array("a.kode","Kode Produk"),
										  array("a.produk","Nama Produk"),
										  array("a.urut","Nomor Urut")
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."reference/view/produk");
			$table->orderby(3);
			$table->sortby("DESC");
			$table->keys(array("ID"));
			$table->hiddens(array("ID"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_users");
			$table->menu(array('Tambah' => array('GET', site_url().'reference/add_produk/new', '0', 'home', 'modal'),
								'Edit' => array('BOOTSTRAPDIALOG', site_url().'reference/dialog_prod/', '1', 'fa fa-edit'),
							   'Delete' => array('POST', site_url().'reference/del_prod/', 'N', 'fa fa-times','isngajax')
							   ));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Produk Master');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	public function get_produk($act, $id=''){
		if($this->newsession->userdata('_LOGGED')){
			if ($act == 'new') {
				$arrdata['act'] = site_url() . 'post/reference/produk/save';
			}else{
				$sql = "SELECT a.id, a.kode, a.produk, a.urut FROM m_produk a where a.id = '".$id."' ";
				$datasess = $this->db->query($sql)->row_array();
				$arrdata['sess'] = $datasess;
				$arrdata['act'] = site_url() . 'post/reference/produk/update';
			}
 			return $arrdata;
		}
	}
	
	public function set_produk($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
			}
			$msg = "MSG||NO||Data Produk Gagal Disimpan";
			$resprod = FALSE;
			$arrprod = $this->main->post_to_query($this->input->post('prod'));
			$arrprod['reset'] = "Y";
			if($act == "save"){
				$this->db->trans_begin();
				$this->db->insert('m_produk', $arrprod);
				if($this->db->affected_rows() > 0){
					$resprod = TRUE;
					$logu = array('aktifitas' => 'Menambahkan Master Produk dengan nama Kode Produk : ' . $arrprod['kode'].', Nama Produk :  '. $arrprod['produk'] . ' dan Nomor Urut : '. $arrprod['urut'],
                        'url' => '{c}' . site_url() . 'post/reference/produk/save' . ' {m} models/reference/produk_act {f} set_produk($act, $isajax)');
                    $this->main->set_activity($logu);
				}
				if($this->db->trans_status() === FALSE || !$resprod){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$msg = "MSG||YES||Data berhasil disimpan||REFRESH";
				}
				return $msg;
			}else{
				$id = $this->input->post('id');
				$this->db->trans_begin();
				$this->db->where('id', $id);
				$this->db->update('m_produk', $arrprod);
				if($this->db->affected_rows() > 0){
					$resprod = TRUE;
					$logu = array('aktifitas' => 'Mengupdate Master Produk dengan nama Kode Produk : ' . $arrprod['kode'].', Nama Produk :  '. $arrprod['produk'] . ' dan Nomor Urut : '. $arrprod['urut'],
                        'url' => '{c}' . site_url() . 'post/reference/produk/save' . ' {m} models/reference/produk_act {f} set_produk($act, $isajax)');
                    $this->main->set_activity($logu);
				}
				if($this->db->trans_status() === FALSE || !$resprod){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$msg = "MSG||YES||Data berhasil diupdate||REFRESH";
				}
				return $msg;
			}
		}
	}

	public function delete_prod($id){
		if($this->newsession->userdata('_LOGGED')){
			// $id = hashids_decrypt($id, _HASHIDS_, 9);print_r($id);die();
			$this->db->trans_begin();
			foreach ($id as $data) {
            	$this->db->delete('m_produk', array('id' => $data));
        	}
			if ($this->db->affected_rows() > 0) {
            		$logu = array('aktifitas' => 'Mendelete Master Produk',
                        'url' => '{c}' . site_url() . 'reference/del_prod' . ' {m} models/reference/produk_act {f} delete_prod($id)');
                    $this->main->set_activity($logu);
            	}
			if ($this->db->trans_status === FALSE) {
            	$this->db->trans_rollback();
            	$msg = "MSG#Proses Gagal#refresh";
	        } else {
	            $this->db->trans_commit();
            	$msg = "MSG#Proses Berhasil#refresh";
	        }
			return $msg;
		}
	}
}
?>