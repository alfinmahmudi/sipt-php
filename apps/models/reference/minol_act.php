<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Users_act extends CI_Model{
	public function list_minol(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
			$arrakses = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'ROLE' ORDER BY 2 ASC", "kode", "uraian");
			$arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS_USER' ORDER BY 2 ASC", "kode", "uraian");
			$query = "SELECT a.id, a.username AS 'Nama Pengguna', a.nama AS 'Nama Lengkap', 'e : ' + a.email + '<div> t : ' + a.telp + '</div>' AS 'Media Komunikasi', b.uraian AS 'Akses', c.uraian AS 'Status' 
					  FROM t_user a LEFT JOIN m_reff b ON a.role = b.kode AND b.jenis = 'ROLE' LEFT JOIN m_reff c ON a.status = c.kode AND c.jenis = 'STATUS_USER' 
					  WHERE a.role <> '99'";
			$table->title("");
			$table->columns(array("a.id", "a.username", "a.nama", "'e : ' + a.email + '<div> t : ' + a.telp + '</div>'", "b.uraian", "c.uraian"));
			$this->newtable->width(array('Nama Pengguna' => 100 , 'Nama Lengkap' => 300, 'Media Komunikasi' => 200, "Akses" => 170, "Status" => 100));
			$this->newtable->search(array(array("a.username","Nama Pengguna"),
										  array("a.nama","Nama Lengkap"),
										  array("a.role", "Akses Pengguna", array("ARRAY", $arrakses)),
										  array("a.status", "Status Pengguna", array("ARRAY", $arrstatus))
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."reference/view/users");
			$table->orderby(3);
			$table->sortby("ASC");
			$table->keys(array("id"));
			$table->hiddens(array("id"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_users");
			$table->menu(array('Tambah' => array('GET', site_url().'reference/users/new', '0', 'home', 'modal'),
								'Edit' => array('BOOTSTRAPDIALOG', site_url().'reference/users/update/', '1', 'fa fa-edit'),
							   'Delete' => array('POST', site_url().'reference/del_user/', 'N', 'fa fa-times','isngajax')
							   ));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Data Pengguna');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}
	
	public function get_user(){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			$arrdata = array();
			$arrdata['act'] = site_url() . 'post/reference/users_ac/save';
			
			$arrdata['role'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'ROLE' AND kode NOT IN ('99','05') ORDER BY 2 ASC","kode","uraian", TRUE);
			$arrdata['direktorat'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT'","kode","uraian", TRUE);
			$arrdata['status'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS_USER'","kode","uraian", TRUE);
			return $arrdata;
		}
	}
	
	public function user_id($id){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			//$id = hashids_decrypt($id, _HASHIDS_, 9);
 			$arrdata = array();
			$query = "SELECT a.id,e.kode AS 'dir',a.telp,a.email, a.username AS 'Nama Pengguna',a.alamat,a.role, a.status,a.nama AS 'Nama Lengkap', b.uraian AS 'Akses', c.uraian AS 'Status' 
					  FROM t_user a LEFT JOIN t_user_role d on d.user_id = a.id LEFT JOIN m_reff b ON a.role = b.kode AND b.jenis = 'ROLE' LEFT JOIN m_reff c ON a.status = c.kode AND c.jenis = 'STATUS_USER' LEFT JOIN m_reff e ON d.direktorat = e.kode AND e.jenis='DIREKTORAT'  WHERE id ='".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['sess'] = $row;
				}
			}
			//print_r($arrdata);die();
			$arrdata['role'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'ROLE' AND kode NOT IN ('99','05') ORDER BY 2 ASC","kode","uraian", TRUE);
			$arrdata['direktorat'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT'","kode","uraian", TRUE);
			$arrdata['status'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS_USER'","kode","uraian", TRUE);
			$arrdata['act'] = site_url() . 'post/reference/users_ac/update';
			//print_r($arrdata);die();
			return $arrdata; 
		}
	}
	
	public function set_user($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			if($act == "save"){
				if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
				}
				$msg = "MSG||NO||Data pengguna gagal disimpan";
				$resuser = FALSE;
				$resrole = FALSE;
				$arrusers = array();
				$arrusers = $this->main->post_to_query($this->input->post('USERS'));
				$arrusers['password'] = md5('123');
				$arrusers['trader_id'] = '1';
				$arrusers['created'] = 'GETDATE()';
				$this->db->trans_begin();
				$this->db->insert('t_user', $arrusers);
				if($this->db->affected_rows() > 0){
					$resuser = TRUE;
					$user_id = $this->db->insert_id();
				}
				if($resuser){
					$i = 0;
					$countizin = count($this->input->post('dokumen'));
					foreach($this->input->post('dokumen') as $a){
						$arrrole['user_id'] = $user_id;
						$arrrole['direktorat'] = $this->input->post('direktorat');
						$arrrole['izin_id'] = $a;
						$arrrole['role'] = $arrusers['role']; 
						$this->db->insert('t_user_role', $arrrole);
						if($this->db->affected_rows() > 0){
							$i++;
						}
					}
					//echo $i." ".$countizin;die();
					if($i == $countizin){
						$resrole = TRUE;
					}
				}
				if($this->db->trans_status() === FALSE || !$resrole){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$ret = "MSG||YES||Data berhasil disimpan||REFRESH";
				}
			}elseif($act == "update"){
				$msg = "MSG||NO||Data pengguna gagal diupdate";
				$id = $this->input->post("id");
				$arrusers = $this->main->post_to_query($this->input->post('USERS'));
				$this->db->where('id',$id);
				$this->db->update('t_user',$arrusers);
				if($this->db->affected_rows() > 0){
					$ret = "MSG||YES||Data berhasil diupdate||REFRESH";
				}
				
			}
			return $ret;
		}
	}
	
	function delete_user($data){
		$this->db->trans_begin();
		$i=0;
		//print_r($data);die();
		foreach($data as $id){
			//$idx =  hashids_decrypt($id, _HASHIDS_, 9);
			$this->db->where('id',$id);
			$this->db->delete('t_user');
			if($this->db->affected_rows() > 0){
				$i++;
			}else{
				$i=0;
				break;
			}
		}	
		if($this->db->trans_status() === FALSE || $i == 0){
			$this->db->trans_rollback();
			$msg = "MSG#Proses Gagal#refresh";
		}else{
			$this->db->trans_commit();
			$msg = "MSG#Proses Berhasil#refresh";
		}
		return $msg;
	}
}
?>