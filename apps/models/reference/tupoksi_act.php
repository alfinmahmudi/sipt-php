<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Tupoksi_act extends CI_Model{
	public function view_licensing(){
		if($this->newsession->userdata('_LOGGED')){
			$sql = "SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT'";
			$arrdata['direktorat'] = $this->main->set_combobox($sql,"kode","uraian", TRUE);
			$m = $this->main->get_result($sql);
			if($m){
				foreach($sql->result_array() as $r){
					$dir[] = $r;
				}
			}
			//print_r($dir);die();
			$i=0;
				foreach($dir as $b){
					$q = "select a.id,a.nama_izin, b.uraian from m_izin a left JOIN m_reff b on a.direktorat_id= b.kode where a.direktorat_id='".$b['kode']."' AND b.jenis='DIREKTORAT' AND a.aktif=1 ";
					$j=$this->main->get_result($q);
					if($j){
						foreach($q->result_array() as $w){
							$n[$b['kode']][] = $w;
						}	
					}	
				$i++;
				}
				
			$arrdata['dir'] = $dir;
			$arrdata['tupoksi'] = "tupoksi";	
                        $arrdata['judul'] = 'Master Tupoksi';
			$arrdata['izin'] = $n;
			$arrdata['act'] = site_url('tupoksi/get_tupoksi');
			$arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'","kode","uraian");
			return $arrdata;
		}
	}

	public function get_tupoksi(){
		if($this->newsession->userdata('_LOGGED')){
			$izin_id = explode('|', $this->input->post('dokumen'));
			$sql = "SELECT a.telaah, b.uraian as role, a.jenis, a.id
					FROM m_telaah a 
					LEFT JOIN m_reff b ON b.kode = a.role AND b.jenis = 'ROLE'
					where a.izin_id = '".$izin_id[1]."' ";
			$sess = $this->db->query($sql)->result_array();
			$arrdata['nama_izin'] = $this->db->query("SELECT nama_izin FROM m_izin where id = '".$izin_id[1]."' ")->row_array();
			$arrdata['sess'] = $sess;
			$arrdata['act'] = site_url('tupoksi/set_tupoksi');
			return $arrdata;
		}
	}

	public function set_tupoksi($isajax){
		if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            $msg = "MSG||NO||Data gagal disimpan";
            $ressla = FALSE;
			$arrsla = $this->input->post('sla');
			//print_r($arrsla);die();
			$i = 0;
			foreach ($arrsla['id'] as $key) {
				$arrsave['telaah'] = $arrsla['sla_waktu'][$i];
				$this->db->trans_begin();
				$this->db->where('id', $key);
				$this->db->update('m_telaah', $arrsave);
				if ($this->db->affected_rows() > 0) {
					$logu = array('aktifitas' => 'Mengupdate Telaah ke m_telaah',
                    'url' => '{c}' . site_url() . 'tupoksi/set_tupoksi' . ' {m} models/tupoksi_act/set_tupoksi {f} set_tupoksi($isajax)');
                	$this->main->set_activity($logu);
                	$ressla = TRUE;
                	$msg = "MSG||YES||Data berhasil disimpan||REFRESH";
				}
				if ($this->db->trans_status() === FALSE || $ressla == FALSE) {
                	$this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                $i++;
			}
			return $msg;
		}
	}
}
?>