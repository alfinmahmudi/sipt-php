<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Agen_act extends CI_Model{
	public function __construct(){
		 parent::__construct();
		 //$this->db2= $this->load->database('default2',TRUE);
	}
	public function get_agen(){
		$db2= $this->load->database('default2',TRUE);		
		//print_r($db2);, a.No_Sertifikat_Agen AS 'NO Sertifikat', a.Produsen_Negara AS 'Produsen Negara',a.NO_SP_Sertifikat_Agen AS 'NO Sertifikat Agen', a.Produsen_Nama AS ' Nama Produsen', a.Produsen_Alamat AS 'Alamat Produsen', a.Supplier_Nama AS 'Nama Supplier', a.Supplier_Alamat AS 'Alamat Supplier', a.Jenis_Barang AS 'Jenis Barang', a.Merk AS 'Merk', a.No_HS AS 'NO HS', a.Wil_Pemasaran AS 'Wilayah Pemasaran'
		if($this->newsession->userdata('_LOGGED')){
				$table = $this->newtable;
				$query = "SELECT '<div>'+a.Status_Ket_1+'</div><div>'+a.Status_Ket_2+'</div><div>'+a.No_Sertifikat_Agen+'</div>' AS 'Status Agen',"
						."'<div>'+a.Produsen_Nama+'</div><div>'+a.Produsen_Negara+'</div>' AS 'Produsen',"
						."'<div>'+a.Supplier_Nama +'</div><div>'+a.Supplier_Alamat+'</div>' AS 'Supplier',"
						."a.Jenis_Barang AS 'Jenis Barang', a.Merk AS 'Merk', a.No_HS AS 'NO HS', a.Wil_Pemasaran AS 'Pemasaran'"
						."from AGEN_view_master a where a.Status_Ket_1 IS NOT NULL";
				$table->title("");
				$table->columns(array("a.Status_Ket_1","a.Status_Ket_2"));
				$this->newtable->width(array('Status' => 150, 'Produsen'=>150,'Supplier' => 150, 'Jenis Barang'=>200,'Merk'=>50,'NO HS'=>30,'Wilayah Pemasaran'=>100));
				$this->newtable->search(array(array("a.Status_Ket_2","JENIS PRODUKSI"),
											  array("a.Status_Ket_1","STATUS"))); 
				$table->cidb($db2);
				$table->ciuri($this->uri->segment_array());
				$table->action(site_url().'licensing/view/agen'); 
				$table->show_search(TRUE);
				$table->show_chk(FALSE);
				$table->single(TRUE);
				$table->dropdown(TRUE);
				$table->use_ajax(TRUE);
				$table->hashids(TRUE);
				$table->postmethod(TRUE);
				$table->title(TRUE);
				$table->tbtarget("refkbli");
				//echo $table->generate($query);die();
				if($this->input->post("data-post")) return $table->generate($query);
				else return array("judul"=>"Master Keagenan", "tabel"=>$table->generate($query));
		}
	}
}
?>