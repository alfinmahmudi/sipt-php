<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Npwp_act extends CI_Model{
	public function list_npwp(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
			$query = "select id AS id, npwp AS NPWP, nama AS Nama_Perusahaan from m_trader_refinasi";
			$table->title("");
			$table->columns(array("id", "npwp", "nama"));
			$this->newtable->width(array('NPWP' => 250 , 'Nama_Perusahaan' => 250));
			$this->newtable->search(array(array("npwp","NPWP"),
										  array("nama","Nama Perusahaan")));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."reference/view/npwp");
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("id"));
			$table->hiddens(array("id"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(TRUE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_npwp".rand());
			$table->menu(array('Tambah' => array('GET', site_url().'reference/npwp', '0', 'home', 'modal'),
							   'Edit' => array('BOOTSTRAPDIALOG', site_url().'reference/npwp/', '1', 'fa fa-edit'),
							   'Delete' => array('POST', site_url().'reference/del_npwp/', 'N', 'fa fa-times','isngajax')));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Data NPWP Perusahaan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}
	
	public function get_npwp($id){
		if($this->newsession->userdata('_LOGGED') && ($this->newsession->userdata('role') == "01" || $this->newsession->userdata('role') == "99")){
			$arrdata = array();
			if($id == ""){
				$arrdata['act'] = site_url() . 'post/reference/npwp_ac/save';
			}else{
				$id = hashids_decrypt($id, _HASHIDS_, 9);
				$arrdata['act'] = site_url() . 'post/reference/npwp_ac/update';
				$query = "SELECT id, npwp, nama
						FROM m_trader_refinasi WHERE id = '".$id."'";
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['sess'] = $row;
					}
				}
			}
			return $arrdata;
		}
	}
	
	public function set_npwp($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "01"){
			if($act == "save"){
				if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
				}
				//print_r($_POST);die();
				$ret = "MSG||NO||Data penanda tangan gagal disimpan";
				$resttd = FALSE;
				$arrttd = $this->main->post_to_query($this->input->post('npwp'));
				$arrttd['created'] = 'GETDATE()';
				$this->db->trans_begin();
				$this->db->insert('m_trader_refinasi', $arrttd);
				if($this->db->affected_rows() > 0){
					$resttd = TRUE;
				}
				
				if($this->db->trans_status() === FALSE || !$resttd){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$ret = "MSG||YES||Data NPWP berhasil disimpan||REFRESH";
				}
			}
			else if($act == "update"){
				if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
				}
				$ret = "MSG||NO||Data penanda tangan gagal disimpan";
				$resttd = FALSE;
				$arrttd = $this->main->post_to_query($this->input->post('npwp'));
				$idx = hashids_decrypt($this->input->post('id'), _HASHIDS_, 9);
				$this->db->trans_begin();
				$this->db->where('id', $idx);
				$this->db->update('m_trader_refinasi', $arrttd);
					if($this->db->affected_rows() > 0){
						$resttd = TRUE;
					}
				if($this->db->trans_status() === FALSE || !$resttd){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$ret = "MSG||YES||Data NPWP berhasil diupdate||REFRESH";
				}	
			}
			
			return $ret;
		}
	}
	
	function delete_npwp($data){
		$this->db->trans_begin();
		$i=0;
		foreach($data as $id){
			$idx =  hashids_decrypt($id, _HASHIDS_, 9);
			$this->db->where('id',$idx);
			$this->db->delete('m_trader_refinasi');
			if($this->db->affected_rows() > 0){
				$i++;
			}else{
				$i=0;
				break;
			}
		}
		if($this->db->trans_status() === FALSE || $i == 0){
			$this->db->trans_rollback();
			$msg = "MSG#Proses Gagal#refresh";
		}else{
			$this->db->trans_commit();
			$msg = "MSG#Proses Berhasil#refresh";
		}
		return $msg;
	}
	
}
?>