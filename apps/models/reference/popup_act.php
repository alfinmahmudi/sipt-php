<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Popup_act extends CI_Model{
	public function list_popup(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
                        $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS_USER' and kode in(00,03) ORDER BY 1", "kode", "uraian");
			$query = "SELECT a.id, a.isi, 
					CASE WHEN a.aktif = '1'
						THEN 'Aktif'
					ELSE 'Non-Aktif'
					END as 'aktif'
					FROM m_popup a";//print_r($query);die();
			$table->title("");
			$table->columns(array("isi", "aktif"));
			$this->newtable->width(array('Nama Pengguna' => 100 , 'Nama Lengkap' => 300, 'Media Komunikasi' => 200, "Akses" => 170, "Status" => 100));
			$this->newtable->search(array(array("a.isi","Isi Berita"),
										  array("a.aktif", 'Status'),
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."reference/view/popup");
			$table->orderby(3);
			$table->sortby("id");
			$table->hiddens(array("id"));
			$table->keys(array("id"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_users");
			$table->menu(array('Tambah' => array('GET', site_url().'reference/add_popup/new', '0', 'home'),
								'Edit' => array('GET', site_url().'reference/add_popup/edit', '1', 'fa fa-edit', 'isngajax'),
							   'Delete' => array('POST', site_url().'reference/del_pop/', 'N', 'fa fa-times','isngajax')
							   ));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Data Pengguna');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	public function get_show(){
		if($this->newsession->userdata('_LOGGED')){
			$sql = "SELECT a.isi from m_popup a WHERE a.aktif = '1'";//print_r($sql);die();
			$arrdata = $this->db->query($sql)->result_array();
 			return $arrdata;
		}
	}

	public function get_popup($act, $id=''){
		if($this->newsession->userdata('_LOGGED')){
			if ($act == 'new') {
				$arrdata['act'] = site_url() . 'post/reference/popup/save';
			}else{
				$sql = "SELECT a.id, a.isi, a.aktif from m_popup a WHERE a.id = '".$id."' ";//print_r($sql);die();
				$datasess = $this->db->query($sql)->row_array();
				$arrdata['sess'] = $datasess;
//                                var_dump($datasess);die();
				$arrdata['act'] = site_url() . 'post/reference/popup/update';
			}
 			return $arrdata;
		}
	}
	
	public function set_popup($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
			}
			$msg = "MSG||NO||Data Gagal Disimpan";
			$respel = FALSE;
			$isi = $this->input->post('pel');
			$stat = $this->input->post('stat');
			$arrinputs = array(
				'isi' => $isi,
				'aktif' => $stat
			);
			if($act == "save"){
				$this->db->trans_begin();
				$this->db->insert('m_popup', $arrinputs);
				if($this->db->affected_rows() > 0){
					$respel = TRUE;
				}
				if($this->db->trans_status() === FALSE || !$respel){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$msg = "MSG||YES||Data berhasil disimpan||REFRESH";
				}
				return $msg;
			}else{
				$id = $this->input->post('id');
				$this->db->trans_begin();
				$this->db->where('id', $id);
				$this->db->update('m_popup', $arrinputs);
				if($this->db->affected_rows() > 0){
					$respel = TRUE;
				}
				if($this->db->trans_status() === FALSE || !$respel){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$msg = "MSG||YES||Data berhasil diupdate||REFRESH";
				}
				return $msg;
			}
		}
	}

	public function delete_pop($id){
		if($this->newsession->userdata('_LOGGED')){
			// $id = hashids_decrypt($id, _HASHIDS_, 9);print_r($id);die();
			$this->db->trans_begin();
			foreach ($id as $data) {
            	$this->db->delete('m_popup', array('id' => $data));
        	}

			if ($this->db->trans_status === FALSE) {
            	$this->db->trans_rollback();
            	$msg = "MSG#Proses Gagal#refresh";
	        } else {
	            $this->db->trans_commit();
            	$msg = "MSG#Proses Berhasil#refresh";
	        }
			return $msg;
		}
	}
}
?>