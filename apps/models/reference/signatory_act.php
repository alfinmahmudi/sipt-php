<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Signatory_act extends CI_Model{
	public function list_signatory(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
			$arrizin = $this->main->array_cb("SELECT nama_izin FROM m_izin WHERE aktif = 1 ORDER BY 1", "nama_izin", "nama_izin");
			$query = "SELECT a.id, a.nama AS 'Nama', a.jabatan AS 'Jabatan', a.nip AS 'NIP',
					  b.nama_izin AS 'Perizinan', 
					  CASE 
					  WHEN a.status = 1 THEN '<i class=\"fa fa-circle text-success\"></i> Aktif'
					  WHEN a.status = 0 THEN '<i class=\"fa fa-circle-o text-mute\"></i> Non Aktif'
					  END AS 'Status'
					  FROM m_ttd a LEFT JOIN m_izin b on a.kd_izin = b.id";
			$table->title("");
			$table->columns(array("a.id", "a.nama", "a.jabatan", "a.nip", "b.nama_izin", "CASE WHEN a.status = 1 THEN '<i class=\"fa fa-circle text-success\"></i> Aktif' WHEN a.status = 0 THEN '<i class=\"fa fa-circle-o text-mute\"></i> Non Aktif' END"));
			$this->newtable->width(array('Nama' => 150 , 'Jabatan' => 200, 'NIP' => 100, "Perizinan" => 250, "Status" => 70));
			$this->newtable->search(array(array("a.nama","Nama Pejabat"),
										  array("a.jabatan","Jabatan"),
										  array("a.nip","NIP"),
										  array("b.nama_izin","Perizinan",array('ARRAY', $arrizin))));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."reference/view/signatory");
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("id"));
			$table->hiddens(array("id"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(TRUE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_signatory_".rand());
			$table->menu(array('Tambah' => array('GET', site_url().'reference/signatory', '0', 'home', 'modal'),
							   'Edit' => array('BOOTSTRAPDIALOG', site_url().'reference/signatory/', '1', 'fa fa-edit')));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Data Pejabat Penanda Tangan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}
	
	public function get_signatory($id){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			$arrdata = array();
			if($id == ""){
				$arrdata['act'] = site_url() . 'post/reference/signatory_ac/save';
			}else{
				$id = hashids_decrypt($id, _HASHIDS_, 9);
				$arrdata['act'] = site_url() . 'post/reference/signatory_ac/update';
				$query = "SELECT a.id, a.nama, a.jabatan, a.nip, a.kd_izin, a.status, b.direktorat_id
						FROM m_ttd a
						LEFT JOIN m_izin b on b.id = a.kd_izin WHERE a.id = '".$id."'";
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['sess'] = $row;
					}
					$arrdata['kd_izin'] = $this->main->set_combobox("SELECT id, nama_izin FROM m_izin WHERE aktif = 1","id","nama_izin", TRUE);
				}
			}
			$arrdata['direktorat'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT'","kode","uraian", TRUE);
			return $arrdata;
		}
	}
	
	public function set_signatory($act, $isajax){
//            print_r($act);die();
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			if($act == "save"){
				if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
				}
				//print_r($_POST);die();
				$ret = "MSG||NO||Data penanda tangan gagal disimpan";
				$resttd = FALSE;
				$arrttd = $this->main->post_to_query($this->input->post('SIGNATORY'));
//                                print_r($arrttd);die();
				if(!$arrttd['status']) $arrttd['status'] = 0;
				$arrttd['created'] = 'GETDATE()';
				if($arrttd['status'] == 1){
					$query_cek = "SELECT a.nama, a.jabatan, a.nip, b.nama_izin
								  FROM m_ttd a
								  LEFT JOIN m_izin b ON b.id = a.kd_izin
								  WHERE a.status = 1 AND a.kd_izin =".$arrttd['kd_izin'];
					$data = $this->main->get_result($query_cek);
					if($data){
						foreach($query_cek->result_array() as $row){
							$pejabat = $row;
						}
						$ret = "MSG||NO||Pejabat Penandatangan untuk perizinan ".$pejabat['nama_izin']." dengan Nama : ".$pejabat['nama']." dan Jabatan : ".$pejabat['jabatan']." masih berstatus aktif. Nonaktifkan terlebih dahulu jika ingin menambah pejabat penandatangan aktif.";
					}else{
						$this->db->trans_begin();
						$this->db->insert('m_ttd', $arrttd);
						if($this->db->affected_rows() > 0){
							$resttd = TRUE;
						}
					}
				}else{
					$this->db->trans_begin();
					$this->db->insert('m_ttd', $arrttd);
					if($this->db->affected_rows() > 0){
						$resttd = TRUE;
					}
				}
				if($this->db->trans_status() === FALSE || !$resttd){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$ret = "MSG||YES||Data penanda tangan berhasil disimpan||REFRESH";
				}
			}
			else if($act == "update"){
				if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
				}
				$ret = "MSG||NO||Data penanda tangan gagal disimpan";
				$resttd = FALSE;
				$arrttd = $this->main->post_to_query($this->input->post('SIGNATORY'));
				$idx = hashids_decrypt($arrttd['id'], _HASHIDS_, 9);
				unset($arrttd['id']);
				if(!$arrttd['status']) $arrttd['status'] = 0;
				if($arrttd['status'] == 1){
					$query_cek = "SELECT a.nama, a.jabatan, a.nip, b.nama_izin
								  FROM m_ttd a
								  LEFT JOIN m_izin b ON b.id = a.kd_izin
								  WHERE a.status = 1 AND a.kd_izin =".$arrttd['kd_izin'];
					$data = $this->main->get_result($query_cek);
					if($data){
						foreach($query_cek->result_array() as $row){
							$pejabat = $row;
						}
						$ret = "MSG||NO||Pejabat Penandatangan untuk perizinan ".$pejabat['nama_izin']." dengan Nama : ".$pejabat['nama']." dan Jabatan : ".$pejabat['jabatan']." masih berstatus aktif. Nonaktifkan terlebih dahulu jika ingin menambah pejabat penandatangan aktif.";
					}else{
						$this->db->trans_begin();
						$this->db->where('id', $idx);
						$this->db->update('m_ttd', $arrttd);
						if($this->db->affected_rows() > 0){
							$resttd = TRUE;
						}
					}
				}else{
					$this->db->trans_begin();
					$this->db->where('id', $idx);
					$this->db->update('m_ttd', $arrttd);
					if($this->db->affected_rows() > 0){
						$resttd = TRUE;
					}
				}
				if($this->db->trans_status() === FALSE || !$resttd){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$ret = "MSG||YES||Data penanda tangan berhasil disimpan||REFRESH";
				}
			}
			return $ret;
		}
	}
	
}
?>