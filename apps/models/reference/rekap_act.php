<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rekap_act extends CI_Model {

    public function rekap() {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata['set_status'] = '';
            $arrdata['selected'] = '';
            $sql = "SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT'";
            $arrdata['direktorat'] = $this->main->set_combobox($sql, "kode", "uraian", TRUE);
            if ($_SESSION['role'] == '98') {
                $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $this->newsession->userdata('kdprop') . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id_prov = '" . $this->newsession->userdata('kdprop') . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['status'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff where jenis='STATUS' and kode = '1000'", "kode", "uraian", TRUE);
                $arrdata['set_status'] = '1000';
                $arrdata['selected'] = 'selected';
            } else {
                $arrdata['status'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff where jenis='STATUS'", "kode", "uraian", TRUE);
                $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            }
            // $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab ORDER BY 2","id", "nama", TRUE);
            // $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec ORDER BY 2","id", "nama", TRUE);
            // $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel ORDER BY 2","id", "nama", TRUE);
            $m = $this->main->get_result($sql);
            if ($m) {
                foreach ($sql->result_array() as $r) {
                    $dir[] = $r;
                }
            }
            //print_r($dir);die();
            $f = "select * from m_izin";
            $v = $this->db->query($f);
            $banyakizin = $v->num_rows();
            for ($x = 0; $x <= $banyakizin; $x++) {
                if ($_SESSION['kode_izin'][$x] != '') {
                    $id .= "'" . $_SESSION['kode_izin'][$x] . "',";
                }
            }
            $id .= "''";

            $i = 0;
            foreach ($dir as $b) {
                $q = "select a.id,a.nama_izin, b.uraian from m_izin a left JOIN m_reff b on a.direktorat_id= b.kode where a.direktorat_id='" . $b['kode'] . "' AND jenis='DIREKTORAT' AND a.aktif = 1 AND id IN(" . $id . ")";
                $j = $this->main->get_result($q);
                if ($j) {
                    foreach ($q->result_array() as $w) {
                        $n[$b['kode']][] = $w;
                    }
                }
                $i++;
            }
            $arrdata['dir'] = $dir;
            $arrdata['izin'] = $n;
            $arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian");
            return $arrdata;
        }
    }

    public function get_bapok($izin, $tg_awal, $tg_akhir, $status, $kdprop, $kdkab, $kdkec, $kdkel) {
        if ($this->newsession->userdata('_LOGGED')) {
            $kd_izin = explode(".", $izin);
            if (trim($kdprop) != '' and $kdprop != null and $kdprop != 'null') {
                $whr_prop = 'AND a.kdprop = ' . $kdprop;
            }
            if (trim($kdkab) != '' and $kdkab != null and $kdkab != 'null') {
                $whr_kab = 'AND a.kdkab = ' . $kdkab;
            }
            if (trim($kdkec) != '' and $kdkec != null and $kdkec != 'null') {
                $whr_kec = 'AND a.kdkec = ' . $kdkec;
            }
            if (trim($kdkel) != '' and $kdkel != null and $kdkel != 'null') {
                $whr_kec = 'AND a.kdkel = ' . $kdkel;
            }
            if ($_SESSION['role'] == '98') {
                $status = '1000';
                $whr_prop = "AND a.kdprop = '" . $_SESSION['kdprop'] . "'";
            }
            if ($status == '1000') {
                $tgl = 'tgl_izin';
            } else {
                $tgl = 'tgl_aju';
            }
            /* $get_id = "SELECT a.no_aju, a.pemasaran, a.pemasaran_prop, a.pemasaran_kab
              FROM t_bapok a
              where status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND tgl_aju BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
              AND trader_id <> '140' " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
              // jangan lupa status  = "AND bp.fl_kirim = '0100'"
              $permohonan_id = $this->db->query($get_id)->result_array();
              $temp = array();

              foreach ($permohonan_id as $say) {
              $jenis = explode(',', $say['pemasaran']);
              if ($jenis[0] == '01') {
              $temp[] = 'Seluruh Indonesia';
              }else{
              if (trim($say['pemasaran_prop']) != ''){
              $temp_prop = explode(",", $say['pemasaran_prop']);
              //$hsl_prop[] = '';

              foreach ($temp_prop as $prop) {
              if (trim($prop) != '') {
              $query_prop = "SELECT a.nama FROM m_prop a where a.id = '".trim($prop)."'";
              $temp_hsl_prop = $this->db->query($query_prop)->row_array();
              $hsl_prop[] = $temp_hsl_prop['nama'];
              }
              }


              }

              if (trim($say['pemasaran_kab']) != '') {
              $temp_kab = explode(",", $say['pemasaran_kab']);
              //$hsl_kab[] = '';

              foreach ($temp_kab as $kab) {
              if (trim($kab) != '') {
              $query_kab = "SELECT a.nama FROM m_kab a where a.id = '".trim($kab)."'";
              $temp_hsl_kab = $this->db->query($query_kab)->row_array();
              $hsl_kab[] = $temp_hsl_kab['nama'];
              }
              }
              }
              if (count($hsl_kab) == 0 && count($hsl_prop) != 0) {
              $temp[] = implode(",", $hsl_prop);
              }elseif (count($hsl_kab) != 0 && count($hsl_prop) == 0) {
              $temp[] = implode(",", $hsl_kab);
              }else{
              $result = array(implode(",", $hsl_kab), implode(",", $hsl_prop));
              $temp[] = implode(",", $result);
              }
              }
              } */
            //print_r($temp);die();

            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Jenis_Izin', 'Tipe_Permohonan', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Email_Perusahaan', 'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ',
                'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ', 'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ',
                'Fax_PJ', 'Urai_Barang', 'beras', 'kedelai', 'cabe', 'bawang_merah', 'gula', 'minyak_goreng', 'terigu', 'daging_sapi', 'daging_ayam', 'telur', 'bawang_putih', 'pemasaran', 'pemasaran_kab', 'pemasaran_prop');
            $urai = 'Urai_Barang';
            if ($_SESSION['role'] == '98') {
                $header = array('No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Jenis_Izin', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Email_Perusahaan', 'Nama_PJ', 'Komoditi');
                $urai = 'Komoditi';
            }
            $sblm = "SET ANSI_NULLS ON
                    SET ANSI_PADDING ON
                    SET ANSI_WARNINGS ON
                    SET ARITHABORT ON
                    SET CONCAT_NULL_YIELDS_NULL ON
                    SET QUOTED_IDENTIFIER ON
                    SET NUMERIC_ROUNDABORT OFF";
            $this->db->query($sblm);

            $query = "SELECT a.no_aju AS 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) AS 'Tanggal_Pendaftaran', a.no_izin AS 'No_izin', 
            dbo.dateIndo(a.tgl_izin) AS 'Tanggal_Izin', dbo.dateIndo(a.tgl_izin_exp) AS 'Tanggal_Expired', dis.uraian as 'Jenis_Izin', 
            c.uraian AS 'Tipe_Permohonan',
            dbo.npwp(a.npwp) AS 'NPWP', d.uraian AS 'Bentuk_Usaha', a.nm_perusahaan 'Nama_Perusahaan', a.almt_perusahaan AS 'Alamat_Perusahaan', 
            y.nama AS 'Propinsi_Perusahaan', l.nama AS 'Kabupaten_Perusahaan', m.nama AS 'Kecamatan_Perusahaan', n.nama AS 'Kelurahan_Perusahaan', 
            a.kdpos AS 'Kodepos_Perusahaan', a.telp AS 'Telepon_Perusahaan', a.fax AS 'Fax_Perusahaan', mt.email as 'Email_Perusahaan',
            i.uraian AS 'Jenis_Identitas_PJ', 
            a.noidentitas_pj AS 'No_Idenitas_PJ', a.nama_pj AS 'Nama_PJ', z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj AS 'Tempat_Lahir_PJ', 
            dbo.dateIndo(a.tgl_lahir_pj) AS 'Tanggal_Lahir_PJ', a.alamat_pj AS 'Alamat_PJ', 
            pja.nama AS 'Propinsi_PJ', pjb.nama AS 'Kabupaten_PJ', pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ', 
            a.fax_pj AS 'Fax_PJ',   
             STUFF((
                    SELECT ', ' +  convert(varchar(30),jns.uraian) 
                    FROM dbo.t_bapok_brg bpk  
            LEFT JOIN m_jenis_bapok jns ON jns.id = bpk.jns_brg where bpk.permohonan_id =  a.id 
            FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'), 1, 2, '')  as '" . $urai . "',
            (select count(*) from t_bapok_brg where permohonan_id = a.id and jns_brg = 1 ) as beras,
(select count(*) from t_bapok_brg where permohonan_id = a.id and jns_brg = 2 ) as kedelai,
(select count(*) from t_bapok_brg where permohonan_id = a.id and jns_brg = 3 ) as cabe,
(select count(*) from t_bapok_brg where permohonan_id = a.id and jns_brg = 4 ) as bawang_merah,
(select count(*) from t_bapok_brg where permohonan_id = a.id and jns_brg = 5 ) as gula,
(select count(*) from t_bapok_brg where permohonan_id = a.id and jns_brg = 6 ) as minyak_goreng,
(select count(*) from t_bapok_brg where permohonan_id = a.id and jns_brg = 7 ) as terigu,
(select count(*) from t_bapok_brg where permohonan_id = a.id and jns_brg = 8 ) as daging_sapi,
(select count(*) from t_bapok_brg where permohonan_id = a.id and jns_brg = 9 ) as daging_ayam,
(select count(*) from t_bapok_brg where permohonan_id = a.id and jns_brg = 10 ) as telur,
(select count(*) from t_bapok_brg where permohonan_id = a.id and jns_brg = 11 ) as bawang_putih,
a.pemasaran, a.pemasaran_kab, a.pemasaran_prop
            FROM t_bapok a
            LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN'
            LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
            LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
            LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
            LEFT JOIN m_prop Y ON y.id = a.kdprop
            LEFT JOIN m_kab l ON l.id = a.kdkab
            LEFT JOIN m_kec m ON m.id = a.kdkec
            LEFT JOIN m_kel n ON n.id = a.kdkel
            LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
            LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
            LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
            LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
            LEFT JOIN m_trader mt ON mt.id = a.trader_id
            LEFT JOIN m_reff dis ON dis.kode = a.jenis_izin AND dis.jenis = 'JENIS_DISTRIBUTOR_BAPOK'
            where a.status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            AND trader_id <> '140' " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . ""; //print_r($query);die();  
            $data = $this->db->query($query);
            $data = $this->main->get_result($query);
            if ($data) {
                foreach ($query->result_array() as $r => $t) {
                    $detil[] = $t;
                }
            }
            //print_r($detil);die();
            $arrdata['header'] = $header;
            $arrdata['detil'] = $detil;
            $arrdata['brg'] = $temp;
            //print_r($arrdata);die();
            return $arrdata;
        }
    }

    public function rekap_bapok() {
        if ($this->newsession->userdata('_LOGGED')) {

            $arrdata['jns_brg'] = $this->main->set_combobox("SELECT a.id, a.uraian FROM m_jenis_bapok a", "id", "uraian", TRUE);
            if ($_SESSION['role'] == '98') {
                $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $_SESSION['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id_prov  = '" . $_SESSION['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
            } else {
                $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            }
            // $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab ORDER BY 2","id", "nama", TRUE);
            // $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec ORDER BY 2","id", "nama", TRUE);
            // $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel ORDER BY 2","id", "nama", TRUE);
            $bln = '';
            $tahun = '';
            for ($i = 0; $i <= 12; $i++) {
                if ($i == 0) {
                    $bln[] = '&nbsp;';
                } else {
                    if ($i <= 9 && $i > 0) {
                        $get_urai = "select dbo.get_bulan('0" . $i . "') as 'bln'";
                    } else {
                        $get_urai = "select dbo.get_bulan('" . $i . "') as 'bln'";
                    }
                    $urai_bulan = $this->db->query($get_urai)->row_array();
                    $bln[] = $urai_bulan['bln'];
                }
            }
            $plus = (int) date('Y') + 5;
            for ($i = (int) date('Y') - 1; $i <= $plus; $i++) {
                if ($i == date('Y') - 1) {
                    $tahun[] = '&nbsp;';
                    $tahun[date('Y') - 1] = date('Y') - 1;
                } else {
                    $tahun[$i] = $i;
                }
            }
            $arrdata['bulan'] = $bln;
            $arrdata['tahun'] = $tahun;
            $arrdata['dir'] = $dir;
            $arrdata['izin'] = $n;
            $arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian");
            return $arrdata;
        }
    }

    public function rekap_bapok_prshn() {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata['prshn'] = '1';
            $arrdata['jns_brg'] = $this->main->set_combobox("SELECT a.id, a.uraian FROM m_jenis_bapok a", "id", "uraian", TRUE);
            if ($_SESSION['role'] == '98') {
                $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $_SESSION['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id_prov  = '" . $_SESSION['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
            } else {
                $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            }
            // $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab ORDER BY 2","id", "nama", TRUE);
            // $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec ORDER BY 2","id", "nama", TRUE);
            // $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel ORDER BY 2","id", "nama", TRUE);
            $bln = '';
            $tahun = '';
            for ($i = 0; $i <= 12; $i++) {
                if ($i == 0) {
                    $bln[] = '&nbsp;';
                } else {
                    if ($i <= 9 && $i > 0) {
                        $get_urai = "select dbo.get_bulan('0" . $i . "') as 'bln'";
                    } else {
                        $get_urai = "select dbo.get_bulan('" . $i . "') as 'bln'";
                    }
                    $urai_bulan = $this->db->query($get_urai)->row_array();
                    $bln[] = $urai_bulan['bln'];
                }
            }
            $plus = (int) date('Y') + 5;
            for ($i = (int) date('Y') - 1; $i <= $plus; $i++) {
                if ($i == date('Y') - 1) {
                    $tahun[] = '&nbsp;';
                    $tahun[date('Y') - 1] = date('Y') - 1;
                } else {
                    $tahun[$i] = $i;
                }
            }
            $arrdata['bulan'] = $bln;
            $arrdata['tahun'] = $tahun;
            $arrdata['dir'] = $dir;
            $arrdata['izin'] = $n;
            $arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian");
            return $arrdata;
        }
    }

    public function rekap_bapokstra() {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata['jenis_bapok'] = '1';
            $arrdata['jns_brg'] = $this->main->set_combobox("SELECT a.id, a.uraian FROM m_jenis_bapok a", "id", "uraian", TRUE);
            if ($_SESSION['role'] == '98') {
                $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop where id = '" . $_SESSION['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab where id_prov  = '" . $_SESSION['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
            } else {
                $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            }
            // $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab ORDER BY 2","id", "nama", TRUE);
            // $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec ORDER BY 2","id", "nama", TRUE);
            // $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel ORDER BY 2","id", "nama", TRUE);
            $bln = '';
            $tahun = '';
            for ($i = 0; $i <= 12; $i++) {
                if ($i == 0) {
                    $bln[] = '&nbsp;';
                } else {
                    if ($i <= 9) {
                        $get_urai = "select dbo.get_bulan('0" . $i . "') as 'bln'";
                    } else {
                        $get_urai = "select dbo.get_bulan('" . $i . "') as 'bln'";
                    }
                    $urai_bulan = $this->db->query($get_urai)->row_array();
//                    
                    $bln[] = $urai_bulan['bln'];
                }
            }
//            print_r($bln);die();
            $tahun[] = ' ';
            $plus = 2017;
            for ($i = (int) date('Y'); $i >= $plus; $i--) {
                if ($i == date('Y')) {
                    $tahun[$i] = (int) date('Y');
                } else {
                    $tahun[$i] = $i;
                }
            }
//            print_r($tahun);die();
            $arrdata['bulan'] = $bln;
            $arrdata['tahun'] = $tahun;
            $arrdata['dir'] = $dir;
            $arrdata['izin'] = $n;
            $arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian");
            return $arrdata;
        }
    }

    public function rekap_antarpulau() {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata['antarpulau'] = '1';
            $arrdata['jns_brg'] = $this->main->set_combobox("SELECT a.id, a.uraian FROM m_jenis_bapok a", "id", "uraian", TRUE);
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            $arrdata['pel_muat'] = $this->main->set_combobox("SELECT a.kode, a.uraian FROM m_peldn a where tipe in('1','3')", "kode", "uraian", TRUE);
            $arrdata['pel_bongkar'] = $this->main->set_combobox("SELECT a.kode, a.uraian FROM m_peldn a where tipe in('1','3')", "kode", "uraian", TRUE);
            // $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab ORDER BY 2","id", "nama", TRUE);
            // $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec ORDER BY 2","id", "nama", TRUE);
            // $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel ORDER BY 2","id", "nama", TRUE);
            $bln = '';
            $tahun = '';
            for ($i = 0; $i <= 12; $i++) {
                if ($i == 0) {
                    $bln[] = '&nbsp;';
                } else {
                    if ($i <= 9) {
                        $get_urai = "select dbo.get_bulan('0" . $i . "') as 'bln'";
                    } else {
                        $get_urai = "select dbo.get_bulan('" . $i . "') as 'bln'";
                    }
                    $urai_bulan = $this->db->query($get_urai)->row_array();
                    $bln[] = $urai_bulan['bln'];
                }
            }
            $tahun[] = ' ';
            $plus = (int) date('Y') - 10;
            for ($i = (int) date('Y'); $i >= $plus; $i--) {
                if ($i == date('Y')) {
                    $tahun[$i] = (int) date('Y');
                } else {
                    $tahun[$i] = $i;
                }
            }
//            print_r($tahun);die();
            $arrdata['bulan'] = $bln;
            $arrdata['tahun'] = $tahun;
            $arrdata['dir'] = $dir;
            $arrdata['izin'] = $n;
            $arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian");
            return $arrdata;
        }
    }
    
    public function rekap_label() {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata['antarpulau'] = '1';
            $arrdata['jns_brg'] = $this->main->set_combobox("SELECT a.id, a.uraian FROM m_jenis_bapok a", "id", "uraian", TRUE);
            $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
            $arrdata['beras'] = $this->main->set_combobox("SELECT a.id, a.uraian FROM m_beras a", "id", "uraian", TRUE);
            // $arrdata['kabupaten'] = $this->main->set_combobox("SELECT id, nama FROM m_kab ORDER BY 2","id", "nama", TRUE);
            // $arrdata['kecamatan'] = $this->main->set_combobox("SELECT id, nama FROM m_kec ORDER BY 2","id", "nama", TRUE);
            // $arrdata['kelurahan'] = $this->main->set_combobox("SELECT id, nama FROM m_kel ORDER BY 2","id", "nama", TRUE);
            $bln = '';
            $tahun = '';
            for ($i = 0; $i <= 12; $i++) {
                if ($i == 0) {
                    $bln[] = '&nbsp;';
                } else {
                    if ($i <= 9) {
                        $get_urai = "select dbo.get_bulan('0" . $i . "') as 'bln'";
                    } else {
                        $get_urai = "select dbo.get_bulan('" . $i . "') as 'bln'";
                    }
                    $urai_bulan = $this->db->query($get_urai)->row_array();
                    $bln[] = $urai_bulan['bln'];
                }
            }
            $tahun[] = ' ';
            $plus = (int) date('Y') - 10;
            for ($i = (int) date('Y'); $i >= $plus; $i--) {
                if ($i == date('Y')) {
                    $tahun[$i] = (int) date('Y');
                } else {
                    $tahun[$i] = $i;
                }
            }
//            print_r($tahun);die();
            $arrdata['bulan'] = $bln;
            $arrdata['tahun'] = $tahun;
            $arrdata['dir'] = $dir;
            $arrdata['izin'] = $n;
            $arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian");
            return $arrdata;
        }
    }

    function excel_bpk($isajax, $jns_brg, $kdprop, $kdkab, $kdkec, $kdkel, $bulan, $tahun) {
//        die($bulan);
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            if ($kdprop != 'null') {
                $whr_prop = 'AND kdprop = ' . $kdprop;
            }
            if ($kdkab != 'null') {
                $whr_kab = 'AND kdkab = ' . $kdkab;
            }
            if ($kdkec != 'null') {
                $whr_kec = 'AND kdkec = ' . $kdkec;
            }
            if ($kdkel != 'null') {
                $whr_kel = 'AND kdkel = ' . $kdkel;
            }
            if ($bulan != '0') {
                $whr_bulan = 'AND bulan = ' . $bulan;
            }
            if ($tahun != '0') {
                $whr_tahun = 'AND tahun = ' . $tahun;
            }
            if ($_SESSION['role'] == '98') {
                $whr_prop = "AND kdprop = '" . $_SESSION['kdprop'] . "'";
            }

            $get_id = "SELECT a.permohonan_id, a.id
                    FROM t_lap_bapok a left join t_bapok b on b.id = a.permohonan_id
                    WHERE a.bapok = '" . $jns_brg . "' " . $whr_bulan . " " . $whr_tahun . " " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "
                    GROUP BY a.permohonan_id, a.id"; //die($get_id);
            // jangan lupa status  = "AND bp.fl_kirim = '0100'"
            $permohonan_id = $this->db->query($get_id)->result_array();
            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Tipe_Permohonan', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ', 'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ', 'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ', 'Fax_PJ', 'Bulan', 'Kode_Bulan', 'Tahun', 'Stok_Awal', 'Stok_Akhir', 'Pengadaan', 'Penyaluran');
            $header_ex = array('Tipe', 'Jumlah', 'Satuan', 'Daerah');

            foreach ($permohonan_id as $id_per) {

                // //                $get_det = "SELECT b.jumlah 'Jumlah', c.uraian 'Tipe', b.satuan 'Satuan',
                // // 			CASE
                // // 			  WHEN LEN(b.daerah) = 2 THEN d.nama
                // // 			  ELSE e.nama
                // // 			END AS 'Daerah'
                // // 			FROM t_lap_bapok a 
                // // 			LEFT JOIN t_lap_bapok_dtl b ON b.lap_id = a.id
                // // 			LEFT JOIN m_reff c ON c.kode = b.tipe AND c.jenis = 'JNS_PEL'
                // // 			LEFT JOIN m_prop d ON d.id = b.daerah
                // // 			LEFT JOIN m_kab e ON e.id = b.daerah
                // // 			WHERE a.permohonan_id = '".$id_per['permohonan_id']."' AND a.bapok = '".$jns_brg."'";
                // // $detil_ex[] = $this->db->query($get_det)->result_array();
                // // $ok = array(
                // // 	'master' => $detil,
                // // 	'detil' => $real
                // // 	);
                //            }
                if ($kdprop != 'null') {
                    $whr_prop = 'AND kdprop = ' . $kdprop;
                }
                if ($kdkab != 'null') {
                    $whr_kab = 'AND kdkab = ' . $kdkab;
                }
                if ($kdkec != 'null') {
                    $whr_kec = 'AND kdkec = ' . $kdkec;
                }
                if ($kdkel != 'null') {
                    $whr_kel = 'AND kdkel = ' . $kdkel;
                }
                if ($bulan != '0') {
                    $whr_bulan = 'AND bulan = ' . $bulan;
                }
                if ($tahun != '0') {
                    $whr_tahun = 'AND tahun = ' . $tahun;
                }
                $sql = "SELECT a.no_aju AS 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) AS 'Tanggal_Pendaftaran', a.no_izin AS 'No_izin', 
                    		dbo.dateIndo(a.tgl_izin) AS 'Tanggal_Izin', d.uraian AS 'Bentuk_Usaha',
                            dbo.dateIndo(a.tgl_izin_exp) AS 'Tanggal_Expired', c.uraian AS 'Tipe_Permohonan', 
                            a.npwp AS 'NPWP',  
                            a.nm_perusahaan 'Nama_Perusahaan', CAST(a.almt_perusahaan AS varchar(100)) AS 'Alamat_Perusahaan', 
                            y.nama AS 'Propinsi_Perusahaan', 
                            l.nama AS 'Kabupaten_Perusahaan', m.nama AS 'Kecamatan_Perusahaan', n.nama AS 'Kelurahan_Perusahaan', 
                            a.kdpos AS 'Kodepos_Perusahaan', a.telp AS 'Telepon_Perusahaan', a.fax AS 'Fax_Perusahaan', i.uraian AS 'Jenis_Identitas_PJ', 
                            a.noidentitas_pj AS 'No_Idenitas_PJ', a.nama_pj AS 'Nama_PJ', z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj AS 'Tempat_Lahir_PJ', 
                            dbo.dateIndo(a.tgl_lahir_pj) AS 'Tanggal_Lahir_PJ', CAST(a.alamat_pj AS varchar(100)) AS 'Alamat_PJ', pja.nama AS 'Propinsi_PJ', 
                            pjb.nama AS 'Kabupaten_PJ', pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ', 
                            a.fax_pj AS 'Fax_PJ', dbo.get_bulan(MAX(bp.bulan)) as 'Bulan', MAX(bp.bulan) as 'Kode_Bulan', MAX(bp.tahun) as 'Tahun', 
                            bp.stok_awl as 'Stok_Awal', bp.stok_akhr as 'Stok_Akhir', bp.pengadaan as 'Pengadaan', bp.penyaluran as 'Penyaluran'
                            FROM t_bapok a
                            LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN'
                            LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
                            LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
                            LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
                            LEFT JOIN m_prop Y ON y.id = a.kdprop
                            LEFT JOIN m_kab l ON l.id = a.kdkab
                            LEFT JOIN m_kec m ON m.id = a.kdkec
                            LEFT JOIN m_kel n ON n.id = a.kdkel
                            LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
                            LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
                            LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
                            LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
                            LEFT JOIN t_lap_bapok bp ON bp.permohonan_id = a.id
                            WHERE bp.bapok = '" . $jns_brg . "' AND a.id = '" . $id_per['permohonan_id'] . "' AND trader_id <> '140' " . $whr_prop . " " . $whr_kab . "
                            " . $whr_kec . " " . $whr_kel . "" . $whr_bulan . " " . $whr_tahun . "
                            GROUP BY bp.permohonan_id, bp.stok_awl, bp.stok_akhr, bp.pengadaan, bp.penyaluran, a.no_aju, a.tgl_aju, a.no_izin, a.tgl_izin, 
                            a.tgl_izin_exp, c.uraian, a.npwp, d.uraian, a.nm_perusahaan, CAST(a.almt_perusahaan AS varchar(100)), y.nama, l.nama, m.nama, n.nama, 
                            a.kdpos, a.telp, a.fax, i.uraian, a.noidentitas_pj, a.nama_pj, z.uraian, a.tmpt_lahir_pj, a.tgl_lahir_pj, 
                            CAST(a.alamat_pj AS varchar(100)), pja.nama, pjb.nama, pjc.nama, pjd.nama, a.telp_pj, a.fax_pj";
                // jangan lupa status  = "AND bp.fl_kirim = '0100'"
//                die($sql);
                $data_det = $this->db->query($sql)->row_array();

                $get = "SELECT a.jumlah as 'Jumlah', a.satuan as 'Satuan',
                            CASE WHEN a.tipe = '1'
                            THEN 'Pemasukan'
                                ELSE 'Penyaluran'
                            END as Tipe,
                            CASE WHEN LEN(a.negara) <> '0'
                            THEN d.nama
                                WHEN LEN(a.daerah) = '2'
                            THEN b.nama
                                ELSE c.nama
                            END as Daerah
                            FROM t_lap_bapok_dtl a
                            LEFT JOIN m_prop b ON b.id = a.daerah
                            LEFT JOIN m_kab c ON c.id = a.daerah
                            LEFT JOIN m_negara d ON d.kode = a.negara
                            WHERE a.lap_id = '" . $id_per['id'] . "'
                            GROUP BY a.tipe, a.lap_id, a.id, a.jumlah, a.negara, 
                            d.nama, a.daerah, c.nama, b.nama, a.satuan ";
                $det = $this->db->query($get)->result_array();
                $detil[] = array('det' => $det,
                    'data' => $data_det);
                // print_r($detil);die();
            }

            //print_r($detil);die();
            $arrdata['header'] = $header;
            $arrdata['detil'] = $detil;
            $arrdata['header_ex'] = $header_ex;
            // $arrdata['detil_ex'] = $detil_ex;
            //print_r($arrdata);die();
            return $arrdata;
        }
    }

    function excel_bpkstra($isajax, $kdprop, $kdkab, $bulan, $tahun, $bulan_akhir, $jns_brg) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }

            if ($bulan != '0') {
                $awal = $tahun . "-" . $bulan . "-01";
                $akhir = $tahun . "-" . $bulan_akhir . "-01";
                if ($_SESSION['role'] == '98') {
                    $whr_bulan = 'AND bulan = ' . $bulan;
                }
            }
            if ($tahun != '0') {
                $whr_tahun = 'AND tahun = ' . $tahun;
            }
            if ($kdprop != 'null') {
                $whr_prop = 'AND kdprop = ' . $kdprop;
            }
            if ($kdkab != 'null') {
                $whr_kab = 'AND kdkab = ' . $kdkab;
            }
            $timeStart = strtotime($awal);
            $timeEnd = strtotime($akhir);
// Menambah bulan ini + semua bulan pada tahun sebelumnya
            $numBulan = 1 + (date("Y", $timeEnd) - date("Y", $timeStart)) * 12;
// menghitung selisih bulan
            $numBulan += date("m", $timeEnd) - date("m", $timeStart);
            if ($_SESSION['role'] == '98') {
                $whr_prop = "AND a.kdprop = '" . $_SESSION['kdprop'] . "'";
            }
            if ($_SESSION['role'] != '98') {
                $header = array("Propinsi");
            } else {
                $header = array("JENIS BARANG", "KOMODITI", "JUMLAH", "SATUAN", "PROPINSI");
            }
            if ($_SESSION['role'] != '98') {
                $sql_add = "";
                $j = 1;
                $bln = array("&nbsp;", "Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                for ($i = 1; $i <= $numBulan; $i++) {
                    $sql_add .= ",(SELECT SUM(b.stok_akhr)
FROM t_lap_bapok b
LEFT JOIN t_bapok c ON c.id = b.permohonan_id where c.kdprop = a.id and b.bulan = '" . $bulan . "' and b.tahun = '" . $tahun . "' and b.bapok = '" . $jns_brg . "') AS '" . $bln[$bulan] . "'";
                    $header[$j] = $bln[$bulan];
                    $j++;
                    $bulan++;
                }
            }
            $sql = "SELECT a.nama as Propinsi " . $sql_add . "
FROM m_prop a
GROUP BY a.id,a.nama order by a.id";
            $header_ex = array('Tipe', 'Jumlah', 'Satuan', 'Daerah');
            if ($_SESSION['role'] == '98') {
                $sql = "select y.uraian as 'JENIS BARANG', g.uraian as 'KOMODITI', z.jumlah as 'JUMLAH', g.urai_satuan as 'SATUAN', z.nama as 'PROPINSI'
                from m_jenis_bapok g 
                left join (
                select sum(b.stok_akhr) as jumlah , a.kdprop, b.bapok, c.nama
                from   t_lap_bapok b 
                        left join t_bapok a   on b.permohonan_id = a.id
                        left join m_prop c on a.kdprop = c.id
                where  a.status = '1000' and b.bapok is not null " . $whr_tahun . " " . $whr_bulan . " " . $whr_prop . " " . $whr_kab . "  
                group by  a.kdprop, b.bapok, c.nama  )  z on  z.bapok = g.id
                left join m_reff y on g.jenis_bapok = y.kode and y.jenis = 'JENIS_BAPOK' order by g.jenis_bapok, g.id";
            }
            $data_det = $this->db->query($sql)->result_array();
//            die($sql);
            $komoditi = "SELECT id,uraian from m_jenis_bapok where id = '" . $jns_brg . "'";
            $data = $this->db->query($komoditi)->result_array();
        }
        //print_r($a);die();
        $arrdata['header'] = $header;
        $arrdata['detil'] = $data_det;
        $arrdata['datax'] = $data;
        $arrdata['header_ex'] = $header_ex;
        // $arrdata['detil_ex'] = $detil_ex;
        //print_r($arrdata);die();
        return $arrdata;
    }

    function excel_antrpl($isajax, $tgl_awal2, $tgl_akhir2, $tgl_awal, $tgl_akhir, $pel_muat, $pel_bongkar) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            if ($tgl_awal != 'null' && $tgl_akhir != 'null') {
                $whr_eta = "AND a.etd BETWEEN '" . $tgl_awal . "' AND '" . $tgl_akhir . "'";
            }
            if ($tgl_awal2 != 'null' && $tgl_akhir2 != 'null') {
                $whr_etd = "AND a.eta BETWEEN '" . $tgl_awal2 . "' AND '" . $tgl_akhir2 . "'";
            }
            if ($pel_muat != 'null') {
                $whr_muat = "AND a.pel_asal = '" . $pel_muat . "'";
            }
            if ($pel_bongkar != 'null') {
                $whr_bongkar = "AND a.pel_tujuan = '" . $pel_bongkar . "'";
            }
            $header = array("no_aju", "created", "jenis_pelapor", "npwp_pengirim", "nama_pengirim", "tp_perusahaan_pengirim", "alamat_pengirim", "prop_pengirim", "kab_pengirim", "kec_pengirim", "kel_pengirim", "telp", "email", "fax", "npwp_penerima", "nama_penerima", "tp_perusanaan_penerima", "alamat_penerima", "prop_penerima", "kab_penerima", "kec_penerima", "kel_penerima", "telp_penerima", "fax_penerima", "email_penerima", "pel_asal", "pel_tujuan", "jenis_asal", "negara_tujuan", "kab_asal", "kab_tujuan", "nama_kapal", "eta", "etd", "jumlah_ekspedisi", "tp_perusaaan_ekspedisi1", "nama_ekspedisi1", "pj_ekspedisi1", "npwp_ekspedisi1", "alamat_ekspedisi1", "prop_ekspedisi1", "kab_ekspedisi1", "kec_ekspedisi1", "kel_ekspedisi1", "telp_ekspedisi1", "email_ekspedisi1", "tp_perusaaan_ekspedisi2", "nama_ekspedisi2", "pj_ekspedisi2", "npwp_ekspedisi2", "alamat_ekspedisi2", "prop_ekspedisi2", "kab_ekspedisi2", "kec_ekspedisi2", "kel_ekspedisi2", "telp_ekspedisi2", "email_ekspedisi2", "HS", "HS_URAI", "HS_JUM");

            $sblm = "SET ANSI_NULLS ON
                        SET ANSI_PADDING ON
                        SET ANSI_WARNINGS ON
                        SET ARITHABORT ON
                        SET CONCAT_NULL_YIELDS_NULL ON
                        SET QUOTED_IDENTIFIER ON
                        SET NUMERIC_ROUNDABORT OFF";
            $this->db->query($sblm);

            $sql = "select a.no_aju as no_aju, a.created as created, b.uraian as jenis_pelapor,
              a.npwp as npwp_pengirim, a.nama as nama_pengirim, c.uraian as tp_perusahaan_pengirim, a.alamat as alamat_pengirim, d.nama as prop_pengirim,
              e.nama as kab_pengirim, f.nama as kec_pengirim, g.nama as kel_pengirim, a.telp as telp, a.email as email, a.fax as fax,
              a.npwp_penerima as npwp_penerima, a.nama_penerima as nama_penerima, h.uraian as tp_perusanaan_penerima, a.alamat_penerima as alamat_penerima,  i.nama as prop_penerima, j.nama as kab_penerima,
              k.nama as kec_penerima, l.nama as kel_penerima, a.telp_penerima as telp_penerima, a.fax_penerima as fax_penerima, a.email_penerima as email_penerima, m.kode + ' - ' +m.uraian as pel_asal,
              n.kode + ' - ' +n.uraian as pel_tujuan, o.uraian as jenis_asal , p.nama as negara_tujuan, q.nama as kab_asal , r.nama as kab_tujuan,
              a.nama_kapal, dbo.dateIndo(a.eta) as eta, dbo.dateIndo(a.etd) as etd , (select COUNT(*) from t_lap_pulau_eks where id_lap = a.id) as jumlah_ekspedisi,
              t.uraian as tp_perusaaan_ekspedisi1, s1.nama as nama_ekspedisi1, s1.nama_pj as pj_ekspedisi1, s1.npwp as npwp_ekspedisi1,
              s1.alamat as alamat_ekspedisi1, u.nama as prop_ekspedisi1, v.nama as kab_ekspedisi1, w.nama as kec_ekspedisi1 , x.nama as kel_ekspedisi1,
              s1.telp as telp_ekspedisi1, s1.email as email_ekspedisi1,
              y.uraian as tp_perusaaan_ekspedisi2, s2.nama as nama_ekspedisi2, s2.nama_pj as pj_ekspedisi2, s2.npwp as npwp_ekspedisi2,
              s2.alamat as alamat_ekspedisi2, z.nama as prop_ekspedisi2, aa.nama as kab_ekspedisi2, ab.nama as kec_ekspedisi2 , ac.nama as kel_ekspedisi2,
              s2.telp as telp_ekspedisi2, s2.email as email_ekspedisi2, STUFF((
              SELECT dt.hs + ' '
              FROM t_lap_pulau_dtl dt
              WHERE dt.id_lap = a.id order by dt.id
              FOR XML PATH(''), TYPE).value('.', 'varchar(MAX)'), 1, 0, '') as HS,
              STUFF((
              SELECT convert(varchar(max),dt.uraian )+ '\n'
              FROM t_lap_pulau_dtl dt
              WHERE dt.id_lap = a.id order by dt.id
              FOR XML PATH(''), TYPE).value('.', 'varchar(MAX)'), 1, 0, '') as HS_URAI,
              STUFF((
              SELECT convert(varchar(max),dt.jumlah)  + ' ' +st.uraian+ ' '
              FROM t_lap_pulau_dtl dt
              left join m_satuan st on dt.satuan = st.satuan
              WHERE dt.id_lap = a.id order by dt.id
              FOR XML PATH(''), TYPE).value('.', 'varchar(MAX)'), 1, 0, '') as HS_JUM
              from t_lap_pulau a
              left join m_reff b on a.jenis_pelapor = b.kode and b.jenis = 'JENIS_PELAPOR'
              left join m_reff c on a.tipe_perushaan = c.kode and c.jenis = 'TIPE_PERUSAHAAN'
              left join m_prop d on a.kdprop = d.id
              left join m_kab e on a.kdkab = e.id
              left join m_kec f on a.kdkec = f.id
              left join m_kel g on a.kdkel = g.id
              left join m_reff h on a.tipe_perushaan_penerima = h.kode and h.jenis = 'TIPE_PERUSAHAAN'
              left join m_prop i on a.kdprop_penerima = i.id
              left join m_kab j on a.kdkab_penerima = j.id
              left join m_kec k on a.kdkec_penerima = k.id
              left join m_kel l on a.kdkel_penerima = l.id
              left join m_peldn m on a.pel_asal = m.kode
              left join m_peldn n on a.pel_tujuan = n.kode
              left join m_reff o on a.jenis_asal = o.kode and o.jenis = 'JENIS_ASAL'
              left join m_negara p on a.negara_tujuan = p.kode
              left join m_kab q on a.kab_asal = q.id
              left join m_kab r on a.kab_tujuan = r.id
              left join t_lap_pulau_eks s1 on s1.id_lap = a.id  and s1.no_urut = 1
              left join t_lap_pulau_eks s2 on s2.id_lap = a.id  and s2.no_urut = 2
              left join m_reff t on s1.tipe_perushaan = t.kode and t.jenis = 'TIPE_PERUSAHAAN'
              left join m_prop u on s1.kdprop = u.id
              left join m_kab v on s1.kdkab = v.id
              left join m_kec w on s1.kdkec = w.id
              left join m_kel x on s1.kdkel = x.id
              left join m_reff y on s2.tipe_perushaan = y.kode and y.jenis = 'TIPE_PERUSAHAAN'
              left join m_prop z on s2.kdprop = z.id
              left join m_kab aa on s2.kdkab = aa.id
              left join m_kec ab on s2.kdkec = ab.id
              left join m_kel ac on s2.kdkel = ac.id
              where DATEDIFF(day,a.etd, getdate()) > 7 " . $whr_eta . " " . $whr_etd . " " . $whr_muat . " " . $whr_bongkar;
            $data_det = $this->db->query($sql)->result_array();
        }
//        print_r($sql);die();
        $arrdata['header'] = $header;
        $arrdata['detil'] = $data_det;
        $arrdata['header_ex'] = $header_ex;
        // $arrdata['detil_ex'] = $detil_ex;
        //print_r($arrdata);die();
        return $arrdata;
    }
    
    function excel_label($isajax, $tgl_awal, $tgl_akhir, $beras, $kdprop, $kdkab) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }
            if ($tgl_awal != 'null' && $tgl_akhir != 'null') {
                $whr_eta = "AND a.tgl_aju BETWEEN '" . $tgl_awal . "' AND '" . $tgl_akhir . "'";
            }
            if ($beras != 'null') {
                $whr_beras = "AND b.kd_jns_beras = '" . $beras . "'";
            }
            if ($kdprop != 'null') {
                $whr_kdprop = "AND a.kdprop = '" . $kdprop . "'";
            }
            if ($kdkab != 'null') {
                $whr_kdkab = "AND a.kdkab = '" . $kdkab . "'";
            }
            $header = array("Merk", "Jenis_Beras", "Jenis_Beras_Khusus", "Ket_Campuran", "Berat_bersih");

            $sblm = "SET ANSI_NULLS ON
                        SET ANSI_PADDING ON
                        SET ANSI_WARNINGS ON
                        SET ARITHABORT ON
                        SET CONCAT_NULL_YIELDS_NULL ON
                        SET QUOTED_IDENTIFIER ON
                        SET NUMERIC_ROUNDABORT OFF";
            $this->db->query($sblm);

            $sql = "select b.merk as Merk, c.uraian as Jenis_Beras, d.uraian as Jenis_Beras_Khusus, b.campuran as Ket_Campuran,
Berat_bersih = STUFF((
          SELECT ', ' + ac.uraian
          FROM t_label_kemasan ab
          left join m_beras_berat ac on ac.id = ab.kd_kemasan
          WHERE ab.merk_id = b.id
          FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'), 1, 1, '')
  from t_label a LEFT JOIN t_label_merek b ON b.permohonan_id = a.id
  LEFT JOIN m_beras c ON c.id = b.kd_jns_beras
  LEFT JOIN m_beras_khusus d ON d.id = b.kd_jns_khusus where a.status = '1000' " . $whr_eta . " " . $whr_beras . " " . $whr_kdprop . " " . $whr_kdkab;//print_r($sql);die();
            $data_det = $this->db->query($sql)->result_array();
        }
        
        $arrdata['header'] = $header;
        $arrdata['detil'] = $data_det;
        $arrdata['header_ex'] = $header_ex;
        // $arrdata['detil_ex'] = $detil_ex;
        //print_r($arrdata);die();
        return $arrdata;
    }

    function excel_bpk_prshn($isajax, $kdprop, $bulan, $tahun, $bulan_akhir, $jns_brg) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (!$isajax) {
                return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
                exit();
            }

            if ($bulan != '0') {
                $awal = $tahun . "-" . $bulan . "-01";
                $akhir = $tahun . "-" . $bulan_akhir . "-01";
            }
            if ($tahun != '0') {
                $whr_tahun = 'AND tahun = ' . $tahun;
            }
            if ($kdprop != 'null') {
                $whr_prop = 'AND kdprop = ' . $kdprop;
            }
            $timeStart = strtotime($awal);
            $timeEnd = strtotime($akhir);
// Menambah bulan ini + semua bulan pada tahun sebelumnya
            $numBulan = 1 + (date("Y", $timeEnd) - date("Y", $timeStart)) * 12;
// menghitung selisih bulan
            $numBulan += date("m", $timeEnd) - date("m", $timeStart);
//            echo $numBulan;die();
            $header = array("Nama Perusahaan", "Propinsi");
            $sql_add = "";
            $j = 2;
            $bln = array("&nbsp;", "Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
            for ($i = 1; $i <= $numBulan; $i++) {
                $sql_add .= ",(SELECT top 1 c.stok_akhr 
		FROM t_lap_bapok c
		WHERE c.bulan = '" . $bulan . "' AND c.permohonan_id = a.id AND c.bapok = '" . $jns_brg . "' AND c.tahun = '" . $tahun . "') as " . $bln[$bulan];
                $header[$j] = $bln[$bulan];
                $j++;
                $bulan++;
            }

            $sql = "SELECT a.nm_perusahaan as 'Nama Perusahaan', d.nama as 'Propinsi' " . $sql_add . "
FROM t_bapok a
LEFT JOIN t_bapok_brg b ON b.permohonan_id = a.id
LEFT JOIN m_prop d on d.id = a.kdprop
WHERE a.status = 1000 and b.jns_brg = '" . $jns_brg . "' and a.trader_id <> '140' " . $whr_prop . "
GROUP BY a.nm_perusahaan, d.nama, a.id order by d.nama";
//           die($sql);
            $data_det = $this->db->query($sql)->result_array();
//            var_dump($data_det);die();
            $komoditi = "SELECT id,uraian from m_jenis_bapok where id = '" . $jns_brg . "'";
            $data = $this->db->query($komoditi)->result_array();
        }
        //print_r($a);die();
        $arrdata['header'] = $header;
        $arrdata['detil'] = $data_det;
        $arrdata['datax'] = $data;
        $arrdata['header_ex'] = $header_ex;
        // $arrdata['detil_ex'] = $detil_ex;
        //print_r($arrdata);die();
        return $arrdata;
    }

    public function tabel_rekap($nm_izin, $tgl_awal, $tgl_akhir, $status, $rekap) {
        $table = $this->newtable;
        $kd_izin = explode("|", $nm_izin);
        $temp = '';
        $whr_prop = '';
        $whr_kab = '';
        $whr_kec = '';
        $whr_kel = '';
        //print_r($status);die();
        if ($rekap['kdprop'] != '') {
            $whr_prop = 'AND kdprop = ' . $rekap['kdprop'];
        }
        if ($rekap['kdkab'] != '') {
            $whr_kab = 'AND kdkab = ' . $rekap['kdkab'];
        }
        if ($rekap['kdkec'] != '') {
            $whr_kec = 'AND kdkec = ' . $rekap['kdkec'];
        }
        if ($rekap['kdkel'] != '') {
            $whr_kec = 'AND kdkel = ' . $rekap['kdkel'];
        }
        if ($_SESSION['role'] == '98') {
            $status = '1000';
            $whr_prop = "AND kdprop = '" . $_SESSION['kdprop'] . "'";
        }
        if ($status == '1000') {
            $tgl = 'tgl_izin';
        } else {
            $tgl = 'tgl_aju';
        }
        $query = "SELECT id, ROW_NUMBER() OVER (ORDER BY id) AS No,
				'<div><a href=\"" . site_url() . "licensing/preview/' + CAST(direktorat_id  AS VARCHAR) +'/'+ CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) + '\">'+ no_aju +'</a></div>' AS No_Pendaftaran , 
				CONVERT(varchar(10),tgl_aju,105) AS Tanggal_Daftar, CONVERT(varchar(10),tgl_izin_exp,105) AS Tanggal_Proses , npwp AS NPWP_Perusahaan, nm_perusahaan AS Nama_Perusahaan , no_izin, CONVERT(varchar(10),tgl_izin,105) AS Tanggal_Izin, 
				CASE 
					WHEN source = 1 THEN 'UPP'
					WHEN source = 0 THEN 'Online'
				END AS Daftar_via,
				'<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log
				FROM view_permohonan 
				where trader_id <> '140' AND status_id='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tgl_awal . "' AND '" . $tgl_akhir . "'
                " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
        if ($_SESSION['role'] == '98') {
            $query = "SELECT id, ROW_NUMBER() OVER (ORDER BY id) AS No,
				npwp AS NPWP_Perusahaan, nm_perusahaan AS Nama_Perusahaan , no_izin, dbo.dateIndo(tgl_izin) AS Tanggal_Izin, 
				dbo.dateIndo(tgl_izin_exp) AS Tanggal_habis
				FROM view_permohonan 
				where trader_id <> '140' AND status_id='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tgl_awal . "' AND '" . $tgl_akhir . "'
                " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
        }
        //print_r($query);die();
        $res = $this->db->query($query)->result_array();
        //print_r($res['0']['No']);die();
        $temp .= '<table class="table table-striped custom-table table-hover">';
        $temp .= '<tbody>';
        $temp .= '<tr>';
        $temp .= '<td >No</td>';
        if ($_SESSION['role'] != '98') {
            $temp .= '<td >No Pendaftaran</td>';
            $temp .= '<td >Tanggal Daftar</td>';
            $temp .= '<td >Tanggal Proses</td>';
            $temp .= '<td >NPWP Perusahaan</td>';
        }
        $temp .= '<td >Nama Perusahaan</td>';
        $temp .= '<td >No izin</td>';
        $temp .= '<td >Tanggal Izin</td>';
        if ($_SESSION['role'] != '98') {
            $temp .= '<td >Daftar via</td>';
            $temp .= '<td >Log</td>';
        } else {
            $temp .= '<td >Tanggal Habis</td>';
        }
        $temp .= '<tr>';
        foreach ($res as $key) {
            $temp .= '<tr>';
            $temp .= '<td >' . $key['No'] . '</td>';
            if ($_SESSION['role'] != '98') {
                $temp .= '<td ><p>' . $key['No_Pendaftaran'] . '</td>';
                $temp .= '<td ><p>' . $key['Tanggal_Daftar'] . '</p></td>';
                $temp .= '<td ><p>' . $key['Tanggal_Proses'] . '</p></td>';
                $temp .= '<td ><p>' . $key['NPWP_Perusahaan'] . '</td>';
            }
            $temp .= '<td ><p>' . $key['Nama_Perusahaan'] . '</td>';
            $temp .= '<td ><p>' . $key['no_izin'] . '</p></td>';
            $temp .= '<td ><p>' . $key['Tanggal_Izin'] . '</p></td>';
            if ($_SESSION['role'] != '98') {
                $temp .= '<td ><p>' . $key['Daftar_via'] . '</p></td>';
                $temp .= '<td ><p>' . $key['Log'] . '</p></td>';
            } else {
                $temp .= '<td >' . $key['Tanggal_habis'] . '</td>';
            }
            $temp .= '</tr>';
        }
        $temp .= '</tbody>';
        $temp .= '</table>';
        return $temp;

        // $table->title("");
        // //$table->columns(array(site_url().'licensing/preview/{IDX}'));
        // $this->newtable->width(array('No' => 20, 'No_Pendaftaran' => 100, 'Tanggal_Daftar' => 50, 'Tanggal_Proses' => 50, 'NPWP_Perusahaan' => 70, 'Nama_Perusahaan' => 70, 'No_Izin' => 50, 'Tanggal_Izin' => 50, 'Daftar_via' => 70, 'Log' => 50));
        // $table->cidb($this->db);
        // $table->orderby('id');
        // $table->ciuri($this->uri->segment_array());
        // $table->action(site_url() . 'post/licensing/rekap_act/search');
        // $table->sortby("ASC");
        // $table->keys(array('id'));
        // $table->hiddens(array('id'));
        // $table->show_search(FALSE);
        // $table->show_chk(FALSE);
        // $table->single(FALSE);
        // $table->dropdown(FALSE);
        // $table->use_ajax(TRUE);
        // $table->hashids(TRUE);
        // $table->postmethod(FALSE);
        // $table->title(TRUE);
        // $table->tbtarget("refkbli");
        // return $table->generate($query);
    }

    public function tabel_rekap_bapokstra($rekap_bapok) {
        $table = $this->newtable;
        $temp = '';
        $whr_prop = '';
        $whr_kab = '';
        $whr_bulan = '';
        $whr_tahun = '';
        //print_r($status);die();
        if ($rekap_bapok['kdprop'] != '') {
            $whr_prop = "AND a.kdprop = '" . $rekap_bapok['kdprop'] . "'";
        }
        if ($rekap_bapok['kdkab'] != '') {
            $whr_kab = "AND a.kdkab = '" . $rekap_bapok['kdkab'] . "'";
        }
        if ($rekap_bapok['bulan'] != '') {
            $whr_bulan = "AND b.bulan = '" . $rekap_bapok['bulan'] . "'";
        }
        if ($rekap_bapok['tahun'] != '') {
            $whr_tahun = "AND b.tahun = '" . $rekap_bapok['tahun'] . "'";
        }
        if ($_SESSION['role'] == '98') {
            $whr_prop = "AND a.kdprop = '" . $_SESSION['kdprop'] . "'";
        }
        $query = "select y.uraian as jenis_bapok, g.uraian as bapok, CONVERT(varchar, CAST(z.jumlah AS money), 1) as jumlah, g.urai_satuan as satuan, z.nama as prop
                from m_jenis_bapok g 
                left join (
                select sum(b.stok_akhr) as jumlah , a.kdprop, b.bapok, c.nama
                from   t_lap_bapok b 
                        left join t_bapok a   on b.permohonan_id = a.id
                        left join m_prop c on a.kdprop = c.id
                where  a.status = '1000' and b.bapok is not null " . $whr_tahun . " " . $whr_bulan . " " . $whr_prop . " " . $whr_kab . "  
                group by  a.kdprop, b.bapok, c.nama  )  z on  z.bapok = g.id
                left join m_reff y on g.jenis_bapok = y.kode and y.jenis = 'JENIS_BAPOK' order by g.jenis_bapok, g.id";
        //print_r($query);die();
        $res = $this->db->query($query)->result_array();
        //print_r($res['0']['No']);die();
        $temp .= '<table class="table table-striped custom-table table-hover">';
        $temp .= '<tbody>';
        $temp .= '<tr>';
        $temp .= '<td >KOMODITI</td>';
        $temp .= '<td >JUMLAH</td>';
        $temp .= '<td >SATUAN</td>';
        $temp .= '<tr>';
        foreach ($res as $key) {
            $temp .= '<tr>';
            $temp .= '<td width = "40%"><p>' . $key['bapok'] . '</p></td>';
            $temp .= '<td ><p>' . $key['jumlah'] . '</p></td>';
            $temp .= '<td ><p>' . $key['satuan'] . '</td>';
            $temp .= '</tr>';
        }
        $temp .= '</tbody>';
        $temp .= '</table>';
        return $temp;

        // $table->title("");
        // //$table->columns(array(site_url().'licensing/preview/{IDX}'));
        // $this->newtable->width(array('No' => 20, 'No_Pendaftaran' => 100, 'Tanggal_Daftar' => 50, 'Tanggal_Proses' => 50, 'NPWP_Perusahaan' => 70, 'Nama_Perusahaan' => 70, 'No_Izin' => 50, 'Tanggal_Izin' => 50, 'Daftar_via' => 70, 'Log' => 50));
        // $table->cidb($this->db);
        // $table->orderby('id');
        // $table->ciuri($this->uri->segment_array());
        // $table->action(site_url() . 'post/licensing/rekap_act/search');
        // $table->sortby("ASC");
        // $table->keys(array('id'));
        // $table->hiddens(array('id'));
        // $table->show_search(FALSE);
        // $table->show_chk(FALSE);
        // $table->single(FALSE);
        // $table->dropdown(FALSE);
        // $table->use_ajax(TRUE);
        // $table->hashids(TRUE);
        // $table->postmethod(FALSE);
        // $table->title(TRUE);
        // $table->tbtarget("refkbli");
        // return $table->generate($query);
    }

    public function tabel_rekap_antarpulau($antar_pulau) {
        // print_r($antar_pulau);die();
        $temp = '';
        $temp_add = '';
        $whr_eta = '';
        $whr_etd = '';
        $whr_muat = '';
        $whr_bongkar = '';
        if ($antar_pulau['tgl_awal'] != '' && $antar_pulau['tgl_akhir'] != '') {
            $temp_add .= " AND a.eta BETWEEN '" . $antar_pulau['tgl_awal'] . "' AND '" . $antar_pulau['tgl_akhir'] . "'";
        }
        if ($antar_pulau['tgl_awal2'] != '' && $antar_pulau['tgl_akhir2'] != '') {
            $temp_add .= " AND a.etd BETWEEN '" . $antar_pulau['tgl_awal2'] . "' AND '" . $antar_pulau['tgl_akhir2'] . "'";
        }
        if ($antar_pulau['pel_muat'] != '') {
            $temp_add .= " AND a.pel_asal = '" . $antar_pulau['pel_muat'] . "'";
        }
        if ($antar_pulau['pel_bongkar'] != '') {
            $temp_add .= " AND a.pel_tujuan = '" . $antar_pulau['pel_bongkar'] . "'";
        }
        if ($antar_pulau['npwp'] != '') {
            $temp_add .= " AND a.npwp = '" . $antar_pulau['npwp'] . "'";
        }
        if ($antar_pulau['nama'] != '') {
            $temp_add .= " AND a.nama = '" . $antar_pulau['nama'] . "'";
        }
        if ($antar_pulau['prop_asal'] != '') {
            $temp_add .= " AND a.prop_asal = '" . $antar_pulau['prop_asal'] . "'";
        }
        if ($antar_pulau['prop_tujuan'] != '') {
            $temp_add .= " AND a.prop_tujuan = '" . $antar_pulau['prop_tujuan'] . "'";
        }
        if ($antar_pulau['nama_kapal'] != '') {
            $temp_add .= " AND a.nama_kapal = '" . $antar_pulau['nama_kapal'] . "'";
        }
        //print_r($status);die();
        $query = "SELECT a.npwp as npwp_pengirim, a.nama as nama_pengirim,
        a.npwp_penerima, a.nama_penerima, m.kode + ' - ' +m.uraian as pel_asal,
        n.kode + ' - ' +n.uraian as pel_tujuan, a.nama_kapal as nama_kapal, dbo.dateIndo(a.eta) as eta, dbo.dateIndo(a.etd) as etd
        from t_lap_pulau a
        left join m_reff b on a.jenis_pelapor = b.kode and b.jenis = 'JENIS_PELAPOR'
        left join m_reff c on a.tipe_perushaan = c.kode and c.jenis = 'TIPE_PERUSAHAAN'
        left join m_prop d on a.kdprop = d.id
        left join m_kab e on a.kdkab = e.id
        left join m_kec f on a.kdkec = f.id
        left join m_kel g on a.kdkel = g.id
        left join m_reff h on a.tipe_perushaan_penerima = h.kode and h.jenis = 'TIPE_PERUSAHAAN'
        left join m_prop i on a.kdprop_penerima = i.id
        left join m_kab j on a.kdkab_penerima = j.id
        left join m_kec k on a.kdkec_penerima = k.id
        left join m_kel l on a.kdkel_penerima = l.id
        left join m_peldn m on a.pel_asal = m.kode
        left join m_peldn n on a.pel_tujuan = n.kode
        left join m_reff o on a.jenis_asal = o.kode and o.jenis = 'JENIS_ASAL'
        left join m_negara p on a.negara_tujuan = p.kode
        left join m_kab q on a.kab_asal = q.id
        left join m_kab r on a.kab_tujuan = r.id
        left join t_lap_pulau_eks s1 on s1.id_lap = a.id  and s1.no_urut = 1 
        left join t_lap_pulau_eks s2 on s2.id_lap = a.id  and s2.no_urut = 2 
        left join m_reff t on s1.tipe_perushaan = t.kode and t.jenis = 'TIPE_PERUSAHAAN'
        left join m_prop u on s1.kdprop = u.id
        left join m_kab v on s1.kdkab = v.id
        left join m_kec w on s1.kdkec = w.id
        left join m_kel x on s1.kdkel = x.id
        left join m_reff y on s2.tipe_perushaan = y.kode and y.jenis = 'TIPE_PERUSAHAAN'
        left join m_prop z on s2.kdprop = z.id
        left join m_kab aa on s2.kdkab = aa.id
        left join m_kec ab on s2.kdkec = ab.id
        left join m_kel ac on s2.kdkel = ac.id
        where DATEDIFF(day,a.etd, getdate()) > 7 " . $temp_add;
        // print_r($query);die();
        $res = $this->db->query($query)->result_array();
        //print_r($res['0']['No']);die();
        $temp .= '<table class="table responsive-data-table data-table">';
        $temp .= '<thead>';
        $temp .= '<tr>';
        $temp .= '<td >NPWP PENGIRIM</td>';
        $temp .= '<td >NAMA PENGIRIM</td>';
        $temp .= '<td >NPWP PENERIMA</td>';
        $temp .= '<td width = "15%">NAMA PENERIMA</td>';
        $temp .= '<td >PEL ASAL</td>';
        $temp .= '<td >PEL TUJUAN</td>';
        $temp .= '<td >TANGGAL MUAT</td>';
        $temp .= '<td >TANGGAL BONGKAR</td>';
        $temp .= '</tr>';
        $temp .= '</thead>';
        $temp .= '<tbody>';
        foreach ($res as $key) {
            $temp .= '<tr>';
            $temp .= '<td ><p>' . $key['npwp_pengirim'] . '</td>';
            $temp .= '<td ><p>' . $key['nama_pengirim'] . '</td>';
            $temp .= '<td ><p>' . $key['npwp_penerima'] . '</td>';
            $temp .= '<td ><p>' . $key['nama_penerima'] . '</td>';
            $temp .= '<td ><p>' . $key['pel_asal'] . '</td>';
            $temp .= '<td ><p>' . $key['pel_tujuan'] . '</td>';
            $temp .= '<td ><p>' . $key['etd'] . '</td>';
            $temp .= '<td ><p>' . $key['eta'] . '</td>';
            $temp .= '</tr>';
        }
        $temp .= '</tbody>';
        $temp .= '</table>';
        return $temp;
    }

    public function get_excel($izin, $tg_awal, $tg_akhir, $status, $kdprop, $kdkab, $kdkec, $kdkel) {
        $kd_izin = explode(".", $izin);
        if (trim($kdprop) != '' and $kdprop != null and $kdprop != 'null') {
            $whr_prop = 'AND kdprop = ' . $kdprop;
        }
        if (trim($kdkab) != '' and $kdkab != null and $kdkab != 'null') {
            $whr_kab = 'AND kdkab = ' . $kdkab;
        }
        if (trim($kdkec) != '' and $kdkec != null and $kdkec != 'null') {
            $whr_kec = 'AND kdkec = ' . $kdkec;
        }
        if (trim($kdkel) != '' and $kdkel != null and $kdkel != 'null') {
            $whr_kec = 'AND kdkel = ' . $kdkel;
        }
        if ($_SESSION['role'] == '98') {
            $whr_prop = " AND a.kdprop = '" . $_SESSION['kdprop'] . "'";
            $status = '1000';
        }
        if ($status == '1000') {
            $tgl = 'a.tgl_izin';
        } else {
            $tgl = 'a.tgl_aju';
        }
        //   ini query dan header nya ==========================================
        $minol = array("3", "4", "5", "6", "13");
        $waralaba = array("17", "18", "19", "20");
        if (trim($kd_izin[1]) == '1') {
            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Tipe_Permohonan', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Jumlah_Pegawai_Tetap', 'Jumlah_Pegawai_Honorer', 'Jumlah_Pegawai_Asing', 'Jumlah_Pegawai_Lokal', 'Relasi', 'Kegiatan_Usaha', 'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ',
                'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ', 'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ', 'Fax_PJ');

            $query = "SELECT a.no_aju as 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) as 'Tanggal_Pendaftaran', a.no_izin as 'No_izin', 
            dbo.dateIndo(a.tgl_izin) as 'Tanggal_Izin', dbo.dateIndo(a.tgl_izin_exp) as 'Tanggal_Expired', c.uraian as 'Tipe_Permohonan', 
			dbo.npwp(a.npwp) as 'NPWP', d.uraian as 'Bentuk_Usaha', a.nm_perusahaan 'Nama_Perusahaan', a.almt_perusahaan as 'Alamat_Perusahaan', 
			y.nama as 'Propinsi_Perusahaan', l.nama as 'Kabupaten_Perusahaan', m.nama as 'Kecamatan_Perusahaan', n.nama as 'Kelurahan_Perusahaan', 
			a.kdpos as 'Kodepos_Perusahaan', a.telp as 'Telepon_Perusahaan', a.fax as 'Fax_Perusahaan', a.pegawai_tetap as 'Jumlah_Pegawai_Tetap',
			a.pegawai_honor as 'Jumlah_Pegawai_Honorer', a.pegawai_asing as 'Jumlah_Pegawai_Asing', a.pegawai_lokal as 'Jumlah_Pegawai_Lokal',
			a.relasi as 'Relasi', a.kegiatan_usaha as 'Kegiatan_Usaha', i.uraian as 'Jenis_Identitas_PJ', a.noidentitas_pj as 'No_Idenitas_PJ', a.nama_pj as 
			'Nama_PJ', z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj as 'Tempat_Lahir_PJ', dbo.dateIndo(a.tgl_lahir_pj) as 'Tanggal_Lahir_PJ', 
			a.alamat_pj as 'Alamat_PJ', pja.nama AS 'Propinsi_PJ', pjb.nama AS 'Kabupaten_PJ', pjc.nama AS 'Kecamatan_PJ', 
			pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ',a.fax_pj as 'Fax_PJ'
			FROM t_siujs a
			LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
			LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
			LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
			LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
			LEFT JOIN m_prop y ON y.id = a.kdprop
			LEFT JOIN m_kab l ON l.id = a.kdkab
			LEFT JOIN m_kec m ON m.id = a.kdkec
			LEFT JOIN m_kel n ON n.id = a.kdkel
			LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
			LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
			LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
			LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
			where status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
        } elseif (trim($kd_izin[1]) == '2') {
            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Tipe_Permohonan', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ',
                'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ', 'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ', 'Fax_PJ',
                'Nilai_Modal', 'Nilai_Saham');

            $query = "SELECT a.no_aju as 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) as 'Tanggal_Pendaftaran', a.no_izin as 'No_izin', 
            dbo.dateIndo(a.tgl_izin) as 'Tanggal_Izin', dbo.dateIndo(a.tgl_izin_exp) as 'Tanggal_Expired', c.uraian as 'Tipe_Permohonan',
			dbo.npwp(a.npwp) as 'NPWP', d.uraian as 'Bentuk_Usaha', a.nm_perusahaan 'Nama_Perusahaan', a.almt_perusahaan as 'Alamat_Perusahaan', 
			y.nama as 'Propinsi_Perusahaan', l.nama as 'Kabupaten_Perusahaan', m.nama as 'Kecamatan_Perusahaan', n.nama as 'Kelurahan_Perusahaan', 
			a.kdpos as 'Kodepos_Perusahaan', a.telp as 'Telepon_Perusahaan', a.fax as 'Fax_Perusahaan', i.uraian as 'Jenis_Identitas_PJ', a.noidentitas_pj as 
			'No_Idenitas_PJ', a.nama_pj as 'Nama_PJ', z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj as 'Tempat_Lahir_PJ', 
			dbo.dateIndo(a.tgl_lahir_pj) as 'Tanggal_Lahir_PJ', a.alamat_pj as 'Alamat_PJ', pja.nama AS 'Propinsi_PJ', pjb.nama AS 'Kabupaten_PJ', 
			pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ', a.fax_pj as 'Fax_PJ', a.nilai_modal as 'Nilai_Modal', 
			a.nilai_saham as 'Nilai_Saham'
			FROM t_siup4 a
			LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
			LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
			LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
			LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
			LEFT JOIN m_prop y ON y.id = a.kdprop
			LEFT JOIN m_kab l ON l.id = a.kdkab
			LEFT JOIN m_kec m ON m.id = a.kdkec
			LEFT JOIN m_kel n ON n.id = a.kdkel
			LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
			LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
			LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
			LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
			where status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
        } elseif (in_array(trim($kd_izin[1]), $minol)) {
            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Tipe_Permohonan', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Nama_Usaha', 'Alamat_Usaha', 'Propinsi_Usaha', 'Kabupaten_Usaha',
                'Kecamatan_Usaha', 'Kelurahan_Usaha', 'Kodepos_Usaha', 'Telepon_Usaha', 'Fax_Usaha', 'Status_Milik', 'NO_SIUP', 'TGL_SIUP',
                'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ', 'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ',
                'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ', 'Fax_PJ', 'Nilai_Modal', 'Kegiatan_Usaha', 'Kelembagaan', 'Jenis_SIUP', 'Golongan', 'Pemasaran', 'Penunjukan');
            $sblm = "SET ANSI_NULLS ON
                        SET ANSI_PADDING ON
                        SET ANSI_WARNINGS ON
                        SET ARITHABORT ON
                        SET CONCAT_NULL_YIELDS_NULL ON
                        SET QUOTED_IDENTIFIER ON
                        SET NUMERIC_ROUNDABORT OFF";
            $this->db->query($sblm);
            $query = "SELECT a.no_aju as 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) as 'Tanggal_Pendaftaran', a.no_izin as 'No_izin', 
            dbo.dateIndo(a.tgl_izin) as 'Tanggal_Izin', dbo.dateIndo(a.tgl_izin_exp) as 'Tanggal_Expired', c.uraian as 'Tipe_Permohonan', 
            dbo.npwp(a.npwp) as 'NPWP', d.uraian as 'Bentuk_Usaha', a.nm_perusahaan 'Nama_Perusahaan',a.almt_perusahaan as 'Alamat_Perusahaan', 
            y.nama as 'Propinsi_Perusahaan', l.nama as 'Kabupaten_Perusahaan', m.nama as 'Kecamatan_Perusahaan', n.nama as 'Kelurahan_Perusahaan', 
            a.kdpos as 'Kodepos_Perusahaan', a.telp as 'Telepon_Perusahaan', 
            a.fax as 'Fax_Perusahaan', a.nm_usaha 'Nama_Usaha',a.almt_usaha as 'Alamat_Usaha', up.nama as 'Propinsi_Usaha', ukab.nama as 'Kabupaten_Usaha', 
            ukec.nama as 'Kecamatan_Usaha', ukel.nama as 'Kelurahan_Usaha', a.kdpos_usaha as 'Kodepos_Usaha', a.telp_usaha as 'Telepon_Usaha', 
            a.fax_usaha as 'Fax_Usaha', a.status_milik as 'Status_Milik', a.no_siup as 'NO_SIUP', dbo.dateIndo(a.tgl_siup) as 'TGL_SIUP', 
            i.uraian as 'Jenis_Identitas_PJ', a.noidentitas_pj as 'No_Idenitas_PJ', a.nama_pj as 'Nama_PJ', 
            z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj as 'Tempat_Lahir_PJ', dbo.dateIndo(a.tgl_lahir_pj) as 'Tanggal_Lahir_PJ', a.alamat_pj as 'Alamat_PJ', 
            pja.nama AS 'Propinsi_PJ', pjb.nama AS 'Kabupaten_PJ', pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ', 
            a.fax_pj as 'Fax_PJ', a.nilai_modal as 'Nilai_Modal', a.kegiatan_usaha as 'Kegiatan_Usaha', a.kelembagaan as 'Kelembagaan',
            CASE a.jenis_siup 
                WHEN 'SIUB Besar' THEN 'SIUB Besar'
                ELSE q.uraian
            END as 'Jenis_SIUP', CASE WHEN fl_gol_a = 1 AND fl_gol_b = 1 AND fl_gol_c = 1 THEN 'Gol A, Gol B, Gol C' WHEN fl_gol_a = 1 AND fl_gol_b = 1 AND a.fl_gol_c != 1 THEN 'Gol A, Gol B' WHEN fl_gol_a = 1 AND fl_gol_b != 1 AND fl_gol_c = 1 THEN 'Gol A, Gol C' WHEN fl_gol_a != 1 AND fl_gol_b = 1 AND fl_gol_c = 1 THEN 'Gol B, Gol C' WHEN fl_gol_a = 1 AND fl_gol_b != 1 AND fl_gol_c != 1 THEN 'Gol A' WHEN fl_gol_a != 1 AND fl_gol_b = 1 AND fl_gol_c != 1 THEN 'Gol B' WHEN fl_gol_a != 1 AND fl_gol_b != 1 AND fl_gol_c = 1 THEN 'Gol C' END AS 'Golongan',
            a.pemasaran AS 'Pemasaran', STUFF((select ','+abc.penerbit_dok from t_upload abc left join t_upload_syarat abd on abd.permohonan_id = a.id and abd.izin_id = a.kd_izin and abd.dok_id IN('28','54','30','33')
            where abd.upload_id = abc.id AND abc.tipe_dok IN('28','54','30','33') FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'), 1, 1, '') as 'Penunjukan'
            FROM t_siupmb a
            LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
            LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
            LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
            LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
            LEFT JOIN m_prop y ON y.id = a.kdprop
            LEFT JOIN m_kab l ON l.id = a.kdkab
            LEFT JOIN m_kec m ON m.id = a.kdkec
            LEFT JOIN m_kel n ON n.id = a.kdkel
            LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
            LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
            LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
            LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
            LEFT JOIN m_prop up ON up.id = a.kdprop_usaha
            LEFT JOIN m_kab ukab ON ukab.id = a.kdkab_usaha
            LEFT JOIN m_kec ukec ON ukec.id = a.kdkec_usaha
            LEFT JOIN m_kel ukel ON ukel.id = a.kdkel_usaha
            LEFT JOIN m_reff q ON q.kode = a.jenis_siup AND q.jenis = 'JENIS_SIUP'
            LEFT JOIN t_siupmb_jenis r ON r.permohonan_id = a.id
            LEFT JOIN m_jenis_minol k ON k.id = r.jns_gol
            where status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
            //print_r($query);die();
        } elseif (in_array(trim($kd_izin[1]), $waralaba)) {
            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Tipe_Permohonan', 'Produk_Waralaba',
                'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan',
                'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Email_Perusahaan', 'Warganegara_PJ', 'Asal_Negara_PJ',
                'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ', 'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ',
                'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ', 'Fax_PJ', 'Email_PJ', 'Tipe_Perusahaan_PW', 'NPWP_PW',
                'Nama_Perusahaan_PW', 'Alamat_Perusahaan_PW', 'Negara_Perusahaan_PW', 'Propinsi_PW', 'Kabupaten_PW', 'Kecamatan_PW', 'Kelurahan_PW',
                'Telepon_PW', 'Fax_PW', 'Kodepos_PW', 'Email_PW', 'PJ_PW', 'Alamat_PJ_PW', 'Tempat_Dikelola_Sendiri', 'Tempat_Diwaralabakan', 'Jenis_Dagang',
                'Merk_Dagang', 'Wilayah_Dagang', 'Tipe_Pemasaran', 'Negara_Dagang', 'Jenis_Usaha', 'Perusahaan_Dalam_Negeri', 'Perusahaan_Luar_Negeri',
                'Nama_Perusahaan_Pendiri', 'Tanggal_Berdiri', 'Alamat_Pendiri');
            $query = "SELECT a.no_aju as 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) as 'Tanggal_Pendaftaran', a.no_izin as 'No_izin', 
            dbo.dateIndo(a.tgl_izin) as 'Tanggal_Izin', 
			dbo.dateIndo(a.tgl_izin_exp) as 'Tanggal_Expired', c.uraian as 'Tipe_Permohonan', prodwar.uraian as 'Produk_Waralaba', dbo.npwp(a.npwp) as 'NPWP', 
			d.uraian as 'Bentuk_Usaha', a.nm_perusahaan 'Nama_Perusahaan',a.almt_perusahaan as 'Alamat_Perusahaan', y.nama as 'Propinsi_Perusahaan', 
			l.nama as 'Kabupaten_Perusahaan', m.nama as 'Kecamatan_Perusahaan', n.nama as 'Kelurahan_Perusahaan', a.kdpos as 'Kodepos_Perusahaan', 
			a.telp as 'Telepon_Perusahaan', a.fax as 'Fax_Perusahaan', a.email as 'Email_Perusahaan', 
			negpj.nama as 'Asal_Negara_PJ', i.uraian as 'Jenis_Identitas_PJ', a.noidentitas_pj as 'No_Idenitas_PJ', a.nama_pj as 'Nama_PJ', 
			z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj as 'Tempat_Lahir_PJ', dbo.dateIndo(a.tgl_lahir_pj) as 'Tanggal_Lahir_PJ', a.alamat_pj as 'Alamat_PJ', 
			pja.nama AS 'Propinsi_PJ', pjb.nama AS 'Kabupaten_PJ', pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ', 
			a.fax_pj as 'Fax_PJ', a.email_pj as 'Email_PJ', tpw.uraian as 'Tipe_Perusahaan_PW', dbo.npwp(a.npwp_pw) as 'NPWP_PW', a.nm_perusahaan_pw as 
			'Nama_Perusahaan_PW', a.almt_perusahaan_pw as 'Alamat_Perusahaan_PW', negpw.nama as 'Negara_Perusahaan_PW', pwprop.nama as 
			'Propinsi_PW', pwkab.nama as 'Kabupaten_PW', pwkec.nama as 'Kecamatan_PW', pwkel.nama as 'Kelurahan_PW', a.telp_pw as 'Telepon_PW',
			a.fax_pw as 'Fax_PW', a.kdpos_pw as 'Kodepos_PW', a.email_pw as 'Email_PW', a.pj_pw as 'PJ_PW', a.alamat_pj_pw as 'Alamat_PJ_PW', 
			a.outlet_sendiri as 'Tempat_Dikelola_Sendiri', a.outlet_waralaba as 'Tempat_Diwaralabakan',
			a.jenis_dagang as 'Jenis_Dagang', a.merk_dagang as 'Merk_Dagang', a.wilayah_dagang as 'Wilayah_Dagang', a.tipe_pemasaran as 'Tipe_Pemasaran',
			negdag.nama as 'Negara_Dagang', jns.uraian as 'Jenis_Usaha', a.jum_dalam as 'Perusahaan_Dalam_Negeri', a.jum_luar as 'Perusahaan_Luar_Negeri',
			a.persh_pendiri as 'Nama_Perusahaan_Pendiri', dbo.dateIndo(a.tgl_pendiri) as 'Tanggal_Berdiri', a.lokasi_pendiri as 'Alamat_Pendiri'
			FROM t_stpw a
			LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
			LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
			LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
			LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
			LEFT JOIN m_prop y ON y.id = a.kdprop
			LEFT JOIN m_kab l ON l.id = a.kdkab
			LEFT JOIN m_kec m ON m.id = a.kdkec
			LEFT JOIN m_kel n ON n.id = a.kdkel
			LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
			LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
			LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
			LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
			LEFT JOIN m_prop pwprop ON pwprop.id = a.kdprop_pw
			LEFT JOIN m_kab pwkab ON pwkab.id = a.kdkab_pw
			LEFT JOIN m_kec pwkec ON pwkec.id = a.kdkec_pw
			LEFT JOIN m_kel pwkel ON pwkel.id = a.kdkel_pw
			LEFT JOIN m_reff prodwar ON prodwar.kode = a.produk_waralaba AND prodwar.jenis = 'PRODUK_WARALABA'
			
			LEFT JOIN m_negara negpj ON negpj.kode = a.asalnegara_pj
			LEFT JOIN m_reff tpw ON tpw.kode = a.tipe_perusahaan AND tpw.jenis = 'TIPE_PERUSAHAAN'
			LEFT JOIN m_negara negpw ON negpw.kode = a.negara_perusahaan_pw
			LEFT JOIN m_negara negdag ON negdag.kode = a.negara_dagang
			LEFT JOIN m_reff jns ON jns.kode = a.jns_usaha AND jns.jenis = 'KEGIATAN_USAHA'
			where status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
            //LEFT JOIN m_reff warpj ON warpj.kode = a.warganegara_pj   warpj.uraian as 'Warganegara_PJ',
//            print_r($query);die();
        } elseif (trim($kd_izin[1]) == '12') {
            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Jenis_Produksi', 'Jenis_Keagenan', 'Tipe_Permohonan', 'Jenis_Produk', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Bentuk_Usaha_Produsen', 'Nama_Produsen', 'Alamat_Produsen', 'Tanggal_Pendirian_Produsen', 'Propinsi_Produsen', 'Kabupaten_Produsen', 'Kecamatan_Produsen', 'Kelurahan_Produsen', 'Telepon_Produsen', 'Fax_Produsen', 'Bentuk_Usaha_Supplier', 'Nama_Supplier', 'Alamat_Supplier', 'Tanggal_Pendirian_Supplier', 'Propinsi_Supplier', 'Kabupaten_Supplier', 'Kecamatan_Supplier', 'Kelurahan_Supplier', 'Telepon_Supplier', 'Fax_Supplier', 'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ', 'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ', 'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ', 'Fax_PJ', 'Bidang_Usaha', 'Jumlah_Pegawai_Lokal', 'Jumlah_Pegawai_Asing', 'Wilayah_Pemasaran', 'Jenis_Barang', 'No_HS', 'Merk_Barang');
            $query = "SELECT a.no_aju as 'No_Pendaftaran', (SELECT MAX(dbo.dateIndo(abc.waktu)) FROM t_log_izin abc WHERE abc.permohonan_id = a.id AND abc.kd_izin = '12' and abc.status = '0100') as 'Tanggal_Pendaftaran', a.no_izin as 'No_izin', dbo.dateIndo(a.tgl_izin) as 'Tanggal_Izin', 
            dbo.dateIndo(a.tgl_izin_exp) as 'Tanggal_Expired', g.uraian as 'Jenis_Produksi', h.uraian as 'Jenis_Keagenan',
            c.uraian as 'Tipe_Permohonan', j.uraian as 'Jenis_Produk', dbo.npwp(a.npwp) as 'NPWP', d.uraian as 'Bentuk_Usaha', 
            a.nm_perusahaan 'Nama_Perusahaan', a.almt_perusahaan as 'Alamat_Perusahaan', y.nama as 'Propinsi_Perusahaan', l.nama as 'Kabupaten_Perusahaan',
            m.nama as 'Kecamatan_Perusahaan', n.nama as 'Kelurahan_Perusahaan', a.kdpos as 'Kodepos_Perusahaan', a.telp as 'Telepon_Perusahaan', 
            a.fax as 'Fax_Perusahaan', e.uraian as 'Bentuk_Usaha_Produsen', a.nm_perusahaan_prod as 'Nama_Produsen', 
            a.almt_perusahaan_prod as 'Alamat_Produsen', dbo.dateIndo(a.tgl_pendirian_prod) as 'Tanggal_Pendirian_Produsen', ya.nama as 'Propinsi_Produsen', 
            la.nama as 'Kabupaten_Produsen', ma.nama as 'Kecamatan_Produsen', na.nama as 'Kelurahan_Produsen', a.telp_prod as 'Telepon_Produsen',
            a.fax_prod as 'Fax_Produsen', f.uraian as 'Bentuk_Usaha_Supplier', a.nm_perusahaan_supl as 'Nama_Supplier', 
            a.almt_perusahaan_supl as 'Alamat_Supplier', dbo.dateIndo(a.tgl_pendirian_supl) as 'Tanggal_Pendirian_Supplier', yb.nama as 'Propinsi_Supplier', 
            lb.nama as 'Kabupaten_Supplier', mb.nama as 'Kecamatan_Supplier', nb.nama as 'Kelurahan_Supplier', a.telp_supl as 'Telepon_Supplier',
            a.fax_supl as 'Fax_Supplier', i.uraian as 'Jenis_Identitas_PJ', a.noidentitas_pj as 'No_Idenitas_PJ', a.nama_pj as 'Nama_PJ', 
            z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj as 'Tempat_Lahir_PJ', dbo.dateIndo(a.tgl_lahir_pj) as 'Tanggal_Lahir_PJ', a.alamat_pj as 'Alamat_PJ', 
            pja.nama AS 'Propinsi_PJ', pjb.nama AS 'Kabupaten_PJ', pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ', 
            a.fax_pj as 'Fax_PJ', a.bidang_usaha AS 'Bidang_Usaha', a.pegawai_lokal as 'Jumlah_Pegawai_Lokal', a.pegawai_asing as 'Jumlah_Pegawai_Asing', 
            a.pemasaran as 'Wilayah_Pemasaran', k.jenis as 'Jenis_Barang', k.hs as 'No_HS', k.merk as 'Merk_Barang'
            FROM t_siup_agen a
            LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
            LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
            LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
            LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
            LEFT JOIN m_prop y ON y.id = a.kdprop
            LEFT JOIN m_kab l ON l.id = a.kdkab
            LEFT JOIN m_kec m ON m.id = a.kdkec
            LEFT JOIN m_kel n ON n.id = a.kdkel
            LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
            LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
            LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
            LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
            LEFT JOIN m_prop ya ON ya.id = a.kdprop_prod
            LEFT JOIN m_kab la ON la.id = a.kdkab_prod
            LEFT JOIN m_kec ma ON ma.id = a.kdkec_prod
            LEFT JOIN m_kel na ON na.id = a.kdkel_prod
            LEFT JOIN m_prop yb ON yb.id = a.kdprop_prod
            LEFT JOIN m_kab lb ON lb.id = a.kdkab_prod
            LEFT JOIN m_kec mb ON mb.id = a.kdkec_prod
            LEFT JOIN m_kel nb ON nb.id = a.kdkel_prod
            LEFT JOIN m_reff e ON e.kode = a.tipe_perusahaan_prod AND e.jenis = 'TIPE_PERUSAHAAN'
            LEFT JOIN m_reff f ON f.kode = a.tipe_perusahaan_supl AND f.jenis = 'TIPE_PERUSAHAAN'
            LEFT JOIN m_reff g ON g.kode = a.produksi AND g.jenis = 'PRODUKSI_KEAGENAN'
            LEFT JOIN m_reff h ON h.kode = a.jenis_agen AND h.jenis = 'JENIS_KEAGENAN'
            LEFT JOIN m_reff j ON j.kode = a.produk AND j.jenis = 'PRODUK'
            LEFT JOIN t_siup_agen_detil k on k.permohonan_id = a.id
            where status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
            //print_r($query);die();
        } elseif (trim($kd_izin[1]) == '14') {
            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Jenis_Garansi', 'Tipe_Permohonan', 'Jenis_Produk', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ', 'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ', 'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ', 'Fax_PJ', 'Nama Importir', 'Alamat Importir', 'Telepon Importir', 'Fax Importir', 'Merk Produk', 'Jenis Produk', 'Tipe Produk');
            $sblm = "SET ANSI_NULLS ON
                        SET ANSI_PADDING ON
                        SET ANSI_WARNINGS ON
                        SET ARITHABORT ON
                        SET CONCAT_NULL_YIELDS_NULL ON
                        SET QUOTED_IDENTIFIER ON
                        SET NUMERIC_ROUNDABORT OFF";
            $this->db->query($sblm);
            $query = "SELECT a.no_aju AS 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) AS 'Tanggal_Pendaftaran', 
                a.no_izin AS 'No_izin', dbo.dateIndo(a.tgl_izin) AS 'Tanggal_Izin', dbo.dateIndo(a.tgl_izin_exp) AS 'Tanggal_Expired', 
                j.uraian AS 'Jenis_Garansi', c.uraian AS 'Tipe_Permohonan', j.uraian AS 'Jenis_Produk', dbo.npwp(a.npwp) AS 'NPWP', 
                d.uraian AS 'Bentuk_Usaha', a.nm_perusahaan 'Nama_Perusahaan', a.almt_perusahaan AS 'Alamat_Perusahaan', 
                y.nama AS 'Propinsi_Perusahaan', l.nama AS 'Kabupaten_Perusahaan', m.nama AS 'Kecamatan_Perusahaan', 
                n.nama AS 'Kelurahan_Perusahaan', 
                a.kdpos AS 'Kodepos_Perusahaan', a.telp AS 'Telepon_Perusahaan', a.fax AS 'Fax_Perusahaan', i.uraian AS 'Jenis_Identitas_PJ', 
                a.noidentitas_pj AS 'No_Idenitas_PJ', a.nama_pj AS 'Nama_PJ', z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj AS 'Tempat_Lahir_PJ', 
                dbo.dateIndo(a.tgl_lahir_pj) AS 'Tanggal_Lahir_PJ', a.alamat_pj AS 'Alamat_PJ', pja.nama AS 'Propinsi_PJ', 
                pjb.nama AS 'Kabupaten_PJ', 
                pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ', a.fax_pj AS 'Fax_PJ', 
                STUFF((
                SELECT convert(varchar(max),dt.nama_pabrik) + '\n'
                FROM t_garansi_pabrik dt
                WHERE dt.permohonan_id = a.id order by dt.id
                FOR XML PATH(''), TYPE).value('.', 'varchar(MAX)'), 1, 0, '') AS 'Nama Importir',
                STUFF((
                SELECT convert(varchar(max),dt.alamat) + '\n'
                FROM t_garansi_pabrik dt
                WHERE dt.permohonan_id = a.id order by dt.id
                FOR XML PATH(''), TYPE).value('.', 'varchar(MAX)'), 1, 0, '') AS 'Alamat Importir',
                STUFF((
                SELECT convert(varchar(max),dt.fax) + '\n'
                FROM t_garansi_pabrik dt
                WHERE dt.permohonan_id = a.id order by dt.id
                FOR XML PATH(''), TYPE).value('.', 'varchar(MAX)'), 1, 0, '') AS 'Telepon Importir',
                STUFF((
                SELECT convert(varchar(max),dt.nama_pabrik) + '\n'
                FROM t_garansi_pabrik dt
                WHERE dt.permohonan_id = a.id order by dt.id
                FOR XML PATH(''), TYPE).value('.', 'varchar(MAX)'), 1, 0, '') AS 'Fax Importir',
                o.merk as 'Merk Produk', p.produk AS 'Jenis Produk', o.tipe AS 'Tipe Produk'
                FROM t_garansi a
                LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN'
                LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
                LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
                LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
                LEFT JOIN m_prop Y ON y.id = a.kdprop
                LEFT JOIN m_kab l ON l.id = a.kdkab
                LEFT JOIN m_kec m ON m.id = a.kdkec
                LEFT JOIN m_kel n ON n.id = a.kdkel
                LEFT JOIN t_garansi_produk o ON o.permohonan_id = a.id
                LEFT JOIN m_produk p ON p.id = o.jenis_produk
                LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
                LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
                LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
                LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
                LEFT JOIN m_reff j ON j.kode = a.jensi_garansi AND j.jenis = 'JENIS_GARANSI'
            where status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
//            print_r($query);die();
        } elseif (trim($kd_izin[1]) == '15' || trim($kd_izin[1]) == '16') {
            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Tipe_Permohonan', 'Jenis_Produk',
                'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan',
                'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Tanggal_Awal_Pameran', 'Tanggal_Akhir_Pameran',
                'Lokasi_Pameran', 'Propinsi_Pameran', 'Kabupaten_Pameran', 'Kecamatan_Pameran', 'Kelurahan_Pameran', 'Judul_Pameran', 'Jenis_Identitas_PJ',
                'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ', 'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ', 'Kabupaten_PJ', 'Kecamatan_PJ',
                'Kelurahan_PJ', 'Telepon_PJ', 'Fax_PJ', 'Nama Importir', 'Alamat Importir', 'Telepon Importir', 'Fax Importir', 'Merk Produk', 'Jenis Produk',
                'Tipe Produk');

            $query = "SELECT a.no_aju as 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) as 'Tanggal_Pendaftaran', a.no_izin as 'No_izin', 
            dbo.dateIndo(a.tgl_izin) as 'Tanggal_Izin', 
			dbo.dateIndo(a.tgl_izin_exp) as 'Tanggal_Expired', c.uraian as 'Tipe_Permohonan', dbo.npwp(a.npwp) as 'NPWP', d.uraian as 'Bentuk_Usaha', 
			a.nm_perusahaan 'Nama_Perusahaan', a.almt_perusahaan as 'Alamat_Perusahaan', y.nama as 'Propinsi_Perusahaan', l.nama as 'Kabupaten_Perusahaan', 
			m.nama as 'Kecamatan_Perusahaan', n.nama as 'Kelurahan_Perusahaan', a.kdpos as 'Kodepos_Perusahaan', a.telp as 'Telepon_Perusahaan', 
			a.fax as 'Fax_Perusahaan', dbo.dateIndo(a.awal_pameran) as 'Tanggal_Awal_Pameran', dbo.dateIndo(a.akhir_pameran) as 'Tanggal_Akhir_Pameran', a.lokasi_pameran as 
			'Lokasi_Pameran', a.kdprop_pameran as 'Propinsi_Pameran', a.kdkab_pameran as 'Kabupaten_Pameran', a.kdkec_pameran as 'Kecamatan_Pameran', 
			a.kdkel_pameran as 'Kelurahan_Pameran', a.tema_pameran 'Judul_Pameran', i.uraian as 'Jenis_Identitas_PJ', a.noidentitas_pj as 'No_Idenitas_PJ', 
			a.nama_pj as 'Nama_PJ', z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj as 'Tempat_Lahir_PJ', 
			dbo.dateIndo(a.tgl_lahir_pj) as 'Tanggal_Lahir_PJ', a.alamat_pj as 
			'Alamat_PJ', pja.nama AS 'Propinsi_PJ', pjb.nama AS 'Kabupaten_PJ', pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 
			'Telepon_PJ', a.fax_pj as 'Fax_PJ'
			FROM t_pameran a
			LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
			LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
			LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
			LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
			LEFT JOIN m_prop y ON y.id = a.kdprop
			LEFT JOIN m_kab l ON l.id = a.kdkab
			LEFT JOIN m_kec m ON m.id = a.kdkec
			LEFT JOIN m_kel n ON n.id = a.kdkel
			LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
			LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
			LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
			LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
			where status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
            // LEFT JOIN t_pameran_event prin ON prin.permohonan_id = a.id , prin.nama as 'Nama_Acara'
            // print_r($query);die();
        } elseif (trim($kd_izin[1]) == '11') {
            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Tipe_Permohonan', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Lokasi', 'Keterangan_Lokasi', 'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ', 'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ', 'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ',
                'Fax_PJ', 'Nilai_Modal', 'Produksi', 'Nama_Produksi', 'Perdagangan', 'Jenis_Dagangan');

            $query = "SELECT a.no_aju as 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) as 'Tanggal_Pendaftaran', a.no_izin as 'No_izin', 
            dbo.dateIndo(a.tgl_izin) as 'Tanggal_Izin', dbo.dateIndo(a.tgl_izin_exp) as 'Tanggal_Expired', c.uraian as 'Tipe_Permohonan',
			dbo.npwp(a.npwp) as 'NPWP', d.uraian as 'Bentuk_Usaha', a.nm_perusahaan 'Nama_Perusahaan', a.almt_perusahaan as 'Alamat_Perusahaan', 
			y.nama as 'Propinsi_Perusahaan', l.nama as 'Kabupaten_Perusahaan', m.nama as 'Kecamatan_Perusahaan', n.nama as 'Kelurahan_Perusahaan', 
			a.kdpos as 'Kodepos_Perusahaan', a.telp as 'Telepon_Perusahaan', a.fax as 'Fax_Perusahaan', a.lokasi as 'Lokasi', a.ket_lokasi as 
			'Keterangan_Lokasi', i.uraian as 'Jenis_Identitas_PJ', a.noidentitas_pj as 'No_Idenitas_PJ', a.nama_pj as 'Nama_PJ', 
			z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj as 'Tempat_Lahir_PJ', dbo.dateIndo(a.tgl_lahir_pj) as 'Tanggal_Lahir_PJ', a.alamat_pj as 'Alamat_PJ', 
			pja.nama AS 'Propinsi_PJ', pjb.nama AS 'Kabupaten_PJ', pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ', 
			a.fax_pj as 'Fax_PJ', a.nilai_modal as 'Nilai_Modal', a.produksi as 'Produksi', a.nama_produksi as 'Nama_Produksi', 
			a.perdagangan as 'Perdagangan', a.jenis_dagangan as 'Jenis_Dagangan'
			FROM t_siupbb a
			LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
			LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
			LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
			LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
			LEFT JOIN m_prop y ON y.id = a.kdprop
			LEFT JOIN m_kab l ON l.id = a.kdkab
			LEFT JOIN m_kec m ON m.id = a.kdkec
			LEFT JOIN m_kel n ON n.id = a.kdkel
			LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
			LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
			LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
			LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
			where status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
            //print_r($query);die();
        } elseif (trim($kd_izin[1]) == '7') {
            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Tipe_Permohonan', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ',
                'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ', 'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ',
                'Fax_PJ', 'Penerbit_Rekom', 'No_Rekom', 'Tanggal_Rekom', 'Propinsi_Rekom', 'Kabupaten_Rekom');

            $query = "SELECT a.no_aju as 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) as 'Tanggal_Pendaftaran', a.no_izin as 'No_izin', 
            dbo.dateIndo(a.tgl_izin) as 'Tanggal_Izin', dbo.dateIndo(a.tgl_izin_exp) as 'Tanggal_Expired', c.uraian as 'Tipe_Permohonan',
			a.npwp as 'NPWP', d.uraian as 'Bentuk_Usaha', a.nm_perusahaan 'Nama_Perusahaan', a.almt_perusahaan as 'Alamat_Perusahaan', 
			y.nama as 'Propinsi_Perusahaan', l.nama as 'Kabupaten_Perusahaan', m.nama as 'Kecamatan_Perusahaan', n.nama as 'Kelurahan_Perusahaan', 
			a.kdpos as 'Kodepos_Perusahaan', a.telp as 'Telepon_Perusahaan', a.fax as 'Fax_Perusahaan', i.uraian as 'Jenis_Identitas_PJ', 
			a.noidentitas_pj as 'No_Idenitas_PJ', a.nama_pj as 'Nama_PJ', z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj as 'Tempat_Lahir_PJ', 
			dbo.dateIndo(a.tgl_lahir_pj) as 'Tanggal_Lahir_PJ', a.alamat_pj as 'Alamat_PJ', 
			pja.nama AS 'Propinsi_PJ', pjb.nama AS 'Kabupaten_PJ', pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ', 
			a.fax_pj as 'Fax_PJ', a.nama_rekom as 'Penerbit_Rekom', a.no_rekom as 'No_Rekom', 
			dbo.dateIndo(a.tgl_rekom) as 'Tanggal_Rekom', a.kdprop_rekom as 'Propinsi_Rekom', a.kdkab_rekom as 'Kabupaten_Rekom'
			FROM t_pkapt a
			LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
			LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
			LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
			LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
			LEFT JOIN m_prop y ON y.id = a.kdprop
			LEFT JOIN m_kab l ON l.id = a.kdkab
			LEFT JOIN m_kec m ON m.id = a.kdkec
			LEFT JOIN m_kel n ON n.id = a.kdkel
			LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
			LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
			LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
			LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
			where status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
        } elseif (trim($kd_izin[1]) == '23') {
            $header = array('Merk_Beras', 'Campuran_Beras', 'Jenis_Beras', 'Jenis_Beras_Khusus', 'Kemasan_Beras', 'No_Pendaftaran', 'Tanggal_Pendaftaran', 'Tipe_Permohonan', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Email_Perusahaan', 'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ',
                'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ', 'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ',
                'Fax_PJ');
            $sblm = "SET ANSI_NULLS ON
                    SET ANSI_PADDING ON
                    SET ANSI_WARNINGS ON
                    SET ARITHABORT ON
                    SET CONCAT_NULL_YIELDS_NULL ON
                    SET QUOTED_IDENTIFIER ON
                    SET NUMERIC_ROUNDABORT OFF";
            $this->db->query($sblm);
            $query = "SELECT 
			mr.merk as 'Merk_Beras', mr.campuran as 'Campuran_Beras', br.uraian as 'Jenis_Beras',bk.uraian as 'Jenis_Beras_Khusus', STUFF((
					SELECT ', ' +  convert(varchar(30),bb.uraian) 
					FROM dbo.t_label_kemasan kms  
			LEFT JOIN m_beras_berat bb ON kms.kd_kemasan = bb.id where kms.merk_id =  mr.id 
			FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'), 1, 2, '')  as 'Kemasan_Beras', a.no_aju AS 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) AS 'Tanggal_Pendaftaran', c.uraian AS 'Tipe_Permohonan',
			dbo.npwp(a.npwp) AS 'NPWP', d.uraian AS 'Bentuk_Usaha', a.nm_perusahaan 'Nama_Perusahaan', a.almt_perusahaan AS 'Alamat_Perusahaan', 
			y.nama AS 'Propinsi_Perusahaan', l.nama AS 'Kabupaten_Perusahaan', m.nama AS 'Kecamatan_Perusahaan', n.nama AS 'Kelurahan_Perusahaan', 
			a.kdpos AS 'Kodepos_Perusahaan', a.telp AS 'Telepon_Perusahaan', a.fax AS 'Fax_Perusahaan', mt.email as 'Email_Perusahaan',
            i.uraian AS 'Jenis_Identitas_PJ', 
			a.noidentitas_pj AS 'No_Idenitas_PJ', a.nama_pj AS 'Nama_PJ', z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj AS 'Tempat_Lahir_PJ', 
			dbo.dateIndo(a.tgl_lahir_pj) AS 'Tanggal_Lahir_PJ', a.alamat_pj AS 'Alamat_PJ', 
			pja.nama AS 'Propinsi_PJ', pjb.nama AS 'Kabupaten_PJ', pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ', 
			a.fax_pj AS 'Fax_PJ'
			FROM t_label a
			left join t_label_merek mr on a.id = mr.permohonan_id
			left join m_beras br on mr.kd_jns_beras = br.id
			left join m_beras_khusus bk on mr.kd_jns_khusus = bk.id
			LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN'
			LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
			LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
			LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
			LEFT JOIN m_prop Y ON y.id = a.kdprop
			LEFT JOIN m_kab l ON l.id = a.kdkab
			LEFT JOIN m_kec m ON m.id = a.kdkec
			LEFT JOIN m_kel n ON n.id = a.kdkel
			LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
			LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
			LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
			LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
            LEFT JOIN m_trader mt ON mt.id = a.trader_id
            where a.status='" . $status . "' AND a.kd_izin='" . trim($kd_izin[1]) . "' AND a.tgl_aju BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            AND a.trader_id <> '140' " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . ""; //print_r($query);die();
        } elseif (trim($kd_izin[1]) == '22') {
            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Tipe_Permohonan', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Email_Perusahaan', 'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ',
                'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ', 'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ',
                'Fax_PJ', 'Urai_Barang');
            $sblm = "SET ANSI_NULLS ON
                    SET ANSI_PADDING ON
                    SET ANSI_WARNINGS ON
                    SET ARITHABORT ON
                    SET CONCAT_NULL_YIELDS_NULL ON
                    SET QUOTED_IDENTIFIER ON
                    SET NUMERIC_ROUNDABORT OFF";
            $this->db->query($sblm);
            $query = "SELECT a.no_aju AS 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) AS 'Tanggal_Pendaftaran', a.no_izin AS 'No_izin', 
			dbo.dateIndo(a.tgl_izin) AS 'Tanggal_Izin', dbo.dateIndo(a.tgl_izin_exp) AS 'Tanggal_Expired', c.uraian AS 'Tipe_Permohonan',
			dbo.npwp(a.npwp) AS 'NPWP', d.uraian AS 'Bentuk_Usaha', a.nm_perusahaan 'Nama_Perusahaan', a.almt_perusahaan AS 'Alamat_Perusahaan', 
			y.nama AS 'Propinsi_Perusahaan', l.nama AS 'Kabupaten_Perusahaan', m.nama AS 'Kecamatan_Perusahaan', n.nama AS 'Kelurahan_Perusahaan', 
			a.kdpos AS 'Kodepos_Perusahaan', a.telp AS 'Telepon_Perusahaan', a.fax AS 'Fax_Perusahaan', mt.email as 'Email_Perusahaan',
            i.uraian AS 'Jenis_Identitas_PJ', 
			a.noidentitas_pj AS 'No_Idenitas_PJ', a.nama_pj AS 'Nama_PJ', z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj AS 'Tempat_Lahir_PJ', 
			dbo.dateIndo(a.tgl_lahir_pj) AS 'Tanggal_Lahir_PJ', a.alamat_pj AS 'Alamat_PJ', 
			pja.nama AS 'Propinsi_PJ', pjb.nama AS 'Kabupaten_PJ', pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ', 
			a.fax_pj AS 'Fax_PJ',   
			 STUFF((
					SELECT ', ' +  convert(varchar(30),jns.uraian) 
					FROM dbo.t_bapok_brg bpk  
			LEFT JOIN m_jenis_bapok jns ON jns.id = bpk.jns_brg where bpk.permohonan_id =  a.id 
			FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'), 1, 2, '')  as 'Urai_Barang'
			FROM t_bapok a
			LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN'
			LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
			LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
			LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
			LEFT JOIN m_prop Y ON y.id = a.kdprop
			LEFT JOIN m_kab l ON l.id = a.kdkab
			LEFT JOIN m_kec m ON m.id = a.kdkec
			LEFT JOIN m_kel n ON n.id = a.kdkel
			LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
			LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
			LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
			LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
            LEFT JOIN m_trader mt ON mt.id = a.trader_id
            where status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            AND trader_id <> '140' " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . ""; //print_r($query);die();
        } elseif (trim($kd_izin[1]) == '10') {
            $header = array('No_Pendaftaran', 'Tanggal_Pendaftaran', 'No_izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Tipe_Permohonan', 'NPWP', 'Bentuk_Usaha', 'Nama_Perusahaan', 'Alamat_Perusahaan', 'Propinsi_Perusahaan', 'Kabupaten_Perusahaan', 'Kecamatan_Perusahaan', 'Kelurahan_Perusahaan', 'Kodepos_Perusahaan', 'Telepon_Perusahaan', 'Fax_Perusahaan', 'Jenis_Identitas_PJ', 'No_Idenitas_PJ', 'Nama_PJ', 'Jabatan_PJ',
                'Tempat_Lahir_PJ', 'Tanggal_Lahir_PJ', 'Alamat_PJ', 'Propinsi_PJ', 'Kabupaten_PJ', 'Kecamatan_PJ', 'Kelurahan_PJ', 'Telepon_PJ',
                'Fax_PJ');

            $query = "SELECT a.no_aju as 'No_Pendaftaran', dbo.dateIndo(a.tgl_aju) as 'Tanggal_Pendaftaran', a.no_izin as 'No_izin', 
            dbo.dateIndo(a.tgl_izin) as 'Tanggal_Izin', dbo.dateIndo(a.tgl_izin_exp) as 'Tanggal_Expired', c.uraian as 'Tipe_Permohonan',
			a.npwp as 'NPWP', d.uraian as 'Bentuk_Usaha', a.nm_perusahaan 'Nama_Perusahaan', a.almt_perusahaan as 'Alamat_Perusahaan', 
			y.nama as 'Propinsi_Perusahaan', l.nama as 'Kabupaten_Perusahaan', m.nama as 'Kecamatan_Perusahaan', n.nama as 'Kelurahan_Perusahaan', 
			a.kdpos as 'Kodepos_Perusahaan', a.telp as 'Telepon_Perusahaan', a.fax as 'Fax_Perusahaan', i.uraian as 'Jenis_Identitas_PJ', 
			a.noidentitas_pj as 'No_Idenitas_PJ', a.nama_pj as 'Nama_PJ', z.uraian AS 'Jabatan_PJ', a.tmpt_lahir_pj as 'Tempat_Lahir_PJ', 
			dbo.dateIndo(a.tgl_lahir_pj) as 'Tanggal_Lahir_PJ', a.alamat_pj as 'Alamat_PJ', 
			pja.nama AS 'Propinsi_PJ', pjb.nama AS 'Kabupaten_PJ', pjc.nama AS 'Kecamatan_PJ', pjd.nama AS 'Kelurahan_PJ', a.telp_pj AS 'Telepon_PJ', 
			a.fax_pj as 'Fax_PJ'
			FROM t_sppgrapt a
			LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
			LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
			LEFT JOIN m_reff z ON z.kode = a.jabatan_pj AND z.jenis = 'JABATAN'
			LEFT JOIN m_reff i ON i.kode = a.identitas_pj AND i.jenis = 'JENIS_IDENTITAS'
			LEFT JOIN m_prop y ON y.id = a.kdprop
			LEFT JOIN m_kab l ON l.id = a.kdkab
			LEFT JOIN m_kec m ON m.id = a.kdkec
			LEFT JOIN m_kel n ON n.id = a.kdkel
			LEFT JOIN m_prop pja ON pja.id = a.kdprop_pj
			LEFT JOIN m_kab pjb ON pjb.id = a.kdkab_pj
			LEFT JOIN m_kec pjc ON pjc.id = a.kdkec_pj
			LEFT JOIN m_kel pjd ON pjd.id = a.kdkel_pj
			where status='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND " . $tgl . " BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'
            " . $whr_prop . " " . $whr_kab . " " . $whr_kec . " " . $whr_kel . "";
        }
        //     $header = array('No_Pendaftaran', 'Tanggal_Daftar', 'NPWP_Perusahaan', 'Nama_Perusahaan', 'No_Izin', 'Tanggal_Izin', 'Tanggal_Expired', 'Daftar_via');
        //     $query = "SELECT no_aju AS No_Pendaftaran, tgl_aju AS Tanggal_Daftar,  npwp AS NPWP_Perusahaan, nm_perusahaan AS Nama_Perusahaan , no_izin AS 'No_Izin', tgl_izin AS Tanggal_Izin, tgl_izin_exp AS Tanggal_Expired ,
        // CASE 
        // 	WHEN source = 1 THEN 'UPP'
        // 	WHEN source = 0 THEN 'Online'
        // END AS Daftar_via
        // FROM view_permohonan
        //    where status_id='" . $status . "' AND kd_izin='" . trim($kd_izin[1]) . "' AND tgl_aju BETWEEN '" . $tg_awal . "' AND '" . $tg_akhir . "'";
        //             //print_r($query);die();
        //    ==============================================
        // print_r($query);die();
        $data = $this->db->query($query);
        $data = $this->main->get_result($query);
        if ($data) {
            foreach ($query->result_array() as $r => $t) {
                if (in_array(trim($kd_izin[1]), $minol)) {
                    $ref = '';
                    $arrwil = explode(",", $t['Pemasaran']);
                    foreach ($arrwil as $wil) {
                        if (trim($wil) != '') {
                            if (strlen($wil) > 2) {
                                $qkab = "SELECT a.nama
                                        FROM m_kab a
                                        WHERE a.id = '" . $wil . "'";
                                $kab = $this->db->query($qkab)->row();
                                $ref[] = $kab->nama;
                            } else {
                                if ($wil == '00') {
                                    $ref[] = 'Seluruh Indonesia';
                                }
                                $qprop = "SELECT a.nama
                                        FROM m_prop a
                                        WHERE a.id = '" . $wil . "'";
                                $prop = $this->db->query($qprop)->row();
                                $ref[] = $prop->nama;
                            }
                        }
                    }
                    $res_ref = implode(", ", $ref);
                    $t['Pemasaran'] = $res_ref;
                }
                $detil[] = $t;
            }
        }
        //print_r($detil);die();
        $arrdata['header'] = $header;
        $arrdata['detil'] = $detil;
        //print_r($arrdata);die();
        return $arrdata;
    }

}

?>