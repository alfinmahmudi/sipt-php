<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Pelabuhan_act extends CI_Model{
	public function list_pelabuhan(){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable; 
			$query = "SELECT a.kode as Kode, a.uraian as 'Nama Pelabuhan' from m_peldn a WHERE a.tipe = '1'";//print_r($query);die();
			$table->title("");
			$table->columns(array("a.kode", "a.uraian"));
			$this->newtable->width(array('Nama Pengguna' => 100 , 'Nama Lengkap' => 300, 'Media Komunikasi' => 200, "Akses" => 170, "Status" => 100));
			$this->newtable->search(array(array("a.kode","Kode Pelabuhan"),
										  array("a.uraian","Nama Pelabuhan"),
										  ));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."reference/view/pelabuhan");
			$table->orderby(3);
			$table->sortby("Kode");
			$table->keys(array("Kode"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(FALSE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->tbtarget("tb_users");
			$table->menu(array('Tambah' => array('GET', site_url().'reference/add_pelabuhan/new', '0', 'home', 'modal'),
								'Edit' => array('BOOTSTRAPDIALOG', site_url().'reference/dialog_pel/', '1', 'fa fa-edit'),
							   'Delete' => array('POST', site_url().'reference/del_pel/', 'N', 'fa fa-times','isngajax')
							   ));
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Master Pelabuhan');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	public function get_pelabuhan($act, $id=''){
		if($this->newsession->userdata('_LOGGED')){
			if ($act == 'new') {
				$arrdata['act'] = site_url() . 'post/reference/pelabuhan/save';
			}else{
				$sql = "SELECT a.kode, a.uraian from m_peldn a WHERE a.tipe = '1' AND a.kode = '".$id."' ";
				$datasess = $this->db->query($sql)->row_array();
				$arrdata['sess'] = $datasess;
				$arrdata['act'] = site_url() . 'post/reference/pelabuhan/update';
			}
 			return $arrdata;
		}
	}
	
	public function set_pelabuhan($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
			}
			$msg = "MSG||NO||Data Pelabuhan Gagal Disimpan";
			$respel = FALSE;
			$arrpel = $this->main->post_to_query($this->input->post('pel'));
			$arrpel['tipe'] = "1";
			if($act == "save"){
				$this->db->trans_begin();
				$this->db->insert('m_peldn', $arrpel);
				if($this->db->affected_rows() > 0){
					$respel = TRUE;
					$logu = array('aktifitas' => 'Menambahkan Master Pelabuhan dengan nama Kode Pelabuhan : ' . $arrpel['kode'].' dan Nama Pelabuhan :  '. $arrpel['uraian'],
                        'url' => '{c}' . site_url() . 'post/reference/pelabuhan/save' . ' {m} models/reference/pelabuhan_act {f} set_pelabuhan($act, $isajax)');
                    $this->main->set_activity($logu);
				}
				if($this->db->trans_status() === FALSE || !$respel){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$msg = "MSG||YES||Data berhasil disimpan||REFRESH";
				}
				return $msg;
			}else{
				$kode = $this->input->post('kode');
				$this->db->trans_begin();
				$this->db->where('kode', $kode);
				$this->db->update('m_peldn', $arrpel);
				if($this->db->affected_rows() > 0){
					$respel = TRUE;
					$logu = array('aktifitas' => 'Mengupdate Master Pelabuhan dengan nama Kode Pelabuhan : ' . $arrpel['kode'].' dan Nama Pelabuhan :  '. $arrpel['uraian'],
                        'url' => '{c}' . site_url() . 'post/reference/pelabuhan/update' . ' {m} models/reference/pelabuhan_act {f} set_pelabuhan($act, $isajax)');
                    $this->main->set_activity($logu);
				}
				if($this->db->trans_status() === FALSE || !$respel){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$msg = "MSG||YES||Data berhasil diupdate||REFRESH";
				}
				return $msg;
			}
		}
	}

	public function delete_pel($id){
		if($this->newsession->userdata('_LOGGED')){
			// $id = hashids_decrypt($id, _HASHIDS_, 9);print_r($id);die();
			$this->db->trans_begin();
			foreach ($id as $data) {
            	$this->db->delete('m_peldn', array('kode' => $data));
        	}
			if ($this->db->affected_rows() > 0) {
            		$logu = array('aktifitas' => 'Mendelete Master Pelabuhan',
                        'url' => '{c}' . site_url() . 'reference/del_pel' . ' {m} models/reference/pelabuhan_act {f} delete_pel($id)');
                    $this->main->set_activity($logu);
            	}
			if ($this->db->trans_status === FALSE) {
            	$this->db->trans_rollback();
            	$msg = "MSG#Proses Gagal#refresh";
	        } else {
	            $this->db->trans_commit();
            	$msg = "MSG#Proses Berhasil#refresh";
	        }
			return $msg;
		}
	}
}
?>