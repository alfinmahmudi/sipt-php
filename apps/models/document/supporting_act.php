<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Supporting_act extends CI_Model {

    var $arrext = array('.jpg', '.JPG', '.jpeg', '.JPEG', '.pdf', ".PDF", ".png", ".PNG");

    function list_document() {
        if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $query = "SELECT A.id, A.created, CAST(B.keterangan AS VARCHAR(MAX)) AS 'Nama Dokumen', A.nomor AS 'Nomor Dokumen', CONVERT(VARCHAR(10), a.tgl_dok, 103) AS 'Tanggal Dokumen', CONVERT(VARCHAR(10), a.tgl_exp, 103) AS 'Tanggal Akhir', '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View' 
					 FROM t_upload A LEFT JOIN m_dok B ON a.tipe_dok = B.id 
					 WHERE A.trader_id = '" . $this->newsession->userdata('trader_id') . "'";
            $table->title("");
            $table->columns(array("A.id", "A.created", "CAST(B.keterangan AS VARCHAR(MAX))", "A.nomor", "CONVERT(VARCHAR(10), a.tgl_dok, 103)", "CONVERT(VARCHAR(10), a.tgl_exp, 103)", "<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
            $this->newtable->width(array('Nama Dokumen' => 350, 'Nomor Dokumen' => 170, 'Tanggal Dokumen' => 120, "Tanggal Akhir" => 120, "View" => 30));
            $this->newtable->search(array(array("B.keterangan", "Nama Dokumen"),
                array("CONVERT(VARCHAR(10), a.tgl_dok, 103)", "Tanggal Dokumen", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd/mm/yyyy'))),
                array("CONVERT(VARCHAR(10), a.tgl_exp, 103)", "Tanggal Akhir", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd/mm/yyyy')))
            ));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "setting/view/document");
            $table->orderby(2);
            $table->sortby("DESC");
            $table->keys(array("id"));
            $table->hiddens(array("id", "created"));
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
//            $table->hashids(TRUE);
            $table->postmethod(TRUE);
            $table->tbtarget("tb_supportdoc");
            $table->menu(array('Tambah' => array('GET', site_url() . 'setting/view/document/new', '0', 'home', 'modal'),
//                    'EDIT' => array('BOOTSTRAPDIALOG', site_url() . 'setting/edit/ajax', 'N', 'fa fa-edit', 'isngajax'),
                    'Delete' => array('POST', site_url() . 'setting/delete_status/ajax', 'N', 'fa fa-times', 'isngajax')
                ));
            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Dokumen Pendukung');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
    }

    function get_document($id, $izin_id, $dok_id, $permohonan_id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $arrdata['izin'] = $this->main->set_combobox("SELECT id, nama_izin FROM m_izin", "id", "nama_izin", TRUE);
            $arrdata['sess']['izin_id'] = $izin_id;
            $arrdata['sess']['tipe_dok'] = $dok_id;
            $arrdata['permohonan_id'] = $permohonan_id;
            $data = $this->db->query("SELECT tgl_akhir, tgl_awal, nomor, penerbit FROM m_dok WHERE id = '" . $dok_id . "'");
            if ($data->num_rows() > 0) {
                $row = $data->row();
                $arrdata['fl_tgl'] = $row->tgl_awal;
                $arrdata['fl_exp'] = $row->tgl_akhir;
                $arrdata['fl_no'] = $row->nomor;
                $arrdata['fl_pen'] = $row->penerbit;
            }
            /* $r = "select upload_id from t_upload_syarat where izin_id='".$izin_id."' AND dok_id='".$dok_id."' AND permohonan_id='".$permohonan_id."'";
              $upload_id = $this->main->get_uraian($r, "upload_id"); */
            /* $arrdata['sess']['id_u'] = $upload_id;
              echo $permohonan_id;
              print_r($arrdata['sess']);die('sws'); */
            if ($izin_id != "" && $dok_id != "") {
                $arrdata['jenis_dokumen'] = $this->main->set_combobox("SELECT id, keterangan FROM m_dok", "id", "keterangan", TRUE);
                $arrdata['readonly'] = "readonly";
                $arrdata['disabled'] = "disabled";
            }
            if ($id == "") {
                $arrdata['act'] = site_url('post/document/supporting_act/save/ajax');
            } else {
                $arrdata['jenis_dokumen'] = $this->main->set_combobox("SELECT id, keterangan FROM m_dok", "id", "keterangan", TRUE);
            }
            return $arrdata;
        }
    }

    function get_wajib(){
        if($this->newsession->userdata('_LOGGED')){
            $dok_id = $this->input->post('data');
            $sql = "SELECT a.tgl_awal, a.tgl_akhir, a.nomor, a.penerbit FROM m_dok a WHERE a.id = '".$dok_id."'";
            $arrdata = $this->db->query($sql)->row_array();
            return $arrdata;
        }
    }

    function set_document($act, $isajax) {
        //echo json_encode($_POST);die();
        if ($this->newsession->userdata('_LOGGED')) {
            if ($act == "save" || $act == "save2") {
                $msg = "Data Gagal Dismpan";
                $resdoc = FALSE;
                $arrdoc = $this->main->post_to_query($this->input->post('document'));
                $append_id = $this->input->post('append_id');
                //print_r($_FILES['userfile']);exit();
                $ekstensi = $this->main->get_uraian("SELECT ekstensi FROM m_dok WHERE id = '".$arrdoc['tipe_dok']."'", "ekstensi");
                if (trim($ekstensi) != '') {
                    $tipe_upload = explode(",", $ekstensi);
                    $ekstensi_dok = explode("/", $_FILES['userfile']['type']);//print_r(strtoupper($ekstensi_dok[1]));die();
                    if (!in_array(strtoupper($ekstensi_dok[1]), $tipe_upload)) {
                        $msg = "MSG||NO||Data Yang Di Upload Harus Menggunakan Ekstensi ".$ekstensi." .";
                        return $msg;
                    }
                }
                $arrupload = array();
                $jnsdoc = $this->main->get_uraian("SELECT kode FROM m_dok WHERE id = '" . $arrdoc['tipe_dok'] . "'", "kode");
                $dir = './upL04d5/document/' . $jnsdoc . '/' . date("Y") . "/" . date("m") . "/" . date("d"); //print_r($dir);exit();
                if (!empty($_FILES['userfile']['error'])) {
                    switch ($_FILES['userfile']['error']) {
                        case '1':
                            $msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
                            break;
                        case '2':
                            $msg = 'MSG||NO||The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                            break;
                        case '3':
                            $msg = 'MSG||NO||The uploaded file was only partially uploaded';
                            break;
                        case '4':
                            $msg = 'MSG||NO||The uploded file was not found';
                            break;
                        case '6':
                            $msg = 'MSG||NO||Missing a temporary folder';
                            break;
                        case '7':
                            $msg = 'MSG||NO||Failed to write file to disk';
                            break;
                        case '8':
                            $msg = 'MSG||NO||File upload stopped by extension';
                            break;
                        case '999':
                        default:
                            $msg = 'MSG||NO||No error code avaiable';
                    }
                } else if (!in_array($this->main->allowed($_FILES['userfile']['name']), $this->arrext)) {
                    $msg = "MSG||NO||Upload file hanya diperbolehkan dalam bentuk .pdf, .png atau .jpg";
                } else if (empty($_FILES['userfile']['tmp_name']) || $_FILES['userfile']['tmp_name'] == 'none') {
                    $msg = 'MSG||NO||File Upload di temukan';
                }else if ($_FILES['userfile']['size'] > 3000000) {
                    $msg = 'MSG||NO||File Upload Maksimal Hanya 2 MB';
                } else {
                    if (file_exists($dir) && is_dir($dir)) {
                        $config['upload_path'] = $dir;
                    } else {
                        if (mkdir($dir, 0777, true)) {
                            if (chmod($dir, 0777)) {
                                $config['upload_path'] = $dir;
                            }
                        }
                    }
                    $config['allowed_types'] = 'jpg|jpeg|pdf|png';
                    $config['remove_spaces'] = TRUE;
                    $ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
                    $config['file_name'] = "DOCUMENT-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext;
                    $this->load->library('upload', $config);
                    $this->upload->display_errors('', '');

                    if (!$this->upload->do_upload("userfile")) {
                        $msg = $this->upload->display_errors();
                    } else {
                        $data = $this->upload->data();
                        unset($arrdoc['id']);
                        if ($this->newsession->userdata('role') == '01') {
                            $permohonan_id = $this->input->post('permohonan_id');
                            $kd_izin = $this->input->post('izin_id');
                            $sqlTrader = "SELECT direktorat_id, kd_izin, tipe_permohonan, trader_id FROM dbo.view_permohonan WHERE id = '" . $permohonan_id . "' and kd_izin ='" . $kd_izin . "'";
                            $trader_id = $this->main->get_uraian($sqlTrader, "trader_id");
                            $arrdoc['trader_id'] = $trader_id;
                        } else {
                            $arrdoc['trader_id'] = $this->newsession->userdata('trader_id');
                        }

                        if ($arrdoc['tgl_exp'] == "")
                            $arrdoc['tgl_exp'] = null;
                        $arrdoc['nama_file'] = $data['file_name'];
                        $arrdoc['ori_file'] = $data['client_name'];
                        $arrdoc['folder'] = $dir;
                        $arrdoc['ekstensi'] = $data['file_ext'];
                        $arrdoc['ukuran'] = $data['file_size'];
                        $arrdoc['created_user'] = $this->newsession->userdata('username');
                        $arrdoc['created'] = 'GETDATE()';
                        $this->db->insert('t_upload', $arrdoc);
                        if ($this->db->affected_rows() > 0) {
                            $resdoc = TRUE;
                            if ($this->input->post("tes")) {
                                $query1 = "SELECT id, id AS 'upload_id', nomor AS 'NoDokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'TglDokumen', tgl_exp AS 'TglAkhir', '<a href=\"" . site_url() . "download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
										FROM t_upload WHERE id = '" . $this->db->insert_id() . "' ";
                                $result = $this->db->query($query1)->result_array();
                                $msg = json_encode($result);
                                //print_r($result);die();
                                //return $msg;
                                //die();
                            } else {
                                $msg = "MSG||YES||Data Berhasil Disimpan|| ||APPEND||" . $append_id;
                            }
                        }
                    }
                }
                if ($act == "save2") {
                    if ($this->newsession->userdata('role') == '01') {
                        $urlNext = 'licensing/form/fourth/02/' . $kd_izin . '/01/' . $permohonan_id;
                    } else {
                        $urlNext = 'licensing/view/requirements';
                    }
                } else {
                    $urlNext = '/setting/view/document';
                }

                if ($this->db->trans_status() === FALSE || !$resdoc) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                }
                //print_r($msg);die();
                return $msg;
            }
        }
    }
    
    function del_status($id) {
//        print_r($id);die();
        /*
          $exp[0]->Direktorat_id
          $exp[1]->kode izin
          $exp[2]->tipe_permohonan
          $exp[3]->id
         */
        $this->db->trans_begin();

            $status = $this->main->get_uraian("select flag_used from t_upload where id=" . $this->db->escape($id), "flag_used");
            if ($status == 1) {
                $msg = "MSG#Tidak Bisa dihapus, File sedang digunakan#refresh";
                return $msg;
            } else {
                $this->db->where('id', $id);
                $this->db->delete('t_upload');
            }

        if ($this->db->trans_status() === FALSE || $i > 0) {
            $this->db->trans_rollback();
            $msg = "MSG#Proses Gagal#refresh";
        } else {
            $this->db->trans_commit();
            $msg = "MSG#Proses Berhasil#refresh";
        }
        return $msg;
    }

}

?>