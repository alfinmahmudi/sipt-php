<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Perizinan_act extends CI_Model{
	
	public function get_licensing(){
		
			$sql = "SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT'";
			$arrdata['direktorat'] = $this->main->set_combobox($sql,"kode","uraian", TRUE);
			$m = $this->main->get_result($sql);
			if($m){
				foreach($sql->result_array() as $r){
					$dir[] = $r;
				}
			}
			//print_r($dir);die();
			$i=0;
				foreach($dir as $b){
					$q = "select a.id,a.nama_izin, b.uraian from m_izin a left JOIN m_reff b on a.direktorat_id= b.kode where a.direktorat_id='".$b['kode']."' AND jenis='DIREKTORAT' AND a.aktif = 1";
					$j=$this->main->get_result($q);
					if($j){
						foreach($q->result_array() as $w){
							$n[$b['kode']][] = $w;
						}	
					}	
				$i++;
				}
				
			$arrdata['dir'] = $dir;	
			$arrdata['izin'] = $n;
			$arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'","kode","uraian");
			$query = "SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_GARANSI'";
			$arrdata['garansi'] = $this->db->query($query)->result_array();
			return $arrdata;
	}
	
	public function view(){
		if(!$this->newsession->userdata('_LOGGED')){
			$value = explode("|", $this->input->post('dokumen'));
			// $keycode = $this->input->post('keycode');
			// if ($this->newsession->userdata('keycode_ses') != $keycode) {
			// 	echo "Capcha Tidak Sesuai";
	  //           exit();
			// }
			$direktorat = $value[0];
			$kd_izin = $value[1];
			$tipe_permohonan = $this->input->post('jenis');
			$garansi = $this->input->post('garansi');
			if ($tipe_permohonan == '01') {
				$addCon = "AND C.baru = '1'";
			}elseif ($tipe_permohonan=='02') {
				$addCon = "AND C.perubahan = '1'";
			}elseif ($tipe_permohonan=='03') {
				$addCon = "AND C.perpanjangan = '1'";
			}

			if ($garansi == '01') {
				$addGar = "AND C.tipe_izin = '01'";
			}elseif ($garansi == '02') {
				$addGar = "AND C.tipe_izin = '02'";
			}
			$query = "SELECT CAST(D.keterangan AS VARCHAR(MAX)) AS 'Dokumen', B.uraian AS 'Direktorat', 
					  C.baru, C.perpanjangan, C.perubahan, C.kategori, C.tipe, E.uraian AS 'Kategori', F.uraian AS 'Syarat', C.izin_id, C.dok_id
					  FROM m_izin A LEFT JOIN m_reff B ON A.direktorat_id = B.kode AND B.jenis = 'DIREKTORAT'
					  LEFT JOIN m_dok_izin C ON A.id = C.izin_id
					  LEFT JOIN m_dok D ON C.dok_id = D.id
					  LEFT JOIN m_reff E ON C.kategori = E.kode AND E.jenis = 'KATEGORI_DOKIZIN'
					  LEFT JOIN m_reff F ON C.tipe = F.kode AND F.jenis = 'TIPE_DOKIZIN'
					  WHERE A.direktorat_id = ".$this->db->escape($direktorat)." AND C.izin_id = ".$this->db->escape($kd_izin)." ".$addCon." ".$addGar." ORDER BY 6,7";
			//print_r($query);die();
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['sess'][] = $row;
				}
				return $arrdata;
			}
		}
	}
	
	public function get_status(){
		$arrdata['jenis'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'","kode","uraian");
		return $arrdata;
	}
	
	public function get_search(){
		$keycode = $this->input->post('keycode');
		if ($this->newsession->userdata('keycode_ses') != $keycode) {
			echo "Capcha Tidak Sesuai";
            exit();
		}
		$no_permohonan = $this->input->post("no_permohonan");
		$npwp = implode('',$this->input->post("NPWP"));
		$query = "select nm_perusahaan, npwp, no_aju, tgl_aju,uraian_status, nama_izin, direktorat, status from view_permohonan  where nonpwp=".$this->db->escape($npwp)." AND no_aju=".$this->db->escape($no_permohonan)." and status_id!='0000'";
		$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['permohonan']= $row;
				}
				return $arrdata;
			}
	}
}
?>