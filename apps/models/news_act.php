<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class News_act extends CI_Model{

	public function list_news(){
		$table = $this->newtable;
                $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS_USER' and kode in(00,03) ORDER BY 1", "kode", "uraian");
		$query = "SELECT a.id as 'ID', a.title as 'Judul', substring(a.contents, 0, 500) as 'Isi Berita', CONVERT(varchar(10),a.dateshow,105) as 'Tanggal Tampil', c.uraian 'Status' 
			from m_news a 
			left join m_reff c on c.kode = a.status 
			where c.jenis = 'STATUS_USER' ";
			
			//print_r($query);die();
			$this->newtable->search(array(array("a.title", 'Judul Berita'),array("a.status", 'Status', array('ARRAY', $arrstatus)),array("a.contents","Isi Berita"),array("a.dateshow","Tanggal Tampil", array("DATEPICKER", array("TRUE", "data-date-format", "yyyy-mm-dd")))));
			$table->title("");
			$table->columns(array("a.id","a.contents", "a.dateshow", "b.title", "c.status"));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."licensing/view/news");	
			$table->detail(site_url()."news/news_detil");
			$table->sortby("DESC");
			$table->keys(array("ID"));
			$table->hiddens(array("ID"));
			$table->show_search(TRUE);
			$table->show_chk(TRUE);
			$table->single(FALSE);
			$table->dropdown(TRUE);
			$table->hashids(TRUE);
			$table->postmethod(TRUE);
			$table->title(TRUE);
			$table->expandrow(TRUE);
			$table->tbtarget("tb_status");
			$table->menu(array('Tambah' => array('GET', site_url('news/add_new'), '0', 'home'),
					'Edit' => array('GET', site_url().'news/edit_news', '1', 'fa fa-pencil-square-o'),
					'Delete' => array('POST', site_url().'news/delete_news', '1', 'fa fa-send','isngajax')));
			
			$arrdata = array('tabel' => $table->generate($query),
							 'judul' => 'Data Penambahan Berita');
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;

	}

	function get_view(){
		$arrdata = array();
		$query_view = "	SELECT top 1 a.id, a.title as 'judul', substring(a.contents, 0, 120) as 'isi', 
						CONVERT(VARCHAR(10),a.dateshow,111) as dateshow
						from m_news a
						where a.status = '00' and dateshow < (CONVERT(VARCHAR(10),GETDATE(),111))
						order by a.id DESC ";
		$sPermohonan = "SELECT TOP 5 *,dbo.dateIndo(tgl_izin) AS izin FROM VIEW_PERMOHONAN WHERE no_izin IS NOT NULL AND tgl_izin IS NOT NULL and status_id = '1000' ORDER BY tgl_izin DESC";
		$aPermohonan = $this->db->query($sPermohonan)->result_array();
		$arrdata['permohonan'] = $aPermohonan;
		$view = $this->db->query($query_view)->row_array();
		
		$query_detil = "SELECT a.id_news as 'id', a.title as 'judul_detil', a.files
		from m_news_dtl a
		where id_news = ".$this->db->escape($view['id']);
		$detil = $this->db->query($query_detil)->row_array();
		if ($detil['id'] != 0) {
			$arrdata['slide'] = array_merge($view,$detil);
		}else{
			$arrdata['slide'] = $value;
		}
		// print_r($arrdata);die();
		$sm_izin = $this->db->query("SELECT nama_izin FROM m_izin")->result_array();

		$arrdata['um'] = $sm_izin;
		//print_r($arrdata);die();
		return $arrdata;
	}

	function get_delete(){
		$msg = "MSG#Berita Gagal Dihapus.#refresh";
		$resdel = FALSE;

		$this->db->trans_begin();
		foreach($_POST['tb_chk'] as $id){
			$id_news = hashids_decrypt($id,_HASHIDS_,9);//echo $id_news;//die();
			$this->db->where('id', $id_news);
			$this->db->delete('m_news');

			$this->db->where('id_news', $id_news);
			$this->db->delete('m_news_dtl');	
		}
				
		if($this->db->affected_rows() > 0){
			$resdel = TRUE;
		}
		if($this->db->trans_status() === FALSE || $resdel == FALSE){
			//die("rollback");
			$this->db->trans_rollback();
			$msg = "MSG#Berita Gagal Dihapus.#refresh";
		}else{
			$this->db->trans_commit();
			$msg = "MSG#Berita Berhasil Dihapus.#refresh";
		}
		return $msg;
	}

	function main_view($id){
		if ($id == '00') {
			$query_view = "	SELECT a.id, a.title as 'judul', substring(a.contents, 0, 300) as 'isi', 
						CONVERT(VARCHAR(10),a.dateshow,111) as dateshow
						from m_news a
						where a.status = '00' and dateshow < (CONVERT(VARCHAR(10),GETDATE(),111))
						order by a.id DESC ";
			$view = $this->db->query($query_view)->result_array();
			$arrdata['view'] = $view;
			$arrdata['id'] = $id;
		}else{
			$query_view = "	SELECT a.id, a.title as 'judul', a.contents as 'isi'
						from m_news a
						where id = ".  $this->db->escape($id)." ";
			$view = $this->db->query($query_view)->result_array();

			$query_detil = "SELECT a.id_news as 'id', a.title as 'judul_detil', a.files
							from m_news_dtl a 
							where id_news = ".$this->db->escape($id)." ";
			$detil = $this->db->query($query_detil)->result_array();
			$banyak = $this->db->query($query_detil)->num_rows();
			$arrdata['view'] = $view;
			$arrdata['detil'] = $detil;
			$arrdata['banyak'] = $banyak;	
		}
		//print_r($arrdata);exit();
		return $arrdata;
	}

	function um_view(){
		$query_view = "	SELECT nama_izin, id FROM m_izin where aktif = 1";
		$view = $this->db->query($query_view)->result_array();
		$arrdata['view'] = $view;
		//print_r($arrdata);exit();
		return $arrdata;
	}

        function um_video(){
		$query_view = "	SELECT nama_izin, id FROM m_izin where aktif = 1";
		$view = $this->db->query($query_view)->result_array();
		$arrdata['view'] = $view;
		//print_r($arrdata);exit();
		return $arrdata;
	}
        
	function list_news_detil($id){
		$id_news = hashids_decrypt($id,_HASHIDS_,9);					
		$sql = "SELECT a.title, a.files
				from m_news_dtl a
				left join m_news b on b.id = a.id_news
				where a.id_news = ".$this->db->escape($id_news)." ";
		$data = $this->db->query($sql)->result_array();
		$banyak = $this->db->query($sql)->num_rows();
		$arrdata = array('data' => $data,
						'banyak' => $banyak);
		return $arrdata;
	}

	function get_new_req(){
		$sql = "SELECT kode , uraian from m_reff where jenis = 'STATUS_USER' ";
		$arrdata['status'] = $this->main->set_combobox($sql,"kode","uraian", TRUE);
		$arrdata['act'] = site_url('news/get_new');
		return $arrdata;
	}

	function get_edit($id){
		$id_news = hashids_decrypt($id,_HASHIDS_,9);
		$query_news = "SELECT a.id, a.title as 'judul', a.contents as 'isi', a.dateshow as 'tanggal', b.title 'judul_gambar', a.status, b.files
			from m_news a 
			left join m_news_dtl b on b.id_news = a.id 
			left join m_reff c on c.kode = a.status 
			where c.jenis = 'STATUS_USER' and a.id = ".$this->db->escape($id_news);
		$data_news = $this->db->query($query_news)->row_array();
		$query_banyak = "SELECT id_news, id, title, files from m_news_dtl where id_news = ".$this->db->escape($id_news);
		$banyak = $this->db->query($query_banyak)->num_rows();
		$data_detil = $this->db->query($query_banyak)->result_array();
		$sql = "SELECT kode , uraian from m_reff where jenis = 'STATUS_USER' ";

		$arrdata = array(
					'data_news' => $data_news,
					'banyak' => $banyak,
					'data_detil' => $data_detil,
					'act' => site_url('news/update_news'),
					'status' => $this->main->set_combobox($sql,"kode","uraian", TRUE));
		return $arrdata;
	}

	function update_news($isajax){
		if(!$isajax){
			return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
			exit();
		}
		
		$msg = "MSG||NO||Berita Gagal Disimpan.||REFRESH";
		$resadd = FALSE;

		$arrcekupload = $_FILES['gambar_detil'];//print_r($arrcekupload['name'][0]);die();
		$arrdetil = $this->input->post('judul_gambar');//print_r($arrdetil);exit();
		$arrid_detil = $this->input->post('id_detil');//print_r($arrid_detil);exit();
		$arrnews = $this->main->post_to_query($this->input->post('NEWS'));//print_r($arrnews);exit(); 
		$arrjudul = $this->input->post('judul');
		$news_id = $this->input->post('id_news');//print_r($news_id);exit();
		$arrfiles = $_FILES['gambar'];
		$arrname = $_FILES['gambar']['name'];
		$arrtype = $_FILES['gambar']['type'];
		$arrtmp_name = $_FILES['gambar']['tmp_name'];
		$arrerror = $_FILES['gambar']['error'];//print_r($arrerror[0]);exit();
		$arrsize = $_FILES['gambar']['size'];
		$jumlah = $this->input->post('cek');//print_r($jumlah);exit();
		$arrext = array('.jpg', '.JPG', '.jpeg', '.JPEG', '.pdf', ".PDF", ".png", ".PNG");
		$dirfile = './upL04d5/document/NEWS/'. date("Y")."/".date("m")."/".date("d")."/";
		$files = $_FILES;

		$this->db->trans_begin();
		$arrnews['createdby'] = $this->newsession->userdata('nama');
		//print_r($arrnews);exit();
		$this->db->where('id', $news_id);
		$this->db->update('m_news', $arrnews);
		
		$i = 0;
		foreach ($arrid_detil as $id_detil) {
			if ($arrcekupload['name'][$i] != null) {
				$allow = $this->main->allowed($arrcekupload['name'][$i]);
				if(!in_array($allow, $arrext)){
					$msg = "MSG||NO||Upload File Surat TDP hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png"; 
					return $msg; break;
				}elseif(empty($_FILES['gambar_detil']['tmp_name'][$i]) || $_FILES['gambar_detil']['tmp_name'] == 'none'){
					$msg = 'MSG||NO||File TDP Upload tidak di temukan';
					return $msg; break;
				}else
				if(!empty($_FILES['gambar_detil']['error'][$i])){
					switch ($_FILES['gambar_detil']['error']){
					  case '1':
						$msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
						return $msg;
						break;
					  case '2':
						$msg = 'MSG||NO||The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
						return $msg;
						break;
					  case '3':
						$msg = 'MSG||NO||The uploaded file was only partially uploaded';
						return $msg;
						break;
					  case '4':
						$msg = 'MSG||NO||The uploded file was not found';
						return $msg;
						break;
					  case '6':
						$msg = 'MSG||NO||Missing a temporary folder';
						return $msg;
						break;
					  case '7':
						$msg = 'MSG||NO||Failed to write file to disk';
						return $msg;
						break;
					  case '8':
						$msg = 'MSG||NO||File upload stopped by extension';
						return $msg;
						break;
					}
				}else{
					if(file_exists($dirfile) && is_dir($dirfile)){
						$config['upload_path'] = $dirfile;
					}else{
						if(mkdir($dirfile, 0777, true)){
							if(chmod($dirfile, 0777)){
								$config['upload_path'] = $dirfile;
							}
						}
					}
					$_FILES['userfile']['name']= $files['gambar_detil']['name'][$i];
			        $_FILES['userfile']['type']= $files['gambar_detil']['type'][$i];
			        $_FILES['userfile']['tmp_name']= $files['gambar_detil']['tmp_name'][$i];
			        $_FILES['userfile']['error']= $files['gambar_detil']['error'][$i];
			        $_FILES['userfile']['size']= $files['gambar_detil']['size'][$i];

					$config4['upload_path'] = $dirfile;
					$config4['allowed_types'] = 'jpg|jpeg|pdf|png';
					$config4['remove_spaces'] = TRUE;
					$ext4 = pathinfo($arrcekupload['name'][$i], PATHINFO_EXTENSION);
					$config4['file_name'] = "NEWS-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext4;
					$this->load->library('upload');
					$this->upload->initialize($config4);
					$this->upload->display_errors('', '');

					$this->upload->do_upload();
					$dataNEWS = $this->upload->data();
					$tmpnews = $dirfile.$config4['file_name'];
				}
				$nama_file = array('files' => $tmpnews);
				$this->db->where('id', $id_detil);
				$this->db->update('m_news_dtl', $nama_file);
			}
			$judul_gambar = array('title' => $arrdetil[$i]);
			$this->db->where('id', $id_detil);
			$this->db->update('m_news_dtl', $judul_gambar);
			$i++;
		}
		//exit("sukses!");
		if ($arrname[0] != null) {
			for($i=0; $i < $jumlah[0]; $i++) { 
					$allow = $this->main->allowed($arrname[$i]);

					if(!in_array($allow, $arrext)){
						$msg = "MSG||NO||Upload File Surat TDP hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png"; 
						return $msg; break;
					}elseif(empty($arrtmp_name[$i]) || $arrtmp_name[$i] == 'none'){
						$msg = 'MSG||NO||File TDP Upload tidak di temukan';
						return $msg; break;
					}else
					if(!empty($arrerror[$i])){
						switch ($arrerror[$i]){
						  case '1':
							$msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
							return $msg;
							break;
						  case '2':
							$msg = 'MSG||NO||The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
							return $msg;
							break;
						  case '3':
							$msg = 'MSG||NO||The uploaded file was only partially uploaded';
							return $msg;
							break;
						  case '4':
							$msg = 'MSG||NO||The uploded file was not found';
							return $msg;
							break;
						  case '6':
							$msg = 'MSG||NO||Missing a temporary folder';
							return $msg;
							break;
						  case '7':
							$msg = 'MSG||NO||Failed to write file to disk';
							return $msg;
							break;
						  case '8':
							$msg = 'MSG||NO||File upload stopped by extension';
							return $msg;
							break;
						}
					}else{
						if(file_exists($dirfile) && is_dir($dirfile)){
							$config['upload_path'] = $dirfile;
						}else{
							if(mkdir($dirfile, 0777, true)){
								if(chmod($dirfile, 0777)){
									$config['upload_path'] = $dirfile;
								}
							}
						}

						if($arrfiles != null) {

							$_FILES['userfile']['name']= $files['gambar']['name'][$i];
					        $_FILES['userfile']['type']= $files['gambar']['type'][$i];
					        $_FILES['userfile']['tmp_name']= $files['gambar']['tmp_name'][$i];
					        $_FILES['userfile']['error']= $files['gambar']['error'][$i];
					        $_FILES['userfile']['size']= $files['gambar']['size'][$i];

							$config4['upload_path'] = $dirfile;
							$config4['allowed_types'] = 'jpg|jpeg|pdf|png';
							$config4['remove_spaces'] = TRUE;
							$ext4 = pathinfo($arrname[$i], PATHINFO_EXTENSION);
							$config4['file_name'] = "NEWS-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext4;
							$this->load->library('upload');
							$this->upload->initialize($config4);
							$this->upload->display_errors('', '');

							$this->upload->do_upload();
							$dataNEWS = $this->upload->data();
						}
						$tmpnews = $dirfile.$config4['file_name'];
					}

					$data = array(
							'files' => $tmpnews,
							'title' => $arrjudul[$i],
							'id_news' => $news_id
						);
					$this->db->insert('m_news_dtl', $data);
				}
			}
			//print_r($data);exit();
		if($this->db->affected_rows() > 0){
			$resadd = TRUE;
		}
		if($this->db->trans_status() === FALSE || $resadd == FALSE){
			// die("rollback");
			$this->db->trans_rollback();
			$msg = "MSG||NO||Berita Gagal Disimpan.||REFRESH";
		}else{
			$this->db->trans_commit();
			$msg = "MSG||NO||Berita Berhasil Disimpan.||REFRESH";
		}
		return $msg;
	}

	function delete_detil(){
		$a = $this->input->post('data');
		$this->db->where('id', $a);
		$this->db->delete('m_news_dtl');
		return true;
	}

	function update_detil(){
		$a = $this->input->post('title');
		$b = $_FILES['gambar_detil'];print_r($b);exit();
		return true;
	}

	public function add_new($isajax){
		if(!$isajax){
			return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.||REFRESH";
			exit();
		}
		
		$msg = "MSG||NO||Berita Gagal Ditambahkan.||REFRESH";
		$resadd = FALSE;

		$arrnews = $this->main->post_to_query($this->input->post('NEWS'));//print_r($arrnews);exit();
		$a = $this->input->post('konten');
		$arrjudul = $this->input->post('judul');
		$arrfiles = $_FILES['gambar'];//print_r($arrfiles);exit();
		$arrname = $_FILES['gambar']['name'];
		$arrtype = $_FILES['gambar']['type'];
		$arrtmp_name = $_FILES['gambar']['tmp_name'];
		$arrerror = $_FILES['gambar']['error'];//print_r($arrerror[0]);exit();
		$arrsize = $_FILES['gambar']['size'];
		$jumlah = $this->input->post('cek');//print_r($jumlah);exit();
		$arrext = array('.jpg', '.JPG', '.jpeg', '.JPEG', '.pdf', ".PDF", ".png", ".PNG");
		$dirfile = './upL04d5/document/NEWS/'. date("Y")."/".date("m")."/".date("d")."/";
		$files = $_FILES;
		$id_news = 0;

		$this->db->trans_begin();
		$arrnews['createdby'] = $this->newsession->userdata('nama');
		//print_r($arrnews);exit();
		$this->db->insert('m_news', $arrnews);
		
		for($i=0; $i < $jumlah[0]; $i++) { 
				$allow = $this->main->allowed($arrname[$i]);//print_r($allow);exit();

				if(!in_array($allow, $arrext)){
					$msg = "MSG||NO||Upload File Surat TDP hanya diperbolehkan dalam bentuk .pdf, .jpg dan .png"; 
					return $msg; break;
				}elseif(empty($arrtmp_name[$i]) || $arrtmp_name[$i] == 'none'){
					$msg = 'MSG||NO||File TDP Upload tidak di temukan';
					return $msg; break;
				}else
				if(!empty($arrerror[$i])){
					switch ($arrerror[$i]){
					  case '1':
						$msg = 'MSG||NO||The uploaded file exceeds the upload_max_filesize directive in php.ini';
						return $msg;
						break;
					  case '2':
						$msg = 'MSG||NO||The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
						return $msg;
						break;
					  case '3':
						$msg = 'MSG||NO||The uploaded file was only partially uploaded';
						return $msg;
						break;
					  case '4':
						$msg = 'MSG||NO||The uploded file was not found';
						return $msg;
						break;
					  case '6':
						$msg = 'MSG||NO||Missing a temporary folder';
						return $msg;
						break;
					  case '7':
						$msg = 'MSG||NO||Failed to write file to disk';
						return $msg;
						break;
					  case '8':
						$msg = 'MSG||NO||File upload stopped by extension';
						return $msg;
						break;
					}
				}else{
					if(file_exists($dirfile) && is_dir($dirfile)){
						$config['upload_path'] = $dirfile;
					}else{
						if(mkdir($dirfile, 0777, true)){
							if(chmod($dirfile, 0777)){
								$config['upload_path'] = $dirfile;
							}
						}
					}

					if($arrfiles != null) {

						$_FILES['userfile']['name']= $files['gambar']['name'][$i];
				        $_FILES['userfile']['type']= $files['gambar']['type'][$i];
				        $_FILES['userfile']['tmp_name']= $files['gambar']['tmp_name'][$i];
				        $_FILES['userfile']['error']= $files['gambar']['error'][$i];
				        $_FILES['userfile']['size']= $files['gambar']['size'][$i];

						$config4['upload_path'] = $dirfile;
						$config4['allowed_types'] = 'jpg|jpeg|pdf|png';
						$config4['remove_spaces'] = TRUE;
						$ext4 = pathinfo($arrname[$i], PATHINFO_EXTENSION);
						$config4['file_name'] = "NEWS-" . date("Ymdhis") . "-" . substr(str_shuffle(str_repeat('0123456789', 5)), 0, 5) . "." . $ext4;
						$this->load->library('upload');
						$this->upload->initialize($config4);
						$this->upload->display_errors('', '');

						$this->upload->do_upload();
						$dataNEWS = $this->upload->data();
					}
					$tmpnews = $dirfile.$config4['file_name'];
				}

				if ($id_news == 0) {
					$id_news = $this->db->insert_id();
				}

				$data = array(
						'files' => $tmpnews,
						'title' => $arrjudul[$i],
						'id_news' => $id_news
					);
				$this->db->insert('m_news_dtl', $data);
			}
			//print_r($data);exit();
		if($this->db->affected_rows() > 0){
			$resadd = TRUE;
		}
		if($this->db->trans_status() === FALSE || $resadd == FALSE){
			// die("rollback");
			$this->db->trans_rollback();
			$msg = "MSG||NO||Berita Gagal Ditambahkan.||REFRESH";
		}else{
			$this->db->trans_commit();
			$msg = "MSG||NO||Berita Berhasil Ditambahkan.||site_url('news/add_new')";
		}
		return $msg;
	}
}
?>