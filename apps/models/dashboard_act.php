<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_act extends CI_Model{

	public function get_data(){
		$query = "SELECT TOP 5 *,dbo.dateIndo(tgl_izin) AS izin FROM VIEW_PERMOHONAN WHERE no_izin IS NOT NULL AND tgl_izin IS NOT NULL and status_id = '1000' ORDER BY tgl_izin DESC";
		$arrterbit = $this->db->query($query)->result_array();
		$query = "SELECT TOP 5 a.nm_perusahaan, a.nama_izin, dbo.dateIndo(a.tgl_aju) AS izin, b.uraian as status
				FROM VIEW_PERMOHONAN a 
				LEFT JOIN m_reff b ON b.kode = a.status_id AND b.jenis = 'STATUS'
				WHERE a.status_id IN('0100') 
				ORDER BY a.tgl_aju DESC";
		$arrproses = $this->db->query($query)->result_array();

		$sql = "SELECT a.direktorat_id,
				 SUM(CASE WHEN MONTH(a.tgl_izin) = '1' THEN 1 ELSE 0 END) AS jan,
				 SUM(CASE WHEN MONTH(a.tgl_izin) = '2' THEN 1 ELSE 0 END) AS feb,
				 SUM(CASE WHEN MONTH(a.tgl_izin) = '3' THEN 1 ELSE 0 END) AS mar,
				 SUM(CASE WHEN MONTH(a.tgl_izin) = '4' THEN 1 ELSE 0 END) AS apr,
				 SUM(CASE WHEN MONTH(a.tgl_izin) = '5' THEN 1 ELSE 0 END) AS mei,
				 SUM(CASE WHEN MONTH(a.tgl_izin) = '6' THEN 1 ELSE 0 END) AS jun,
				 SUM(CASE WHEN MONTH(a.tgl_izin) = '7' THEN 1 ELSE 0 END) AS jul,
				 SUM(CASE WHEN MONTH(a.tgl_izin) = '8' THEN 1 ELSE 0 END) AS aug,
				 SUM(CASE WHEN MONTH(a.tgl_izin) = '9' THEN 1 ELSE 0 END) AS sep,
				 SUM(CASE WHEN MONTH(a.tgl_izin) = '10' THEN 1 ELSE 0 END) AS okt,
				 SUM(CASE WHEN MONTH(a.tgl_izin) = '11' THEN 1 ELSE 0 END) AS nov,
				 SUM(CASE WHEN MONTH(a.tgl_izin) = '12' THEN 1 ELSE 0 END) AS des,
				 SUM(CASE WHEN MONTH(a.tgl_izin) IN ('1','2','3','4','5','6','7','8','9','10','11','12') THEN 1 ELSE 0 END) AS jml
				 FROM VIEW_PERMOHONAN a
				 WHERE YEAR(a.tgl_izin) = YEAR(GETDATE()) AND a.status_id = '1000' AND a.no_izin IS NOT NULL AND a.status_dok = '01'
				 GROUP BY a.direktorat_id";
		$arrdir = $this->db->query($sql)->result_array();
		foreach ($arrdir as $kay) {
			$res[$kay['direktorat_id']] = array(
						$kay['jan'],
						$kay['feb'],
						$kay['mar'],
						$kay['apr'],
						$kay['mei'],
						$kay['jun'],
						$kay['jul'],
						$kay['aug'],
						$kay['sep'],
						$kay['okt'],
						$kay['nov'],
						$kay['des']
					);
		}

		$per_izin = "SELECT a.kd_izin, a.nama_izin,
					 SUM(CASE WHEN MONTH(a.tgl_izin) = '1' THEN 1 ELSE 0 END) AS jan,
					 SUM(CASE WHEN MONTH(a.tgl_izin) = '2' THEN 1 ELSE 0 END) AS feb,
					 SUM(CASE WHEN MONTH(a.tgl_izin) = '3' THEN 1 ELSE 0 END) AS mar,
					 SUM(CASE WHEN MONTH(a.tgl_izin) = '4' THEN 1 ELSE 0 END) AS apr,
					 SUM(CASE WHEN MONTH(a.tgl_izin) = '5' THEN 1 ELSE 0 END) AS mei,
					 SUM(CASE WHEN MONTH(a.tgl_izin) = '6' THEN 1 ELSE 0 END) AS jun,
					 SUM(CASE WHEN MONTH(a.tgl_izin) = '7' THEN 1 ELSE 0 END) AS jul,
					 SUM(CASE WHEN MONTH(a.tgl_izin) = '8' THEN 1 ELSE 0 END) AS aug,
					 SUM(CASE WHEN MONTH(a.tgl_izin) = '9' THEN 1 ELSE 0 END) AS sep,
					 SUM(CASE WHEN MONTH(a.tgl_izin) = '10' THEN 1 ELSE 0 END) AS okt,
					 SUM(CASE WHEN MONTH(a.tgl_izin) = '11' THEN 1 ELSE 0 END) AS nov,
					 SUM(CASE WHEN MONTH(a.tgl_izin) = '12' THEN 1 ELSE 0 END) AS des,
					 SUM(CASE WHEN MONTH(a.tgl_izin) IN ('1','2','3','4','5','6','7','8','9','10','11','12') THEN 1 ELSE 0 END) AS jml
					 FROM VIEW_PERMOHONAN a
					 WHERE YEAR(a.tgl_izin) = YEAR(GETDATE()) AND a.status_id = '1000' AND a.no_izin IS NOT NULL AND a.status_dok = '01'
					 GROUP BY a.kd_izin, a.nama_izin";
		$arrizin = $this->db->query($per_izin)->result_array();
		foreach ($arrizin as $kay) {
			$izin[$kay['kd_izin']] = array(
						$kay['jan'],
						$kay['feb'],
						$kay['mar'],
						$kay['apr'],
						$kay['mei'],
						$kay['jun'],
						$kay['jul'],
						$kay['aug'],
						$kay['sep'],
						$kay['okt'],
						$kay['nov'],
						$kay['des']
					);
			$id[] = array('id' => $kay['kd_izin'],
				'nama_izin' => $kay['nama_izin']);
		}		

		$arrdata['ket'] = $id;
		$arrdata['izin'] = $izin;//print_r($arrdata['izin']);die();
		$arrdata['dir'] = $res;
		$arrdata['proses'] = $arrproses;
		$arrdata['terbit'] = $arrterbit;
		return $arrdata;
	}

	public function get_pie(){
		$sql = "SELECT TOP 5 COUNT(a.kd_izin) as jml, a.kd_izin as id
					FROM VIEW_PERMOHONAN a 
					where a.status_id = '1000'
					GROUP BY a.kd_izin
					ORDER BY jml DESC";
		$jumlah = $this->db->query($sql)->result_array();
		foreach ($jumlah as $kd_izin) {
			$arrsiupmb = array("3", "4", "5", "6", "13");
	        $arrpameran = array("15", "16");
	        $arrstpw = array("17", "18", "19", "20", "21");
	        $field = "";
	        if (in_array($kd_izin['id'], $arrsiupmb)) {
	            $dbTable = "SIUP-MB";
	        } elseif ($kd_izin['id'] == "1") {
	            $dbTable = "SIUJS";
	        } elseif ($kd_izin['id'] == "2") {
	            $dbTable = "SIUP-4";
	        } elseif ($kd_izin['id'] == "12") {
	            $dbTable = "SIUP-AGEN";
	        } elseif ($kd_izin['id'] == "7") {
	            $dbTable = "PKAPT";
	        } elseif ($kd_izin['id'] == "8") {
	            $dbTable = "PGAPT";
	        } elseif ($kd_izin['id'] == "9") {
	            $dbTable = "SPPGAPT";
	        } elseif ($kd_izin['id'] == "10") {
	            $dbTable = "SPPGKR";
	        } elseif ($kd_izin['id'] == "11") {
	            $dbTable = "SIUP-BB";
	        } elseif ($kd_izin['id'] == "14") {
	            $dbTable = "Manual Garansi";
	        } elseif (in_array($kd_izin['id'], $arrpameran)) {
	            $dbTable = "Pameran";
	        } elseif (in_array($kd_izin['id'], $arrstpw)) {
	            $dbTable = "STPW";
	        }
	        $nama[] = $dbTable;
		}
		$query = "SELECT TOP 5 COUNT(a.kdprop) as jml, a.kdprop as id, b.nama
					FROM VIEW_PERMOHONAN a 
					LEFT JOIN m_prop b ON b.id = a.kdprop
					where a.status_id = '1000'
					GROUP BY a.kdprop, b.nama
					ORDER BY jml DESC";
		//print_r($nama);die();
		$res = $this->db->query($query)->result_array();//print_r($daerah);die();

		$arrdata['first_daerah'] = $res['0']['jml'];
		$arrdata['sec_daerah'] = $res['1']['jml'];
		$arrdata['third_daerah'] = $res['2']['jml'];
		$arrdata['fourth_daerah'] = $res['3']['jml'];
		$arrdata['fifth_daerah'] = $res['4']['jml'];
		
		$arrdata['first_nama'] = $res['0']['nama'];
		$arrdata['sec_nama'] = $res['1']['nama'];
		$arrdata['third_nama'] = $res['2']['nama'];
		$arrdata['fourth_nama'] = $res['3']['nama'];
		$arrdata['fifth_nama'] = $res['4']['nama'];

		$arrdata['first_jml'] = $jumlah['0']['jml'];
		$arrdata['sec_jml'] = $jumlah['1']['jml'];
		$arrdata['third_jml'] = $jumlah['2']['jml'];
		$arrdata['fourth_jml'] = $jumlah['3']['jml'];
		$arrdata['fifth_jml'] = $jumlah['4']['jml'];
		
		$arrdata['first_name'] = $nama['0'];
		$arrdata['sec_name'] = $nama['1'];
		$arrdata['third_name'] = $nama['2'];
		$arrdata['fourth_name'] = $nama['3'];
		$arrdata['fifth_name'] = $nama['4'];
		json_encode($arrdata);//print_r($arrdata);die();
		return $arrdata;
	}

	public function get_proses(){
		if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;

            $direktorat = "'" . join("','", $this->newsession->userdata('dir_id')) . "'";
            $kd_izin = "'" . join("','", $this->newsession->userdata('kode_izin')) . "'";
            $arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
            $arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");
            if (implode($this->newsession->userdata('dir_id')) == "02") {
                $where = "status_id IN ('0100','0104')";
            } else {
                $where = "status_id = '0100'";
            }

            $query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
                        no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' + ur_status_dok AS 'Pengajuan',
                        direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
                        CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
                         no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
                        status AS 'Status', last_proses AS SORTTGL, '<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log, 
                        kd_izin,  case when ((DATEDIFF(minute, last_proses, GETDATE()) / sla_waktu) * 100) > 100 or last_proses = NULL 
								then '<div class=\"pulse pulse5 bg-danger light-color\" \>  '+ convert(varchar(5),DATEDIFF(minute,  last_proses, GETDATE())/60) + ' Jam '+ convert(varchar(5),DATEDIFF(minute,  last_proses, GETDATE())%60) + ' Menit  </div>' 
							when ((DATEDIFF(minute, last_proses, GETDATE()) / sla_waktu) * 100) > 75 and ((DATEDIFF(minute, last_proses, GETDATE()) / sla_waktu) * 100) <= 100 
								then '<div class=\"pulse pulse5 bg-warning light-color\" \>  '+ convert(varchar(5),DATEDIFF(minute,  last_proses, GETDATE())/60) + ' Jam '+ convert(varchar(5),DATEDIFF(minute,  last_proses, GETDATE())%60) + ' Menit  </div>' 
								else '<div class=\"pulse pulse2 bg-success light-color\" \>  '+ convert(varchar(5),DATEDIFF(minute,  last_proses, GETDATE())/60) + ' Jam '+ convert(varchar(5),DATEDIFF(minute,  last_proses, GETDATE())%60) + ' Menit  </div>' end  as SLA
                        FROM VIEW_PERMOHONAN 
                        LEFT JOIN  m_waktu_proses b on kd_izin = sla_izin and status_id = sla_status
                        WHERE " . $where . "  AND direktorat_id IN (" . $direktorat . ") AND kd_izin IN (" . $kd_izin . ")";
            //print_r($query);die();
            $table->title("");
            $table->columns(array("id", "direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
                array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'", site_url() . 'licensing/preview/{IDX}'),
                "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
                "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
                "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
                "status", "CONVERT(VARCHAR(10), tgl_aju, 120)",
                "<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>"));
            $this->newtable->width(array('Pengajuan' => 200, 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
            $this->newtable->search(array(array("nm_perusahaan", "Nama Perusahaan"), array("no_aju", "Nomor Permohonan"),
                array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("no_aju", "No. Izin"),
                array("nm_perusahaan", "Nama Perusahaan"),
                array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url() . 'get/cb/set_dokumen/')),
                array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
                array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
                array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
            ));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/inbox");
            $table->orderby('last_proses');
            $table->sortby("ASC");
            $table->keys(array("IDX"));
            $table->hiddens(array("id", "IDX", "SORTTGL", "kd_izin"));
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            $table->menu(array(//'Tambah' => array('GET', site_url() . 'licensing/manual', '0', 'home', 'modal'),
                // 'Edit' => array('GET', site_url() . 'licensing/form/first', '1', 'fa fa-edit'),
                'Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-eye')
                    // 'Delete' => array('POST', site_url().'licensing/delete_inbox/ajax','N', 'fa fa-times','isngajax')
            ));
            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Permohonan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
	}

	public function get_terbit(){
		if ($this->newsession->userdata('_LOGGED')) {
            $table = $this->newtable;
            $arrdirektorat = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'DIREKTORAT' ORDER BY 1", "kode", "uraian");
            $arrjenis = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN' ORDER BY 1", "kode", "uraian");
            $arrstatus = $this->main->array_cb("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS' ORDER BY 1", "uraian", "uraian");

            $direktorat = "'" . join("','", $this->newsession->userdata('dir_id')) . "'";
            $kd_izin = "'" . join("','", $this->newsession->userdata('kode_izin')) . "'";

            $query = "SELECT id, direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR) AS IDX,
no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>' + ur_status_dok AS 'Pengajuan',
direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>' AS 'Dokumen Perizinan', '<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' +
CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>' AS 'Daftar Dokumen',
no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>' AS 'Perizinan',
status AS 'Status', CONVERT(VARCHAR(10), tgl_aju, 120) AS SORTTGL, '<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>' AS Log, kd_izin
FROM VIEW_PERMOHONAN WHERE status_id = '1000' AND direktorat_id IN (" . $direktorat . ") AND kd_izin IN (" . $kd_izin . ")";
            $table->title("");
            $table->columns(array("id", "direktorat_id + '/' + CAST(kd_izin AS VARCHAR) + '/' + tipe_permohonan + '/' + CAST(id AS VARCHAR)",
                array("no_aju + '<div>Tanggal : ' + CONVERT(VARCHAR(10), tgl_aju, 105) + '</div>'", site_url() . 'licensing/preview/{IDX}'),
                "direktorat + '<div><b>' + nama_izin + '</b> ( ' + uraian_status + ' )</div>'",
                "'<div><b>' +npwp+ '</b></div>' + '<div><b>' +nm_perusahaan+ '</b></div>' + CONVERT(VARCHAR(10), tgl_aju, 105)  + ' ' + CONVERT(VARCHAR(8), created, 108) + '<div>' +created_user+ '</div>'",
                "no_izin + '<div>' + ISNULL(CONVERT(VARCHAR(10),  tgl_izin, 120),'-') + ' s.d ' + ISNULL(CONVERT(VARCHAR(10), tgl_izin_exp, 120),'-') + '</div>'",
                "status", "CONVERT(VARCHAR(10), tgl_aju, 120)",
                "<button data-url = \"" . site_url() . "get/popup/log/' + CAST(id AS VARCHAR) + '/' + CAST(kd_izin AS VARCHAR) + '\" data-toggle=\"button\" class=\"btn btn-info btn-xs\" aria-pressed=\"true\" onClick=\"showlog($(this)); return false;\"><i class=\"fa fa-info\"></i></button>"));
            $this->newtable->width(array('Pengajuan' => 200, 'Dokumen Perizinan' => 400, 'Daftar Dokumen' => 200, "Perizinan" => 170, "Status" => 100));
            $this->newtable->search(array(array("no_aju", "Nomor Permohonan"),
                array("CONVERT(VARCHAR(10), tgl_aju, 105)", "Tanggal Permohonan", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("no_aju", "No. Izin"),
                array("nm_perusahaan", "Nama Perusahaan"),
                array("CONVERT(VARCHAR(10), tgl_ijin, 105)", "Tanggal Izin", array('DATEPICKER', array('TRUE', 'data-date-format', 'dd-mm-yyyy'))),
                array("direktorat_id", 'Direktorat', array('ARRAY', $arrdirektorat), array(site_url() . 'get/cb/set_dokumen/')),
                array("kd_izin", 'Dokumen Perizinan', array('ARRAY', array())),
                array("tipe_permohonan", 'Jenis Perizinan', array('ARRAY', $arrjenis)),
                array("status", 'Status Permohonan', array('ARRAY', $arrstatus))
            ));
            $table->cidb($this->db);
            $table->ciuri($this->uri->segment_array());
            $table->action(site_url() . "licensing/view/released");
            $table->orderby(8);
            $table->sortby("ASC");
            $table->keys(array("IDX"));
            $table->hiddens(array("id", "IDX", "SORTTGL", "kd_izin"));
            $table->show_search(TRUE);
            $table->show_chk(TRUE);
            $table->single(FALSE);
            $table->dropdown(TRUE);
            $table->hashids(FALSE);
            $table->postmethod(TRUE);
            $table->title(TRUE);
            $table->tbtarget("tb_status");
            $table->menu(array('Edit' => array('GET', site_url() . 'licensing/form/first', '1', 'fa fa-edit'),
                'Preview' => array('GET', site_url() . 'licensing/preview', '1', 'fa fa-eye')));
            $arrdata = array('tabel' => $table->generate($query),
                'judul' => 'Data Permohonan');
            if ($this->input->post("data-post"))
                return $table->generate($query);
            else
                return $arrdata;
        }
	}

}