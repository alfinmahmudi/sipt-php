<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$autoload['packages'] = array();
$autoload['libraries'] = array('database','parser','newsession','session','newtable');
$autoload['helper'] = array('url','array','form','hashids','login','date');
$autoload['config'] = array('hashids');
$autoload['language'] = array();
$autoload['model'] = array('main','licensing/email_act');
