<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$route['default_controller'] = "home";
$route['internal'] = "login/internal";
// $route['verifikasi/(:any)/(:any)/(:any)'] = "prints/licensing/$1/$2/$3/aut";
$route['verifikasi/(:any)/(:any)/(:any)'] = "prints/direk/$1/$2/$3/aut";
$route['404_override'] = '';