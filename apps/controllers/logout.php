<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Logout extends CI_Controller{
	public function __construct() {
        parent::__construct();
    }
	
	public function index(){
		$role = $this->newsession->userdata('role');
		$this->newsession->sess_destroy();
		if($role == "05") {
			redirect(site_url('portal/news'));
		}else {
			redirect(site_url('internal'));
		}
		
		exit();
	}
}