<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Manual extends CI_Controller{
	var $ineng = "";
	
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }
	
	public function index(){
		echo "Forbidden";
	}
	
	public function get_data($isajax){
		$ret = "";
        if(strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            $ret = "NO";
        }
		if($ret == ""){
			$kd_izin = $this->input->post('dokumen');
			$nomor_aju = $this->input->post('nomor_aju');
			$arrsiupmb = array("3","4","5","6","13");
	        $arrpameran = array("15","16");
	        $arrstpw = array("17","18","19","20");
	        $field="";
	    	if (in_array($kd_izin, $arrsiupmb)) {
	    		$dbTable = "t_siupmb";
	    	} elseif ($kd_izin == "1") {
	    		$dbTable = "t_siujs";
	    	} elseif ($kd_izin == "2") {
	    		$dbTable = "t_siup4";
	    	} elseif ($kd_izin == "12") {
	    		$dbTable = "t_siup_agen";
	    	} elseif ($kd_izin == "7") {
	            $dbTable = "t_pkapt";
	        } elseif ($kd_izin == "8") {
	            $dbTable = "t_pgapt";
	        } elseif ($kd_izin == "9") {
	            $dbTable = "t_sppgapt";
	        } elseif ($kd_izin == "10") {
	            $dbTable = "t_sppgrapt";
	        } elseif ($kd_izin == "11") {
	    		$dbTable = "t_siupbb";
	    	} elseif ($kd_izin == "14") {
	            $dbTable = "t_garansi";
	        } elseif (in_array($kd_izin, $arrpameran)) {
	            $dbTable = "t_pameran";
	        } elseif (in_array($kd_izin, $arrstpw)) {
	            $dbTable = "t_stpw";
	        }
            $this->load->model('manual_act'); 
            $ret = $this->manual_act->get_data($dbTable, $kd_izin, $nomor_aju);
		}
		if ($isajax != "ajax") {
            redirect(base_url());
            exit();
        }
        echo $ret;
	}	
}