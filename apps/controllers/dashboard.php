<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends CI_Controller{
	
	var $content = "";
	var $breadcumbs = "";
	var $ineng = "";
	
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }
	
	public function index(){
		$this->load->model('dashboard_act');
		$arrdata = $this->dashboard_act->get_data();
		$this->content = (!$this->content) ? $this->load->view($this->ineng.'/backend/dashboard/default', $arrdata, true) : $this->content;
		$data = $this->main->set_content('backend', $this->breadcumbs, $this->content);
		$this->parser->parse($this->ineng.'/backend/dashboard/home', $data);
	}

	public function famous(){
		$this->load->model('dashboard_act');
		$arrdata = $this->dashboard_act->get_pie();
		$this->content = $this->load->view($this->ineng.'/backend/dashboard/famous', $arrdata, true);
		$this->index();
	}

}