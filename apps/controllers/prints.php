<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Prints extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('site_lang')) {
            $this->lang->load('message', $this->session->userdata('site_lang'));
            $this->ineng = $this->session->userdata('site_lang');
        } else {
            $this->lang->load('message', 'id');
            $this->ineng = "id";
        }
    }

    public function index() {
        echo "Forbidden";
    }

    public function direk($dir, $izin, $id) {
        $dir = hashids_decrypt($dir, _HASHIDS_, 9);
        $kd_izin = hashids_decrypt($izin, _HASHIDS_, 9);
        $id = hashids_decrypt($id, _HASHIDS_, 9);
        $get_izin = "SELECT TOP 1 a.no_izin, a.tgl_izin, a.tgl_izin_exp
                  FROM VIEW_PERMOHONAN a
                  WHERE a.kd_izin = '" . $kd_izin . "' AND a.id = '" . $id . "' AND a.status_id = '1000'";
        $arrizin = $this->db->query($get_izin)->row_array();
        $get_dok = "SELECT folder, nama_file
                FROM t_upload a 
                WHERE a.nomor = '" . $arrizin['no_izin'] . "' AND a.tgl_dok = '" . $arrizin['tgl_izin'] . "' AND a.tgl_exp = '" . $arrizin['tgl_izin_exp'] . "'";
        $dok = $this->db->query($get_dok)->row_array();
        $file = $dok['folder'] . '/' . $dok['nama_file'];
        $filename = $dok['nama_file'];
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="fffss' . $filename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile($file);
    }

    public function licensing($dir, $izin, $id, $act = "") {
        if ($act != 'aut') {
            //print_r(APPPATH);die();
            if (!$this->newsession->userdata('_LOGGED')) {
                redirect(site_url('internal'));
                exit();
            }
        }

        /*
          @ID @Nama Izin
          1	Surat Izin Usaha Jasa Survey
          2	Surat Izin Usaha Perusahaan Perantara Perdangangan properti (SIU-P4)
          3	Surat Izin Usaha Perdagangan  Minuman Beralkohol - Importir
          4	Surat Izin Usaha Perdagangan  Minuman Beralkohol - Distributor
          5	Surat Izin Usaha Perdagangan  Minuman Beralkohol - Sub Distributor
          6	Surat Keterangan Pengecer Minuman Beralkohol Golongan A (SKP-A)
          7	Pedagang Kayu Antar Pulau Terdaftar (PKAPT)
          8	Pedagang Gula Antar Pulau Terdaftar (PGAPT)
          9	Surat Persetujuan Perdagangan Gula Antar Pulau (SPPGAP)
          10	Surat Persetujuan Perdagangan Gula Rafinasi Antar Pulau (SPPGRAP)
          11	Surat Izin Usaha Perdagangan Bahan Berbahaya (SIUP B2) sebagai DT-B2
          12	Surat Tanda Pendaftaran Agen atau Distributor Barang atau Jasa Dalam atau Luar Negeri
          13	Surat Keterangan Penjual Langsung Minuman Beralkohol Golongan A (SKPL-A)
         */
        $dir = '0' . hashids_decrypt($dir, _HASHIDS_, 9);
        $izin = hashids_decrypt($izin, _HASHIDS_, 9);
        //    die($dir.'dsads'.$izin);
        // die($dir.$izin);
        //print_r($_GET['catatan']);die();
        //print_r($dir.$izin);die();
        $arrsiupmb = array(3, 4, 5, 6, 13);
        if (in_array($izin, $arrsiupmb)) {
            $this->load->model('print/siupmb_cetak_act');
            $arrdata = $this->siupmb_cetak_act->set_prints($izin, $id);
            $arrmagin = array();
            ob_start();
            header('Content-type: application/pdf');
            // if ($act != "") {
            //     $classfile = APPPATH . "views/template/penjelasan/" . $dir . "/13.php";
            // } else {
            $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
            // }
            require_once($classfile);
            $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
            $objpdf->generate($arrdata);
        }
        if ($izin == '22') {
            $this->load->model('print/sipdis_cetak_act');
            $arrdata = $this->sipdis_cetak_act->set_prints($izin, $id);
            $arrmagin = array();
            ob_start();
            header('Content-type: application/pdf');
            // if ($act != "") {
            //     $classfile = APPPATH . "views/template/penjelasan/" . $dir . "/13.php";
            // } else {
            $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
            // }
            require_once($classfile);
            $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
            $objpdf->generate($arrdata);
        }
        if ($izin == "2") {
            $this->load->model('print/siup4_cetak_act');
            $arrdata = $this->siup4_cetak_act->set_prints($izin, $id);
            $arrmagin = array();
            ob_start();
            header('Content-type: application/pdf');
            // if ($act != "") {
            //     $classfile = APPPATH . "views/template/penjelasan/01/12.php";
            // } else {
            $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
            // }
            require_once($classfile);
            $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
            $objpdf->generate();
        }
        if ($izin == "1") {
            $this->load->model('print/siujs_cetak_act');
            $arrdata = $this->siujs_cetak_act->set_prints($izin, $id);
            $arrmagin = array();
            ob_start();
            header('Content-type: application/pdf');
            /* if ($act != "" ) {
              $classfile = APPPATH . "views/template/penjelasan/" . $dir . "/" . $izin . ".php";
              } else { */
            $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
            // }
            require_once($classfile);
            $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
            $objpdf->generate();
        }
        if ($izin == "11") {
            $this->load->model('print/siupbb_cetak_act');
            $arrdata = $this->siupbb_cetak_act->set_prints($izin, $id);
            $arrmagin = array();
            ob_start();
            header('Content-type: application/pdf');
            $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
            require_once($classfile);
            $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
            $objpdf->generate();
        }
        if ($izin == "12") {
            $this->load->model('print/siupagen_cetak_act');
            $arrdata = $this->siupagen_cetak_act->set_prints($izin, $id);
            $arrmagin = array();
            ob_start();

            // if ($act != "") {
            //     $classfile = APPPATH . "views/template/penjelasan/" . $dir . "/" . $izin . ".php";
            // } else {
            $id = hashids_decrypt($id, _HASHIDS_, 9);
            // if ($id == '1302') {
                header('Content-type: application/pdf');
                $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                $arrdata['arrdata'] = $arrdata;
                $this->load->view("template/" . $dir . "/" . $izin . ".php", $arrdata);
            // } else {
            //     header('Content-type: application/pdf');
            //     $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
            //     require_once($classfile);
            //     $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
            //     $objpdf->generate();
            // }
            // }
        }
        if ($izin == "14") {
            $this->load->model('print/garansi_cetak_act');
            $arrdata = $this->garansi_cetak_act->set_prints($izin, $id);
            $arrmagin = array();
            ob_start();
            header('Content-type: application/pdf');
            $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
            require_once($classfile);
            $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
            $objpdf->generate();
        }


        $arrstpw = array(17, 18, 19, 20, 21);
        if (in_array($izin, $arrstpw)) {
            $this->load->model('print/stpw_cetak_act');
            $arrdata = $this->stpw_cetak_act->set_prints($izin, $id);
            $arrmagin = array();
            ob_start();
            header('Content-type: application/pdf');
            $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
            require_once($classfile);
            $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
            $objpdf->generate();
        }
        if ($izin == "15") {
            $this->load->model('print/pameran_cetak_act');
            $arrdata = $this->pameran_cetak_act->set_prints($izin, $id);
            $arrmagin = array();
            ob_start();
            header('Content-type: application/pdf');
            $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
            require_once($classfile);
            $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
            $objpdf->generate();
        }
        if ($izin == "16") {
            $this->load->model('print/pameranp_cetak_act');
            $arrdata = $this->pameranp_cetak_act->set_prints($izin, $id);
            $arrmagin = array();
            ob_start();
            header('Content-type: application/pdf');
            $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
            require_once($classfile);
            $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
            $objpdf->generate();
        }
        if ($izin == "10") {
            $this->load->model('print/sppgrap_cetak_act');
            $arrdata = $this->sppgrap_cetak_act->set_prints($izin, $id);
            $arrmargin = array();
            ob_start();
            header('Content-type: application/pdf');
            if ($arrdata['row']['tipe_permohonan'] == '02') {
                $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . "_03.php";
            } else {
                $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
            }
            require_once($classfile);
            $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
            $objpdf->generate();
        }
        if ($izin == "7") {
            $this->load->model('print/pkapt_cetak_act');
            $arrdata = $this->pkapt_cetak_act->set_prints($izin, $id);
            $arrmagin = array();
            ob_start();
            header('Content-type: application/pdf');
            $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
            require_once($classfile);
            $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
            $objpdf->generate();
            
            
        }
        /* if($izin == "19" ){
          $this->load->model('print/stpw_cetak_act');
          $arrdata = $this->stpw_cetak_act->set_prints($izin, $id);
          $arrmagin = array();
          ob_start();
          header('Content-type: application/pdf');
          $classfile = APPPATH."views/template/".$dir."/".$izin.".php";
          require_once($classfile);
          $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
          $objpdf->generate();
          }
          if($izin == "18" ){
          $this->load->model('print/stpw_cetak_act');
          $arrdata = $this->stpw_cetak_act->set_prints($izin, $id);
          $arrmagin = array();
          ob_start();
          header('Content-type: application/pdf');
          $classfile = APPPATH."views/template/".$dir."/".$izin.".php";
          require_once($classfile);
          $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
          $objpdf->generate();
          }
          if($izin == "18" ){
          $this->load->model('print/stpw_cetak_act');
          $arrdata = $this->stpw_cetak_act->set_prints($izin, $id);
          $arrmagin = array();
          ob_start();
          header('Content-type: application/pdf');
          $classfile = APPPATH."views/template/".$dir."/".$izin.".php";
          require_once($classfile);
          $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
          $objpdf->generate();
          }if($izin == "20" ){
          $this->load->model('print/stpw_cetak_act');
          $arrdata = $this->stpw_cetak_act->set_prints($izin, $id);
          $arrmagin = array();
          ob_start();
          header('Content-type: application/pdf');
          $classfile = APPPATH."views/template/".$dir."/".$izin.".php";
          require_once($classfile);
          $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
          $objpdf->generate();
          } */
    }

}
