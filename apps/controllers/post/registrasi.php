<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Registrasi extends CI_Controller{
	var $ineng = "";
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }
	
	public function index(){
		echo "Forbidden";
	}

	public function regis_act($isajax){
			if(!$this->newsession->userdata('_LOGGED')){	
				if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
					redirect(base_url());
					exit();
				}else{
					$keycode = $this->input->post('keycode');
					$this->load->model('registrasi_act');
					$ret = $this->registrasi_act->set_registrasi($isajax, $keycode);
				}
				if($isajax!="ajax"){
					redirect(base_url());
				}
				echo $ret;
			}
		}

	public function preview_act($isajax){
		if($this->newsession->userdata('_LOGGED')){
				if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
					redirect(base_url());
					exit();
				}else{
					$this->load->model('registrasi_act');
					$ret = $this->registrasi_act->set_preview($isajax);
				}
				if($isajax!="ajax"){
					redirect(base_url());
				}
				echo $ret;
			}
	}

	public function pembaharuan_act($isajax){
		if($this->newsession->userdata('_LOGGED')){
				if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
					redirect(base_url());
					exit();
				}else{
					$this->load->model('registrasi_act');
					$ret = $this->registrasi_act->set_pembaharuan($isajax);
				}
				if($isajax!="ajax"){
					redirect(base_url());
				}
				echo $ret;
			}
	}

	public function resend_mail($isajax){
		if($this->newsession->userdata('_LOGGED')){
				if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
					redirect(base_url());
					exit();
				}else{
					$this->load->model('registrasi_act');
					$ret = $this->registrasi_act->set_mail($isajax);
				}
				if($isajax!="ajax"){
					redirect(base_url());
				}
				echo $ret;
			}
	}

	public function resend_tolak($isajax){
		if($this->newsession->userdata('_LOGGED')){
				if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
					redirect(base_url());
					exit();
				}else{
					$this->load->model('registrasi_act');
					$ret = $this->registrasi_act->set_mail_tolak($isajax);
				}
				if($isajax!="ajax"){
					redirect(base_url());
				}
				echo $ret;
			}
	}

	public function pemroses_act($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
				if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
					redirect(base_url());
					exit();
				}else{
					$this->load->model('registrasi_act');
					if ($act == 'save') {
						$ret = $this->registrasi_act->set_pemroses($isajax);
					}elseif ($act == 'update') {
						$ret = $this->registrasi_act->set_edit($isajax);
					}
				}
				if($isajax!="ajax"){
					redirect(base_url());
				}
				echo $ret;
			}
	}

	public function get_izin(){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('registrasi_act');
			$ret = $this->registrasi_act->get_izin();
			echo $ret;
		}
	}
}
?>