<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reference extends CI_Controller{
	var $ineng = "";
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }
	
	public function index(){
		echo "Forbidden";
	}
	
	public function users_ac($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == '99'){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('reference/users_act');
				$ret = $this->users_act->set_user($act, $isajax);
				
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}

	public function pelabuhan($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == '99'){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('reference/pelabuhan_act');
				$ret = $this->pelabuhan_act->set_pelabuhan($act, $isajax);
				
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}

	public function popup($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == '99'){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('reference/popup_act');
				$ret = $this->popup_act->set_popup($act, $isajax);
				
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}

	public function produk($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == '99'){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('reference/produk_act');
				$ret = $this->produk_act->set_produk($act, $isajax);
				
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}
		
	public function signatory_ac($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == '99'){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('reference/signatory_act');
				$ret = $this->signatory_act->set_signatory($act, $isajax);
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}
	
	public function npwp_ac($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && ($this->newsession->userdata('role') == '99' || $this->newsession->userdata('role') == '01' )){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('reference/npwp_act');
				$ret = $this->npwp_act->set_npwp($act, $isajax);
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}
	
	public function formating_ac($act, $isajax){
		if($this->newsession->userdata('_LOGGED') && ($this->newsession->userdata('role') == '99' || $this->newsession->userdata('role') == '01' )){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('reference/formating_act');
				$ret = $this->formating_act->set_format($act, $isajax);
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}
	
}