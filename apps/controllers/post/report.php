<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report extends CI_Controller{
	var $ineng = "";
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }
	
	public function index(){
		echo "Forbidden";
	}
	
	public function jenis_pelapor_pulau(){
		if ($this->newsession->userdata('_LOGGED')) {
            /* [direktorat] => 02
              [dokumen] => 4
              [jenis] => 01 */
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $type = $this->input->post('jenis');
                //echo "MSG||YES||".site_url().'licensing/form/first/'.$dir.'/'.hashids_encrypt($doc, _HASHIDS_, 6).'/'.hashids_encrypt($type, _HASHIDS_, 6);
                echo "MSG||YES||" . site_url() . 'pulau/add_report/first/' . $type;
            }
        }
	}
	
	
	public function siupmb_act($step, $act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('report/siupmb_act');
				if($step == "pengadaanmb"){ 
					$ret = $this->siupmb_act->set_pengadaanmb($act, $isajax);
				}else if($step == "penyaluranmb"){ 
					$ret = $this->siupmb_act->set_penyaluranmb($act, $isajax);
				}
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}

	public function sppgrapt_act($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('report/sppgrapt_act');
				$ret = $this->sppgrapt_act->set_data($act, $isajax);
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}

	public function pelaporan_act($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('report/pelaporan_act');
				$ret = $this->pelaporan_act->set_data($act, $isajax);
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}	

	public function siujs_act($step, $act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('report/siujs_act');
				$ret = $this->siujs_act->set_lap_siujs($act, $isajax);
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}

	public function siup4_act($step, $act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('report/siup4_act');
				$ret = $this->siup4_act->set_lap_siup4($act, $isajax);
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}	
}
?>