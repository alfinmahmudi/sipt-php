<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Document extends CI_Controller{
	public function __construct() {
        parent::__construct();
    }
	
	public function index(){
		echo "Forbidden";
	}
	
	public function supporting_act($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('document/supporting_act');
				$ret = $this->supporting_act->set_document($act, $isajax);
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}
	
	public function get_requirements($doc,$isajax){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('licensing/siupmb_act');
			$arrdata = $this->siupmb_act->get_requirements($doc);
			$jml = count($arrdata);
			for($i = 0; $i < $jml; $i++){
				echo $ret = '<tr><td width="20%"><input type="hidden" id="id[]" name="REQUIREMENTS[id][]" value="" readonly="readonly"/><input type="hidden" name="REQUIREMENTS[dok_id][]" value="'.$arrdata[$i]['dok_ids'].'" readonly="readonly"/><input type="hidden" name="REQUIREMENTS[upload_id][]" '. ($arrdata[$i]['tipe'] =='1' ? 'wajib="yes"':""). ' value="'.$arrdata[$i]['id_upload'].'" readonly="readonly"/>'.$arrdata[$i]['nomor'].'</td><td width="35%"><center>'.$arrdata[$i]['penerbit_dok'].'</center></td><td width="20%"><center>'.$arrdata[$i]['tgl_dok'].'</center></td><td width="20%"><center>'.$arrdata[$i]['tgl_exp'].'</center></td><td width="10%"><center>'.$arrdata[$i]['files'].'</center></td></tr>';
			}
		}
	}
	
	
}