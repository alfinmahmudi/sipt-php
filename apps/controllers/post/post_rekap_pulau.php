<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Post_rekap_pulau extends CI_Controller {

    var $ineng = "";

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('site_lang')) {
            $this->lang->load('message', $this->session->userdata('site_lang'));
            $this->ineng = $this->session->userdata('site_lang');
        } else {
            $this->lang->load('message', 'id');
            $this->ineng = "id";
        }
    }

    public function index() {
        echo "Forbidden";
    }

    public function excel() {
        if ($this->newsession->userdata('_LOGGED')) {
            ini_set('memory_limit', '512M');
            $this->load->model('reference/rekap_pulau_act');
            $data = $this->rekap_pulau_act->excel_antrpl();
            $this->load->library('newphpexcel');
            $this->newphpexcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $this->newphpexcel->getDefaultStyle()->getFont()->setName('Calibri')->setSize(10);
            $arrheader = array('font' => array('bold' => true),
                'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
                'fill' => array('type' => (PHPExcel_Style_Fill::FILL_SOLID),
                    'color' => array('rgb' => 'C0C0C0')));
            $kolom = array('B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1', 'J1', 'K1', 'L1', 'M1', 'N1', 'O1', 'P1', 'Q1', 'R1', 'S1', 'T1', 'U1', 'V1', 'W1', 'X1', 'Y1', 'Z1', 'AA1', 'AB1', 'AC1', 'AD1', 'AE1', 'AF1', 'AG1', 'AH1', 'AI1', 'AJ1', 'AK1', 'AL1', 'AM1', 'AN1', 'AO1', 'AP1', 'AQ1', 'AR1', 'AS1', 'AT1', 'AU1', 'AV1', 'AW1', 'AX1', 'AY1', 'AZ1', 'BA1', 'BB1', 'BC1', 'BD1', 'BE1', 'BF1', 'BG1', 'BH1', 'BI1', 'BJ1', 'BK1', 'BL1', 'BM1', 'BN1', 'BO1', 'BP1', 'BQ1', 'BR1', 'BS1', 'BT1', 'BU1', 'BV1', 'BW1', 'BX1', 'BY1', 'BZ1');
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A1', 'No');
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->setActiveSheetIndex(0)
                        ->setCellValue($kolom[$i], $data['header'][$i]);
            }

            $this->newphpexcel->getActiveSheet()
                    ->getStyle($this->newphpexcel->getActiveSheet()->calculateWorksheetDimension())
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->newphpexcel->getActiveSheet()->getStyle('A1')->applyFromArray($arrheader);
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->getActiveSheet()->getStyle($kolom[$i])->applyFromArray($arrheader);
            }

            $this->newphpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            for ($i = 0; $i < count($data['header']); $i++) {
                $subkolom = substr($kolom[$i], 0, 1);
                $this->newphpexcel->getActiveSheet()->getColumnDimension($subkolom)->setWidth(25);
            }
            $i = 2;
            $no = 1;
            foreach ($data['detil'] as $row) {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no);
                for ($z = 0; $z < count($data['header']); $z++) {
                    if ($data['header'][$z] != 'id') {
                        $subkolom = substr($kolom[$z], 0, -1);
                        $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row[$data['header'][$z]]);
                    }
                }
                $fl_det = count($data['header']);
                for ($u = 0; $u < count($datadet['header']); $u++) {
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($kolom[$fl_det], $datadet['header'][$u]);
                    $fl_det++;
                }
                $datadet = $this->rekap_pulau_act->get_detil($row['id'], $data['hs']);
                foreach ($datadet['detil'] as $det) {
                    $col = count($data['header']);
                    for ($b = 0; $b < count($datadet['header']); $b++) {
                        for ($z = 0; $z < count($data['header']); $z++) {
                            if (in_array($data['header'][$z], array("jenis_pelapor", "npwp_pengirim", "nama_pengirim", "tp_perusahaan_pengirim", "alamat_pengirim", "prop_pengirim", "kab_pengirim", "kec_pengirim", "kel_pengirim", "telp", "email", "fax", "npwp_penerima", "nama_penerima", "tp_perusanaan_penerima", "alamat_penerima", "prop_penerima", "kab_penerima", "kec_penerima", "kel_penerima", "telp_penerima", "fax_penerima", "email_penerima", "pel_asal", "pel_tujuan", "jenis_asal")) == true) {
                                $subkolom = substr($kolom[$z], 0, -1);
                                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row[$data['header'][$z]]);
                            }
                        }
                        if ($datadet['header'][$b] == 'HS_URAI') {
                            $det['HS_URAI'] = trim(str_replace("<b>=></b>", "", $det['HS_URAI']));
                        }
                        $subnext = substr($kolom[$col], 0, -1);
                        $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subnext . $i, $det[$datadet['header'][$b]]);
                        $col++;
                    }
                    $i++;
                }
                $no++;
            }
            ob_clean();
            $file = "SIPT_Rekap_" . date('YmdHis') . ".xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment;filename=$file");
            header("Cache-Control: max-age=0");
            header("Pragma: no-cache");
            header("Expires: 0");
            $objWriter = PHPExcel_IOFactory::createWriter($this->newphpexcel, 'Excel5');
            $objWriter->save('php://output');
            exit();
        }
    }

}

?>