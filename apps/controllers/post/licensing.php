<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Licensing extends CI_Controller {

    var $ineng = "";

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('site_lang')) {
            $this->lang->load('message', $this->session->userdata('site_lang'));
            $this->ineng = $this->session->userdata('site_lang');
        } else {
            $this->lang->load('message', 'id');
            $this->ineng = "id";
        }
    }

    public function index() {
        echo "Forbidden";
    }

    public function requirements() {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/licensing_act');
            $arrdata = $this->licensing_act->view();
            echo $this->load->view($this->ineng . '/backend/licensing/requirements-result', $arrdata, true);
        }
    }

    public function pulau($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('report/pulau_act');
                if ($step == "first") {
                    $ret = $this->pulau_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->pulau_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    //  print_r($_POST);die();
                    $ret = $this->pulau_act->set_third($act, $isajax);
                } else if ($step == "komoditi") {
                    //  print_r($_POST);die();
                    $ret = $this->pulau_act->set_komoditi($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function choice($pelaporan = '') {
        //print_r($_POST);die();
        if ($this->newsession->userdata('_LOGGED')) {
            /* [direktorat] => 02
              [dokumen] => 4
              [jenis] => 01 */
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $doc = $this->input->post('dokumen');
                $ex = explode("|", $doc);
                $type = $this->input->post('jenis');
                //echo "MSG||YES||".site_url().'licensing/form/first/'.$dir.'/'.hashids_encrypt($doc, _HASHIDS_, 6).'/'.hashids_encrypt($type, _HASHIDS_, 6);
                if ($pelaporan == 'pelaporan') {
                    echo "MSG||YES||" . site_url() . 'pelaporan/form/first/' . $ex[0] . '/' . $ex[1] . '/' . $type;
                } else {
                    echo "MSG||YES||" . site_url() . 'licensing/form/first/' . $ex[0] . '/' . $ex[1] . '/' . $type;
                }
            }
        }
    }

    public function siupmb_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/siupmb_act');
                if ($step == "first") {
                    $ret = $this->siupmb_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->siupmb_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    //	print_r($_POST);die();
                    $ret = $this->siupmb_act->set_third($act, $isajax);
                } else if ($step == "fourth") {
                    $ret = $this->siupmb_act->set_fourth($act, $isajax);
                } else if ($step == "pelaporan") {
                    $this->load->model('pelaporan/pelaporan_act');
                    $ret = $this->pelaporan_act->set_pelaporan($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function sipdis_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/sipdis_act');
                if ($step == "first") {
                    $ret = $this->sipdis_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->sipdis_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    //  print_r($_POST);die();
                    $ret = $this->sipdis_act->set_third($act, $isajax);
                } else if ($step == "fourth") {
                    $ret = $this->sipdis_act->set_fourth($act, $isajax);
                } else if ($step == "pelaporan") {
                    $this->load->model('pelaporan/pelaporan_act');
                    $ret = $this->pelaporan_act->set_pelaporan($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function label_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/label_act');
                if ($step == "first") {
                    $ret = $this->label_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->label_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    //  print_r($_POST);die();
                    $ret = $this->label_act->set_third($act, $isajax);
                } else if ($step == "fourth") {
                    $ret = $this->label_act->set_fourth($act, $isajax);
                } else if ($step == "pelaporan") {
                    $this->load->model('pelaporan/pelaporan_act');
                    $ret = $this->pelaporan_act->set_pelaporan($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function siup4_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/siup4_act');
                if ($step == "first") {
                    $ret = $this->siup4_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->siup4_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    $ret = $this->siup4_act->set_third($act, $isajax);
                } else if ($step == "fourth") {
                    $ret = $this->siup4_act->set_fourth($act, $isajax);
                } else if ($step == "pelaporan") {
                    $this->load->model('pelaporan/pelaporan_act');
                    $ret = $this->pelaporan_act->set_pelaporan($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function siujs_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/siujs_act');
                if ($step == "first") {
                    $ret = $this->siujs_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->siujs_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    $ret = $this->siujs_act->set_third($act, $isajax);
                } else if ($step == "fourth") {
                    $ret = $this->siujs_act->set_fourth($act, $isajax);
                } else if ($step == "fifth") {
                    $ret = $this->siujs_act->set_fifth($act, $isajax);
                } else if ($step == "sixth") {
                    $ret = $this->siujs_act->set_sixth($act, $isajax);
                } else if ($step == "pelaporan") {
                    $this->load->model('pelaporan/pelaporan_act');
                    $ret = $this->pelaporan_act->set_pelaporan($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function siupagen_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/siupagen_act');
                if ($step == "first") {
                    $ret = $this->siupagen_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->siupagen_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    $ret = $this->siupagen_act->set_third($act, $isajax);
                } else if ($step == "fourth") {
                    $ret = $this->siupagen_act->set_fourth($act, $isajax);
                } else if ($step == "fifth") {
                    $ret = $this->siupagen_act->set_fifth($act, $isajax);
                } else if ($step == "pelaporan") {
                    $this->load->model('pelaporan/pelaporan_act');
                    $ret = $this->pelaporan_act->set_pelaporan($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function siupbb_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/siupbb_act');
                if ($step == "first") {
                    $ret = $this->siupbb_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->siupbb_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    $ret = $this->siupbb_act->set_third($act, $isajax);
                } else if ($step == "fourth") {
                    $ret = $this->siupbb_act->set_fourth($act, $isajax);
                } else if ($step == "pelaporan") {
                    $this->load->model('pelaporan/pelaporan_act');
                    $ret = $this->pelaporan_act->set_pelaporan($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function pgapt_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/pgapt_act');
                if ($step == "first") {
                    $ret = $this->pgapt_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->pgapt_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    $ret = $this->pgapt_act->set_third($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function sppgrap_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/sppgrap_act');
                if ($step == "first") {
                    $ret = $this->sppgrap_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->sppgrap_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    $ret = $this->sppgrap_act->set_third($act, $isajax);
                } else if ($step == "produsen") {
                    $ret = $this->sppgrap_act->set_produsen($act, $isajax);
                } else if ($step == "po") {
                    $ret = $this->sppgrap_act->set_po($act, $isajax);
                } else if ($step == "distributor") {
                    $ret = $this->sppgrap_act->set_distributor($act, $isajax);
                } else if ($step == "fourth") {
                    $ret = $this->sppgrap_act->set_fourth($act, $isajax);
                } else if ($step == "pelaporan") {
                    $this->load->model('pelaporan/pelaporan_act');
                    $ret = $this->pelaporan_act->set_pelaporan($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function pkapt_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/pkapt_act');
                if ($step == "first") {
                    $ret = $this->pkapt_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->pkapt_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    $ret = $this->pkapt_act->set_third($act, $isajax);
                } else if ($step == "pelaporan") {
                    $this->load->model('pelaporan/pelaporan_act');
                    $ret = $this->pelaporan_act->set_pelaporan($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function sppgap_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/sppgap_act');
                if ($step == "first") {
                    $ret = $this->sppgap_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->sppgap_act->set_second($act, $isajax);
                } else if ($step == "produsen") {
                    $ret = $this->sppgap_act->set_produsen($act, $isajax);
                } else if ($step == "distributor") {
                    $ret = $this->sppgap_act->set_distributor($act, $isajax);
                } else if ($step == "fourth") {
                    $ret = $this->sppgap_act->set_fourth($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function pameran_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/pameran_act');
                if ($step == "first") {
                    $ret = $this->pameran_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->pameran_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    $ret = $this->pameran_act->set_third($act, $isajax);
                } else if ($step == "fourth") {
                    $ret = $this->pameran_act->set_fourth($act, $isajax);
                } else if ($step == "pelaporan") {
                    $this->load->model('pelaporan/pelaporan_act');
                    $ret = $this->pelaporan_act->set_pelaporan($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function stpw_act($step, $act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/stpw_act');
                if ($step == "first") {
                    $ret = $this->stpw_act->set_first($act, $isajax);
                } else if ($step == "second") {
                    $ret = $this->stpw_act->set_second($act, $isajax);
                } else if ($step == "third") {
                    $ret = $this->stpw_act->set_third($act, $isajax);
                } else if ($step == "fourth") {
                    $ret = $this->stpw_act->set_fourth($act, $isajax);
                } else if ($step == "fifth") {
                    $ret = $this->stpw_act->set_fifth($act, $isajax);
                } else if ($step == "pelaporan") {
                    $this->load->model('pelaporan/pelaporan_act');
                    $ret = $this->pelaporan_act->set_pelaporan($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function garansi_act($step, $act, $isajax, $s = null) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/garansi_act');
                if ($step == "first") {
                    $ret = $this->garansi_act->set_first($act, $isajax, $s);
                } else if ($step == "second") {
                    $ret = $this->garansi_act->set_second($act, $isajax, $s);
                } else if ($step == "third") {
                    $ret = $this->garansi_act->set_third($act, $isajax);
                } else if ($step == "fourth") {
                    $ret = $this->garansi_act->set_fourth($act, $isajax);
                } else if ($step == "fifth") {
                    $ret = $this->garansi_act->set_fifth($act, $isajax);
                } else if ($step == "importir") {
                    $ret = $this->garansi_act->set_importir($act, $isajax);
                } else if ($step == "pelaporan") {
                    $this->load->model('pelaporan/pelaporan_act');
                    $ret = $this->pelaporan_act->set_pelaporan($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function excel_bpk($isajax, $jns_brg, $kdprop = '', $kdkab = '', $kdkec = '', $kdkel = '', $bulan = '', $tahun = '') {
        if ($this->newsession->userdata('_LOGGED')) {
            ini_set('memory_limit', '512M');
            $this->load->model('reference/rekap_act');
            $data = $this->rekap_act->excel_bpk($isajax, $jns_brg, $kdprop, $kdkab, $kdkec, $kdkel, $bulan, $tahun);

            $this->load->library('newphpexcel');
            $this->newphpexcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $this->newphpexcel->getDefaultStyle()->getFont()->setName('Calibri')->setSize(10);
            $arrheader = array('font' => array('bold' => true),
                'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
                'fill' => array('type' => (PHPExcel_Style_Fill::FILL_SOLID),
                    'color' => array('rgb' => 'C0C0C0')));
            $kolom = array('B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1', 'J1', 'K1', 'L1', 'M1', 'N1', 'O1', 'P1', 'Q1', 'R1', 'S1', 'T1', 'U1', 'V1', 'W1', 'X1', 'Y1', 'Z1', 'AA1', 'AB1', 'AC1', 'AD1', 'AE1', 'AF1', 'AG1', 'AH1', 'AI1', 'AJ1', 'AK1', 'AL1', 'AM1', 'AN1', 'AO1', 'AP1', 'AQ1', 'AR1', 'AS1', 'AT1', 'AU1', 'AV1', 'AW1', 'AX1', 'AY1', 'BB1', 'BC1', 'BD1', 'BE1', 'BF1', 'BG1', 'BH1', 'BI1', 'BJ1', 'BK1', 'BL1', 'BM1', 'BN1', 'BO1', 'BP1', 'BQ1', 'BR1', 'BS1', 'BT1', 'BU1', 'BV1', 'BW1', 'BX1', 'BY1', 'BZ1');
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A1', 'No');
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->setActiveSheetIndex(0)
                        ->setCellValue($kolom[$i], $data['header'][$i]);
            }

            $this->newphpexcel->getActiveSheet()
                    ->getStyle($this->newphpexcel->getActiveSheet()->calculateWorksheetDimension())
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->newphpexcel->getActiveSheet()->getStyle('A1')->applyFromArray($arrheader);
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->getActiveSheet()->getStyle($kolom[$i])->applyFromArray($arrheader);
            }

            $this->newphpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            for ($i = 0; $i < count($data['header']); $i++) {
                $subkolom = substr($kolom[$i], 0, 1);
                $this->newphpexcel->getActiveSheet()->getColumnDimension($subkolom)->setWidth(25);
            }
            $i = 2;
            $no = 1;
            //print_r($data['detil']);die();
            foreach ($data['detil'] as $row) {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no);
                for ($z = 0; $z < count($data['header']); $z++) {
                    $subkolom = substr($kolom[$z], 0, -1);
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row['data'][$data['header'][$z]]);
                }
                foreach ($row['det'] as $det) {
                    $i++;
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i);
                    for ($k = 0; $k < count($data['header_ex']); $k++) {
                        $subkolom = substr($kolom[$k], 0, -1);
                        $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $det[$data['header_ex'][$k]]);
                    }
                }
                $i++;
                $no++;
            }
            // $i = 2; // for row that to detil from detil in excel
            // $ia = 3; // for row that to detil from detil_ex in excel
            // $no = 1;
            // $fl = 0; // temp for count how many colom for all array, to know where 
            //         // the detil_ex start I'm throw detail header in $data['header_ex'] 
            // $jml_dtl = 0;
            // // step : first loop the master and than nasted loop the detil.
            // foreach ($data['detil'] as $row) {
            //     $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no);
            //     for ($z = 0; $z < count($data['header']); $z++) {
            //         $subkolom = substr($kolom[$z], 0, -1);
            //         $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row[$data['header'][$z]]);
            //         $fl = $z;
            //     }
            //     $bn = (int)$fl - count($data['header_ex']) + 1;
            //     foreach ($data['detil_ex'][$jml_dtl] as $key) {//print_r($key[$data['header']['37']]); (thats how you get the value)
            //         for ($l = $bn; $l <= $fl; $l++) {
            //             $subkolom = substr($kolom[$l], 0, -1);
            //             $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $ia, $key[$data['header'][$l]]);
            //             //print_r($subkolom . $ia.'<br>');
            //         }
            //         $ia++;
            //     }
            //     $jml_ex = count($data['detil_ex'][$jml_dtl]);
            //     $fl_merge = $ia - 1;
            //     for ($z = 0; $z < $bn; $z++) {
            //         $subkolom = substr($kolom[$z], 0, -1);//print_r($subkolom . $i.':'.$subkolom . $fl_merge);die();
            //         $this->newphpexcel->setActiveSheetIndex(0)->mergeCells($subkolom . $i.':'.$subkolom . $fl_merge);
            //     }
            //     $i = $i + $jml_ex;
            //     $i++;
            //     $ia = $i + 1;
            //     $no++;
            //     $jml_dtl++;
            // }
            // // //print_r($data['detil_ex']);die();
            // // foreach ($data['detil_ex'] as $key => $value) {print_r($value);
            // //     for ($l = $bn; $l <= $fl; $l++) {
            // //         $subkolom = substr($kolom[$l], 0, -1);
            // //         $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $ia, $key[$data['header'][$l]]);
            // //         //print_r($subkolom . $ia.'<br>');
            // //     }
            // //     $ia++;
            // // }
            // //print_r($ia);
            // //die();  
            ob_clean();
            $file = "SIPT_Rekap_" . date('YmdHis') . ".xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment;filename=$file");
            header("Cache-Control: max-age=0");
            header("Pragma: no-cache");
            header("Expires: 0");
            $objWriter = PHPExcel_IOFactory::createWriter($this->newphpexcel, 'Excel5');
            $objWriter->save('php://output');
            exit();
        }
    }

    public function excel_bpk_prshn($isajax, $kdprop = '', $bulan = '', $tahun = '', $bulan_akhir = '', $jns_brg = '') {
        if ($this->newsession->userdata('_LOGGED')) {
            ini_set('memory_limit', '512M');
            $this->load->model('reference/rekap_act');
            $data = $this->rekap_act->excel_bpk_prshn($isajax, $kdprop, $bulan, $tahun, $bulan_akhir, $jns_brg);

            $this->load->library('newphpexcel');
            $this->newphpexcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $this->newphpexcel->getDefaultStyle()->getFont()->setName('Calibri')->setSize(10);
            $arrheader = array('font' => array('bold' => true),
                'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
                'fill' => array('type' => (PHPExcel_Style_Fill::FILL_SOLID),
                    'color' => array('rgb' => 'C0C0C0')));
            $kolom = array('B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4', 'I4', 'J4', 'K4', 'L4', 'M4', 'N4', 'O4', 'P4', 'Q4', 'R4', 'S4', 'T4', 'U4', 'V4', 'W4', 'X4', 'Y4', 'Z4', 'AB4', 'AC4', 'AD4', 'AE4', 'AF4', 'AG4', 'AH4', 'AI4', 'AJ4', 'AK4', 'AL4', 'AM4', 'AN4', 'AO4', 'AP4', 'AQ4', 'AR4', 'AS4', 'AT4', 'AU4', 'AV4', 'AW4', 'AX4', 'AY4', 'AZ4', 'BB4', 'BC4', 'BD4', 'BE4', 'BF4', 'BG4', 'BH4', 'BI4', 'BJ4', 'BK4', 'BL4', 'BM4', 'BN4', 'BO4', 'BP4', 'BQ4', 'BR4', 'BS4', 'BT4', 'BU4', 'BV4', 'BW4', 'BX4', 'BY4', 'BZ4');

            $this->newphpexcel->getActiveSheet()->mergeCells('B3:B4');
            $this->newphpexcel->getActiveSheet()->mergeCells('A3:A4');
            $this->newphpexcel->getActiveSheet()->mergeCells('C3:C4');
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('D3', $tahun);
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('B1', $data['datax'][0]['uraian']);

            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A3', 'No');
            for ($i = 0; $i < count($data['header']); $i++) {
                if ($data['header'][$i] == 'Nama Perusahaan') {
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('B3', $data['header'][$i]);
                } elseif ($data['header'][$i] == 'Propinsi') {
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('C3', $data['header'][$i]);
                } else {
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($kolom[$i], $data['header'][$i]);
                }
            }
            $this->newphpexcel->getActiveSheet()->mergeCells('A1:' . substr($kolom[$i - 1], 0, -1) . '1');
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A1', 'Rekapitulasi Stok ' . $data['datax'][0]['uraian'] . ' Periode ' . $data['header'][2] . ' sampai ' . $data['header'][$i - 1] . ' ' . $tahun);
            if ($data['datax'][0]['id'] == '6') {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue(substr($kolom[$i - 1], 0, -1) . '2', '*Note : Dalam satuan Liter');
            } else {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue(substr($kolom[$i - 1], 0, -1) . '2', '*Note : Dalam satuan Ton');
            }
            $this->newphpexcel->getActiveSheet()->mergeCells('D3:' . substr($kolom[$i - 1], 0, -1) . '3');
            $this->newphpexcel->getActiveSheet()
                    ->getStyle($this->newphpexcel->getActiveSheet()->calculateWorksheetDimension())
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->newphpexcel->getActiveSheet()->getStyle('A3')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('B3')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('C3')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('D3:' . $kolom[$i - 1])->applyFromArray($arrheader);
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->getActiveSheet()->getStyle($kolom[$i])->applyFromArray($arrheader);
            }

            $this->newphpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            for ($i = 0; $i < count($data['header']); $i++) {
                $subkolom = substr($kolom[$i], 0, 1);
                $this->newphpexcel->getActiveSheet()->getColumnDimension($subkolom)->setWidth(25);
            }
            $i = 5;
            $no = 1;
            //print_r($data['detil']);die();
            foreach ($data['detil'] as $row) {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no);
                for ($z = 0; $z < count($data['header']); $z++) {
                    $subkolom = substr($kolom[$z], 0, -1);
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row[$data['header'][$z]]);
                }
                $i++;
                $no++;
            }
            // $i = 2; // for row that to detil from detil in excel
            // $ia = 3; // for row that to detil from detil_ex in excel
            // $no = 1;
            // $fl = 0; // temp for count how many colom for all array, to know where 
            //         // the detil_ex start I'm throw detail header in $data['header_ex'] 
            // $jml_dtl = 0;
            // // step : first loop the master and than nasted loop the detil.
            // foreach ($data['detil'] as $row) {
            //     $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no);
            //     for ($z = 0; $z < count($data['header']); $z++) {
            //         $subkolom = substr($kolom[$z], 0, -1);
            //         $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row[$data['header'][$z]]);
            //         $fl = $z;
            //     }
            //     $bn = (int)$fl - count($data['header_ex']) + 1;
            //     foreach ($data['detil_ex'][$jml_dtl] as $key) {//print_r($key[$data['header']['37']]); (thats how you get the value)
            //         for ($l = $bn; $l <= $fl; $l++) {
            //             $subkolom = substr($kolom[$l], 0, -1);
            //             $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $ia, $key[$data['header'][$l]]);
            //             //print_r($subkolom . $ia.'<br>');
            //         }
            //         $ia++;
            //     }
            //     $jml_ex = count($data['detil_ex'][$jml_dtl]);
            //     $fl_merge = $ia - 1;
            //     for ($z = 0; $z < $bn; $z++) {
            //         $subkolom = substr($kolom[$z], 0, -1);//print_r($subkolom . $i.':'.$subkolom . $fl_merge);die();
            //         $this->newphpexcel->setActiveSheetIndex(0)->mergeCells($subkolom . $i.':'.$subkolom . $fl_merge);
            //     }
            //     $i = $i + $jml_ex;
            //     $i++;
            //     $ia = $i + 1;
            //     $no++;
            //     $jml_dtl++;
            // }
            // // //print_r($data['detil_ex']);die();
            // // foreach ($data['detil_ex'] as $key => $value) {print_r($value);
            // //     for ($l = $bn; $l <= $fl; $l++) {
            // //         $subkolom = substr($kolom[$l], 0, -1);
            // //         $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $ia, $key[$data['header'][$l]]);
            // //         //print_r($subkolom . $ia.'<br>');
            // //     }
            // //     $ia++;
            // // }
            // //print_r($ia);
            // //die();  
            ob_clean();
            $file = "SIPT_Rekap_" . date('YmdHis') . ".xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment;filename=$file");
            header("Cache-Control: max-age=0");
            header("Pragma: no-cache");
            header("Expires: 0");
            $objWriter = PHPExcel_IOFactory::createWriter($this->newphpexcel, 'Excel5');
            $objWriter->save('php://output');
            exit();
        }
    }

    public function excel_bpkstra_user($isajax, $kdprop = '', $kdkab = '', $bulan = '', $tahun = '') {
        if ($this->newsession->userdata('_LOGGED')) {
            ini_set('memory_limit', '512M');
            $this->load->model('reference/rekap_act');
            $data = $this->rekap_act->excel_bpkstra($isajax, $kdprop, $kdkab, $bulan, $tahun);
            $this->load->library('newphpexcel');
            $this->newphpexcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $this->newphpexcel->getDefaultStyle()->getFont()->setName('Calibri')->setSize(10);
            $arrheader = array('font' => array('bold' => true),
                'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
                'fill' => array('type' => (PHPExcel_Style_Fill::FILL_SOLID),
                    'color' => array('rgb' => 'C0C0C0')));
            $kolom = array('B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1', 'J1', 'K1', 'L1', 'M1', 'N1', 'O1', 'P1', 'Q1', 'R1', 'S1', 'T1', 'U1', 'V1', 'W1', 'X1', 'Y1', 'Z1', 'AB1', 'AC1', 'AD1', 'AE1', 'AF1', 'AG1', 'AH1', 'AI1', 'AJ1', 'AK1', 'AL1', 'AM1', 'AN1', 'AO1', 'AP1', 'AQ1', 'AR1', 'AS1', 'AT1', 'AU1', 'AV1', 'AW1', 'AX1', 'AY1', 'AZ1', 'BB1', 'BC1', 'BD1', 'BE1', 'BF1', 'BG1', 'BH1', 'BI1', 'BJ1', 'BK1', 'BL1', 'BM1', 'BN1', 'BO1', 'BP1', 'BQ1', 'BR1', 'BS1', 'BT1', 'BU1', 'BV1', 'BW1', 'BX1', 'BY1', 'BZ1');
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A1', 'No');
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->setActiveSheetIndex(0)
                        ->setCellValue($kolom[$i], $data['header'][$i]);
            }

            $this->newphpexcel->getActiveSheet()
                    ->getStyle($this->newphpexcel->getActiveSheet()->calculateWorksheetDimension())
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->newphpexcel->getActiveSheet()->getStyle('A1')->applyFromArray($arrheader);
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->getActiveSheet()->getStyle($kolom[$i])->applyFromArray($arrheader);
            }

            $this->newphpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            for ($i = 0; $i < count($data['header']); $i++) {
                $subkolom = substr($kolom[$i], 0, 1);
                $this->newphpexcel->getActiveSheet()->getColumnDimension($subkolom)->setWidth(25);
            }
            $i = 2;
            $no = 1;
            //print_r($data['detil']);die();
            foreach ($data['detil'] as $row) {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no);
                for ($z = 0; $z < count($data['header']); $z++) {
                    $subkolom = substr($kolom[$z], 0, -1);
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row[$data['header'][$z]]);
                }
                $i++;
                $no++;
            }
            // $i = 2; // for row that to detil from detil in excel
            // $ia = 3; // for row that to detil from detil_ex in excel
            // $no = 1;
            // $fl = 0; // temp for count how many colom for all array, to know where 
            //         // the detil_ex start I'm throw detail header in $data['header_ex'] 
            // $jml_dtl = 0;
            // // step : first loop the master and than nasted loop the detil.
            // foreach ($data['detil'] as $row) {
            //     $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no);
            //     for ($z = 0; $z < count($data['header']); $z++) {
            //         $subkolom = substr($kolom[$z], 0, -1);
            //         $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row[$data['header'][$z]]);
            //         $fl = $z;
            //     }
            //     $bn = (int)$fl - count($data['header_ex']) + 1;
            //     foreach ($data['detil_ex'][$jml_dtl] as $key) {//print_r($key[$data['header']['37']]); (thats how you get the value)
            //         for ($l = $bn; $l <= $fl; $l++) {
            //             $subkolom = substr($kolom[$l], 0, -1);
            //             $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $ia, $key[$data['header'][$l]]);
            //             //print_r($subkolom . $ia.'<br>');
            //         }
            //         $ia++;
            //     }
            //     $jml_ex = count($data['detil_ex'][$jml_dtl]);
            //     $fl_merge = $ia - 1;
            //     for ($z = 0; $z < $bn; $z++) {
            //         $subkolom = substr($kolom[$z], 0, -1);//print_r($subkolom . $i.':'.$subkolom . $fl_merge);die();
            //         $this->newphpexcel->setActiveSheetIndex(0)->mergeCells($subkolom . $i.':'.$subkolom . $fl_merge);
            //     }
            //     $i = $i + $jml_ex;
            //     $i++;
            //     $ia = $i + 1;
            //     $no++;
            //     $jml_dtl++;
            // }
            // // //print_r($data['detil_ex']);die();
            // // foreach ($data['detil_ex'] as $key => $value) {print_r($value);
            // //     for ($l = $bn; $l <= $fl; $l++) {
            // //         $subkolom = substr($kolom[$l], 0, -1);
            // //         $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $ia, $key[$data['header'][$l]]);
            // //         //print_r($subkolom . $ia.'<br>');
            // //     }
            // //     $ia++;
            // // }
            // //print_r($ia);
            // //die();  
            ob_clean();
            $file = "SIPT_Rekap_" . date('YmdHis') . ".xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment;filename=$file");
            header("Cache-Control: max-age=0");
            header("Pragma: no-cache");
            header("Expires: 0");
            $objWriter = PHPExcel_IOFactory::createWriter($this->newphpexcel, 'Excel5');
            $objWriter->save('php://output');
            exit();
        }
    }

    public function excel_bpkstra($isajax, $kdprop = '', $kdkab = '', $bulan = '', $tahun = '', $bulan_akhir = '', $jns_brg = '') {
        if ($this->newsession->userdata('_LOGGED')) {
            ini_set('memory_limit', '512M');
            $this->load->model('reference/rekap_act');
            $data = $this->rekap_act->excel_bpkstra($isajax, $kdprop, $kdkab, $bulan, $tahun, $bulan_akhir, $jns_brg);

            $this->load->library('newphpexcel');
            $this->newphpexcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $this->newphpexcel->getDefaultStyle()->getFont()->setName('Calibri')->setSize(10);
            $arrheader = array('font' => array('bold' => true),
                'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
                'fill' => array('type' => (PHPExcel_Style_Fill::FILL_SOLID),
                    'color' => array('rgb' => 'C0C0C0')));
            $kolom = array('B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4', 'I4', 'J4', 'K4', 'L4', 'M4', 'N4', 'O4', 'P4', 'Q4', 'R4', 'S4', 'T4', 'U4', 'V4', 'W4', 'X4', 'Y4', 'Z4', 'AB4', 'AC4', 'AD4', 'AE4', 'AF4', 'AG4', 'AH4', 'AI4', 'AJ4', 'AK4', 'AL4', 'AM4', 'AN4', 'AO4', 'AP4', 'AQ4', 'AR4', 'AS4', 'AT4', 'AU4', 'AV4', 'AW4', 'AX4', 'AY4', 'AZ4', 'BB4', 'BC4', 'BD4', 'BE4', 'BF4', 'BG4', 'BH4', 'BI4', 'BJ4', 'BK4', 'BL4', 'BM4', 'BN4', 'BO4', 'BP4', 'BQ4', 'BR4', 'BS4', 'BT4', 'BU4', 'BV4', 'BW4', 'BX4', 'BY4', 'BZ4');

            $this->newphpexcel->getActiveSheet()->mergeCells('B3:B4');
            $this->newphpexcel->getActiveSheet()->mergeCells('A3:A4');
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('C3', $tahun);
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('B1', $data['datax'][0]['uraian']);
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A3', 'No');
            for ($i = 0; $i < count($data['header']); $i++) {
                if ($data['header'][$i] == 'Propinsi') {
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('B3', $data['header'][$i]);
                } else {
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($kolom[$i], $data['header'][$i]);
                }
            }
            $this->newphpexcel->getActiveSheet()->mergeCells('A1:' . substr($kolom[$i - 1], 0, -1) . '1');
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A1', 'Rekapitulasi Stok ' . $data['datax'][0]['uraian'] . ' Periode ' . $data['header'][1] . ' sampai ' . $data['header'][$i - 1] . ' ' . $tahun);
            if ($data['datax'][0]['id'] == '6') {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue(substr($kolom[$i - 1], 0, -1) . '2', '*Note : Dalam satuan Liter');
            } else {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue(substr($kolom[$i - 1], 0, -1) . '2', '*Note : Dalam satuan Ton');
            }
            $this->newphpexcel->getActiveSheet()->mergeCells('C3:' . substr($kolom[$i - 1], 0, -1) . '3');
            $this->newphpexcel->getActiveSheet()
                    ->getStyle($this->newphpexcel->getActiveSheet()->calculateWorksheetDimension())
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->newphpexcel->getActiveSheet()->getStyle('A3')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('B3')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('C3:' . $kolom[$i - 1])->applyFromArray($arrheader);
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->getActiveSheet()->getStyle($kolom[$i])->applyFromArray($arrheader);
            }

            $this->newphpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            for ($i = 0; $i < count($data['header']); $i++) {
                $subkolom = substr($kolom[$i], 0, 1);
                $this->newphpexcel->getActiveSheet()->getColumnDimension($subkolom)->setWidth(25);
            }
            $i = 5;
            $no = 1;
            //print_r($data['detil']);die();
            foreach ($data['detil'] as $row) {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no);
                for ($z = 0; $z < count($data['header']); $z++) {
                    $subkolom = substr($kolom[$z], 0, -1);
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row[$data['header'][$z]]);
                }
                $i++;
                $no++;
            }
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('B39', 'Total');
            for ($i = 2; $i <= count($data['header']); $i++) {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue(substr($kolom[$i - 1], 0, -1) . '39', '=sum(' . substr($kolom[$i - 1], 0, -1) . '5:' . substr($kolom[$i - 1], 0, -1) . '38)');
            }
            // $i = 2; // for row that to detil from detil in excel
            // $ia = 3; // for row that to detil from detil_ex in excel
            // $no = 1;
            // $fl = 0; // temp for count how many colom for all array, to know where 
            //         // the detil_ex start I'm throw detail header in $data['header_ex'] 
            // $jml_dtl = 0;
            // // step : first loop the master and than nasted loop the detil.
            // foreach ($data['detil'] as $row) {
            //     $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no);
            //     for ($z = 0; $z < count($data['header']); $z++) {
            //         $subkolom = substr($kolom[$z], 0, -1);
            //         $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row[$data['header'][$z]]);
            //         $fl = $z;
            //     }
            //     $bn = (int)$fl - count($data['header_ex']) + 1;
            //     foreach ($data['detil_ex'][$jml_dtl] as $key) {//print_r($key[$data['header']['37']]); (thats how you get the value)
            //         for ($l = $bn; $l <= $fl; $l++) {
            //             $subkolom = substr($kolom[$l], 0, -1);
            //             $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $ia, $key[$data['header'][$l]]);
            //             //print_r($subkolom . $ia.'<br>');
            //         }
            //         $ia++;
            //     }
            //     $jml_ex = count($data['detil_ex'][$jml_dtl]);
            //     $fl_merge = $ia - 1;
            //     for ($z = 0; $z < $bn; $z++) {
            //         $subkolom = substr($kolom[$z], 0, -1);//print_r($subkolom . $i.':'.$subkolom . $fl_merge);die();
            //         $this->newphpexcel->setActiveSheetIndex(0)->mergeCells($subkolom . $i.':'.$subkolom . $fl_merge);
            //     }
            //     $i = $i + $jml_ex;
            //     $i++;
            //     $ia = $i + 1;
            //     $no++;
            //     $jml_dtl++;
            // }
            // // //print_r($data['detil_ex']);die();
            // // foreach ($data['detil_ex'] as $key => $value) {print_r($value);
            // //     for ($l = $bn; $l <= $fl; $l++) {
            // //         $subkolom = substr($kolom[$l], 0, -1);
            // //         $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $ia, $key[$data['header'][$l]]);
            // //         //print_r($subkolom . $ia.'<br>');
            // //     }
            // //     $ia++;
            // // }
            // //print_r($ia);
            // //die();  
            ob_clean();
            $file = "SIPT_Rekap_" . date('YmdHis') . ".xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment;filename=$file");
            header("Cache-Control: max-age=0");
            header("Pragma: no-cache");
            header("Expires: 0");
            $objWriter = PHPExcel_IOFactory::createWriter($this->newphpexcel, 'Excel5');
            $objWriter->save('php://output');
            exit();
        }
    }

    public function excel_antrpl($isajax, $tgl_awal, $tgl_akhir, $tgl_awal2, $tgl_akhir2, $pel_muat, $pel_bongkar) {
        if ($this->newsession->userdata('_LOGGED')) {
            ini_set('memory_limit', '512M');
            $this->load->model('reference/rekap_act');
            $data = $this->rekap_act->excel_antrpl($isajax, $tgl_awal, $tgl_akhir, $tgl_awal2, $tgl_akhir2, $pel_muat, $pel_bongkar);

            $this->load->library('newphpexcel');
            $this->newphpexcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $this->newphpexcel->getDefaultStyle()->getFont()->setName('Calibri')->setSize(10);
            $arrheader = array('font' => array('bold' => true),
                'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
                'fill' => array('type' => (PHPExcel_Style_Fill::FILL_SOLID),
                    'color' => array('rgb' => 'C0C0C0')));
            $kolom = array('B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1', 'J1', 'K1', 'L1', 'M1', 'N1', 'O1', 'P1', 'Q1', 'R1', 'S1', 'T1', 'U1', 'V1', 'W1', 'X1', 'Y1', 'Z1', 'AB1', 'AC1', 'AD1', 'AE1', 'AF1', 'AG1', 'AH1', 'AI1', 'AJ1', 'AK1', 'AL1', 'AM1', 'AN1', 'AO1', 'AP1', 'AQ1', 'AR1', 'AS1', 'AT1', 'AU1', 'AV1', 'AW1', 'AX1', 'AY1', 'AZ1', 'BB1', 'BC1', 'BD1', 'BE1', 'BF1', 'BG1', 'BH1', 'BI1', 'BJ1', 'BK1', 'BL1', 'BM1', 'BN1', 'BO1', 'BP1', 'BQ1', 'BR1', 'BS1', 'BT1', 'BU1', 'BV1', 'BW1', 'BX1', 'BY1', 'BZ1');
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A1', 'No');
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->setActiveSheetIndex(0)
                        ->setCellValue($kolom[$i], $data['header'][$i]);
            }

            $this->newphpexcel->getActiveSheet()
                    ->getStyle($this->newphpexcel->getActiveSheet()->calculateWorksheetDimension())
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->newphpexcel->getActiveSheet()->getStyle('A1')->applyFromArray($arrheader);
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->getActiveSheet()->getStyle($kolom[$i])->applyFromArray($arrheader);
            }

            $this->newphpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            for ($i = 0; $i < count($data['header']); $i++) {
                $subkolom = substr($kolom[$i], 0, 1);
                $this->newphpexcel->getActiveSheet()->getColumnDimension($subkolom)->setWidth(25);
            }
            $i = 2;
            $no = 1;
            //print_r($data['detil']);die();
            foreach ($data['detil'] as $row) {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no);
                for ($z = 0; $z < count($data['header']); $z++) {
                    $subkolom = substr($kolom[$z], 0, -1);
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row[$data['header'][$z]]);
                    $this->newphpexcel->getActiveSheet()->getStyle('BI' . $i)->getAlignment()->setWrapText(true);
                    $this->newphpexcel->getActiveSheet()->getStyle('BJ' . $i)->getAlignment()->setWrapText(true);
                    $this->newphpexcel->getActiveSheet()->getStyle('BK' . $i)->getAlignment()->setWrapText(true);
                    $this->newphpexcel->getActiveSheet()->getColumnDimension('BI')->setWidth(10);
                    $this->newphpexcel->getActiveSheet()->getColumnDimension('BJ')->setWidth(100);
                    $this->newphpexcel->getActiveSheet()->getColumnDimension('BK')->setWidth(15);
                }
                $this->newphpexcel->getActiveSheet()
                        ->getStyle($this->newphpexcel->getActiveSheet()->calculateWorksheetDimension())
                        ->getAlignment()
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $i++;
                $no++;
            }

            ob_clean();
            $file = "SIPT_Rekap_" . date('YmdHis') . ".xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment;filename=$file");
            header("Cache-Control: max-age=0");
            header("Pragma: no-cache");
            header("Expires: 0");
            $objWriter = PHPExcel_IOFactory::createWriter($this->newphpexcel, 'Excel5');
            $objWriter->save('php://output');
            exit();
        }
    }
    
    public function excel_label($isajax, $tgl_awal, $tgl_akhir, $beras, $kdprop, $kdkab) {
        if ($this->newsession->userdata('_LOGGED')) {
            ini_set('memory_limit', '512M');
            $this->load->model('reference/rekap_act');
            $data = $this->rekap_act->excel_label($isajax, $tgl_awal, $tgl_akhir, $beras, $kdprop, $kdkab);
            $this->load->library('newphpexcel');
            $this->newphpexcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $this->newphpexcel->getDefaultStyle()->getFont()->setName('Calibri')->setSize(10);
            $arrheader = array('font' => array('bold' => true),
                'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
                'fill' => array('type' => (PHPExcel_Style_Fill::FILL_SOLID),
                    'color' => array('rgb' => 'C0C0C0')));
            $kolom = array('B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1', 'J1', 'K1', 'L1', 'M1', 'N1', 'O1', 'P1', 'Q1', 'R1', 'S1', 'T1', 'U1', 'V1', 'W1', 'X1', 'Y1', 'Z1', 'AB1', 'AC1', 'AD1', 'AE1', 'AF1', 'AG1', 'AH1', 'AI1', 'AJ1', 'AK1', 'AL1', 'AM1', 'AN1', 'AO1', 'AP1', 'AQ1', 'AR1', 'AS1', 'AT1', 'AU1', 'AV1', 'AW1', 'AX1', 'AY1', 'AZ1', 'BB1', 'BC1', 'BD1', 'BE1', 'BF1', 'BG1', 'BH1', 'BI1', 'BJ1', 'BK1', 'BL1', 'BM1', 'BN1', 'BO1', 'BP1', 'BQ1', 'BR1', 'BS1', 'BT1', 'BU1', 'BV1', 'BW1', 'BX1', 'BY1', 'BZ1');
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A1', 'No');
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->setActiveSheetIndex(0)
                        ->setCellValue($kolom[$i], $data['header'][$i]);
            }

            $this->newphpexcel->getActiveSheet()
                    ->getStyle($this->newphpexcel->getActiveSheet()->calculateWorksheetDimension())
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->newphpexcel->getActiveSheet()->getStyle('A1')->applyFromArray($arrheader);
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->getActiveSheet()->getStyle($kolom[$i])->applyFromArray($arrheader);
            }

            $this->newphpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            for ($i = 0; $i < count($data['header']); $i++) {
                $subkolom = substr($kolom[$i], 0, 1);
                $this->newphpexcel->getActiveSheet()->getColumnDimension($subkolom)->setWidth(25);
            }
            $i = 2;
            $no = 1;
            //print_r($data['detil']);die();
            foreach ($data['detil'] as $row) {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no);
                for ($z = 0; $z < count($data['header']); $z++) {
                    $subkolom = substr($kolom[$z], 0, -1);
                    $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row[$data['header'][$z]]);
                    $this->newphpexcel->getActiveSheet()->getStyle('BI' . $i)->getAlignment()->setWrapText(true);
                    $this->newphpexcel->getActiveSheet()->getStyle('BJ' . $i)->getAlignment()->setWrapText(true);
                    $this->newphpexcel->getActiveSheet()->getStyle('BK' . $i)->getAlignment()->setWrapText(true);
                    $this->newphpexcel->getActiveSheet()->getColumnDimension('BI')->setWidth(10);
                    $this->newphpexcel->getActiveSheet()->getColumnDimension('BJ')->setWidth(100);
                    $this->newphpexcel->getActiveSheet()->getColumnDimension('BK')->setWidth(15);
                }
                $this->newphpexcel->getActiveSheet()
                        ->getStyle($this->newphpexcel->getActiveSheet()->calculateWorksheetDimension())
                        ->getAlignment()
                        ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $i++;
                $no++;
            }

            ob_clean();
            $file = "SIPT_Rekap_" . date('YmdHis') . ".xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment;filename=$file");
            header("Cache-Control: max-age=0");
            header("Pragma: no-cache");
            header("Expires: 0");
            $objWriter = PHPExcel_IOFactory::createWriter($this->newphpexcel, 'Excel5');
            $objWriter->save('php://output');
            exit();
        }
    }

    public function persyaratan($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/licensing_act');
                $ret = $this->licensing_act->add_new($act, $isajax);
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function add_minol($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/siupmb_act');
                if ($act == 'save') {
                    $ret = $this->siupmb_act->save_minol($isajax);
                } elseif ($act == 'update') {
                    $ret = $this->siupmb_act->update_minol($isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function tembusan($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            //print_r($_POST);die();
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('licensing/status_act');
                $ret = $this->status_act->set_tembusan($act, $isajax);
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function rekap_act($act) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $nm_izin = $this->input->post('dokumen');
                $tgl_awal = $this->input->post('tgl_awal');
                $tgl_akhir = $this->input->post('tgl_akhir');
                $tgl_awal2 = $this->input->post('tgl_awal2');
                $tgl_akhir2 = $this->input->post('tgl_akhir2');
                $status = $this->input->post('status');
                $rekap = $this->input->post('rekap');
                $antar_pulau = $this->input->post('antar_pulau');
                $rekap_bapok = $this->input->post('rekap_bapok');
                $this->load->model('reference/rekap_act');
                if ($act == "search") {
                    $data = $this->rekap_act->tabel_rekap($nm_izin, $tgl_awal, $tgl_akhir, $status, $rekap);
                    echo $data;
                } elseif ($act == "export") {
                    $this->rekap_act->get_excel($nm_izin, $tgl_awal, $tgl_akhir, $status);
                } elseif ($act == "search_bapok") {
                    $data = $this->rekap_act->tabel_rekap_bapokstra($rekap_bapok);
                    echo $data;
                } elseif ($act == "search_antarpulau") {
                    $data = $this->rekap_act->tabel_rekap_antarpulau($antar_pulau);
                    echo $data;
                }
            }
        }
    }

    public function tes2($nm_izin, $tgl_awal, $tgl_akhir, $status) {
        //print_r("Nama Izin = ".$nm_izin." Tgl awal = ".$tgl_awal. " Tgl Akhir = ".$tgl_akhir. " Status = ".$status);exit();
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('reference/rekap_act');
            $data = $this->rekap_act->get_excel($nm_izin, $tgl_awal, $tgl_akhir, $status);
            //print_r($data);die();
            $this->load->library('newphpexcel');
            $this->newphpexcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $this->newphpexcel->getDefaultStyle()->getFont()->setName('Calibri')->setSize(10);
            $arrheader = array('font' => array('bold' => true),
                'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
                'fill' => array('type' => (PHPExcel_Style_Fill::FILL_SOLID),
                    'color' => array('rgb' => 'C0C0C0')));
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A1', 'No')
                    ->setCellValue('B1', 'No Pendaftaran')
                    ->setCellValue('C1', 'Tanggal Daftar')
                    ->setCellValue('D1', 'Tanggal Proses')
                    ->setCellValue('E1', 'NPWP Perusahaan')
                    ->setCellValue('F1', 'Nama Perusahaan')
                    ->setCellValue('G1', 'No Izin')
                    ->setCellValue('H1', 'Tanggal Izin')
                    ->setCellValue('I1', 'Daftar Via');
            $this->newphpexcel->getActiveSheet()
                    ->getStyle($this->newphpexcel->getActiveSheet()->calculateWorksheetDimension())
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->newphpexcel->getActiveSheet()->getStyle('A1')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('B1')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('C1')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('D1')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('E1')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('F1')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('G1')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('H1')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getStyle('I1')->applyFromArray($arrheader);
            $this->newphpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $this->newphpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $this->newphpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
            $this->newphpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
            $this->newphpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $this->newphpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $this->newphpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $this->newphpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
            $this->newphpexcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);

            $i = 2;
            $no = 1;
            foreach ($data as $row) {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no)
                        ->setCellValue('B' . $i, $row['No_Pendaftaran'])
                        ->setCellValue('C' . $i, $row['Tanggal_Daftar'])
                        ->setCellValue('D' . $i, $row['Tanggal Proses'])
                        ->setCellValue('E' . $i, $row['NPWP_Perusahaan'])
                        ->setCellValue('F' . $i, $row['Nama_Perusahaan'])
                        ->setCellValue('G' . $i, $row['No_Izin'])
                        ->setCellValue('H' . $i, $row['Tanggal_Izin'])
                        ->setCellValue('I' . $i, $row['Daftar_via']);
                $i++;
                $no++;
            }


            ob_clean();
            $file = "test.xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment;filename=$file");
            header("Cache-Control: max-age=0");
            header("Pragma: no-cache");
            header("Expires: 0");
            $objWriter = PHPExcel_IOFactory::createWriter($this->newphpexcel, 'Excel5');
            $objWriter->save('php://output');
            exit();
        }
    }

    public function tes($nm_izin, $tgl_awal, $tgl_akhir, $status, $kdprop = '', $kdkab = '', $kdkec = '', $kdkel = '') {
        // ini_set('display_errors', 1);
        // ini_set('display_startup_errors', 1);
        ini_set('memory_limit', '512M');
        // error_reporting(E_ALL);
        //print_r("Nama Izin = ".$nm_izin." Tgl awal = ".$tgl_awal. " Tgl Akhir = ".$tgl_akhir. " Status = ".$status);exit();
        if ($this->newsession->userdata('_LOGGED')) {
            if ($nm_izin == '03.22') {
                $this->load->model('reference/rekap_act');
                $data = $this->rekap_act->get_bapok($nm_izin, $tgl_awal, $tgl_akhir, $status, $kdprop, $kdkab, $kdkec, $kdkel);
            } else {
                $this->load->model('reference/rekap_act');
                $data = $this->rekap_act->get_excel($nm_izin, $tgl_awal, $tgl_akhir, $status, $kdprop, $kdkab, $kdkec, $kdkel);
            }
            //print_r(count($data['detil']));die();
            $this->load->library('newphpexcel');
            $this->newphpexcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $this->newphpexcel->getDefaultStyle()->getFont()->setName('Calibri')->setSize(10);
            $arrheader = array('font' => array('bold' => true),
                'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                    'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
                'fill' => array('type' => (PHPExcel_Style_Fill::FILL_SOLID),
                    'color' => array('rgb' => 'C0C0C0')));
            $kolom = array('B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1', 'J1', 'K1', 'L1', 'M1', 'N1', 'O1', 'P1', 'Q1', 'R1', 'S1', 'T1', 'U1', 'V1', 'W1', 'X1', 'Y1', 'Z1', 'AA1', 'AB1', 'AC1', 'AD1', 'AE1', 'AF1', 'AG1', 'AH1', 'AI1', 'AJ1', 'AK1', 'AL1', 'AM1', 'AN1', 'AO1', 'AP1', 'AQ1', 'AR1', 'AS1', 'AT1', 'AU1', 'AV1', 'AW1', 'AX1', 'AY1', 'AZ1', 'BB1', 'BC1', 'BD1', 'BE1', 'BF1', 'BG1', 'BH1', 'BI1', 'BJ1', 'BK1', 'BL1', 'BM1', 'BN1', 'BO1', 'BP1', 'BQ1', 'BR1', 'BS1', 'BT1', 'BU1', 'BV1', 'BW1', 'BX1', 'BY1', 'BZ1');
            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A1', 'No');
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->setActiveSheetIndex(0)
                        ->setCellValue($kolom[$i], $data['header'][$i]);
            }

            $this->newphpexcel->getActiveSheet()
                    ->getStyle($this->newphpexcel->getActiveSheet()->calculateWorksheetDimension())
                    ->getAlignment()
                    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->newphpexcel->getActiveSheet()->getStyle('A1')->applyFromArray($arrheader);
            for ($i = 0; $i < count($data['header']); $i++) {
                $this->newphpexcel->getActiveSheet()->getStyle($kolom[$i])->applyFromArray($arrheader);
            }

            $this->newphpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            for ($i = 0; $i < count($data['header']); $i++) {
                $subkolom = substr($kolom[$i], 0, 1);
                $this->newphpexcel->getActiveSheet()->getColumnDimension($subkolom)->setWidth(25);
            }
            $i = 2;
            $no = 1;
            foreach ($data['detil'] as $row) {
                $this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A' . $i, $no);
                for ($z = 0; $z < count($data['header']); $z++) {
                    $subkolom = substr($kolom[$z], 0, -1);
                    switch ($data['header'][$z]) {
                        case 'pemasaran':
                            $cetak = $this->main->get_pemasaran_bapok($row[$data['header'][$z]], 1);
                            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $cetak);
                            break;
                        case 'pemasaran_kab':
                            $cetak = $this->main->get_pemasaran_bapok($row[$data['header'][$z]], 2);
                            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $cetak);
                            break;
                        case 'pemasaran_prop':
                            $cetak = $this->main->get_pemasaran_bapok($row[$data['header'][$z]], 3);
                            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $cetak);
                            break;
                        default :
                            $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $row[$data['header'][$z]]);
                    }
                }
                $i++;
                $no++;
            }
            /* if ($nm_izin == '03.22') {
              $i = 2;
              foreach ($data['brg'] as $brg) {
              $plus = count($data['header'])-1;
              $subkolom = substr($kolom[$plus], 0, -1);
              $this->newphpexcel->setActiveSheetIndex(0)->setCellValue($subkolom . $i, $brg);
              $i++;
              }
              } */
            ob_clean();
            $file = "SIPT_Rekap_" . date('YmdHis') . ".xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment;filename=$file");
            header("Cache-Control: max-age=0");
            header("Pragma: no-cache");
            header("Expires: 0");
            $objWriter = PHPExcel_IOFactory::createWriter($this->newphpexcel, 'Excel5');
            $objWriter->save('php://output');
            exit();
        }
    }

}
