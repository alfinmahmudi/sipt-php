<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Proccess extends CI_Controller {

    var $ineng = "";

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('site_lang')) {
            $this->lang->load('message', $this->session->userdata('site_lang'));
            $this->ineng = $this->session->userdata('site_lang');
        } else {
            $this->lang->load('message', 'id');
            $this->ineng = "id";
        }
    }

    public function index() {
        echo "Forbidden";
    }

    public function label_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                if ($_POST['SESUDAH'] == '2000' || $_POST['SESUDAH'] == 'resend_email') {
                    $arrdata = $this->input->post('data');
                    $dir = '0' . hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                    $izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
                    $id = $arrdata['id'];
                    $this->load->model('proccess/label_act');
                    $ret = $this->label_act->set_proccess($act, $isajax);
                    $CI = & get_instance();
                    $CI->load->model('print/siupmb_cetak_act');
                    $arrdata = $CI->siupmb_cetak_act->set_prints($izin, $id, '1');
                    $arrdata['cetak'] = 1;
                    $arrmagin = array();
                    ob_start();
                    $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                    require_once($classfile);
                    $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
                    $objpdf->generate($arrdata);
                    $id = hashids_decrypt($id, _HASHIDS_, 9);
                    $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha(1000, $dir, $izin, $id, '');
                } else {
                    $this->load->model('proccess/label_act');
                    $ret = $this->label_act->set_proccess($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function siupmb_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                if ($_POST['SESUDAH'] == '1000' || $_POST['SESUDAH'] == 'resend_email') {
                    $arrdata = $this->input->post('data');
                    $dir = '0' . hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                    $izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
                    $id = $arrdata['id'];
                    $arrsiupmb = array(3, 4, 5, 6, 13);
                    $this->load->model('proccess/siupmb_act');
                    $ret = $this->siupmb_act->set_proccess($act, $isajax);
                    if (in_array($izin, $arrsiupmb)) {
                        $CI = & get_instance();
                        $CI->load->model('print/siupmb_cetak_act');
                        $arrdata = $CI->siupmb_cetak_act->set_prints($izin, $id, '1');
                        $arrdata['cetak'] = 1;
                        $arrmagin = array();
                        ob_start();
                        $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                        require_once($classfile);
                        $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
                        $objpdf->generate($arrdata);
                    }
                    $id = hashids_decrypt($id, _HASHIDS_, 9);
                    $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha(1000, $dir, $izin, $id, '');
                } else {
                    $this->load->model('proccess/siupmb_act');
                    $ret = $this->siupmb_act->set_proccess($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function sipdis_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                if ($_POST['SESUDAH'] == '1000' || $_POST['SESUDAH'] == 'resend_email') {
                    $arrdata = $this->input->post('data');
                    $dir = '0' . hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                    $izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
                    $id = $arrdata['id'];
                    $this->load->model('proccess/sipdis_act');
                    $ret = $this->sipdis_act->set_proccess($act, $isajax);
                    if ($izin == '22') {
                        $CI = & get_instance();
                        $CI->load->model('print/sipdis_cetak_act');
                        $arrdata = $CI->sipdis_cetak_act->set_prints($izin, $id, '1');
                        $arrdata['cetak'] = 1;
                        $arrmagin = array();
                        ob_start();
                        $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                        require_once($classfile);
                        $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
                        $objpdf->generate($arrdata);
                    }
                    $id = hashids_decrypt($id, _HASHIDS_, 9);
                    $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha(1000, $dir, $izin, $id, '');
                } else {
                    $this->load->model('proccess/sipdis_act');
                    $ret = $this->sipdis_act->set_proccess($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function siupagen_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                if ($_POST['SESUDAH'] == '1000' || $_POST['SESUDAH'] == 'resend_email') {
                    $arrdata = $this->input->post('data');
                    $dir = '0' . hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                    $izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
                    $id = $arrdata['id'];
                    $this->load->model('proccess/siupagen_act');
                    $ret = $this->siupagen_act->set_proccess($act, $isajax);
                    if ($izin == "12") {
                        $CI = & get_instance();
                        $CI->load->model('print/siupagen_cetak_act');
                        $arrdata = $CI->siupagen_cetak_act->set_prints($izin, $id, '1');
                        $arrdata['cetak'] = 1;
                        $arrdata['arrdata'] = $arrdata;
                        $arrmagin = array();
                        ob_start();
                        //$classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                        //require_once($classfile);
                        //$objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
                        //$objpdf->generate($arrdata);
                        $this->load->view("template/" . $dir . "/" . $izin . ".php", $arrdata);
                    }
                    $id = hashids_decrypt($id, _HASHIDS_, 9);
                    $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha(1000, $dir, $izin, $id, '');
                } else {
                    $this->load->model('proccess/siupagen_act');
                    $ret = $this->siupagen_act->set_proccess($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function siup4_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                if ($_POST['SESUDAH'] == '1000' || $_POST['SESUDAH'] == 'resend_email') {
                    $arrdata = $this->input->post('data');
                    $dir = '0' . hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                    $izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
                    $id = $arrdata['id'];
                    $this->load->model('proccess/siup4_act');
                    $ret = $this->siup4_act->set_proccess($act, $isajax);
                    if ($izin == "2") {
                        $CI = & get_instance();
                        $CI->load->model('print/siup4_cetak_act');
                        $arrdata = $CI->siup4_cetak_act->set_prints($izin, $id, '1');
                        $arrdata['cetak'] = 1;
                        $arrmagin = array();
                        ob_start();
                        $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                        require_once($classfile);
                        $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
                        $objpdf->generate($arrdata);
                    }
                    $id = hashids_decrypt($id, _HASHIDS_, 9);
                    $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha(1000, $dir, $izin, $id, '');
                } else {
                    $this->load->model('proccess/siup4_act');
                    $ret = $this->siup4_act->set_proccess($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function siujs_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                if ($_POST['SESUDAH'] == '1000' || $_POST['SESUDAH'] == 'resend_email') {
                    $arrdata = $this->input->post('data');
                    $dir = '0' . hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                    $izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
                    $id = $arrdata['id'];
                    $this->load->model('proccess/siujs_act');
                    $ret = $this->siujs_act->set_proccess($act, $isajax);
                    if ($izin == "1") {
                        $CI = & get_instance();
                        $CI->load->model('print/siujs_cetak_act');
                        $arrdata = $CI->siujs_cetak_act->set_prints($izin, $id, '1');
                        $arrdata['cetak'] = 1;
                        $arrmagin = array();
                        ob_start();
                        $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                        require_once($classfile);
                        $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
                        $objpdf->generate($arrdata);
                    }
                    $id = hashids_decrypt($id, _HASHIDS_, 9);
                    $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha(1000, $dir, $izin, $id, '');
                } else {
                    $this->load->model('proccess/siujs_act');
                    $ret = $this->siujs_act->set_proccess($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function sppgrap_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                if ($_POST['SESUDAH'] == '1000' || $_POST['SESUDAH'] == 'resend_email') {
                    $arrdata = $this->input->post('data');
                    $dir = '0' . hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                    $izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
                    $id = $arrdata['id'];
                    $this->load->model('proccess/sppgrap_act');
                    $ret = $this->sppgrap_act->set_proccess($act, $isajax);
                    if ($izin == "10") {
                        $CI = & get_instance();
                        $CI->load->model('print/sppgrap_cetak_act');
                        $arrdata = $CI->sppgrap_cetak_act->set_prints($izin, $id, '1');
                        $arrdata['cetak'] = 1;
                        $arrmagin = array();
                        ob_start();
                        if ($arrdata['row']['tipe_permohonan'] == '02') {
                            $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . "_03.php";
                        } else {
                            $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                        }
                        require_once($classfile);
                        $objpdf = new pdf("P", "mm", "A4", $arrmagin, $arrdata);
                        $objpdf->generate($arrdata);
                    }
                    $id = hashids_decrypt($id, _HASHIDS_, 9);
                    $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha(1000, $dir, $izin, $id, '');
                } else {
                    $this->load->model('proccess/sppgrap_act');
                    $ret = $this->sppgrap_act->set_proccess($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function siupbb_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                if ($_POST['SESUDAH'] == '1000' || $_POST['SESUDAH'] == 'resend_email') {
                    $arrdata = $this->input->post('data');
                    $dir = '0' . hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                    $izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
                    $id = $arrdata['id'];
                    $this->load->model('proccess/siupbb_act');
                    $ret = $this->siupbb_act->set_proccess($act, $isajax);
                    if ($izin == "11") {
                        $CI = & get_instance();
                        $CI->load->model('print/siupbb_cetak_act');
                        $arrdata = $CI->siupbb_cetak_act->set_prints($izin, $id, '1');
                        $arrdata['cetak'] = 1;
                        $arrmagin = array();
                        ob_start();
                        $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                        require_once($classfile);
                        $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
                        $objpdf->generate($arrdata);
                    }
                    $id = hashids_decrypt($id, _HASHIDS_, 9);
                    $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha(1000, $dir, $izin, $id, '');
                } else {
                    $this->load->model('proccess/siupbb_act');
                    $ret = $this->siupbb_act->set_proccess($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function pgapt_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('proccess/pgapt_act');
                $ret = $this->pgapt_act->set_proccess($act, $isajax);
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function pkapt_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                if ($_POST['SESUDAH'] == '1000' || $_POST['SESUDAH'] == 'resend_email') {
                    $arrdata = $this->input->post('data');
                    $dir = '0' . hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                    $izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
                    $id = $arrdata['id'];
                    $this->load->model('proccess/pkapt_act');
                    $ret = $this->pkapt_act->set_proccess($act, $isajax);
                    $CI = & get_instance();
                    $CI->load->model('print/pkapt_cetak_act');
                    $arrdata = $CI->pkapt_cetak_act->set_prints($izin, $id, '1');
                    $arrdata['cetak'] = 1;
                    $arrmagin = array();
                    ob_start();
                    $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                    require_once($classfile);
                    $objpdf = new pdf("P", "mm", "A4", $arrmargin, $arrdata);
                    $objpdf->generate($arrdata);

                    $id = hashids_decrypt($id, _HASHIDS_, 9);
                    $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha(1000, $dir, $izin, $id, '');
                } else {
                    $this->load->model('proccess/pkapt_act');
                    $ret = $this->pkapt_act->set_proccess($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function sppgap_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                $this->load->model('proccess/sppgap_act');
                $ret = $this->sppgap_act->set_proccess($act, $isajax);
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function stpw_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                if ($_POST['SESUDAH'] == '1000' || $_POST['SESUDAH'] == 'resend_email') {
                    $arrdata = $this->input->post('data');
                    $dir = '0' . hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                    $izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
                    $id = $arrdata['id'];
                    $this->load->model('proccess/stpw_act');
                    $ret = $this->stpw_act->set_proccess($act, $isajax);
                    $arrstpw = array(17, 18, 19, 20);
                    if (in_array($izin, $arrstpw)) {
                        $CI = & get_instance();
                        $CI->load->model('print/stpw_cetak_act');
                        $arrdata = $CI->stpw_cetak_act->set_prints($izin, $id, '1');
                        $arrdata['cetak'] = 1;
                        $arrmagin = array();
                        ob_start();
                        $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                        require_once($classfile);
                        $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
                        $objpdf->generate($arrdata);
                    }
                    $id = hashids_decrypt($id, _HASHIDS_, 9);
                    $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha(1000, $dir, $izin, $id, '');
                } else {
                    $this->load->model('proccess/stpw_act');
                    $ret = $this->stpw_act->set_proccess($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function pameran_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                if ($_POST['SESUDAH'] == '1000' || $_POST['SESUDAH'] == 'resend_email') {
                    $arrdata = $this->input->post('data');
                    $dir = '0' . hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                    $izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
                    $id = $arrdata['id'];
                    $arrpameran = array(15, 16);
                    $this->load->model('proccess/pameran_act');
                    $ret = $this->pameran_act->set_proccess($act, $isajax);
                    if (in_array($izin, $arrpameran)) {
                        $CI = & get_instance();
                        if ($izin == "15") {
                            $CI->load->model('print/pameran_cetak_act');
                            $arrdata = $CI->pameran_cetak_act->set_prints($izin, $id, '1');
                            $arrdata['cetak'] = 1;
                        } else {
                            $CI->load->model('print/pameranp_cetak_act');
                            $arrdata = $CI->pameranp_cetak_act->set_prints($izin, $id, '1');
                            $arrdata['cetak'] = 1;
                        }
                        $arrmagin = array();
                        ob_start();
                        $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                        require_once($classfile);
                        $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
                        $objpdf->generate($arrdata);
                    }
                    $id = hashids_decrypt($id, _HASHIDS_, 9);
                    $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha(1000, $dir, $izin, $id, '');
                } else {
                    $this->load->model('proccess/pameran_act');
                    $ret = $this->pameran_act->set_proccess($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

    public function garansi_act($act, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
                redirect(base_url());
                exit();
            } else {
                if ($_POST['SESUDAH'] == '1000' || $_POST['SESUDAH'] == 'resend_email') {
                    $arrdata = $this->input->post('data');
                    $dir = '0' . hashids_decrypt($this->input->post('direktorat'), _HASHIDS_, 9);
                    $izin = hashids_decrypt($arrdata['kd_izin'], _HASHIDS_, 9);
                    $id = $arrdata['id'];
                    $this->load->model('proccess/garansi_act');
                    $ret = $this->garansi_act->set_proccess($act, $isajax);
                    if ($izin == "14") {
                        $CI = & get_instance();
                        $CI->load->model('print/garansi_cetak_act');
                        $arrdata = $CI->garansi_cetak_act->set_prints($izin, $id, '1');
                        $arrdata['cetak'] = 1;
                        $arrmagin = array();
                        ob_start();
                        $classfile = APPPATH . "views/template/" . $dir . "/" . $izin . ".php";
                        require_once($classfile);
                        $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
                        $objpdf->generate($arrdata);
                    }
                    $id = hashids_decrypt($id, _HASHIDS_, 9);
                    $send_email_pelaku_usaha = $this->email_act->email_pelaku_usaha(1000, $dir, $izin, $id, '');
                } else {
                    $this->load->model('proccess/garansi_act');
                    $ret = $this->garansi_act->set_proccess($act, $isajax);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $ret;
        }
    }

}
