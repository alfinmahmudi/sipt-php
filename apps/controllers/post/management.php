<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Management extends CI_Controller{
	public function __construct() {
        parent::__construct();
    }
	
	public function index(){
		echo "Forbidden";
	}
	
	public function profil($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('management/management_act');
				$ret = $this->management_act->set_profil($act, $isajax);
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}	
	
}