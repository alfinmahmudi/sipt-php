<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reference extends CI_Controller{
	
	var $content = "";
	var $breadcumbs = "";
	var $ineng = "";
	
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }
	
	public function index(){
		if($this->newsession->userdata('_LOGGED')){
			$this->content = (!$this->content) ? $this->load->view($this->ineng.'/backend/default', '', true) : $this->content;
			$data = $this->main->set_content('backend', $this->breadcumbs, $this->content);
			$this->parser->parse($this->ineng.'/backend/home', $data);
		}else{
			redirect(site_url().'portal');
			exit();
		}
	}

	public function form($step, $dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			if($step == "first"){
				$this->load->model('licensing/siupmb_act');
				$arrdata = $this->siupmb_act->get_first($dir, $doc, $type, $id);
				$this->content = $this->load->view($this->ineng.'/backend/licensing/'.$dir.'/form_', $arrdata, true);
				$this->index();
			}elseif($step == "second") {
				$this->load->model('licensing/siupmb_act');
				$arrdata = $this->siupmb_act->get_second($dir, $doc, $type, $id);
				$this->content = $this->load->view($this->ineng.'/backend/licensing/'.$dir.'/second', $arrdata, true);
				$this->index();
			}elseif($step == "third") {
				$this->load->model('licensing/siupmb_act');
				$arrdata = $this->siupmb_act->get_third($dir, $doc, $type, $id);
				$this->content = $this->load->view($this->ineng.'/backend/licensing/'.$dir.'/third', $arrdata, true);
				$this->index();
			}elseif($step == "fourth") {
				$this->load->model('licensing/siupmb_act');
				$arrdata = $this->siupmb_act->get_fourth($dir, $doc, $type, $id);
				$this->content = $this->load->view($this->ineng.'/backend/licensing/'.$dir.'/fourth', $arrdata, true);
				$this->index();
			}
		}else{
			redirect(site_url('portal'));
			exit();
		}
		
	}

	public function preview_form($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			if($dir == "01"){
				$this->load->model('licensing/siupmb_act');
				$arrdata = $this->siupmb_act->get_first($dir, $doc, $type, $id);
				$this->content = $this->load->view($this->ineng.'/backend/licensing/'.$dir.'/form', $arrdata, true);
				$this->index();
			}elseif($dir == "02") {
				$this->load->model('licensing/siupmb_act');
				$arrdata = $this->siupmb_act->get_preview($dir, $doc, $type, $id);
				$this->content = $this->load->view($this->ineng.'/backend/licensing/'.$dir.'/preview', $arrdata, true);
				$this->index();
			}elseif($dir == "03") {
				$this->load->model('licensing/siupmb_act');
				$arrdata = $this->siupmb_act->get_third($dir, $doc, $type, $id);
				$this->content = $this->load->view($this->ineng.'/backend/licensing/'.$dir.'/third', $arrdata, true);
				$this->index();
			}
		}else{
			redirect(site_url('portal'));
			exit();
		}

	}

	public function view($menu, $sub = "", $id = ""){
		if($this->newsession->userdata('_LOGGED')){
			if($menu == "users"){
				$this->load->model('reference/users_act');
				$arrdata = $this->users_act->list_user();
				if($this->input->post("data-post")){
					echo $arrdata;
				}else{
					$this->content = $this->load->view('browse/grid', $arrdata, true);
					$this->index();
				}
			}else if($menu == "formating"){
				$this->load->model('reference/formating_act');
				$arrdata = $this->formating_act->list_number();
				if($this->input->post("data-post")){
					echo $arrdata;
				}else{
					$this->content = $this->load->view('browse/grid', $arrdata, true);
					$this->index();
				}
				
			}else if($menu == "jns_minol"){
				$this->load->model('reference/minol_act');//die("ok");
				$arrdata = $this->users_act->list_minol();
				if($this->input->post("data-post")){
					echo $arrdata;
				}else{
					$this->content = $this->load->view('browse/grid', $arrdata, true);
					$this->index();
				}
			}else if($menu == "tupoksi"){
				$this->load->model('reference/tupoksi_act');//die("ok");
				$arrdata = $this->tupoksi_act->view_licensing();
				$this->content = $this->load->view($this->ineng.'/backend/licensing/add_req', $arrdata, true);
				$this->index();
			}else if($menu == "popup"){
				$this->load->model('reference/popup_act');
				$arrdata = $this->popup_act->list_popup();
				if($this->input->post("data-post")){
					echo $arrdata;
				}else{
					$this->content = $this->load->view('browse/grid', $arrdata, true);
					$this->index();
				}
			}else if($menu == "pelabuhan"){
				$this->load->model('reference/pelabuhan_act');
				$arrdata = $this->pelabuhan_act->list_pelabuhan();
				if($this->input->post("data-post")){
					echo $arrdata;
				}else{
					$this->content = $this->load->view('browse/grid', $arrdata, true);
					$this->index();
				}
			}else if($menu == "produk"){
				$this->load->model('reference/produk_act');
				$arrdata = $this->produk_act->list_produk();
				if($this->input->post("data-post")){
					echo $arrdata;
				}else{
					$this->content = $this->load->view('browse/grid', $arrdata, true);
					$this->index();
				}
			}else if($menu == "signatory"){
				$this->load->model('reference/signatory_act');
				$arrdata = $this->signatory_act->list_signatory();
				if($this->input->post("data-post")){
					echo $arrdata;
				}else{
					$this->content = $this->load->view('browse/grid', $arrdata, true);
					$this->index();
				}
			}else if($menu == "npwp"){
				$this->load->model('reference/npwp_act');
				$arrdata = $this->npwp_act->list_npwp();
				if($this->input->post('data-post')){
					echo $arrdata;
				}else{
					$this->content = $this->load->view('browse/grid', $arrdata, true);
					$this->index();
				}
			}else if($menu == "rekap"){
				$this->load->model('reference/rekap_act');
				$data = $this->rekap_act->rekap();
				$this->content = $this->load->view($this->ineng.'/backend/rekap.php',$data,true);
				$this->index();
			}else if($menu == "rekap_bapok"){
				$this->load->model('reference/rekap_act');
				$data = $this->rekap_act->rekap_bapok();
				$this->content = $this->load->view($this->ineng.'/backend/rekap.php',$data,true);
				$this->index();
			}else if($menu == "rekap_bapokstra"){
				$this->load->model('reference/rekap_act');
				$data = $this->rekap_act->rekap_bapokstra();
				$this->content = $this->load->view($this->ineng.'/backend/rekap.php',$data,true);
				$this->index();
			}else if($menu == "rekap_antarpulau"){
				$this->load->model('reference/rekap_act');
				$data = $this->rekap_act->rekap_antarpulau();
				$this->content = $this->load->view($this->ineng.'/backend/report/pulau/rekap_pulau.php',$data,true);
				$this->index();
			}else if($menu == "rekap_bapok_prshn"){
				$this->load->model('reference/rekap_act');
				$data = $this->rekap_act->rekap_bapok_prshn();
				$this->content = $this->load->view($this->ineng.'/backend/rekap.php',$data,true);
				$this->index();
			}else if($menu == "rekap_label"){
				$this->load->model('reference/rekap_act');
				$data = $this->rekap_act->rekap_label();
				$this->content = $this->load->view($this->ineng.'/backend/rekap.php',$data,true);
				$this->index();
			}
			else{
				redirect(base_url().'home', refresh);
			}
		}
	}
	
	public function users($menu,$id){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			$this->load->model('reference/users_act');
			if($menu != "update"){
				$arrdata = $this->users_act->get_user();
			}else{
				$arrdata = $this->users_act->user_id($id);
			}//print_r($arrdata);die();
			echo $this->load->view($this->ineng.'/backend/reference/users/form', $arrdata, true);
		}
	}

	public function get_popup(){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "05"){
			$this->load->model('reference/popup_act');
			$arrdata = $this->popup_act->get_show();
			echo json_encode($arrdata);
		}
	}

	public function add_pelabuhan($menu){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			$this->load->model('reference/pelabuhan_act');
			$arrdata = $this->pelabuhan_act->get_pelabuhan($menu);
			echo $this->load->view($this->ineng.'/backend/reference/pelabuhan/form', $arrdata, true);
		}
	}

	public function add_popup($menu, $id=''){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			$this->load->model('reference/popup_act');
			$arrdata = $this->popup_act->get_popup($menu, $id);
			$this->content = $this->load->view($this->ineng.'/backend/reference/popup/form', $arrdata, true);
			$this->index();
		}
	}
	
	public function add_produk($menu){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			$this->load->model('reference/produk_act');
			$arrdata = $this->produk_act->get_produk($menu);
			echo $this->load->view($this->ineng.'/backend/reference/produk/form', $arrdata, true);
		}
	}
	
	public function formating($menu, $id){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			$this->load->model('reference/formating_act');
			$arrdata = $this->formating_act->get_format($id);
			//print_r($arrdata['sess']);
		}
	}
	
	public function signatory($id){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			$this->load->model('reference/signatory_act');
			$arrdata = $this->signatory_act->get_signatory($id);
//                        print_r($arrdata);die();
			echo $this->load->view($this->ineng.'/backend/reference/signatory/form',$arrdata, true);
		}
	}
	
	public function npwp($id){
		if($this->newsession->userdata('_LOGGED') && ($this->newsession->userdata('role') == "01" || $this->newsession->userdata('role') == "99")){
			$this->load->model('reference/npwp_act');
			$arrdata = $this->npwp_act->get_npwp($id);
			echo $this->load->view($this->ineng.'/backend/reference/npwp/form',$arrdata, true);
		}
	}
	
	public function formating_edit($id){
		if($this->newsession->userdata('_LOGGED') && $this->newsession->userdata('role') == "99"){
			$this->load->model('reference/formating_act');
			$arrdata = $this->formating_act->get_format($id);
			echo $this->load->view($this->ineng.'/backend/reference/formating/form',$arrdata, true); 
		}
	}
	
	/* public function user($id){
		if($this->newsession->userdata('_LOGGED') && ($this->newsession->userdata('role') == "01" || $this->newsession->userdata('role') == "99")){
			$this->load->model('reference/user_act');
			$arrdata = $this->user_act->user($id);
			echo $this->load->view($this->ineng.'/backend/reference/users/form',$arrdata, true);
		}else{
			echo "Error";
		}
	} */
	
	public function del_npwp($isajax){
		if($this->newsession->userdata('_LOGGED') && ($this->newsession->userdata('role') == "01" || $this->newsession->userdata('role') == "99")){
			$this->load->model('reference/npwp_act');
			$ret = $this->npwp_act->delete_npwp($_POST['tb_chk']);
			echo $ret;
		}	
	}
	
	public function del_user($isajax){
		if($this->newsession->userdata('_LOGGED') && ($this->newsession->userdata('role') == "01" || $this->newsession->userdata('role') == "99")){
			$this->load->model('reference/users_act');
			$ret = $this->users_act->delete_user($_POST['tb_chk']);
			echo $ret;
		}	
	}

	public function dialog_pel($id){
		if($this->newsession->userdata('_LOGGED')){
			$act = 'update';
			$this->load->model('reference/pelabuhan_act');
			$arrdata = $this->pelabuhan_act->get_pelabuhan($act,$id);
			echo $this->load->view($this->ineng.'/backend/reference/pelabuhan/form',$arrdata, true);
			
		}
	}

	public function dialog_prod($id){
		if($this->newsession->userdata('_LOGGED')){
			$act = 'update';
			$this->load->model('reference/produk_act');
			$arrdata = $this->produk_act->get_produk($act,$id);
			echo $this->load->view($this->ineng.'/backend/reference/produk/form',$arrdata, true);
		}
	}

	public function del_pel($isajax){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('reference/pelabuhan_act');
			$ret = $this->pelabuhan_act->delete_pel($_POST['tb_chk']);
			echo $ret;
		}	
	}

	public function del_pop($isajax){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('reference/popup_act');
			$ret = $this->popup_act->delete_pop($_POST['tb_chk']);
			echo $ret;
		}	
	}

	public function del_prod($isajax){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('reference/produk_act');
			$ret = $this->produk_act->delete_prod($_POST['tb_chk']);
			echo $ret;
		}	
	}

}