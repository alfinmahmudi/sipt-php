<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Licensing extends CI_Controller {

    var $content = "";
    var $breadcumbs = "";
    var $ineng = "";

    public function __construct() {
        parent::__construct();
        if ($this->newsession->userdata('_LOGGED')) {
            if ($this->session->userdata('site_lang')) {
                $this->lang->load('message', $this->session->userdata('site_lang'));
                $this->ineng = $this->session->userdata('site_lang');
            } else {
                $this->lang->load('message', 'id');
                $this->ineng = "id";
            }
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function index() {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/backend/default', '', true) : $this->content;
            $data = $this->main->set_content('backend', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/backend/home', $data);
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function check($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/status_act');
            $arrdata = $this->status_act->cek_edit();
            echo $arrdata;
        }
    }

    public function check_tdp() {
        $this->load->model('registrasi_act');
        $arrdata = $this->registrasi_act->get_vaidate_tdp();
        echo json_encode($arrdata);
    }
    
    public function check_nib() {
        $this->load->model('registrasi_act');
        $arrdata = $this->registrasi_act->get_vaidate_nib();
        echo json_encode($arrdata);
    }

    public function validate_npwp() {
        $npwp = $this->input->post('npwp'); //print_r($npwp);die();
        $url = "http://10.30.30.5/addon/api/website_api/npwp/" . $npwp;
        $ch = curl_init();

        // set URL and other appropriate options  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:4agROXxoNEhMC5meU78Al1nZWwcqGIJDzvpPL6K0', 'User-Agent:Website-Demo'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // grab URL and pass it to the browser  

        $output = curl_exec($ch);
        if (strlen($output) == 0) {
            $result['stat'] = '0';
            echo json_encode($result);
        } else {
            $result['stat'] = '1';
            $result['res'] = json_decode($output);
            echo json_encode($result);
        }
    }

    public function check_notif() {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/status_act');
            $arrdata = $this->status_act->get_notif();
            echo json_encode($arrdata);
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function check_regis() {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/status_act');
            $arrdata = $this->status_act->get_regis();
            echo json_encode($arrdata);
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function data_table() {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/status_act');
            $arrdata = $this->status_act->get_table();
            echo json_encode($arrdata);
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function data_petugas() {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/status_act');
            $arrdata = $this->status_act->get_petugas();
            echo json_encode($arrdata);
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function form($step, $dir, $doc, $type, $id, $s = null) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($step == "first") {
                if ($dir == "01") {
                    if ($doc == "1") {
                        $this->load->model('licensing/siujs_act');
                        $arrdata = $this->siujs_act->get_first($dir, $doc, $type, $id);
                    } elseif ($doc == "2") {
                        $this->load->model('licensing/siup4_act');
                        $arrdata = $this->siup4_act->get_first($dir, $doc, $type, $id);
                    } elseif ($doc == "12") {
                        $this->load->model('licensing/siupagen_act');
                        $arrdata = $this->siupagen_act->get_first($dir, $doc, $type, $id);
                    } elseif (($doc == "16") || ($doc == "15")) {
                        $this->load->model('licensing/pameran_act');
                        $arrdata = $this->pameran_act->get_first($dir, $doc, $type, $id);
                    } elseif (($doc == "18") || ($doc == "17") || ($doc == "19") || ($doc == "20") || ($doc == "21")) {
                        $this->load->model('licensing/stpw_act');
                        $arrdata = $this->stpw_act->get_first($dir, $doc, $type, $id);
                    } elseif ($doc == "14") {
                        $this->load->model('licensing/garansi_act');
                        $arrdata = $this->garansi_act->get_first($dir, $doc, $type, $id);
                    } elseif ($doc == "24") {
                        $this->load->model('licensing/siupl_act');
                        $arrdata = $this->siupl_act->get_first($dir, $doc, $type, $id);
                    }

                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/form', $arrdata, true);
                    $this->index();
                } elseif ($dir == "02") {
                    if ($doc == "7") {
                        $this->load->model('licensing/pkapt_act');
                        $arrdata = $this->pkapt_act->get_first($dir, $doc, $type, $id);
                    } else {
                        $this->load->model('licensing/siupmb_act');
                        $arrdata = $this->siupmb_act->get_first($dir, $doc, $type, $id);
                    }
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/form', $arrdata, true);
                    $this->index();
                } elseif ($dir == "03") {
                    if ($doc == "11") {
                        $this->load->model('licensing/siupbb_act');
                        $arrdata = $this->siupbb_act->get_first($dir, $doc, $type, $id);
                    } else if ($doc == "7") {
                        $this->load->model('licensing/pkapt_act');
                        $arrdata = $this->pkapt_act->get_first($dir, $doc, $type, $id);
                    } else if ($doc == "8") {
                        $this->load->model('licensing/pgapt_act');
                        $arrdata = $this->pgapt_act->get_first($dir, $doc, $type, $id);
                    } else if ($doc == "9") {
                        $this->load->model('licensing/sppgap_act');
                        $arrdata = $this->sppgap_act->get_first($dir, $doc, $type, $id);
                    } else if ($doc == "10") {
                        $this->load->model('licensing/sppgrap_act');
                        $arrdata = $this->sppgrap_act->get_first($dir, $doc, $type, $id);
                    } else if ($doc == "22") {
                        $this->load->model('licensing/sipdis_act');
                        $arrdata = $this->sipdis_act->get_first($dir, $doc, $type, $id);
                    } else if ($doc == "23") {
                        $this->load->model('licensing/label_act');
                        $arrdata = $this->label_act->get_first($dir, $doc, $type, $id);
                    }
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/form', $arrdata, true);
                    $this->index();
                }
            } elseif ($step == "second") {
                if ($dir == "01") {
                    if ($doc == "1") {
                        $this->load->model('licensing/siujs_act');
                        $arrdata = $this->siujs_act->get_second($dir, $doc, $type, $id);
                    } elseif ($doc == "2") {
                        $this->load->model('licensing/siup4_act');
                        $arrdata = $this->siup4_act->get_second($dir, $doc, $type, $id);
                    } elseif ($doc == "12") {
                        $this->load->model('licensing/siupagen_act');
                        $arrdata = $this->siupagen_act->get_second($dir, $doc, $type, $id);
                    } elseif (($doc == "16") || ($doc == "15")) {
                        $this->load->model('licensing/pameran_act');
                        $arrdata = $this->pameran_act->get_second($dir, $doc, $type, $id);
                    } elseif (($doc == "18") || ($doc == "17") || ($doc == "19") || ($doc == "20") || ($doc == "21")) {
                        $this->load->model('licensing/stpw_act');
                        $arrdata = $this->stpw_act->get_second($dir, $doc, $type, $id);
                    } elseif ($doc == "14") {
                        $this->load->model('licensing/garansi_act');
                        $arrdata = $this->garansi_act->get_second($dir, $doc, $type, $id, $s);
                    }elseif ($doc == "24") {
                        $this->load->model('licensing/siupl_act');
                        $arrdata = $this->siupl_act->get_second($dir, $doc, $type, $id);
                    }

                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/second', $arrdata, true);
                    $this->index();
                } elseif ($dir == "02") {
                    if ($doc == '7') {
                        $this->load->model('licensing/pkapt_act');
                        $arrdata = $this->pkapt_act->get_second($dir, $doc, $type, $id);
                    } else {
                        $this->load->model('licensing/siupmb_act');
                        $arrdata = $this->siupmb_act->get_second($dir, $doc, $type, $id);
                    }
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/second', $arrdata, true);
                    $this->index();
                } elseif ($dir == "03") {
                    if ($doc == '11') {
                        $this->load->model('licensing/siupbb_act');
                        $arrdata = $this->siupbb_act->get_second($dir, $doc, $type, $id);
                    } elseif ($doc == '7') {
                        $this->load->model('licensing/pkapt_act');
                        $arrdata = $this->pkapt_act->get_second($dir, $doc, $type, $id);
                    } elseif ($doc == '8') {
                        $this->load->model('licensing/pgapt_act');
                        $arrdata = $this->pgapt_act->get_second($dir, $doc, $type, $id);
                    } else if ($doc == '9') {
                        $this->load->model('licensing/sppgap_act');
                        $arrdata = $this->sppgap_act->get_second($dir, $doc, $type, $id);
                    } else if ($doc == "10") {
                        $this->load->model('licensing/sppgrap_act');
                        $arrdata = $this->sppgrap_act->get_second($dir, $doc, $type, $id);
                    } else if ($doc == "22") {
                        $this->load->model('licensing/sipdis_act');
                        $arrdata = $this->sipdis_act->get_second($dir, $doc, $type, $id);
                    } else if ($doc == "23") {
                        $this->load->model('licensing/label_act');
                        $arrdata = $this->label_act->get_second($dir, $doc, $type, $id);
                    }

                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/second', $arrdata, true);
                    $this->index();
                }
            } elseif ($step == "third") {
                if ($dir == "01") {
                    if ($doc == "1") {
                        $this->load->model('licensing/siujs_act');
                        $arrdata = $this->siujs_act->get_third($dir, $doc, $type, $id);
                    } elseif ($doc == "2") {
                        $this->load->model('licensing/siup4_act');
                        $arrdata = $this->siup4_act->get_lstthird($dir, $doc, $type, $id);
                        if ($this->input->post("data-post")) {
                            echo $arrdata;
                            exit();
                        }
                    } elseif ($doc == "12") {
                        $this->load->model('licensing/siupagen_act');
                        $arrdata = $this->siupagen_act->get_third($dir, $doc, $type, $id);
                    } elseif ($doc == "16") {
                        $this->load->model('licensing/pameran_act');
                        $arrdata = $this->pameran_act->get_third($dir, $doc, $type, $id);
                        if ($this->input->post("data-post")) {
                            echo $arrdata;
                            exit();
                        }
                    } elseif (($doc == "18") || ($doc == "17") || ($doc == "19") || ($doc == "20") || ($doc == "21")) {
                        $this->load->model('licensing/stpw_act');
                        $arrdata = $this->stpw_act->get_third($dir, $doc, $type, $id);
                    } elseif ($doc == "14") {
                        $this->load->model('licensing/garansi_act');
                        $arrdata = $this->garansi_act->get_third($dir, $doc, $type, $id);
                    }elseif ($doc == "24") {
                        $this->load->model('licensing/siupl_act');
                        $arrdata = $this->siupl_act->get_third($dir, $doc, $type, $id);
                    }
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/third', $arrdata, true);
                    $this->index();
                } elseif ($dir == "02") {
                    if ($doc == '7') {
                        $this->load->model('licensing/pkapt_act');
                        $arrdata = $this->pkapt_act->get_third($dir, $doc, $type, $id);
                    } else {
                        $this->load->model('licensing/siupmb_act');
                        $arrdata = $this->siupmb_act->get_third($dir, $doc, $type, $id);
                    }
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/third', $arrdata, true);
                    $this->index();
                } elseif ($dir == "03") {
                    if ($doc == '8') {
                        $this->load->model('licensing/pgapt_act');
                        $arrdata = $this->pgapt_act->get_third($dir, $doc, $type, $id);
                    } else if ($doc == '11') {
                        $this->load->model('licensing/siupbb_act');
                        $arrdata = $this->siupbb_act->get_third($dir, $doc, $type, $id);
                    } else if ($doc == '7') {
                        $this->load->model('licensing/pkapt_act');
                        $arrdata = $this->pkapt_act->get_third($dir, $doc, $type, $id);
                    } else if ($doc == '9') {
                        $this->load->model('licensing/sppgap_act');
                        $arrdata = $this->sppgap_act->get_third($dir, $doc, $type, $id);
                    } else if ($doc == "10") {
                        $this->load->model('licensing/sppgrap_act');
                        $arrdata = $this->sppgrap_act->get_third($dir, $doc, $type, $id);
                    } else if ($doc == "22") {
                        $this->load->model('licensing/sipdis_act');
                        $arrdata = $this->sipdis_act->get_third($dir, $doc, $type, $id);
                    } else if ($doc == "23") {
                        $this->load->model('licensing/label_act');
                        $arrdata = $this->label_act->get_third($dir, $doc, $type, $id);
                    }
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/third', $arrdata, true);
                    $this->index();
                }
            } elseif ($step == "fourth") {
                //die($step. $dir. $doc. $type. $id);
                if ($dir == "01") {
                    if ($doc == "1") {
                        $this->load->model('licensing/siujs_act');
                        $arrdata = $this->siujs_act->get_lstfourth($dir, $doc, $type, $id);
                        if ($this->input->post("data-post")) {
                            echo $arrdata;
                            exit();
                        }
                    } elseif ($doc == "2") {
                        $this->load->model('licensing/siup4_act');
                        $arrdata = $this->siup4_act->get_fourth($dir, $doc, $type, $id);
                    } elseif ($doc == "12") {
                        $this->load->model('licensing/siupagen_act');
                        $arrdata = $this->siupagen_act->get_fourth($dir, $doc, $type, $id);
                    } elseif (($doc == "16") || ($doc == "15")) {
                        $this->load->model('licensing/pameran_act');
                        $arrdata = $this->pameran_act->get_fourth($dir, $doc, $type, $id);
                    } elseif (($doc == "18") || ($doc == "17") || ($doc == "19") || ($doc == "20") || ($doc == "21")) {
                        $this->load->model('licensing/stpw_act');
                        $arrdata = $this->stpw_act->get_fourth($dir, $doc, $type, $id);
                    } elseif ($doc == "14") {
                        $this->load->model('licensing/garansi_act');
                        $arrdata = $this->garansi_act->get_fourth($dir, $doc, $type, $id);
                    }elseif ($doc == "24") {
                        $this->load->model('licensing/siupl_act');
                        $arrdata = $this->siupl_act->get_fourth($dir, $doc, $type, $id);
                    }
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/fourth', $arrdata, true);
                    $this->index();
                } elseif ($dir == "02") {//die("de");
                    $this->load->model('licensing/siupmb_act');
                    $arrdata = $this->siupmb_act->get_fourth($dir, $doc, $type, $id);
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/fourth', $arrdata, true);
                    $this->index();
                } elseif ($dir == "03") {
                    if ($doc == '11') {
                        $this->load->model('licensing/siupbb_act');
                        $arrdata = $this->siupbb_act->get_fourth($dir, $doc, $type, $id);
                    } elseif ($doc == '9') {
                        $this->load->model('licensing/sppgap_act');
                        $arrdata = $this->sppgap_act->get_fourth($dir, $doc, $type, $id);
                    } elseif ($doc == '10') {
                        $this->load->model('licensing/sppgrap_act');
                        $arrdata = $this->sppgrap_act->get_fourth($dir, $doc, $type, $id);
                    } else if ($doc == "22") {
                        $this->load->model('licensing/sipdis_act');
                        $arrdata = $this->sipdis_act->get_fourth($dir, $doc, $type, $id);
                    } elseif ($doc == '23') {
                        $this->load->model('licensing/label_act');
                        $arrdata = $this->label_act->get_fourth($dir, $doc, $type, $id);
                    }
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/fourth', $arrdata, true);
                    $this->index();
                }
            } elseif ($step == "fifth") {
                if ($dir == "01") {
                    if ($doc == "1") {
                        $this->load->model('licensing/siujs_act');
                        $arrdata = $this->siujs_act->get_lstfifth($dir, $doc, $type, $id);
                    } elseif (($doc == "18") || ($doc == "17") || ($doc == "19") || ($doc == "20") || ($doc == "21")) {
                        $this->load->model('licensing/stpw_act');
                        $arrdata = $this->stpw_act->get_fifth($dir, $doc, $type, $id);
                    } elseif ($doc == "12") {
                        $this->load->model('licensing/siupagen_act');
                        $arrdata = $this->siupagen_act->get_fifth($dir, $doc, $type, $id);
                    }elseif ($doc == "24") {
                        $this->load->model('licensing/siupl_act');
                        $arrdata = $this->siupl_act->get_fifth($dir, $doc, $type, $id);
                    }
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/fifth', $arrdata, true);
                    $this->index();
                }
            } elseif ($step == "sixth") {
                if ($dir == "01") {
                    if ($doc == "1") {
                        $this->load->model('licensing/siujs_act');
                        $arrdata = $this->siujs_act->get_sixth($dir, $doc, $type, $id);
                    }
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/sixth', $arrdata, true);
                    $this->index();
                }
            } elseif ($step == "importir") {
                if ($dir == "01") {
                    if ($doc == "14") {
                        $this->load->model('licensing/garansi_act');
                        $arrdata = $this->garansi_act->get_importir($dir, $doc, $type, $id);
                    }
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/importir', $arrdata, true);
                    $this->index();
                }
            }
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    function table($dir, $doc, $type, $id) {
        $table2 = $this->newtable;
        $query2 = "SELECT id, nama_produsen AS 'Nama Distributor', alamat AS 'Alamat' FROM t_sppgapt_distributor
				  	WHERE permohonan_id = '" . $id . "'";

        $arr['num_rows2'] = $this->db->query($query2)->num_rows();
        $table2->title("");
        $table2->columns(array("id", "nama_produsen", "alamat"));
        $table2->width(array('Nama Distributor' => 100, 'Alamat' => 300));
        $table2->search(array(array("a.nama_produsen", "Nama Distributor")));
        $table2->cidb($this->db);
        $table2->ciuri($this->uri->segment_array());
        $table2->action(site_url() . "/licensing/form/third/" . $dir . "/" . $doc . "/" . $type . "/" . $id);
        $table2->orderby(1);
        $table2->sortby("ASC");
        $table2->keys(array("id"));
        $table2->hiddens(array("id"));
        $table2->show_search(TRUE);
        $table2->show_chk(TRUE);
        $table2->single(TRUE);
        $table2->dropdown(TRUE);
        $table2->hashids(TRUE);
        $table2->postmethod(TRUE);
        $table2->tbtarget("refDistributor");
        $table2->menu(array('Tambah' => array('GET', site_url() . 'licensing/view/distributor/' . $doc . '/' . $id, '0', 'home'),
            'Edit' => array('GET', site_url() . 'licensing/view/distributor/' . $doc . '/' . $id, '1', 'fa fa-edit')));
        $arr['lstDistributor'] = $table2->generate($query2);
        //print_r($arrdata);die();
        //if($this->input->post("data-post")) return $table2->generate($query2);
        //else return $arr;
        return $arr;
    }

    public function preview($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($dir == "01") {
                if ($doc == "1") {
                    $this->load->model('licensing/siujs_act');
                    $arrdata = $this->siujs_act->get_preview($dir, $doc, $type, $id);
                } elseif ($doc == "2") {
                    $this->load->model('licensing/siup4_act');
                    $arrdata = $this->siup4_act->get_preview($dir, $doc, $type, $id);
                } elseif ($doc == "12") {
                    $this->load->model('licensing/siupagen_act');
                    $arrdata = $this->siupagen_act->get_preview($dir, $doc, $type, $id);
                } elseif ($doc == "16") {
                    $this->load->model('licensing/pameran_act');
                    $arrdata = $this->pameran_act->get_preview($dir, $doc, $type, $id);
                } elseif ($doc == "15") {
                    $this->load->model('licensing/pameran_act');
                    $arrdata = $this->pameran_act->get_preview($dir, $doc, $type, $id);
                } elseif (($doc == "19") || ($doc == "18") || ($doc == "17") || ($doc == "20") || ($doc == "21")) {
                    $this->load->model('licensing/stpw_act');
                    $arrdata = $this->stpw_act->get_preview($dir, $doc, $type, $id);
                } elseif ($doc == "14") {
                    $this->load->model('licensing/garansi_act');
                    $arrdata = $this->garansi_act->get_preview($dir, $doc, $type, $id);
                }
                $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/preview', $arrdata, true);
                $this->index();
            } elseif ($dir == "02") {
                if ($doc == "7") {
                    $this->load->model('licensing/pkapt_act');
                    $arrdata = $this->pkapt_act->get_preview($dir, $doc, $type, $id);
                } else {
                    $this->load->model('licensing/siupmb_act');
                    $arrdata = $this->siupmb_act->get_preview($dir, $doc, $type, $id);
                }
                $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/preview', $arrdata, true);
                $this->index();
            } elseif ($dir == "03") {
                if ($doc == "11") {
                    $this->load->model('licensing/siupbb_act');
                    $arrdata = $this->siupbb_act->get_preview($dir, $doc, $type, $id);
                } elseif ($doc == "7") {
                    $this->load->model('licensing/pkapt_act');
                    $arrdata = $this->pkapt_act->get_preview($dir, $doc, $type, $id);
                } elseif ($doc == "8") {
                    $this->load->model('licensing/pgapt_act');
                    $arrdata = $this->pgapt_act->get_preview($dir, $doc, $type, $id);
                } else if ($doc == "9") {
                    $this->load->model('licensing/sppgap_act');
                    $arrdata = $this->sppgap_act->get_preview($dir, $doc, $type, $id);
                } else if ($doc == "10") {
                    $this->load->model('licensing/sppgrap_act');
                    $arrdata = $this->sppgrap_act->get_preview($dir, $doc, $type, $id);
                } else if ($doc == "22") {
                    $this->load->model('licensing/sipdis_act');
                    $arrdata = $this->sipdis_act->get_preview($dir, $doc, $type, $id);
                } else if ($doc == "23") {
                    $this->load->model('licensing/label_act');
                    $arrdata = $this->label_act->get_preview($dir, $doc, $type, $id);
                }
                $this->content = $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/preview', $arrdata, true);
                $this->index();
            }
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function preview_regis($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('registrasi_act');
            $arrdata = $this->registrasi_act->get_preview($id);
            //print_r($arrdata);exit();
            $this->content = $this->load->view($this->ineng . '/backend/preview_regis', $arrdata, true);
            $this->index();
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function edit_regis($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('registrasi_act');
            $arrdata = $this->registrasi_act->edit_regis($id);
            //print_r($arrdata);exit();
            $this->content = $this->load->view($this->ineng . '/backend/edit_regis', $arrdata, true);
            $this->index();
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function edit_pemroses($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('registrasi_act');
            $arrdata = $this->registrasi_act->get_pemroses($id);
            //print_r($arrdata);exit();
            $this->content = $this->load->view($this->ineng . '/backend/pemroses', $arrdata, true);
            $this->index();
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function update_regis($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('registrasi_act');
            $arrdata = $this->registrasi_act->update_regis($isajax);
            echo $arrdata;
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function tolak_regis($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('registrasi_act');
            $arrdata = $this->registrasi_act->set_tolak($isajax);
            echo $arrdata;
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function check_username() {
        $temp = $_POST['data'];
        $this->load->model('registrasi_act');
        $arrdata = $this->registrasi_act->get_vaidate($temp);
        echo json_encode($arrdata);
    }

    public function get_per($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/licensing_act');
            $arrdata = $this->licensing_act->get_per();
            //print_r($arrdata);exit();
            $this->content = $this->load->view($this->ineng . '/backend/licensing/maintance_per', $arrdata, true);
            $this->index();
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function delete_lic() {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/licensing_act');
            $arrdata = $this->licensing_act->del_lic();
        }
    }

    public function set_per($id, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/licensing_act');
            $arrdata = $this->licensing_act->set_per($id, $isajax);
            echo $arrdata;
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function report($dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $izin = array("5", "6", "7", "11", "13", "14", "15", "16", "17", "18", "19", "20", "21", "12");
            $flag = '';
            if ($dir == "01") {
                if ($doc == "1") {
                    $this->load->model('report/siujs_act');
                    $arrdata['lstLapSiujs'] = $this->siujs_act->get_lstLapSiujs($dir, $doc, $type, $id);
                    if ($this->input->post("data-post")) {
                        echo $this->siujs_act->get_lstLapSiujs($dir, $doc, $type, $id);
                        exit();
                    }
                    //$arrdata = $this->siujs_act->get_lstLapSiujs($dir, $doc, $type, $id);
                } elseif ($doc == "2") {
                    $this->load->model('report/siup4_act');
                    $arrdata['lstLapSiup4'] = $this->siup4_act->get_lstLapSiup4($dir, $doc, $type, $id);
                    if ($this->input->post("data-post")) {
                        echo $this->siup4_act->get_lstLapSiup4($dir, $doc, $type, $id);
                        exit();
                    }
                } elseif (in_array($doc, $izin)) {
                    $this->load->model('report/pelaporan_act');
                    $arrdata = $this->pelaporan_act->get_preview($dir, $doc, $type, $id);
                    $flag = '1';
                }
                if ($flag == '') {
                    $this->content = $this->load->view($this->ineng . '/backend/report/' . $dir . '/' . $doc . '/report', $arrdata, true);
                } else {
                    if ($this->input->post("data-post")) {
                        echo $arrdata;
                    } else {
                        $this->content = $this->load->view('browse/grid', $arrdata, true);
                    }
                }
                $this->index();
            } elseif ($dir == "02") {
                if ($doc == "4" || $doc == "3" || $doc == "5") {
                    $explode = explode(".", $id);
                    $this->load->model('report/siupmb_act');
                    $arrdata['lstPengadaan'] = $this->siupmb_act->get_lstPengadaan($dir, $doc, $type, $explode[0]);
                    $arrdata['lstPenyaluran'] = $this->siupmb_act->get_lstPenyaluran($dir, $doc, $type, $explode[0]);
                    if ($this->input->post("data-post")) {

                        if ($explode[1] == "pengadaan") {
                            echo $this->siupmb_act->get_lstPengadaan($dir, $doc, $type, $explode[0]);
                            exit();
                        } else {
                            echo $this->siupmb_act->get_lstPenyaluran($dir, $doc, $type, $explode[0]);
                            exit();
                        }
                    }
                } elseif (in_array($doc, $izin)) {
                    $this->load->model('report/pelaporan_act');
                    $arrdata = $this->pelaporan_act->get_preview($dir, $doc, $type, $id);
                    $flag = '1';
                }
                if ($flag == '') {
                    $this->content = $this->load->view($this->ineng . '/backend/report/' . $dir . '/' . $doc . '/report', $arrdata, true);
                } else {
                    if ($this->input->post("data-post")) {
                        echo $arrdata;
                    } else {
                        $this->content = $this->load->view('browse/grid', $arrdata, true);
                    }
                }
                $this->index();
            } elseif ($dir == "03") {
                if ($doc == "8") {
                    $this->load->model('report/pgapt_act');
                    $arrdata = $this->pgapt_act->get_preview($dir, $doc, $type, $id);
                } else if ($doc == "10") {
                    $this->load->model('report/sppgrapt_act');
                    $arrdata = $this->sppgrapt_act->get_preview($dir, $doc, $type, $id);
                    $flag = '1';
                } else if ($doc == "22") {
                    $this->load->model('report/sipdis_act');
                    $arrdata = $this->sipdis_act->get_preview($dir, $doc, $type, $id);
                    $flag = '1';
                } elseif (in_array($doc, $izin)) {
                    $this->load->model('report/pelaporan_act');
                    $arrdata = $this->pelaporan_act->get_preview($dir, $doc, $type, $id);
                    $flag = '1';
                }

                if ($flag == '') {
                    $this->content = $this->load->view($this->ineng . '/backend/report/' . $dir . '/' . $doc . '/report', $arrdata, true);
                } else {
                    if ($this->input->post("data-post")) {
                        echo $arrdata;
                    } else {
                        $this->content = $this->load->view('browse/grid', $arrdata, true);
                        $this->index();
                    }
                }
            }
        } else {
            redirect(site_url('portal'));
            exit();
        }
    }

    public function view($menu, $sub = "", $id = "", $dtl) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($menu == "requirements") {#List Perysaratan Perizinan dan Direktorat
                $this->load->model('licensing/licensing_act');
                $arrdata = $this->licensing_act->get_licensing();
                $this->content = $this->load->view($this->ineng . '/backend/licensing/requirements', $arrdata, true);
                $this->index();
            } else if ($menu == "status") {#List Penelusuran Dokumen Perijinan (Pelaku Usaha)
                //	print_r($_SESSION);die();
                $this->load->model('licensing/status_act');
                $arrdata = $this->status_act->list_status();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                    //var_dump($arrdata);die();
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "agen") {#List Penelusuran Dokumen Perijinan (Pelaku Usaha)
                $this->load->model('agen_act');
                $arrdata = $this->agen_act->get_agen();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "add_requirements") {#List Penelusuran Dokumen Perijinan (Pelaku Usaha)
                $this->load->model('licensing/licensing_act');
                $arrdata = $this->licensing_act->view_licensing();
                $this->content = $this->load->view($this->ineng . '/backend/licensing/add_req', $arrdata, true);
                $this->index();
            } else if ($menu == "persyaratan") {#List Penelusuran Dokumen Perijinan (Pelaku Usaha)
                $this->load->model('licensing/licensing_act');
                $arrdata = $this->licensing_act->master_view();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "all") {
                $this->load->model('licensing/status_act');
                $arrdata = $this->status_act->statusAll();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "report") {#List Penelusuran Dokumen Laporan (Pelaku Usaha)
                $this->load->model('licensing/status_act');
                $arrdata = $this->status_act->list_report();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "request") {//registrasi pelaku usaha baru
                $this->load->model('registrasi_act');
                $arrdata = $this->registrasi_act->list_request();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('id/backend/list_request', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "pemroses") {//registrasi pelaku usaha baru
                $this->load->model('registrasi_act');
                $arrdata = $this->registrasi_act->list_pemroses();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "jns_minol") {
                $this->load->model('licensing/siupmb_act');
                $arrdata = $this->siupmb_act->list_minol();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "news") {//registrasi pelaku usaha baru
                $this->load->model('news_act');
                $arrdata = $this->news_act->list_news();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "lst_agen") {
                $this->load->model('licensing/siupagen_act');
                $arrdata = $this->siupagen_act->get_listagen();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "inbox") {#List Penerimaan Dokumen Perijinan - Internal Kemendag
                $this->load->model('licensing/status_act');
                $arrdata = $this->status_act->list_inbox();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "proccess") {#List Proses Dokumen Perijinan - Internal Kemendag 
                $this->load->model('licensing/status_act');
                $arrdata = $this->status_act->list_proccess();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "rollback") {#List Rollback Dokumen Perijinan - Internal Kemendag 
                $this->load->model('licensing/status_act');
                $arrdata = $this->status_act->list_rollback();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "issued") {#Penerbitan Dokumen di Pemroses Subdit.
                $this->load->model('licensing/status_act');
                $arrdata = $this->status_act->list_issued();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "released") {#Penyerahan Dokumen ke UPP.
                $this->load->model('licensing/status_act');
                $arrdata = $this->status_act->list_released();
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view('browse/grid', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "tenaga_ahli") {
                if ($sub == "2") {
                    $this->load->model('licensing/siup4_act');
                    $arrdata = $this->siup4_act->get_tenaga_ahli($sub, $id, $dtl);
                } elseif ($sub == "1") {
                    $this->load->model('licensing/siujs_act');
                    $arrdata = $this->siujs_act->get_tenaga_ahli($sub, $id, $dtl);
                }
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/01/' . $sub . '/tenaga_ahli', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "cabang") {
                if ($sub == "1") {
                    $this->load->model('licensing/siujs_act');
                    $arrdata = $this->siujs_act->get_cabang($id, $dtl);
                }
                if ($this->input->post("data-post")) {
                    echo $arrdata;
                } else {
                    $this->content = $this->load->view($this->ineng . '/backend/licensing/01/' . $sub . '/cabang', $arrdata, true);
                    $this->index();
                }
            } else if ($menu == "document") {#List Dokumen Pendukung
                $this->load->model('document/supporting_act');
                if ($sub == "") {
                    $arrdata = $this->supporting_act->list_supporting();
                    if ($this->input->post("data-post")) {
                        echo $arrdata;
                    } else {
                        $this->content = $this->load->view('browse/grid', $arrdata, true);
                        $this->index();
                    }
                } else if ($sub == "new") {
                    $arrdata = $this->supporting_act->get_document($id);
                    echo $this->load->view($this->ineng . '/backend/document/supporting-form', $arrdata, true);
                }
            } else if ($menu == "produsen") {
                //print_r($menu."-".$sub."-".$id."-".$dtl);die();
                $this->load->model('licensing/sppgrap_act');
                $arrdata = $this->sppgrap_act->get_produsen($id, $dtl);
                echo $this->load->view($this->ineng . '/backend/licensing/03/10/fprodusen', $arrdata, true);
            } else if ($menu == "po") {
                //print_r($menu."-".$sub."-".$id."-".$dtl);die();
                $this->load->model('licensing/sppgrap_act');
                $arrdata = $this->sppgrap_act->get_po($id, $dtl);
                echo $this->load->view($this->ineng . '/backend/licensing/03/10/fpo', $arrdata, true);
            } else if ($menu == "distributor") {
                $this->load->model('licensing/sppgrap_act');
                $arrdata = $this->sppgrap_act->get_distributor($id, $dtl);
                echo $this->load->view($this->ineng . '/backend/licensing/03/10/fdistributor', $arrdata, true);
            } else if ($menu == "pameran_event") {
                $this->load->model('licensing/pameran_act');
                $arrdata = $this->pameran_act->get_pameran_event($id, $dtl);
                echo $this->load->view($this->ineng . '/backend/licensing/01/' . $sub . '/fpameranevent', $arrdata, true);
            } else {
                redirect(base_url() . 'home', refresh);
            }
        }
    }

    public function proses() {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('dashboard_act');
            $arrdata = $this->dashboard_act->get_proses();
            $this->content = $this->load->view('/browse/grid', $arrdata, true);
            $this->index();
        }
    }

    public function terbit() {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('dashboard_act');
            $arrdata = $this->dashboard_act->get_terbit();
            $this->content = $this->load->view('/browse/grid', $arrdata, true);
            $this->index();
        }
    }

    public function choice($pelaporan = '') {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/status_act');
            $arrdata = $this->status_act->get_choice($pelaporan);
            echo $this->load->view($this->ineng . '/backend/licensing/choice', $arrdata, true);
        }
    }

    public function manual() {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('manual_act');
            $arrdata = $this->manual_act->get_manual();
            echo $this->load->view($this->ineng . '/backend/licensing/manual', $arrdata, true);
        }
    }

    public function laporan($menu, $ajax, $dtl = '') {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($menu == "siujs") {
                $this->load->model('report/siujs_act');
                $data = $this->siujs_act->set_laporan($_POST['tb_chk']);
            } elseif ($menu == "siup4") {
                $this->load->model('report/siup4_act');
                $data = $this->siup4_act->set_laporan($_POST['tb_chk']);
            } elseif ($menu == "siupmb") {
                $this->load->model('report/siupmb_act');
                $data = $this->siupmb_act->set_laporan($_POST['tb_chk'], $dtl);
            } elseif ($menu == "sppgrapt") {
                $this->load->model('report/sppgrapt_act');
                $data = $this->sppgrapt_act->set_laporan($_POST['tb_chk']);
            } elseif ($menu == "sipdis") {
                $this->load->model('report/sipdis_act');
                $data = $this->sipdis_act->set_laporan($_POST['tb_chk']);
            } elseif ($menu == "all") {
                $this->load->model('report/pelaporan_act');
                $data = $this->pelaporan_act->set_laporan($_POST['tb_chk']);
            }
            if ($ajax != "ajax") {
                redirect(base_url());
            }
            echo $data;
        }
    }

    public function garansi($post, $dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/garansi_act');
            $arrdata = $this->garansi_act->get_garansi($post);
            if ($this->input->post("data-post")) {
                echo $this->garansi_act->get_third($dir, $doc, $type, $id);
            } else {
                echo $this->load->view($this->ineng . '/backend/licensing/garansi', $arrdata, true);
            }
        }
    }

    public function persyaratan($act) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/licensing_act');
            $arrdata = $this->licensing_act->get_new();
            //print_r($arrdata);exit();
            echo $this->load->view($this->ineng . '/backend/licensing/persyaratan', $arrdata, true);
        }
    }

    public function new_pemroses($act) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('registrasi_act');
            $arrdata = $this->registrasi_act->get_pemroses();
            //print_r($arrdata);exit();
            $this->content = $this->load->view($this->ineng . '/backend/pemroses', $arrdata, true);
            $this->index();
        }
    }

    public function importir($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/garansi_act');
            $arrdata = $this->garansi_act->get_importir_1($id);
            echo $this->load->view($this->ineng . '/backend/licensing/importir', $arrdata, true);
        }
    }

    public function add_minol($act) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/siupmb_act');
            $arrdata = $this->siupmb_act->add_minol($act);
            echo $this->load->view($this->ineng . '/backend/licensing/add_minol', $arrdata, true);
        }
    }

    public function del_garansi() {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->load->model('licensing/garansi_act');
                $data = $this->garansi_act->delete_garansi($_POST['tb_chk']);
                echo $data;
            }
        }
    }

    public function del_importir() {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->load->model('licensing/garansi_act');
                $data = $this->garansi_act->delete_importir($_POST['tb_chk']);
                echo $data;
            }
        }
    }

    public function del_produsen($menu, $isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->load->model('licensing/sppgrap_act');
                $data = $this->sppgrap_act->delete_produsen($_POST['tb_chk'], $menu);
                if ($isajax != "ajax") {
                    redirect(base_url());
                }
                echo $data;
            }
        }
    }

    public function delete_tenaga_ahli() {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->load->model('licensing/siujs_act');
                $data = $this->siujs_act->delete_ahli($_POST['tb_chk']);
                echo $data;
            }
        }
    }

    public function delete_data_cabang() {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->load->model('licensing/siujs_act');
                $data = $this->siujs_act->delete_cabang($_POST['tb_chk']);
                echo $data;
            }
        }
    }

    public function delete_pameran() {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->load->model('licensing/pameran_act');
                $data = $this->pameran_act->delete_pameran($_POST['tb_chk']);
                echo $data;
            }
        }
    }

    public function del_siup4($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->load->model('licensing/siup4_act');
                $data = $this->siup4_act->delete_siup4($_POST['tb_chk']);
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $data;
        }
    }

    public function dialog_garansi($id, $post) {
        if ($this->newsession->userdata('_LOGGED')) {
            //print_r('sds'.$this->uri->segment(3));die();
            $this->load->model('licensing/garansi_act');
            $arrdata = $this->garansi_act->get_garansi($id, $post);
            echo $this->load->view($this->ineng . '/backend/licensing/garansi', $arrdata, true);
        }
    }

    public function dialog_persyaratan($id, $post) {
        if ($this->newsession->userdata('_LOGGED')) {
            //print_r('sds'.$this->uri->segment(3));die();
            $this->load->model('licensing/licensing_act');
            $arrdata = $this->licensing_act->get_new($id, $post);
            echo $this->load->view($this->ineng . '/backend/licensing/persyaratan', $arrdata, true);
        }
    }

    public function dialog_minol($id, $post) {
        if ($this->newsession->userdata('_LOGGED')) {
            //print_r('sds'.$this->uri->segment(3));die();
            $this->load->model('licensing/siupmb_act');
            $arrdata = $this->siupmb_act->add_minol($id, $post);
            echo $this->load->view($this->ineng . '/backend/licensing/add_minol', $arrdata, true);
        }
    }

    public function dialog_importir($id, $post) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/garansi_act');
            $arrdata = $this->garansi_act->get_importir_1($id, $post);
            echo $this->load->view($this->ineng . '/backend/licensing/importir', $arrdata, true);
        }
    }

    public function delete_status($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->load->model('licensing/status_act');
                $data = $this->status_act->del_status($_POST['tb_chk']);
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $data;
        }
    }

    public function delete_inbox($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->load->model('licensing/status_act');
                $data = $this->status_act->del_inbox($_POST['tb_chk']);
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $data;
        }
    }

    public function delete_persyaratan($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->load->model('licensing/licensing_act');
                $data = $this->licensing_act->del_persyaratan($_POST['tb_chk']);
            }
            echo $data;
        }
    }

    public function delete_pemroses($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->load->model('registrasi_act');
                $data = $this->registrasi_act->del_pemroses($_POST['tb_chk']);
            }
            echo $data;
        }
    }

    public function delete_minol($isajax) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->load->model('licensing/siupmb_act');
                $data = $this->siupmb_act->del_minol($_POST['tb_chk']);
            }
            echo $data;
        }
    }

    public function delete_laporan($menu, $isajax, $tabel = '', $id = '') {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                if ($menu == "siujs") {
                    $this->load->model('report/siujs_act');
                    $data = $this->siujs_act->del_laporan($_POST['tb_chk']);
                } elseif ($menu == "siup4") {
                    $this->load->model('report/siup4_act');
                    $data = $this->siup4_act->del_laporan($_POST['tb_chk']);
                } elseif ($menu == "siupmb") {
                    $this->load->model('report/siupmb_act');
                    $data = $this->siupmb_act->del_laporan($_POST['tb_chk'], $tabel);
                } elseif ($menu == "sppgrapt") {
                    $this->load->model('report/sppgrapt_act');
                    $data = $this->sppgrapt_act->del_laporan($_POST['tb_chk'], $tabel);
                } elseif ($menu == "sipdis") {
                    $this->load->model('report/sipdis_act');
                    $data = $this->sipdis_act->del_laporan($_POST['tb_chk'], $tabel);
                } elseif ($menu == "laporan") {
                    $this->load->model('report/pelaporan_act');
                    $data = $this->pelaporan_act->del_laporan($_POST['tb_chk'], $tabel);
                }
            } else {
                if ($menu == "laporan") {
                    $this->load->model('report/pelaporan_act');
                    $data = $this->pelaporan_act->del_laporan($id, $tabel);
                }
            }
            if ($isajax != "ajax") {
                redirect(base_url());
            }
            echo $data;
        }
    }

    public function edit_form($step, $dir, $doc, $type, $id) {
        if ($this->newsession->userdata('_LOGGED')) {
            if ($step == "first") {
                if ($dir == "01") {
                    if ($doc == "1") {
                        $this->load->model('licensing/siujs_act');
                        $arrdata = $this->siujs_act->get_first($dir, $doc, $type, $id);
                    } elseif ($doc == "2") {
                        $this->load->model('licensing/siup4_act');
                        $arrdata = $this->siup4_act->get_first($dir, $doc, $type, $id);
                    } elseif ($doc == "12") {
                        $this->load->model('licensing/siupagen_act');
                        $arrdata = $this->siupagen_act->get_first($dir, $doc, $type, $id);
                    }
                    echo $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/form', $arrdata, true);
                } elseif ($dir == "02") {
                    $this->load->model('licensing/siupmb_act');
                    $arrdata = $this->siupmb_act->get_first($dir, $doc, $type, $id);
                    echo $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/form', $arrdata, true);
                }
            } elseif ($step == "second") {
                if ($dir == "01") {
                    if ($doc == "1") {
                        $this->load->model('licensing/siujs_act');
                        $arrdata = $this->siujs_act->get_second($dir, $doc, $type, $id);
                    } elseif ($doc == "2") {
                        $this->load->model('licensing/siup4_act');
                        $arrdata = $this->siup4_act->get_second($dir, $doc, $type, $id);
                    } elseif ($doc == "12") {
                        $this->load->model('licensing/siupagen_act');
                        $arrdata = $this->siupagen_act->get_second($dir, $doc, $type, $id);
                    }
                    echo $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/second', $arrdata, true);
                } elseif ($dir == "02") {
                    $this->load->model('licensing/siupmb_act');
                    $arrdata = $this->siupmb_act->get_second($dir, $doc, $type, $id);
                    echo $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/second', $arrdata, true);
                }
            } elseif ($step == "third") {
                if ($dir == "01") {
                    if ($doc == "1") {
                        $this->load->model('licensing/siujs_act');
                        $arrdata = $this->siujs_act->get_third($dir, $doc, $type, $id);
                    } elseif ($doc == "2") {
                        $this->load->model('licensing/siup4_act');
                        $arrdata = $this->siup4_act->get_lstthird($dir, $doc, $type, $id);
                    } elseif ($doc == "12") {
                        $this->load->model('licensing/siupagen_act');
                        $arrdata = $this->siupagen_act->get_third($dir, $doc, $type, $id);
                    }
                    echo $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/third', $arrdata, true);
                } elseif ($dir == "02") {
                    $this->load->model('licensing/siupmb_act');
                    $arrdata = $this->siupmb_act->get_third($dir, $doc, $type, $id);
                    echo $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/third', $arrdata, true);
                }
            } elseif ($step == "fourth") {
                if ($dir == "01") {
                    if ($doc == "1") {
                        $this->load->model('licensing/siujs_act');
                        $arrdata = $this->siujs_act->get_lstfourth($dir, $doc, $type, $id);
                    } elseif ($doc == "2") {
                        $this->load->model('licensing/siup4_act');
                        $arrdata = $this->siup4_act->get_fourth($dir, $doc, $type, $id);
                    } elseif ($doc == "12") {
                        $this->load->model('licensing/siupagen_act');
                        $arrdata = $this->siupagen_act->get_fourth($dir, $doc, $type, $id);
                    }
                    echo $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/fourth', $arrdata, true);
                } elseif ($dir == "02") {
                    $this->load->model('licensing/siupmb_act');
                    $arrdata = $this->siupmb_act->get_fourth($dir, $doc, $type, $id);
                    echo $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/fourth', $arrdata, true);
                }
            } elseif ($step == "fifth") {
                if ($dir == "01") {
                    if ($doc == "1") {
                        $this->load->model('licensing/siujs_act');
                        $arrdata = $this->siujs_act->get_lstfifth($dir, $doc, $type, $id);
                        echo $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/fifth', $arrdata, true);
                    }
                }
            } elseif ($step == "sixth") {
                if ($dir == "01") {
                    if ($doc == "1") {
                        $this->load->model('licensing/siujs_act');
                        $arrdata = $this->siujs_act->get_sixth($dir, $doc, $type, $id);
                        echo $this->load->view($this->ineng . '/backend/licensing/' . $dir . '/' . $doc . '/sixth', $arrdata, true);
                    }
                }
            }
        }
    }

    public function popup_kbli($target, $callback, $fieldcallback) {
        $target = str_replace('%7B', '{', str_replace('%7D', '}', str_replace('%7C', '|', $target)));
        $callback = str_replace('%7B', '{', str_replace('%7D', '}', $callback));
        $this->load->model("licensing/siupmb_act");
        $arrdata = $this->siupmb_act->referensi_kbli($target, $callback, $fieldcallback);
        if ($this->input->post("data-post")) {
            echo str_replace(chr(10), '', $arrdata);
        } else {
            echo $this->load->view('browse/pop-up-tabel', preg_replace('#(?(?!<!--.*?-->)(?: {2,}|[\r\n\t]+)|(<!--.*?-->))#s', '$1', $arrdata), true);
        }
    }

    public function popup_spp($target, $callback, $fieldcallback) {
        $target1 = str_replace("{", "%7B", $target);
        $target2 = str_replace("}", "%7B", $target1);
        $callback = str_replace('%7B', '{', str_replace('%7D', '}', $callback));
        $this->load->model("licensing/sppgap_act");
        $arrdata = $this->sppgap_act->referensi_spp($target2, $callback, $fieldcallback);
        if ($this->input->post("data-post")) {
            echo str_replace(chr(10), '', $arrdata);
        } else {
            echo $this->load->view('browse/pop-up-tabel', preg_replace('#(?(?!<!--.*?-->)(?: {2,}|[\r\n\t]+)|(<!--.*?-->))#s', '$1', $arrdata), true);
        }
    }

    public function popup_syarat($id, $target, $callback, $fieldcallback, $permohonan_id = "", $multiple = "", $putin = "", $doc = "") {//print_r($id.' -- '.$target.' -- '.$callback.' -- '.$fieldcallback.' -- '.$permohonan_id.' -- '.$multiple.' -- '.$putin.' -- '.$doc);die();
        $target = str_replace('%7B', '{', str_replace('%7D', '}', str_replace('%7C', '|', $target)));
        $callback = str_replace('%7B', '{', str_replace('%7D', '}', $callback));
        $this->load->model("licensing/siupmb_act");
        $arrdata = $this->siupmb_act->list_dok($target, $callback, $fieldcallback, $id, $multiple, $putin, $doc, $permohonan_id);
        if ($this->input->post("data-post")) {
            echo str_replace(chr(10), '', $arrdata);
        } else {
            echo $this->load->view('browse/pop-up-tabel', preg_replace('#(?(?!<!--.*?-->)(?: {2,}|[\r\n\t]+)|(<!--.*?-->))#s', '$1', $arrdata), true);
        }
    }

    public function popup_ahli($id, $doc, $permohonan_id) {
        $IDx = hashids_decrypt($id, _HASHIDS_, 9);
        if ($doc == "1") {
            $this->load->model("licensing/siujs_act");
            $arrdata = $this->siujs_act->list_dok_ahli($id, $doc, $permohonan_id);
        } elseif ($doc == "2") {
            $this->load->model("licensing/siup4_act");
            $arrdata = $this->siup4_act->list_dok_ahli($id, $doc, $permohonan_id);
        }
        if ($this->input->post("data-post")) {
            echo str_replace(chr(10), '', $arrdata);
        } else {
            echo $this->load->view('browse/pop-up-tabel', preg_replace('#(?(?!<!--.*?-->)(?: {2,}|[\r\n\t]+)|(<!--.*?-->))#s', '$1', $arrdata), true);
        }
    }

    public function tabs() {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/licensing_act');
            $arrdata = $this->licensing_act->get_licensing();
            $this->content = $this->load->view('test', $arrdata, true);
            $this->index();
        }
    }

    public function append($id, $doc, $permohonan_id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/siujs_act');
            $arrdata = $this->siujs_act->get_dtl_cabang($id, $permohonan_id);
            //print_r($arrdata);die();
            echo $this->load->view($this->ineng . '/backend/licensing/01/' . $doc . '/cabang_dtl', $arrdata, true);
        }
    }

    public function popup_tembusan($izin_id, $permohonan_id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/status_act');
            $arrdata = $this->status_act->tembusan($izin_id, $permohonan_id);
            echo $this->load->view($this->ineng . '/backend/licensing/tembusan', $arrdata, true);
        }
    }

    public function popup_log($izin_id, $permohonan_id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/status_act');
            $arrdata = array(); //$this->status_act->tembusan($izin_id, $permohonan_id);
            echo $this->load->view($this->ineng . '/backend/licensing/tembusan', $arrdata, true);
        }
    }

    public function popup_old_doc($dir, $doc, $target, $callback, $fieldcallback) {
        $target = str_replace('%7B', '{', str_replace('%7D', '}', str_replace('%7C', '|', $target)));
        $callback = str_replace('%7B', '{', str_replace('%7D', '}', $callback));
        if ($dir == '01') {
            if ($doc == '1') {
                $this->load->model("licensing/siupjs_act");
                $arrdata = $this->siupjs_act->referensi_old_doc($target, $callback, $fieldcallback, $dir, $doc);
            } elseif ($doc == '2') {
                $this->load->model("licensing/siup4_act");
                $arrdata = $this->siup4_act->referensi_old_doc($target, $callback, $fieldcallback, $dir, $doc);
            } elseif ($doc == '12') {
                $this->load->model("licensing/siupagen_act");
                $arrdata = $this->siupagen_act->referensi_old_doc($target, $callback, $fieldcallback, $dir, $doc);
            }
        } elseif ($dir == '02') {
            $this->load->model("licensing/siupmb_act");
            $arrdata = $this->siupmb_act->referensi_old_doc($target, $callback, $fieldcallback, $dir, $doc);
        } elseif ($dir == '03') {
            $this->load->model("licensing/siupbb_act");
            $arrdata = $this->siupbb_act->referensi_old_doc($target, $callback, $fieldcallback, $dir, $doc);
        }
        if ($this->input->post("data-post")) {
            echo str_replace(chr(10), '', $arrdata);
        } else {
            echo $this->load->view('browse/pop-up-tabel', preg_replace('#(?(?!<!--.*?-->)(?: {2,}|[\r\n\t]+)|(<!--.*?-->))#s', '$1', $arrdata), true);
        }
    }

    public function en($dir, $id, $negara, $step) {
        $this->load->model('licensing/stpw_act');
        $ret = $this->stpw_act->get_data($id, $negara, $step);
        if ($negara != "ID") {
            $this->load->view('id/backend/licensing/01/' . $dir . '/id', $ret);
        } else {
            $this->load->view('id/backend/licensing/01/' . $dir . '/idn', $ret);
        }
    }

    public function get_produsen($id) {
        $this->load->model('licensing/sppgrap_act');
        if ($this->input->post("data-post"))
            echo $this->sppgrap_act->get_lst_produsen($id);
    }

    public function get_distributor($id) {
        $this->load->model('licensing/sppgrap_act');
        if ($this->input->post("data-post"))
            echo $this->sppgrap_act->get_lst_distributor($id);
    }

    public function get_garansi($id) {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/garansi_act');
            if ($this->input->post("data-post"))
                echo $this->garansi_act->get_lst_garansi($id);
        }
    }

    public function add_report($menu, $dir, $doc, $permohonan_id, $id = '') {
        if ($this->newsession->userdata('_LOGGED')) {
            $arraySiupmb = array(3, 4, 5);
            $izin = array("6", "7", "11", "13", "14", "15", "16", "17", "18", "19", "20", "21", "12");
            $flag = '';
            if (in_array($doc, $arraySiupmb)) {
                $this->load->model('report/siupmb_act');
                $arrdata = $this->siupmb_act->get_data($menu, $dir, $doc, $permohonan_id, $id);
            } elseif ($doc == '1') {
                $this->load->model('report/siujs_act');
                $arrdata = $this->siujs_act->get_data($menu, $dir, $doc, $permohonan_id, $id);
            } elseif ($doc == '2') {
                $this->load->model('report/siup4_act');
                $arrdata = $this->siup4_act->get_data($menu, $dir, $doc, $permohonan_id, $id);
            } elseif ($doc == '10') {
                $this->load->model('report/sppgrapt_act');
                $arrdata = $this->sppgrapt_act->get_data($menu, $dir, $doc, $permohonan_id, $id);
            } elseif ($doc == '22') {
                $this->load->model('report/sipdis_act');
                $arrdata = $this->sipdis_act->get_data($menu, $dir, $doc, $permohonan_id, $id);
            } elseif (in_array($doc, $izin)) {
                $this->load->model('report/pelaporan_act');
                $arrdata = $this->pelaporan_act->get_data($menu, $dir, $doc, $permohonan_id, $id);
                $flag = '1';
            }
            if ($flag == '' && $doc != '22') {
                echo $this->load->view($this->ineng . '/backend/report/' . $dir . '/' . $doc . '/' . $menu, $arrdata, true);
            } elseif ($doc == '22') {
                $this->content = $this->load->view($this->ineng . '/backend/report/' . $dir . '/' . $doc . '/' . $menu, $arrdata, true);
                $this->index();
            } else {
                echo $this->load->view($this->ineng . '/backend/report/03/10/form_1', $arrdata, true);
            }
        }
    }

    public function cekNPWP() {
        if ($this->newsession->userdata('_LOGGED')) {
            $npwp = $_POST['a'];
            $sql = "select * from t_garansi where npwp='" . $npwp . "'";
            $r = $this->db->query($sql)->num_rows();
            if ($r != 0) {
                $arrdata = array();
                $arrdata['jenis_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff where jenis='JENIS_GARANSI'", "kode", "uraian", TRUE);
                $arrdata['negara_perusahaan'] = $this->main->set_combobox("SELECT kode, nama FROM m_negara ORDER BY 2", "kode", "nama", TRUE);
                $arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2", "id", "nama", TRUE);
                $arrdata['direktorat'] = $dir;
                $arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'", "kode", "uraian", TRUE);
                $arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN'", "kode", "uraian", TRUE);
                $sql = "SELECT nama_izin FROM m_izin WHERE id = '" . $doc . "' AND direktorat_id='" . $dir . "'";
                $nama_izin = $this->main->get_uraian($sql, 'nama_izin');
                $arrdata['nama_izin'] = $nama_izin;
                $arrdata['act'] = site_url('post/licensing/garansi_act/first/update');
                $query = "SELECT TOP(1) kd_izin,jensi_garansi,tipe_perusahaan, no_aju, tgl_aju, trader_id, tipe_permohonan, npwp, tipe_perusahaan, nm_perusahaan, almt_perusahaan, 
						kdprop, kdkab, kdkec, kdkel, kdpos, telp, fax
						FROM t_garansi
						WHERE npwp = '" . $npwp . "' AND no_izin IS NOT NULL ORDER BY tgl_izin DESC ";
                //	echo $query;die();
                $data = $this->main->get_result($query);
                if ($data) {
                    foreach ($query->result_array() as $row) {
                        $arrdata['sess'] = $row;
                    }
                    $arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '" . $row['kdprop'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '" . $row['kdkab'] . "' ORDER BY 2", "id", "nama", TRUE);
                    $arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '" . $row['kdkec'] . "' ORDER BY 2", "id", "nama", TRUE);
                } else {
                    echo json_encode(array("ERROR" => "KOSONG"));
                    die();
                }
                //print_r($arrdata);die();
                echo json_encode($arrdata);
            } else {
                echo json_encode(array("ERROR" => "KOSONG"));
            }
        }
    }

    function setNPWP($dir, $doc, $type, $id) {
        $this->load->model('licensing/garansi_act');
        $arrdata = $this->garansi_act->set_produk($dir, $doc, $type, $id);
        echo $arrdata;
    }

}
