<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    var $ineng = "";

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('site_lang')) {
            $this->lang->load('message', $this->session->userdata('site_lang'));
            $this->ineng = $this->session->userdata('site_lang');
        } else {
            $this->lang->load('message', 'id');
            $this->ineng = "id";
        }
    }

    public function index() {
        echo "Forbidden";
    }

    public function attempt($session, $isajax) {
        $ret = "";
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            $ret = "NO";
        }
        if ($ret == "") {
            $uname = $this->input->post('uid');
            $pass = $this->input->post('pwd');
            $keycode = $this->input->post('keycode');
            $this->load->model('login_act');
            $ret = $this->login_act->get_inatrade($uname, $pass, $keycode);
        }
        if ($isajax != "ajax") {
            redirect(base_url());
            exit();
        }
        echo $ret;
    }

    public function timeout($session, $isajax) {
        $ret = "";
        if (strtolower($_SERVER['REQUEST_METHOD']) != "post") {
            $ret = "NO";
        }
        if ($ret == "") {
            $uname = $this->input->post('uid');
            $pass = $this->input->post('pwd');
            $keycode = $this->input->post('keycode');
            $this->load->model('login_act');
            $ret = $this->login_act->get_internal($uname, $pass, $keycode);
        }
        if ($isajax != "ajax") {
            redirect(base_url());
            exit();
        }
        echo $ret;
    }

    public function internal() {
        echo $this->load->view($this->ineng . '/portal/login/internal', array(), true);
    }

    public function test_oss() {
        echo $url = "http://hub.kemendag.go.id/index.php/oss_services/inqueryniball/";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:MQbTaGOivtnc1y4ldD2JVf3EZWUhKsBYe6gHw5Lj'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'WEB-SIPT');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'data={"INQUERYNIBALL":{"nib":"[812000040066]","oss_id":"-"}}');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        $cek = json_decode($output);
        print_r($cek);
        die();
    }

}
