<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pulau extends CI_Controller{
	
	var $content = "";
	var $breadcumbs = "";
	var $ineng = "";
	
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }

    public function index(){
		if($this->newsession->userdata('_LOGGED')){
			$this->content = (!$this->content) ? $this->load->view($this->ineng.'/backend/default', '', true) : $this->content;
			$data = $this->main->set_content('backend', $this->breadcumbs, $this->content);
			$this->parser->parse($this->ineng.'/backend/home', $data);
		}else{
			redirect(site_url('portal'));
			exit();
		}
	}

    public function home(){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('report/pulau_act');
			$arrdata = $this->pulau_act->list_report();
			if($this->input->post("data-post")){
				echo $arrdata;
			}else{
				$this->content = $this->load->view('browse/grid', $arrdata, true);
				$this->index();
			}
		}
	}

	public function search_hs(){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('report/pulau_act');
			$arrdata = $this->pulau_act->search_hs();
			echo $arrdata;
		}
	}

	public function verif($resend=''){
		if($this->newsession->userdata('_LOGGED')){
			$arrdata = $this->input->post('data');
            $id = $arrdata['id'];
            $status = '0000';
            $this->load->model('report/pulau_act');
			if ($resend != 'resend') {
	            $ret = $this->pulau_act->verif_act();
			}else{
				$ret = "MSG||YES||Data Pelaporan Berhasil Dikirim Ulang||REFRESH";
			}

            $CI = & get_instance();
            $arrdata = $CI->pulau_act->set_prints($id, '1');
            $arrdata['cetak'] = 1;
            $arrmagin = array();
            ob_start();
            $classfile = APPPATH . "views/id/backend/report/pulau/cetakan.php";
            require_once($classfile);
            $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
            $objpdf->generate($arrdata);

            if ($status == '0000') {
            	$send_email_pelaku_usaha = $this->pulau_act->email_pelaku_usaha($status, $id);
            }else{
            	$send_email_pelaku_usaha = $this->pulau_act->email_pelaku_usaha($status, $id);
            }
			echo $ret;
		}
	}

	public function choice(){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('report/pulau_act');
			$arrdata = $this->pulau_act->get_choice();
			echo $this->load->view($this->ineng.'/backend/report/pulau/choice',$arrdata, true);
		}
	}

	public function add_eks(){
		$this->load->model('report/pulau_act');
		$arrdata = $this->pulau_act->form_ekspedisi();
		echo json_encode($arrdata);
	}

	public function cek_edit(){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('report/pulau_act');
			$arrdata = $this->pulau_act->cek($_POST['tb_chk']);
			echo $arrdata;
		}
	}

	public function cek_copy(){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('report/pulau_act');
			$arrdata = $this->pulau_act->cek_copy($_POST['tb_chk']);
			echo $arrdata;
		}
	}

	public function add_report($step, $jenis, $id = ''){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('report/pulau_act');
			if ($step == 'first') {
				$arrdata = $this->pulau_act->get_first($jenis, $id);
				$this->content = $this->load->view($this->ineng.'/backend/report/pulau/'.$step.'_'.$jenis, $arrdata, true);
                                $this->index();
			}elseif ($step == 'second') {
				$arrdata = $this->pulau_act->get_second($jenis, $id);
				$this->content = $this->load->view($this->ineng.'/backend/report/pulau/'.$step, $arrdata, true);
                $this->index();
			}elseif ($step == 'third') {
				$arrdata = $this->pulau_act->get_third($jenis, $id);
				// print_r($arrdata);die();
				if($this->input->post("data-post")){
					echo $arrdata;
				}else{
					$this->content = $this->load->view($this->ineng.'/backend/report/pulau/'.$step, $arrdata, true);
                	$this->index();
				}
			}
			
			
		}
	}

	public function add_komoditi($jenis, $id_lap, $id=''){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('report/pulau_act');
			$arrdata = $this->pulau_act->add_komoditi($jenis, $id_lap, $id);
			$this->content = $this->load->view($this->ineng.'/backend/report/pulau/komoditi', $arrdata, true);
			$this->index();
		}
	}

	public function del_komoditi(){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('report/pulau_act');
			$arrdata = $this->pulau_act->delete_komoditi($_POST['tb_chk']);
			echo $arrdata;
		}
	}

	public function delete_report($isajax){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('report/pulau_act');
			$arrdata = $this->pulau_act->del_report($_POST['tb_chk'], $isajax);
			echo $arrdata;
		}
	}

	public function preview_report($id_lap){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('report/pulau_act');
			$arrdata = $this->pulau_act->preview_report($id_lap);
			$this->content = $this->load->view($this->ineng.'/backend/report/pulau/preview', $arrdata, true);
			$this->index();
		}
	}

	public function prints($id){
		if($this->newsession->userdata('_LOGGED')){
			$id = hashids_decrypt($id, _HASHIDS_, 9);
			$this->load->model('report/pulau_act');
	        $arrdata = $this->pulau_act->set_prints($id);
//	         print_r($arrdata['eks']);die();
	        $arrmagin = array();
	        ob_start();
	        header('Content-type: application/pdf');
	        // if ($act != "") {
	        //     $classfile = APPPATH . "views/template/penjelasan/" . $dir . "/13.php";
	        // } else {
	        $classfile = APPPATH . "views/id/backend/report/pulau/cetakan.php";
	        // }
	        require_once($classfile);
	        $objpdf = new pdf("P", "mm", "Legal", $arrmargin, $arrdata);
	        $objpdf->generate($arrdata);
	    }
	}

}
?>