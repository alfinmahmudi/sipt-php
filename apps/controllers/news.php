<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News extends CI_Controller{

	var $content = "";
	var $breadcumbs = "";
	var $ineng = "";

	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }

    public function index(){
		if($this->newsession->userdata('_LOGGED')){
			$this->content = (!$this->content) ? $this->load->view($this->ineng.'/backend/default', '', true) : $this->content;
			$data = $this->main->set_content('backend', $this->breadcumbs, $this->content);
			$this->parser->parse($this->ineng.'/backend/home', $data);
		}else{
			redirect(site_url('portal'));
			exit();
		}
	}

    public function add_new(){
    	if($this->newsession->userdata('_LOGGED')){
    		$this->load->model('news_act');
			$arrdata = $this->news_act->get_new_req();
			// print_r($arrdata);exit();
			$this->content = $this->load->view($this->ineng.'/backend/create_news', $arrdata, true);
			$this->index();
		}else{
			redirect(site_url('portal'));
			exit();
		}
    }

    public function get_new($isajax){
    	if($this->newsession->userdata('_LOGGED')){
			$this->load->model('news_act');
			$arrdata = $this->news_act->add_new($isajax);
			echo $arrdata;
		}else{
			redirect(site_url('portal'));
			exit();
		}
    }

    public function news_view($id){
    	if ($id == 00) {
    		
    	}
		$this->load->model('news_act');
		$arrdata = $this->news_act->add_new($id);
		echo $arrdata;
    }

    public function news_detil($id){
    	if($this->newsession->userdata('_LOGGED')){
			$this->load->model('news_act');
			$arrdata = $this->news_act->list_news_detil($id);
			echo $this->load->view('./browse/news_details', $arrdata, true);
		}else{
			redirect(site_url('portal'));
			exit();
		}
    }

    public function delete_detil(){
    	if($this->newsession->userdata('_LOGGED')){
			$this->load->model('news_act');
			$arrdata = $this->news_act->delete_detil();
		}else{
			redirect(site_url('portal'));
			exit();
		}
    }

    public function update_detil(){
    	if($this->newsession->userdata('_LOGGED')){
			$this->load->model('news_act');
			$arrdata = $this->news_act->update_detil();
		}else{
			redirect(site_url('portal'));
			exit();
		}
    }

    public function edit_news($id){
    	if($this->newsession->userdata('_LOGGED')){
			$this->load->model('news_act');
			$arrdata = $this->news_act->get_edit($id);
			//print_r($arrdata);exit();
			$this->content = $this->load->view($this->ineng.'/backend/create_news', $arrdata, true);
			$this->index();
		}else{
			redirect(site_url('portal'));
			exit();
		}
    }

    public function delete_news(){
    	if($this->newsession->userdata('_LOGGED')){
			$this->load->model('news_act');
			$arrdata = $this->news_act->get_delete();
			echo $arrdata;
		}else{
			redirect(site_url('portal'));
			exit();
		}
    }

    public function update_news($isajax){
    	if($this->newsession->userdata('_LOGGED')){//print_r($_FILES);exit();
			$this->load->model('news_act');
			$arrdata = $this->news_act->update_news($isajax);
			echo $arrdata;
		}else{
			redirect(site_url('portal'));
			exit();
		}
    }
}