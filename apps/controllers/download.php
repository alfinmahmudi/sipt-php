<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Download extends CI_Controller{
	public function __construct() {
        parent::__construct();
    }
	
	public function index(){
		echo "Forbidden";
	}
	
	public function document($a, $y, $m, $d, $f){
		if($this->newsession->userdata('_LOGGED')){
			$file = str_replace('core/', '', BASEPATH) . 'upL04d5/document/' . $a . '/' . $y . '/' . $m . "/" . $d . "/" . $f;
			$imginfo = getimagesize($file);
			header("Content-type: $imginfo[mime]");
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			ob_clean();
			flush();
			readfile($file);
			exit;
		}else{
			redirect(site_url().'home', refresh);
		}
	}

	public function doc($a, $b, $c, $d, $e){
		if($this->newsession->userdata('_LOGGED')){
			$file = str_replace('core/', '', BASEPATH) . 'upL04d5/document/' . $a . '/' . $b . '/' . $c . "/" . $d . "/" . $e; //echo $file;exit();
			$imginfo = getimagesize($file);
			header("Content-type: $imginfo[mime]");
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			ob_clean();
			flush();
			readfile($file);
			exit;
		}else{
			redirect(site_url().'home', refresh);
		}
	}

	public function document_public($a, $y, $m, $d){
		if(!$this->newsession->userdata('_LOGGED')){
			$file = str_replace('core/', '', BASEPATH) . $a . '/' . $y . '/' . $m . "/" . $d;
			$imginfo = getimagesize($file);
			header("Content-type: $imginfo[mime]");
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			ob_clean();
			flush();
			readfile($file);
			exit;
		}else{
			redirect(site_url().'home', refresh);
		}
	}
	
}