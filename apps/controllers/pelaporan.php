<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pelaporan extends CI_Controller{
	
	var $content = "";
	var $breadcumbs = "";
	var $ineng = "";
	
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }
	
	public function index(){
		if($this->newsession->userdata('_LOGGED')){
			$this->content = (!$this->content) ? $this->load->view($this->ineng.'/backend/default', '', true) : $this->content;
			$data = $this->main->set_content('backend', $this->breadcumbs, $this->content);
			$this->parser->parse($this->ineng.'/backend/home', $data);
		}else{
			redirect(site_url('portal'));
			exit();
		}
	}
        
	public function form($step, $dir, $doc, $type, $id,$s=null){
		if($this->newsession->userdata('_LOGGED')){
			if($step == "first"){
				if($dir == "01"){
					if ($doc == "1") {
						$this->load->model('pelaporan/pelaporan_act');
						$arrdata = $this->pelaporan_act->get_pelaporan($dir, $doc, $type, $id);
					}elseif ($doc == "2") {
						$this->load->model('pelaporan/pelaporan_act');
						$arrdata = $this->pelaporan_act->get_pelaporan($dir, $doc, $type, $id);
					}elseif ($doc== "12") {
						$this->load->model('pelaporan/pelaporan_act');
						$arrdata = $this->pelaporan_act->get_pelaporan($dir, $doc, $type, $id);
					}elseif (($doc== "16") ||($doc== "15")){
						$this->load->model('pelaporan/pelaporan_act');
						$arrdata = $this->pelaporan_act->get_pelaporan($dir, $doc, $type, $id);
					}elseif (($doc== "18") || ($doc == "17") || ($doc == "19") || ($doc == "20") || ($doc == "21")) {
						$this->load->model('pelaporan/pelaporan_act');
						$arrdata = $this->pelaporan_act->get_pelaporan($dir, $doc, $type, $id);
					}elseif($doc == "14"){
						$this->load->model('pelaporan/pelaporan_act');
						$arrdata = $this->pelaporan_act->get_pelaporan($dir, $doc, $type, $id);
					}
					
					$this->content = $this->load->view($this->ineng.'/backend/licensing/pelaporan', $arrdata, true);
					$this->index();
				}elseif ($dir == "02") {
					$this->load->model('pelaporan/pelaporan_act');
					$arrdata = $this->pelaporan_act->get_pelaporan($dir, $doc, $type, $id);
					$this->content = $this->load->view($this->ineng.'/backend/licensing/pelaporan', $arrdata, true);
					$this->index();
				}elseif ($dir == "03") {
					if($doc == "11"){
						$this->load->model('pelaporan/pelaporan_act');
						$arrdata = $this->pelaporan_act->get_pelaporan($dir, $doc, $type, $id);
					}else if($doc == "8"){
						$this->load->model('pelaporan/pelaporan_act');
						$arrdata = $this->pelaporan_act->get_pelaporan($dir, $doc, $type, $id);
					}else if($doc == "7"){
						$this->load->model('licensing/pkapt_act');
						$arrdata = $this->pkapt_act->get_pelaporan($dir, $doc, $type, $id);
					}else if($doc == "9"){
						$this->load->model('licensing/sppgap_act');
						$arrdata = $this->sppgap_act->get_pelaporan($dir, $doc, $type, $id);
					}else if($doc == "10"){
						$this->load->model('licensing/sppgrap_act');
						$arrdata = $this->sppgrap_act->get_pelaporan($dir, $doc, $type, $id);
					}
					$this->content = $this->load->view($this->ineng.'/backend/licensing/pelaporan', $arrdata, true);
					$this->index();
				}
			}
		}
	}

	function set_detil($act, $isajax){
		$this->load->model('report/sipdis_act');
		$arrdata = $this->sipdis_act->set_detil($act, $isajax);
		echo $arrdata;
	}

	function cek_sess($isajax){
		$this->load->model('report/sipdis_act');
		$arrdata = $this->sipdis_act->cek_sess($isajax);
		echo json_encode($arrdata);
	}

	function get_detil($id){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('report/sipdis_act');
			$arrdata = $this->sipdis_act->get_detil($id);
			echo $this->load->view('./browse/tdpud_detil', $arrdata, true);
		}else{
			redirect(site_url('portal'));
			exit();
		}	
	}

	public function check($dir, $doc, $permohonan_id, $isajax){
		if ($this->newsession->userdata('_LOGGED')) {
                        $this->load->model('report/sipdis_act');
			$arrdata = $this->sipdis_act->cek_edit($dir, $doc, $permohonan_id, $isajax);
			echo $arrdata;
		}
    }

    public function val_edit($isajax){
		if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('report/sipdis_act');
			$arrdata = $this->sipdis_act->val_edit($isajax);
			echo $arrdata;
		}
    }

    public function begin_upload($isajax){
		if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('report/sipdis_act');
			$arrdata = $this->sipdis_act->begin_upload($isajax);
			echo json_encode($arrdata);
		}
    }

    public function read_excel($isajax){
		if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('report/sipdis_act');
			$arrdata = $this->sipdis_act->read_excel($isajax);
			echo json_encode($arrdata);
		}
    }
    public function rollback_rep($isajax){
		if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('report/sipdis_act');
			$arrdata = $this->sipdis_act->rollback_rep($_POST['tb_chk']);
			echo $arrdata;
		}
    }
}