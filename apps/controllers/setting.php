<?php if ( ! 
defined('BASEPATH')) exit('No direct script access allowed');
class Setting extends CI_Controller{
	
	var $content = "";
	var $breadcumbs = "";
	var $ineng = "";
	
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }
	
	public function index(){
		if($this->newsession->userdata('_LOGGED')){
			$this->content = (!$this->content) ? $this->load->view($this->ineng.'/backend/default', '', true) : $this->content;
			$data = $this->main->set_content('backend', $this->breadcumbs, $this->content);
			$this->parser->parse($this->ineng.'/backend/home', $data);
		}else{
			redirect(site_url('portal'));
			exit();
		}
	}
	
	public function view($menu, $sub = "", $id = ""){
		if($this->newsession->userdata('_LOGGED')){
			if($menu == "document"){#Dokumen Pendukung
				$this->load->model('document/supporting_act');
				if($sub == ""){
					$arrdata = $this->supporting_act->list_document();
					if($this->input->post("data-post")){
						echo $arrdata;
					}else{
						$this->content = $this->load->view('browse/grid', $arrdata, true);
						$this->index();
					}
				}elseif($sub == "new"){
					$arrdata = $this->supporting_act->get_document($id);
					echo $this->load->view($this->ineng.'/backend/document/supporting-form', $arrdata, true);
				}
			}elseif($menu == "profil") {
				$this->load->model('management/management_act');
				$arrdata = $this->management_act->get_profil();
				$this->content = $this->load->view($this->ineng.'/backend/management/profile', $arrdata, true);
				$this->index();
			}elseif($menu == "company") {
				$this->load->model('management/company_act');
				$arrdata = $this->company_act->get_profil();
				$this->content = $this->load->view($this->ineng.'/backend/management/company', $arrdata, true);
				$this->index();
			}

			
			else{
				redirect(base_url().'/home', refresh);
			}
		}
	}
        
        public function delete_status($isajax){
		if($this->newsession->userdata('_LOGGED')){
			if($_SERVER['REQUEST_METHOD']=="POST"){
				$this->load->model('document/supporting_act');
				$data = $this->supporting_act->del_status($_POST['tb_chk'][0]);
			}
			if($isajax != "ajax"){
				redirect(base_url());
			}
		echo $data;
		}
	}
        
        public function edit($isajax){
		if($this->newsession->userdata('_LOGGED')){
			if($_SERVER['REQUEST_METHOD']=="POST"){
                            $this->load->model('document/supporting_act');
                            $arrdata = $this->supporting_act->get_document($_POST['tb_chk'][0]);
                            echo $this->load->view($this->ineng.'/backend/document/supporting-form', $arrdata, true);
			}
			if($isajax != "ajax"){
				redirect(base_url());
			}
		echo $data;
		}
	}

	public function popup_dok($izin_id, $dok_id){
		if($this->newsession->userdata('_LOGGED')){
			//print_r($izin_id."-".$dok_id);die();
			$id="";
			$this->load->model('document/supporting_act');
			$arrdata = $this->supporting_act->get_document($id, $izin_id, $dok_id);
			echo $this->load->view($this->ineng.'/backend/document/supporting-form',$arrdata, true);
		}
	}

	public function popup_dok2($izin_id, $dok_id, $ids=""){
		if($this->newsession->userdata('_LOGGED')){
			//print_r($izin_id."-".$dok_id);die();
			$id="";
			$this->load->model('document/supporting_act');
			$arrdata = $this->supporting_act->get_document($id, $izin_id, $dok_id);
			if (trim($ids) != "") {
				$arrdata['append_id'] = $ids;
			}
			echo $this->load->view($this->ineng.'/backend/document/supporting2',$arrdata, true);
		}
	}

	public function popup_dok3($izin_id, $dok_id, $permohonan_id = ""){
		if($this->newsession->userdata('_LOGGED')){
			//print_r($izin_id."-".$dok_id);die();
			$id="";
			$this->load->model('document/supporting_act');
			$arrdata = $this->supporting_act->get_document($id, $izin_id, $dok_id, $permohonan_id);
			echo $this->load->view($this->ineng.'/backend/document/supporting2',$arrdata, true);
		}
	}

	public function edit_per(){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('management/company_act');
			$arrdata = $this->company_act->edit_per();
			$this->content = $this->load->view($this->ineng.'/backend/management/edit_per', $arrdata, true);
			$this->index();
		}
	}

	public function set_per($isajax){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('management/company_act');
			$arrdata = $this->company_act->set_per($isajax);
			echo $arrdata;
		}	
	}

	public function cek_wajib($id_dok){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('document/supporting_act');
			$arrdata = $this->supporting_act->get_wajib();
			echo json_encode($arrdata);
		}
	}
	
	
	
}