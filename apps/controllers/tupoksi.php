<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tupoksi extends CI_Controller{

	var $content = "";
	var $breadcumbs = "";
	var $ineng = "";

	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }

    public function index(){
		if($this->newsession->userdata('_LOGGED')){
			$this->content = (!$this->content) ? $this->load->view($this->ineng.'/backend/default', '', true) : $this->content;
			$data = $this->main->set_content('backend', $this->breadcumbs, $this->content);
			$this->parser->parse($this->ineng.'/backend/home', $data);
		}else{
			redirect(site_url('portal'));
			exit();
		}
	}

	public function get_diretorat(){
		if($this->newsession->userdata('_LOGGED')){
			$p = 'sla';
			$this->load->model('licensing/licensing_act');
			$arrdata = $this->licensing_act->view_licensing($p);
			$this->content = $this->load->view($this->ineng.'/backend/licensing/add_req', $arrdata, true);
			$this->index();
		}
	}

	public function get_tupoksi(){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('reference/tupoksi_act');
			$arrdata = $this->tupoksi_act->get_tupoksi();
			//print_r($arrdata);exit();
			$this->content = $this->load->view($this->ineng.'/backend/maintance_tupoksi', $arrdata, true);
			$this->index();
		}else{
			redirect(site_url('portal'));
			exit();
		}
	}

	public function set_tupoksi($isajax){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('reference/tupoksi_act');
			$arrdata = $this->tupoksi_act->set_tupoksi($isajax);
			echo $arrdata;
		}else{
			redirect(site_url('portal'));
			exit();
		}
	}
}
?>