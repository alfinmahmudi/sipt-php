<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Popup extends CI_Controller{
	public function __construct() {
        parent::__construct();
    }
	
	public function index(){
		echo "Forbidden";
	}
	
	public function tenaga_ahli($a,$dtl){
		//print_r($dtl);die();
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model("licensing/siup4_act");
			$arrdata = $this->siup4_act->get_tenaga_ahli($a,$dtl);
			echo $this->load->view('id/backend/licensing/01/tenaga_ahli',$arrdata, true);
		}

	}

	public function log($permohonan_id,$kd_izin){
		if($this->newsession->userdata('_LOGGED')){
			$a = hashids_encrypt($permohonan_id, _HASHIDS_, 9);
			$b = hashids_encrypt($kd_izin, _HASHIDS_, 9);
			$this->load->model('licensing/log_act');
			$arrdata = $this->log_act->get_log($a,$b);
			echo $this->load->view('id/backend/log/list', $arrdata, true);
		}

	}
}