<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Log extends CI_Controller{
	var $ineng = "";
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }
	
	public function index(){
		echo "Forbidden";
	}
	
	public function izin($a, $b){
		$this->load->model('licensing/log_act');
		$arrdata = $this->log_act->get_log($a, $b);
		echo $this->load->view($this->ineng.'/backend/log/list', $arrdata, true);
    }

    
}