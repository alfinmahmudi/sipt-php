<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Onfly extends CI_Controller{
	public function __construct(){
        parent::__construct();
    }
	
	public function index(){
		echo "Forbidden";
	}
	
	
	public function onfly_act($act, $dir, $doc, $id, $isajax){
		/*
		@ID @Nama Izin
		1	Surat Izin Usaha Jasa Survey
		2	Surat Izin Usaha Perusahaan Perantara Perdangangan properti (SIU-P4)
		3	Surat Izin Usaha Perdagangan  Minuman Beralkohol - Importir
		4	Surat Izin Usaha Perdagangan  Minuman Beralkohol - Distributor
		5	Surat Izin Usaha Perdagangan  Minuman Beralkohol - Sub Distributor
		6	Surat Keterangan Pengecer Minuman Beralkohol Golongan A (SKP-A)
		7	Pedagang Kayu Antar Pulau Terdaftar (PKAPT)
		8	Pedagang Gula Antar Pulau Terdaftar (PGAPT)
		9	Surat Persetujuan Perdagangan Gula Antar Pulau (SPPGAP)
		10	Surat Persetujuan Perdagangan Gula Rafinasi Antar Pulau (SPPGRAP)
		11	Surat Izin Usaha Perdagangan Bahan Berbahaya (SIUP B2) sebagai DT-B2
		12	Surat Tanda Pendaftaran Agen atau Distributor Barang atau Jasa Dalam atau Luar Negeri
		13	Surat Keterangan Penjual Langsung Minuman Beralkohol Golongan A (SKPL-A)
		*/
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$arrsiupmb = array(3,4,5,6,13);
				$arrpameran = array(15,16);
				$arrstpw = array(17,18,19,20);
				if(in_array($doc, $arrsiupmb)){
					$this->load->model('licensing/siupmb_act');
					$ret = $this->siupmb_act->set_onfly($act, $id, $isajax);
				}elseif ($doc == '2') {
					$this->load->model('licensing/siup4_act');
					$ret = $this->siup4_act->set_onfly($act, $id, $isajax);
				}elseif ($doc == '1') {
					$this->load->model('licensing/siujs_act');
					$ret = $this->siujs_act->set_onfly($act, $id, $isajax);
				}elseif ($doc == '12') {
					$this->load->model('licensing/siupagen_act');
					$ret = $this->siupagen_act->set_onfly($act, $id, $isajax);
				}elseif ($doc == '11') {
					$this->load->model('licensing/siupbb_act');
					$ret = $this->siupbb_act->set_onfly($act, $id, $isajax);
				}elseif ($doc == '8') {
					$this->load->model('licensing/pgapt_act');
					$ret = $this->pgapt_act->set_onfly($act, $id, $isajax);
				}elseif ($doc == '7') {
					$this->load->model('licensing/pkapt_act');
					$ret = $this->pkapt_act->set_onfly($act, $id, $isajax);
				}elseif ($doc == '9') {
					$this->load->model('licensing/sppgap_act');
					$ret = $this->sppgap_act->set_onfly($act, $id, $isajax);
				}elseif (in_array($doc, $arrpameran)){
					$this->load->model('licensing/pameran_act');
					$ret = $this->pameran_act->set_onfly($act, $id, $isajax);
				}elseif (in_array($doc, $arrstpw)){
					$this->load->model('licensing/stpw_act');
					$ret = $this->stpw_act->set_onfly($act, $id, $isajax);
				}elseif ($doc == '14') {
					$this->load->model('licensing/garansi_act');
					$ret = $this->garansi_act->set_onfly($act, $id, $isajax);
				}
			}
			if($isajax!="ajax"){
				return false;
			}
			echo $ret;
		}
	}
}