<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ac extends CI_Controller{
	public function __construct() {
        parent::__construct();
    }
	
	public function index(){
		echo "Forbidden";
	}

	function get_permohonan($dir, $doc) {
        if (($dir == "") ||($doc == "")) {
            return $this->index();
        } else {
            $key = strtolower($_REQUEST['q']);
            $query = "SELECT id, kd_izin, no_izin, tgl_izin, tgl_izin_exp
                      FROM view_permohonan 
                      WHERE status_id = '1000'
                      AND direktorat_id = ".$dir."
                      AND kd_izin = ".$doc." 
                      AND trader_id = ".$this->newsession->userdata('trader_id')."
                      AND no_izin like '%$key%'";
            $res = $this->main->get_result($query);
            if ($res) {
                foreach ($query->result_array() as $row) {
                    echo "<b>No. Izin </b>" . $row['no_izin'] . "<br><b>Tanggal Izin</b> " . $row['tgl_izin'] . "<br><b>Tanggal Akhir Izin </b>" . $row['tgl_izin_exp'] . "|" . $row['no_izin'] . "|" . $row['id'] . "|" . $row['kd_izin'] . "\n";
                }
            } else {
                echo "Data tidak ditemukan|||\n";
            }
        }
    }

    function set_form($id, $doc) {
    	$arrsiupmb = array("3","4","5","6","13");
        $arrpameran = array("15","16");
        $arrstpw = array("17","18","19","20");
        $field="";
    	if (in_array($doc, $arrsiupmb)) {
    		$dbTable = "t_siupmb";
            $addField = "lokasi,status_milik,no_siup,tgl_siup,jenis_siup,";
    	} elseif ($doc == "1") {
    		$dbTable = "t_siujs";
    	} elseif ($doc == "2") {
    		$dbTable = "t_siup4";
    	} elseif ($doc == "12") {
    		$dbTable = "t_siup_agen";
            $addField = "produksi, jenis_agen, bidang_usaha, pemasaran,";
    	} elseif ($doc == "11") {
    		$dbTable = "t_siupbb";
            $addField = "kategori,";
    	} elseif ($doc == "7") {
            $dbTable = "t_pkapt";
        } elseif (in_array($doc, $arrpameran)) {
            $dbTable = "t_pameran";
        } elseif (in_array($doc, $arrstpw)) {
            $dbTable = "t_stpw";
            $addField = "produk_waralaba, negara_perusahaan,";
        }

        $query = "SELECT id,kd_izin,no_izin,tgl_izin,tgl_izin_exp,npwp,tipe_perusahaan,nm_perusahaan,almt_perusahaan,
        		  kdprop,kdkab,kdkec,kdkel,kdpos,telp,fax,".$addField."
				  dbo.get_region(2, kdprop) AS prop_perusahaan, dbo.get_region(4, kdkab) AS kab_perusahaan,
				  dbo.get_region(7, kdkec) AS kec_perusahaan, dbo.get_region(10, kdkel) AS kel_perusahaan
				  FROM ".$dbTable."
				  WHERE id = ".$id." and kd_izin = ".$doc;
        $res = $this->main->get_result($query);
        if ($res) {
            $ret = "";
            foreach ($query->result_array() as $row) {
                $ret .= $row['id'] . "|"; #0
                $ret .= $row['no_izin'] . "|"; #1
                $ret .= $row['tgl_izin'] . "|"; #2
                $ret .= $row['tgl_izin_exp'] . "|"; #3
                $ret .= $row['npwp'] . "|"; #4
                $ret .= $row['nm_perusahaan'] . "|"; #5
                $ret .= $row['almt_perusahaan'] . "|"; #6
                $ret .= $row['kdprop'] . "|"; #7
                $ret .= $row['kdkab'] . "|"; #8
                $ret .= $row['kdkec'] . "|"; #9
                $ret .= $row['kdkel'] . "|"; #10
                $ret .= $row['kdpos'] . "|"; #11
                $ret .= $row['telp'] . "|"; #12
                $ret .= $row['fax'] . "|"; #13
                $ret .= $row['lokasi'] . "|"; #14
                $ret .= $row['status_milik'] . "|"; #15
                $ret .= $row['no_siup'] . "|"; #16
                $ret .= $row['tgl_siup'] . "|"; #17
                $ret .= $row['jenis_siup'] . "|"; #18
                $ret .= $row['tipe_perusahaan'] . "|"; #19
                $ret .= $row['prop_perusahaan'] . "|"; #20
                $ret .= $row['kab_perusahaan'] . "|"; #21
                $ret .= $row['kec_perusahaan'] . "|"; #22
                $ret .= $row['kel_perusahaan'] . "|"; #23
                $ret .= $row['produksi'] . "|"; #24
                $ret .= $row['jenis_agen'] . "|"; #25
                $ret .= $row['bidang_usaha'] . "|"; #26
                $ret .= $row['pemasaran'] . "|"; #27
                $ret .= $row['kategori'] . "|"; #28
                $ret .= $row['produk_waralaba'] . "|"; #29
                $ret .= $row['negara_perusahaan'] . "|"; #30
            }
            echo $ret;
        }
    }
}