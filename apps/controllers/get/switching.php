<?php
class Switching extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
 
    public function language($language = ""){
        $language = ($language != "") ? $language : "id";
        $this->session->set_userdata('site_lang', $language);
        redirect(base_url());
    }
}