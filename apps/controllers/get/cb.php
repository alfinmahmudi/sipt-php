<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cb extends CI_Controller{
	public function __construct() {
        parent::__construct();
    }
	
	public function index(){
		echo "Forbidden";
	}
	
	public function set_kota($prop){
        $result = "";
        $query = "SELECT id, nama FROM m_kab WHERE id_prov = '" . $prop . "' ORDER BY 1";
        $data = $this->main->get_result($query);
        if ($data) {
            $result .= "<option value=\"\"></option>";
            foreach ($query->result_array() as $row) {
                $result .= "<option value=\"" . $row['id'] . "\">" . $row['nama'] . "</option>";
            }
        }
        echo str_replace(chr(10), '', $result);
    }
	
    public  function set_kec($kab){
        $result = "";
        $query = "SELECT id, nama FROM m_kec WHERE id_kabupaten = '".$kab."' ORDER BY 2";
        $data = $this->main->get_result($query);
        if ($data) {
            $result .= "<option value=\"\"></option>";
            foreach ($query->result_array() as $row) {
                $result .= "<option value=\"" . $row['id'] . "\">" . $row['nama'] . "</option>";
            }
        }
        echo str_replace(chr(10), '', $result);
    }
	
	public  function set_kel($kec){
        $result = "";
        $query = "SELECT id, nama FROM m_kel WHERE id_kecamatan = '".$kec."' ORDER BY 2";
        $data = $this->main->get_result($query);
        if ($data) {
            $result .= "<option value=\"\"></option>";
            foreach ($query->result_array() as $row) {
                $result .= "<option value=\"" . $row['id'] . "\">" . $row['nama'] . "</option>";
            }
        }
        echo str_replace(chr(10), '', $result);
    }

	public  function set_dokumen($dir){
        $result = "";
        $query = "SELECT id, nama_izin FROM m_izin WHERE direktorat_id = '".$dir."' AND aktif = '1' ORDER BY 2";
        $data = $this->main->get_result($query);
        if ($data) {
            $result .= "<option value=\"\"></option>";
            foreach ($query->result_array() as $row) {
                $result .= "<option value=\"" . $row['nama_izin'] . "\">" . $row['nama_izin'] . "</option>";
            }
        }
        echo str_replace(chr(10), '', $result);
    }
    
    public  function set_dokumen2($dir){
        $result = "";
        $query = "SELECT id, nama_izin FROM m_izin WHERE direktorat_id = '".$dir."' AND aktif = '1' ORDER BY 2";
        $data = $this->main->get_result($query);
        if ($data) {
            $result .= "<option value=\"\"></option>";
            foreach ($query->result_array() as $row) {
                $result .= "<option value=\"" . $row['nama_izin'] . "\">" . $row['nama_izin'] . "</option>";
            }
        }
        echo str_replace(chr(10), '', $result);
    }
    
    public  function set_dokumen3($dir){
        $result = "";
        $query = "SELECT id, nama_izin FROM m_izin WHERE direktorat_id = '".$dir."' AND aktif = '1' ORDER BY 2";
        $data = $this->main->get_result($query);
        if ($data) {
            $result .= "<option value=\"\"></option>";
            foreach ($query->result_array() as $row) {
                $result .= "<option value=\"" . $row['id'] . "\">" . $row['nama_izin'] . "</option>";
            }
        }
        echo str_replace(chr(10), '', $result);
    }
	
	
    public function set_dok_izin($izin){
        $result = "";
        $query = "SELECT a.dok_id as id, b.keterangan
                  FROM m_dok_izin a
                  LEFT JOIN m_dok b on b.id = a.dok_id
                  WHERE a.izin_id =".$izin;
        //print_r($query);die();
        $data = $this->main->get_result($query);
        if ($data) {
            $result .= "<option value=\"\"></option>";
            foreach ($query->result_array() as $row) {
                $result .= "<option value=\"" . $row['id'] . "\">" . $row['keterangan'] . "</option>";
            }
        }
        echo str_replace(chr(10), '', $result);
    }
    
    public function set_jenis($kd){
        $result = "";
        $query = "SELECT DISTINCT a.jenis_agen, b.uraian FROM m_agen_tipe a 
LEFT JOIN m_reff b ON a.jenis_agen = b.kode and 
jenis = 'PRODUKSI_KEAGENAN' WHERE kd_produk = '" . $kd . "' ORDER BY 1";
        $data = $this->main->get_result($query);
        if ($data) {
            $result .= "<option value=\"\"></option>";
            foreach ($query->result_array() as $row) {
                $result .= "<option value=\"" . $row['jenis_agen'] . "\">" . $row['uraian'] . "</option>";
            }
        }
        echo str_replace(chr(10), '', $result);
    }
	
    public function set_status($jenis, $kd){
        $result = "";
        $query = "SELECT distinct a.produksi, b.uraian 
                FROM m_agen_tipe a 
                LEFT JOIN m_reff b ON a.produksi = b.kode and jenis = 'JENIS_KEAGENAN' 
                WHERE jenis_agen = '". $jenis ."' and kd_produk = '".$kd."' 
                ORDER BY 1";
        // print_r($query);die();
        $data = $this->main->get_result($query);
        if ($data) {
            $result .= "<option value=\"\"></option>";
            foreach ($query->result_array() as $row) {
                $result .= "<option value=\"" . $row['produksi'] . "\">" . $row['uraian'] . "</option>";
            }
        }
        echo str_replace(chr(10), '', $result);
    }
}