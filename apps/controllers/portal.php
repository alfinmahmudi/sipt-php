<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Portal extends CI_Controller {

    var $content = "";
    var $breadcumbs = "";
    var $ineng = "";

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('site_lang')) {
            $this->lang->load('message', $this->session->userdata('site_lang'));
            $this->ineng = $this->session->userdata('site_lang');
        } else {
            $this->lang->load('message', 'id');
            $this->ineng = "id";
        }
    }

    public function confirm($id, $username) {
        if (trim($id) != '' && trim($username) != '') {
            $this->load->model('login_act');
            $isi = $this->login_act->get_confirm($id, $username);
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/portal/confirm', $isi, true) : $this->content;
            $data = $this->main->set_content('portal', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/portal/home', $data);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function index() {
        if (!$this->newsession->userdata('_LOGGED')) {
            $isi['permohonan'] = $this->db->query("select top(5) nm_perusahaan, nama_izin, tgl_izin  from view_permohonan order by  tgl_izin desc")->result_array();
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/portal/default', $isi, true) : $this->content;
            $data = $this->main->set_content('portal', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/portal/home', $data);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function izin() {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->load->model('perizinan_act');
            $isi = $this->perizinan_act->get_licensing();
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/portal/izin', $isi, true) : $this->content;
            $data = $this->main->set_content('portal', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/portal/home', $data);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function news() {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->load->model('news_act');
            $isi['data'] = $this->news_act->get_view();
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/portal/default', $isi, true) : $this->content;
            $data = $this->main->set_content('portal', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/portal/home', $data);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function news_view($id) {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->load->model('news_act');
            $isi = $this->news_act->main_view($id);
            //print_r($isi);exit();
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/portal/main_view', $isi, true) : $this->content;
            $data = $this->main->set_content('portal', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/portal/home', $data);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function um_view($id) {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->load->model('news_act');
            $isi = $this->news_act->um_view($id);
            //print_r($isi);exit();
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/portal/um_view', $isi, true) : $this->content;
            $data = $this->main->set_content('portal', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/portal/home', $data);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function um_video($id) {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->load->model('news_act');
            $isi = $this->news_act->um_video($id);
            //print_r($isi);exit();
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/portal/um_video', $isi, true) : $this->content;
            $data = $this->main->set_content('portal', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/portal/home', $data);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function regulasi($id) {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/portal/regulasi', "", true) : $this->content;
            $data = $this->main->set_content('portal', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/portal/home', $data);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function informasi($id) {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/portal/informasi', "", true) : $this->content;
            $data = $this->main->set_content('portal', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/portal/home', $data);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function status() {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->load->model('perizinan_act');
            $isi = $this->perizinan_act->get_status();
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/portal/status', $isi, true) : $this->content;
            $data = $this->main->set_content('portal', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/portal/home', $data);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function serach_status() {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->load->model('perizinan_act');
            $arrdata = $this->perizinan_act->get_search();
            echo $this->load->view($this->ineng . '/portal/status_result', $arrdata, true);
        }
    }

    public function get_perizinan() {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->load->model('perizinan_act');
            $arrdata = $this->perizinan_act->view();
            echo $this->load->view($this->ineng . '/portal/requirements-result', $arrdata, true);
        }
    }

    public function validasi() {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/portal/validasi', '', true) : $this->content;
            $data = $this->main->set_content('portal', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/portal/home', $data);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function result_validasi() {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->load->model('validasi_act');
            $data = $this->validasi_act->get_validasi();
            echo $this->load->view($this->ineng . '/portal/result_validasi', $data, true);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function set_confirm() {
        $this->load->model('login_act');
        $ret = $this->login_act->set_confirm();
        echo $ret;
    }

    public function login() {
        echo $this->load->view($this->ineng . '/portal/login/form', array(), true);
    }

    public function contact() {
        if (!$this->newsession->userdata('_LOGGED')) {
            $this->content = $this->load->view($this->ineng . '/portal/contact', '', true);
            $this->index();
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function test_kirim_email() {
        $this->load->model('main');
        $email = 'testbobihariadi@gmail.com';
        $message = 'test';
        $subject = 'test email';
        $this->main->send_email($email, $message, $subject);
        die('ok');
    }

    public function regis() {

        if (!$this->newsession->userdata('_LOGGED')) {//die("controller");
            $this->load->model('registrasi_act');
            $isi = $this->registrasi_act->get_registrasi();
            $this->content = (!$this->content) ? $this->load->view($this->ineng . '/portal/registrasi', $isi, true) : $this->content;
            $data = $this->main->set_content('portal', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/portal/home', $data);
        } else {
            redirect(site_url() . 'home');
        }
    }

    public function check_username() {
        $temp = $_POST['data'];
        $this->load->model('registrasi_act');
        $arrdata = $this->registrasi_act->get_vaidate($temp);
        echo json_encode($arrdata);
    }
    
    public function validate_npwp(){
		$npwp = $this->input->post('npwp');
		$token = $this->input->post('adds');
        $token_dec = $this->main->dencrypt($token);
        $arrExpld = explode("-", $token_dec);
        $realToken = $arrExpld[0];
        $trueToken = 'sipt'.date('YmdH');

        if ($realToken == $trueToken) {
        	if ($npwp == '023357478101000') {
				$data = array(
					'NPWP' => '023357478101000',
				    'NAMA' => 'KILANG PADI MEUTUAH BARO'
				);
				$output = json_encode($data);
			}elseif ($npwp == '141894352802000') {
				$data = array(
					'NPWP' => '141894352802000',
				    'NAMA' => 'PURWANTO (SUMBER REJEKI)'
				);
				$output = json_encode($data);
			}else{
//				$url = "http://www.kemendag.go.id/addon/api/website_api/npwp/".$npwp; 
                                $url = "http://hub.kemendag.go.id/index.php/website_api/npwp/11/".$npwp;
				$ch = curl_init();  
				
				// set URL and other appropriate options  
				curl_setopt($ch, CURLOPT_URL, $url);  
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:4agROXxoNEhMC5meU78Al1nZWwcqGIJDzvpPL6K0','User-Agent:Website-Demo'));  
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
				
				// grab URL and pass it to the browser  
				
				$output = curl_exec($ch);	
			}
			
			$cek = json_decode($output);
			// print_r($cek->NPWP);die();
			if($cek->NPWP == ''){
				$result['stat'] = '0';
				echo json_encode($result);
			}else{
				$result['stat'] = '1';
				$result['res'] = json_decode($output); 
				echo json_encode($result);
			}
        }else{die("iku");
        	$result['stat'] = '0';
			echo json_encode($result);
        }
			
	}

    public function validate_oss() {
        $npwp = $this->input->post('nib');
        $token = $this->input->post('adds');
        $token_dec = $this->main->dencrypt($token);
        $arrExpld = explode("-", $token_dec);
        $realToken = $arrExpld[0];
        $trueToken = 'sipt' . date('YmdH');

        if ($realToken == $trueToken) {
            if ($npwp == '023357478101000') {
                $data = array(
                    'NPWP' => '023357478101000',
                    'NAMA' => 'KILANG PADI MEUTUAH BARO'
                );
                $output = json_encode($data);
            } elseif ($npwp == '141894352802000') {
                $data = array(
                    'NPWP' => '141894352802000',
                    'NAMA' => 'PURWANTO (SUMBER REJEKI)'
                );
                $output = json_encode($data);
            } else {
//				$url = "http://www.kemendag.go.id/addon/api/website_api/npwp/".$npwp; 
                $url = "http://hub.kemendag.go.id/index.php/oss_services/inqueryniball/";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
//                curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:MQbTaGOivtnc1y4ldD2JVf3EZWUhKsBYe6gHw5Lj'));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:4agROXxoNEhMC5meU78Al1nZWwcqGIJDzvpPL6K0', 'User-Agent:Website-Demo'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, 'nib=' . $npwp);
                curl_setopt($ch, CURLOPT_USERAGENT, 'WEB-SIPT');
                $output1 = curl_exec($ch);
            }

            $cek = json_decode($output1);
            //print_r($cek);
            // die();
            $sqlGetTipe = "SELECT a.kode
                        from m_reff a 
                        where a.kode_oss = " . $this->db->escape($cek->responinqueryNIBAll->dataNIB[0]->jenis_perseroan) . " and a.jenis = 'TIPE_PERUSAHAAN'";
            $dataTipe = $this->db->query($sqlGetTipe)->row();
            $sqlGetIden = "SELECT a.kode
                        from m_reff a 
                        where a.kode_oss = " . $this->db->escape($cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->jenis_nik_penanggung_jwb) . " and a.jenis = 'JENIS_IDENTITAS'";
            $dataTipeIden = $this->db->query($sqlGetIden)->row();
            $sqlGetJab = "SELECT a.kode
                        from m_reff a 
                        where a.kode_oss = " . $this->db->escape($cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->jabatan_penanggung_jwb) . " and a.jenis = 'JABATAN'"; //die($sqlGetJab);
            $dataTipeJab = $this->db->query($sqlGetJab)->row();
            $arrTemp = '';
//             print_r($cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->identitas_penanggung_jwb);die();
            $arrTemp['npwp'] = $cek->responinqueryNIBAll->dataNIB[0]->npwp_perseroan;
            $arrTemp['tipe_perusahaan'] = $dataTipe->kode;
            $arrTemp['nm_perusahaan'] = $cek->responinqueryNIBAll->dataNIB[0]->nama_perseroan;
            $rt = 'RT. '.substr($cek->responinqueryNIBAll->dataNIB[0]->rt_rw_perseroan,0,3).', RW. '.substr($cek->responinqueryNIBAll->dataNIB[0]->rt_rw_perseroan,4,6);
            $arrTemp['almt_perusahaan'] = $cek->responinqueryNIBAll->dataNIB[0]->alamat_perseroan . " " . $rt;
            $arrTemp['kdprop'] = substr($cek->responinqueryNIBAll->dataNIB[0]->perseroan_daerah_id, 0, 2);
            $arrTemp['kdkab'] = substr($cek->responinqueryNIBAll->dataNIB[0]->perseroan_daerah_id, 0, 4);
            $arrTemp['kdkec'] = substr($cek->responinqueryNIBAll->dataNIB[0]->perseroan_daerah_id, 0, 6);
            $arrTemp['kdkel'] = substr($cek->responinqueryNIBAll->dataNIB[0]->perseroan_daerah_id, 0, 10);
            $arrTemp['kdpos'] = $cek->responinqueryNIBAll->dataNIB[0]->kode_pos_perseroan;
            $arrTemp['telp'] = $cek->responinqueryNIBAll->dataNIB[0]->nomor_telepon_perseroan;
            $arrTemp['na_pj'] = $cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->nama_penanggung_jwb;
            $arrTemp['identitas_pj'] = $dataTipeIden->kode;
            $arrTemp['noidentitas_pj'] = $cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->nik_penanggung_jwb;
            $arrTemp['jabatan_pj'] = $cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->jabatan_penanggung_jwb;
            $arrTemp['telp_pj'] = $cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->no_telp_penanggung_jwb;
            $jalan ="";
            $nomor = "";
            $blok = "";
            if($cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->jalan_penanggung_jwb != "-"){
                $jalan = $cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->jalan_penanggung_jwb . ", ";
            }
            if($cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->blok_penanggung_jwb != "-"){
                $blok = $cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->blok_penanggung_jwb . ", " ;
            }
            if($cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->no_penanggung_jwb != "-"){
                $nomor = $cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->no_penanggung_jwb . ", " ; 
            }
            $rt_pj = 'RT. '.substr($cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->rt_rw_penanggung_jwb,0,3).', RW. '.substr($cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->rt_rw_penanggung_jwb,4,6);
            $arrTemp['alamat_pj'] = $cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->alamat_penanggung_jwb . ", " . $jalan. $blok . $nomor . $rt_pj;
            $arrTemp['kdprop_pj'] = substr($cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->daerah_id_penanggung_jwb, 0, 2);
            $arrTemp['kdkab_pj'] = substr($cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->daerah_id_penanggung_jwb, 0, 4);
            $arrTemp['kdkec_pj'] = substr($cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->daerah_id_penanggung_jwb, 0, 6);
            $arrTemp['kdkel_pj'] = substr($cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->daerah_id_penanggung_jwb, 0, 10);
            $arrTemp['email_pj'] = $cek->responinqueryNIBAll->dataNIB[0]->penanggung_jwb[0]->email_penanggung_jwb;


            if ($cek->responinqueryNIBAll->dataNIB[0]->npwp_perseroan == '') {
                $result['stat'] = '0';
                echo json_encode($result);
            } else {
                $result['stat'] = '1';
                $result['res'] = $arrTemp;
                $result['log'] = $output1;
                echo json_encode($result);
            }
        } else {
//            die("iku");
            $result['stat'] = '0';
            echo json_encode($result);
        }
    }

}
