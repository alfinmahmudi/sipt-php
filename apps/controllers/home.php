<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    var $content = "";
    var $breadcumbs = "";
    var $ineng = "";

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('site_lang')) {
            $this->lang->load('message', $this->session->userdata('site_lang'));
            $this->ineng = $this->session->userdata('site_lang');
        } else {
            $this->lang->load('message', 'id');
            $this->ineng = "id";
        }
    }

   

    public function index() {
//        die($this->newsession->userdata("role"));
        if ($this->newsession->userdata('_LOGGED')) {
            if ($this->newsession->userdata("role") == "04" || $this->newsession->userdata("role") == "08") {
                $this->load->model('dashboard_act');
                $arrdata = $this->dashboard_act->get_data();
                $this->content = (!$this->content) ? $this->load->view($this->ineng.'/backend/dashboard/default', $arrdata, true) : $this->content;
            }else{
                if ($this->newsession->userdata("role") == "05") {
                $data['permohonan'] = $this->db->query("select * from view_permohonan where trader_id='" . $this->newsession->userdata('trader_id') . "' AND status_id='0000'")->num_rows();
                $data['terkirim'] = $this->db->query("select * from view_permohonan where trader_id='" . $this->newsession->userdata('trader_id') . "' AND status_id='0100'")->num_rows();
                } else {
                    $this->load->model('licensing/status_act');
                    $data['jml'] = $this->status_act->get_jml();
                    $data['sla'] = $this->main->get_sla($this->newsession->userdata('role')); //print_r($data['sla']);die();
                }
                $this->content = (!$this->content) ? $this->load->view($this->ineng . '/backend/default', $data, true) : $this->content;
            }
            
            $data = $this->main->set_content('backend', $this->breadcumbs, $this->content);
            $this->parser->parse($this->ineng . '/backend/home', $data);
        } else {
            redirect(site_url('portal/news'));
            exit();
        }
    }

    public function document() {
        if ($this->newsession->userdata('_LOGGED')) {
            $this->load->model('licensing/licensing_act');
            $arrdata = $this->licensing_act->document();
            if ($this->input->post("data-post")) {
                echo $arrdata;
            } else {
                $this->content = $this->load->view('browse/grid', $arrdata, true);
                $this->index();
            }
        }
    }

    public function manual() {
        $arr = array('permits' => 'P',
            'pid' => 50,
            'cproduct' => 32,
            'ccode' => 'OJAN',
            'item' => 4,
            'car' => '1');
        echo $this->main->um_garansi($arr);
    }

}
