<?php
class LanguageLoader{
    function initialize() {
        $ikd =& get_instance();
        $ikd->load->helper('language');
        if($ikd->session->userdata('site_lang')) $ikd->lang->load('message',$ikd->session->userdata('site_lang'));
		else $ikd->lang->load('message','id');
    }
}