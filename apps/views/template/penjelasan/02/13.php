<?php
error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $def_margin = array(15,50,15,15);

    function pdf($orientasi, $ukuran, $kertas, $margins,$data){
        $this->ukuranhuruf = 12;
        $this->ukuranhurufdetil = 10;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->pdf->footer = 1;
		if(is_array($margins) and count($margins) == 4) 
		{
			$this->pdf->SetMargins($margins[0],$margins[1],$margins[2],$margins[3]); }
		else {

		$this->pdf->SetMargins($this->def_margin[0],$this->def_margin[1],$this->def_margin[2],$this->def_margin[3]);
		}
		$this->data($data, $margins);
    }

    function Footer(){
        if ($this->pdf->footer == 1) {
            #$this->pdf->SetY(-45);
            $this->pdf->SetFont('Arial', '', 9);
            $Yget2 = $this->pdf->GetY();
            $this->pdf->SetY($Yget2);
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
        }
    }
	
	/*----------------------------------------------------------- Common Function ---------------------------------------------------------*/
	function NewIkd(){
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }
    
    function NewIkdLandscape(){
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }
	
    function iif($kondisi, $benar, $salah){
        if ($kondisi) return $benar;
        else return $salah;
    }

    function convert_date($date){
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }
	
	function FormatNPWP($varnpwp){
		$varresult = '';
		$varresult = substr($varnpwp,0,2).".".substr($varnpwp,2,3).".".substr($varnpwp,5,3).".".substr($varnpwp,8,1)."-".substr($varnpwp,9,3).".".substr($varnpwp,12,3);
		return $varresult;
	}

	function FormatTDP($varnoijin){
		$varresult = '';
		$varresult = substr($varnoijin,0,2).".".substr($varnoijin,2,2).".".substr($varnoijin,4,1).".".substr($varnoijin,5,2).".".substr($varnoijin,7,5);
		return $varresult;
	}
	/*------------------------------------------------------------ End Common Function ----------------------------------------------------*/

	/*------------------------------------------------------------ ng'Batik Function -------------------------------------------------------*/
    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 3.7) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);
        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);
        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
            $this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }
	/*------------------------------------------------------------ End ngBatik Function -------------------------------------------------------*/
	
	/*------------------------------------------------------------ Parsing Data Function -------------------------------------------------------*/
	function data($arrdata, $margin){
        $this->NewIkd();
		$dflength = 174;
		$kiri = $this->def_margin[0];
		$kanan = $this->def_margin[3];
		$lebarstaticpendaftaran = 54;
		if(is_array($margin) and count($margin) == 4){
			$kiri = $margin[0];
			$kanan = $margin[3];
			if($kiri > $this->def_margin[0]){
				$ldef = $kiri - $this->def_margin[0];	
				$dflength = $dflength - $ldef;
			}else{
				$ldef = $this->def_margin[0]- $kiri;	
				$dflength = $dflength + $ldef;
			}
			
			if($kanan > $this->def_margin[3]){
				$rdef = $kanan - $this->def_margin[3];
				$dflength = $dflength - $rdef;
			}else{
				$rdef = $this->def_margin[3] - $kanan;
				$dflength = $dflength + $rdef;
			}
		}
		$x = $kiri;
		$this->tulis($x, '+0', 'Nomor', '', $dflength, '', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x+15, '+0', ':', '', $dflength, '', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x+20, '+0', $arrdata['row']['no_penjelasan'], '', $dflength, '', 'Arial', '', $this->ukuranhuruf-1);
		
		
		$this->tulis($x+115, '+0', 'Jakarta, '.$this->convert_date(date('Y-m-d')), '', $dflength, '', 'Arial', '', $this->ukuranhuruf-1);
		
		
		$this->tulis($x, '+5', 'Lampir', '', $dflength, '', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x+15, '+0', ':', '', $dflength, '', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x+20, '+0', '-', '', $dflength, '', 'Arial', '', $this->ukuranhuruf-1);
		
		$this->tulis($x, '+5', 'Perihal', '', $dflength, '', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x+15, '+0', ':', '', $dflength, '', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x+20, '+0', 'Penjelasan Permohonan '.$arrdata['row']['nama_izin'], 'L', 45, 'M', 'Arial', '', $this->ukuranhuruf-1);
		
		$this->tulis($x+115, '-20', 'Kepada Yth.', '', $dflength, '', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x+115, '+5', 'Sdr. '.$arrdata['row']['nama_pj'], '', $dflength, '', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x+115, '+5', ucfirst(strtolower($arrdata['row']['jabatan_pj'])), '', $dflength, '', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x+115, '+5', 'PT .'.$arrdata['row']['nm_perusahaan'], '', 65, 'M', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x+115, '+0', ucfirst(strtolower($arrdata['row']['almt_perusahaan'])).' '.$arrdata['row']['prop_perusahaan'].' ', 'L', 55, 'M', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x+115, '+0', $arrdata['row']['prop_perusahaan'], 'L', 55, '', 'Arial', '', $this->ukuranhuruf-1);
		$this->pdf->Ln(10);
		$this->tulis($x, '+5', '      Berdasarkan hasil penelitian kami atas surat permohonan Saudara No '.$arrdata['row']['no_aju'].' tanggal '.$this->convert_date($arrdata['row']['tgl_aju']).' yang kami terima tanggal '.$this->convert_date($arrdata['row']['tgl_terima']).' perihal tersebut diatas, dengan ini kami beritahukan bahwa permohonan Saudara untuk pendaftaran '.$arrdata['row']['permohonan'].' sebagai '.$arrdata['row']['nama_izin'].' dari ... belum dapat kami penuhi mengingat belum memenuhi persyaratan sebagaimana dimaksud dalam Peraturan Menteri Perdagangan No. 11/M-DAG/PER/3/2006 tanggal 29 Maret 2006, yaitu:  ' ,'J', $dflength, 'M', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x+5, '+5', $arrdata['catatan'] ,'L', $dflength, 'M', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x, '+5', '    Sehubungan dengan hal tersebut, bersama ini berkas permohonan Saudara kami kembalikan untuk dilengkapi persyaratan sebagaimana dimaksud diatas untuk dapat diproses lebih lanjut' ,'J', $dflength, 'M', 'Arial', '', $this->ukuranhuruf-1);
  
		$this->tulis($x, '+2', '     Demikian atas perhatian Saudara kami ucapkan terima kasih' ,'J', $dflength, 'M', 'Arial', '', $this->ukuranhuruf-1);
		
		$this->pdf->Ln(30);
		
		$this->tulis($x + 74, '+10', 'A.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x + 75, '+4', 'Direktur Bina Usaha Perdagangan', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', $this->ukuranhuruf-1);
		
		$this->pdf->Ln(20);
		$this->tulis($x + 55, '+', 'Fetnayeti', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', $this->ukuranhuruf-1);
		
		$this->tulis($x, '+25', 'Tembusan :', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf-1);
		//for ($i=0; $i < count($arrdata['tembusan']) ; $i++) { 
		$this->tulis($x, '+6', '1. Dirjen Perdagangan Dalam Negeri', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf-1);
		$this->tulis($x, '+6', '2. Pertinggal', 'L', $lebarnomor, '','Arial', '', $this->ukuranhuruf-1);
		//}
		/*
		$this->tulis($x, '+8', strtoupper($arrdata['row']['jenis_agen']), 'C', $dflength, '', 'Arial', 'B', 20);
		$this->tulis($x, '+8', 'BARANG PRODUKSI '. strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 20);
		$this->pdf->Ln(10);
		#Border Nomor Telepon
		
		
		$this->pdf->Ln(5);
		
		#Border Atas
		
		
		//$this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext, $x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 11);
		//$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
		
		
		$this->pdf->SetFont('Arial','',10);		
		if($arrdata['row']['jns_produk'] == "02"){
			$this->pdf->SetFillColor(255,0,0);
		}else{
			$this->pdf->SetFillColor(30,144,255);
		}
		$this->pdf->MultiCell(0,8,"		NOMOR                                         :  ".$arrdata['row']['no_aju'],1,1,true);		
		$this->pdf->SetFont('Arial','',10);
		if($arrdata['row']['jns_produk'] == "02"){
			$this->pdf->SetFillColor(255,0,0);
		}else{
			$this->pdf->SetFillColor(30,144,255);
		}
		$this->pdf->MultiCell(0,8,"		BERLAKU SAMPAI DENGAN       :  ".$arrdata['row']['exp'],1,1,true);
		/*
		$this->pdf->tulis($x + 0.5, 0, '  NOMOR ', 'L', 180, '', 'Arial', '', 10,1,10);
		$this->pdf->tulis($x + 55, '-1', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
	
		$this->tulis($x + 60, '0', 'FAX//1234567' , 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		
		
		#Border Kegiatan Usaha
		//$this->pdf->Ln();
		//$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
		
		
		$this->tulis($x + 0.5, '+11', '  BERLAKU SAMPAI DENGAN', 'L', 180, '', 'Arial', '', 10,1,10);
		$this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', 'MARET 2020', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		//$this->pdf->Ln(8);
		//$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
		
		$this->pdf->Ln(10);
		
		
		$this->tulis($x + 55, '-5', 'DIKEMBALIKAN KEPADA :' , '', 70, '', 'Arial', '', 10,0,13);
		$this->pdf->Ln(5);
		#nama perusahaan
		$this->tulis($x + 1, 0, '1.	NAMA PERUSAHAAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '0', strtoupper($arrdata['row']['nm_perusahaan']) , '', 70, '', 'Arial', 'B', 10,0,13);
		$this->pdf->Ln(8);
		#alamat
		$this->tulis($x + 1, '0', '2.	ALAMAT ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', ucfirst($arrdata['row']['almt_perusahaan']).' Kel. '.$arrdata['row']['kel_perusahaan'].', Kec. '.$arrdata['row']['kec_perusahaan'].', '.$arrdata['row']['prop_perusahaan'] , '', 140, 'M', 'Arial', '', 10,0,13);
	//	$this->tulis($x + 60, '+0',  , '', 70, '', 'Arial', '', 10,0,13);
		
		//$this->pdf->Ln(8);
		#tlp/fax
		$this->tulis($x + 1, '+', '3.	NO. TELP / FAX ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '0', $arrdata['row']['telp'].' / '.$arrdata['row']['fax'] , '', 70, '', 'Arial', '', 10,0,13);
		
		$this->pdf->Ln(8);
		#nama pj
		$this->tulis($x + 1, '+', '4.	NAMA PENANGGUNG JAWAB ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '0', strtoupper($arrdata['row']['nama_pj']) , '', 70, '', 'Arial', 'B', 10,0,13);
		$this->pdf->Ln(10);
		$this->tulis($x + 1, '+0', 'Karena alasan sebagai berkut       : '.$arrdata['catatan'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		#footer
		#terbit
		$this->pdf->Ln(40);
		$this->tulis($x + 105, '+23', 'Diterbitkan di ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
		$this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 137, '0', 'J A K A R T A' , '', 70, '', 'Arial', 'B', 10,0,13);
		#tanggal
		$this->tulis($x + 105, '+4', 'pada tanggal ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
		$this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 137, '0', $this->convert_date(date('Y-m-d')) , '', 70, '', 'Arial', 'B', 10,0,13);
		
		$this->pdf->Ln(10);
		$this->tulis($x + 75, '+', 'a.n. MENTRI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
		$this->tulis($x + 75, '+4', 'Direktur Bina Usaha Perdagangan', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		
		$this->pdf->Ln(20);
		$this->tulis($x + 55, '+', 'Fetnayeti', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
	*/
    }
	/*------------------------------------------------------------ End Parsing Data Function ---------------------------------------------------*/    
	/*------------------------------------------------------------ Generate Function -----------------------------------------------------------*/
	function generate($arrdata){
        if($arrdata['cetak'] != ''){
        	$namafile = $arrdata['namafile'];
        	$dir = $arrdata['dir']; 
        	if(file_exists($dir) && is_dir($dir)){
				$path = 'upL04d5/document/DOKAGEN/'. date("Y")."/".date("m")."/".date("d").'/'.$namafile.'.pdf';
			}else{
				if(mkdir($dir, 0777, true)){
					if(chmod($dir, 0777)){
						$path = 'upL04d5/document/DOKAGEN/'. date("Y")."/".date("m")."/".date("d").'/'.$namafile.'.pdf';
					}
				}
			}
        	$this->pdf->Output($path,'F');	
        }else{
			$this->pdf->Output();
		}
    }
	/*------------------------------------------------------------ End Generate Function -------------------------------------------------------*/

}

?>