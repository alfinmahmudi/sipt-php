<?php

error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $ukuranhurufkop;
    var $huruf;
    var $def_margin = array(15, 40, 20, 15);

    function pdf($orientasi, $ukuran, $kertas, $margins, $data) {
        $this->ukuranhuruf = 12;
        $this->ukuranhurufdetil = 10;
        $this->ukuranhurufkop = 14;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->huruf = 'Arial';
        $this->qrcode = new QRcode();
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->pdf->footer = 1;
        if (is_array($margins) and count($margins) == 4) {
            $this->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
        } else {

            $this->pdf->SetMargins($this->def_margin[0], $this->def_margin[1], $this->def_margin[2], $this->def_margin[3]);
        }
        $this->data($data, $margins);
    }

    function Footer($arrdata) {
        if ($this->pdf->footer == 1) {
            $this->pdf->SetY(-35);
            $this->pdf->SetFont('Arial', '', 9);
            /* $Yget2 = -60;
              $this->pdf->SetY($Yget2); */
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
        }
    }
    function RotatedText($x, $y, $txt, $angle) {
        //Text rotated around its origin
        $this->pdf->Rotate($angle, $x, $y);
        $this->pdf->Text($x, $y, $txt);
        $this->pdf->Rotate(0);
    }

    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */

    function NewIkd($arrdata) {
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Footer($arrdata);
        $this->pdf->SetY($Yget);
    }

    function NewIkdLandscape() {
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }

    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }

    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */

    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */

    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 4) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);

        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));

        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));

        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);

        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
        //$this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
            $this->pdf->MultiCell($lebar, $tinggi, $teks, 0, $align, 0);
        else if ($tipe == "I")
            $this->pdf->Image('upL04d5' . $teks, $xpos, $ypos, 30, 40);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }

    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */

    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */

    function data($arrdata, $margin) {
        $this->NewIkd($arrdata);
        $dflength = 192;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarlabelstatic = 70;
        $lebarnomor = 8;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        }
        $x = $kiri;
if ($arrdata['row']['status'] != '1000') {
                $this->pdf->SetTextColor(219, 219, 219);
                $this->pdf->SetFont('Arial', 'B', 60);
                $this->RotatedText(50, 200, "D    R    A    F    T", 35); 
                $this->pdf->SetTextColor(000, 000, 000);
            }
        $this->tulis($x, '+6', 'Tanda Daftar Pelaku Usaha Distribusi', 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);
        $this->tulis($x, '+6', 'Barang Kebutuhan Pokok Sebagai ' . $arrdata['row']['jenis_izin'], 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);

        $this->tulis($x, '+8', 'Nomor : ' . $arrdata['row']['no_izin'], 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);

        $this->pdf->Ln(8);
        #Nama Perusahaan
        // $this->tulis($x, '+2', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        // $this->tulis(($x + $lebarnomor), 0, 'Nama Perusahaan', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['nm_perusahaan']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, 'B', $this->ukuranhuruf);

        $this->tulis($x, '+2', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Nama Perusahaan', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, strtoupper($arrdata['row']['nm_perusahaan']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, 'B', $this->ukuranhuruf, 0, 5);

        $this->tulis($x, '+2', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Kelembagaan Usaha', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['jenis_izin']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);

        #NPWP
        $this->tulis($x, '+6', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Nomor Pokok Wajib Pajak', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['npwp']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);

        #Alamat Perusahaan
        $this->tulis($x, '+6', '4.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Alamat Kantor dan Nomor Telepon', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, strtoupper(strtolower(trim($arrdata['row']['almt_perusahaan']))) . ', Kel. ' . ucwords($arrdata['row']['kel_perusahaan']) . ', Kec. ' . ucwords($arrdata['row']['kec_perusahaan']) . ', ' . ucwords($arrdata['row']['kab_perusahaan']) . ', Prop. ' . ucwords($arrdata['row']['prop_perusahaan']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'No. Telp/Fax: ' . $this->iif($arrdata['row']['telp'], $arrdata['row']['telp'], '-') . '/' . $this->iif($arrdata['row']['fax'], $arrdata['row']['fax'], '-'), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);


        #Penanggung Jawab
        $this->tulis($x, '+1', '5.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Nama Pemilik / Penanggung Jawab', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, strtoupper($arrdata['row']['nama_pj']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);

        #Alamat Penanggung Jawab
        $this->tulis($x, '+6', '6.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Alamat Pemilik / Penanggung Jawab', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords(trim($arrdata['row']['alamat_pj'])) . ', Kel. ' . ucwords($arrdata['row']['kel_pj']) . ', Kec. ' . ucwords($arrdata['row']['kec_pj']) . ', ' . ucwords($arrdata['row']['kab_pj']) . ', Prop. ' . ucwords($arrdata['row']['prop_pj']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        #Nilai modal
        // $this->tulis($x, '+1', '7.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        // $this->tulis(($x + $lebarnomor), 0, 'Wilayah Distribusi', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['pemasaran'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf);

        $this->tulis($x, '+1', '7.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Wilayah Distribusi', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['pemasaran'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        #Kegiatan Usaha
        $this->tulis($x, '+1', '8.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Bidang Usaha', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['detil']['bapok'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        #Kelembagaan
        // $this->tulis($x, '+6', '8.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        // $this->tulis(($x + $lebarnomor), 0, 'Jenis Barang Kebutuhan Pokok yang Diperdagangkan', 'L', $lebarlabelstatic, 'M', $this->huruf, '', $this->ukuranhuruf, '', '5');
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), '-9', ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        // $no_det = 1;
        // foreach ($arrdata['detil'] as $detil) {
        //     $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), '+6', $no_det.') '.$detil, 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf, 0, 5);
        //     $no_det++;
        // }

        $this->tulis($x, '+1', '9.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Jenis Barang Kebutuhan Pokok yang Diperdagangkan', 'L', $lebarlabelstatic, 'M', $this->huruf, '', $this->ukuranhuruf, '', '5');
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), '-9', ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['detil']['jenis'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        if ($arrdata['row']['status_dok'] == '02') {
            $this->pdf->SetTextColor(112, 121, 125);
            $this->pdf->SetFont('Arial', 'B', 60);
            $this->RotatedText(110, 260, "DICABUT", 0);
            $this->pdf->SetTextColor(000, 000, 000);
        }

        $this->pdf->Ln(20);

        #Dikeluarkan di 
        //$this->tulis($x, '+6','', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 25)), '+5', 'Dikeluarkan di', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 55)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 57)), 0, 'J A K A R T A', 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, 'B', $this->ukuranhuruf);

        #Tgl Awal
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 25)), '+5', 'Pada Tanggal', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 55)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 57)), 0, $arrdata['row']['tgl_izin'], 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);

        #Tgl Akhir 
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 25)), '+5', 'Berlaku s/d', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 55)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        if (trim($arrdata['row']['tgl_izin_exp']) == '') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 57)), 0, ucwords(date_indo($arrdata['suggest']['tgl_izin_exp'])), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf);
        } else {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 57)), 0, ucwords($arrdata['row']['tgl_izin_exp']), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf);
        }

        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+10', 'a.n. MENTERI PERDAGANGAN', 'C', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);

        if ($arrdata['row']['jabatan_ttd'] == '') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 30)), '+5', $arrdata['ttd']['jabatan'], 'C', $lebarlabelstatic + 10, 'M', 'Arial', '', $this->ukuranhuruf);
        } else {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 30)), '+5', $arrdata['row']['jabatan_ttd'], 'C', $lebarlabelstatic + 10, 'M', 'Arial', '', $this->ukuranhuruf);
        }
        if ($arrdata['row']['status'] == '1000') {
        // $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '+5', 'Bina Usaha Perdagangan', '', 25, '', 'Arial', '', 10);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+7', 'TTD', 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        }else{
            $this->pdf->ln(15);
        }
        if ($arrdata['row']['nama_ttd'] == '') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+7', $arrdata['ttd']['nama'], 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        } else {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+7', $arrdata['row']['nama_ttd'], 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        }

        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 30)), '+5', $arrdata['row']['jabatan_ttd'], 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 30)), '+7', 'TTD', 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 30)), '+7', $arrdata['row']['nama_ttd'], 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 30)), '+2', 'NIP : ' . $arrdata['row']['nip_ttd'], 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        #qrcode
        //$string = $arrdata['row']['nm_perusahaan'].'|'.$this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-').'|'.$arrdata['row']['tgl_izin'].'|'.$arrdata['link'];
        if ($arrdata['row']['status'] == '1000') {
        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor ), ($this->pdf->GetY() - 35), 25); #Coba diganti ganti aja 30 dan 15 nya.
        }else{
            $this->pdf->SetXY($x + ($lebarlabelstatic + $lebarnomor),$this->pdf->GetY() - 35);
            $this->pdf->drawTextBox('QR CODE', 25, 25, 'C', 'M');
        }
        if ($arrdata['urlFoto']['url'] != "") {
            $this->pdf->Image('upL04d5' . $arrdata['urlFoto']['url'], 55, $this->pdf->GetY() - 37, 22, 28);
        }

        $this->pdf->Ln(30);
        #Tembusan
        if (count($arrdata['tembusan']) > 0) {
            $this->tulis($x, '+10', '', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf - 4);
            for ($i = 0; $i < count($arrdata['tembusan']); $i++) {
                $this->tulis($x, '+4', $arrdata['tembusan'][$i]['urutan'] . '.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 4);
                $this->tulis($x + 3.2, 0, $arrdata['tembusan'][$i]['keterangan'] . ';', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 4);
            }
        }

        $this->tulis(20, 340, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
    }

    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */

    function generate($arrdata) {
        if ($arrdata['cetak'] != '') {
            $namafile = $arrdata['namafile'];
            $dir = $arrdata['dir'];
            if (file_exists($dir) && is_dir($dir)) {
                $path = 'upL04d5/document/DOKSIPDIS/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
            } else {
                if (mkdir($dir, 0777, true)) {
                    if (chmod($dir, 0777)) {
                        $path = 'upL04d5/document/DOKSIPDIS/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
                    }
                }
            }
            $this->pdf->Output($path, 'F');
        } else {
            $this->pdf->Output();
        }
    }

    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}

?>