<?php

error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $ukuranhurufkop;
    var $huruf;
    var $def_margin = array(15, 40, 20, 15);
    var $qrCodeString;

    function pdf($orientasi, $ukuran, $kertas, $margins, $data) {
        $this->ukuranhuruf = 11;
        $this->ukuranhurufdetil = 10;
        $this->ukuranhurufkop = 14;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->huruf = 'Arial';
        $this->qrcode = new QRcode();
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->pdf->footer = 1;
        if (is_array($margins) and count($margins) == 4) {
            $this->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
        } else {

            $this->pdf->SetMargins($this->def_margin[0], $this->def_margin[1], $this->def_margin[2], $this->def_margin[3]);
        }
        $this->data($data, $margins);
    }

    function Footer($arrdata) {
        if ($this->pdf->footer == 1) {
            $this->pdf->SetY(-53);
            $this->pdf->SetFont('Arial', '', 9);
            /* $Yget2 = -60;
              $this->pdf->SetY($Yget2); */
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
            //$string = $arrdata['row']['no_izin'];
        }
    }

    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */

    function NewIkd($arrdata) {
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Footer($arrdata);
        $this->pdf->SetY($Yget);
    }

    function NewIkdLandscape() {
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }

    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }

    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */

    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */

    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 4) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);

        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));

        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));

        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);

        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
        //$this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
            $this->pdf->MultiCell($lebar, $tinggi, $teks, 0, $align, 0);
        else if ($tipe == "I")
            $this->pdf->Image('upL04d5' . $teks, $xpos, $ypos, 30, 40);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }

    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */

    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */

    function data($arrdata, $margin) {
        $this->NewIkd($arrdata);
        $dflength = 192;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarlabelstatic = 70;
        $lebarnomor = 8;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        } else {
            //$dflength = $dflength - $this->def_margin[3];
        }
        $tgl = $this->convert_date(date('Y-m-d'));
        $x = $kiri;
        //$this->tulis($x, '0', 'LAMPIRAN III', 'L', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf-2);
        $this->tulis($x + 10, '+5', 'Nomor', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 30, '+0', ':', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 32, '+0', $arrdata['row']['no_izin'], 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 120, '+0', 'Jakarta, ' . $arrdata['row']['izin_ind'], 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);

        $this->tulis($x+ 10, '+5', 'Lampiran', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 30, '+0', ':', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 32, '+0', '-', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);

       // $this->tulis($x + 34, '0', 'Kepada Yth,', 'C', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 33, '+4', 'Kepada Yth,', 'C', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 33, '+4', ucfirst($arrdata['row']['jabatan_pj']), 'C', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 120, '0', ucfirst($arrdata['row']['nm_perusahaan']), '', $dflength, 'M', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 120, '+0', ucfirst($arrdata['row']['almt_perusahaan']), 'J', 65, 'M', $this->huruf, '', $this->ukuranhuruf - 2);

        $this->tulis($x + 10, '-15', 'Perihal', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 30, '+0', ':', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 32, '+0', 'Persetujuan Perdagangan Antarpulau', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 32, '+4', 'Gula Kristal Rafinasi (SPPAGKR)', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);

        if ($arrdata['row']['status_dok'] == '02') {
            $this->pdf->SetTextColor(112, 121, 125);
            $this->pdf->SetFont('Arial', 'B', 60);
            $this->RotatedText(110, 260, "DICABUT", 0);   
            $this->pdf->SetTextColor(000, 000, 000);
        }
        
        $this->pdf->Ln(15);
		
		$this->tulis($x + 30, '+8', 'Berkenaan dengan surat saudara Nomor :  ' . $arrdata['row']['no_aju'], 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
		$this->tulis($x + 122, '0', 'tanggal ' . $this->convert_date($arrdata['row']['tgl_aju']).' perihal tersebut', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
		$this->tulis($x + 20, '+4', 'diatas, bersama ini disampaikan :', 'J', ($dflength - $kanan) + 8, 'M', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
		
        $this->tulis($x + 20, '+5', '1.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 25), '0', 'Dapat disetuji perpanjangan waktu kedatangan sebagaiamana yang tercantum dalam Surat Persetujuan Perdagangan Antarpulau Gula Kristal Rafinasi (SPPAGKR) Nomor : 714/UPP/SPPAGKR/08/2016 tanggal 08 Agustus 2016, sebagai berikut :', 'J', ($dflength - $kanan) - 17 , 'M', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
		
        //$this->tulis($x, '0', 'Sehubungan dengan : ', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 70, '+5', 'S E M U L A', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 110, '0', 'M E N J A D I ', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
		
		 $this->tulis($x + 67, '+5', '08 Oktober 2016', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x + 107, '0', '08 November 2016', 'L', $dflength, '', $this->huruf, '', $this->ukuranhuruf - 2);
		
		$this->tulis($x + 20, '+9', '2.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 25), '0', 'Ketentuan dan persyaratan lain sebagaimana tercantum dalam Surat Persetujuan Perdagangan Antarpulau Gula Kristal Rafinasi (SPPAGKR) tersebut diatas, dengan jumlah sisa gula yang belum dikapalkan sebanyak '.$arrdata['row']['jml_gula'].' (ton) tetap berlaku.', 'J', ($dflength - $kanan) - 17 , 'M', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
		
		$this->tulis($x + 20, '+5', '3.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 25), '0', 'Persetujuan perpanjangan ini merupakan yang terakhir. Apabila terdapat sisa gula yang belum didistribusikan, maka harus diajukan permohonan SPPAGKR kembali.', 'J', ($dflength - $kanan) - 17 , 'M', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

		 $this->tulis(($x + 20), '+5', 'Demikian disampaikan untuk dilaksanakan dengan sebaik-baiknya.', 'J', ($dflength - $kanan) - 17 , 'M', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
       
		/*
        $this->tulis($x, '+8', 'Serta berdasarkan Peraturan Menteri Perdagangan Nomor : 74/M-DAG/PER/9/2015 tanggal 28 September 2015 tentang Perdagangan Antar Pulau Gula Kristal Rafinasi dan Peraturan Menteri Perdagangan Nomor 53/M-DAG/PER/9/2014 tanggal 2 September 2014 tentang Pelayanan Terpadu Perdagangan, dengan ini diberikan :', 'J', ($dflength - $kanan) + 8, 'M', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+5', 'SURAT PERSETUJUAN PERDAGANGN ANTARPULAU GULA KRISTAL RAFINASI (SPPAGKR)', 'C', $dflength - 10, 'M', $this->huruf, 'B', $this->ukuranhuruf);

        /* $this->tulis($x, '+2', 'Nomor    : '.$arrdata['row']['no_izin'], 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf-2,0,5);
          $this->tulis($x, '+0', 'Tanggal  :'.$arrdata['row']['izin_ind'], 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf-2,0,5);
          $this->tulis($x, '+2', 'dengan ini diberitahukan bahwa :', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf-2,0,5);

          $this->tulis($x, '+0', 'Nama dan alamat Perusahaan', 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf-2,0,5);
          $this->tulis($x+64, '+0', ':', 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf-2,0,5);
          $this->tulis(($x + 66), '+0', ucfirst($arrdata['row']['nm_perusahaan']).' '.ucfirst($arrdata['row']['almt_perusahaan']), 'L', ($dflength-$kanan) - 50 , 'M', $this->huruf, '', $this->ukuranhuruf-2,0,5);

          $this->tulis($x, '+0', 'Nama Pemilik dan Penanggung jawab', 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf-2,0,5);
          $this->tulis(($x+64), 0, ': '.ucfirst($arrdata['row']['nama_pj']), 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf-2,0,5);

          $this->tulis($x, '+6', 'Nomor Pengakuan PGAPT', 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf-2,0,5);
          $this->tulis(($x + 64), 0, ': '.$arrdata['row']['no_pgapt'], 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf-2,0,5);
          $txt ="Surat Persetujuan Perdagangan Antar Pulau Gula Kristal Rafinasi(SPPAGKR),";
          $this->tulis($x, '+5', 'diberikan', 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf-2,0,5);
          $this->tulis($x + 15, '+0', $txt, 'L', ($dflength-$kanan), '', $this->huruf, 'B', $this->ukuranhuruf-2,0,5); */
		  /*
        $this->tulis($x, '+0', 'Untuk Pengangkutan:', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+6', '1.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'Jenis Gula', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 64), 0, ': ' . $arrdata['row']['jns_gula'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+5', '2.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'Jumlah (ton)', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 64), 0, ': ' . $arrdata['row']['jml_gula'] . ' (ton)', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+5', '3.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'Gula Berasal dari', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 64), 0, ': ' . $arrdata['row']['asal_gula'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+5', '4.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'Nama Perusahaan Industri Rafinasi', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 64), 0, ': ' . ucfirst($arrdata['row']['nm_perusahaan']), 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+5', '5.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'Alamat                                                        : ', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 66), 0, ucfirst(trim($arrdata['row']['almt_perusahaan'])) . ', ' . trim($arrdata['row']['kel_perusahaan']) . ', ' . trim($arrdata['row']['kec_perusahaan']) . ', ' . trim($arrdata['row']['kab_perusahaan']) . ', ' . trim($arrdata['row']['prop_perusahaan']), 'L', ($dflength - $kanan) - 60, 'M', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+0', '6.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'Pelabuhan Asal/Muat', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 64), 0, ': ' . $arrdata['row']['loading_gula'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+5', '7.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'Pelabuhan Tujuan', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 64), 0, ': ' . $arrdata['row']['discharge_gula'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+5', '8.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'Nama Industri penerima/pengguna', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 64), 0, ': ', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        foreach ($arrdata['nama_industri'] as $key) {
            $this->tulis(($x + 64), 0, ': ' . $key['nama_produsen'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        }

        $this->tulis($x, '+6', 'Dengan Ketentuan sebagai berikut :', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis($x, '+5', 'a.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'Surat Persetujuan Perdagangan Antar Pulau Gula Kristal Rafinasi (SPPAGKR) ini DIBATALKAN apabila mengubah, menambah atau mengganti isi yang tercantu dalam SPPAGKR. ', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+0', 'b.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'Gula yang diantarpulaukan ini hanya ditujukan untuk perusahaan industri penerima/pengguna yang tercantum dalam SPPAGKR ini', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+0', 'c.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'SPPAGKR merupakan kesatuan dokumen yang wajib disertakan dalam pengangkutan antar pulau gula kristal rafinasi.', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+0', 'd.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'Saudara wajib menyampaikan laporan realisasi antar pulau gula kristal rafinasi kepada Direktur Jenderal Perdagangan Dalam Negeri, Kementerian Perdagangan Jl. M.I. Ridwan Rais Nomor 5 Jakarta Pusat setiap satu bulan sekali.', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x, '+0', 'e.', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);
        $this->tulis(($x + 5), '0', 'SPPAGKR ini hanya berlaku untuk pengangkutan dan pengapalan sejumlah tersebut pada butir 2 diatas sampai dengan batas waktu kedatangan '.$this->convert_date($arrdata['row']['exp']==""?date("Y-m-d",strtotime("+30 days")):$arrdata['row']['exp']), 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);

        $this->tulis($x + 5, '+0', 'Demikaian agar saudara maklum', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2, 0, 5);*/

        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+10', 'a.n. MENTERI PERDAGANGAN', 'C', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf - 2);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+5', $arrdata['row']['jabatan_ttd'], 'C', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf - 2);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+10', 'TTD', 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf+5);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+10', $arrdata['row']['nama_ttd'], 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf - 2);
        $this->pdf->Ln();
        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['izin_ind'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 12), ($this->pdf->GetY() - 25), 20); #Coba diganti ganti aja 30 dan 15 nya.
        if(count($arrdata['tembusan']) > 0){
            $this->tulis($x, '+', 'Tembusan :', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'U', 8);
            for($i=0;$i<count($arrdata['tembusan']);$i++){
                $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'].'.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 2);
                $this->tulis($x + 5, 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf - 2);
            }
        }
        // if (count($arrdata['tembusan']) > 0) {
        #Tembusan
        // $this->tulis($x, '-20', '', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        // $this->tulis($x, '', 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf - 2);
        // $this->tulis($x, '+4', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 2);
        // $this->tulis(($x + $lebarnomor), 0, 'Dirjen PDN, Kemendag', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2);
        // $this->tulis($x, '+4', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 2);
        // $this->tulis(($x + $lebarnomor), 0, 'Dirjen IA, Kementerian Perindustrian', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2);
        // $this->tulis($x, '+4', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 2);
        // $this->tulis(($x + $lebarnomor), 0, 'Sesditjen PDN, Kemendag', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2);
        // $this->tulis($x, '+4', '4.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 2);
        // $this->tulis(($x + $lebarnomor), 0, 'Koordinator dan Pelaksana UPP, Kemendag', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2);
        // for ($i = 0; $i < count($arrdata['tembusan']); $i++) {
        //     $this->tulis($x, '+4', $arrdata['tembusan'][$i]['urutan'] . '.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 2);
        //     $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2);
        // }
        //}
        // $this->tulis($x, '-40', 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf-2);
        // $this->tulis($x, '+5', '1. Dirjen PDN, Kemendag', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf-2);
        // $this->tulis($x, '+5', '2. Dirjen 1A Kementerian Perindustrian', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf-2);
        // $this->tulis($x, '+5', '3. Sesditjen Perdagangan Dalam Negeri, Kemendag', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf-2);
        // $this->tulis($x, '+5', '4. Koordinator dan Pelaksana UPP, Kemendag', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf-2);

        $this->tulis($x, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui sistem SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'BI', 7);
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        /*

          $this->tulis($x, '+6', 'SURAT IJIN USAHA PERDAGANGAN BAHAN BERBAHAYA (SIUP-B2)', 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);
          $this->tulis($x, '+6', 'SEBAGAI DISTRIBUTOR TERDAFTAR(DT-B2)', 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);

          $this->tulis($x, '+8', 'Nomor :      '.$arrdata['row']['no_izin'].'-'.$dflength, 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);

          $this->pdf->Ln(8);
          #Nama Perusahaan
          $this->tulis($x, '+2', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Nama Perusahaan', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['nm_perusahaan']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, 'B', $this->ukuranhuruf);

          #Alamat Perusahaan
          $this->tulis($x, '+6', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Alamat Kantor Perusahaan', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['almt_perusahaan']) .', Kel. '. ucwords($arrdata['row']['kel_perusahaan']) .', Kec. '. ucwords($arrdata['row']['kec_perusahaan']) .', '. ucwords($arrdata['row']['kab_perusahaan']).','  , 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'Prop. '. ucwords($arrdata['row']['prop_perusahaan'])  , 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'No.Telp :'.ucwords($arrdata['row']['telp']) , 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'No.Fax :'.ucwords($arrdata['row']['fax']) , 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);


          #Penanggung Jawab
          $this->tulis($x, '+1', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Nama Pemilik / Penanggung Jawab', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['nama_pj']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf,'', $this->ukuranhuruf);

          #Alamat Penanggung Jawab
          $this->tulis($x, '+6', '4.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Alamat Pemilik / Penanggung Jawab', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['alamat_pj']).', Kel '. ucwords($arrdata['row']['kel_pj']).', Kec '.ucwords($arrdata['row']['kec_pj']).', '.ucwords($arrdata['row']['kab_pj']).', Prov '.ucwords($arrdata['row']['prop_pj']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf);

          #NPWP
          $this->tulis($x, '+2', '5.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Nomor Pokok Wajib Pajak (NPWP)', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['npwp']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf,'', $this->ukuranhuruf);

          #Nilai modal
          $this->tulis($x, '+6', '6.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Nilai Investasi Perusahaan Seluruhnya tidak Termasuk Tanah dan Bangunan Tempat Usaha', 'L', $lebarlabelstatic , 'M', $this->huruf, '', $this->ukuranhuruf,0,5);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), '-15', ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['nilai_modal']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf,'', $this->ukuranhuruf);

          #Bidang Usaha
          $this->tulis($x, '+18', '7.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Bidang Usaha ', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords(trim($arrdata['row']['perdagangan'])), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'J', $this->huruf,'', $this->ukuranhuruf,0,5);


          #Kegiatan Usaha
          $this->tulis($x, '+6', '8.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Jenis Kegiatan Usaha', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['tipe_perusahaan']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf,'', $this->ukuranhuruf,0,5);

          #Kelembagaan
          $this->tulis($x, '+6', '9.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Jenis Barang/Jasa Dagangan Utama ', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['jenis_dagangan']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf,'', $this->ukuranhuruf,0,5);

          $this->tulis(($x-2), '+6', '10.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Surat Izin Usaha Perdagangan Bahan Berbahaya (SIUP-B2) berlaku untuk melakukan kegiatan perdagangan diseluruh wilayah Repubilk Indonesia selama perusahaan masih menjalankan kegiatan usahanya', 'J', ($dflength - $lebarnomor), 'M', 'Arial', '', $this->ukuranhuruf,0,5);




          /*
          #Keterangan 11
          $this->tulis($x, '+6', '11.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Surat Keterangan Penjual Langsung Minuman Beralkohol Golongan A ini berlaku untuk melakukan kegiatan usaha perdagangan minuman beralkohol golongan A di wilayah '.$arrdata['pemasaran'].'; sesuai Surat Penunjukan sebagai Penjual Langsung Minuman Beralkohol Golongan A dari '.$arrdata['suratSKPLA'], 'J', ($dflength - $lebarnomor), 'M', 'Arial', '', $this->ukuranhuruf,0,5);

          #Keterangan 12
          $this->tulis($x, '+1', '12.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Surat Keterangan Penjual Langsung Minuman Beralkohol Golongan A ini diberikan dengan ketentuan sebagaimana tercantum dalam halaman kedua :', 'J', ($dflength - $lebarnomor), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $yttd = $this->pdf->GetY();
         */
        #Dikeluarkan di 
        //$this->tulis($x, '+6','', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        /*
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+10', 'Dikeluarkan di', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 65)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0,'J A K A R T A', 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, 'B', $this->ukuranhuruf);

          #Tgl Awal
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+5', 'Pada Tanggal', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 65)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords($arrdata['row']['tgl_izin']), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);

          #Tgl Akhir
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+5', 'Berlaku s/d', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 65)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords($arrdata['row']['tgl_izin_exp']), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);

          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+10', 'a.n. MENTERI PERDAGANGAN', 'C', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+5', 'Direktur Bahan Pokok dan Barang Strategis', 'C', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf - 1);
          $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+25', $arrdata['row']['nama_ttd'], 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);

          #qrcode
          $string = utf8_decode($arrdata['row']['nm_perusahaan']."\n".$this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-')."\n".$arrdata['row']['tgl_izin']."\n".$arrdata['link']);
          $this->qrcode->genQRcode($string, 'L');
          $this->qrcode->disableBorder();
          $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 60),($this->pdf->GetY()+5), 20); #Coba diganti ganti aja 30 dan 15 nya.

          #foto pemilik/penanggung jawab
          $this->tulis($x+$lebarnomor+73,$yttd+200,$arrdata['urlFoto']['url'],'', '','I', $this->huruf, '', $this->ukuranhuruf);
          if(count($arrdata['tembusan']) > 0){
          #Tembusan
          $this->tulis($x, '+10','','L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          for ($i=0; $i < count($arrdata['tembusan']) ; $i++) {
          $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'].'.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf);
          }
          }

          #qrcode
          /*
          $string = $arrdata['row']['no_izin'];
          $this->qrcode->genQRcode($string, 'L');
          $this->qrcode->disableBorder();
          $this->qrcode->displayFPDF($this->pdf, 33, $ydrcode+10, 33); #Coba diganti ganti aja 30 dan 15 nya.

          #Tembusan
          $this->tulis($x, '+40','','L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          for ($i=0; $i < count($arrdata['tembusan']) ; $i++) {
          $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'].'.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf);
          }
          /*$this->tulis($x, '+6', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Kadis Perindustrian dan Perdagangan Prov. Jawa Timur;', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis($x, '+6', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Kadis Perindustrian dan Perdagangan Kota Surabaya;', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis($x, '+6', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Pertinggal;', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf); */

        #foto pemilik/penanggung jawab
        //$this->tulis($x+$lebarnomor+5,$yttd+10,$arrdata['urlFoto']['url'],'', '','I', $this->huruf, '', $this->ukuranhuruf);
        #qrcode
        /* $string = $arrdata['row']['no_izin'];
          $this->qrcode->genQRcode($string, 'L');
          $this->qrcode->disableBorder();
          $this->qrcode->displayFPDF($this->pdf, 30, $ydrcode+10, 15); #Coba diganti ganti aja 30 dan 15 nya. */

        #ketentuan
        /*
          $this->pdf->Addpage();
          $this->tulis($x, '+3','', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Surat Izin Usaha Perdagangan Bahan Berbahaya (SIUP-B2) ini ditetapkan dengan ketentuan sebagai berikut :', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis($x, '+6', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Berlaku untuk melakukan kegiatan usaha perdagangan minuman beralkohol di wilayan pemasaran yang disebutkan pada nomor 11 dengan masa berlaku sebagaimana ditetapkan dalam SIUP-B2 ini.', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis($x, '+6', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Perusahaan wajib menjalankan kegiatan usaha berdasarkan ketentuan perundang-undangan yang berlaku dan menyampaikan laporan Realisasi Pengadaan dan Penyaluran minuman beralkohol setiap triwulan tahun kalender berjalan kepada Direktur Jenderal Perdagangan Dalam Negeri Kementerian Perdagangan sebagai berikut : ', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis(($x + $lebarnomor) ,'+1', 'a.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor + 5 ), 0,'Triwulan I disampaikan pada tanggal 31 Maret ', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis(($x + $lebarnomor), '+1', 'b.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor + 5), 0,'Triwulan II disampaikan pada tanggal 30 Juni ', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis(($x + $lebarnomor), '+1', 'c.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor + 5), 0,'Triwulan III disampaikan pada tanggal 30 September ', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis(($x + $lebarnomor), '+1', 'd.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor + 5), 0,'Triwulan IV disampaikan pada tanggal 31 Desember ', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis($x, '+6', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Perusahaan wajib memberitahukan setiap ada perubahan pada perusahaan, yang menyebabkan SIUP-MB ini tidak sesuai dengan keadaan perusahaan, kepada Direktur Jenderal Perdagangan Dalam Negri Kementerian Perdagangan.', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis($x, '+6', '4.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'SIUP-B2 mempunyai masa berlaku sesuai dengan masa berlaku perjanjian tertulis dengan ketentuan paling lama 3 (tiga) tahun terhitung sejak tanggal diterbitkan dan dapat diperpanjang. Perpanjangan SIUP-MB dilakukan 1 (satu) bulan sebelum masa berlakunya berakhir.', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);
         */
    }

    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */

    function generate($arrdata) {
        //$this->pdf->Output('./pdf/'.date('YmdHis').'.pdf','F');
        if ($arrdata['cetak'] != '') {
            $namafile = $arrdata['namafile'];
            $dir = $arrdata['dir'];
            if (file_exists($dir) && is_dir($dir)) {
                $path = 'upL04d5/document/DOKSPPGRAP/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile ;
            } else {
                if (mkdir($dir, 0777, true)) {
                    if (chmod($dir, 0777)) {
                        $path = 'upL04d5/document/DOKSPPGRAP/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile ;
                    }
                }
            }
            $this->pdf->Output($path, 'F');
        } else {
            $this->pdf->Output();
        }
    }

    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}

?>