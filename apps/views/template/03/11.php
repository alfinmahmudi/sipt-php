<?php

error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $ukuranhurufkop;
    var $huruf;
    var $def_margin = array(15, 40, 20, 15);
    var $qrCodeString;

    function pdf($orientasi, $ukuran, $kertas, $margins, $data) {
        $this->ukuranhuruf = 11;
        $this->ukuranhurufdetil = 10;
        $this->ukuranhurufkop = 14;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->huruf = 'Arial';
        $this->qrcode = new QRcode();
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->pdf->footer = 1;
        if (is_array($margins) and count($margins) == 4) {
            $this->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
        } else {

            $this->pdf->SetMargins($this->def_margin[0], $this->def_margin[1], $this->def_margin[2], $this->def_margin[3]);
        }
        $this->data($data, $margins);
    }
    
    

    function Footer($arrdata) {
        if ($this->pdf->footer == 1) {
            $this->pdf->SetY(-53);
            $this->pdf->SetFont('Arial', '', 9);
            /* $Yget2 = -60;
              $this->pdf->SetY($Yget2); */
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
            //$string = $arrdata['row']['no_izin'];
        }
    }

    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */

    function NewIkd($arrdata) {
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Footer($arrdata);
        $this->pdf->SetY($Yget);
    }

    function NewIkdLandscape() {
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }

    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }
    
    function RotatedText($x, $y, $txt, $angle) {
        //Text rotated around its origin
        $this->pdf->Rotate($angle, $x, $y);
        $this->pdf->Text($x, $y, $txt);
        $this->pdf->Rotate(0);
    }
    

    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */

    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */

    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 4) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);

        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));

        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));

        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);

        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
        //$this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
            $this->pdf->MultiCell($lebar, $tinggi, $teks, 0, $align, 0);
        else if ($tipe == "I")
            $this->pdf->Image('upL04d5' . $teks, $xpos, $ypos, 30, 40);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }

    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */

    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */

    function data($arrdata, $margin) {
        $this->NewIkd($arrdata);
        $dflength = 192;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarlabelstatic = 70;
        $lebarnomor = 8;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        } else {
            //$dflength = $dflength - $this->def_margin[3];
        }
        $x = $kiri;
        
      if ($arrdata['row']['status'] != '1000') {
                $this->pdf->SetTextColor(219, 219, 219);
                $this->pdf->SetFont('Arial', 'B', 60);
                $this->RotatedText(50, 180, "D    R    A    F    T", 35);
                $this->pdf->SetTextColor(000, 000, 000);
            }
        $this->tulis($x, '+6', 'SURAT IZIN USAHA PERDAGANGAN BAHAN BERBAHAYA (SIUP-B2)', 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);
        $this->tulis($x, '+6', 'SEBAGAI DISTRIBUTOR TERDAFTAR (DT-B2)', 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);

        $this->tulis($x, '+8', 'Nomor :      ' . $arrdata['row']['no_izin'], 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);

        
        $this->pdf->Ln(8);
        #Nama Perusahaan
        $this->tulis($x, '+2', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Nama Perusahaan', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, strtoupper($arrdata['row']['nm_perusahaan']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, 'B', $this->ukuranhuruf);

        #Alamat Perusahaan
        $this->tulis($x, '+6', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Alamat Kantor Perusahaan', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords(trim($arrdata['row']['almt_perusahaan'])) . ', Kel. ' . ucwords(trim($arrdata['row']['kel_perusahaan'])) . ', Kec. ' . ucwords(trim($arrdata['row']['kec_perusahaan'])) . ', ' . ucwords(trim($arrdata['row']['kab_perusahaan'])) . ','. ' Provinsi. ' . str_replace('Dki', 'DKI', ucwords(trim($arrdata['row']['prop_perusahaan']))), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 10)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'Prop. ' . str_replace('Dki', 'DKI', ucwords(trim($arrdata['row']['prop_perusahaan']))), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'No.Telp : ' . ucwords($arrdata['row']['telp']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'No.Fax  : ' . ucwords($arrdata['row']['fax']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);


        #Penanggung Jawab
        $this->tulis($x, '+1', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Nama Pemilik / Penanggung Jawab', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, strtoupper($arrdata['row']['nama_pj']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);

        #Alamat Penanggung Jawab
        $this->tulis($x, '+6', '4.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Alamat Pemilik / Penanggung Jawab', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords(trim($arrdata['row']['alamat_pj'])) . ', Kel ' . ucwords(trim($arrdata['row']['kel_pj'])) . ', Kec ' . trim(ucwords($arrdata['row']['kec_pj'])) . ', ' . ucwords(trim($arrdata['row']['kab_pj'])) . ', Provinsi ' . str_replace('Dki', 'DKI', ucwords(trim($arrdata['row']['prop_pj']))), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 10)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        #NPWP
        $this->tulis($x, '+2', '5.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Nomor Pokok Wajib Pajak (NPWP)', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['npwp']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);

        #Nilai modal
        $this->tulis($x, '+6', '6.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Nilai Investasi Perusahaan Seluruhnya tidak Termasuk Tanah dan Bangunan Tempat Usaha', 'L', $lebarlabelstatic, 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), '-15', ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['nilai_modal']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);

        #Bidang Usaha
        $this->tulis($x, '+18', '7.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Bidang Usaha ', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'Perdagangan Barang', 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'J', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        if ($arrdata['row']['status_dok'] == '02') {
            $this->pdf->SetTextColor(112, 121, 125);
            $this->pdf->SetFont('Arial', 'B', 60);
            $this->RotatedText(110, 260, "DICABUT", 0);   
            $this->pdf->SetTextColor(000, 000, 000);
        }

        #Kegiatan Usaha
        $this->tulis($x, '+6', '8.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Jenis Kegiatan Usaha', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        if ($arrdata['row']['code_prod'] == '01') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'Distributor Terdaftar Bahan Berbahaya Produksi Dalam Negeri dari ' . trim($arrdata['row']['nama_produksi']). ' No. '. $arrdata['penunjukan']['nomor'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);
        } elseif ($arrdata['row']['code_prod'] == '02') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'Distributor Terdaftar Bahan Berbahaya dari PT. Perusahaan Perdagangan Indonesia No. '. $arrdata['penunjukan']['nomor'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);
        } else {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'Distributor Terdaftar Barang berbahaya PT. Perusahaan Perdagangan Indonesia dan ' . $arrdata['row']['nama_produksi'].' No. '.$arrdata['penunjukan']['nomor'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);
        }
        #Kelembagaan
        $this->tulis($x, '+2', '9.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Jenis Barang/Jasa Dagangan Utama ', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['jenis_dagangan']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        $this->tulis(($x - 2), '+6', '10.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Surat Izin Usaha Perdagangan Bahan Berbahaya (SIUP-B2) berlaku untuk melakukan kegiatan perdagangan di seluruh wilayah Republik Indonesia selama perusahaan masih menjalankan kegiatan usahanya.', 'J', ($dflength - $lebarnomor - 5), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);




        /*
          #Keterangan 11
          $this->tulis($x, '+6', '11.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Surat Keterangan Penjual Langsung Minuman Beralkohol Golongan A ini berlaku untuk melakukan kegiatan usaha perdagangan minuman beralkohol golongan A di wilayah '.$arrdata['pemasaran'].'; sesuai Surat Penunjukan sebagai Penjual Langsung Minuman Beralkohol Golongan A dari '.$arrdata['suratSKPLA'], 'J', ($dflength - $lebarnomor), 'M', 'Arial', '', $this->ukuranhuruf,0,5);

          #Keterangan 12
          $this->tulis($x, '+1', '12.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Surat Keterangan Penjual Langsung Minuman Beralkohol Golongan A ini diberikan dengan ketentuan sebagaimana tercantum dalam halaman kedua :', 'J', ($dflength - $lebarnomor), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $yttd = $this->pdf->GetY();
         */
        #Dikeluarkan di 
        //$this->tulis($x, '+6','', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+10', 'Dikeluarkan di', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 65)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, 'J A K A R T A', 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, 'B', $this->ukuranhuruf);

        #Tgl Awal
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+5', 'Pada Tanggal', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 65)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords($arrdata['row']['tgl_izin']), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);

        #Tgl Akhir 
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+5', 'Berlaku s/d', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 65)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        if (ucwords($arrdata['row']['permohonan']) == 'Perubahan' && trim($arrdata['row']['tgl_izin_exp']) == '' && $arrdata['row']['status'] != '1000'){
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords($arrdata['row']['tgl_izin_exp_lama']), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf);
        }elseif(trim($arrdata['row']['tgl_izin_exp']) == '') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords(date_indo($arrdata['suggest']['tgl_dok'])), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf);
        }else{
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords($arrdata['row']['tgl_izin_exp']), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf);
        }
        //$this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords($arrdata['row']['tgl_izin_exp']), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);

        if ($arrdata['panjang'] == 1 && $arrdata['row']['permohonan'] == 'Perpanjangan') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+5', 'Status', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 65)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords($arrdata['row']['permohonan']). ' Pertama (I)', 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        }elseif ($arrdata['panjang'] == 2 && $arrdata['row']['permohonan'] == 'Perpanjangan') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+5', 'Status', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 65)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords($arrdata['row']['permohonan']). ' Kedua (II)', 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        }elseif ($arrdata['panjang'] == 3 && $arrdata['row']['permohonan'] == 'Perpanjangan') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+5', 'Status', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 65)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords($arrdata['row']['permohonan']). ' Ketiga (III)', 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        }elseif ($arrdata['panjang'] == 4 && $arrdata['row']['permohonan'] == 'Perpanjangan') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+5', 'Status', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 65)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords($arrdata['row']['permohonan']). ' Keempat (IV)', 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        }elseif ($arrdata['panjang'] == 5 && $arrdata['row']['permohonan'] == 'Perpanjangan') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+5', 'Status', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 65)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords($arrdata['row']['permohonan']). ' Kelima (V)', 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        }else{
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+5', 'Status', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 65)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 67)), 0, ucwords($arrdata['row']['permohonan']), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        }

        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+10', 'a.n. MENTERI PERDAGANGAN', 'C', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);

        if ($arrdata['row']['jabatan_ttd'] == '') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+5', $arrdata['ttd']['jabatan'], 'C', $lebarlabelstatic, M, 'Arial', '', $this->ukuranhuruf - 1);
        }else{
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+5', $arrdata['row']['jabatan_ttd'], 'C', $lebarlabelstatic, M, 'Arial', '', $this->ukuranhuruf - 1);
        }
        
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+5', 'Bahan Pokok dan Barang Strategis', 'C', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf - 1);
        if ($arrdata['row']['status'] == '1000') {
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+15', 'TTD', 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf+5);
        }else{
            $this->pdf->ln(15);
        }
        if ($arrdata['row']['nama_ttd'] == '') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+10', $arrdata['ttd']['nama'], 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        }else{
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 37)), '+10', $arrdata['row']['nama_ttd'], 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        }
        #qrcode
        if ($arrdata['row']['status'] == '1000') {
        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 19), ($this->pdf->GetY() - 35), 20); #Coba diganti ganti aja 30 dan 15 nya.
        
        }else{
            $this->pdf->SetXY($x + ($lebarlabelstatic + $lebarnomor +19),$this->pdf->GetY() - 35);
            $this->pdf->drawTextBox('QR CODE', 20, 20, 'C', 'M');
        }
        #Tembusan
        if ((strtolower($arrdata['foto_pemilik']['ekstensi']) != '.pdf') && ($arrdata['row']['status'] == '1000')) {
            $this->pdf->Image('upL04d5' . $arrdata['foto_pemilik']['url'], 70, $this->pdf->GetY() - 62, 28, 38);
        }elseif(strtolower($arrdata['foto_pemilik']['ekstensi']) != '.pdf'){
            $this->pdf->Image('upL04d5' . $arrdata['foto_pemilik']['url'], 70, $this->pdf->GetY() - 52, 28, 38);
        }
        $this->pdf->Ln(40);
        $this->tulis($x, '-40', 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x, '+5', '1. Gubernur Provinsi ' . str_replace('Dki', 'DKI', ucwords(trim($arrdata['row']['prop_perusahaan']))). ';', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 2);
        // $this->tulis($x, '+5', '2. Koordinator dan Pelaksana UPP, Kementerian Perdagangan R.I;', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 2);
        // $this->tulis($x, '+5', '2. Kepala Dinas Perindustrian dan Perdagangan Provinsi;' . str_replace('Dki', 'DKI', ucwords(trim($arrdata['row']['prop_perusahaan']))), 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 2);
        $this->tulis($x, '+5', '2. Dirjen Perdagangan Dalam Negeri; ', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 2);
        if (count($arrdata['tembusan']) > 0) {
            // $this->tulis($x, '-10', '', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
            // $this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf - 2);
            for ($i = 0; $i < count($arrdata['tembusan']); $i++) {
                $this->tulis($x, '+5', $arrdata['tembusan'][$i]['urutan'] . '.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 2);
                $this->tulis($x + 3.5, 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 2);
            }
        }
        // $this->pdf->Image('upL04d5' . $arrdata['foto_pemilik']['url'], 24, $this->pdf->GetY() - 85, 35, 50);
        $this->tulis($x, 350, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'BI', 7);
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);

        #qrcode
        /*
          $string = $arrdata['row']['no_izin'];
          $this->qrcode->genQRcode($string, 'L');
          $this->qrcode->disableBorder();
          $this->qrcode->displayFPDF($this->pdf, 33, $ydrcode+10, 33); #Coba diganti ganti aja 30 dan 15 nya.

          #Tembusan
          $this->tulis($x, '+40','','L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          for ($i=0; $i < count($arrdata['tembusan']) ; $i++) {
          $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'].'.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf);
          }
          /*$this->tulis($x, '+6', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Kadis Perindustrian dan Perdagangan Prov. Jawa Timur;', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis($x, '+6', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Kadis Perindustrian dan Perdagangan Kota Surabaya;', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis($x, '+6', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Pertinggal;', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf); */

        #foto pemilik/penanggung jawab
        //$this->tulis($x+$lebarnomor+5,$yttd+10,$arrdata['urlFoto']['url'],'', '','I', $this->huruf, '', $this->ukuranhuruf);
        #qrcode
        /* $string = $arrdata['row']['no_izin'];
          $this->qrcode->genQRcode($string, 'L');
          $this->qrcode->disableBorder();
          $this->qrcode->displayFPDF($this->pdf, 30, $ydrcode+10, 15); #Coba diganti ganti aja 30 dan 15 nya. */

        #ketentuan
        /*
          $this->pdf->Addpage();
          $this->tulis($x, '+3','', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Surat Izin Usaha Perdagangan Bahan Berbahaya (SIUP-B2) ini ditetapkan dengan ketentuan sebagai berikut :', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis($x, '+6', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Berlaku untuk melakukan kegiatan usaha perdagangan minuman beralkohol di wilayan pemasaran yang disebutkan pada nomor 11 dengan masa berlaku sebagaimana ditetapkan dalam SIUP-B2 ini.', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis($x, '+6', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Perusahaan wajib menjalankan kegiatan usaha berdasarkan ketentuan perundang-undangan yang berlaku dan menyampaikan laporan Realisasi Pengadaan dan Penyaluran minuman beralkohol setiap triwulan tahun kalender berjalan kepada Direktur Jenderal Perdagangan Dalam Negeri Kementerian Perdagangan sebagai berikut : ', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis(($x + $lebarnomor) ,'+1', 'a.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor + 5 ), 0,'Triwulan I disampaikan pada tanggal 31 Maret ', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis(($x + $lebarnomor), '+1', 'b.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor + 5), 0,'Triwulan II disampaikan pada tanggal 30 Juni ', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis(($x + $lebarnomor), '+1', 'c.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor + 5), 0,'Triwulan III disampaikan pada tanggal 30 September ', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis(($x + $lebarnomor), '+1', 'd.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor + 5), 0,'Triwulan IV disampaikan pada tanggal 31 Desember ', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis($x, '+6', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'Perusahaan wajib memberitahukan setiap ada perubahan pada perusahaan, yang menyebabkan SIUP-MB ini tidak sesuai dengan keadaan perusahaan, kepada Direktur Jenderal Perdagangan Dalam Negri Kementerian Perdagangan.', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);

          $this->tulis($x, '+6', '4.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, 'SIUP-B2 mempunyai masa berlaku sesuai dengan masa berlaku perjanjian tertulis dengan ketentuan paling lama 3 (tiga) tahun terhitung sejak tanggal diterbitkan dan dapat diperpanjang. Perpanjangan SIUP-MB dilakukan 1 (satu) bulan sebelum masa berlakunya berakhir.', 'J', ($dflength-$kanan), 'M', $this->huruf, '', $this->ukuranhuruf,0,5);
         */
    }

    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */

    function generate($arrdata) {
        if ($arrdata['cetak'] != '') {
            $namafile = $arrdata['namafile'];
            $dir = $arrdata['dir'];
            if (file_exists($dir) && is_dir($dir)) {
                $path = 'upL04d5/document/DOKSIUB2/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile ;
            } else {
                if (mkdir($dir, 0777, true)) {
                    if (chmod($dir, 0777)) {
                        $path = 'upL04d5/document/DOKSIUB2/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile ;
                    }
                }
            }
            $this->pdf->Output($path, 'F');
        } else {
            $this->pdf->Output();
        }
    }

    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}

?>