<?php

error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $def_margin = array(15, 50, 15, 15);

    function pdf($orientasi, $ukuran, $kertas, $margins, $data) {
        $this->ukuranhuruf = 12;
        $this->ukuranhurufdetil = 10;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->qrcode = new QRcode();
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->pdf->footer = 1;
        if (is_array($margins) and count($margins) == 4) {
            $this->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
        } else {

            $this->pdf->SetMargins($this->def_margin[0], $this->def_margin[1], $this->def_margin[2], $this->def_margin[3]);
        }
        $this->data($data, $margins);
    }

    function Footer() {
        if ($this->pdf->footer == 1) {
            #$this->pdf->SetY(-45);
            $this->pdf->SetFont('Arial', '', 9);
            $Yget2 = $this->pdf->GetY();
            $this->pdf->SetY($Yget2);
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
        }
    }

    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */

    function NewIkd() {
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function NewIkdLandscape() {
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }

    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }

    function FormatNPWP($varnpwp) {
        $varresult = '';
        $varresult = substr($varnpwp, 0, 2) . "." . substr($varnpwp, 2, 3) . "." . substr($varnpwp, 5, 3) . "." . substr($varnpwp, 8, 1) . "-" . substr($varnpwp, 9, 3) . "." . substr($varnpwp, 12, 3);
        return $varresult;
    }

    function FormatTDP($varnoijin) {
        $varresult = '';
        $varresult = substr($varnoijin, 0, 2) . "." . substr($varnoijin, 2, 2) . "." . substr($varnoijin, 4, 1) . "." . substr($varnoijin, 5, 2) . "." . substr($varnoijin, 7, 5);
        return $varresult;
    }

    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */

    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */

    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 3.7) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);
        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);
        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
            $this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }

    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */

    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */

    function data($arrdata, $margin) {
        $this->NewIkd();
        $dflength = 174;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarstaticpendaftaran = 54;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        }
        $x = $kiri;
        $this->tulis($x, '+0', 'Nomor', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 20, '+0', ':', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 22, '+0', $arrdata['row']['no_izin'], 'L', $dflength, '', 'Arial', '', 10);

        $this->tulis($x + 140, '+0', 'Jakarta, ' . $arrdata['row']['izin'], 'L', $dflength, '', 'Arial', '', 10);

        $this->tulis($x, '+5', 'Lampiran', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 20, '+0', ':', 'L', $dflength, '', 'Arial', '', 10);

        $this->tulis($x, '+5', 'Perihal', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 20, '+0', ':', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 22, '+0', 'Persetujuan Penyelenggaraan Pameran Dagang', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 22, '+4', 'Konvensi dan/atau Seminar Dagang Internasional', 'L', $dflength, '', 'Arial', '', 10);



        $this->tulis($x, '+20', 'Kepada Yth.', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x, '+5', 'Sdr. Direktur', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x, '+5', $arrdata['row']['nm_perusahaan'], 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x, '+5', $arrdata['row']['almt_perusahaan'], 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x, '+5', "Kel. " . $arrdata['row']['kel_perusahaan'] . ", Kec. " . $arrdata['row']['kec_perusahaan'], 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x, '+5', $arrdata['row']['prop_perusahaan'] . " " . $arrdata['row']['kdpos'], 'L', $dflength, '', 'Arial', '', 10);

        $this->tulis($x + 1, '+10', '   Sehubungan dengan pengajuan permohonan Saudara No. '.$arrdata['row']['no_aju'].'  tanggal ' . $arrdata['row']['tg_aju'] . ' perihal tersebut diatas, maka berdasarkan Keputusan Menteri Perindustrian dan Perdagangan Nomor 199/MPP/Kep/6/2001 tentang Persetujuan Penyelenggaraan Pameran Dagang, Konvensi dan/atau Seminar Dagang, dengan ini kami memberikan persetujuan untuk menyelenggarakan Pameran Dagang Internasional : ', 'J', $dflength, 'M', 'Arial', '', 10, 2);

        $this->tulis($x, '+10', 'Judul/tema', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 22, '+0', ':', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 24, '+0', '"' . $arrdata['row']['tema_pameran'] . '"', 'L', $dflength - 20, 'M', 'Arial', 'B', 10);

        $this->tulis($x, '+1', 'Waktu', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 22, '+0', ':', 'L', $dflength, '', 'Arial', '', 10);
        if (substr($arrdata['row']['awal_pameran'], 5,2) != substr($arrdata['row']['akhir_pameran'], 5,2)) {
            $month = explode(substr($arrdata['row']['tgl_awal'], -5), $arrdata['row']['tgl_awal']);
            $this->tulis($x + 24, '+0', $month[0]. " - " . $arrdata['row']['tgl_akhir'], 'L', $dflength, '', 'Arial', '', 10);
        }else{
            $this->tulis($x + 24, '+0', substr($arrdata['row']['tgl_awal'], 0, 2). " - " . $arrdata['row']['tgl_akhir'], 'L', $dflength, '', 'Arial', '', 10);
        }

        $this->tulis($x, '+5', 'Lokasi', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 22, '+0', ':', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 24, '+0', $arrdata['row']['lokasi_pameran'] .',', 'L', $dflength - 90, 'M', 'Arial', '', 10);
        $this->tulis($x + 24, '+0', $arrdata['row']['prop_pameran'], 'L', $dflength - 90, 'M', 'Arial', '', 10);

        $this->tulis($x, '+10', 'Dengan Ketentuan :', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 22, '+0', ':', 'L', $dflength, '', 'Arial', '', 10);

        if ($arrdata['row']['status_dok'] == '02') {
            $this->pdf->SetTextColor(112, 121, 125);
            $this->pdf->SetFont('Arial', 'B', 60);
            $this->RotatedText(110, 260, "DICABUT", 0);   
            $this->pdf->SetTextColor(000, 000, 000);
        }
        
        $this->tulis($x, '+5', '1.', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 5, '+0', 'Telah memenuhi semua persyaratan yang ditetapkan oleh instansi terkait.', 'J', $dflength, 'M', 'Arial', '', 10, 2);

        $this->tulis($x, '+2', '2.', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 5, '+0', 'Penyelenggara wajib :', 'J', $dflength, 'M', 'Arial', '', 10, 2);

        $this->tulis($x + 5, '+1', 'a.', 'L', $dflength-15, '', 'Arial', '', 10);
        $this->tulis($x + 10, '+0', 'Menaati perundang-undangan yang berlaku.', 'J', $dflength-15, 'M', 'Arial', '', 10, 2);
        $this->tulis($x + 5, '+1', 'b.', 'L', $dflength-15, '', 'Arial', '', 10);
        $this->tulis($x + 10, '+0', 'Mereekspor barang pameran dari luar negeri sesuai dengan ketentuan yang berlaku atau melakukan kewajiban kepabeanan apabila barang (yang masuk dengan fasilitas bea masuk) terjual pada waktu pameran.', 'J', $dflength-15, 'M', 'Arial', '', 10, 2);
        $this->tulis($x + 5, '+1', 'c.', 'L', $dflength-15, '', 'Arial', '', 10);
        $this->tulis($x + 10, '+0', 'Menyampaikan laporan hasil penyelenggaraan pameran kepada kami selambat-lambatnya 14 (empat belas) hari kerja setelah pelaksanaan pameran.', 'J', $dflength-15, 'M', 'Arial', '', 10, 2);
        $this->tulis($x + 5, '+1', 'd.', 'L', $dflength-15, '', 'Arial', '', 10);
        $this->tulis($x + 10, '+0', 'Menyampaikan laporan apabila terjadi perubahan atas judul/tema dan / atau waktu/jadwal serta tempat penyelenggaraan setelah surat persetujuan diterbitkan, disertai alasan perubahan dimaksud kepada kami selambat-lambatnya 10 (sepuluh) hari kerja sebelum pelaksanaan.', 'J', $dflength-15, 'M', 'Arial', '', 10, 2);
        $this->tulis($x + 5, '+1', 'e.', 'L', $dflength-15, '', 'Arial', '', 10);
        $this->tulis($x + 10, '+0', 'Menyampaikan laporan apabila pameran tidak dapat dilaksanakan kepada kami selambat-lambatnya 14 (empat belas) hari kerja sejak tanggal yang harus dilaksanakan.', 'J', $dflength-15, 'M', 'Arial', '', 10, 2);
          $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 75), ($this->pdf->GetY()+10), 20);
        $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
     
        $this->pdf->AddPage();
        $this->tulis($x + 120, '+5', 'Nomor', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 135, '+0', ':', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 138, '+0', $arrdata['row']['no_izin'], 'L', $dflength, '', 'Arial', '', 10);

        $this->tulis($x + 120, '+5', 'Tanggal', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 135, '+0', ':', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 138, '+0', $arrdata['row']['izin'], 'L', $dflength, '', 'Arial', '', 10);
        $this->pdf->Ln(10);
        // $this->tulis($x + 10, '+5', 'f.', 'L', $dflength-15, '', 'Arial', '', 10);
        // $this->tulis($x + 14, '+0', 'Apabila penyelenggaraan melanggar ketentuan sebagaimana dimaksud pasal 14 Keputusan Menteri Perindustrian dan Perdagangan Nomor 199/MPP/Kep/6/2011 atau Penyelenggara Pameran Dagang, Konvensi dan/atau Seminar Dagang menyimpang dari maksud dan tujuan penyelenggaraan semula, penyelenggara yang bersangkutan dikenakan sanksi tidak diberikan Surat Persetujuan Penyelenggara Pameran Dagang, Konvensi dan atau seminar dagang internasional berikutnya.', 'J', $dflength-15, 'M', 'Arial', '', 10, 2);
        $this->tulis($x, '+1', '3.', 'L', $dflength-15, '', 'Arial', '', 10);
        $this->tulis($x + 5, '+0', 'Peserta/orang asing hanya dapat melakukan kegiatan dibidang pameran dagang (sesuai Visa yang diterbitkan Ditjen Imigrasi), dan menyampaikan laporan keberadaan peserta dari luar negeri kepada Ditjen Imigrasi, Kementerian Hukum dan Hak Asasi Manusia.', 'J', $dflength-5, 'M', 'Arial', '', 10, 2);
        $this->tulis($x, '+2', '4.', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 5, '+0', 'Pelanggaran terhadap angka 1 dan 2 atau terjadi penyimpangan dari maksud dari tujuan penyelenggara semula, penyelenggara yang bersangkutan dikenakan sanksi tidak akan diberikan Surat Persetujuan Pameran Dagang, Konvensi dan atau Seminar Dagang Internasional yang berikutnya dan dikenakan sanksi sesuai peraturan perundang-undangan oleh instansi yang berwenang.', 'J', $dflength-5, 'M', 'Arial', '', 10, 2);
        $this->tulis($x, '+2', '5.', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 5, '+0', 'Penyelenggara diminta ikut berperan aktif mensosialisasikan Program Peningkatan Penggunaan Produksi Dalam Negeri dalam penyelenggaraan pemeran melalui poster/spanduk, umbul-umbul, dan leaflet/brosur.', 'J', $dflength-5, 'M', 'Arial', '', 10, 2);

        $this->tulis($x + 10, '+10', 'Demikian disampaikan untuk dilaksanakan dengan sebaik-baiknya.', 'L', $dflength, '', 'Arial', '', 10);

        $this->pdf->Ln(10);
        // $this->tulis($x + 115, '+', $arrdata['row']['jabatan_ttd'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        // //$this->tulis($x + 115, '+4', 'Bina Usaha Perdagangan', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);

        // $this->pdf->Ln(10);
        // $this->tulis($x + 140, '+4', 'TTD', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 20, 0, 20);

        // $this->pdf->Ln(10);
        // $this->tulis($x + 115, '+4', $arrdata['row']['nama_ttd'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);

        $this->tulis($x + 75, '+7', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);

        if ($arrdata['row']['jabatan_ttd'] == '') {
                $this->tulis($x + 75, '+1', $arrdata['ttd']['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }else{
            $this->tulis($x + 75, '+1', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }
        
        $this->tulis($x + 75, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 15, 0, 15);

        if ($arrdata['row']['nama_ttd'] == '') {
            $this->tulis($x + 75, '+15', $arrdata['ttd']['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }else{
            $this->tulis($x + 75, '+15', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }

        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 80), ($this->pdf->GetY() - 16), 20); #Coba diganti ganti aja 30 dan 15 nya.
        // if($arrdata['urlFoto']['url'] != ""){
        // 	$this->tulis($x+$lebarnomor+5,$yttd+10,$arrdata['urlFoto']['url'],'', '','I', $this->huruf, '', $this->ukuranhuruf);
        // }

        $this->pdf->Ln(20);
        if (count($arrdata['tembusan']) > 0) {
            $this->tulis($x, '+', 'Tembusan', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'U', 10);
            for ($i = 0; $i < count($arrdata); $i++) {
                $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'] . '.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf);
            }
        }

        //$this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        /*

          #Tembusan
          $this->tulis($x, '+10','','L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          for ($i=0; $i < count($arrdata['tembusan']) ; $i++) {
          $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'].'.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf);
          }
          }

          /*
          $this->tulis($x, '+8', 'TANDA PENDAFTARAN', 'C', $dflength, '', 'Arial', 'B', 20);
          $this->tulis($x, '+8', 'PETUNJUK PENGGUNAAN(MANUAL) DAN KARTU JAMINAN/GARANSI PURNA JUAL' , 'C', $dflength, '', 'Arial', 'B', 12);
          $this->tulis($x, '+8', 'DALAM BAHASA INDONESIA BAGI PRODUK TELEMATIKA DAN ELEKTRONIKA', 'C', $dflength, '', 'Arial', 'B', 12);
          $this->tulis($x, '+8', 'PRODUKSI DALAM NEGERI', 'C', $dflength, '', 'Arial', 'B', 12);
          $this->pdf->Ln(8);

          $this->tulis(($x + 18), 0, 'NOMOR    :  P.32232323232323232323', 'C', $dflength - 30, '', 'Arial', 'B', 10,1,8);
          $this->pdf->Ln(13);

          $this->tulis($x + 55, '-5', 'DIBERIKAN KEPADA :' , '', 70, '', 'Arial', '', 10,0,13);
          $this->pdf->Ln(5);
          #nama perusahaan
          $this->tulis($x + 1, 0, '1.	NAMA PERUSAHAAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nm_perusahaan']) , '', 70, '', 'Arial', 'B', 10,0,13);

          #alamat
          $this->tulis($x + 1, '+5', '    ALAMAT ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', ucfirst($arrdata['row']['almt_perusahaan']) , '', 70, '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '+4', 'Kel. '.$arrdata['row']['kel_perusahaan'].', Kec. '.$arrdata['row']['kec_perusahaan'].', '.$arrdata['row']['prop_perusahaan'] , '', 70, '', 'Arial', '', 10,0,13);


          #tlp/fax
          $this->tulis($x + 1, '+5', '    NO. TELP / FAX ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', $arrdata['row']['telp'].' / '.$arrdata['row']['fax'] , '', 70, '', 'Arial', '', 10,0,13);

          $this->pdf->Ln(8);
          #nama pj
          $this->tulis($x + 1, '+', '2.	NAMA PENANGGUNG JAWAB ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nama_pj']) , '', 70, '', 'Arial', 'B', 10,0,13);
          #jabatan
          $this->tulis($x + 1, '+4', '    JABATAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', $arrdata['row']['jabatan_pj'] , '', 70, '', 'Arial', '', 10,0,13);


          $this->pdf->Ln(10);
          $this->tulis($x + 1, '+5', 'Telah mendaftarkan Petunjuk Penggunaan(Manual) dan Kartu Jaminan/Garansi Purna Jual dalam Bahasa Indonesia, sebagai berikut :', 'L', $dflength, 'M', 'Arial', '', 10,0);
          $this->tulis($x, '+5', ' JENIS PRODUK ', 'C', 85, '', 'Arial', 'B', 10,1,10);
          $this->tulis($x + 85, 0, ' MERK ', 'C', 30, '', 'Arial', 'B', 10,1,10);
          $this->tulis($x + 115, 0, ' TIPE MODEL ', 'C', 70, '', 'Arial', 'B', 10,1,10);



          $this->pdf->Ln(15);
          #footer
          #terbit
          $this->tulis($x + 105, '+', 'Diterbitkan di ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
          $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 137, '0', 'J A K A R T A' , '', 70, '', 'Arial', 'B', 10,0,13);
          #tanggal
          $this->tulis($x + 105, '+4', 'pada tanggal ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
          $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 137, '0', '   OKTOBER 2015' , '', 70, '', 'Arial', 'B', 10,0,13);

          $this->pdf->Ln(10);
          $this->tulis($x + 75, '+', 'a.n. MENTRI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
          $this->tulis($x + 75, '+4', 'Direktur Bina Usaha Perdagangan', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);

          $this->pdf->Ln(20);
          $this->tulis($x + 55, '+', 'Fetnayeti', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);


          if(count($arrdata['tembusan']) > 0){
          #Tembusan
          $this->tulis($x, '+10','','L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          for ($i=0; $i < count($arrdata['tembusan']) ; $i++) {
          $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'].'.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf);
          }
          }

         */
    }

    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */

    function generate($arrdata) {
        //$this->pdf->Output('./pdf/'.date('YmdHis').'.pdf','F');
        if ($arrdata['cetak'] != '') {
            $namafile = $arrdata['namafile'];
            $dir = $arrdata['dir'];
            if (file_exists($dir) && is_dir($dir)) {
                $path = 'upL04d5/document/DOKPMRN/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
            } else {
                if (mkdir($dir, 0777, true)) {
                    if (chmod($dir, 0777)) {
                        $path = 'upL04d5/document/DOKPMRN/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile ;
                    }
                }
            }
            $this->pdf->Output($path, 'F');
        } else {
            $this->pdf->Output();
        }
    }

    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}

?>