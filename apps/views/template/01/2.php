<?php

ob_clean();
ob_start();
error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $def_margin = array(15, 50, 15, 15);

    function pdf($orientasi, $ukuran, $kertas, $margins, $data) {
        $this->ukuranhuruf = 12;
        $this->ukuranhurufdetil = 10;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->qrcode = new QRcode();
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->pdf->footer = 1;
        $lebarnomor = 5;
        if (is_array($margins) and count($margins) == 4) {
            $this->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
        } else {

            $this->pdf->SetMargins($this->def_margin[0], $this->def_margin[1], $this->def_margin[2], $this->def_margin[3]);
        }
        $this->data($data, $margins);
    }

    function Footer($arrdata) {
        if ($this->pdf->footer == 1) {
            $this->pdf->SetY(-35);
            $this->pdf->SetFont('Arial', '', 9);
            /* $Yget2 = -60;
              $this->pdf->SetY($Yget2); */
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
        }
    }

    function Header($arrdata) {
        //$this->pdf->SetTextColor(244,244,244);
        $this->pdf->SetTextColor(177, 177, 172);
        $this->pdf->SetFont('Arial', 'B', 60);
        $this->RotatedText(15, 140, $arrdata['row']['no_izin'], 0);
    }

    function RotatedText($x, $y, $txt, $angle) {
        //Text rotated around its origin
        $this->pdf->Rotate($angle, $x, $y);
        $this->pdf->Text($x, $y, $txt);
        $this->pdf->Rotate(0);
    }

    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */

    function NewIkd($arrdata) {
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Header($arrdata);
        $this->pdf->SetTextColor(0);
        $this->Footer($arrdata);
        $this->pdf->SetY($Yget);
    }

    function NewIkdLandscape() {
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }

    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }

    function FormatNPWP($varnpwp) {
        $varresult = '';
        $varresult = substr($varnpwp, 0, 2) . "." . substr($varnpwp, 2, 3) . "." . substr($varnpwp, 5, 3) . "." . substr($varnpwp, 8, 1) . "-" . substr($varnpwp, 9, 3) . "." . substr($varnpwp, 12, 3);
        return $varresult;
    }

    function FormatTDP($varnoijin) {
        $varresult = '';
        $varresult = substr($varnoijin, 0, 2) . "." . substr($varnoijin, 2, 2) . "." . substr($varnoijin, 4, 1) . "." . substr($varnoijin, 5, 2) . "." . substr($varnoijin, 7, 5);
        return $varresult;
    }

    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */

    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */

    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 4) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);
        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);
        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
            $this->pdf->MultiCell($lebar, $tinggi, $teks, $garis, $align, 0);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }

    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */

    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */

    function data($arrdata, $margin) {
        $this->NewIkd($arrdata);
        $dflength = 175;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarstaticpendaftaran = 55;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        }
        $x = $kiri;
        if ($arrdata['row']['status'] != '1000') {
                $this->pdf->SetTextColor(219, 219, 219);
                $this->pdf->SetFont('Arial', 'B', 60);
                $this->RotatedText(50, 200, "D    R    A    F    T", 35); 
                $this->pdf->SetTextColor(000, 000, 000);
            }
        $this->tulis($x, 0, 'SURAT IZIN USAHA PERUSAHAAN', 'C', $dflength, '', 'Arial', 'B', $this->ukuranhuruf);
        $this->tulis($x, '+6', 'PERANTARA PERDAGANGAN PROPERTI', 'C', $dflength, '', 'Arial', 'B', $this->ukuranhuruf);
        $this->tulis($x, '+6', '(SIU-P4)', 'C', $dflength, '', 'Arial', 'B', $this->ukuranhuruf);
        $this->pdf->Ln(8);

        $this->tulis($x, '+6', $arrdata['row']['tipe_perusahaan'], 'C', $dflength, '', 'Arial', 'B', $this->ukuranhuruf);
        //$this->pdf->SetY($YNext);
        $this->pdf->SetY($this->pdf->GetY() + 6);
        $yAwal = $this->pdf->GetY();
        $YNow = $this->pdf->GetY();



        $this->pdf->Line($x + ($dflength - $lebarstaticpendaftaran), $YNow, $x + ($dflength - $lebarstaticpendaftaran), $YNow + 20);
        $this->pdf->Line($x + ($dflength - $lebarstaticpendaftaran) + ($lebarstaticpendaftaran / 5), $YNow, $x + ($dflength - $lebarstaticpendaftaran) + ($lebarstaticpendaftaran / 5), $YNow + 6.1);
        $this->pdf->Line($x + ($dflength - $lebarstaticpendaftaran) + (($lebarstaticpendaftaran / 5) * 2), $YNow, $x + ($dflength - $lebarstaticpendaftaran) + (($lebarstaticpendaftaran / 5) * 2), $YNow + 6.1);
        $this->pdf->Line($x + ($dflength - $lebarstaticpendaftaran) + (($lebarstaticpendaftaran / 5) * 3), $YNow, $x + ($dflength - $lebarstaticpendaftaran) + (($lebarstaticpendaftaran / 5) * 3), $YNow + 6.1);
        $this->pdf->Line($x + ($dflength - $lebarstaticpendaftaran) + (($lebarstaticpendaftaran / 5) * 4), $YNow, $x + ($dflength - $lebarstaticpendaftaran) + (($lebarstaticpendaftaran / 5) * 4), $YNow + 6.1);

        #Nomor
        $this->tulis($x, '+6', 'NOMOR :', 'C', (($dflength - $lebarstaticpendaftaran) / 2), '', 'Arial', '', 9);
        $this->tulis($x, $YNow + 12, $arrdata['row']['no_izin'], 'C', (($dflength - $lebarstaticpendaftaran) / 2), '', 'Arial', 'B', 11);

        #Pendaftaran Ulang
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 2, 'Pendaftaran Ulang ke ', 'L', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2) + 55, '', ':', 'L', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', ($arrdata['row']['pembaharuan'] == 0 ? '-' : $arrdata['row']['pembaharuan']), '', 54, '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5 + (($lebarstaticpendaftaran / 5) * 1)), '', ($arrdata['row']['pembaharuan'] == 0 ? '-' : $arrdata['row']['pembaharuan']), '', 54, '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5 + (($lebarstaticpendaftaran / 5) * 2)), '', ($arrdata['row']['pembaharuan'] == 0 ? '-' : $arrdata['row']['pembaharuan']), '', 54, '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5 + (($lebarstaticpendaftaran / 5) * 3)), '', ($arrdata['row']['pembaharuan'] == 0 ? '-' : $arrdata['row']['pembaharuan']), '', 54, '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5 + (($lebarstaticpendaftaran / 5) * 4)), '', ($arrdata['row']['pembaharuan'] == 0 ? '-' : $arrdata['row']['pembaharuan']), '', 54, '', 'Arial', '', 9);
        $this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 6.3, $x + $dflength, $YNow + 6.3);
        $YNext = $this->pdf->GetY() + 6;

        #perubahan
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext, 'Perubahan ', 'L', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2) + 55, '', ': ', 'L', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', ($arrdata['row']['pembaharuan'] == 0 ? '-' : $arrdata['row']['pembaharuan']), '', 54, '', 'Arial', '', 9);
        $this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext + 5, $x + $dflength, $YNext + 5);
        $YNext = $this->pdf->GetY() + 6;

        #Penggantian
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext, 'Penggantian ', 'L', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2) + 55, '', ': ', 'L', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', ($arrdata['row']['pembaharuan'] == 0 ? '-' : $arrdata['row']['pembaharuan']), '', 54, '', 'Arial', '', 9);
        $this->pdf->Line($x, $YNext + 6, $x + $dflength, $YNext + 6);
        $yHeader = $this->pdf->GetY() + 6;
        $YNow = $this->pdf->GetY() + 8;

        #nama perusahaan
        $this->tulis($x, $YNow, ' Nama Perusahaan', 'L', ((($dflength - $lebarstaticpendaftaran) / 2) - 5), '', 'Arial', '', 9);
        $this->tulis($x + ((($dflength - $lebarstaticpendaftaran) / 2) - 5), $YNow, ':', 'C', 5, '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow, strtoupper($arrdata['row']['nm_perusahaan']), 'L', ($dflength - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);
        $yGaris = $this->pdf->GetY();

        $YNow = $this->pdf->GetY() + 8;
        $this->pdf->Line($x, $YNow - 2, $x + $dflength, $YNow - 2);

        #npwp
        $this->tulis($x, $YNow, ' NPWP', 'L', ((($dflength - $lebarstaticpendaftaran) / 2) - 5), '', 'Arial', '', 9);
        $this->tulis($x + ((($dflength - $lebarstaticpendaftaran) / 2) - 5), $YNow, ':', 'C', 5, '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow, $arrdata['row']['npwp'], 'L', ($dflength - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);

        $YNow = $this->pdf->GetY() + 8;
        $this->pdf->Line($x, $YNow - 2, $x + $dflength, $YNow - 2);

        #nama penanggung jawab
        $this->tulis(($x + 1), $YNow, 'Nama Pemilik/Pengurus/Penanggung Jawab', 'L', ((($dflength - $lebarstaticpendaftaran) / 2) - 5), 'M', 'Arial', '', 9);
        $this->tulis($x + ((($dflength - $lebarstaticpendaftaran) / 2) - 5), $YNow, ':', 'C', 5, '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow, strtoupper($arrdata['row']['nama_pj']), 'L', ($dflength - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);

        $YNow = $this->pdf->GetY() + 8;
        $this->pdf->Line($x, $YNow, $x + $dflength, $YNow);

        #Alamat Perusahaan
        $this->tulis($x, $YNow + 2, ' Alamat Perusahaan', 'L', ((($dflength - $lebarstaticpendaftaran) / 2) - 5), 'M', 'Arial', '', 9);
        $this->tulis($x + ((($dflength - $lebarstaticpendaftaran) / 2) - 5), $YNow + 2, ':', 'C', 5, '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 2, ucwords($arrdata['row']['almt_perusahaan']) . ', Kel. ' . ucwords($arrdata['row']['kel_perusahaan']) . ', Kec. ' . ucwords($arrdata['row']['kec_perusahaan']) . ', ' . ucwords($arrdata['row']['kab_perusahaan']) . ', Prop. ' . ucwords($arrdata['row']['prop_perusahaan']), 'L', ($dflength - (($dflength - $lebarstaticpendaftaran) / 2)), 'M', 'Arial', '', 9);

        $YNow = $this->pdf->GetY() + 4;
        $this->pdf->Line($x, $YNow - 2, $x + $dflength, $YNow - 2);

        #Nomor Telepon
        $this->tulis($x, $YNow, '  Nomor Telepon', 'L', ((($dflength - $lebarstaticpendaftaran) / 2) - 5), 'M', 'Arial', '', 9);
        $this->tulis($x + ((($dflength - $lebarstaticpendaftaran) / 2) - 5), $YNow, ':', 'C', 5, '', 'Arial', '', 9);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow, $arrdata['row']['telp'], 'L', ($dflength - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);
        $this->pdf->Line($x + ($dflength - $lebarstaticpendaftaran), $YNow - 2, $x + ($dflength - $lebarstaticpendaftaran), $YNow + 6.1);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), $YNow, 'Fax: ' . ($arrdata['row']['fax'] == 0 ? '-' : $arrdata['row']['fax']), '', $lebarstaticpendaftaran, '', 'Arial', '', 9);
        $yAkhir = $this->pdf->GetY() + 6;

        $ySelisih = $yAkhir - $yAwal;
        $this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $yAwal, $x + (($dflength - $lebarstaticpendaftaran) / 2), $yAkhir);
        $this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2) - 5, $yHeader, $x + (($dflength - $lebarstaticpendaftaran) / 2) - 5, $yAkhir);

        $this->pdf->Rect($x, $yAwal, $dflength, $ySelisih);
        $this->pdf->Ln(10);
        $yKetAwal = $this->pdf->GetY();
        $this->tulis($x + 1, '+2', 'IZIN INI BERLAKU UNTUK MELAKUKAN KEGIATAN PERANTARA PERDAGANGAN PROPERTI DI SELURUH WILAYAH REPUBLIK INDONESIA, SELAMA PERUSAHAAN MASIH MENJALANKAN USAHANYA, DAN WAJIB DIDAFTAR ULANG SETIAP 5 (LIMA) TAHUN SEKALI', 'J', ($dflength - 2), 'M', 'Arial', '', 9);
        $yKetAkhir = $this->pdf->GetY();
        $selisih = $yKetAkhir - $yKetAwal;
        $this->pdf->Rect($x, $yKetAwal, $dflength, $selisih + 1);

        #Area Signature
        $this->pdf->Ln(8);
        $panjang = strlen($arrdata['row']['penerbit']);
        if ($panjang > 40) {
            $mulai = 40;
            $minus = 5;
        } else {
            $mulai = 30;
            $minus = 0;
        }

        if ($arrdata['row']['status_dok'] == '02') {
            $this->pdf->SetTextColor(112, 121, 125);
            $this->pdf->SetFont('Arial', 'B', 60);
            $this->RotatedText(110, 260, "DICABUT", 0);   
            $this->pdf->SetTextColor(000, 000, 000);
        }
        
        $this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '', 'Diterbitkan di', '', 25, '', 'Arial', '', 10);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran)), '', ':', '', 5, '', 'Arial', '', 10);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', 'JAKARTA', '', 55, '', 'Arial', '', 10);

        $this->tulis($x + 1.5, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '', 'Pada Tanggal', '', 25, '', 'Arial', '', 10);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran)), '', ':', '', 5, '', 'Arial', '', 10);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', $arrdata['row']['tgl_izin'], '', 55, '', 'Arial', '', 10);

        $this->pdf->Ln(8);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai + 18), '', 'An. Menteri Perdagangan', '', 25, '', 'Arial', '', 10);

        if ($arrdata['row']['jabatan_ttd'] == '') {
            $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai + 25), '+5', $arrdata['ttd']['jabatan'], C, 25, '', 'Arial', '', 10);
        }else{
            $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai + 25), '+5', $arrdata['row']['jabatan_ttd'], C, 25, '', 'Arial', '', 10);
        }

        // $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '+5', 'Bina Usaha Perdagangan', '', 25, '', 'Arial', '', 10);
        $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai + 18), '+10', 'TTD', 'C', 40, '', 'Arial', '', 15);

        if ($arrdata['row']['nama_ttd'] == '') {
            $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai + 25), '+10', $arrdata['ttd']['nama'], C, 25, '', 'Arial', '', 10);
        }else{
            $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai + 25), '+10', $arrdata['row']['nama_ttd'], C, 25, '', 'Arial', '', 10);
        }
        #End Area Signature
        #qrcode
        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 60), ($this->pdf->GetY() - 25), 20); #Coba diganti ganti aja 30 dan 15 nya.

        $this->pdf->Ln(20);
        if (count($arrdata['tembusan']) > 0) {
            $this->tulis($x, '+', 'Tembusan', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'U', 8);
            for ($i = 0; $i < count($arrdata['tembusan']); $i++) {
                $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'] . '.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis($x + 5, 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf);
            }
        }

        $this->pdf->Image('upL04d5' . $arrdata['foto_pemilik']['url'], 17, $this->pdf->GetY() - 55, 28, 38);
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);


        $this->pdf->Addpage();
        $this->tulis($x, 40, 'KETENTUAN UMUM', 'L', $dflength - $this->def_margin[3], '', 'Arial', 'B', $this->ukuranhuruf + 2);
        $this->tulis($x  + 10, '+8', 'A. UMUM', 'L', $dflength - $this->def_margin[3], '', 'Arial', 'B', $this->ukuranhuruf + 2);
        $this->tulis($x, '+5', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);

        $this->tulis($x + 15, '+5', '1.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Pemilik SIU-P4 dapat melakukan kegiatan di bidang usaha perantara perdagangan properti di seluruh wilayah Indonesia selama perusahaan menjalankan usahanya.', 'J', ($dflength - $kanan), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x + 15, '+5', '2.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Perusahaan perantara perdagangan properti dalam menjalankan kegiatan usahanya berpedoman pada ketentuan dan tata cara yang berlaku serta peraturan perundang-undangan.', 'J', ($dflength - $kanan), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x  + 10, '+8', 'B. KEWAJIBAN', 'L', $dflength - $this->def_margin[3], '', 'Arial', 'B', $this->ukuranhuruf + 2);

        $this->tulis($x + 15, '+7', '1.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Menyampaikan laporan kegiatan perusahaan setiap 1 (satu) tahun sekali.', 'J', ($dflength - $kanan), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x + 15, '+5', '2.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Melaporkan perubahan data perusahaan yang memuat perubahan nama dan alamat perusahaan, pemilik/pengurus/penanggungjawab perusahaan, dan/atau tenaga ahli.', 'J', ($dflength - $kanan), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x + 15, '+5', '3.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Melaporkan secara tertulis apabila menutup atau mengakhiri kegiatan usahanya dengan mengembalikan SIU-P4 asli.', 'J', ($dflength - $kanan), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x + 15, '+5', '4.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Setiap tenaga ahli pada P4 yang belum memiliki sertifikat kompetensi yang diterbitkan oleh lembaga sertifikasi wajib memiliki sertifikat dimaksud paling lambat 6 (enam) bulan setelah tanggal penerbitan SIUP4.', 'J', ($dflength - $kanan), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x  + 10, '+8', 'C. SANKSI', 'L', $dflength - $this->def_margin[3], '', 'Arial', 'B', $this->ukuranhuruf + 2);

        $this->tulis($x + 15, '+5', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 15, 0, 'Pemilik SIU-P4 yang melakukan pelanggaran ketentuan ini dapat dikenakan sanksi administratif dan/atau sanksi pidana sesuai dengan peraturan perundang-undangan.', 'J', ($dflength - $kanan), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x  + 10, '+8', 'D. HIMBAUAN', 'L', $dflength - $this->def_margin[3], '', 'Arial', 'B', $this->ukuranhuruf + 2);

        $this->tulis($x + 15, '+5', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 15, 0, 'Pemilik SIU-P4 dihimbau agar menjadi Anggota Organisasi Perusahaan Perantara Perdagangan Properti.', 'J', ($dflength - $kanan), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor ) + 80, ($this->pdf->GetY() + 60), 15); #Coba diganti ganti aja 30 dan 15 nya.

        $this->tulis(20, 280, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);

    }

    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */

    function generate($arrdata) {
        if ($arrdata['cetak'] != '') {
            $namafile = $arrdata['namafile'];
            $dir = $arrdata['dir'];
            if (file_exists($dir) && is_dir($dir)) {
                $path = 'upL04d5/document/DOKSIUP4/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile ;
            } else {
                if (mkdir($dir, 0777, true)) {
                    if (chmod($dir, 0777)) {
                        $path = 'upL04d5/document/DOKSIUP4/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile ;
                    }
                }
            }
            $this->pdf->Output($path, 'F');
        } else {
            $this->pdf->Output();
        }
    }

    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}

?>