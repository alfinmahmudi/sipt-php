<?php

error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $def_margin = array(15, 50, 15, 15);

    function pdf($orientasi, $ukuran, $kertas, $margins, $data) {
        $this->ukuranhuruf = 12;
        $this->ukuranhurufdetil = 10;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->qrcode = new QRcode();
        $this->pdf->footer = 1;
        if (is_array($margins) and count($margins) == 4) {
            $this->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
        } else {

            $this->pdf->SetMargins($this->def_margin[0], $this->def_margin[1], $this->def_margin[2], $this->def_margin[3]);
        }
        $this->data($data, $margins);
    }

    function Footer() {
        if ($this->pdf->footer == 1) {
            #$this->pdf->SetY(-45);
            $this->pdf->SetFont('Arial', '', 9);
            $Yget2 = $this->pdf->GetY();
            $this->pdf->SetY($Yget2);
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
        }
    }

    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */

    function NewIkd() {
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function NewIkdLandscape() {
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }

    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }

    function FormatNPWP($varnpwp) {
        $varresult = '';
        $varresult = substr($varnpwp, 0, 2) . "." . substr($varnpwp, 2, 3) . "." . substr($varnpwp, 5, 3) . "." . substr($varnpwp, 8, 1) . "-" . substr($varnpwp, 9, 3) . "." . substr($varnpwp, 12, 3);
        return $varresult;
    }

    function FormatTDP($varnoijin) {
        $varresult = '';
        $varresult = substr($varnoijin, 0, 2) . "." . substr($varnoijin, 2, 2) . "." . substr($varnoijin, 4, 1) . "." . substr($varnoijin, 5, 2) . "." . substr($varnoijin, 7, 5);
        return $varresult;
    }

    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */

    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */

    function NbLines($w, $txt) {
        //Computes the number of lines a MultiCell of width w will take
        $cw = &$this->pdf->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->pdf->rMargin - $this->pdf->x;
        $wmax = ($w - 2 * $this->pdf->cMargin) * 1000 / $this->pdf->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l+=$cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }

    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 3.7) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);
        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);
        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
            $this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }

    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */

    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */

    function data($arrdata, $margin) {
        $this->NewIkd();
        $dflength = 174;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarstaticpendaftaran = 54;
        $lebarnomor = 8;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        }
        $x = $kiri;

        $this->tulis($x, '+8', 'SURAT TANDA PENDAFTARAN WARALABA', 'C', $dflength, '', 'Arial', 'B', 18);
        $this->tulis($x + 3, '+8', '(PENERIMA WARALABA LANJUTAN BERASAL DARI WARALABA ' . strtoupper($arrdata['row']['produk_wrlb']) . ')', 'C', $dflength, '', 'Arial', 'B', 13);
        $this->pdf->Ln(8);
        #NOMOR
        $this->tulis($x + 0.5, '+1', '  Nomor', 'L', 180, '', 'Arial', '', 10, 1, 10);
        $this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', $arrdata['row']['no_izin'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        #MASA BERLAKU
        $this->tulis($x + 0.5, '+7', '  Masa Berlaku', 'L', 180, '', 'Arial', '', 10, 1, 10);
        $this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', $arrdata['row']['tgl_ex'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        #NAMA PERUSAHAAN
        $this->tulis($x + 0.5, '-3', '  Nama Perusahaan', 'L', 180, '', 'Arial', '', 10, 0, 30);
        $this->tulis($x + 55, '+8', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', strtoupper($arrdata['row']['nm_perusahaan']), 'L', ($dflength - $lebarstaticpendaftaran), 'B', 'Arial', 'B', 10);

        $spasi_al = $this->NbLines(($dflength - $lebarstaticpendaftaran), trim($arrdata['row']['almt_perusahaan']));
        $panjang_al = 0;
        $nambah = 0;        
        if ($spasi_al > 2) {
            $panjang_al = $spasi_al * 3.7;
            $nambah = 1;
        }

        #alamat
        $this->tulis($x + 0.5, '-3', '  ', 'L', 180, '', 'Arial', '', 10, 1, 30 + $panjang_al + $nambah);
        $this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 0.5, '+4', '  Alamat', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', ucfirst(trim($arrdata['row']['almt_perusahaan'])), 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 10.5, '+3', ' Telepon', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', $arrdata['row']['telp'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 10.5, '+3', ' Fax', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', $arrdata['row']['fax'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 10.5, '+3', ' Email', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', $arrdata['row']['email'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);


        $this->tulis($x + 0.5, '-3', '  Nama Penanggung Jawab', 'L', 180, '', 'Arial', '', 10, 0, 30);
        $this->tulis($x + 55, '+8', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', strtoupper($arrdata['row']['nama_pj']), 'L', ($dflength - $lebarstaticpendaftaran), 'B', 'Arial', '', 10);

        #alamat
        $this->tulis($x + 0.5, '-3', '  ', 'L', 180, '', 'Arial', '', 10, 1, 15);
        $this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 0.5, '+3', '  Jabatan', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', ucfirst($arrdata['row']['jabatan_pj']), 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        if ($arrdata['row']['status_dok'] == '02') {
            $this->pdf->SetTextColor(112, 121, 125);
            $this->pdf->SetFont('Arial', 'B', 60);
            $this->RotatedText(110, 260, "DICABUT", 0);   
            $this->pdf->SetTextColor(000, 000, 000);
        }

        $this->tulis($x + 0.5, '-3', '  Barang / Jasa Objek', 'L', 180, '', 'Arial', '', 10, 0, 30);
        $this->tulis($x + 55, '+8', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', $arrdata['row']['jenis_dagang'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        #alamat
        $this->tulis($x + 0.5, '-3', '  ', 'L', 180, '', 'Arial', '', 10, 1, 19);
        $this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 0.5, '+3', '  Waralaba', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 1.5, '+3', ' Merek', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', $arrdata['row']['merk_dagang'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);



        $this->tulis($x + 0.5, '-3', '  Nama Perusahaan Pemberi', 'L', 180, '', 'Arial', '', 10, 0, 30);
        $this->tulis($x + 55, '+8', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', $arrdata['row']['nm_perusahaan_pw'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'B', 10);

        #alamat
        $this->tulis($x + 0.5, '-4', '  ', 'L', 180, '', 'Arial', '', 10, 1, 24);
        $this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 0.5, '+3', '  Waralaba Lanjutan', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 1.5, '+3', ' Alamat', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', ucwords(trim($arrdata['row']['alamat_pj'])), 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 1.5, '+3', ' Penanggung Jawab', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', $arrdata['row']['pj_pw'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);


        #alamat
        $tipe = explode(",", $arrdata['row']['tipe_pemasaran']);
        $spasi_dag = $this->NbLines(($dflength - $lebarstaticpendaftaran), $arrdata['dagang']);
        $spasi_neg = $this->NbLines(($dflength - $lebarstaticpendaftaran), $arrdata['negara']);
        $panjang_dag = 0;
        $panjang_neg = 0;
        $extend = 0;
        if ($tipe[0] == 1) {
                $panjang_dag = $spasi_dag * 3.7;
                $panjang_dag = $panjang_dag + 3.7;
                if ($spasi_dag > 1) {
                    $extend = 1;
                }
        }
        if ($tipe[1] == 1) {
                $panjang_neg = $spasi_neg * 3.7;
                $panjang_neg = $panjang_neg + 3.7 + 3.7;
                if ($spasi_dag > 1) {
                    $extend = 1;
                }
        }
         
        $this->tulis($x + 0.5, '+5', '  ', 'L', 180, '', 'Arial', '', 10, 1, 27.5 + $panjang_dag + $panjang_neg + $extend);
        $this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 0.5, '-12', '  Nomor dan Tanggal', 'L', 180, '', 'Arial', '', 10, 0, 30);
        $this->tulis($x + 55, '+8', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', $arrdata['suratPerjanjian']['nomor']." , ". $arrdata['suratPerjanjian']['tgl_dok'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        // $this->tulis($x + 60, '+5', $panjang_neg . '*' . $spasi_neg . ' -- ' .$panjang_dag . '*' . $spasi_dag, 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        $this->tulis($x + 0.5, '+3', '  Perjanjian', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 1.5, '+5', ' Wilayah Pemasaran', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        // $t = 0;
        // for ($d = 1; $d < count($arrdata['dagang']); $d++) {
        //     if ($t == 0) {
        //         $r = '+5';
        //     } else {
        //         $r = 0;
        //     }
        //     if ($d == (count($arrdata['dagang']) - 1)) {
        //         $e = "";
        //     } else {
        //         $e = ",";
        //     }
        //     $this->tulis($x + 60 + $t, $r, $arrdata['dagang'][$d]['nama'] . $e, 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        //     $t = $t + 38;
        // }
        if ($tipe[0] == 1) {
            // $this->pdf->Ln(10);            
            $this->tulis($x + 60, '+5', "Dalam Negeri : ", 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
            $this->tulis($x + 60, '+5', $arrdata['dagang'], 'L', ($dflength - $lebarstaticpendaftaran), 'M', 'Arial', '', 10);
        }
        if ($tipe[1] == 1) {
            $this->tulis($x + 60, '+2', "Luar Negeri : ", 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
            $this->tulis($x + 60, '+5', $arrdata['negara'], 'L', ($dflength - $lebarstaticpendaftaran), 'M', 'Arial', '', 10);
        }
        if ($tipe[0] != 1 && $tipe[1] != 1) {
            $this->pdf->Ln(10);
        }
        $this->tulis($x + 1.5, '+2', ' Jumlah Gerai Waralaba', 'L', 180, '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', $arrdata['row']['outlet_waralaba'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

        $this->tulis($x + 0.5, '+5', '', 'L', 180, '', 'Arial', '', 10, 1, 16);
        $this->tulis($x + 1.5, '+3', 'Penerima Waralaba Lanjutan wajib mengutamakan penggunaan barang dan /atau jasa hasil produksi', 'L', 180, 'M', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 1.5, '+2', 'dalam negeri sepanjang memenuhi standar mutu yang ditetapkan.', 'L', 180, 'M', 'Arial', '', 10, 0, 10);

        $this->pdf->Ln(3);
        $this->tulis($x + 75, '+', 'Jakarta, ' . $arrdata['row']['izin'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 10);
        $this->tulis($x + 75, '+5', 'Pejabat Penerbit STPW', 'C', ($dflength - $lebarstaticpendaftaran + 8), '', 'Arial', '', 10, 0, 10);

        $this->pdf->Ln(3);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 90)), '+5', 'a.n. MENTERI PERDAGANGAN', 'C', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf - 2);

        if ($arrdata['row']['jabatan_ttd'] == '') {
                $this->tulis($x + 80, '+4', $arrdata['ttd']['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }else{
            $this->tulis($x + 80, '+4', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }
        
        $this->tulis($x + 80, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 20, 0, 15);

        if ($arrdata['row']['nama_ttd'] == '') {
            $this->tulis($x + 80, '+15', $arrdata['ttd']['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }else{
            $this->tulis($x + 80, '+15', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }

        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 90)), '+5', $arrdata['row']['jabatan_ttd'], 'C', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf - 2);
        // // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 90)), '+5', 'Bina Usaha Perdagangan', 'C', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf - 2);
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 90)), '+15', 'TTD', 'C', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf + 5);
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 90)), '+10', $arrdata['row']['nama_ttd'], 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf - 2);


        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 70), ($this->pdf->GetY() - 35), 25); #Coba diganti ganti aja 30 dan 15 nya.
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
        /*
          $this->pdf->Ln(13);
          #Border Nomor Telepon


          //$this->pdf->Ln(5);

          #Border Atas


          //$this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext, $x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 11);
          //$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());

          /*
          $this->pdf->SetFont('Arial','',10);
          $this->pdf->SetFillColor(30,144,255);
          $this->pdf->MultiCell(0,8,"		NOMOR                                         :   //STP/9191919191911919",1,1,true);

          $this->pdf->SetFont('Arial','',10);
          $this->pdf->SetFillColor(30,144,255);
          $this->pdf->MultiCell(0,8,"		BERLAKU SAMPAI DENGAN       :   2020",1,1,true);
         */
        //$this->pdf->tulis($x + 0.5, 0, '  NOMOR ', 'C', 180, '', 'Arial', '', 10,1,10);
        //$this->pdf->tulis($x + 55, '-1', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
        //$this->tulis($x + 60, '0', 'FAX//1234567' , 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
        #Border Kegiatan Usaha
        //$this->pdf->Ln();
        //$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());

        /*
          $this->tulis($x + 0.5, '+11', '  BERLAKU SAMPAI DENGAN', 'L', 180, '', 'Arial', '', 10,1,10);
          $this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '+5', 'MARET 2020', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          //$this->pdf->Ln(8);
          //$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
          $this->pdf->Ln(10);


          $this->tulis($x + 55, '-5', 'DIBERIKAN KEPADA :' , '', 70, '', 'Arial', '', 10,0,13);
          $this->pdf->Ln(5);
          #nama perusahaan
          $this->tulis($x + 1, 0, '1.	NAMA PERUSAHAAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nm_perusahaan']) , '', 70, '', 'Arial', 'B', 10,0,13);

          #alamat
          $this->tulis($x + 1, '+5', '    ALAMAT ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', ucfirst($arrdata['row']['almt_perusahaan']) , '', 70, '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '+4', 'Kel. '.$arrdata['row']['kel_perusahaan'].', Kec. '.$arrdata['row']['kec_perusahaan'].', '.$arrdata['row']['prop_perusahaan'] , '', 70, '', 'Arial', '', 10,0,13);


          #tlp/fax
          $this->tulis($x + 1, '+5', '    NO. TELP / FAX ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', $arrdata['row']['telp'].' / '.$arrdata['row']['fax'] , '', 70, '', 'Arial', '', 10,0,13);

          $this->pdf->Ln(8);
          #nama pj
          $this->tulis($x + 1, '+', '2.	NAMA PENANGGUNG JAWAB ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nama_pj']) , '', 70, '', 'Arial', 'B', 10,0,13);
          #jabatan
          $this->tulis($x + 1, '+4', '    JABATAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', $arrdata['row']['jabatan_pj'] , '', 70, '', 'Arial', '', 10,0,13);


          $this->pdf->Ln(10);
          $this->tulis($x + 1, '+5', 'Telah mendaftarkan Petunjuk Penggunaan(Manual) dan Kartu Jaminan/Garansi Purna Jual dalam Bahasa Indonesia, sebagai berikut :', 'L', $dflength, 'M', 'Arial', '', 10,0);
          $this->tulis($x, '+5', ' JENIS PRODUK ', 'C', 80, '', 'Arial', 'B', 10,1,10);
          $this->tulis($x + 80, 0, ' MERK ', 'C', 30, '', 'Arial', 'B', 10,1,10);
          $this->tulis($x + 110, 0, ' TIPE MODEL ', 'C', 70, '', 'Arial', 'B', 10,1,10);
          /*
          for($i=0;$i<7;$i++){
          $this->tulis($x, '+10', $res['produk'], 'C', 80, '', 'Arial', 'B', 10,1,10);
          $this->tulis($x + 80, 0, $res['merk'], 'C', 30, '', 'Arial', 'B', 10,1,10);
          $this->tulis($x + 110, 0,  $res['tipe'], 'C', 70, '', 'Arial', 'B', 10,1,10);
          }

          if(count($arrdata['p']) != 0 || count($arrdata['p']) < 7){
          $i=0;
          foreach($arrdata['p'] as $res){
          $this->tulis($x, '+10', $res['produk'], 'C', 80, '', 'Arial', 'B', 10,1,10);
          $this->tulis($x + 80, 0, $res['merk'], 'C', 30, '', 'Arial', 'B', 10,1,10);
          $this->tulis($x + 110, 0,  $res['tipe'], 'C', 70, '', 'Arial', 'B', 10,1,10);
          $i++;
          }
          }else{
          $this->tulis($x + 80, 0, ' TERLAMPIR ', 'C', 30, '', 'Arial', 'B', 10,0,10);
          }

          $this->pdf->Ln(15);
          #footer
          #terbit
          $this->tulis($x + 105, '+', 'Diterbitkan di ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
          $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 137, '0', 'J A K A R T A' , '', 70, '', 'Arial', 'B', 10,0,13);
          #tanggal
          $this->tulis($x + 105, '+4', 'pada tanggal ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
          $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 137, '0', '   OKTOBER 2015' , '', 70, '', 'Arial', 'B', 10,0,13);

          $this->pdf->Ln(10);
          $this->tulis($x + 75, '+', 'a.n. MENTRI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
          $this->tulis($x + 75, '+4', 'Direktur Bina Usaha Perdagangan', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);

          $this->pdf->Ln(20);
          $this->tulis($x + 55, '+', 'Fetnayeti', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);


          if(count($arrdata['tembusan']) > 0){
          #Tembusan
          $this->tulis($x, '+10','','L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          for ($i=0; $i < count($arrdata['tembusan']) ; $i++) {
          $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'].'.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf);
          }
          }


          /*

          $this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext, $x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 11);
          $this->tulis($x, 0, 'NOMOR TDP', 'C', (($dflength - $lebarstaticpendaftaran) / 2), '', 'Arial', '', 9);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), 0, 'BERLAKU S/D TANGGAL', 'C', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);


          $this->pdf->Line($x + ($dflength - $lebarstaticpendaftaran), $YNext, $x + ($dflength - $lebarstaticpendaftaran), $YNow + 11);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 1.5), '', 'PENDAFTARAN  : '.$arrdata['row']['pendaftaran'], '', 54, '', 'Arial', '', 9);
          $this->pdf->Ln(6);

          #Nomor TDP
          $this->tulis($x, 0, $this->FormatTDP($arrdata['row']['no_ijin']), 'C', (($dflength - $lebarstaticpendaftaran) / 2), '', 'Arial', 'B', 11);

          #Tanggal TDP
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), 0, $this->iif($arrdata['row']['tgl_akhir'], strtoupper($this->convert_date($arrdata['row']['tgl_akhir']))), 'C', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', 'B', 11);
          $this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran)), $YNext + 6.3, $x + $dflength, $YNext + 6.3);

          #PEMBAHARUAN
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 1.5), '', 'PEMBAHARUAN : '.($arrdata['row']['pembaharuan'] == 0 ? '-' : $arrdata['row']['pembaharuan']), '', 54, '', 'Arial', '', 9);


          $YSec = $this->pdf->GetY();
          $this->pdf->SetY($YSec);
          $YNow = $this->pdf->GetY();
          $this->pdf->Line($x, $YNext, ($x + $dflength), $YNext);
          $this->pdf->Line($x, $YNext, $x, $YNow + 5);
          $this->pdf->Line(($x + $dflength), $YNext, ($x + $dflength), $YNow + 5);
          $this->pdf->Line($x, $YNow + 5, ($x + $dflength), $YNow + 5);
          $this->pdf->Ln(10);

          #Border Atas

          $ystartpoint = $this->pdf->GetY();

          $this->pdf->Line($x, $ystartpoint, ($x + $dflength), $ystartpoint);
          $this->tulis($x + 1.5, '+3', 'NAMA PERUSAHAAN : ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 9);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '', 'STATUS : ', '', 41.5, '', 'Arial', '', 9);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '+6', strtoupper($arrdata['row']['ur_status_perusahaan']), '', 41.5, '', 'Arial', '', 10);
          $this->pdf->newFlowingBlock(($dflength - $lebarstaticpendaftaran), 5, 'W', 'J', 0, false);
          $this->tulis($x + 1.5, '', strtoupper($arrdata['row']['nama']), 'L', 10, 'W', 'Arial', 'B',13);
          $this->pdf->finishFlowingBlock();
          $this->pdf->Ln(6);
          $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
          $this->tulis($x + 1.5, 0, 'PENANGGUNG JAWAB / PENGURUS : '.strtoupper($arrdata['row']['nama_pemilik']), 'L', ($dflength - 1.5), '', 'Arial', '', 10, 0, 13);

          #Border Alamat
          $this->pdf->Ln();
          $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
          $this->tulis($x + 1.5, '+3', 'ALAMAT : ', 'L', 25, '', 'Arial', '', 9);
          $rowalamat = strtoupper($arrdata['row']['alamat']);
          $kelurahan = 'KEL';
          $pos=strpos($rowalamat,$kelurahan);
          if($pos !== false){
          $alamat1=explode('KEL.',$alamat);
          $this->tulis($x + 20, 0, $alamat1[0], 'L', 5, '', 'Arial', '', 10);
          $this->tulis($x + 25, 0, $alamat1[1], 'L', ($dflength - 20), 'M', 'Arial', '', 10);
          }else{
          $this->tulis($x + 20, 0, strtoupper($arrdata['row']['alamat']), 'L', ($dflength - 20), 'M', 'Arial', '',10);
          $this->tulis($x + 20, '+1', 'KEL. '. strtoupper($arrdata['row']['ur_kel']).' KEC. '.strtoupper($arrdata['row']['ur_kec']), 'L', ($dflength - 20), 'M', 'Arial', '', 10);
          $this->tulis($x + 20, '+1', strtoupper($arrdata['row']['urai_kota']).' '.$arrdata['row']['ur_pos'], 'L', ($dflength - 20), '', 'Arial', '',10);
          $this->pdf->Ln(2);
          }

          #Border NPWP
          $this->pdf->Ln();
          $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
          $this->tulis($x + 1.5, 0, 'NPWP   :   '.$this->FormatNPWP($arrdata['row']['npwp']), 'L', ($dflength - 1.5), '', 'Arial', '', 10, 0, 13);

          #Border Nomor Telepon
          $this->pdf->Ln();
          $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
          $this->tulis($x + 1.5, 0, 'NOMOR TELEPON :         '.$arrdata['row']['telepon'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - 14), '', 'FAX :          '.$arrdata['row']['fax'], '', 70, '', 'Arial', '', 10,0,13);


          #Border Kegiatan Usaha
          $this->pdf->Ln();
          $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());

          $this->tulis($x + 1.5, '+3', 'KEGIATAN USAHA POKOK : ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '', 'KBLI : ', 'C', 41.5, '', 'Arial', '', 10);

          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '+5', $arrdata['row']['kbli'], 'C', 41.5, '', 'Arial', '', 10);
          $this->tulis($x + 10, '', str_replace(chr(10),' ',strtoupper($arrdata['row']['pokok'])), 'J', ($dflength - $lebarstaticpendaftaran), 'M', 'Arial', '', 10);

          #Border Bawah Pisan
          $this->pdf->Ln();
          $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
          $this->pdf->Line($x, $ystartpoint, $x, $this->pdf->GetY());
          $this->pdf->Line(($x + $dflength), $ystartpoint, ($x + $dflength), $this->pdf->GetY());
          #End Border Bawah Pisan

          #Area Signature
          $this->pdf->Ln(8);
          $panjang = strlen($arrdata['row']['penerbit']);
          if ($panjang > 40){
          $mulai = 40;
          $minus = 5;
          }else{
          $mulai = 30;
          $minus = 0;
          }

          $this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '', 'Dikeluarkan di', '', 25, '', 'Arial', '', 10);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran)), '', ':', '', 5, '', 'Arial', '', 10);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', strtoupper($arrdata['row']['urai_kota']), '', 55, '', 'Arial', '', 10);

          $this->tulis($x + 1.5, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '', 'Pada Tanggal', '', 25, '', 'Arial', '', 10);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran)), '', ':', '', 5, '', 'Arial', '', 10);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', $this->iif($arrdata['row']['tgl_ijin'], strtoupper($this->convert_date($arrdata['row']['tgl_ijin']))), '', 55, '', 'Arial', '', 10);

          $this->pdf->Ln(8);
          $this->tulis($x + 1.5, '', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength / 2) - ($panjang/2)), '', 'KEPALA '.strtoupper($arrdata['row']['penerbit']), 'C', (($dflength / 2) + ($panjang/2)), 'M', 'Arial', '', 10);

          $this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength / 2) - 10-$minus), '', 'SELAKU', 'C', (($dflength / 2) + 10), 'M', 'Arial', '', 10);

          $this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength / 2) - 10-$minus), '', 'KEPALA KANTOR PENDAFTARAN PERUSAHAAN', 'C', (($dflength / 2) + 10), 'M', 'Arial', '', 10);

          $this->pdf->Ln(34);
          $this->tulis($x + 1.5, '', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength / 2) - 10-$minus), '', $arrdata['row']['nama_ttd'], 'C', (($dflength / 2) + 10), '', 'Arial', 'U', 10);

          $this->tulis($x + 1.5, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength / 2) - 10-$minus), '', 'NIP. '. $arrdata['row']['nip_ttd'], 'C', (($dflength / 2) + 10), '', 'Arial', '', 10);
          #End Area Signature
         */
    }

    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */

    function generate($arrdata) {
        if ($arrdata['cetak'] != '') {
            $namafile = $arrdata['namafile'];
            $dir = $arrdata['dir'];
            if (file_exists($dir) && is_dir($dir)) {
                $path = 'upL04d5/document/DOKWRPNJ/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
            } else {
                if (mkdir($dir, 0777, true)) {
                    if (chmod($dir, 0777)) {
                        $path = 'upL04d5/document/DOKWRPNJ/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
                    }
                }
            }
            $this->pdf->Output($path, 'F');
        } else {
            $this->pdf->Output();
        }
    }

    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}

?>