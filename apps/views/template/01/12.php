<?php

error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf extends NEWFPDF {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $def_margin = array(15, 50, 15, 15);
    var $qrCodeString;

    function Footer() {

        if ($this->showbarang == 1) {
            $this->SetXY(15, 111);
            $this->SetFont('Arial', '', 12);
            $this->set_lampiran_agen();
        }
    }

    function Header() {
        // Logo
        //$this->Image('logo.png',10,6,30);
        // Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        // Move to the right
        $this->Cell(80);
        // Title
        // $this->Cell(30, 10, 'Title', 1, 0, 'C');
        // Line break
        $this->Ln(20);
    }

    function set_lampiran_agen() {

        $data = $this->footer_data;
        $dflength = 174;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarstaticpendaftaran = 54;
        $lebarnomor = 8;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        }
        $x = $kiri;
        // 

        $this->tulis(20, 53, 'LAMPIRAN SURAT TANDA PENDAFTARAN SEBAGAI', 'C', $dflength, '', 'Arial', 'B', 18);
        $this->tulis($x, '+8', strtoupper($data['jenis_agen']), 'C', $dflength, '', 'Arial', 'B', 18);
        if ($data['jenis_produk'] == '02') {
            $this->tulis($x, '+8', 'JASA PERUSAHAAN ' . strtoupper($data['produksi']), 'C', $dflength, '', 'Arial', 'B', 18);
        } elseif ($data['jenis_produk'] == '03') {
            $this->tulis($x, '+8', 'PUPUK PRODUKSI ' . strtoupper($data['produksi']), 'C', $dflength, '', 'Arial', 'B', 18);
        } else {
            $this->tulis($x, '+8', 'BARANG PRODUKSI ' . strtoupper($data['produksi']), 'C', $dflength, '', 'Arial', 'B', 18);
        }

        $this->Ln(10);
        $this->tulis($x + 1, 0, '  NAMA PERUSAHAAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '0', strtoupper($data['nm_perusahaan']), '', 70, '', 'Arial', 'B', 10, 0, 13);

        $this->tulis($x + 1, '+5', '  N O M O R ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '0', strtoupper($data['no_izin']), '', 70, '', 'Arial', 'B', 10, 0, 13);

        $this->tulis($x + 1, '+5', '  BERLAKU SAMPAI DENGAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '0', strtoupper($data['exp']), '', 70, '', 'Arial', 'B', 10, 0, 13);
        $this->Ln();

        if (strtolower($data['produksi']) == "dalam negeri") {
            $this->tulis($x, 102, 'JENIS BARANG ', 'C', 120, '', 'Arial', 'B', 10, 'L,R,T', 10);
            // $this->tulis($x + 120, 102, 'NO.HS ', 'C', 32, '', 'Arial', 'B', 10, 'L,R,T', 10);
            $this->tulis($x + 120, 102, 'MERK ', 'C', 65, '', 'Arial', 'B', 10, 'L,R,T', 10);

            $this->SetY(130);
            $this->SetX(115);
            $this->Rect(15, 112, 185, 120);
            $this->Line(135, 231, 135, 110);
            // $this->Line(167, 231, 167, 110);
        } else {
            $this->tulis($x, 102, 'JENIS BARANG ', 'C', 120, '', 'Arial', 'B', 10, 'L,R,T', 10);
            $this->tulis($x + 120, 102, 'NO.HS ', 'C', 32, '', 'Arial', 'B', 10, 'L,R,T', 10);
            $this->tulis($x + 152, 102, 'MERK ', 'C', 33, '', 'Arial', 'B', 10, 'L,R,T', 10);

            $this->SetY(130);
            $this->SetX(115);
            $this->Rect(15, 112, 185, 120);
            $this->Line(135, 231, 135, 110);
            $this->Line(167, 231, 167, 110);
        }


        $this->tulis($x + 105, '+100', 'Diterbitkan di ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 137, '0', 'J A K A R T A', '', 70, '', 'Arial', 'B', 10, 0, 13);
        #tanggal
        $this->tulis($x + 105, '+4', 'pada tanggal ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 137, '0', $data['izin'], '', 70, '', 'Arial', 'B', 10, 0, 13);

        $this->Ln(10);
        $this->tulis($x + 75, '+', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);

        if ($data['jabatan_ttd'] == '') {
            $this->tulis($x + 75, '+4', $data['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 75, '+4', $data['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }

        $this->tulis($x + 75, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 20, 0, 15);

        if ($nama_ttd == '') {
            $this->tulis($x + 75, '+15', $data['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 75, '+15', $data['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }


        $string = utf8_decode($data['nm_perusahaan'] . "\n" . $this->iif($data['no_izin'], $data['no_izin'], '-') . "\n" . $data['izin'] . "\n" . $data['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this, $x + ($lebarlabelstatic + $lebarnomor + 65), ($this->GetY() - 20), 25); #Coba diganti ganti aja 30 dan 15 nya.

        $this->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
    }

    function RotatedText($x, $y, $txt, $angle) {
        //Text rotated around its origin
        $this->Rotate($angle, $x, $y);
        $this->Text($x, $y, $txt);
        $this->Rotate(0);
    }

    function format_huruf($raw_url_encoded) {
        $reportSubtitle = stripslashes($raw_url_encoded);
        $reportSubtitle = iconv('UTF-8', 'windows-1252', $reportSubtitle);
        $reportSubtitle = str_replace("&#41;", ")", $reportSubtitle);
        $reportSubtitle = str_replace("&#40;", "(", $reportSubtitle);
        return $reportSubtitle;
    }

    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */

    function NewIkd() {
        $this->AddPage();
        $Yget = $this->GetY();
        $this->SetY($Yget);
    }

    function NewIkdLandscape() {
        $this->AddPage('L', 'mm', 'Legal');
        $Yget = $this->GetY();
        //  $this->Footer();
        $this->SetY($Yget);
    }

    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }

    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }

    function FormatNPWP($varnpwp) {
        $varresult = '';
        $varresult = substr($varnpwp, 0, 2) . "." . substr($varnpwp, 2, 3) . "." . substr($varnpwp, 5, 3) . "." . substr($varnpwp, 8, 1) . "-" . substr($varnpwp, 9, 3) . "." . substr($varnpwp, 12, 3);
        return $varresult;
    }

    function FormatTDP($varnoijin) {
        $varresult = '';
        $varresult = substr($varnoijin, 0, 2) . "." . substr($varnoijin, 2, 2) . "." . substr($varnoijin, 4, 1) . "." . substr($varnoijin, 5, 2) . "." . substr($varnoijin, 7, 5);
        return $varresult;
    }

    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */

    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */

    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 3.7) {
        if ($huruf != "")
            $this->SetFont($huruf, $model, $ukuran);
        if (substr($xpos, 0, 1) == "+")
            $this->SetX($this->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->SetX($this->GetX() + intval($xpos));
        if (substr($ypos, 0, 1) == "+")
            $this->SetY($this->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->SetY($this->GetY() + intval($ypos));
        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->SetY($ypos);
        if ($tipe == "W")
            $this->WriteFlowingBlock($teks);
        else if ($tipe == "M")
            $this->MultiCell($lebar, 4, $teks, 0, $align, 0);
        else
            $this->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }

    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */

    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */

    function data($arrdata, $margin) {
        $this->footer_data = array(
            'jenis_produk' => $arrdata['row']['jns_produk'],
            'jenis_agen' => $arrdata['row']['jenis_agen'],
            'jns_produk' => $arrdata['row']['jns_produk'],
            'produksi' => $arrdata['row']['produksi'],
            'nm_perusahaan' => $arrdata['row']['nm_perusahaan'],
            'no_izin' => $arrdata['row']['no_izin'],
            'exp' => $arrdata['row']['exp'],
            'izin' => $arrdata['row']['izin'],
            'jabatan_ttd' => $arrdata['row']['jabatan_ttd'],
            'jabatan' => $arrdata['ttd']['jabatan'],
            'nama' => $arrdata['ttd']['nama'],
            'nama_ttd' => $arrdata['row']['nama_ttd'],
            'link' => $arrdata['link'],
            'wilayah' => $arrdata['row']['pemasaran']);
        $this->NewIkd();
        $dflength = 174;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarstaticpendaftaran = 54;
        $lebarnomor = 8;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        }
        if ($arrdata['row']['status'] != '1000') {
            $this->SetTextColor(219, 219, 219);
            $this->SetFont('Arial', 'B', 60);
            $this->RotatedText(50, 200, "D    R    A    F    T", 35);
            $this->SetTextColor(000, 000, 000);
        }
        $x = $kiri;

        $this->Ln(8);
        $this->tulis($x, '+8', 'SURAT TANDA PENDAFTARAN SEBAGAI', 'C', $dflength, '', 'Arial', 'B', 20);
        $this->tulis($x, '+8', strtoupper($arrdata['row']['jenis_agen']), 'C', $dflength, '', 'Arial', 'B', 20);
        if ($arrdata['row']['jns_produk'] == '02') {
            $this->tulis($x, '+8', 'JASA PERUSAHAAN ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 20);
        } elseif ($arrdata['row']['jns_produk'] == '03') {
            $this->tulis($x, '+8', 'PUPUK PRODUKSI ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 20);
        } else {
            $this->tulis($x, '+8', 'BARANG PRODUKSI ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 20);
        }
        $this->Ln(10);
        #Border Nomor Telepon


        $this->Ln(5);

        #Border Atas
        //$this->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext, $x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 11);
        //$this->Line($x, $this->GetY(), ($x + $dflength), $this->GetY());
        // 01 Dalam Negeri
        // 02 Luar Negeri 
        $this->SetFont('Arial', 'B', 10);
        if ($arrdata['row']['jns_produk'] == "01") {
            if ($arrdata['row']['jns_produksi'] == "01") {
                $this->SetFillColor(0, 0, 255); //change color with RGB for jenis "barang Dalam Negeri" Biru   
            } else if ($arrdata['row']['jns_produksi'] == "02") {
                $this->SetFillColor(255, 255, 0); //change color with RGB for jenis "barang Luar Negeri" Kuning  
            }
        } elseif ($arrdata['row']['jns_produk'] == "02") {
            $this->SetFillColor(255, 0, 0); //change color with RGB for jenis "Jasa Dalam Negeri" Merah   
        } elseif ($arrdata['row']['jns_produk'] == "03") {
            $this->SetFillColor(0, 255, 0); //change color with RGB for jenis "pupuk"
        } else {
            $this->SetFillColor(30, 144, 255);
        }
        // if (in_array($arrdata['row']['status'], array('0200', '0100', '0101', '0700', '0600'))) { 
        //     $this->MultiCell(0, 8, "           NOMOR                                          :  " . trim($arrdata['temp_terbit']['no_izin']), 1, 1, true);
        // } else {
        $this->SetXY(15, 72);
        $this->MultiCell(175, 8, "  NOMOR                                          :  ", 1, 1, true);
        // }
        $this->SetFont('Arial', 'B', 10);
        if ($arrdata['row']['jns_produk'] == "01") {
            if ($arrdata['row']['jns_produksi'] == "01") {
                $this->SetFillColor(0, 0, 255); //change color with RGB for jenis "barang Dalam Negeri" Biru   
            } else if ($arrdata['row']['jns_produksi'] == "02") {
                $this->SetFillColor(255, 255, 0); //change color with RGB for jenis "barang Luar Negeri" Kuning  
            }
        } elseif ($arrdata['row']['jns_produk'] == "02") {
            $this->SetFillColor(255, 0, 0); //change color with RGB for jenis "Jasa Dalam Negeri" Merah   
        } elseif ($arrdata['row']['jns_produk'] == "03") {
            $this->SetFillColor(0, 255, 0); //change color with RGB for jenis "pupuk"
        } else {
            $this->SetFillColor(30, 144, 255);
        }
        $this->SetXY(15, 80);
        $this->MultiCell(175, 8, "  BERLAKU SAMPAI DENGAN       :  ", 1, 1, true);


        $this->Ln(10);


        $this->tulis($x + 55, '-5', 'DIBERIKAN KEPADA :', '', 70, '', 'Arial', '', 10, 0, 13);
        $this->Ln(5);
        #nama perusahaan
        $this->tulis($x + 1, 0, '1. NAMA PERUSAHAAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 65, '0', strtoupper($arrdata['row']['nm_perusahaan']), '', 70, '', 'Arial', 'B', 10, 0, 13);
        $this->Ln(8);
        #alamat
        $this->tulis($x + 1, '0', '2.   ALAMAT ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 65, '+5', ucwords($arrdata['row']['alamat']), '', 120, 'M', 'Arial', '', 10, 0, 13);
        //  $this->tulis($x + 60, '+0',  , '', 70, '', 'Arial', '', 10,0,13);
        //$this->Ln(8);
        #tlp/fax
        $this->tulis($x + 1, '+', '3.   NO. TELP / FAX ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 65, '0', $arrdata['row']['telp'] . ' / ' . $arrdata['row']['fax'], '', 70, '', 'Arial', '', 10, 0, 13);

        $this->Ln(8);
        #nama pj
        $this->tulis($x + 1, '+', '4.   NAMA PENANGGUNG JAWAB ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 65, '0', strtoupper($arrdata['row']['nama_pj']), '', 70, '', 'Arial', 'B', 10, 0, 13);
        #jabatan
        $this->tulis($x + 2, '+4', '    JABATAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 65, '0', $arrdata['row']['jabatan_pj'], '', 70, '', 'Arial', '', 10, 0, 13);

        if ($arrdata['row']['status_dok'] == '02') {
            $this->SetTextColor(112, 121, 125);
            $this->SetFont('Arial', 'B', 60);
            $this->RotatedText(110, 260, "DICABUT", 0);
            $this->SetTextColor(000, 000, 000);
        }

        $this->Ln(7);
        #nama pj
        $produsen = strtoupper($this->format_huruf($arrdata['row']['nm_perusahaan_prod']));
        $string = iconv('UTF-8', 'windows-1252', $produsen);
        $this->tulis($x + 1, '+', '5.   NAMA PRODUSEN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        // $this->tulis($x + 60, '+5', ucfirst($arrdata['row']['almt_perusahaan_prod']), '', 120, 'M', 'Arial', '', 10, 0, 13);
        if ($arrdata['row']['jns_produksi'] == '01') {
            $this->tulis($x + 65, '+5', strtoupper($arrdata['row']['tipe_perusahaan_prod'] . '. ' . $arrdata['row']['nm_perusahaan_prod']), '', 120, 'M', 'Arial', 'B', 10, 0, 13);
        } else {
            $this->tulis($x + 65, '+5',strtoupper($arrdata['row']['nm_perusahaan_prod']) , '', 120, 'M', 'Arial', 'B', 10, 0, 13);
        }
        #alamat produsen
        $this->tulis($x + 2, '-1', '      ALAMAT ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        // $this->tulis($x + 60, '+5', ucfirst($arrdata['row']['alamat']), '', 100, 'M', 'Arial', '', 10, 0, 13);
        if ($arrdata['row']['jns_produksi'] == '01') {
            $this->tulis($x + 65, '+5', ucfirst($arrdata['row']['alamat_prod']), '', 110, 'M', 'Arial', '', 10, 0, 13);
        } else {
        	if ($arrdata['row']['id'] == 3109) {
        		$this->tulis($x + 65, '+5', ucfirst($arrdata['row']['almt_perusahaan_prod']), '', 110, 'M', 'Arial', '', 10, 0, 13);
        	}else{
        		$this->tulis($x + 65, '+5', ucfirst($arrdata['row']['almt_perusahaan_prod']), '', 110, 'M', 'Arial', '', 10, 0, 13);
        	}
        }
        //$this->Ln(1);
        #nama pemasok
        $this->tulis($x + 1, '+', '6.   NAMA PEMASOK (SUPPLIER) ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        if ($arrdata['row']['nm_perusahaan_supl'] == '') {
            $this->tulis($x + 65, '0', "--", '', 70, '', 'Arial', 'B', 10, 0, 13);
        } else {
            $this->tulis($x + 65, '+5', strtoupper($this->format_huruf($arrdata['row']['nm_perusahaan_supl'])), '', 70, 'M', 'Arial', 'B', 10, 0, 13);
        }
        #alamat pemasok
        $this->tulis($x + 2, '0', '      ALAMAT ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        // $this->tulis($x + 60, '+5', ucfirst($arrdata['row']['alamat']), '', 100, 'M', 'Arial', '', 10, 0, 13);
        if ($arrdata['row']['almt_perusahaan_supl'] == '') {
            $this->tulis($x + 65, '+5', "--", '', 120, 'M', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 65, '+5', ucfirst($this->format_huruf($arrdata['row']['almt_perusahaan_supl'])), '', 110, 'M', 'Arial', '', 10, 0, 13);
        }

        //$this->Ln(1);
        #nama pj
        if ($arrdata['row']['jns_produk'] != "02") {
            $this->tulis($x + 1, '-3', '7.  JENIS BARANG ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 1, '-3', '7.  JENIS JASA ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }
        $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
//        $this->tulis($x + 60, 0, $this->format_huruf($arrdata['row']['jenis']), 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 65, '+5', ucwords(strlen($this->format_huruf($arrdata['row']['jenis'])) < 125) ? ucwords($this->format_huruf($arrdata['row']['jenis'])) : 'Terlampir', '', 125, 'M', 'Arial', '', 10, 0, 13);
        //$this->Ln(8);
        #nama pj
        $this->tulis($x + 1, '-3', '8.  MERK ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 65, '+5', (strlen($arrdata['row']['merk']) < 125) ? $this->format_huruf($arrdata['row']['merk']) : 'Terlampir', '', 125, 'M', 'Arial', '', 10, 0, 13);

        if ($arrdata['row']['jns_produk'] != "02") {
            if ($arrdata['row']['jns_produksi'] == "02") {
                $this->tulis($x + 1, '+', '9.   NO. HS ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                $this->tulis($x + 65, '0', (strlen(trim($arrdata['row']['hs'])) < 80) ? $arrdata['row']['hs'] : 'Terlampir', '', 70, '', 'Arial', '', 10, 0, 13);
                // $this->tulis($x + 60, '0', strlen($arrdata['row']['hs']), '', 70, '', 'Arial', '', 10, 0, 13);
                $next = '10';
                $this->Ln(8);
            }
        } else {
            $next = '9';
        }


        #nama pj
        $pemasaran = explode(",", $arrdata['row']['pemasaran']);
        if (count($pemasaran)) {

            function tes($arr) {
                return ucfirst(trim($arr));
            }

        }
        $map = (count($pemasaran)) ? array_map("tes", $pemasaran) : ucfirst($arrdata['row']['pemasaran']);
        $this->tulis($x + 1, '+1', $next . '.    WILAYAH PEMASARAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        if (strlen($arrdata['row']['pemasaran']) > 55) {
            $this->tulis($x + 65, '0', "Terlampir", '', 70, '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 65, '0', implode(", ", $map), '', 70, '', 'Arial', '', 10, 0, 13);
        }

        $this->Ln(10);
        #footer
        #terbit
        $this->tulis($x + 105, '+', 'Diterbitkan di ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 137, '0', 'J A K A R T A', '', 70, '', 'Arial', 'B', 10, 0, 13);
        #tanggal
        $this->tulis($x + 105, '+4', 'pada tanggal ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 137, '0', $arrdata['row']['izin'], '', 70, '', 'Arial', 'B', 10, 0, 13);

        $this->Ln(10);
        $this->tulis($x + 75, '+', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        if ($arrdata['row']['jabatan_ttd'] == '') {
            $this->tulis($x + 75, '+4', $arrdata['ttd']['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 75, '+4', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }

        $this->tulis($x + 75, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 20, 0, 15);

        if ($arrdata['row']['nama_ttd'] == '') {
            $this->tulis($x + 75, '+15', $arrdata['ttd']['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 75, '+15', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }

        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this, $x + ($lebarlabelstatic + $lebarnomor + 65), ($this->GetY() - 22), 20); #Coba diganti ganti aja 30 dan 15 nya.

        if ($arrdata['tembusan'] != '') {
            
        } else {
            $this->tulis($x, '-30', 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf - 9);
            for ($i = 0; $i < count($arrdata['tembusan']); $i++) {
                $this->tulis($x, '+4', $arrdata['tembusan'][$i]['urutan'] . '.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 12);
                $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 12);
            }
        }
        $this->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);

        if (in_array($arrdata['row']['status'], array('0200', '0100', '0101', '0700', '0600'))) {
            $nomors = trim($arrdata['temp_terbit']['no_izin']);
            $tgl_exps = $arrdata['temp_terbit']['tgl_izin_exp'];
        } else {
            $nomors = trim($arrdata['row']['no_izin']);
            $tgl_exps = $arrdata['row']['exp'];
        }
        $this->SetXY(75, 70);
        $this->tulis('', '', $nomors, 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'B', 10, 0, 13);
        $this->SetXY(75, 78);
        $this->tulis('', '', $tgl_exps, 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'B', 10, 0, 13);

        $this->AddPage();
        $this->showbarang = 0;
        $this->tulis($x, '+', 'KETENTUAN UMUM', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x, '+10', '1.', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 5, '+5', 'Apabila terdapat kekeliruan dalam Surat Tanda Pendaftaran (STP) ini akan diadakan perubahan atau penyesuaian sebagaimana mestinya sampai kepada pembatalan atau pencabutan masa berikutnya', 'J', $dflength, 'M', 'Arial', '', 12, 0, 13);

        $this->tulis($x, '+2', '2.', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 5, '+4', 'Setiap pemegang (STP) wajib menyampaikan laporan kegiatan perusahaan setiap 6(enam) bulan sekali kepada Direktur Bina Usaha Dan Pelaku Distribusi', 'J', $dflength, 'M', 'Arial', '', 12, 0, 13);

        $this->tulis($x, '+2', '3.', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 5, '+5', 'Setiap perusahaan yang tidak lagi melakukan usahanya atau menutup perusahaannya wajib melaporkan penutupan kegiatan perusahaan usahanya dan mengembalika STP asli kepada Direktur Bina Usaha Dan Pelaku Distribusi', 'J', $dflength, 'M', 'Arial', '', 12, 0, 13);

        $this->tulis($x, '+2', '4.', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 5, '+5', 'Setiap perusahaan yang melakukan perubahan nama prinsipal, status penunjukan keagenan /  kedistribusian, merk, wilayah pemasaran jenis barang, alamat perusahaan, penanggung jawab perusahaan wajib melaporkan kepada Direktur Bina Usaha Dan Pelaku Distribusi', 'J', $dflength, 'M', 'Arial', '', 12, 0, 13);
        $this->tulis($x, '+14', 'SANKSI', 'L', $dflength, 'M', 'Arial', '', 12, 0, 13);

        $this->tulis($x, '+2', '1.', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 5, '+5', 'Setiap perusahaan agen / distributor yang tidak melakukan pendaftaran dikenakan sanksi administratif sampai dengan pencabutan SIUP. (Pasal 24) ', 'J', $dflength, 'M', 'Arial', '', 12, 0, 13);

        $this->tulis($x, '0', '2.', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 5, '+5', 'Setiap perusahaan agen / distributor yang tidak menyampaikan laporan kegiatan dan perubahan perubahan yang dilakukan dikenakan sanksi administratif sampai dengan pemberhentian sementara STP selama 6(enam) bulan atau pencabutan STP. (Pasal 25)', 'J', $dflength, 'M', 'Arial', '', 12, 0, 13);
        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this, $x + ($lebarlabelstatic + $lebarnomor + 70), ($this->GetY() + 20), 25); #Coba diganti ganti aja 30 dan 15 nya.

        $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);

        if (strlen($arrdata['row']['pemasaran']) > 55) {
            $this->AddPage();
            $this->showbarang = 0;
            $this->Ln(8);
            $this->tulis($x, '+8', 'LAMPIRAN SURAT TANDA PENDAFTARAN SEBAGAI', 'C', $dflength, '', 'Arial', 'B', 18);
            $this->tulis($x, '+8', strtoupper($arrdata['row']['jenis_agen']), 'C', $dflength, '', 'Arial', 'B', 18);
            if ($arrdata['row']['jns_produk'] == '02') {
                $this->tulis($x, '+8', 'JASA PERUSAHAAN ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 18);
            } elseif ($arrdata['row']['jns_produk'] == '03') {
                $this->tulis($x, '+8', 'PUPUK PRODUKSI ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 18);
            } else {
                $this->tulis($x, '+8', 'BARANG PRODUKSI ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 18);
            }

            $this->Ln();
            $this->tulis($x + 1, 0, '  NAMA PERUSAHAAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nm_perusahaan']), '', 70, '', 'Arial', 'B', 10, 0, 13);

            $this->tulis($x + 1, '+5', '  N O M O R ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 60, '0', strtoupper($arrdata['row']['no_izin']), '', 70, '', 'Arial', 'B', 10, 0, 13);

            $this->tulis($x + 1, '+5', '  BERLAKU SAMPAI DENGAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 60, '0', strtoupper($arrdata['row']['exp']), '', 70, '', 'Arial', 'B', 10, 0, 13);
            $this->Ln();
            $this->SetFillColor(255, 255, 255);
            $this->tulis($x, '0', 'WILAYAH PEMASARAN ', 'C', 185, '', 'Arial', 'B', 10, 'L,R,T', 10);
            $this->Ln();
            $this->SetXY(15, 93);
            $this->SetFont('Arial', '', 10);
            $this->MultiCell(185, 5, str_replace("\n", " ", $arrdata['row']['pemasaran']), 0, 'J', false);
            $this->SetY(130);
            $this->SetX(115);
            $this->Rect(15, 93, 185, 120);
            //$this->Ln();
            $this->SetY(130);
            $this->SetX(115);
            $this->tulis($x + 105, '+100', 'Diterbitkan di ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
            $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 137, '0', 'J A K A R T A', '', 70, '', 'Arial', 'B', 10, 0, 13);
            #tanggal
            $this->tulis($x + 105, '+4', 'pada tanggal ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
            $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 137, '0', $arrdata['row']['izin'], '', 70, '', 'Arial', 'B', 10, 0, 13);

            $this->Ln(10);
            $this->tulis($x + 75, '+', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);

            if ($arrdata['row']['jabatan_ttd'] == '') {
                $this->tulis($x + 75, '+4', $arrdata['ttd']['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            } else {
                $this->tulis($x + 75, '+4', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            }

            $this->tulis($x + 75, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 20, 0, 15);

            if ($arrdata['row']['nama_ttd'] == '') {
                $this->tulis($x + 75, '+15', $arrdata['ttd']['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            } else {
                $this->tulis($x + 75, '+15', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            }


            $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['izin'] . "\n" . $arrdata['link']);
            $this->qrcode->genQRcode($string, 'L');
            $this->qrcode->disableBorder();
            $this->qrcode->displayFPDF($this, $x + ($lebarlabelstatic + $lebarnomor + 65), ($this->GetY() - 20), 20); #Coba diganti ganti aja 30 dan 15 nya.

            $this->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
            $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
        }


        $len = strlen($arrdata['row']['jenis']);
        $len_merk = strlen($arrdata['row']['merk']);
        $jns = str_replace("\n", " ", $arrdata['row']['jenis']);
        $jns = $this->format_huruf($jns);
        $len_hs = strlen($arrdata['row']['hs']);
        $ceil = ceil($len / 1619);
        if ($len >= 125 || $len_hs >= 80 || $len_merk >= 125) {
            // $this->AddPage();
            // $this->showbarang = 1;
            // $this->SetTopMargin(92);
            // // $this->SetMargins(50, 100, 100);
            // $this->SetAutoPageBreak(true, 65);
            // $this->SetFont('Arial', '', 12);
            // $this->SetFillColor(255, 255, 255);
            if ($arrdata['row']['id'] == 70739 || $arrdata['row']['id'] == 71738) {

                $removeSpace = str_replace(" ", "", $arrdata['row']['hs']);
                $removeEnter = str_replace("\n", ";", $removeSpace);

                $sumStrHS = strlen($removeEnter);
                $sumStrJNS = strlen($jns);

                $modStrHS = $sumStrHS / 300;
                $modStrJNS = $sumStrJNS / 1200;

                $startHS = 0;
            	$startJNS = 0;

                for ($i=0; $i < 11; $i++) {

                	$fixHS = substr($removeEnter, $startHS, 300);
                	$fixJNS = substr($jns, $startJNS, 1200);

                	if ($i == 0) {
                		$this->AddPage();
			            $this->showbarang = 1;
			            $this->SetTopMargin(92);
			            // $this->SetMargins(50, 100, 100);
			            $this->SetAutoPageBreak(true, 65);
			            $this->SetFont('Arial', '', 12);
			            $this->SetFillColor(255, 255, 255);

                		$this->SetY(112);
		                $this->SetX(167);
		                $this->MultiCell(32, 5, $arrdata['row']['merk'], 0, 'J', false);

		                $this->SetY(112);
		                $this->SetX(135);
		                $this->MultiCell(32, 5, str_replace("\n", " ", $fixHS), 0, 'J', false);

		                $this->SetXY(15, 112);
		                $this->MultiCell(120, 5, preg_replace("/\s+/", " ", $fixJNS), 0, 'J', false);

                	}elseif ($i == 10) {
                		$fixJNS = substr($jns, $startJNS, $sumStrJNS);
                		$this->AddPage();
			            $this->showbarang = 1;
			            $this->SetTopMargin(92);
			            // $this->SetMargins(50, 100, 100);
			            $this->SetAutoPageBreak(true, 65);
			            $this->SetFont('Arial', '', 12);
			            $this->SetFillColor(255, 255, 255);

                		$this->SetY(112);
		                $this->SetX(135);
		                $this->MultiCell(32, 5, str_replace("\n", " ", $fixHS), 0, 'J', false);

		                $this->SetXY(15, 112);
		                $this->MultiCell(120, 5, preg_replace("/\s+/", " ", $fixJNS), 0, 'J', false);
                	} else{
                		$this->AddPage();
			            $this->showbarang = 1;
			            $this->SetTopMargin(92);
			            // $this->SetMargins(50, 100, 100);
			            $this->SetAutoPageBreak(true, 65);
			            $this->SetFont('Arial', '', 12);
			            $this->SetFillColor(255, 255, 255);

                		$this->SetY(112);
		                $this->SetX(135);
		                $this->MultiCell(32, 5, str_replace("\n", " ", $fixHS), 0, 'J', false);

		                $this->SetXY(15, 112);
		                $this->MultiCell(120, 5, preg_replace("/\s+/", " ", $fixJNS), 0, 'J', false);

		                
                	}

                	$startHS = $startHS + 300;
		            $startJNS = $startJNS + 1200;


                }

            } else {
            	$this->AddPage();
	            $this->showbarang = 1;
	            $this->SetTopMargin(92);
	            // $this->SetMargins(50, 100, 100);
	            $this->SetAutoPageBreak(true, 65);
	            $this->SetFont('Arial', '', 12);
	            $this->SetFillColor(255, 255, 255);
                $this->SetXY(15, 112);
                $this->MultiCell(120, 5, preg_replace("/\s+/", " ", $jns), 0, 'J', false);
                if (strtolower($arrdata['row']['jns_produksi']) == "02" || $arrdata['row']['jns_produk'] == '02') {
                    $this->SetY(112);
                    $this->SetX(167);
                    $this->MultiCell(32, 5, $arrdata['row']['merk'], 0, 'J', true);

                    $this->SetY(112);
                    $this->SetX(135);
                    $this->MultiCell(32, 5, $arrdata['row']['hs'], 0, 'J', true);
                } else {
                    $this->SetY(112);
                    $this->SetX(136);
                    $this->MultiCell(58, 5, $arrdata['row']['merk'], 0, 'J', true);
                }
                
            }
        }
    }

    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */




    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}

$objpdf = new pdf();
$margins = array(15, 50, 15, 15);
$objpdf->ukuranhuruf = 12;
$objpdf->ukuranhurufdetil = 10;
$objpdf->orientasi = $orientasi;
$objpdf->ukuran = $ukuran;
$objpdf->kertas = $kertas;
$objpdf->pdf = new NEWFPDF("P", "mm", "A4");
$objpdf->qrcode = new QRcode();
$objpdf->showbarang = 0;
$objpdf->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
$objpdf->data($arrdata, $margins);
if ($arrdata['cetak'] != '') {
    $namafile = $arrdata['namafile'];
    $dir = $arrdata['dir'];
    if (file_exists($dir) && is_dir($dir)) {
        $path = 'upL04d5/document/DOKAGEN/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
    } else {
        if (mkdir($dir, 0777, true)) {
            if (chmod($dir, 0777)) {
                $path = 'upL04d5/document/DOKAGEN/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
            }
        }
    }
    $objpdf->Output($path, 'F');
} else {
    $objpdf->Output();
}
?>