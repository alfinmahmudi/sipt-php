<?php

error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $def_margin = array(15, 50, 15, 15);
    var $angle=0;

    function Rotate($angle,$x=-1,$y=-1)
    {
	if($x==-1)
		$x=$this->x;
	if($y==-1)
		$y=$this->y;
	if($this->angle!=0)
		$this->pdf->_out('Q');
	$this->angle=$angle;
	if($angle!=0)
	{
		$angle*=M_PI/180;
		$c=cos($angle);
		$s=sin($angle);
		$cx=$x*$this->k;
		$cy=($this->h-$y)*$this->k;
		$this->pdf->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
	}
    }
    
    function _endpage()
    {
	if($this->angle!=0)
	{
		$this->angle=0;
		$this->pdf->_out('Q');
	}
	parent::_endpage();
    }

    function pdf($orientasi, $ukuran, $kertas, $margins, $data) {
        $this->ukuranhuruf = 12;
        $this->ukuranhurufdetil = 10;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->qrcode = new QRcode();
        $this->pdf->footer = 1;
        if (is_array($margins) and count($margins) == 4) {
            $this->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
        } else {

            $this->pdf->SetMargins($this->def_margin[0], $this->def_margin[1], $this->def_margin[2], $this->def_margin[3]);
        }
        $this->data($data, $margins);
    }

    function Footer() {

        $this->pdf->SetY(-15);
        // Arial italic 8
        $this->pdf->SetFont('Arial', 'IB', 7);
        // Page number
        $this->pdf->Cell(0, 10, 'Dokumen ini sah, diterbitkan secara elektronik melalui sistem SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', 0, 0, 'C');
        $this->pdf->SetY(30);
        $this->pdf->SetFont('Arial', '', 10);
        //   $this->tulis(100, 100, 'Dokumen ini sah, diterbitkan secara elektronik melalui sistem SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'I', 7);
    }

    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */

    function NewIkd() {
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->pdf->SetY($Yget);
    }

    function NewIkdLandscape() {
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->pdf->SetY($Yget);
    }

    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }

    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }

    function FormatNPWP($varnpwp) {
        $varresult = '';
        $varresult = substr($varnpwp, 0, 2) . "." . substr($varnpwp, 2, 3) . "." . substr($varnpwp, 5, 3) . "." . substr($varnpwp, 8, 1) . "-" . substr($varnpwp, 9, 3) . "." . substr($varnpwp, 12, 3);
        return $varresult;
    }

    function FormatTDP($varnoijin) {
        $varresult = '';
        $varresult = substr($varnoijin, 0, 2) . "." . substr($varnoijin, 2, 2) . "." . substr($varnoijin, 4, 1) . "." . substr($varnoijin, 5, 2) . "." . substr($varnoijin, 7, 5);
        return $varresult;
    }
    
    function RotatedText($x, $y, $txt, $angle) {
        //Text rotated around its origin
        $this->Rotate($angle, $x, $y);
        $this->pdf->Text($x, $y, $txt);
        $this->Rotate(0);
    }

    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */

    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */

    function SetWidths($w) {
        //Set the array of column widths
        $this->pdf->widths = $w;
    }

    function SetAligns($a) {
        //Set the array of column alignments
        $this->pdf->aligns = $a;
    }

    function Row($data, $string) {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->pdf->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed

        $this->CheckPageBreak($h, $string);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->pdf->widths[$i];
            $a = isset($this->pdf->aligns[$i]) ? $this->pdf->aligns[$i] : 'L';
            //Save the current position
            $x = $this->pdf->GetX();
            $y = $this->pdf->GetY();
            //Draw the border
            $this->pdf->Rect($x, $y, $w, $h);
            //Print the text
            $this->pdf->MultiCell($w, 5, $data[$i], 0, $a);
            //Put the position to the right of the cell
            $this->pdf->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->pdf->Ln($h);
    }

    function CheckPageBreak($h, $string) {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->pdf->GetY() + $h + 40 > $this->pdf->PageBreakTrigger) {
            $this->pdf->AddPage($this->pdf->CurOrientation);
            if ($arrdata['row']['status'] != '1000') {
                $this->pdf->SetTextColor(219, 219, 219);
                $this->pdf->SetFont('Arial', 'B', 60);
                $this->RotatedText(100, 200, "D    R    A    F    T", 35); 
                $this->pdf->SetTextColor(000, 000, 000);
            }
            $this->Footer();
            $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 100), ($this->pdf->GetY() +230), 20);
            $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 150, 35);

            // $this->tulis(10, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui sistem SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'I', 7);
        }
    }

    function NbLines($w, $txt) {
        //Computes the number of lines a MultiCell of width w will take
        $cw = &$this->pdf->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->pdf->rMargin - $this->pdf->x;
        $wmax = ($w - 2 * $this->pdf->cMargin) * 1000 / $this->pdf->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l+=$cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }

    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 3.7) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);
        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);
        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
            $this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }

    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */

    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */

    function data($arrdata, $margin) {
        $this->NewIkd();
        $dflength = 174;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarstaticpendaftaran = 54;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        }
        $x = $kiri;
         if ($arrdata['row']['status'] != '1000') {
                $this->pdf->SetTextColor(219, 219, 219);
                $this->pdf->SetFont('Arial', 'B', 60);
                $this->RotatedText(100, 200, "D    R    A    F    T", 35); 
                $this->pdf->SetTextColor(000, 000, 000);
            }
        $this->tulis($x, '+0', 'Nomor', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 20, '+0', ':', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 22, '+0', $arrdata['row']['no_izin'], 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 140, '+0', 'Jakarta, ' . $arrdata['row']['izin'], 'L', $dflength, '', 'Arial', '', 10);

        $this->tulis($x, '+5', 'Lampiran', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 20, '0', ':', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 22, '+0', '1 (satu) Berkas', 'L', $dflength, '', 'Arial', '', 10);

        $this->tulis($x, '+5', 'Perihal', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 20, '+0', ':', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 22, '+0', 'Permohonan Persetujuan Prinsip Pameran', 'L', $dflength, '', 'Arial', '', 10);

        if ($arrdata['row']['status_dok'] == '02') {
            $this->pdf->SetTextColor(112, 121, 125);
            $this->pdf->SetFont('Arial', 'B', 60);
            $this->RotatedText(110, 260, "DICABUT", 0);   
            $this->pdf->SetTextColor(000, 000, 000);
        }

        $this->tulis($x, '+20', 'Kepada Yth.', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x, '+5', 'Sdr. Direktur', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x, '+5', $arrdata['row']['nm_perusahaan'], 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x, '+5', $arrdata['row']['almt_perusahaan'] . " , Kel. " . $arrdata['row']['kel_perusahaan'] . ", Kec. " . $arrdata['row']['kec_perusahaan'], 'L', $dflength - 60, 'M', 'Arial', '', 10);
        $this->tulis($x, '+5', $arrdata['row']['prop_perusahaan'] . " " . $arrdata['row']['kdpos'], 'L', $dflength, '', 'Arial', '', 10);

        $this->tulis($x + 1, '+10', '       Sehubungan dengan permohonan Saudara No. ' . $arrdata['row']['no_aju'] . ' tanggal ' . $arrdata['row']['tgl_kirim'] . ' perihal permohonan rekomendasi jadwal pameran, yang bertempat di ' . $arrdata['row']['lokasi_pameran'] . ' untuk tahun ' . substr($arrdata['tahun'], -4, 4) . ', dengan ini kami sampaikan hal-hal sebagai berikut : ', 'J', $dflength + 4, 'M', 'Arial', '', 10, 2);

        $this->tulis($x, '+2', '1.', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 5, '+0', 'Kementerian Perdagangan pada prinsipnya menyetujui Permohonan Izin Prinsip Pameran di ' . $arrdata['row']['lokasi_pameran'] . ' untuk tahun ' . substr($arrdata['tahun'], -4, 4) . ' sebagaimana terlampir.', 'J', $dflength, 'M', 'Arial', '', 10, 2);
        $this->tulis($x, '+1', '2.', 'L', $dflength, '', 'Arial', '', 10);
        $this->tulis($x + 5, '+0', 'Sesuai dengan ketentuan dalam Keputusan Menteri Perindustrian dan Perdagangan Nomor 199/MPP/Kep/6/2001 tentang Persetujuan Penyelenggaraan Pameran Dagang, Konvensi dan/atau Seminar Dagang. Permohonan sebagaimana butir 1 diatas, agar segera diajukan oleh masing masing perusahaan yang bersangkutan sebelum pelaksanaan pameran.', 'J', $dflength, 'M', 'Arial', '', 10, 2);

        $this->tulis($x + 10, '+10', 'Demikian disampaikan untuk dilaksanakan dengan sebaik-baiknya.', 'L', $dflength, '', 'Arial', '', 10);

        $this->pdf->Ln(10);
        // $this->tulis($x + 115, '+', $arrdata['row']['jabatan_ttd'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
        // //$this->tulis($x + 115, '+4', 'Bina Usaha Perdagangan', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
        // $this->pdf->Ln(7);
        // $this->tulis($x + 140, '+4', 'TTD', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
        // $this->pdf->Ln(7);
        // $this->tulis($x + 115, '+4', $arrdata['row']['nama_ttd'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);

        $this->tulis($x + 75, '+7', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);

        if ($arrdata['row']['jabatan_ttd'] == '') {
            $this->tulis($x + 75, '+4', $arrdata['ttd']['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 75, '+4', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }

        $this->tulis($x + 75, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 15, 0, 15);

        if ($arrdata['row']['nama_ttd'] == '') {
            $this->tulis($x + 75, '+15', $arrdata['ttd']['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 75, '+15', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }

        // if($arrdata['urlFoto']['url'] != ""){
        // 	$this->tulis($x+$lebarnomor+5,$yttd+10,$arrdata['urlFoto']['url'],'', '','I', $this->huruf, '', $this->ukuranhuruf);
        // }
        #qrcode
        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 80), ($this->pdf->GetY() - 15), 20); #Coba diganti ganti aja 30 dan 15 nya.
        // if (count($arrdata['tembusan']) > 0) {
        // 	$this->tulis($x, '', 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf-2);
        // 	for ($i=0; $i < count($arrdata['tembusan']) ; $i++) { 
        // 		$this->tulis($x, '+4', $arrdata['tembusan'][$i]['urutan'].'.', 'L', $lebarnomor, '', $this->huruf, '', 2);
        // 		$this->tulis(($x + 5), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength-$kanan), '', $this->huruf, '', 2);
        // 	}
        // }

        $this->pdf->Ln(20);
        if (count($arrdata['tembusan']) > 0) {
            $this->tulis($x, '+', 'Tembusan', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'U', 8);
            for ($i = 0; $i < count($arrdata['tembusan']); $i++) {
                $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'] . '.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis($x + 5, 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf);
            }
        }

        $this->tulis($x, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui sistem SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'I', 7);
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);

        // $this->pdf->AddPage();
        // $this->tulis($x, '+0', 'Lampiran Surat No', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        // $this->tulis($x + 35, '+0', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        // $this->tulis($x+40, '+0', $arrdata['row']['no_izin'], 'L', $dflength, '', 'Arial', '', 10);
        // $this->tulis($x, '+5', 'Tanggal', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        // $this->tulis($x + 35, '+0', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        // $this->tulis($x+40, '+0', $arrdata['row']['izin'], 'L', $dflength, '', 'Arial', '', 10);
        // $this->pdf->Ln(10);
        // $this->tulis($x+ 25, '+10', 'Data Penambahan Event di '.$arrdata['row']['lokasi_pameran'].'-'.$arrdata['row']['prop_pameran'].', tahun '.date('Y'), 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'B', 12);
        //$this->tulis($x + 60, '', $arrdata['row']['prop_pameran'].', tahun '.date('Y'), 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'B', 12);
        // $this->tulis($x+40, '+0', $arrdata['row']['prop_pameran'].', tahun '.date('Y'), 'L', $dflength, '', 'Arial', '', 10);
        // $this->tulis($x, '+5', ' No ', 'C', 10, '', 'Arial', 'B', 10,1,10);
        // $this->tulis($x + 10, 0, ' Name Of Event ', 'C', 70, '', 'Arial', 'B', 10,1,10);
        // $this->tulis($x + 80, 0, ' Organizer ', 'C', 40, '', 'Arial', 'B', 10,1,10);
        // $this->tulis($x + 120, 0, ' Mulai ', 'C', 30, '', 'Arial', 'B', 10,1,10);
        // $this->tulis($x + 150, 0, ' Berakhir ', 'C', 30, '', 'Arial', 'B', 10,1,10);
        // $no=1;
        // for($i=0;$i<count($arrdata['event']);$i++){
        // 	$this->tulis($x, '+10', $no, 'C', 10, '', 'Arial', '', 10,1,10);
        // 	$this->tulis($x + 10, 0, $arrdata['event'][$i]['nama'], 'C', 70, '', 'Arial', '', 10,1,10);
        // 	$this->tulis($x + 80, 0, $arrdata['event'][$i]['organizer'], 'C', 40, '', 'Arial', '', 10,1,10);
        // 	$this->tulis($x + 120, 0, $arrdata['event'][$i]['mulai'], 'C', 30, '', 'Arial', '', 10,1,10);
        // 	$this->tulis($x + 150, 0, $arrdata['event'][$i]['berakhir'], 'C', 30, '', 'Arial', '', 10,1,10);
        // $no++;	
        // }
        // $this->pdf->Ln(5);
        //       $this->pdf->SetX(20);
        //       $this->SetWidths(array(10, 40, 40, 40, 40));
        //       $this->SetAligns(array(C, C, C, C, C));
        //       $this->pdf->SetFont('', 'B', '');
        //       $datas = array('NO', 'Name Of Event', 'Organizer', 'Mulai', 'Berakhir');
        //       $this->SetAligns(array(L, L, L));
        //       $this->Row($datas);
        $no = 1;
        //    for($i=0;$i<14;$i++){
        //     $this->pdf->SetFont('', '', '');
        //     $datas = array($no, $arrdata['event'][$i]['nama'], $arrdata['event'][$i]['organizer'], $arrdata['event'][$i]['mulai'], $arrdata['event'][$i]['berakhir']);
        //     $this->pdf->SetX(20);
        //     $this->Row($datas);
        //     $no++;
        // }
        // $this->tulis($x + 115, '+', $arrdata['row']['jabatan_ttd'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
        // //$this->tulis($x + 115, '+4', 'Bina Usaha Perdagangan', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
        // $this->pdf->Ln(7);
        // $this->tulis($x + 140, '+4', 'TTD', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
        // $this->pdf->Ln(7);
        // $this->tulis($x + 115, '+4', $arrdata['row']['nama_ttd'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
        $banyakEvent = count($arrdata['event']);
        $event = $banyakEvent / 14;
        $event = ceil($event);
        $event = $event - 1;
        //       if ($event == 0) {
        //           $this->tulis($x + 75, '+7', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);
        //           if ($arrdata['row']['jabatan_ttd'] == '') {
        //                   $this->tulis($x + 75, '+4', $arrdata['ttd']['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        //           }else{
        //               $this->tulis($x + 75, '+4', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        //           }
        //           $this->tulis($x + 75, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 15, 0, 15);
        //           if ($arrdata['row']['nama_ttd'] == '') {
        //               $this->tulis($x + 75, '+15', $arrdata['ttd']['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        //           }else{
        //               $this->tulis($x + 75, '+15', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        //           }
        //           $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        //           $this->qrcode->genQRcode($string, 'L');
        //           $this->qrcode->disableBorder();
        //           $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 80), ($this->pdf->GetY() - 15), 20); #Coba diganti ganti aja 30 dan 15 nya.
        //       }else{
        //           $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        //           $this->qrcode->genQRcode($string, 'L');
        //           $this->qrcode->disableBorder();
        //           $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 80), ($this->pdf->GetY() + 15), 20); #Coba diganti ganti aja 30 dan 15 nya.
        //       }
        // $this->tulis($x, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui sistem SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'I', 7);
        // $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        /*

          #Tembusan
          $this->tulis($x, '+10','','L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          for ($i=0; $i < count($arrdata['tembusan']) ; $i++) {
          $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'].'.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf);
          }
          }

          /*
          $this->tulis($x, '+8', 'TANDA PENDAFTARAN', 'C', $dflength, '', 'Arial', 'B', 20);
          $this->tulis($x, '+8', 'PETUNJUK PENGGUNAAN(MANUAL) DAN KARTU JAMINAN/GARANSI PURNA JUAL' , 'C', $dflength, '', 'Arial', 'B', 12);
          $this->tulis($x, '+8', 'DALAM BAHASA INDONESIA BAGI PRODUK TELEMATIKA DAN ELEKTRONIKA', 'C', $dflength, '', 'Arial', 'B', 12);
          $this->tulis($x, '+8', 'PRODUKSI DALAM NEGERI', 'C', $dflength, '', 'Arial', 'B', 12);
          $this->pdf->Ln(8);

          $this->tulis(($x + 18), 0, 'NOMOR    :  P.32232323232323232323', 'C', $dflength - 30, '', 'Arial', 'B', 10,1,8);
          $this->pdf->Ln(13);

          $this->tulis($x + 55, '-5', 'DIBERIKAN KEPADA :' , '', 70, '', 'Arial', '', 10,0,13);
          $this->pdf->Ln(5);
          #nama perusahaan
          $this->tulis($x + 1, 0, '1.	NAMA PERUSAHAAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nm_perusahaan']) , '', 70, '', 'Arial', 'B', 10,0,13);

          #alamat
          $this->tulis($x + 1, '+5', '    ALAMAT ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', ucfirst($arrdata['row']['almt_perusahaan']) , '', 70, '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '+4', 'Kel. '.$arrdata['row']['kel_perusahaan'].', Kec. '.$arrdata['row']['kec_perusahaan'].', '.$arrdata['row']['prop_perusahaan'] , '', 70, '', 'Arial', '', 10,0,13);


          #tlp/fax
          $this->tulis($x + 1, '+5', '    NO. TELP / FAX ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', $arrdata['row']['telp'].' / '.$arrdata['row']['fax'] , '', 70, '', 'Arial', '', 10,0,13);

          $this->pdf->Ln(8);
          #nama pj
          $this->tulis($x + 1, '+', '2.	NAMA PENANGGUNG JAWAB ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nama_pj']) , '', 70, '', 'Arial', 'B', 10,0,13);
          #jabatan
          $this->tulis($x + 1, '+4', '    JABATAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '0', $arrdata['row']['jabatan_pj'] , '', 70, '', 'Arial', '', 10,0,13);


          $this->pdf->Ln(10);
          $this->tulis($x + 1, '+5', 'Telah mendaftarkan Petunjuk Penggunaan(Manual) dan Kartu Jaminan/Garansi Purna Jual dalam Bahasa Indonesia, sebagai berikut :', 'L', $dflength, 'M', 'Arial', '', 10,0);
          $this->tulis($x, '+5', ' JENIS PRODUK ', 'C', 85, '', 'Arial', 'B', 10,1,10);
          $this->tulis($x + 85, 0, ' MERK ', 'C', 30, '', 'Arial', 'B', 10,1,10);
          $this->tulis($x + 115, 0, ' TIPE MODEL ', 'C', 70, '', 'Arial', 'B', 10,1,10);



          $this->pdf->Ln(15);
          #footer
          #terbit
          $this->tulis($x + 105, '+', 'Diterbitkan di ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
          $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 137, '0', 'J A K A R T A' , '', 70, '', 'Arial', 'B', 10,0,13);
          #tanggal
          $this->tulis($x + 105, '+4', 'pada tanggal ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
          $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 137, '0', '   OKTOBER 2015' , '', 70, '', 'Arial', 'B', 10,0,13);

          $this->pdf->Ln(10);
          $this->tulis($x + 75, '+', 'a.n. MENTRI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
          $this->tulis($x + 75, '+4', 'Direktur Bina Usaha Perdagangan', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);

          $this->pdf->Ln(20);
          $this->tulis($x + 55, '+', 'Fetnayeti', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);


          if(count($arrdata['tembusan']) > 0){
          #Tembusan
          $this->tulis($x, '+10','','L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
          for ($i=0; $i < count($arrdata['tembusan']) ; $i++) {
          $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'].'.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf);
          }
          }

         */

        $b = 14;
        // if ($no > 14) {
        // for ($a=0; $a < $banyakEvent; $a++) {
        $terusan = $banyakEvent - 14;
        $this->pdf->AddPage();
        $this->pdf->SetFont('Arial', '', 10);
        
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 80), ($this->pdf->GetY() +210), 20); #Coba diganti ganti aja 30 dan 15 nya.
        $this->tulis($x, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui sistem SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'I', 7);
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        $this->tulis($x, '+0', 'Lampiran Surat No', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        $this->tulis($x + 35, '+0', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        $this->tulis($x + 40, '+0', $arrdata['row']['no_izin'], 'L', $dflength, '', 'Arial', '', 10);

        $this->tulis($x, '+5', 'Tanggal', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        $this->tulis($x + 35, '+0', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        $this->tulis($x + 40, '+0', $arrdata['row']['izin'], 'L', $dflength, '', 'Arial', '', 10);
        $this->pdf->Ln(10);
        if ($arrdata['row']['status'] != '1000') {
                $this->pdf->SetTextColor(219, 219, 219);
                $this->pdf->SetFont('Arial', 'B', 60);
                $this->RotatedText(100, 200, "D    R    A    F    T", 35); 
                $this->pdf->SetTextColor(000, 000, 000);
            }
        $this->tulis($x + 25, '+10', 'Data Penambahan Event di ' . $arrdata['row']['lokasi_pameran'] . '-' . $arrdata['row']['prop_pameran'] . ', tahun ' . date('Y'), 'C', ($dflength - $lebarstaticpendaftaran), 'M', 'Arial', 'B', 12);
        $this->pdf->Ln(5);
        $this->pdf->SetX(15);
        $this->SetWidths(array(10, 40, 40, 40, 40));
        $this->SetAligns(array(C, C, C, C, C));
        $this->pdf->SetFont('Arial', 'B', 10);
        $datas = array('No', 'Name Of Event', 'Organizer', 'Mulai', 'Berakhir');
        $this->SetAligns(array(L, L, L));
        $this->Row($datas);
        // if ($terusan > 14) {
        $this->pdf->SetFont('Arial', '', 10);
        for ($i = 0; $i < $banyakEvent; $i++) {
            $this->pdf->SetFont('', '', '');
            $datas = array($no, $arrdata['event'][$i]['nama'], $arrdata['event'][$i]['organizer'], $arrdata['event'][$i]['mulai'], $arrdata['event'][$i]['berakhir']);
            // $this->pdf->SetX(20);
            $this->Row($datas, $string);
            $no++;
            $b++;
            $fl = '';
            if ($no == "32" && $arrdata['row']['id'] == '161') {
                $fl = $i;
                break;
            }
        }
        if ($no == "32" && $arrdata['row']['id'] == '161') {
            $this->pdf->AddPage();
            $this->pdf->SetFont('Arial', '', 10);
            $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 80), ($this->pdf->GetY() +210), 20);
            $this->tulis($x, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui sistem SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'I', 7);
            $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);

            $this->pdf->Ln(5);
            $this->pdf->SetX(15);
            $this->SetWidths(array(10, 40, 40, 40, 40));
            $this->SetAligns(array(C, C, C, C, C));
            $this->pdf->SetFont('Arial', 'B', 10);
            $datas = array('No', 'Name Of Event', 'Organizer', 'Mulai', 'Berakhir');
            $this->SetAligns(array(L, L, L));
            $this->Row($datas);
            // if ($terusan > 14) {
            $this->pdf->SetFont('Arial', '', 10);
            for ($k = $fl; $k < $banyakEvent; $k++) {
                $this->pdf->SetFont('', '', '');
                $datas = array($no, $arrdata['event'][$k]['nama'], $arrdata['event'][$k]['organizer'], $arrdata['event'][$k]['mulai'], $arrdata['event'][$k]['berakhir']);
                // $this->pdf->SetX(20);
                $this->Row($datas, $string);
                $no++;
                $b++;
                $fl = '';
            }
        }
        // }else{
        //     for($i=0;$i<$terusan;$i++){
        //         $this->pdf->SetFont('', '', '');
        //         $datas = array($no, $arrdata['event'][$b]['nama'], $arrdata['event'][$b]['organizer'], $arrdata['event'][$b]['mulai'], $arrdata['event'][$b]['berakhir']);
        //         $this->pdf->SetX(20);
        //         $this->Row($datas);
        //         $no++;
        //         $b++;
        //     }
        // }
        $this->pdf->Ln(10);
        $this->tulis($x + 75, '+7', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);

        if ($arrdata['row']['jabatan_ttd'] == '') {
            $this->tulis($x + 75, '+4', $arrdata['ttd']['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 75, '+4', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }

        $this->tulis($x + 75, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 15, 0, 15);

        if ($arrdata['row']['nama_ttd'] == '') {
            $this->tulis($x + 75, '+15', $arrdata['ttd']['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 75, '+15', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }
    }

    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */

    function generate($arrdata) {
        //$this->pdf->Output('./pdf/'.date('YmdHis').'.pdf','F');
        if ($arrdata['cetak'] != '') {
            $namafile = $arrdata['namafile'];
            $dir = $arrdata['dir'];
            if (file_exists($dir) && is_dir($dir)) {
                $path = 'upL04d5/document/DOKPMRN/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
            } else {
                if (mkdir($dir, 0777, true)) {
                    if (chmod($dir, 0777)) {
                        $path = 'upL04d5/document/DOKPMRN/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
                    }
                }
            }
            $this->pdf->Output($path, 'F');
        } else {
            $this->pdf->Output();
        }
    }

    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}

?>