<?php

error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $def_margin = array(15, 50, 15, 15);
    //////////////////////////////////////////////////////
    var $widths;
    var $aligns;
    var $angle=0;

    function Rotate($angle,$x=-1,$y=-1)
    {
	if($x==-1)
		$x=$this->x;
	if($y==-1)
		$y=$this->y;
	if($this->angle!=0)
		$this->pdf->_out('Q');
	$this->angle=$angle;
	if($angle!=0)
	{
		$angle*=M_PI/180;
		$c=cos($angle);
		$s=sin($angle);
		$cx=$x*$this->k;
		$cy=($this->h-$y)*$this->k;
		$this->pdf->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
	}
    }
    
    function _endpage()
    {
	if($this->angle!=0)
	{
		$this->angle=0;
		$this->pdf->_out('Q');
	}
	parent::_endpage();
    }

    function pdf($orientasi, $ukuran, $kertas, $margins, $data) {
        $this->ukuranhuruf = 12;
        $this->ukuranhurufdetil = 10;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->pdf->footer = 1;
        $this->qrcode = new QRcode();


        if (is_array($margins) and count($margins) == 4) {
            $this->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
        } else {

            $this->pdf->SetMargins($this->def_margin[0], $this->def_margin[1], $this->def_margin[2], $this->def_margin[3]);
        }
        $this->data($data, $margins);
    }

    function Footer() {
        if ($this->pdf->footer == 1) {
            #$this->pdf->SetY(-45);
            $this->pdf->SetFont('Arial', '', 9);
            $Yget2 = $this->pdf->GetY();
            $this->pdf->SetY($Yget2);
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
        }
    }

    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */

    function NewIkd() {
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function NewIkdLandscape() {
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }

    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }

    function FormatNPWP($varnpwp) {
        $varresult = '';
        $varresult = substr($varnpwp, 0, 2) . "." . substr($varnpwp, 2, 3) . "." . substr($varnpwp, 5, 3) . "." . substr($varnpwp, 8, 1) . "-" . substr($varnpwp, 9, 3) . "." . substr($varnpwp, 12, 3);
        return $varresult;
    }

    function FormatTDP($varnoijin) {
        $varresult = '';
        $varresult = substr($varnoijin, 0, 2) . "." . substr($varnoijin, 2, 2) . "." . substr($varnoijin, 4, 1) . "." . substr($varnoijin, 5, 2) . "." . substr($varnoijin, 7, 5);
        return $varresult;
    }
    
    function RotatedText($x, $y, $txt, $angle) {
        //Text rotated around its origin
        $this->Rotate($angle, $x, $y);
        $this->pdf->Text($x, $y, $txt);
        $this->Rotate(0);
    }

    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */

    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */

    function SetWidths($w) {
        //Set the array of column widths
        $this->pdf->widths = $w;
    }

    function SetAligns($a) {
        //Set the array of column alignments
        $this->pdf->aligns = $a;
    }

    function Row($data) {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->pdf->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed

        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->pdf->widths[$i];
            $a = isset($this->pdf->aligns[$i]) ? $this->pdf->aligns[$i] : 'L';
            //Save the current position
            $x = $this->pdf->GetX();
            $y = $this->pdf->GetY();
            //Draw the border
            $this->pdf->Rect($x, $y, $w, $h);
            //Print the text
            $this->pdf->MultiCell($w, 5, $data[$i], 0, $a);
            //Put the position to the right of the cell
            $this->pdf->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->pdf->Ln($h);
    }

    function CheckPageBreak($h) {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->pdf->GetY() + $h > $this->pdf->PageBreakTrigger)
            $this->pdf->AddPage($this->pdf->CurOrientation);
    }

    function NbLines($w, $txt) {
        //Computes the number of lines a MultiCell of width w will take
        $cw = &$this->pdf->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->pdf->rMargin - $this->pdf->x;
        $wmax = ($w - 2 * $this->pdf->cMargin) * 1000 / $this->pdf->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l+=$cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }

    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 3.7) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);
        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);
        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
            $this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }

    function mcell($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 10) {
        //global $this->tg;
        $this->pdf->SetFont($huruf, $model, $ukuran);
        $this->pdf->MultiCell($lebar, $tinggi, $teks, 1, $align, 0);
    }

    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */

    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */

    /////////////////////////////////
    function data($arrdata, $margin) {
        $this->NewIkd();
        $dflength = 174;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarstaticpendaftaran = 54;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        }
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        $x = $kiri;
        if ($arrdata['row']['status'] != '1000') {
                $this->pdf->SetTextColor(219, 219, 219);
                $this->pdf->SetFont('Arial', 'B', 60);
                $this->RotatedText(100, 200, "D    R    A    F    T", 35); 
                $this->pdf->SetTextColor(000, 000, 000);
            }
        for ($t = 0; $t < count($arrdata['p']); $t++) {
            $this->tulis($x, '+8', 'TANDA PENDAFTARAN', 'C', $dflength, '', 'Arial', 'B', 20);
            $this->tulis($x, '+8', 'PETUNJUK PENGGUNAAN(MANUAL) DAN KARTU JAMINAN/GARANSI PURNA JUAL', 'C', $dflength, '', 'Arial', 'B', 12);
            $this->tulis($x, '+8', 'DALAM BAHASA INDONESIA BAGI PRODUK TELEMATIKA DAN ELEKTRONIKA', 'C', $dflength, '', 'Arial', 'B', 12);
            if ($arrdata['pabrik'] > 0) {
                if ($arrdata['jensi_garansi'] == '01') {
                    $this->tulis($x, '+8', 'PRODUSEN', 'C', $dflength, '', 'Arial', 'B', 12);
                }else{
                    $this->tulis($x, '+8', 'IMPORTIR', 'C', $dflength, '', 'Arial', 'B', 12);
                }
            }else{
                $this->tulis($x, '+8', 'PRODUKSI DALAM NEGERI', 'C', $dflength, '', 'Arial', 'B', 12);
            }
            
            $this->pdf->Ln(8);

            $this->tulis(($x + 18), 0, 'NOMOR    :  ' . $arrdata['row']['no_izin'], 'C', $dflength - 30, '', 'Arial', 'B', 10, 1, 8);
            $this->pdf->Ln(13);
            #Border Nomor Telepon
            //$this->pdf->Ln(5);
            #Border Atas
            //$this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext, $x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 11);
            //$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());

            /*
              $this->pdf->SetFont('Arial','',10);
              $this->pdf->SetFillColor(30,144,255);
              $this->pdf->MultiCell(0,8,"		NOMOR                                         :   //STP/9191919191911919",1,1,true);

              $this->pdf->SetFont('Arial','',10);
              $this->pdf->SetFillColor(30,144,255);
              $this->pdf->MultiCell(0,8,"		BERLAKU SAMPAI DENGAN       :   2020",1,1,true);
             */
            //$this->pdf->tulis($x + 0.5, 0, '  NOMOR ', 'C', 180, '', 'Arial', '', 10,1,10);
            //$this->pdf->tulis($x + 55, '-1', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
            //$this->tulis($x + 60, '0', 'FAX//1234567' , 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
            #Border Kegiatan Usaha
            //$this->pdf->Ln();
            //$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());

            /*
              $this->tulis($x + 0.5, '+11', '  BERLAKU SAMPAI DENGAN', 'L', 180, '', 'Arial', '', 10,1,10);
              $this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
              $this->tulis($x + 60, '+5', 'MARET 2020', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
              //$this->pdf->Ln(8);
              //$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
              $this->pdf->Ln(10);

             */
            $this->tulis($x + 55, '-5', 'DIBERIKAN KEPADA :', '', 70, '', 'Arial', '', 10, 0, 13);
            $this->pdf->Ln(5);
            #nama perusahaan
            $this->tulis($x + 1, 0, '1.	NAMA PERUSAHAAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nm_perusahaan']), '', 70, '', 'Arial', 'B', 10, 0, 13);

            #alamat
            $this->tulis($x + 1, '+5', '    ALAMAT ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 55, '0', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 60, '+4', ucfirst(trim($arrdata['row']['almt_perusahaan'])), 'J', 130, 'M', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 60, '+0', 'Kel. ' . trim($arrdata['row']['kel_perusahaan']) . ', Kec. ' . trim($arrdata['row']['kec_perusahaan']) . ', ' . trim($arrdata['row']['prop_perusahaan']), '', 50, 'M', 'Arial', '', 10, 0, 13);

            if ($arrdata['row']['status_dok'] == '02') {
                $this->pdf->SetTextColor(112, 121, 125);
                $this->pdf->SetFont('Arial', 'B', 60);
                $this->RotatedText(110, 260, "DICABUT", 0);   
                $this->pdf->SetTextColor(000, 000, 000);
            }

            #tlp/fax
            $this->tulis($x + 1, '+0', '    NO. TELP / FAX ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 60, '0', $arrdata['row']['telp'] . ' / ' . $arrdata['row']['fax'], '', 70, '', 'Arial', '', 10, 0, 13);

            $this->pdf->Ln(8);
            #nama pj
            $this->tulis($x + 1, '+', '2.	NAMA PENANGGUNG JAWAB ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nama_pj']), '', 70, '', 'Arial', 'B', 10, 0, 13);
            #jabatan
            $this->tulis($x + 1, '+4', '    JABATAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 60, '0', $arrdata['row']['jabatan_pj'], '', 70, '', 'Arial', '', 10, 0, 13);

            if (count($arrdata['pabrik']) > 0) {
                $no = 1;
                $alp = array('', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
                $this->pdf->Ln(8);
                #nama pj
                $this->tulis($x + 1, '+', '3.	SUPPLIER ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                foreach ($arrdata['pabrik'] as $kuy) {
                    #jabatan
                    $this->tulis($x + 10, '+4', $alp[$no] . '.    NAMA SUPPLIER ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                    $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                    $this->tulis($x + 60, '0', $kuy['nama_pabrik'], '', 70, '', 'Arial', 'B', 10, 0, 13);

                    $this->tulis($x + 10, '+5', '       ALAMAT SUPPLIER ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                    $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                    $this->tulis($x + 60, '+4', ucfirst(trim($kuy['alamat'])), 'J', 100, 'M', 'Arial', '', 10, 0, 13);

                    $this->tulis($x + 10, '-4', '       NO TELEPON ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                    $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                    $this->tulis($x + 60, '0', $kuy['telp'], '', 70, '', 'Arial', '', 10, 0, 13);
                    $no++;
                }
            }

            $this->pdf->Ln(10);
            $this->tulis($x + 1, '+5', 'Telah mendaftarkan Petunjuk Penggunaan(Manual) dan Kartu Jaminan/Garansi Purna Jual dalam Bahasa Indonesia, sebagai berikut :', 'L', $dflength, 'M', 'Arial', '', 10, 0);
            //if(count($arrdata['p']) != 0 || count($arrdata['p']) < 7){
            /*   $this->tulis($x, '+5', ' JENIS PRODUK ', 'C', 85, '', 'Arial', 'B', 10, 1, 10);
              $this->tulis($x + 85, 0, ' MERK ', 'C', 30, '', 'Arial', 'B', 10, 1, 10);
              $this->tulis($x + 115, 0, ' TIPE MODEL ', 'C', 70, '', 'Arial', 'B', 10, 1, 10);

              $mod = 0;
              $len = strlen($arrdata['p'][$t]['tipe']); */
            // if($len < 34){
            // 	$mod = 34-$len;
            // }else{
            // 	$mod = ($len-34) + (34 - ($len-34));
            // }
            // if($len == 34){
            // 	$mod=0;
            // }
            // for($q=0;$q<$mod;$q++){
            // 	$rf.="=";
            // }
            //print_r($mod.'x');die();
            if (strlen($arrdata['p'][$t]['tipe']) >= '27') {
                $a = '27';
                $hasil = strlen($arrdata['p'][$t]['tipe']) / (int) $a;
                $q = substr($hasil, 0, 1);
                $q = (int) $q + 1;
                $q = $q * 10;
            }
        
            $arrmerk = explode("<br>", $arrdata['p'][$t]['merk']);
            foreach ($arrmerk as $merk_fix) {
                $merk_aja = $merk_fix.'\n';
            }
            $this->pdf->Ln(5);
            $this->pdf->SetX(20);
            $this->SetWidths(array(50, 60, 60));
            $this->SetAligns(array(C, C, C));
            $this->pdf->SetFont('', 'B', '');
            $datas = array('JENIS PRODUK', 'MERK', 'TIPE / MODEL');
            $this->SetAligns(array(L, L, L));
            $this->Row($datas);
            $this->pdf->SetFont('', '', '');
            $datas = array($arrdata['p'][$t]['produk'], str_replace("<br>", "\n", trim($arrdata['p'][$t]['merk'])) , str_replace("<br>", "\n", $arrdata['p'][$t]['tipe']));
            $this->pdf->SetX(20);
            $this->Row($datas);
            //    $this->tulis($x, '+10', $arrdata['p'][$t]['produk'], 'C', 85, '', 'Arial', 'B', 10, 1, $q);
            //    $this->tulis(($x) + 85, 0, $arrdata['p'][$t]['merk'], 'C', 30, '', 'Arial', 'B', 10, 1, $q);
            //    $this->mcell(($x) + 115, 0, $arrdata['p'][$t]['tipe'] . "dsadsadsdada" . $rf, 'C', 70, '', 'Arial', 'B', 10, 1);
            //    $this->mcell(($x) + 115, 0, $arrdata['p'][$t]['tipe'] . "dsadsadsdada" . $rf, 'C', 70, '', 'Arial', 'B', 10, 1);
            //    $this->mcell(($x) + 115, 0, $arrdata['p'][$t]['tipe'] . "dsadsadsdada" . $rf, 'C', 70, '', 'Arial', 'B', 10, 1);
            $rf = "";
            $mod = null;
            //$len=strlen($arrdata['p'][$t]['tipe']); 
            //}
            /*
              for($i=0;$i<7;$i++){
              $this->tulis($x, '+10', $res['produk'], 'C', 80, '', 'Arial', 'B', 10,1,10);
              $this->tulis($x + 80, 0, $res['merk'], 'C', 30, '', 'Arial', 'B', 10,1,10);
              $this->tulis($x + 110, 0,  $res['tipe'], 'C', 70, '', 'Arial', 'B', 10,1,10);
              }
             */

            //$i=0;
            //	if(count($arrdata['p']) != 0 || count($arrdata['p']) < 7){
            //	foreach($arrdata['p'] as $res){
            //		$ex = explode(",",$res['tipe']);
            //		if(count($ex) != 0){
            //			for($s=0;$s<count($ex);$s++){
            /* $this->tulis($x, '+10', $res['produk'], 'C', 85, '', 'Arial', 'B', 10,1,10);
              $this->tulis(($x) + 85, 0, $res['merk'], 'C', 30, '', 'Arial', 'B', 10,1,10);
              $this->tulis(($x) + 115, 0, $ex[$s], 'C', 70, '', 'Arial', 'B', 10,1,10); */
            //			}
            //		}else{
            //			$this->tulis($x, '+10', $res['produk'], 'C', 85, '', 'Arial', 'B', 10,1,10);
            //			$this->tulis(($x) + 85, 0, $res['merk'], 'C', 30, '', 'Arial', 'B', 10,1,10);
            //			$this->tulis(($x) + 115, 0,  $res['tipe'], 'C', 70, '', 'Arial', 'B', 10,1,10);
            //		}	
            //		$i++;
            //		if($i == 6) break;
            //		}
            //	}else{
            //		$this->tulis($x, 0, ' TERLAMPIR ', 'C', 30, '', 'Arial', 'B', 10,0,10);
            //	}

            $this->pdf->Ln(5);
            #footer
            #terbit
            $this->tulis($x + 105, '+', 'Diterbitkan di ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);
            $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);
            $this->tulis($x + 137, '0', 'J A K A R T A', '', 70, '', 'Arial', 'B', 10, 0);
            #tanggal
            $this->tulis($x + 105, '+4', 'pada tanggal ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 00);
            $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);
            $this->tulis($x + 137, '0', $arrdata['row']['izin'], '', 70, '', 'Arial', 'B', 10, 0);

            $this->tulis($x + 75, '+7', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);

            if ($arrdata['row']['jabatan_ttd'] == '') {
                    $this->tulis($x + 75, '+1', $arrdata['ttd']['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            }else{
                $this->tulis($x + 75, '+1', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            }
            
            $this->tulis($x + 75, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 20, 0, 15);

            if ($arrdata['row']['nama_ttd'] == '') {
                $this->tulis($x + 75, '+15', $arrdata['ttd']['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            }else{
                $this->tulis($x + 75, '+15', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            }
            
            // $this->tulis($x + 75, '+4', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);
            // $this->tulis($x + 75, '+4', 'Bina Usaha Perdagangan', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);
            // $this->tulis($x + 75, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 20, 0);
            // $this->tulis($x + 75, '+10', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);

            $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
            $this->qrcode->genQRcode($string, 'L');
            $this->qrcode->disableBorder();
            $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 70), ($this->pdf->GetY() - 25), 25); #Coba diganti ganti aja 30 dan 15 nya.

            // $this->pdf->Ln(5);
            if (count($arrdata['tembusan']) > 0) {
                $this->tulis($x, '+10', '', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf - 4);
                for ($i = 0; $i < count($arrdata['tembusan']); $i++) {
                    $this->tulis($x, '+4', $arrdata['tembusan'][$i]['urutan'] . '.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 4);
                    $this->tulis($x + 3.2, 0, $arrdata['tembusan'][$i]['keterangan'] . ';', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 4);
                }
            }
            $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
            $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
            //}
            //}
            #qrcode


            /*

              $this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext, $x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 11);
              $this->tulis($x, 0, 'NOMOR TDP', 'C', (($dflength - $lebarstaticpendaftaran) / 2), '', 'Arial', '', 9);
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), 0, 'BERLAKU S/D TANGGAL', 'C', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);


              $this->pdf->Line($x + ($dflength - $lebarstaticpendaftaran), $YNext, $x + ($dflength - $lebarstaticpendaftaran), $YNow + 11);
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 1.5), '', 'PENDAFTARAN  : '.$arrdata['row']['pendaftaran'], '', 54, '', 'Arial', '', 9);
              $this->pdf->Ln(6);

              #Nomor TDP
              $this->tulis($x, 0, $this->FormatTDP($arrdata['row']['no_ijin']), 'C', (($dflength - $lebarstaticpendaftaran) / 2), '', 'Arial', 'B', 11);

              #Tanggal TDP
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), 0, $this->iif($arrdata['row']['tgl_akhir'], strtoupper($this->convert_date($arrdata['row']['tgl_akhir']))), 'C', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', 'B', 11);
              $this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran)), $YNext + 6.3, $x + $dflength, $YNext + 6.3);

              #PEMBAHARUAN
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 1.5), '', 'PEMBAHARUAN : '.($arrdata['row']['pembaharuan'] == 0 ? '-' : $arrdata['row']['pembaharuan']), '', 54, '', 'Arial', '', 9);


              $YSec = $this->pdf->GetY();
              $this->pdf->SetY($YSec);
              $YNow = $this->pdf->GetY();
              $this->pdf->Line($x, $YNext, ($x + $dflength), $YNext);
              $this->pdf->Line($x, $YNext, $x, $YNow + 5);
              $this->pdf->Line(($x + $dflength), $YNext, ($x + $dflength), $YNow + 5);
              $this->pdf->Line($x, $YNow + 5, ($x + $dflength), $YNow + 5);
              $this->pdf->Ln(10);

              #Border Atas

              $ystartpoint = $this->pdf->GetY();

              $this->pdf->Line($x, $ystartpoint, ($x + $dflength), $ystartpoint);
              $this->tulis($x + 1.5, '+3', 'NAMA PERUSAHAAN : ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 9);
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '', 'STATUS : ', '', 41.5, '', 'Arial', '', 9);
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '+6', strtoupper($arrdata['row']['ur_status_perusahaan']), '', 41.5, '', 'Arial', '', 10);
              $this->pdf->newFlowingBlock(($dflength - $lebarstaticpendaftaran), 5, 'W', 'J', 0, false);
              $this->tulis($x + 1.5, '', strtoupper($arrdata['row']['nama']), 'L', 10, 'W', 'Arial', 'B',13);
              $this->pdf->finishFlowingBlock();
              $this->pdf->Ln(6);
              $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
              $this->tulis($x + 1.5, 0, 'PENANGGUNG JAWAB / PENGURUS : '.strtoupper($arrdata['row']['nama_pemilik']), 'L', ($dflength - 1.5), '', 'Arial', '', 10, 0, 13);

              #Border Alamat
              $this->pdf->Ln();
              $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
              $this->tulis($x + 1.5, '+3', 'ALAMAT : ', 'L', 25, '', 'Arial', '', 9);
              $rowalamat = strtoupper($arrdata['row']['alamat']);
              $kelurahan = 'KEL';
              $pos=strpos($rowalamat,$kelurahan);
              if($pos !== false){
              $alamat1=explode('KEL.',$alamat);
              $this->tulis($x + 20, 0, $alamat1[0], 'L', 5, '', 'Arial', '', 10);
              $this->tulis($x + 25, 0, $alamat1[1], 'L', ($dflength - 20), 'M', 'Arial', '', 10);
              }else{
              $this->tulis($x + 20, 0, strtoupper($arrdata['row']['alamat']), 'L', ($dflength - 20), 'M', 'Arial', '',10);
              $this->tulis($x + 20, '+1', 'KEL. '. strtoupper($arrdata['row']['ur_kel']).' KEC. '.strtoupper($arrdata['row']['ur_kec']), 'L', ($dflength - 20), 'M', 'Arial', '', 10);
              $this->tulis($x + 20, '+1', strtoupper($arrdata['row']['urai_kota']).' '.$arrdata['row']['ur_pos'], 'L', ($dflength - 20), '', 'Arial', '',10);
              $this->pdf->Ln(2);
              }

              #Border NPWP
              $this->pdf->Ln();
              $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
              $this->tulis($x + 1.5, 0, 'NPWP   :   '.$this->FormatNPWP($arrdata['row']['npwp']), 'L', ($dflength - 1.5), '', 'Arial', '', 10, 0, 13);

              #Border Nomor Telepon
              $this->pdf->Ln();
              $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
              $this->tulis($x + 1.5, 0, 'NOMOR TELEPON :         '.$arrdata['row']['telepon'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - 14), '', 'FAX :          '.$arrdata['row']['fax'], '', 70, '', 'Arial', '', 10,0,13);


              #Border Kegiatan Usaha
              $this->pdf->Ln();
              $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());

              $this->tulis($x + 1.5, '+3', 'KEGIATAN USAHA POKOK : ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '', 'KBLI : ', 'C', 41.5, '', 'Arial', '', 10);

              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '+5', $arrdata['row']['kbli'], 'C', 41.5, '', 'Arial', '', 10);
              $this->tulis($x + 10, '', str_replace(chr(10),' ',strtoupper($arrdata['row']['pokok'])), 'J', ($dflength - $lebarstaticpendaftaran), 'M', 'Arial', '', 10);

              #Border Bawah Pisan
              $this->pdf->Ln();
              $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
              $this->pdf->Line($x, $ystartpoint, $x, $this->pdf->GetY());
              $this->pdf->Line(($x + $dflength), $ystartpoint, ($x + $dflength), $this->pdf->GetY());
              #End Border Bawah Pisan

              #Area Signature
              $this->pdf->Ln(8);
              $panjang = strlen($arrdata['row']['penerbit']);
              if ($panjang > 40){
              $mulai = 40;
              $minus = 5;
              }else{
              $mulai = 30;
              $minus = 0;
              }

              $this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '', 'Dikeluarkan di', '', 25, '', 'Arial', '', 10);
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran)), '', ':', '', 5, '', 'Arial', '', 10);
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', strtoupper($arrdata['row']['urai_kota']), '', 55, '', 'Arial', '', 10);

              $this->tulis($x + 1.5, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '', 'Pada Tanggal', '', 25, '', 'Arial', '', 10);
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran)), '', ':', '', 5, '', 'Arial', '', 10);
              $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', $this->iif($arrdata['row']['tgl_ijin'], strtoupper($this->convert_date($arrdata['row']['tgl_ijin']))), '', 55, '', 'Arial', '', 10);

              $this->pdf->Ln(8);
              $this->tulis($x + 1.5, '', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
              $this->tulis($x + (($dflength / 2) - ($panjang/2)), '', 'KEPALA '.strtoupper($arrdata['row']['penerbit']), 'C', (($dflength / 2) + ($panjang/2)), 'M', 'Arial', '', 10);

              $this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
              $this->tulis($x + (($dflength / 2) - 10-$minus), '', 'SELAKU', 'C', (($dflength / 2) + 10), 'M', 'Arial', '', 10);

              $this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
              $this->tulis($x + (($dflength / 2) - 10-$minus), '', 'KEPALA KANTOR PENDAFTARAN PERUSAHAAN', 'C', (($dflength / 2) + 10), 'M', 'Arial', '', 10);

              $this->pdf->Ln(34);
              $this->tulis($x + 1.5, '', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
              $this->tulis($x + (($dflength / 2) - 10-$minus), '', $arrdata['row']['nama_ttd'], 'C', (($dflength / 2) + 10), '', 'Arial', 'U', 10);

              $this->tulis($x + 1.5, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
              $this->tulis($x + (($dflength / 2) - 10-$minus), '', 'NIP. '. $arrdata['row']['nip_ttd'], 'C', (($dflength / 2) + 10), '', 'Arial', '', 10);
              #End Area Signature
             */
            /*
              if(count($arrdata['p']) > 7){

              $this->tulis($x, '+5', ' JENIS PRODUK ', 'C', 85, '', 'Arial', 'B', 10,1,10);
              $this->tulis($x + 85, 0, ' MERK ', 'C', 30, '', 'Arial', 'B', 10,1,10);
              $this->tulis($x + 115, 0, ' TIPE MODEL ', 'C', 70, '', 'Arial', 'B', 10,1,10);

              for($j=$i;$j<count($arrdata['p']);$j++){
              $this->tulis($x, '+10', $arrdata['p'][$j]['produk'], 'C', 85, '', 'Arial', 'B', 10,1,10);
              $this->tulis($x + 85, 0, $arrdata['p'][$j]['merk'], 'C', 30, '', 'Arial', 'B', 10,1,10);
              $this->tulis($x + 115, 0,  $arrdata['p'][$j]['tipe'], 'C', 70, '', 'Arial', 'B', 10,1,10);
              $i++;
              }
              }
             */
            // if ($t != (count($arrdata['p']) - 1)) {
            //     $this->pdf->AddPage();
            // }
        }
        /*
          $this->pdf->AddPage();
          $this->tulis($x, 0, 'Nomor', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $this->tulis($x + 17, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $this->tulis($x + 20, 0, $arrdata['row']['no_izin'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

          $this->tulis($x + 50, 0, 'Jakarta ' . $arrdata['row']['izin'], 'R', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

          $this->tulis($x, '+5', 'Lampiran', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $this->tulis($x + 17, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $this->tulis($x + 20, 0, count($arrdata['p']) . ' berkas', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

          $this->tulis($x, '+5', 'Hal', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $this->tulis($x + 17, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $this->tulis($x + 20, 0, 'Tanda Pendaftaran Petunjuk', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $this->tulis($x + 20, '+5', 'Penggunaan dan Kartu Jaminan', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

          $this->tulis($x, '+15', 'Yth. Direktur Utama', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $this->tulis($x, '+5', $arrdata['row']['nm_perusahaan'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $this->tulis($x, '+5', $arrdata['row']['almt_perusahaan'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $this->tulis($x, '+5', $arrdata['row']['prop_perusahaan'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $nmr = "";
          for ($u = 0; $u < count($arrdata['p']); $u++) {
          $nmr.= $arrdata['p'][$u]['izn'];
          if (count($arrdata['p']) - $u == 2) {
          $nmr.=" dan ";
          } else if (count($arrdata['p']) - ($u + 1) != 0) {
          $nmr.=",";
          }
          }

          $this->tulis($x + 1, '+15', '     Berdasarkan penelitian kami atas surat permohonan Saudara Nomor : ' . $arrdata['row']['no_aju'] . ', tanggal ' . $arradata['row']['tgl_kirim'] . ', prihal permohonan pendaftaran, yang kami terima tanggal ' . $arradata['row']['tgl_terima'] . ', dengan ini kami beritahukan bahwa permohonan Saudara untuk Pendaftaran Penggunaan (Manual) dan Kartu Jaminan/Garansi Purna Jual dapat kami penuhi dengan diterbitkannya Tanda Pendaftaran Nomor ' . $nmr . ' sebagaimana terlampir.', 'J', $dflength, 'M', 'Arial', '', 10, 0);

          $this->tulis($x + 1, '+4', '     Sehubungan dengan telah diterbitkannya Tanda Pendaftaran, bersama ini disampaikan hal-hal sebagai berikut :', 'J', $dflength, 'M', 'Arial', '', 10, 0);
          $this->tulis($x + 1, '+0', '1.', 'J', $dflength, '', 'Arial', '', 10, 0);
          $this->tulis($x + 5, '+0', 'Nomor Tanda Pendaftaran, wajib dicantumkan pada petunjuk penggunaan dan kartu garansi serta pada kemasan:', 'J', $dflength, 'M', 'Arial', '', 10, 0);
          $this->tulis($x + 1, '+0', '2.', 'J', $dflength, '', 'Arial', '', 10, 0);
          $this->tulis($x + 5, '+0', 'Pendaftaran berikutnya, agar membawa rekomendasi dari Dinas di bidang perdagangan yang telah diperoleh perusahaan dan membawa copy Tanda Pendaftaran terakhir yang telah dimiliki perusahaan beserta surat pengantarnya.', 'J', $dflength, 'M', 'Arial', '', 10, 0);
          $this->tulis($x + 5, '+4', '      Demikian atas perhatian Saudara kami ucapkan terima kasih', 'J', $dflength, 'M', 'Arial', '', 10, 0);

          $this->pdf->Ln(50);
          $this->tulis($x + 75, '+', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);
          $this->tulis($x + 75, '+4', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);
          $this->tulis($x + 75, '+4', 'Bina Usaha Perdagangan', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);

          $this->pdf->Ln(20);
          $this->tulis($x + 80, '+', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0);


          $this->pdf->Ln(20);
          if (count($arrdata['tembusan']) > 0) {
          $this->tulis($x, '+', 'Tembusan', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'U', 10);
          for ($i = 0; $i < count($arrdata); $i++) {
          $this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'] . '.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
          $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf);
          }
          }

          $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
          $this->qrcode->genQRcode($string, 'L');
          $this->qrcode->disableBorder();
          $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 80), ($this->pdf->GetY() - 40), 20);
          $this->pdf->Ln(13);
          $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
          $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui sistem SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'I', 7);

         */
    }

    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */

    function generate($arrdata) {
        if ($arrdata['cetak'] != '') {
            $namafile = $arrdata['namafile'];
            $dir = $arrdata['dir'];
            if (file_exists($dir) && is_dir($dir)) {
                $path = 'upL04d5/document/DOKGRNSI/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
            } else {
                if (mkdir($dir, 0777, true)) {
                    if (chmod($dir, 0777)) {
                        $path = 'upL04d5/document/DOKGRNSI/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
                    }
                }
            }
            $this->pdf->Output($path, 'F');
        } else {
            $this->pdf->Output();
        }
    }

    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}

?>