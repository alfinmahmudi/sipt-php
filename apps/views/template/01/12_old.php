<?php

error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $def_margin = array(15, 50, 15, 15);
    var $qrCodeString;

    function pdf($orientasi, $ukuran, $kertas, $margins, $data) {
        $this->ukuranhuruf = 12;
        $this->ukuranhurufdetil = 10;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->qrcode = new QRcode();
        $this->pdf->footer = 1;
        if (is_array($margins) and count($margins) == 4) {
            $this->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
        } else {

            $this->pdf->SetMargins($this->def_margin[0], $this->def_margin[1], $this->def_margin[2], $this->def_margin[3]);
        }
        $this->data($data, $margins);
    }

    function Footer() {
        if ($this->pdf->footer == 1) {
            #$this->pdf->SetY(-45);
            $this->pdf->SetFont('Arial', '', 9);
            $Yget2 = $this->pdf->GetY();
            $this->pdf->SetY($Yget2);
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
        }
    }

    function RotatedText($x, $y, $txt, $angle) {
        //Text rotated around its origin
        $this->pdf->Rotate($angle, $x, $y);
        $this->pdf->Text($x, $y, $txt);
        $this->pdf->Rotate(0);
    }

    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */

    function NewIkd() {
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function NewIkdLandscape() {
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }

    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }

    function FormatNPWP($varnpwp) {
        $varresult = '';
        $varresult = substr($varnpwp, 0, 2) . "." . substr($varnpwp, 2, 3) . "." . substr($varnpwp, 5, 3) . "." . substr($varnpwp, 8, 1) . "-" . substr($varnpwp, 9, 3) . "." . substr($varnpwp, 12, 3);
        return $varresult;
    }

    function FormatTDP($varnoijin) {
        $varresult = '';
        $varresult = substr($varnoijin, 0, 2) . "." . substr($varnoijin, 2, 2) . "." . substr($varnoijin, 4, 1) . "." . substr($varnoijin, 5, 2) . "." . substr($varnoijin, 7, 5);
        return $varresult;
    }

    function format_huruf($raw_url_encoded) {
        $reportSubtitle = stripslashes($raw_url_encoded);
        $reportSubtitle = iconv('UTF-8', 'windows-1252', $reportSubtitle);
        return $reportSubtitle;
    }

    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */

    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */

    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 3.7) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);
        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);
        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
            $this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }

    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */

    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */

    function data($arrdata, $margin) {
        $this->NewIkd();
        $dflength = 174;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarstaticpendaftaran = 54;
        $lebarnomor = 8;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        }
        $x = $kiri;

        $this->tulis($x, '+8', 'SURAT TANDA PENDAFTARAN SEBAGAI', 'C', $dflength, '', 'Arial', 'B', 20);
        $this->tulis($x, '+8', strtoupper($arrdata['row']['jenis_agen']), 'C', $dflength, '', 'Arial', 'B', 20);
        if ($arrdata['row']['jns_produk'] == '02') {
            $this->tulis($x, '+8', 'JASA PERUSAHAAN ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 20);
        } elseif ($arrdata['row']['jns_produk'] == '03') {
            $this->tulis($x, '+8', 'PUPUK PRODUKSI ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 20);
        } else {
            $this->tulis($x, '+8', 'BARANG PRODUKSI ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 20);
        }
        $this->pdf->Ln(10);
        #Border Nomor Telepon


        $this->pdf->Ln(5);

        #Border Atas
        //$this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext, $x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 11);
        //$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
        // 01 Dalam Negeri
        // 02 Luar Negeri 
        $this->pdf->SetFont('Arial', 'B', 10);
        if ($arrdata['row']['jns_produk'] == "01") {
            if ($arrdata['row']['jns_produksi'] == "01") {
                $this->pdf->SetFillColor(0, 0, 255); //change color with RGB for jenis "barang Dalam Negeri" Biru   
            } else if ($arrdata['row']['jns_produksi'] == "02") {
                $this->pdf->SetFillColor(255, 255, 0); //change color with RGB for jenis "barang Luar Negeri" Kuning  
            }
        } elseif ($arrdata['row']['jns_produk'] == "02") {
            $this->pdf->SetFillColor(255, 0, 0); //change color with RGB for jenis "Jasa Dalam Negeri" Merah   
        } elseif ($arrdata['row']['jns_produk'] == "03") {
            $this->pdf->SetFillColor(0, 255, 0); //change color with RGB for jenis "pupuk"
        } else {
            $this->pdf->SetFillColor(30, 144, 255);
        }
        // if (in_array($arrdata['row']['status'], array('0200', '0100', '0101', '0700', '0600'))) { 
        //     $this->pdf->MultiCell(0, 8, "		   NOMOR                                          :  " . trim($arrdata['temp_terbit']['no_izin']), 1, 1, true);
        // } else {
        $this->pdf->MultiCell(0, 8, "  NOMOR                                          :  ", 1, 1, true);
        // }
        $this->pdf->SetFont('Arial', 'B', 10);
        if ($arrdata['row']['jns_produk'] == "01") {
            if ($arrdata['row']['jns_produksi'] == "01") {
                $this->pdf->SetFillColor(0, 0, 255); //change color with RGB for jenis "barang Dalam Negeri" Biru   
            } else if ($arrdata['row']['jns_produksi'] == "02") {
                $this->pdf->SetFillColor(255, 255, 0); //change color with RGB for jenis "barang Luar Negeri" Kuning  
            }
        } elseif ($arrdata['row']['jns_produk'] == "02") {
            $this->pdf->SetFillColor(255, 0, 0); //change color with RGB for jenis "Jasa Dalam Negeri" Merah   
        } elseif ($arrdata['row']['jns_produk'] == "03") {
            $this->pdf->SetFillColor(0, 255, 0); //change color with RGB for jenis "pupuk"
        } else {
            $this->pdf->SetFillColor(30, 144, 255);
        }
        $this->pdf->MultiCell(0, 8, "  BERLAKU SAMPAI DENGAN       :  ", 1, 1, true);


        /*
          $this->pdf->tulis($x + 0.5, 0, '  NOMOR ', 'L', 180, '', 'Arial', '', 10,1,10);
          $this->pdf->tulis($x + 55, '-1', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);

          $this->tulis($x + 60, '0', 'FAX//1234567' , 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);


          #Border Kegiatan Usaha
          //$this->pdf->Ln();
          //$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());


          $this->tulis($x + 0.5, '+11', '  BERLAKU SAMPAI DENGAN', 'L', 180, '', 'Arial', '', 10,1,10);
          $this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + 60, '+5', 'MARET 2020', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          //$this->pdf->Ln(8);
          //$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
         */
        $this->pdf->Ln(10);


        $this->tulis($x + 55, '-5', 'DIBERIKAN KEPADA :', '', 70, '', 'Arial', '', 10, 0, 13);
        $this->pdf->Ln(5);
        #nama perusahaan
        $this->tulis($x + 1, 0, '1.	NAMA PERUSAHAAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nm_perusahaan']), '', 70, '', 'Arial', 'B', 10, 0, 13);
        $this->pdf->Ln(8);
        #alamat
        $this->tulis($x + 1, '0', '2.	ALAMAT ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', ucwords($arrdata['row']['alamat']), '', 120, 'M', 'Arial', '', 10, 0, 13);
        //	$this->tulis($x + 60, '+0',  , '', 70, '', 'Arial', '', 10,0,13);
        //$this->pdf->Ln(8);
        #tlp/fax
        $this->tulis($x + 1, '+', '3.	NO. TELP / FAX ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '0', $arrdata['row']['telp'] . ' / ' . $arrdata['row']['fax'], '', 70, '', 'Arial', '', 10, 0, 13);

        $this->pdf->Ln(8);
        #nama pj
        $this->tulis($x + 1, '+', '4.	NAMA PENANGGUNG JAWAB ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nama_pj']), '', 70, '', 'Arial', 'B', 10, 0, 13);
        #jabatan
        $this->tulis($x + 2, '+4', '  	JABATAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '0', $arrdata['row']['jabatan_pj'], '', 70, '', 'Arial', '', 10, 0, 13);

        if ($arrdata['row']['status_dok'] == '02') {
            $this->pdf->SetTextColor(112, 121, 125);
            $this->pdf->SetFont('Arial', 'B', 60);
            $this->RotatedText(110, 260, "DICABUT", 0);
            $this->pdf->SetTextColor(000, 000, 000);
        }

        $this->pdf->Ln(7);
        #nama pj
        $this->tulis($x + 1, '+', '5.	NAMA PRODUSEN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        // $this->tulis($x + 60, '+5', ucfirst($arrdata['row']['almt_perusahaan_prod']), '', 120, 'M', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', strtoupper($arrdata['row']['nm_perusahaan_prod']), '', 120, 'M', 'Arial', 'B', 10, 0, 13);

        #alamat produsen
        $this->tulis($x + 2, '-1', '	  ALAMAT ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        // $this->tulis($x + 60, '+5', ucfirst($arrdata['row']['alamat']), '', 100, 'M', 'Arial', '', 10, 0, 13);
        if ($arrdata['row']['jns_produksi'] == '01') {
            $this->tulis($x + 60, '+5', ucfirst($arrdata['row']['alamat_prod']), '', 120, 'M', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 60, '+5', ucfirst($arrdata['row']['almt_perusahaan_prod']), '', 120, 'M', 'Arial', '', 10, 0, 13);
        }
        //$this->pdf->Ln(1);
        #nama pemasok
        $this->tulis($x + 1, '+', '6.	NAMA PEMASOK (SUPPLIER) ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nm_perusahaan_supl']), '', 70, '', 'Arial', 'B', 10, 0, 13);

        #alamat pemasok
        $this->tulis($x + 2, '+4', '	  ALAMAT ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        // $this->tulis($x + 60, '+5', ucfirst($arrdata['row']['alamat']), '', 100, 'M', 'Arial', '', 10, 0, 13);
        if ($arrdata['row']['almt_perusahaan_supl'] == '') {
            $this->tulis($x + 60, '+5', "--", '', 120, 'M', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 60, '+5', ucfirst($arrdata['row']['almt_perusahaan_supl']), '', 120, 'M', 'Arial', '', 10, 0, 13);
        }

        //$this->pdf->Ln(1);
        #nama pj
        if ($arrdata['row']['jns_produk'] != "02") {
            $this->tulis($x + 1, '-3', '7.	JENIS BARANG ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 1, '-3', '7.  JENIS JASA ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', ucwords(strlen($arrdata['row']['jenis']) < 125) ? ucwords($arrdata['row']['jenis']) : 'Terlampir', '', 125, 'M', 'Arial', '', 10, 0, 13);
        //$this->pdf->Ln(8);
        #nama pj
        $this->tulis($x + 1, '-3', '8.	MERK ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', (strlen($arrdata['row']['merk']) < 125) ? $arrdata['row']['merk'] : 'Terlampir', '', 125, 'M', 'Arial', '', 10, 0, 13);

        if ($arrdata['row']['jns_produk'] != "02") {
            if ($arrdata['row']['jns_produksi'] == "02") {
                $this->tulis($x + 1, '+', '9.   NO. HS ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                $this->tulis($x + 60, '0', (strlen($arrdata['row']['hs']) < 80) ? $arrdata['row']['hs'] : 'Terlampir', '', 70, '', 'Arial', '', 10, 0, 13);
                // $this->tulis($x + 60, '0', strlen($arrdata['row']['hs']), '', 70, '', 'Arial', '', 10, 0, 13);
                $next = '10';
                $this->pdf->Ln(8);
            }
        } else {
            $next = '9';
        }


        #nama pj
        $pemasaran = explode(",", $arrdata['row']['pemasaran']);
        if (count($pemasaran)) {

            function tes($arr) {
                return ucfirst(trim($arr));
            }

        }
        $map = (count($pemasaran)) ? array_map("tes", $pemasaran) : ucfirst($arrdata['row']['pemasaran']);
        $this->tulis($x + 1, '+', $next . '.	WILAYAH PEMASARAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        if (strlen($arrdata['row']['pemasaran']) > 55) {
            $this->tulis($x + 60, '0', "Terlampir", '', 70, '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 60, '0', implode(", ", $map), '', 70, '', 'Arial', '', 10, 0, 13);
        }

        $this->pdf->Ln(10);
        #footer
        #terbit
        $this->tulis($x + 105, '+', 'Diterbitkan di ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 137, '0', 'J A K A R T A', '', 70, '', 'Arial', 'B', 10, 0, 13);
        #tanggal
        $this->tulis($x + 105, '+4', 'pada tanggal ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 137, '0', $arrdata['row']['izin'], '', 70, '', 'Arial', 'B', 10, 0, 13);

        $this->pdf->Ln(10);
        $this->tulis($x + 75, '+', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        if ($arrdata['row']['jabatan_ttd'] == '') {
            $this->tulis($x + 75, '+4', $arrdata['ttd']['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 75, '+4', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }

        $this->tulis($x + 75, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 20, 0, 15);

        if ($arrdata['row']['nama_ttd'] == '') {
            $this->tulis($x + 75, '+15', $arrdata['ttd']['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 75, '+15', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }

        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 65), ($this->pdf->GetY() - 22), 20); #Coba diganti ganti aja 30 dan 15 nya.

        if ($arrdata['tembusan'] != '') {
            
        } else {
            $this->tulis($x, '-30', 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf - 9);
            for ($i = 0; $i < count($arrdata['tembusan']); $i++) {
                $this->tulis($x, '+4', $arrdata['tembusan'][$i]['urutan'] . '.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 12);
                $this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 12);
            }
        }
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);

        if (in_array($arrdata['row']['status'], array('0200', '0100', '0101', '0700', '0600'))) {
            $nomors = trim($arrdata['temp_terbit']['no_izin']);
            $tgl_exps = $arrdata['temp_terbit']['tgl_izin_exp'];
        } else {
            $nomors = trim($arrdata['row']['no_izin']);
            $tgl_exps = $arrdata['row']['exp'];
        }
        $this->pdf->SetXY(78, 87);
        $this->tulis('', '', $nomors, 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'B', 10, 0, 13);
        $this->pdf->SetXY(78, 95);
        $this->tulis('', '', $tgl_exps, 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'B', 10, 0, 13);
        /*

          $this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext, $x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 11);
          $this->tulis($x, 0, 'NOMOR TDP', 'C', (($dflength - $lebarstaticpendaftaran) / 2), '', 'Arial', '', 9);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), 0, 'BERLAKU S/D TANGGAL', 'C', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);


          $this->pdf->Line($x + ($dflength - $lebarstaticpendaftaran), $YNext, $x + ($dflength - $lebarstaticpendaftaran), $YNow + 11);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 1.5), '', 'PENDAFTARAN  : '.$arrdata['row']['pendaftaran'], '', 54, '', 'Arial', '', 9);
          $this->pdf->Ln(6);

          #Nomor TDP
          $this->tulis($x, 0, $this->FormatTDP($arrdata['row']['no_ijin']), 'C', (($dflength - $lebarstaticpendaftaran) / 2), '', 'Arial', 'B', 11);

          #Tanggal TDP
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), 0, $this->iif($arrdata['row']['tgl_akhir'], strtoupper($this->convert_date($arrdata['row']['tgl_akhir']))), 'C', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', 'B', 11);
          $this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran)), $YNext + 6.3, $x + $dflength, $YNext + 6.3);

          #PEMBAHARUAN
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 1.5), '', 'PEMBAHARUAN : '.($arrdata['row']['pembaharuan'] == 0 ? '-' : $arrdata['row']['pembaharuan']), '', 54, '', 'Arial', '', 9);


          $YSec = $this->pdf->GetY();
          $this->pdf->SetY($YSec);
          $YNow = $this->pdf->GetY();
          $this->pdf->Line($x, $YNext, ($x + $dflength), $YNext);
          $this->pdf->Line($x, $YNext, $x, $YNow + 5);
          $this->pdf->Line(($x + $dflength), $YNext, ($x + $dflength), $YNow + 5);
          $this->pdf->Line($x, $YNow + 5, ($x + $dflength), $YNow + 5);
          $this->pdf->Ln(10);

          #Border Atas

          $ystartpoint = $this->pdf->GetY();

          $this->pdf->Line($x, $ystartpoint, ($x + $dflength), $ystartpoint);
          $this->tulis($x + 1.5, '+3', 'NAMA PERUSAHAAN : ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 9);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '', 'STATUS : ', '', 41.5, '', 'Arial', '', 9);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '+6', strtoupper($arrdata['row']['ur_status_perusahaan']), '', 41.5, '', 'Arial', '', 10);
          $this->pdf->newFlowingBlock(($dflength - $lebarstaticpendaftaran), 5, 'W', 'J', 0, false);
          $this->tulis($x + 1.5, '', strtoupper($arrdata['row']['nama']), 'L', 10, 'W', 'Arial', 'B',13);
          $this->pdf->finishFlowingBlock();
          $this->pdf->Ln(6);
          $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
          $this->tulis($x + 1.5, 0, 'PENANGGUNG JAWAB / PENGURUS : '.strtoupper($arrdata['row']['nama_pemilik']), 'L', ($dflength - 1.5), '', 'Arial', '', 10, 0, 13);

          #Border Alamat
          $this->pdf->Ln();
          $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
          $this->tulis($x + 1.5, '+3', 'ALAMAT : ', 'L', 25, '', 'Arial', '', 9);
          $rowalamat = strtoupper($arrdata['row']['alamat']);
          $kelurahan = 'KEL';
          $pos=strpos($rowalamat,$kelurahan);
          if($pos !== false){
          $alamat1=explode('KEL.',$alamat);
          $this->tulis($x + 20, 0, $alamat1[0], 'L', 5, '', 'Arial', '', 10);
          $this->tulis($x + 25, 0, $alamat1[1], 'L', ($dflength - 20), 'M', 'Arial', '', 10);
          }else{
          $this->tulis($x + 20, 0, strtoupper($arrdata['row']['alamat']), 'L', ($dflength - 20), 'M', 'Arial', '',10);
          $this->tulis($x + 20, '+1', 'KEL. '. strtoupper($arrdata['row']['ur_kel']).' KEC. '.strtoupper($arrdata['row']['ur_kec']), 'L', ($dflength - 20), 'M', 'Arial', '', 10);
          $this->tulis($x + 20, '+1', strtoupper($arrdata['row']['urai_kota']).' '.$arrdata['row']['ur_pos'], 'L', ($dflength - 20), '', 'Arial', '',10);
          $this->pdf->Ln(2);
          }

          #Border NPWP
          $this->pdf->Ln();
          $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
          $this->tulis($x + 1.5, 0, 'NPWP   :   '.$this->FormatNPWP($arrdata['row']['npwp']), 'L', ($dflength - 1.5), '', 'Arial', '', 10, 0, 13);

          #Border Nomor Telepon
          $this->pdf->Ln();
          $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
          $this->tulis($x + 1.5, 0, 'NOMOR TELEPON :         '.$arrdata['row']['telepon'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - 14), '', 'FAX :          '.$arrdata['row']['fax'], '', 70, '', 'Arial', '', 10,0,13);


          #Border Kegiatan Usaha
          $this->pdf->Ln();
          $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());

          $this->tulis($x + 1.5, '+3', 'KEGIATAN USAHA POKOK : ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '', 'KBLI : ', 'C', 41.5, '', 'Arial', '', 10);

          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '+5', $arrdata['row']['kbli'], 'C', 41.5, '', 'Arial', '', 10);
          $this->tulis($x + 10, '', str_replace(chr(10),' ',strtoupper($arrdata['row']['pokok'])), 'J', ($dflength - $lebarstaticpendaftaran), 'M', 'Arial', '', 10);

          #Border Bawah Pisan
          $this->pdf->Ln();
          $this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
          $this->pdf->Line($x, $ystartpoint, $x, $this->pdf->GetY());
          $this->pdf->Line(($x + $dflength), $ystartpoint, ($x + $dflength), $this->pdf->GetY());
          #End Border Bawah Pisan

          #Area Signature
          $this->pdf->Ln(8);
          $panjang = strlen($arrdata['row']['penerbit']);
          if ($panjang > 40){
          $mulai = 40;
          $minus = 5;
          }else{
          $mulai = 30;
          $minus = 0;
          }

          $this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '', 'Dikeluarkan di', '', 25, '', 'Arial', '', 10);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran)), '', ':', '', 5, '', 'Arial', '', 10);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', strtoupper($arrdata['row']['urai_kota']), '', 55, '', 'Arial', '', 10);

          $this->tulis($x + 1.5, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '', 'Pada Tanggal', '', 25, '', 'Arial', '', 10);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran)), '', ':', '', 5, '', 'Arial', '', 10);
          $this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', $this->iif($arrdata['row']['tgl_ijin'], strtoupper($this->convert_date($arrdata['row']['tgl_ijin']))), '', 55, '', 'Arial', '', 10);

          $this->pdf->Ln(8);
          $this->tulis($x + 1.5, '', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength / 2) - ($panjang/2)), '', 'KEPALA '.strtoupper($arrdata['row']['penerbit']), 'C', (($dflength / 2) + ($panjang/2)), 'M', 'Arial', '', 10);

          $this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength / 2) - 10-$minus), '', 'SELAKU', 'C', (($dflength / 2) + 10), 'M', 'Arial', '', 10);

          $this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength / 2) - 10-$minus), '', 'KEPALA KANTOR PENDAFTARAN PERUSAHAAN', 'C', (($dflength / 2) + 10), 'M', 'Arial', '', 10);

          $this->pdf->Ln(34);
          $this->tulis($x + 1.5, '', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength / 2) - 10-$minus), '', $arrdata['row']['nama_ttd'], 'C', (($dflength / 2) + 10), '', 'Arial', 'U', 10);

          $this->tulis($x + 1.5, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
          $this->tulis($x + (($dflength / 2) - 10-$minus), '', 'NIP. '. $arrdata['row']['nip_ttd'], 'C', (($dflength / 2) + 10), '', 'Arial', '', 10);
          #End Area Signature
         */

        if (strlen($arrdata['row']['pemasaran']) > 55) {
            $this->pdf->AddPage();
            $this->tulis($x, '+8', 'LAMPIRAN SURAT TANDA PENDAFTARAN SEBAGAI', 'C', $dflength, '', 'Arial', 'B', 18);
            $this->tulis($x, '+8', strtoupper($arrdata['row']['jenis_agen']), 'C', $dflength, '', 'Arial', 'B', 18);
            if ($arrdata['row']['jns_produk'] == '02') {
                $this->tulis($x, '+8', 'JASA PERUSAHAAN ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 18);
            } elseif ($arrdata['row']['jns_produk'] == '03') {
                $this->tulis($x, '+8', 'PUPUK PRODUKSI ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 18);
            } else {
                $this->tulis($x, '+8', 'BARANG PRODUKSI ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 18);
            }

            $this->pdf->Ln();
            $this->tulis($x + 1, 0, '  NAMA PERUSAHAAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nm_perusahaan']), '', 70, '', 'Arial', 'B', 10, 0, 13);

            $this->tulis($x + 1, '+5', '  N O M O R ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 60, '0', strtoupper($arrdata['row']['no_izin']), '', 70, '', 'Arial', 'B', 10, 0, 13);

            $this->tulis($x + 1, '+5', '  BERLAKU SAMPAI DENGAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 60, '0', strtoupper($arrdata['row']['exp']), '', 70, '', 'Arial', 'B', 10, 0, 13);
            $this->pdf->Ln();
            $this->pdf->SetFillColor(255, 255, 255);
            $this->tulis($x, '0', 'WILAYAH PEMASARAN ', 'C', 185, '', 'Arial', 'B', 10, 'L,R,T', 10);
            $this->pdf->Ln();
            // $str_wil = ($l == 0) ? 1619 * $l : (1619 * $l) + 1;
            // $str_hs = ($l == 0) ? 437 * $l : (437 * $l) + 1;
            // $ss = str_replace("\n", "", substr($arrdata['row']['pemasaran'], $str_wil, 1619));
            //$er = preg_replace('/\s+/', ' ', $rr);
            $this->pdf->SetFont('Arial', '', 10);
            $this->pdf->MultiCell(185, 5, str_replace("\n", " ", $arrdata['row']['pemasaran']), 0, 'J', false);
            $this->pdf->SetY(110.7);
            $this->pdf->SetX(135);
            /* $this->pdf->Ln(); */
            //$this->pdf->Rect(15,83,182,89);
            $this->pdf->SetY(130);
            $this->pdf->SetX(115);
            $this->pdf->Rect(15, 111, 185, 120);
            //$this->pdf->Ln();
            $this->pdf->SetY(130);
            $this->pdf->SetX(115);
            $this->tulis($x + 105, '+100', 'Diterbitkan di ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
            $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 137, '0', 'J A K A R T A', '', 70, '', 'Arial', 'B', 10, 0, 13);
            #tanggal
            $this->tulis($x + 105, '+4', 'pada tanggal ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
            $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            $this->tulis($x + 137, '0', $arrdata['row']['izin'], '', 70, '', 'Arial', 'B', 10, 0, 13);

            $this->pdf->Ln(10);
            $this->tulis($x + 75, '+', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);

            if ($arrdata['row']['jabatan_ttd'] == '') {
                $this->tulis($x + 75, '+4', $arrdata['ttd']['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            } else {
                $this->tulis($x + 75, '+4', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            }

            $this->tulis($x + 75, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 20, 0, 15);

            if ($arrdata['row']['nama_ttd'] == '') {
                $this->tulis($x + 75, '+15', $arrdata['ttd']['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            } else {
                $this->tulis($x + 75, '+15', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
            }


            $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
            $this->qrcode->genQRcode($string, 'L');
            $this->qrcode->disableBorder();
            $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 65), ($this->pdf->GetY() - 20), 20); #Coba diganti ganti aja 30 dan 15 nya.

            $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
            $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
        }

        $this->pdf->AddPage();

        $this->tulis($x, '+', 'KETENTUAN UMUM', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x, '+10', '1.', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 5, '+5', 'Apabila terdapat kekeliruan dalam Surat Tanda Pendaftaran (STP) ini akan diadakan perubahan atau penyesuaian sebagaimana mestinya sampai kepada pembatalan atau pencabutan masa berikutnya', 'J', $dflength, 'M', 'Arial', '', 12, 0, 13);

        $this->tulis($x, '+2', '2.', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 5, '+4', 'Setiap pemegang (STP) wajib menyampaikan laporan kegiatan perusahaan setiap 6(enam) bulan sekali kepada Direktur Bina Usaha Dan Pelaku Distribusi', 'J', $dflength, 'M', 'Arial', '', 12, 0, 13);

        $this->tulis($x, '+2', '3.', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 5, '+5', 'Setiap perusahaan yang tidak lagi melakukan usahanya atau menutup perubahannya wajib melaporkan penutupan kegiatan perusahaan usahanya dan mengembalika STP asli kepada Direktur Bina Usaha Dan Pelaku Distribusi', 'J', $dflength, 'M', 'Arial', '', 12, 0, 13);

        $this->tulis($x, '+2', '4.', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 5, '+5', 'Setiap perusahaan yang melakukan perubahan nama prinsipal, status penunjukan keagenan /  kedistribusian, merk, wilayah pemasaran jenis barang, alamat perusahaan, penanggung jawab perusahaan wajib melaporkan kepada Direktur Bina Usaha Dan Pelaku Distribusi', 'J', $dflength, 'M', 'Arial', '', 12, 0, 13);
        $this->tulis($x, '+14', 'SANKSI', 'L', $dflength, 'M', 'Arial', '', 12, 0, 13);

        $this->tulis($x, '+2', '1.', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 5, '+5', 'Setiap perusahaan agen / distributor yang tidak melakukan pendaftaran dikenakan sanksi administratif sampai dengan pencabutan SIUP. ((Pasal 24) ', 'J', $dflength, 'M', 'Arial', '', 12, 0, 13);

        $this->tulis($x, '0', '2.', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        $this->tulis($x + 5, '+5', 'Setiap perusahaan agen / distributor yang tidak menyampaikan laporan kegiatan dan perubahan perubahan yang dilakukan dikenakan sanksi administratif sampai dengan pemberhentian sementara STP selama 6(enam) bulan atau pencabutan STP. (Pasal 25)', 'J', $dflength, 'M', 'Arial', '', 12, 0, 13);
        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 70), ($this->pdf->GetY() + 20), 25); #Coba diganti ganti aja 30 dan 15 nya.

        $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);


        /* $rr = "dedededhoehduehudheudheudhuehduehduhehuehduehdehdeduheudheduheduheuidheiudheuihdeuihdeuihdeuhudheuidheudheudheuidsfsdfsdfsdffasdfsdfsdfsdgergtwvfevtsvrwtvrvrwevrgvtrevrtvrvrvtrvtrvtv
          nxuiehxiuhiuheuiceichiuhuihiuhuihiuhuihiuhiuhuihiuhiuhiuhiuhuihuihiuhiuhxeyuevduyvdhehdbjendneiduiehdrgrfbkdurfrfgrygfyurgfyurgfyurgfyugryufgyrugfuyrgfyurgfuygruyfgyrugfuygrfuygrfyugruyfgryugfuyrgfyugryfugryugfyurgfyugryufg
          nxuiehxiuhiuheuiceichiuhuihiuhuihiuhuihiuhiuhuihiuhiuhiuhiuhuihuihiuhiuhxeyuevduyvdhehdbjendneiduiehdrgrfbkdurfrfgrygfyurgfyurgfyurgfyugryufgyrugfuyrgfyurgfuygruyfgyrugfuygrfuygrfyugruyfgryugfuyrgfyugryfugryugfyurgfyugryufg
          nxuiehxiuhiuheuiceichiuhuihiuhuihiuhuihiuhiuhuihiuhiuhiuhiuhuihuihiuhiuhxeyuevduyvdhehdbjendneiduiehdrgrfbkdurfrfgrygfyurgfyurgfyurgfyugryufgyrugfuyrgfyurgfuygruyfgyrugfuygrfuygrfyugruyfgryugfuyrgfyugryfugryugfyurgfyugryufg
          nxuiehxiuhiuheuiceichiuhuihiuhuihiuhuihiuhiuhuihiuhiuhiuhiuhuihuihiuhiuhxeyuevduyvdhehdbjendneiduiehdrgrfbkdurfrfgrygfyurgfyurgfyurgfyugryufgyrugfuyrgfyurgfuygruyfgyrugfuygrfuygrfyugruyfgryugfuyrgfyugryfugryugfyurgfyugryufg
          nxuiehxiuhiuheuiceichiuhuihiuhuihiuhuihiuhiuhuihiuhiuhiuhiuhuihuihiuhiuhxeyuevduyvdhehdbjendneiduiehdrgrfbkdurfrfgrygfyurgfyurgfyurgfyugryufgyrugfuyrgfyurgfuygruyfgyrugfuygrfuygrfyugruyfgryugfuyrgfyugryfugryugfyurgfyugryufg
          nxuiehxiuhiuheuiceichiuhuihiuhuihiuhuihiuhiuhuihiuhiuhiuhiuhuihuihiuhiuhxeyuevduyvdhehdbjendneiduiehdrgrfbkdurfrfgrygfyurgfyurgfyurgfyugryufgyrugfuyrgfyurgfuygruyfgyrugfuygrfuygrfyugruyfgryugfuyrgfyugryfugryugfyurgfyugryufg
          nxuiehxiuhiuheuiceichiuhuihiuhuihiuhuihiuhiuhuihiuhiuhiuhiudedededhoehduehudheudheudhuehduehduhehuehduehdehdeduheudheduheduheuidheiudheuihdeuihdeuihdeuhudheuidheudheudheuidsfsdfsdfsdffasdfsdfsdfsdgergtwvfevtsvrwtvrvrwevrgvtrevrtvrvrvtrvtrvtv
          nxuiehxiuhiuheuiceichiuhuihiuhuihiuhuihiuhiuhuihiuhiuhiuhiuhuihuihiuhiuhxeyuevduyvdhehdbjendneiduiehdrgrfbkdurfrfgrygfyurgfyurgfyurgfyugryufgyrugfuyrgfyurgfuygruyfgyrugfuygrfuygrfyugruyfgryugfuyrgfyugryfugryugfyurgfyugryufg
          nxuiehxiuhiuheuiceichiuhuihiuhuihiuhuihiuhiuhuihiuhiuhiuhiuhuihuihiuhiuhxeyuevduyvdhehdbjendneiduiehdrgrfbkdurfrfgrygfyurgfyurgfyurgfyugryufgyrugfuyrgfyurgfuygruyfgyrugfuygrfuygrfyugruyfgryugfuyrgfyugryfugryugfyurgfyugryufg
          nxuiehxiuhiuhjdjj huie jee de
          dxcdccdfcd tes";
         */
        $len = strlen($arrdata['row']['jenis']);
        $len_merk = strlen($arrdata['row']['merk']);
        $jns = str_replace("\n", " ", $arrdata['row']['jenis']);
        $jns = $this->format_huruf($jns);
        $len_hs = strlen($arrdata['row']['hs']);
        $ceil = ceil($len / 1619);
        if ($len >= 125 || $len_hs >= 80 || $len_merk >= 125) {
            for ($l = 0; $l < $ceil; $l++) {
                $this->pdf->AddPage();
                $this->tulis($x, '+8', 'LAMPIRAN SURAT TANDA PENDAFTARAN SEBAGAI', 'C', $dflength, '', 'Arial', 'B', 18);
                $this->tulis($x, '+8', strtoupper($arrdata['row']['jenis_agen']), 'C', $dflength, '', 'Arial', 'B', 18);
                if ($arrdata['row']['jns_produk'] == '02') {
                    $this->tulis($x, '+8', 'JASA PERUSAHAAN ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 18);
                } elseif ($arrdata['row']['jns_produk'] == '03') {
                    $this->tulis($x, '+8', 'PUPUK PRODUKSI ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 18);
                } else {
                    $this->tulis($x, '+8', 'BARANG PRODUKSI ' . strtoupper($arrdata['row']['produksi']), 'C', $dflength, '', 'Arial', 'B', 18);
                }

                $this->pdf->Ln();
                $this->tulis($x + 1, 0, '  NAMA PERUSAHAAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                $this->tulis($x + 60, '0', strtoupper($arrdata['row']['nm_perusahaan']), '', 70, '', 'Arial', 'B', 10, 0, 13);

                $this->tulis($x + 1, '+5', '  N O M O R ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                $this->tulis($x + 60, '0', strtoupper($arrdata['row']['no_izin']), '', 70, '', 'Arial', 'B', 10, 0, 13);

                $this->tulis($x + 1, '+5', '  BERLAKU SAMPAI DENGAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                $this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                $this->tulis($x + 60, '0', strtoupper($arrdata['row']['exp']), '', 70, '', 'Arial', 'B', 10, 0, 13);
                $this->pdf->Ln();
                $this->pdf->SetFillColor(255, 255, 255);
                if ($arrdata['row']['jns_produk'] == '02') {
                    $this->tulis($x, '0', 'JENIS JASA ', 'C', 152, '', 'Arial', 'B', 10, 'L,R,T', 10);
                    // $this->tulis($x + 120, '0', 'NO.HS ', 'C', 32, '', 'Arial', 'B', 10, 'L,R,T', 10);
                    $this->tulis($x + 152, '0', 'MERK ', 'C', 33, '', 'Arial', 'B', 10, 'L,R,T', 10);
                    $this->pdf->Ln();
                    $str = ($l == 0) ? 1700 * $l : (1700 * $l) + 1;
                    $str_hs = ($l == 0) ? 298 * $l : (298 * $l) + 1;
                    $ss = str_replace("\n", "", substr(trim($jns), $str, 1700));
                    //$er = preg_replace('/\s+/', ' ', $rr);
                    $this->pdf->SetFont('Arial', '', 10);
                    $this->pdf->MultiCell(150, 5, preg_replace("/\s+/", " ", $ss), 0, 'J', false);
                    $this->pdf->SetY(110.7);
                    $this->pdf->SetX(167);
                    $this->pdf->MultiCell(32, 5, $arrdata['row']['merk'], 0, 'J', true);
                    /* $this->pdf->Ln(); */
                    //$this->pdf->Rect(15,83,182,89);
                    $this->pdf->SetY(130);
                    $this->pdf->SetX(115);
                    $this->pdf->Rect(15, 111, 185, 120);
                    // $this->pdf->Line(135, 231, 135, 110);
                    $this->pdf->Line(167, 231, 167, 110);
                } else {
                    $this->tulis($x, '0', 'JENIS BARANG ', 'C', 120, '', 'Arial', 'B', 10, 'L,R,T', 10);
                    $this->tulis($x + 120, '0', 'NO.HS ', 'C', 32, '', 'Arial', 'B', 10, 'L,R,T', 10);
                    $this->tulis($x + 152, '0', 'MERK ', 'C', 33, '', 'Arial', 'B', 10, 'L,R,T', 10);
                    $this->pdf->Ln();
                    $str = ($l == 0) ? 1619 * $l : (1619 * $l) + 1;
                    $str_hs = ($l == 0) ? 298 * $l : (298 * $l) + 1;
                    $ss = str_replace("\n", "", substr(trim($jns), $str, 1371));
                    //$er = preg_replace('/\s+/', ' ', $rr);
                    $this->pdf->SetFont('Arial', '', 10);
                    $this->pdf->MultiCell(118, 5, preg_replace("/\s+/", " ", $ss), 0, 'J', false);
                    $this->pdf->SetY(110.7);
                    $this->pdf->SetX(135);
                    $this->pdf->MultiCell(32, 5, substr($arrdata['row']['hs'], $str_hs, 298), 0, 'J', true);
                    $this->pdf->SetY(110.7);
                    $this->pdf->SetX(167);
                    $this->pdf->MultiCell(32, 5, $arrdata['row']['merk'], 0, 'J', true);
                    /* $this->pdf->Ln(); */
                    //$this->pdf->Rect(15,83,182,89);
                    $this->pdf->SetY(130);
                    $this->pdf->SetX(115);
                    $this->pdf->Rect(15, 111, 185, 120);
                    $this->pdf->Line(135, 231, 135, 110);
                    $this->pdf->Line(167, 231, 167, 110);
                }
                //$this->pdf->Ln();
                $this->pdf->SetY(130);
                $this->pdf->SetX(115);
                $this->tulis($x + 105, '+100', 'Diterbitkan di ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
                $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                $this->tulis($x + 137, '0', 'J A K A R T A', '', 70, '', 'Arial', 'B', 10, 0, 13);
                #tanggal
                $this->tulis($x + 105, '+4', 'pada tanggal ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
                $this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                $this->tulis($x + 137, '0', $arrdata['row']['izin'], '', 70, '', 'Arial', 'B', 10, 0, 13);

                $this->pdf->Ln(10);
                $this->tulis($x + 75, '+', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);

                if ($arrdata['row']['jabatan_ttd'] == '') {
                    $this->tulis($x + 75, '+4', $arrdata['ttd']['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                } else {
                    $this->tulis($x + 75, '+4', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                }

                $this->tulis($x + 75, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 20, 0, 15);

                if ($arrdata['row']['nama_ttd'] == '') {
                    $this->tulis($x + 75, '+15', $arrdata['ttd']['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                } else {
                    $this->tulis($x + 75, '+15', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
                }


                $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
                $this->qrcode->genQRcode($string, 'L');
                $this->qrcode->disableBorder();
                $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 65), ($this->pdf->GetY() - 20), 25); #Coba diganti ganti aja 30 dan 15 nya.

                $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
                $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
            }
        }
    }

    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */

    function generate($arrdata) {
        if ($arrdata['cetak'] != '') {
            $namafile = $arrdata['namafile'];
            $dir = $arrdata['dir'];
            if (file_exists($dir) && is_dir($dir)) {
                $path = 'upL04d5/document/DOKAGEN/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
            } else {
                if (mkdir($dir, 0777, true)) {
                    if (chmod($dir, 0777)) {
                        $path = 'upL04d5/document/DOKAGEN/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
                    }
                }
            }
            $this->pdf->Output($path, 'F');
        } else {
            $this->pdf->Output();
        }
    }

    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}

?>