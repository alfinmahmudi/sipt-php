<?php
error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $ukuranhurufkop;
    var $huruf;
    var $def_margin = array(15, 40, 20, 15);

    function pdf($orientasi, $ukuran, $kertas, $margins, $data) {
        $this->ukuranhuruf = 11;
        $this->ukuranhurufdetil = 10;
        $this->ukuranhurufkop = 14;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->huruf = 'Arial';
        $this->qrcode = new QRcode();
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->pdf->footer = 1;
        if (is_array($margins) and count($margins) == 4) {
            $this->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
        } else {

            $this->pdf->SetMargins($this->def_margin[0], $this->def_margin[1], $this->def_margin[2], $this->def_margin[3]);
        }
        $this->data($data, $margins);
    }

    function Footer($arrdata) {
        if ($this->pdf->footer == 1) {
            $this->pdf->SetY(-20);
            $this->pdf->SetFont('Arial', '', 9);
            /* $Yget2 = -60;
              $this->pdf->SetY($Yget2); */
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
        }
    }

    function Header($arrdata) {
        //$this->pdf->SetTextColor(244,244,244);
        $this->pdf->SetTextColor(177, 177, 172);
        $this->pdf->SetFont('Arial', 'B', 60);
        $this->RotatedText(12, 140, $arrdata['row']['no_izin'], 0);
    }

    function RotatedText($x, $y, $txt, $angle) {
        //Text rotated around its origin
        $this->pdf->Rotate($angle, $x, $y);
        $this->pdf->Text($x, $y, $txt);
        $this->pdf->Rotate(0);
    }

    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */

    function NewIkd($arrdata) {
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Header($arrdata);
        $this->pdf->SetTextColor(0);
        $this->Footer($arrdata);
        $this->pdf->SetY($Yget);
    }

    function NewIkdLandscape() {
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }

    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }

    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */

    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */

    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 4) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);

        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));

        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));

        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);

        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
        //$this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
            $this->pdf->MultiCell($lebar, $tinggi, $teks, $garis, $align, 0);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }

    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */

    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */

    function data($arrdata, $margin) {

        $this->NewIkd($arrdata);

        //$this->pdf->SetTextColor(0);
        $dflength = 192;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarlabelstatic = 40;
        $lebarnomor = 5;
        if (is_array($margin) and count($margin) == 4) {

            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        }
       
        $x = $kiri;
        $yawal = $this->pdf->GetY();
        $this->tulis($x + 2, '+4', 'Nomor : ' . $arrdata['row']['no_izin'], 'L', $dflength - $this->def_margin[3], '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + 120, '', 'Jakarta, ' . $arrdata['row']['tgl_izin'], 'L', $dflength - $this->def_margin[3], '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x, '+8', 'SURAT IZIN USAHA JASA SURVEY', 'C', $dflength - $this->def_margin[3], '', 'Arial', 'BU', $this->ukuranhuruf + 2);
        $this->tulis($x, '+6', 'Berlaku s.d. tanggal : ' . $this->convert_date($arrdata['row']['exp']), 'C', $dflength - $this->def_margin[3], '', 'Arial', '', $this->ukuranhuruf);
        $this->pdf->Ln(6);
        $yNomor = $this->pdf->GetY();
        $selisih = $yNomor - $yawal;
        $this->pdf->Rect($x, $yawal, $dflength - $this->def_margin[3], $selisih);


        $this->tulis($x, '+3', 'MENTERI PERDAGANGAN REPUBLIK INDONESIA', 'C', $dflength - $this->def_margin[3], '', 'Arial', '', $this->ukuranhuruf);

        $this->pdf->Ln(4);
        #memperhatikan
        $this->tulis($x, '+2', 'Memperhatikan', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic, '', ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '', 'Surat Permohonan ' . $arrdata['row']['nm_perusahaan'], 'L', ($dflength - $lebarnomor - $lebarlabelstatic - $this->def_margin[3]), '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+5', 'Nomor ' . $arrdata['row']['no_aju'] . ' tanggal ' . $arrdata['row']['tgl_aju'], 'L', ($dflength - $lebarnomor - $lebarlabelstatic - $this->def_margin[3]), '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+5', 'Tentang permohonan memperoleh Surat Izin Usaha Jasa Survey ', 'L', ($dflength - $lebarnomor - $lebarlabelstatic - $this->def_margin[3]), '', 'Arial', '', $this->ukuranhuruf);
        $this->pdf->Ln(5);
        #mengingat
        $this->tulis($x, '+1', 'Mengingat', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic, '', ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '', 'Peraturan Menteri Perdagangan No. 14/M-DAG/PER/3/2006 tentang Ketentuan dan Tata Cara Penerbitan Surat Izin Usaha Jasa Survey', 'J', ($dflength - $lebarnomor - $lebarlabelstatic - $this->def_margin[3]), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);
        $this->pdf->Ln(4);
        $this->tulis($x, '-2', 'M E M U T U S K A N', 'C', $dflength - $this->def_margin[3], '', 'Arial', '', $this->ukuranhuruf);

        $this->pdf->Ln(5);
        #Nama Perusahaan
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+2', '1.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), 0, 'Nama Perusahaan', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), 0, ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0,strtoupper($arrdata['row']['nm_perusahaan']), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), '', 'Arial', 'B', $this->ukuranhuruf);

        #Alamat Perusahaan
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+5', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), 0, 'Alamat', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), 0, ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, ucwords(trim($arrdata['row']['almt_perusahaan']) . ' Kel. ' . $arrdata['row']['kel_perusahaan'] . ' Kec. ' . $arrdata['row']['kec_perusahaan'] . ' ' . $arrdata['row']['kab_perusahaan'] . ' Prov. ' . $arrdata['row']['prop_perusahaan']), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        #Telp Perusahaan
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+1', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), 0, 'No. Telp/Fax/Email', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), 0, ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, ucwords($arrdata['row']['telp'] . '/' . $arrdata['row']['fax'] . '/' . $arrdata['row']['email_PJ']), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), '', 'Arial', '', $this->ukuranhuruf);

        #Badan Hukum Perusahaan
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+5', '2.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), 0, 'No. Pengesahan Badan Hukum', 'L', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), '-3', ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, ucwords($arrdata['aktaPengesahan']['nomor']), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), '', 'Arial', '', $this->ukuranhuruf);

        #Dari
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+9', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), -5, 'Dari', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), 0, ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, ucwords($arrdata['aktaPengesahan']['penerbit_dok']), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), '', 'Arial', '', $this->ukuranhuruf);

        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+9', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), -5, 'Tanggal', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), 0, ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, ucwords($arrdata['aktaPengesahan']['tgl_dok']), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), '', 'Arial', '', $this->ukuranhuruf);

        #NPWP
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+5', '3.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), 0, 'NPWP Perusahaan', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), 0, ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, ucwords($arrdata['row']['npwp']), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), '', 'Arial', '', $this->ukuranhuruf);

        #Nama
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+5', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), 0, 'Nama', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), 0, ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, strtoupper($arrdata['row']['nama_pj']), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), '', 'Arial', '', $this->ukuranhuruf);

        #Alamat Rumah
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+5', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), 0, 'Alamat Rumah', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), 0, ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, ucfirst(trim($arrdata['row']['alamat_pj']) . ' Kel.' . $arrdata['row']['kel_pj'] . ' Kec. ' . $arrdata['row']['kec_pj'] . ' ' . $arrdata['row']['kab_pj'] . ' Prov. ' . $arrdata['row']['prop_pj']), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        #Telepon PJ
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+1', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), 0, 'No. Telepon', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), 0, ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, ucwords($arrdata['row']['telp_pj']), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), '', 'Arial', '', $this->ukuranhuruf);

        #Kantor Cabang
        // $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+5', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        // $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), 0, 'Kantor Cabang', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        // $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), 0, ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+5', '4.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), 0, 'Kantor Cabang', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), 0, ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        
        if ($arrdata['row']['status_dok'] == '02') {
            $this->pdf->SetTextColor(112, 121, 125);
            $this->pdf->SetFont('Arial', 'B', 60);
            $this->RotatedText(110, 260, "DICABUT", 0);   
            $this->pdf->SetTextColor(000, 000, 000);
        }
        
        if (count($arrdata['cabang']) > 0) {
            for ($i = 0; $i < count($arrdata['cabang']); $i++) {
                if ($i > 0)
                    $y = '+5';
                else
                    $y = 0;
                $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), $y, '- '. ucwords($arrdata['cabang'][$i]['nama_cabang']), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), '', 'Arial', '', $this->ukuranhuruf);
            }
        }else {
            $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, '-', 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), '', 'Arial', '', $this->ukuranhuruf);
        }

        #Bidang Kegiatan
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '+5', '5.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + ($lebarnomor * 2), 0, 'Bidang Kegiatan', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 2), 0, ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        if ($arrdata['kegiatan'] != "") {//(count($arrdata['cabang']) > 0) {
            $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, 'Jasa Survey: ', 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), 'M', 'Arial', '', $this->ukuranhuruf);
            $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, '- '.$arrdata['kegiatan'], 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), 'M', 'Arial', '', $this->ukuranhuruf - 2);
            /* for ($i=0; $i < count($arrdata['cabang']) ; $i++) {
              $this->tulis($x + ($lebarlabelstatic*2) + ($lebarnomor*3), '+5', ucwords('- '.$arrdata['cabang'][$i]['nama_cabang']), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic*2) - ($lebarnomor*2)), '', 'Arial', '', $this->ukuranhuruf);
              } */
            //$ex=explode(",",$arrdata['row']['kegiatan_usaha']);
            //if(count($ex) > 1){
            //$this->tulis($x + ($lebarlabelstatic*2) + ($lebarnomor*3 ), '+4', implode(",",$ex), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic*2) - ($lebarnomor*2) + 10), 'M', 'Arial', '', 10);
            //}else{
              // $ke = 1;
              // foreach ($arrdata['kegiatan'] as $keg ) {
              //   if ($ke == '1') {
              //       $pjg = '+4';
              //   }else{
              //       $pjg = '+1';
              //   }
              //   if ($keg != '') {
              //          $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3 ), $pjg, '- '.str_replace(")", "", $keg), 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), 'M', 'Arial', '', 10);
              //          $ke++;
              //      }   
              // }
            //}
        } else {
            $this->tulis($x + ($lebarlabelstatic * 2) + ($lebarnomor * 3), 0, 'Jasa Survey: -', 'J', ($dflength - $this->def_margin[3] - ($lebarlabelstatic * 2) - ($lebarnomor * 2)), '', 'Arial', '', $this->ukuranhuruf);
        }

        $this->pdf->Ln(2);
        #Pertama
        $this->tulis($x, 0, 'PERTAMA', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic, '', ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '', 'Memberikan Izin Usaha Jasa Survey kepada  ' . $arrdata['row']['nm_perusahaan'], 'L', ($dflength - $lebarnomor - $lebarlabelstatic - $this->def_margin[3]), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);
        #Kedua
        $this->tulis($x, 0, 'KEDUA', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic, '', ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '', 'Surat Izin Usaha Jasa Survey berlaku untuk seluruh Indonesia selama 5 (lima) tahun terhitung sejak tanggal diterbitkan.', 'J', ($dflength - $lebarnomor - $lebarlabelstatic - $this->def_margin[3]), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);
        #Ketiga
        $this->tulis($x, 0, 'KETIGA', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic, '', ':', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis($x + $lebarlabelstatic + $lebarnomor, '', 'Apabila ternyata terdapat kekeliruan dalam pemberian Surat Izin Usaha Jasa Survey ini dikemudian hari, akan diadakan peninjauan dan/atau pembetulan sebagaimana mestinya', 'J', ($dflength - $lebarnomor - $lebarlabelstatic - $this->def_margin[3]), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        #Area Signature
        //$this->pdf->Ln(2);
        $panjang = strlen($arrdata['row']['penerbit']);
        if ($panjang > 40) {
            $mulai = 40;
            $minus = 5;
        } else {
            $mulai = 30;
            $minus = 0;
        }

        $this->tulis($x + 50, '-2', 'a.n. MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12, 0, 13);
        if ($arrdata['row']['jabatan_ttd'] == '') {
            $this->tulis($x + 50, '+4', $arrdata['ttd']['jabatan'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 50, '+4', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }

        $this->tulis($x + 50, '+10', "TTD", 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 20, 0, 15);

        if ($arrdata['row']['nama_ttd'] == '') {
            $this->tulis($x + 50, '+13', $arrdata['ttd']['nama'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        } else {
            $this->tulis($x + 50, '+13', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        }
        #End Area Signature	
        #qrcode
        $this->pdf->Image('upL04d5'.$arrdata['foto_pemilik']['url'], 17, $this->pdf->GetY() - 35 , 30, 45);
        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 46), ($this->pdf->GetY() - 30), 20); #Coba diganti ganti aja 30 dan 15 nya.
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        //$this->tulis(140, 275, 'TTD', '', 25, '', 'Arial', 'I', 9);
        $this->tulis(20, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
       
       #ketentuan
        $this->pdf->Addpage();
        $this->tulis($x + 0.5, '+8', '  ', 'L', 180, '', 'Arial', '', 14, 1, 200);
        $this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10, 0, 13);
        $this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
        $this->tulis($x, '+8', 'KETENTUAN - KETENTUAN', 'C', $dflength - $this->def_margin[3], '', 'Arial', 'B', $this->ukuranhuruf + 2);
        $this->tulis($x, '+5', 'BAGI PEMILIK SURAT IZIN USAHA JASA SURVEY (SIUJS)', 'C', $dflength - $this->def_margin[3], '', 'Arial', 'B', $this->ukuranhuruf + 2);
        $this->tulis($x  + 20, '+8', 'A. UMUM', 'L', $dflength - $this->def_margin[3], '', 'Arial', 'B', $this->ukuranhuruf + 2);
        $this->tulis($x, '+5', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);

        $this->tulis($x + 20, '+5', '1.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Pemilik SIUJS dapat melakukan kegiatan di bidang usaha jasa di seluruh wilayah Indonesia sesuai bidang kegiatan survey yang dimiliki sepanjang Izin Usahanya masih Berlaku.', 'J', ($dflength - $kanan) - 50, 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x + 20, '+2', '2.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Perusahaan Jasa Survey, tidak diperkenankan melakukan kegiatan di bidang industri/produksi, pertambangan, maritime/pelayaran, asuransi, perdagangan umum serta usaha yang dapat menyebabkan perusahaan survey tidak dapat melakukan tugasnya dalam memberikan pendapatnya harus jujur, objektif dan memihak (independen) dan bertangung jawab dengan mengeluarkan survey report atau inspection sertivication', 'J', ($dflength - $kanan) - 50, 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x + 20, '+2', '3.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Perusahaan Jasa Survey dalam menjalankan kegiatan usahanya menggunakan ketentuan dan tata cara yang berlaku serta peraturan perundang-undangan yang berkaitan dengan survey.', 'J', ($dflength - $kanan) - 50, 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x + 20, '+6', 'B. KEWAJIBAN', 'L', $dflength - $this->def_margin[3], '', 'Arial', 'B', $this->ukuranhuruf + 2);

        $this->tulis($x + 20, '+7', '1.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Menyampaikan laporan kegiatan perusahaan setiap 1 (satu) tahun sekali.', 'J', ($dflength - $kanan) - 50, 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x + 20, '+2', '2.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Melaporkan secara tertulis setiap mempekerjakan tenaga ahli survey warga negara asing pendatang paling lambat 30 (tiga puluh) hari setelah TKWNAP tersebut dipekerjakan', 'J', ($dflength - $kanan) - 50, 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x + 20, '+2', '3.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Melaporkan perubahan pemegang saham, susunan direksi, dan nama perusahaan.', 'J', ($dflength - $kanan) - 50, 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x + 20, '+2', '4.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Melaporkan secara tertulis apabila perusahaan menutup kegiatan usahanya dengan mengembalikan SIUJS asli.', 'J', ($dflength - $kanan) - 50, 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x + 20, '+6', 'C. SANKSI', 'L', $dflength - $this->def_margin[3], '', 'Arial', 'B', $this->ukuranhuruf + 2);

        $this->tulis($x + 20, '+6', '', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor) + 20, 0, 'Pemilik SIUJS yang melakukan pelanggaran ketentuan ini dapat dikenakan sanksi pidana sesuai dengan perundang-undangan yang berlaku.', 'J', ($dflength - $kanan) - 50, 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor ) + 30, ($this->pdf->GetY() + 20), 15); #Coba diganti ganti aja 30 dan 15 nya.

        $this->tulis(20, 280, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
    
        }

    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */

    function generate($arrdata) {
        //$this->pdf->Output('./pdf/'.date('YmdHis').'.pdf','F');
        if ($arrdata['cetak'] != '') {
            $namafile = $arrdata['namafile'];
            $dir = $arrdata['dir'];
            if (file_exists($dir) && is_dir($dir)) {
                $path = 'upL04d5/document/DOKSIUJS/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile ;
            } else {
                if (mkdir($dir, 0777, true)) {
                    if (chmod($dir, 0777)) {
                        $path = 'upL04d5/document/DOKSIUJS/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile ;
                    }
                }
            }
            $this->pdf->Output($path, 'F');
        } else {
            $this->pdf->Output();
        }
    }

    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}

?>