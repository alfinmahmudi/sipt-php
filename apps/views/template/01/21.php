<?php
error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $def_margin = array(15,50,15,15);

    function pdf($orientasi, $ukuran, $kertas, $margins,$data){
        $this->ukuranhuruf = 12;
        $this->ukuranhurufdetil = 10;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->qrcode = new QRcode();
        $this->pdf->footer = 1;
		if(is_array($margins) and count($margins) == 4) 
		{
			$this->pdf->SetMargins($margins[0],$margins[1],$margins[2],$margins[3]); }
		else {

		$this->pdf->SetMargins($this->def_margin[0],$this->def_margin[1],$this->def_margin[2],$this->def_margin[3]);
		}
		$this->data($data, $margins);
    }

    function Footer(){
        if ($this->pdf->footer == 1) {
            #$this->pdf->SetY(-45);
            $this->pdf->SetFont('Arial', '', 9);
            $Yget2 = $this->pdf->GetY();
            $this->pdf->SetY($Yget2);
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
        }
    }
	
	/*----------------------------------------------------------- Common Function ---------------------------------------------------------*/
	function NewIkd(){
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }
    
    function NewIkdLandscape(){
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }
	
    function iif($kondisi, $benar, $salah){
        if ($kondisi) return $benar;
        else return $salah;
    }

    function convert_date($date){
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }
	
	function FormatNPWP($varnpwp){
		$varresult = '';
		$varresult = substr($varnpwp,0,2).".".substr($varnpwp,2,3).".".substr($varnpwp,5,3).".".substr($varnpwp,8,1)."-".substr($varnpwp,9,3).".".substr($varnpwp,12,3);
		return $varresult;
	}

	function FormatTDP($varnoijin){
		$varresult = '';
		$varresult = substr($varnoijin,0,2).".".substr($varnoijin,2,2).".".substr($varnoijin,4,1).".".substr($varnoijin,5,2).".".substr($varnoijin,7,5);
		return $varresult;
	}
	/*------------------------------------------------------------ End Common Function ----------------------------------------------------*/

	/*------------------------------------------------------------ ng'Batik Function -------------------------------------------------------*/
    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 3.7) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);
        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);
        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
            $this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }
	/*------------------------------------------------------------ End ngBatik Function -------------------------------------------------------*/
	
	/*------------------------------------------------------------ Parsing Data Function -------------------------------------------------------*/
	function data($arrdata, $margin){
        $this->NewIkd();
		$dflength = 174;
		$kiri = $this->def_margin[0];
		$kanan = $this->def_margin[3];
		$lebarstaticpendaftaran = 54;
		if(is_array($margin) and count($margin) == 4){
			$kiri = $margin[0];
			$kanan = $margin[3];
			if($kiri > $this->def_margin[0]){
				$ldef = $kiri - $this->def_margin[0];	
				$dflength = $dflength - $ldef;
			}else{
				$ldef = $this->def_margin[0]- $kiri;	
				$dflength = $dflength + $ldef;
			}
			
			if($kanan > $this->def_margin[3]){
				$rdef = $kanan - $this->def_margin[3];
				$dflength = $dflength - $rdef;
			}else{
				$rdef = $this->def_margin[3] - $kanan;
				$dflength = $dflength + $rdef;
			}
		}
		$x = $kiri;
		for($b=0;$b<=1;$b++){
		if($b == 1){
			$this->pdf->AddPage();
			$text = "(ARSIP)";
		}else{
			$text = "(ASLI)";
		}
		$this->tulis(($x+80), -17, $text, 'C', $dflength, '', 'Arial', 'B', 10);
		$this->tulis($x, '+8', 'SURAT TANDA PENDAFTARAN WARALABA', 'C', $dflength, '', 'Arial', 'B', 15);
		$this->tulis($x + 2, '+8', '(PEMBERI WARALABA BERASAL DARI '.strtoupper($arrdata['row']['produk_wrlb']).'SKALA MIKRO DAN KECIL)' , 'C', $dflength, '', 'Arial', 'B', 12);
		$this->pdf->Ln(8);
		#NOMOR
		$this->tulis($x + 0.5, '+1', '  NOMOR', 'L', 180, '', 'Arial', '', 10,1,10);
		$this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', $arrdata['row']['no_izin'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		#MASA BERLAKU
		$this->tulis($x + 0.5, '+7', '  MASA BERLAKU', 'L', 180, '', 'Arial', '', 10,1,10);
		$this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', $this->convert_date($arrdata['row']['tgl_ex']), 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		#KOTAK
		$this->tulis($x + 0.5, '-3', '  ', 'L', 180, '', 'Arial', '', 14,1,45);
		$this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

		#NAMA PERUSAHAAN
		$this->tulis($x + 0.5, '-3', '  Nama Perusahaan', 'L', 180, '', 'Arial', '', 10,0,30);
		$this->tulis($x + 55, '+8', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', strtoupper($arrdata['row']['nm_perusahaan']), 'L', ($dflength - $lebarstaticpendaftaran), 'B', 'Arial', '', 10);
		
		#alamat
		$this->tulis($x + 0.5, '+4.5', '  Alamat', 'L', 180, '', 'Arial', '', 10,0,10);
		$this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', ucfirst($arrdata['row']['almt_perusahaan']), 'L', ($dflength - $lebarstaticpendaftaran), 'M', 'Arial', '', 10);
		
		$this->tulis($x + 1, '+1.5', ' Telepon', 'L', 180, '', 'Arial', '', 10,0,10);
		$this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', $arrdata['row']['telp'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		
		$this->tulis($x + 1.5, '+4.5', ' Fax', 'L', 180, '', 'Arial', '', 10,0,10);
		$this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', $arrdata['row']['fax'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		
		$this->tulis($x + 1.5, '+4.5', ' Email', 'L', 180, '', 'Arial', '', 10,0,10);
		$this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', $arrdata['row']['email'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		
		#KOTAK
		$this->tulis($x + 0.5, '+8', '  ', 'L', 180, '', 'Arial', '', 14,1,38);
		$this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

		#NAMA PERUSAHAAN
		$this->tulis($x + 0.5, '-10', '  Nama Penanggung Jawab', 'L', 180, '', 'Arial', '', 10,0,30);
		$this->tulis($x + 55, '+8', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', strtoupper($arrdata['row']['nama_pj']), 'L', ($dflength - $lebarstaticpendaftaran), 'B', 'Arial', '', 10);
		
		#alamat
		$this->tulis($x + 0.5, '+4.5', '  Alamat', 'L', 180, '', 'Arial', '', 10,0,10);
		$this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', ucfirst($arrdata['row']['alamat_pj']), 'L', ($dflength - $lebarstaticpendaftaran), 'M', 'Arial', '', 10);
		
		$this->tulis($x + 1, '+1.5', ' Telepon', 'L', 180, '', 'Arial', '', 10,0,10);
		$this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', $arrdata['row']['telp_pj'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		
		$this->tulis($x + 1.5, '+4.5', ' Fax', 'L', 180, '', 'Arial', '', 10,0,10);
		$this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', $arrdata['row']['fax_pj'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		
		$this->tulis($x + 1.5, '+4.5', ' Email', 'L', 180, '', 'Arial', '', 10,0,10);
		$this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', $arrdata['row']['email_pj'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

		// #KOTAK
		// $this->tulis($x + 0.5, '-3', '  ', 'L', 180, '', 'Arial', '', 14,1,45);
		// $this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		// $this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);

		// $this->pdf->Ln(4);
		// $this->tulis($x + 0.5, '-3', '  Nama Penanggung Jawab', 'L', 180, '', 'Arial', '', 10,0,30);
		// $this->tulis($x + 55, '+8', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		// $this->tulis($x + 60, '+5', strtoupper($arrdata['row']['nama_pj']), 'L', ($dflength - $lebarstaticpendaftaran), 'B', 'Arial', '', 10);
		
		// $this->tulis($x + 0.5, '+4.5', '  Alamat', 'L', 180, '', 'Arial', '', 10,0,10);
		// $this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		// $this->tulis($x + 60, '+5', ucfirst($arrdata['row']['alamat_pj']), 'L', ($dflength - $lebarstaticpendaftaran), 'M', 'Arial', '', 10);
		
		// $this->tulis($x + 1, '+1.5', ' Telepon', 'L', 180, '', 'Arial', '', 10,0,10);
		// $this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		// $this->tulis($x + 60, '+5', $arrdata['row']['telp_pj'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		
		// $this->tulis($x + 1.5, '+4.5', ' Fax', 'L', 180, '', 'Arial', '', 10,0,10);
		// $this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		// $this->tulis($x + 60, '+5', $arrdata['row']['fax_pj'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		
		// $this->tulis($x + 1.5, '+4.5', ' Email', 'L', 180, '', 'Arial', '', 10,0,10);
		// $this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		// $this->tulis($x + 60, '+5', $arrdata['row']['email_pj'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		
		
		$this->tulis($x + 0.5, '-3', '  Barang / Jasa Objek', 'L', 180, '', 'Arial', '', 10,0,30);
		$this->tulis($x + 55, '+8', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', $arrdata['row']['jenis_dagang'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		
		#alamat
		$this->tulis($x + 0.5, '-2', '  ', 'L', 180, '', 'Arial', '', 10,1,18);
		$this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		
		$this->tulis($x + 0.5, '+3', '  Waralaba', 'L', 180, '', 'Arial', '', 10,0,10);
		$this->tulis($x + 55, '-3', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		
		$this->tulis($x + 1.5, '+3', ' Merk', 'L', 180, '', 'Arial', '', 10,0,10);
		$this->tulis($x + 55, '-3', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', $arrdata['row']['merk_dagang'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		
		$this->tulis($x + 1.5, '+6', ' Pemberi Waralaba Wajib :', 'L', 180, '', 'Arial', '', 10,0,10);
		$this->tulis($x + 1.5, '+5', '- Menyampaikan Laporan Tahunan Kegiatan Waralaba; ', 'L', 180, '', 'Arial', '', 10,0,10);
		$this->tulis($x + 1.5, '+8', '- Bekerjasama dengan Pengusaha Kecil dan Menengah didaerah setempat sebagai Penerima Waralaba atau pemasok barang dan /atau jasa sepanjang memenuhi ketentuan persyaratan; ', 'L', 180, 'M', 'Arial', '', 10,0,10);
		$this->tulis($x + 1.5, '+1', '- Mengutamakan penggunaan barang dan /atau jasa hasil produksi dalam negeri sepanjang memenuhi standar mutu yang ditetapkan; ', 'L', 180, 'M', 'Arial', '', 10,0,10);
		$this->tulis($x + 1.5, '+1', '- Melakukan pembinaan kepada Penerima Waralaba secara berkesinambungan; ', 'L', 180, 'M', 'Arial', '', 10,0,10);
		$this->tulis($x + 0.5, '-35', '', 'L', 180, '', 'Arial', '', 10,1,40);
		
		$this->pdf->Ln(55);
		$this->tulis($x + 75, '+', 'Jakarta,'.$this->convert_date($arrdata['row']['tgl_aju']), 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,10);
		$this->tulis($x + 75, '+5', 'a.n.MENTERI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,10);
		$this->tulis($x + 75, '+5', $arrdata['row']['jabatan_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,10);
		$this->tulis($x + 75, '+5', 'Bina Usaha Perdagangan', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,10);
		$this->tulis($x + 75, '+25', $arrdata['row']['nama_ttd'], 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,10);

		$string = utf8_decode($arrdata['row']['nm_perusahaan']."\n".$this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-')."\n".$arrdata['row']['tgl_izin']."\n".$arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
		$this->qrcode->disableBorder();
		$this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor + 80), ($this->pdf->GetY() - 15), 20); #Coba diganti ganti aja 30 dan 15 nya.

		$this->tulis($x, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui sistem SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'I', 7);
		$this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
		/*
		$this->pdf->Ln(13);
		#Border Nomor Telepon
		
		
		//$this->pdf->Ln(5);
		
		#Border Atas
		
		
		//$this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext, $x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 11);
		//$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
		
		/*
		$this->pdf->SetFont('Arial','',10);
		$this->pdf->SetFillColor(30,144,255);
		$this->pdf->MultiCell(0,8,"		NOMOR                                         :   //STP/9191919191911919",1,1,true);
		
		$this->pdf->SetFont('Arial','',10);
		$this->pdf->SetFillColor(30,144,255);
		$this->pdf->MultiCell(0,8,"		BERLAKU SAMPAI DENGAN       :   2020",1,1,true);
		*/
		//$this->pdf->tulis($x + 0.5, 0, '  NOMOR ', 'C', 180, '', 'Arial', '', 10,1,10);
		//$this->pdf->tulis($x + 55, '-1', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
	
		//$this->tulis($x + 60, '0', 'FAX//1234567' , 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		
		
		#Border Kegiatan Usaha
		//$this->pdf->Ln();
		//$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
		
		/*
		$this->tulis($x + 0.5, '+11', '  BERLAKU SAMPAI DENGAN', 'L', 180, '', 'Arial', '', 10,1,10);
		$this->tulis($x + 55, '-2', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+5', 'MARET 2020', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		//$this->pdf->Ln(8);
		//$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
		$this->pdf->Ln(10);
		
		
		$this->tulis($x + 55, '-5', 'DIBERIKAN KEPADA :' , '', 70, '', 'Arial', '', 10,0,13);
		$this->pdf->Ln(5);
		#nama perusahaan
		$this->tulis($x + 1, 0, '1.	NAMA PERUSAHAAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '0', strtoupper($arrdata['row']['nm_perusahaan']) , '', 70, '', 'Arial', 'B', 10,0,13);
		
		#alamat
		$this->tulis($x + 1, '+5', '    ALAMAT ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '0', ucfirst($arrdata['row']['almt_perusahaan']) , '', 70, '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '+4', 'Kel. '.$arrdata['row']['kel_perusahaan'].', Kec. '.$arrdata['row']['kec_perusahaan'].', '.$arrdata['row']['prop_perusahaan'] , '', 70, '', 'Arial', '', 10,0,13);
		
	
		#tlp/fax
		$this->tulis($x + 1, '+5', '    NO. TELP / FAX ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '0', $arrdata['row']['telp'].' / '.$arrdata['row']['fax'] , '', 70, '', 'Arial', '', 10,0,13);
		
		$this->pdf->Ln(8);
		#nama pj
		$this->tulis($x + 1, '+', '2.	NAMA PENANGGUNG JAWAB ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '0', strtoupper($arrdata['row']['nama_pj']) , '', 70, '', 'Arial', 'B', 10,0,13);
		#jabatan
		$this->tulis($x + 1, '+4', '    JABATAN ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 55, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 60, '0', $arrdata['row']['jabatan_pj'] , '', 70, '', 'Arial', '', 10,0,13);
		
		
		$this->pdf->Ln(10);
		$this->tulis($x + 1, '+5', 'Telah mendaftarkan Petunjuk Penggunaan(Manual) dan Kartu Jaminan/Garansi Purna Jual dalam Bahasa Indonesia, sebagai berikut :', 'L', $dflength, 'M', 'Arial', '', 10,0);
		$this->tulis($x, '+5', ' JENIS PRODUK ', 'C', 80, '', 'Arial', 'B', 10,1,10);
		$this->tulis($x + 80, 0, ' MERK ', 'C', 30, '', 'Arial', 'B', 10,1,10);
		$this->tulis($x + 110, 0, ' TIPE MODEL ', 'C', 70, '', 'Arial', 'B', 10,1,10);
		/*
		for($i=0;$i<7;$i++){
			$this->tulis($x, '+10', $res['produk'], 'C', 80, '', 'Arial', 'B', 10,1,10);
			$this->tulis($x + 80, 0, $res['merk'], 'C', 30, '', 'Arial', 'B', 10,1,10);
			$this->tulis($x + 110, 0,  $res['tipe'], 'C', 70, '', 'Arial', 'B', 10,1,10);
		}
		
		if(count($arrdata['p']) != 0 || count($arrdata['p']) < 7){
			$i=0;
			foreach($arrdata['p'] as $res){
			$this->tulis($x, '+10', $res['produk'], 'C', 80, '', 'Arial', 'B', 10,1,10);
			$this->tulis($x + 80, 0, $res['merk'], 'C', 30, '', 'Arial', 'B', 10,1,10);
			$this->tulis($x + 110, 0,  $res['tipe'], 'C', 70, '', 'Arial', 'B', 10,1,10);
			$i++;
			}
		}else{
			$this->tulis($x + 80, 0, ' TERLAMPIR ', 'C', 30, '', 'Arial', 'B', 10,0,10);
		}
		
		$this->pdf->Ln(15);
		#footer
		#terbit
		$this->tulis($x + 105, '+', 'Diterbitkan di ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
		$this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 137, '0', 'J A K A R T A' , '', 70, '', 'Arial', 'B', 10,0,13);
		#tanggal
		$this->tulis($x + 105, '+4', 'pada tanggal ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
		$this->tulis($x + 133, 0, ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + 137, '0', '   OKTOBER 2015' , '', 70, '', 'Arial', 'B', 10,0,13);
		
		$this->pdf->Ln(10);
		$this->tulis($x + 75, '+', 'a.n. MENTRI PERDAGANGAN', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
		$this->tulis($x + 75, '+4', 'Direktur Bina Usaha Perdagangan', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		
		$this->pdf->Ln(20);
		$this->tulis($x + 55, '+', 'Fetnayeti', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 12,0,13);
		
		
		if(count($arrdata['tembusan']) > 0){
			#Tembusan
			$this->tulis($x, '+10','','L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
			$this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
			for ($i=0; $i < count($arrdata['tembusan']) ; $i++) { 
				$this->tulis($x, '+6', $arrdata['tembusan'][$i]['urutan'].'.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
				$this->tulis(($x + $lebarnomor), 0, $arrdata['tembusan'][$i]['keterangan'], 'L', ($dflength-$kanan), '', $this->huruf, '', $this->ukuranhuruf);
			}
		}
		
		
		/*
		
		$this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran) / 2), $YNext, $x + (($dflength - $lebarstaticpendaftaran) / 2), $YNow + 11);
		$this->tulis($x, 0, 'NOMOR TDP', 'C', (($dflength - $lebarstaticpendaftaran) / 2), '', 'Arial', '', 9);
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), 0, 'BERLAKU S/D TANGGAL', 'C', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', '', 9);
		
		
		$this->pdf->Line($x + ($dflength - $lebarstaticpendaftaran), $YNext, $x + ($dflength - $lebarstaticpendaftaran), $YNow + 11);
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 1.5), '', 'PENDAFTARAN  : '.$arrdata['row']['pendaftaran'], '', 54, '', 'Arial', '', 9);
		$this->pdf->Ln(6);
		
		#Nomor TDP
		$this->tulis($x, 0, $this->FormatTDP($arrdata['row']['no_ijin']), 'C', (($dflength - $lebarstaticpendaftaran) / 2), '', 'Arial', 'B', 11);
		
		#Tanggal TDP
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) / 2), 0, $this->iif($arrdata['row']['tgl_akhir'], strtoupper($this->convert_date($arrdata['row']['tgl_akhir']))), 'C', (($dflength - $lebarstaticpendaftaran) - (($dflength - $lebarstaticpendaftaran) / 2)), '', 'Arial', 'B', 11);
		$this->pdf->Line($x + (($dflength - $lebarstaticpendaftaran)), $YNext + 6.3, $x + $dflength, $YNext + 6.3);
		
		#PEMBAHARUAN
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 1.5), '', 'PEMBAHARUAN : '.($arrdata['row']['pembaharuan'] == 0 ? '-' : $arrdata['row']['pembaharuan']), '', 54, '', 'Arial', '', 9);
		
		
		$YSec = $this->pdf->GetY();
		$this->pdf->SetY($YSec);
		$YNow = $this->pdf->GetY();
		$this->pdf->Line($x, $YNext, ($x + $dflength), $YNext);
		$this->pdf->Line($x, $YNext, $x, $YNow + 5);
		$this->pdf->Line(($x + $dflength), $YNext, ($x + $dflength), $YNow + 5);
		$this->pdf->Line($x, $YNow + 5, ($x + $dflength), $YNow + 5);
		$this->pdf->Ln(10);
		
		#Border Atas
		
		$ystartpoint = $this->pdf->GetY();
		
		$this->pdf->Line($x, $ystartpoint, ($x + $dflength), $ystartpoint);
		$this->tulis($x + 1.5, '+3', 'NAMA PERUSAHAAN : ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 9);
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '', 'STATUS : ', '', 41.5, '', 'Arial', '', 9);
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '+6', strtoupper($arrdata['row']['ur_status_perusahaan']), '', 41.5, '', 'Arial', '', 10);
		$this->pdf->newFlowingBlock(($dflength - $lebarstaticpendaftaran), 5, 'W', 'J', 0, false);
		$this->tulis($x + 1.5, '', strtoupper($arrdata['row']['nama']), 'L', 10, 'W', 'Arial', 'B',13);
        $this->pdf->finishFlowingBlock(); 
		$this->pdf->Ln(6);
		$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
		$this->tulis($x + 1.5, 0, 'PENANGGUNG JAWAB / PENGURUS : '.strtoupper($arrdata['row']['nama_pemilik']), 'L', ($dflength - 1.5), '', 'Arial', '', 10, 0, 13);
		
		#Border Alamat
		$this->pdf->Ln();
		$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
		$this->tulis($x + 1.5, '+3', 'ALAMAT : ', 'L', 25, '', 'Arial', '', 9);
		$rowalamat = strtoupper($arrdata['row']['alamat']);
		$kelurahan = 'KEL';
		$pos=strpos($rowalamat,$kelurahan);
		if($pos !== false){
			$alamat1=explode('KEL.',$alamat);	
			$this->tulis($x + 20, 0, $alamat1[0], 'L', 5, '', 'Arial', '', 10);
			$this->tulis($x + 25, 0, $alamat1[1], 'L', ($dflength - 20), 'M', 'Arial', '', 10);
		}else{
			$this->tulis($x + 20, 0, strtoupper($arrdata['row']['alamat']), 'L', ($dflength - 20), 'M', 'Arial', '',10);
			$this->tulis($x + 20, '+1', 'KEL. '. strtoupper($arrdata['row']['ur_kel']).' KEC. '.strtoupper($arrdata['row']['ur_kec']), 'L', ($dflength - 20), 'M', 'Arial', '', 10);
			$this->tulis($x + 20, '+1', strtoupper($arrdata['row']['urai_kota']).' '.$arrdata['row']['ur_pos'], 'L', ($dflength - 20), '', 'Arial', '',10);
			$this->pdf->Ln(2);
		}
		
		#Border NPWP
		$this->pdf->Ln();
		$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
		$this->tulis($x + 1.5, 0, 'NPWP   :   '.$this->FormatNPWP($arrdata['row']['npwp']), 'L', ($dflength - 1.5), '', 'Arial', '', 10, 0, 13);
		
		#Border Nomor Telepon
		$this->pdf->Ln();
		$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
		$this->tulis($x + 1.5, 0, 'NOMOR TELEPON :         '.$arrdata['row']['telepon'], 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0,13);
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) - 14), '', 'FAX :          '.$arrdata['row']['fax'], '', 70, '', 'Arial', '', 10,0,13);
		
		
		#Border Kegiatan Usaha
		$this->pdf->Ln();
		$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
		
		$this->tulis($x + 1.5, '+3', 'KEGIATAN USAHA POKOK : ', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '', 'KBLI : ', 'C', 41.5, '', 'Arial', '', 10);
		
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 12), '+5', $arrdata['row']['kbli'], 'C', 41.5, '', 'Arial', '', 10);
		$this->tulis($x + 10, '', str_replace(chr(10),' ',strtoupper($arrdata['row']['pokok'])), 'J', ($dflength - $lebarstaticpendaftaran), 'M', 'Arial', '', 10);
		
		#Border Bawah Pisan
		$this->pdf->Ln();
		$this->pdf->Line($x, $this->pdf->GetY(), ($x + $dflength), $this->pdf->GetY());
		$this->pdf->Line($x, $ystartpoint, $x, $this->pdf->GetY());
		$this->pdf->Line(($x + $dflength), $ystartpoint, ($x + $dflength), $this->pdf->GetY());
		#End Border Bawah Pisan
		
		#Area Signature
		$this->pdf->Ln(8);
		$panjang = strlen($arrdata['row']['penerbit']);
		if ($panjang > 40){
		$mulai = 40;
		$minus = 5;
		}else{
		$mulai = 30;
		$minus = 0;
		}
		
		$this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '', 'Dikeluarkan di', '', 25, '', 'Arial', '', 10);
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran)), '', ':', '', 5, '', 'Arial', '', 10);
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', strtoupper($arrdata['row']['urai_kota']), '', 55, '', 'Arial', '', 10);
		
		$this->tulis($x + 1.5, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '', 'Pada Tanggal', '', 25, '', 'Arial', '', 10);
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran)), '', ':', '', 5, '', 'Arial', '', 10);
		$this->tulis($x + (($dflength - $lebarstaticpendaftaran) + 5), '', $this->iif($arrdata['row']['tgl_ijin'], strtoupper($this->convert_date($arrdata['row']['tgl_ijin']))), '', 55, '', 'Arial', '', 10);
		
		$this->pdf->Ln(8);
		$this->tulis($x + 1.5, '', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
		$this->tulis($x + (($dflength / 2) - ($panjang/2)), '', 'KEPALA '.strtoupper($arrdata['row']['penerbit']), 'C', (($dflength / 2) + ($panjang/2)), 'M', 'Arial', '', 10);

		$this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
		$this->tulis($x + (($dflength / 2) - 10-$minus), '', 'SELAKU', 'C', (($dflength / 2) + 10), 'M', 'Arial', '', 10);

		$this->tulis($x + 1.5, 0, '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
		$this->tulis($x + (($dflength / 2) - 10-$minus), '', 'KEPALA KANTOR PENDAFTARAN PERUSAHAAN', 'C', (($dflength / 2) + 10), 'M', 'Arial', '', 10);
		
		$this->pdf->Ln(34);
		$this->tulis($x + 1.5, '', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
		$this->tulis($x + (($dflength / 2) - 10-$minus), '', $arrdata['row']['nama_ttd'], 'C', (($dflength / 2) + 10), '', 'Arial', 'U', 10);
		
		$this->tulis($x + 1.5, '+5', '', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10,0);
		$this->tulis($x + (($dflength / 2) - 10-$minus), '', 'NIP. '. $arrdata['row']['nip_ttd'], 'C', (($dflength / 2) + 10), '', 'Arial', '', 10);
		#End Area Signature
		*/
		}
    }
	/*------------------------------------------------------------ End Parsing Data Function ---------------------------------------------------*/    
	/*------------------------------------------------------------ Generate Function -----------------------------------------------------------*/
	function generate($arrdata){
        if($arrdata['cetak'] != ''){
        	$namafile = $arrdata['namafile'];
        	$dir = $arrdata['dir']; 
        	if(file_exists($dir) && is_dir($dir)){
				$path = 'upL04d5/document/DOKWRPW/'. date("Y")."/".date("m")."/".date("d").'/'.$namafile;
			}else{
				if(mkdir($dir, 0777, true)){
					if(chmod($dir, 0777)){
						$path = 'upL04d5/document/DOKWRPW/'. date("Y")."/".date("m")."/".date("d").'/'.$namafile;
					}
				}
			}
        	$this->pdf->Output($path,'F');	
        }
        else{
			$this->pdf->Output();
		}
    }
	/*------------------------------------------------------------ End Generate Function -------------------------------------------------------*/

}

?>