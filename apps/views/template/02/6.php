<?php

error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');

class pdf {

    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $ukuranhurufkop;
    var $huruf;
    var $def_margin = array(15, 40, 20, 15);
    var $qrCodeString;
    var $angle=0;

    function Rotate($angle,$x=-1,$y=-1)
    {
	if($x==-1)
		$x=$this->x;
	if($y==-1)
		$y=$this->y;
	if($this->angle!=0)
		$this->pdf->_out('Q');
	$this->angle=$angle;
	if($angle!=0)
	{
		$angle*=M_PI/180;
		$c=cos($angle);
		$s=sin($angle);
		$cx=$x*$this->k;
		$cy=($this->h-$y)*$this->k;
		$this->pdf->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
	}
    }
    
    function _endpage()
    {
	if($this->angle!=0)
	{
		$this->angle=0;
		$this->pdf->_out('Q');
	}
	parent::_endpage();
    }
    
    function pdf($orientasi, $ukuran, $kertas, $margins, $data) {
        $this->ukuranhuruf = 12;
        $this->ukuranhurufdetil = 10;
        $this->ukuranhurufkop = 14;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->huruf = 'Arial';
        $this->qrcode = new QRcode();
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->pdf->footer = 1;
        if (is_array($margins) and count($margins) == 4) {
            $this->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
        } else {

            $this->pdf->SetMargins($this->def_margin[0], $this->def_margin[1], $this->def_margin[2], $this->def_margin[3]);
        }
        $this->data($data, $margins);
    }
    
    function RotatedText($x, $y, $txt, $angle)
    {
	//Text rotated around its origin
	$this->Rotate($angle,$x,$y);
	$this->pdf->Text($x,$y,$txt);
	$this->Rotate(0);
    }

    function Footer($arrdata) {
        if ($this->pdf->footer == 1) {
            $this->pdf->SetY(-35);
            $this->pdf->SetFont('Arial', '', 9);
            /* $Yget2 = -60;
              $this->pdf->SetY($Yget2); */
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
        }
    }

    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */

    function NewIkd($arrdata) {
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Footer($arrdata);
        $this->pdf->SetY($Yget);
    }

    function NewIkdLandscape() {
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }

    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }

    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }

    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */

    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */

    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 4) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);

        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));

        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));

        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);

        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
        //$this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
            $this->pdf->MultiCell($lebar, $tinggi, $teks, 0, $align, 0);
        else if ($tipe == "I")
            $this->pdf->Image('upL04d5' . $teks, $xpos, $ypos, 30, 40);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }

    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */

    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */

    function data($arrdata, $margin) {
        $this->NewIkd($arrdata);
        $dflength = 192;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarlabelstatic = 70;
        $lebarnomor = 8;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }

            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        }
        if($arrdata['row']['status'] != 1000){
            $this->pdf->SetTextColor(206, 206, 206);
            $this->pdf->SetFont('Arial', 'B', 60);
            $this->RotatedText(160,230, "D    R    A    F    T", 40);   
            $this->pdf->SetTextColor(000, 000, 000);
        }
        $x = $kiri;

        $this->tulis($x, '+6', 'SURAT KETERANGAN PENGECER MINUMAN BERALKOHOL GOLONGAN A', 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);
        $this->tulis($x, '+6', '(SKP-A)', 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);

        $this->tulis($x, '+8', 'Nomor : ' . $arrdata['row']['no_izin'], 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);

        $this->pdf->Ln(8);
        #Nama Perusahaan
        $this->tulis($x, '+2', '1.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Nama Perusahaan', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', 'Arial', '', $this->ukuranhuruf);
//        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['nm_perusahaan'].' ('.$arrdata['row']['nm_usaha'].')'), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', 'Arial', 'B', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, strtoupper($arrdata['row']['nm_usaha']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', 'Arial', 'B', $this->ukuranhuruf);
        #Alamat Perusahaan
        $this->tulis($x, '+1', '2.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Alamat Kantor Perusahaan', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, trim($arrdata['row']['almt_usaha']) . ', Kel. ' . ucwords(trim($arrdata['row']['kel_perusahaan'])) . ', Kec. ' . ucwords(trim($arrdata['row']['kec_perusahaan'])) . ', ' . ucwords(trim($arrdata['row']['kab_perusahaan'])) . ', Prop. ' . ucwords(trim($arrdata['row']['prop_perusahaan'])), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'No. Telp/Fax: ' . $this->iif($arrdata['row']['telp_usaha'], $arrdata['row']['telp_usaha'], '-') . '/' . $this->iif($arrdata['row']['fax_usaha'], $arrdata['row']['fax_usaha'], '-'), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);


        #Penanggung Jawab
        $this->tulis($x, '+1', '3.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Nama Pemilik / Penanggung Jawab', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, strtoupper($arrdata['row']['nama_pj_usaha']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf);


        #Alamat Penanggung Jawab
        $this->tulis($x, '+6', '4.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Alamat Pemilik / Penanggung Jawab', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords(trim($arrdata['row']['alamat_pj_usaha'])), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        #NPWP
        $this->tulis($x, '+2', '5.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Nomor Pokok Wajib Pajak', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['npwp']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf);

        #Nilai modal
        $this->tulis($x, '+6', '6.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Nilai Modal dan Kekayaan Bersih', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', 'Arial', '', $this->ukuranhuruf);
        if (trim($arrdata['row']['nilai_modal']) == 'Rp. 0,-') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, '-', 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf);   
        }else{
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['nilai_modal']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf);
        }

        #Kegiatan Usaha
        $this->tulis($x, '+6', '7.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Kegiatan Usaha', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'Perdagangan Barang', 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf, 0, 5);

        #Kelembagaan
        $this->tulis($x, '+6', '8.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Kelembagaan', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, 'Pengecer', 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf, 0, 5);

        #Bidang Usaha
        $this->tulis($x, '+6', '9.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Bidang Usaha (Sesuali KBLI 2009)', 'L', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, '46333 (Perdagangan Besar Minuman Beralkohol)', 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        if ($arrdata['row']['status_dok'] == '02') {
            $this->pdf->SetTextColor(112, 121, 125);
            $this->pdf->SetFont('Arial', 'B', 60);
            $this->RotatedText(110, 260, "DICABUT", 0);   
            $this->pdf->SetTextColor(000, 000, 000);
        }
        #Jenis Minuman

        $this->tulis($x, '+1', '10.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $yjenisA = $this->pdf->GetY();
        $this->tulis(($x + $lebarnomor), $yjenis, 'Jenis Minuman Beralkohol', 'L', $lebarlabelstatic, 'M', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), $yjenisA, ':', 'L', 5, 'M', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), $yjenisA, 'Golongan A ', 'J', 25, 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);
        // $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5 + 25)), $yjenisA, implode(",", $arrdata['jns_gol_a']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5 + 25)), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x, '+1', '11.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Surat Keterangan Pengecer Minuman Beralkohol Golongan A ini berlaku untuk melakukan kegiatan usaha perdagangan minuman beralkohol golongan A di wilayah ' . $arrdata['pemasaran'] . ', sesuai Surat Penunjukan sebagai Pengecer dari '.$arrdata['suratSKPA'], 'J', ($dflength - $lebarnomor), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

            // $bnyksurat = explode(",", $arrdata['suratSKPA']);
            // $next_num = 11;
            // foreach ($bnyksurat as $key) {
                
            //     $next_num++;
            // }
            #Keterangan 12
            $this->tulis($x, '+1', '12.', 'L', $lebarnomor, '', 'Arial', '', $this->ukuranhuruf);
            $this->tulis(($x + $lebarnomor), 0, 'Surat Keterangan Pengecer Minuman Beralkohol Golongan A ini diberikan dengan ketentuan sebagaimana tercantum dalam halaman kedua :', 'J', ($dflength - $lebarnomor), 'M', 'Arial', '', $this->ukuranhuruf, 0, 5);

        $yttd = $this->pdf->GetY();

        #Dikeluarkan di 
        //$this->tulis($x, '+6','', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 25)), '+5', 'Dikeluarkan di', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 55)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 57)), 0, 'J A K A R T A', 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, 'B', $this->ukuranhuruf);

        #Tgl Awal
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 25)), '+5', 'Pada Tanggal', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 55)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 57)), 0, ucwords($arrdata['row']['tgl_izin']), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);

        #Tgl Akhir 
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 25)), '+5', 'Berlaku s/d', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 55)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        if (trim($arrdata['row']['tgl_izin_exp']) == '') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 57)), 0, ucwords(date_indo($arrdata['suggest']['tgl_izin_exp'])), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf);
        }else{
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 57)), 0, ucwords($arrdata['row']['tgl_izin_exp']), 'L', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', 'Arial', '', $this->ukuranhuruf);
        }

        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+10', 'a.n. MENTERI PERDAGANGAN', 'C', $lebarlabelstatic, '', 'Arial', '', $this->ukuranhuruf);

        if ($arrdata['row']['jabatan_ttd'] == '') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 30)), '+5', $arrdata['ttd']['jabatan'], 'C', $lebarlabelstatic + 10, 'M', 'Arial', '', $this->ukuranhuruf);
        }else{
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 30)), '+5', $arrdata['row']['jabatan_ttd'], 'C', $lebarlabelstatic + 10, 'M', 'Arial', '', $this->ukuranhuruf);
        }

        if ($arrdata['row']['status'] == '1000') {
        // $this->tulis($x + (($dflength - $lebarstaticpendaftaran) - $mulai), '+5', 'Bina Usaha Perdagangan', '', 25, '', 'Arial', '', 10);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+7', 'TTD', 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        }else{
            $this->pdf->ln(15);
        }
        if ($arrdata['row']['nama_ttd'] == '') {
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+7', $arrdata['ttd']['nama'], 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        }else{
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 35)), '+7', $arrdata['row']['nama_ttd'], 'C', $lebarlabelstatic, 'M', 'Arial', '', $this->ukuranhuruf);
        }

        #qrcode
        $string = utf8_decode($arrdata['row']['nm_perusahaan'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        if ($arrdata['row']['status'] == '1000') {
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor), ($this->pdf->GetY() - 35), 25); #Coba diganti ganti aja 30 dan 15 nya.
        }else{
            $this->pdf->SetXY($x + ($lebarlabelstatic + $lebarnomor),$this->pdf->GetY() - 35);
            $this->pdf->drawTextBox('QR CODE', 30, 30, 'C', 'M');
        }
        // $this->pdf->Ln(30);
        // #Tembusan
        // if (count($arrdata['tembusan']) > 0) {
        //     $this->tulis($x, '+10', '', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        //     $this->tulis($x, 0, 'Tembusan :', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf - 4);
        //     for ($i = 0; $i < count($arrdata['tembusan']); $i++) {
        //         $this->tulis($x, '+4', $arrdata['tembusan'][$i]['urutan'] . '.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf - 4);
        //         $this->tulis($x + 3.2, 0, $arrdata['tembusan'][$i]['keterangan'] . ';', 'L', ($dflength - $kanan), '', $this->huruf, '', $this->ukuranhuruf - 4);
        //     }
        // }



        $this->tulis($x, 330, 'Dokumen ini sah, diterbitkan secara elektronik melalui sistem SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        #ketentuan
        $this->pdf->Addpage();
        $this->tulis($x, '+3', '', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Surat Keterangan Pengecer atau Penjual Langsung Minuman Berlkohol Golongan A ini ditetapkan dengan ketentuan sebagai berikut :', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x, '+6', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Berlaku untuk melakukan kegiatan usaha perdagangan minuman beralkohol Golongan A di wilayah pemasaran yang disebutkan pada nomor 11 dengan masa berlaku sebagaimana ditetapkan dalam SKP-A atau SKPL-A ini.', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x, '+6', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Perusahaan wajib menjalankan kegiatan usaha berdasarkan ketentuan perundang-undangan yang berlaku dan menyampaikan laporan Realisasi Pengadaan dan Penyaluran minuman beralkohol setiap triwulan tahun kalender berjalan kepada Direktur Jenderal Perdagangan Dalam Negeri Kementerian Perdagangan sebagai berikut : ', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        $this->tulis(($x + $lebarnomor), '+1', 'a.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor + 5), 0, 'Triwulan I disampaikan pada tanggal 31 Maret ', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        $this->tulis(($x + $lebarnomor), '+1', 'b.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor + 5), 0, 'Triwulan II disampaikan pada tanggal 30 Juni ', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        $this->tulis(($x + $lebarnomor), '+1', 'c.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor + 5), 0, 'Triwulan III disampaikan pada tanggal 30 September ', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        $this->tulis(($x + $lebarnomor), '+1', 'd.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor + 5), 0, 'Triwulan IV disampaikan pada tanggal 31 Desember ', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x, '+6', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Perusahaan wajib memberitahukan setiap ada perubahan pada perusahaan, yang menyebabkan  SKP-A atau SKPL-A ini tidak sesuai dengan keadaan perusahaan, kepada Direktur Jenderal Perdagangan Dalam Negri Kementerian Perdagangan.', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);

        $this->tulis($x, '+6', '4.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'SKP-A atau SKPL-A mempunyai masa berlaku sesuai dengan masa berlaku perjanjian tertulis dengan ketentuan paling lama 3 (tiga) tahun terhitung sejak tanggal diterbitkan dan dapat diperpanjang. Perpanjangan SKP-A atau SKPL-A dilakukan 1 (satu) bulan sebelum masa berlakunya berakhir.', 'J', ($dflength - $kanan), 'M', $this->huruf, '', $this->ukuranhuruf, 0, 5);
        $this->tulis(20, 340, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
        if ($arrdata['row']['status'] == '1000') {
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + ($lebarlabelstatic + $lebarnomor ), ($this->pdf->GetY() - 35), 15); #Coba diganti ganti aja 30 dan 15 nya.
        }else{
            $this->pdf->SetXY($x + ($lebarlabelstatic + $lebarnomor),$this->pdf->GetY() - 35);
            $this->pdf->drawTextBox('QR CODE', 20, 20, 'C', 'M');
        }
    }

    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */

    function generate($arrdata) {
        //  $this->pdf->Output();
        if ($arrdata['cetak'] != '') {
            $namafile = $arrdata['namafile'];
            $dir = $arrdata['dir'];
            if (file_exists($dir) && is_dir($dir)) {
                $path = 'upL04d5/document/DOKSKPA/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile ;
            } else {
                if (mkdir($dir, 0777, true)) {
                    if (chmod($dir, 0777)) {
                        $path = 'upL04d5/document/DOKSKPA/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile ;
                    }
                }
            }
            $this->pdf->Output($path, 'F');
        } else {
            $this->pdf->Output();
        }
    }

    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}

?>