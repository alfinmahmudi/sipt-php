<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-sm-6">
		<h5 ><b>Pemasukan</b></h5>
		<table class="table table-striped custom-table table-hover">
		<tbody>
			<tr>
			<td >No</td>
			<td >Jumlah</td>
			<td >Daerah Asal</td>
			</tr>
			<?php $a = 1;?>
			<?php foreach ($data as $key) {
				if ($key['tipe'] == 1) { ?> 
				<tr>
					<td ><?= $a ?></td>
					<td ><p><?php echo $key['jumlah']; ?></p></td>
					<td ><p><?php echo $key['nama']; ?></p></td>
					<?php $a++;?>
				</tr>
			<?php } } ?>
		</tbody>
		</table>	
	</div>
	<div class="col-sm-6">
		<h5 ><b>Penyaluran</b></h5>
		<table class="table table-striped custom-table table-hover">
		<tbody>
			<tr>
			<td >No</td>
			<td >Jumlah</td>
			<td >Daerah Asal</td>
			</tr>
			<?php $a = 1;?>
			<?php foreach ($data as $key) { 
				if ($key['tipe'] == 2) { ?> 
				<tr>
					<td ><?= $a ?></td>
					<td ><p><?php echo $key['jumlah']; ?></p></td>
					<td ><p><?php echo $key['nama']; ?></p></td>
					<?php $a++;?>
				</tr>
			<?php } } ?>
		</tbody>
		</table>
	</div>
</div>