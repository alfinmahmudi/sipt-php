<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<h5 ><b>Detil Berita</b></h5>
<table class="table table-striped custom-table table-hover">
<tbody>
	<tr>
	<td >No</td>
	<td >Judul Gambar</td>
	<td >File</td>
	</tr>
	<?php $a = 1;?>
	<?php for ($i=0; $i < $banyak; $i++) { ?> 
	<tr>
		<td ><?= $a ?></td>
		<td ><?= $data[$i]['title']; ?></td>
		<td ><a href="<?php echo base_url().substr($data[$i]['files'], 2); ?>" target=blank><i class="fa fa-download"></i></a></td>
		<?php $a++;?>
	</tr>
	<?php } ?>
</tbody>
</table>