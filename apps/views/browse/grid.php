<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-dialog/bootstrap-dialog.min.css" type="text/css" cache="false" />
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-dialog/bootstrap-dialog.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtable.js?v=<?= date("YmdHis"); ?>" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtablehash.js?v=<?= date("YmdHis"); ?>" type="text/javascript"></script>
<div class="page-head">
  <h3 class="m-b-less">
    <?= $judul; ?>
  </h3>
</div>
<div class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
      <?= $tabel; ?>
      </section>
    </div>
  </div>
</div>
