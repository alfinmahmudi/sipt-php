<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="page-head">
<?php if ($id == '00') {?>
  <h3 class="m-b-less"> List Berita</h3>
  <div class="wrapper">
    <div class="row">
      <table class="table table-striped custom-table table-hover">
      <tbody>
        <tr>
        <td >No</td>
        <td >Judul Berita</td>
        <td >Action</td>
        </tr><?php $a = 1;?>
         <?php foreach ($view as $key) {?>
        <tr>
          <input type="hidden" id="id_detil" name="id_detil[]" value="<?= $key['id'];?>">
          <td ><?= $a ?></td>
          <td ><p><?php echo $key['isi'];?> </p></td>
          <td ><button><a href="<?php echo site_url('portal/news_view').'/'.$key['id']?>"> Lebih Detil </a></button></td>
          <?php $a++;?>
        </tr>
        <?php } ?>
      </tbody>
      </table>
    </div>
  </div>
<?php }else{?>
  <h3 class="m-b-less"> <?php echo strtoupper($view[0]['judul']);?></h3>
</div>
<div class="wrapper">

<section id="slider-home">
  <div class="flex-container">
    <div class="flexslider">
      <ul class="slides">
      <?php foreach ($detil as $key) {?>
        <li> <img style="max-width:100%; max-height:100%;" src="<?php echo base_url().substr($key['files'], 2); ?>" alt="Jasa Survey">
        </li>
      <?php }?>
      </ul>
    </div>
  </div>
</section>
  <br>
  <div class="row">
    <div class="col-md-12 column ui-sortable">
      <?php 
      $p = explode('.', $view[0]['isi']);//echo count($p);die();
      $c = 1;
      $temp = "";
      foreach ($p as $enter) {
        if ($c % 2 == 0) {
          if (strlen($enter) > 10) {
            echo "<p>".$temp.".".$enter."</p>";
            $temp = null;
          }
        }else{
          $temp.=$enter;
        }
        $c++;
      }

      ?>
    </div>
  </div>
</div>
<?php }?>
