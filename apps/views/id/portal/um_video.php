<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="page-head">
    <h3 class="m-b-less" style="margin-left: 20px;"> List Video Tutorial</h3>
    <div class="wrapper">
        <div class="row">
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/hak_akses.mp4" type="video/mp4">
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px">
                        <h2 align="left">Hak Akses</h2>
                        <p style="margin-top: 5px;" align="left">Hak Akses</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/siujs.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px">
                        <h2 align="left">SIUJS</h2>
                        <p style="margin-top: 5px;" align="left">Surat Izin Usaha Jasa Survey</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/siup4.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px">
                        <h2 align="left">SIUP4</h2>
                        <p style="margin-top: 5px;" align="left">Surat Izin Usaha Perushaan Perantara Perdagangan Properti</p>
                    </div>
                </div>
            </div>
        </div>
        <div style="height: 30px;"></div>
        <div class="row">
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/stp_agen.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px;">
                        <h2 align="left">STP Agen</h2>
                        <p style="margin-top: 5px;" align="left">STP Agen atau Distributor Barang atau Jasa Dalam atau Luar Negeri</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/manual_garansi.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px;">
                        <h2 align="left">Manual Garansi</h2>
                        <p style="margin-top: 5px;" align="left">Informasi Pendaftaran Petunjuk Penggunakan (Manual) dan Kartu Garansi (Garansi)</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/pameran_dagang.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px;">
                        <h2 align="left">Pameran Dagang</h2>
                        <p style="margin-top: 5px;" align="left">Pameran Dagang, Konvensi, Dan Atau Seminar Dagang Internasional</p>
                    </div>
                </div>
            </div>
        </div>
        <div style="height: 30px;"></div>
        <div class="row">
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/stpw.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px;">
                        <h2 align="left">STPW</h2>
                        <p style="margin-top: 5px;" align="left">Surat Tanda Pendaftaran Waralaba</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/siupitmb.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px;">
                        <h2 align="left">SIUPMB ITMB</h2>
                        <p style="margin-top: 5px;" align="left">Surat Izin Usaha Perdagangan Minuman Berakohol untuk ITMB</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/siupmb_distributor.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px;">
                        <h2 align="left">SIUPMB Distributor</h2>
                        <p style="margin-top: 5px;" align="left">Surat Izin Usaha Perdagangan Minuman Berakohol untuk Distributor</p>
                    </div>
                </div>
            </div>
        </div>
        <div style="height: 30px;"></div>
        <div class="row">
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/siup_subdistributor.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px;">
                        <h2 align="left">SIUPMB Sub Distributor</h2>
                        <p style="margin-top: 5px;" align="left">Surat Izin Usaha Perdagangan Minuman Berakohol untuk Sub Distributor</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/SKPA.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px;">
                        <h2 align="left">SKPA</h2>
                        <p style="margin-top: 5px;" align="left">Surat Keterangan Pengecer Minuman Berakohol Golongan A</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/pkaptmov.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px;">
                        <h2 align="left">PKAPT</h2>
                        <p style="margin-top: 5px;" align="left">Pedagang Kayu Antar Pulau Terdaftar</p>
                    </div>
                </div>
            </div>
        </div>
        <div style="height: 30px;"></div>
        <div class="row">
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/skpla.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px;">
                        <h2 align="left">SKPLA</h2>
                        <p style="margin-top: 5px;" align="left">Surat Keterangan Penjual Langsung Minuman Berakohol Golongan A</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                
                <div style="background-color: #ffffff; border: solid 1px #cccccc">
                    <div align="center" style="margin-top: 10px; margin-left: 10px; margin-right: 10px;">
                        <video width="350" style="width: 100%;" controls>
                            <source src="<?= base_url(); ?>assets/SIUPB2 .mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                            Your browser does not support HTML5 video.
                        </video>
                    </div>
                    <div style="height: 100px; margin-left: 10px;">
                        <h2 align="left">SIUPB2</h2>
                        <p style="margin-top: 5px;" align="left">Surat Izin Usaha Perdagangan Bahan Berbahaya sebagai DT-B2</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
