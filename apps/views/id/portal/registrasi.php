<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<script src="<?= base_url(); ?>assets/fend/js/a.js?<?= date('His'); ?>"></script>
<script src="<?= base_url('assets/bend/js/sipt.js'); ?>"></script>
<script src="<?= base_url('assets/bend/js/jquery.validate.min.js?v=2'); ?>"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/form-validation-init.js?v=1"></script>

<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <form role="form" data-redirect = "true" class="form-horizontal" capcha='TRUE' name="fsupportdocnew" id="fsupportdocnew" enctype="multipart/form-data" method="post" action="<?= $act ?>" autocomplete="off" data-agreement"true" check-agreement="yes" >
                <div id="profper" class="col-lg-6">
                    <section class="panel">
                        <header class="panel-heading"> Profile Perusahaan </header>
                        <div class="panel-body">
                            <div class="form-group" id="d_reg">
                                <label class=" control-label">Registrasi</label>
                                <?= form_dropdown('REG[tipe_registrasi]', $pendaftaran, '', 'id="tipe_reg" class="form-control select2" wajib="yes" onchange="cek_regis()"'); ?>
                                <!--select name="REG[tipe_registrasi]" id="tipe_reg" class="form-control select2" wajib="yes" onchange="cek_regis()" >
                                  <option value="01">Perusahaan Dalam Negri</option>
                                  <option value="02">Perorangan Dalam Negri</option>
                                  <option value="03">Perusahaan Luar Negri</option>
                                  <option value="04">Perorangan Luar Negri</option>
                                </select-->
                            </div>
                            <!-- <div class="form-group" id="d_dir">
                                <label class=" control-label">Direktorat</label>
                            <?= form_dropdown('REG[direktorat]', $direktorat, '', 'id="direktorat" wajib="yes" class="form-control select2" '); ?>
                            </div> -->
                            <div class="form-group" id="nib">
                                <label class=" control-label">NIB</label>
                                <input class="form-control" wajib="yes" id="nib1" name="REG[nib]" onkeyup="validate_npwp();" maxlength="13" size="3">
                            </div>
                            <div class="form-group" id="d_npwp">
                                <div>NPWP</div>
                                <div class="col-lg-2" id="npwp">
                                    <input name="NPWP[1]" id="npwp1" wajib="yes" nomor class="nomor" onkeyup="movefocus(this.form, this, 'npwp2', event, '');" maxlength="2" size="2"  >
                                </div>
                                <div class="col-lg-2" id="npwp">
                                    <input name="NPWP[2]" id="npwp2" wajib="yes" nomor class="nomor" onkeyup="movefocus(this.form, this, 'npwp3', event, '');" maxlength="3" size="3"  >
                                </div>
                                <div class="col-lg-2" id="npwp">
                                    <input name="NPWP[3]" id="npwp3" wajib="yes" nomor class="nomor" onkeyup="movefocus(this.form, this, 'npwp4', event, '');" maxlength="3" size="3"  >
                                </div>
                                <div class="col-lg-2" id="npwp">
                                    <input name="NPWP[4]" id="npwp4" wajib="yes" nomor class="nomor" onkeyup="movefocus(this.form, this, 'npwp5', event, '');" maxlength="1" size="1"  >
                                </div>
                                <div class="col-lg-2" id="npwp">
                                    <input name="NPWP[5]" id="npwp5" wajib="yes" class="nomor" onkeyup="movefocus(this.form, this, 'npwp6', event, '');" maxlength="3" size="3"  >
                                </div>
                                <div class="col-lg-2" id="npwp">
                                    <input name="NPWP[6]" id="npwp6" wajib="yes" class="nomor" onkeyup="movefocus(this.form, this, 'lampnpwp', event, '');" maxlength="3" size="3"  >
                                </div>
                                <div class="col-lg-12 col-sm-12 alert alert-danger alert-dismissable" style="display: none; background: red; color: white;" id="warn_npwp" contenteditable="true">
                                    <strong></strong>
                                </div>

                                <!--<div class="form-group" style="display: none;" id="flampiran">-->
                                <!--                                    <div class="form-group" style="display: block; margin-left: 0px;" id="flampiran">
                                                                        <label class="control-label">File Lampiran Scan NPWP </label> 
                                                                    <input type="file" class="form-control" name="filenpwp" id="lampnpwp" wajib = "yes" style="width: 100%;">
                                                                    <font size ="1" color="red">Format file .jpg .png dan .pdf max 2 MB, Semua Dokumen Yang Diunggah Wajib Berwarna Berdasarkan Dokumen Asli</font>
                                                                </div>-->
                                <!--<font size ="1" color="red">Bilamana untuk perusahaan yang merupakan cabang dari perusahaan pusat harus menggunakan NPWP kantor cabang</font>-->
                            </div>
                            <div id="bentuk_usaha">
                                <div class="form-group" id="d_usaha">
                                    <label class=" control-label">Bentuk Usaha</label>
                                    <?= form_dropdown('REG[tipe_perusahaan]', $usaha, '', 'id="tipe_perusahaan" onChange="open_nama()" wajib="yes" class="form-control select2" '); ?>
                                </div>
                            </div>
                            <div id="perorangan">
                                <div class="form-group">
                                    <label class=" control-label" id="nama_prorg">Nama</label>
                                    <label class=" control-label" id="nama_prush">Nama Perusahaan</label>
                                    <input class="form-control" wajib="yes" id="nama_perorangan" name="REG[nm_perusahaan]">
                                </div>
                                <div id="negara_per">
                                    <div class="form-group">
                                        <label class=" control-label">Negara Asal</label>
                                        <?= form_dropdown('REG[negara]', $negara, '', 'id="negara" class="form-control select2"'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class=" control-label" id="alamat_prorg">Alamat</label>
                                    <label class=" control-label" id="alamat_prush">Alamat Perusahaan</label>
                                    <textarea class="form-control" wajib="yes" id="alamat_perorangan" name="REG[almt_perusahaan]"></textarea>
                                </div>
                                <div id="luar_per">
                                    <div class="form-group">
                                        <label class=" control-label">Propinsi</label>
                                        <?= form_dropdown('REG[kdprop]', $prop, '', 'id="prop_perorangan" wajib="yes" data-url="' . site_url() . 'get/cb/set_kota/"  class="form-control select2 " onChange = "combobox($(this), \'#kab_perorangan\'); return false;" '); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label">Kabupaten / Kota</label>
                                        <?= form_dropdown('REG[kdkab]', $kab, '', 'id="kab_perorangan" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kec/" onChange = "combobox($(this), \'#kec_perorangan\');"'); ?>
                                    </div>
                                    <!--<input class="form-control" type="hidden" id="kec_perorangan" name="REG[kdkec]">-->
                                    <!--<input class="form-control" type="hidden" id="kel_perorangan" name="REG[kdkel]">-->
                                    <div class="form-group">
                                        <label class=" control-label">Kecamatan</label>
                                        <?= form_dropdown('REG[kdkec]', $kec, '', 'id="kec_perorangan" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kel/" onChange = "combobox($(this), \'#kel_perorangan\');"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label">Kelurahan</label>
                                        <?= form_dropdown('REG[kdkel]', $kel, '', 'id="kel_perorangan" wajib="yes" class="form-control input-sm select2"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label" id="kdpos_prorg">Kode Pos</label>
                                        <label class=" control-label" id="kdpos_prush">Kode Pos Perusahaan</label>
                                        <input class="form-control" id="kdpos_perorangan" wajib="yes" name="REG[kdpos]">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class=" control-label" id="tlp_prorg">Telepon</label>
                                    <label class=" control-label" id="tlp_prush">Telepon Perusahaan</label>
                                    <input class="form-control" id="telp_perorangan" wajib="yes" name="REG[telp]">
                                </div>
<!--                                <div class="form-group">
                                    <label class=" control-label" id="fax_prorg">Fax</label>
                                    <label class=" control-label" id="fax_prush">Fax Perusahaan</label>
                                    <input class="form-control" id="fax_perorangan" wajib="yes" name="REG[fax]">
                                </div>
                                <div class="form-group">
                                    <label class=" control-label">Email Perusahaan </label>
                                    <input class="form-control" wajib="yes" id="email" name="REG[email]">
                                </div>-->
                            </div>

                            <div id="perusahaan">
                                <!-- <div class="form-group">
                                    <label class=" control-label">Alamat Pabrik</label>
                                    <textarea class="form-control" id="alamat_pabrik" name="REG[alamat_pabrik]"></textarea>
                                </div> -->
                                <!-- <div class="form-group">
                                    <label class=" control-label">Jenis Usaha</label>
                                    <input class="form-control" id="jns_usaha" wajib="yes" name="REG[jns_usaha]">
                                </div> -->

                            </div>
                        <header class="panel-heading"> Penanggung Jawab Perusahaan </header>
                            <div class="form-group">
                                <label class=" control-label">Nama</label>
                                <input class="form-control" wajib="yes" id="nama_pj" name="REG[na_pj]">
                            </div>
<!--                            <div class="form-group">
                                <label class=" control-label">Jenis Identitas</label>
                                <?= form_dropdown('REG[identitas_pj]', $identitas, '', 'id="identitas_pj" class="form-control select2" wajib="yes"'); ?>
                            </div>-->
                            <div class="form-group">
                                <label class=" control-label" id="nik">NIK</label>
                                <input class="form-control" wajib="yes" id="no_identitas_pj" name="REG[noidentitas_pj]">
                            </div>   
                            <div class="form-group">
                                <label class="control-label">Jabatan</label>
                                
                                <input class="form-control" wajib="yes" id="jabatan_pj" name="REG[jabatan_pj]">
                            </div>
                            <?php //form_dropdown('REG[jabatan_pj]', $jabatan, '', 'id="jabatan_pj" class="form-control select2" type="hidden" '); ?>
                            <div class="form-group">
                                <label class=" control-label">Telepon</label>
                                <input class="form-control" wajib="yes" id="telp_pj" name="REG[telp_pj]">
                            </div>
                            <div id="negara_pj">
                                <div class="form-group">
                                    <label class=" control-label">Kewarganegaraan</label>
                                    <?= form_dropdown('REG[negara_pj]', $negara, '', 'id="negara" class="form-control select2"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class=" control-label">Alamat</label>
                                <textarea class="form-control" wajib="yes" id="alamat_pj" name="REG[alamat_pj]"></textarea>
                                <font size ="1" color="red"></font>
                            </div>
                            <div id="luar_pj">
                                <div class="form-group">
                                    <label class=" control-label">Propinsi </label>
                                    <?= form_dropdown('REG[kdprop_pj]', $prop, '', 'id="prop_pj" wajib="yes" data-url="' . site_url() . 'get/cb/set_kota/"  class="form-control select2 " onChange = "combobox($(this), \'#kab_pj\'); return false;" '); ?>
                                </div>
                                <div class="form-group">
                                    <label class=" control-label">Kabupaten / Kota </label>
                                    <?= form_dropdown('REG[kdkab_pj]', $kab, '', 'id="kab_pj" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kec/" onChange = "combobox($(this), \'#kec_pj\');"'); ?>
                                </div>
                                <!--<input class="form-control" type="hidden" id="kec_pj" name="REG[kdkec_pj]">-->
                                <!--<input class="form-control" type="hidden" id="kel_pj" name="REG[kdkel_pj]">-->
                                <div class="form-group">
                                    <label class=" control-label">Kecamatan </label>
                                    <?= form_dropdown('REG[kdkec_pj]', $kec, '', 'id="kec_pj" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kel/" onChange = "combobox($(this), \'#kel_pj\');"'); ?>
                                </div>
                                <div class="form-group">
                                    <label class=" control-label">Kelurahan </label>
                                    <?= form_dropdown('REG[kdkel_pj]', $kel, '', 'id="kel_pj" wajib="yes" class="form-control input-sm select2"'); ?>
                                </div>
                            </div>
                            <!--                            <div class="form-group">
                                                            <label class=" control-label">Tempat Lahir</label>
                                                            <input class="form-control" wajib="yes" id="tmpt_lahir_pj" name="REG[tmpt_lahir_pj]">
                                                        </div>
                                                        <div class="form-group">
                                                            <label class=" control-label">Tanggal Lahir </label>
                                                            <input type="text" class="form-control mb-10 datepickers" id="d" data-date-format="yyyy-mm-dd" name="REG[tgl_lahir_pj]" wajib="yes">
                                                        </div>-->
                            <!--                     <div class="form-group">
                                                    <label class=" control-label">Telp/HP </label>
                                                    <input class="form-control" wajib="yes" id="fax_pj" name="REG[ftelp_pj]">
                                                </div> -->
                            <!-- <div class="form-group">
                                <label class=" control-label">Fax </label>
                                <input class="form-control" wajib="yes" id="fax_pj" name="REG[fax_pj]">
                            </div> -->
                            <div class="form-group">
                                <label class=" control-label">Email </label>
                                <input class="form-control" wajib="yes" id="email_pj" name="REG[email_pj]">
                            </div>
                            <!--                            <div class="form-group">
                                                            <label class="control-label">File Lampiran Scan Identitas Penanggung Jawab</label> 
                                                            <input type="file" class="form-control" name="filektp_pj" wajib="yes" id="filektp_pj">
                                                            <font size ="1" color="red">Format file .jpg .png dan .pdf max 2 MB, Semua Dokumen Yang Diunggah Wajib Berwarna Berdasarkan Dokumen Asli</font>
                                                        </div>-->
                        
                        </div>
                    </section>
                </div>
                  <div id="koor" class="col-lg-6">
                    <header class="panel-heading"> Koordinator Pengguna SIPT </header>
                    <div class="panel-body">
                        <label class="checkbox-custom inline check-success">
                            <input value="1" id="checkbox-agreement" type="checkbox" name="checkbox" onclick="check()" check-agreement="yes"> 
                            <label for="checkbox-agreement">
                                <p>Data sama dengan Penanggung Jawab Perusahaan</p>
                            </label>
                        </label>
                        <!--<div class="form-group">-->
                            <!--<label class=" control-label">Username</label>-->
                            <input class="form-control space" wajib="yes" onchange="validate_username()" id="username_koor" name="KOOR[username]" type="hidden">
                        <!--</div>-->
                        <div class="col-lg-12 col-sm-12 alert alert-danger alert-dismissable" style="display: none; background: red; color: white;" id="warn_username" contenteditable="true">
                            <input type="hidden" id="result_username" value="0">
                            <strong></strong>
                        </div>
                        <div class="form-group">
                            <label class=" control-label">Nama Koordinator</label>
                            <input class="form-control" wajib="yes" id="nama_koor" name="KOOR[nama]">
                        </div>
                        <div class="form-group">
                            <label class=" control-label">Alamat Koordinator</label>
                            <textarea class="form-control"  id="alamat_koor" wajib="yes" name="KOOR[alamat]"></textarea>
                        </div>
                        <div class="form-group">
                            <label class=" control-label">No Telepon / HP</label>
                            <input class="form-control" id="telp_koor" wajib="yes" name="KOOR[telp]"></input>
                        </div>
                        <div class="form-group">
                            <label class=" control-label">No KTP Koordinator</label>
                            <input class="form-control" id="ktp_koor" wajib="yes" name="KOOR[no_identitas]"></input>
                        </div>
                        <div class="form-group">
                            <label class=" control-label">Email Koordinator</label>
                            <input class="form-control" id="email_koor" wajib="yes" name="KOOR[email]"></input>
                            <font size ="1" color="red">Untuk Email Lebih dari satu dapat menggunakan pembatas berupa "," EX:sipt@kemendag.go.id, sipt-pdn.@kemendag.go.id (wajib menggunakan Email publik seperti gmail atau yahoo)</font>
                        </div>
                        <div class="ktp-af">
                            <div class="form-group ktp">
                                <label class="control-label">File Lampiran Scan KTP</label> 
                                <input type="file" class="form-control" name="filektp" wajib="yes" id="filektp">
                                <font size ="1" color="red">Format file .jpg .png dan .pdf max 2 MB, Semua Dokumen Yang Diunggah Wajib Berwarna Berdasarkan Dokumen Asli</font>
                            </div>
                        </div>
                        <div class="sk-af">
                            <div class="form-group sk">
                                <label class="control-label">File Lampiran Surat Kuasa</label> 
                                <input type="file" class="form-control" name="filesk" wajib="yes" id="filesk">
                                <font size ="1" color="red">Format file .jpg .png dan .pdf max 2 MB, Semua Dokumen Yang Diunggah Wajib Berwarna Berdasarkan Dokumen Asli</font>
                            </div>
                        </div>
                        <div class="form-group">
                            <a href="javascript:changeImage();"><img src="<?= base_url(); ?>keycode.php?<?= md5("YmdHis"); ?>" style="width:130px;" id="img-keycode"></a>
                        </div>
                        <div class="form-group">
                            <label class=" control-label">Masukkan Kode</label>
                            <input id="keycode" type="text" name="keycode" wajib="yes" class="form-control">
                        </div>
                    </div>
                </div>

                <div id="pj" class="col-lg-6">
                    
                </div>
                
                <div class="row">
                    <div class="col-lg-6" id="back">

                    </div>
                    <div class="col-lg-6" id="next">
                        <!--<span class="pull-right"><button type="button" class="btn btn-sm btn-success addon-btn m-b-10" onclick="next('02');"><i class="fa fa-arrow-right"></i>Selanjutnya</button></span>-->
                        <span class="pull-right"><button id="simpan" type="submit" class="btn btn-sm btn-success addon-btn m-b-10" onclick="obj('#fsupportdocnew'); return false;"><i class="fa fa-arrow-right"></i>Daftar</button></span>
                    </div>
                    <input type="hidden" id="adds" value="<?= $addGets; ?>" />
                    <input type="hidden" id="log" name="log" />
                </div>
            </form>
        </div>
    </div>
</div>



<script src="<?= base_url(); ?>assets/bend/js/select2.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js" type="text/javascript"></script>
<script type="text/javascript">
                            function changeImage() {
                                document.getElementById("img-keycode").src = "<?= base_url(); ?>keycode.php?rnd=" + Math.random();
                            }
</script>
<script>
    $(document).ready(function (e) {
        
        $('#nib').hide();
        $('#fsupportdocnew').validate();
        $('#btn_prof').css('background-color', '#383838');
        $('#btn_prof').css('color', 'white');
//        $("#nama_perorangan").attr("placeholder", "ex: Nama Perusahaan (tanpa menuliskan bentuk usaha)");
        var a = document.getElementById("tipe_reg").value;
        if (a == '') {
            $('#perusahaan').attr('hidden', false);
            $('#nama_prorg').attr('hidden', true);
            $('#alamat_prorg').attr('hidden', true);
            $('#kdpos_prorg').attr('hidden', true);
            $('#tlp_prorg').attr('hidden', true);
            $('#fax_prorg').attr('hidden', true);
            $('#negara_per').attr('hidden', true);
            $('#negara_pj').attr('hidden', true);
        }
        $(".datepickers").datepicker({
            autoclose: true
        });

        $('.nomor').keydown(function (event) {
            var a = event.keyCode;
            var b = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 9, 11, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106];
            if (jQuery.inArray(a, b) === -1) {
                event.preventDefault();
            }
        });

        $('.space').keydown(function (event) {
            var a = event.keyCode;
            var b = [32];
            if (jQuery.inArray(a, b) != -1) {
                event.preventDefault();
            }
        });

    });

    function open_nama() {
        var jenis_perusahaan = $('#tipe_perusahaan').val();
        if (jenis_perusahaan == '05') {
            $('#nama_perorangan').attr('readonly', false);
        } else {
//            $('#nama_perorangan').attr('readonly', true);
        }
    }

    function next(e) {
        var doc = "'#fsupportdocnew'";
        if (e == 02) {
            $('#profper').hide();
            $('#pj').fadeIn();
            $('#next').html('');
            $('#btn_pj').css('background-color', '#383838');
            $('#btn_pj').css('color', 'white');
            $('#btn_prof').css('background-color', 'white');
            $('#btn_prof').css('color', 'black');

            var next = '<span class="pull-right"><button type="button" class="btn btn-sm btn-success addon-btn m-b-10" onclick="next(\'03\');"><i class="fa fa-arrow-right"></i>Selanjutnya</button></span>';
            var back = '<button class="btn btn-sm btn-info addon-btn m-b-10" type="button" onclick="back(\'01\');"><i class="fa fa-arrow-left"></i>Kembali</button>';
            $('#back').append(back);
            $('#next').append(next);
        } else {
            $('#pj').hide();
            $('#koor').fadeIn();
            $('#next').html('');
            $('#back').html('');
            $('#btn_pj').css('background-color', 'white');
            $('#btn_pj').css('color', 'black');
            $('#btn_koor').css('background-color', '#383838');
            $('#btn_koor').css('color', 'white');

            var next = '<span class="pull-right"><button id="simpan" type="submit" class="btn btn-sm btn-success addon-btn m-b-10" onclick="obj(' + doc + '); return false;"><i class="fa fa-arrow-right"></i>Daftar</button></span>';
            var back = '<button class="btn btn-sm btn-info addon-btn m-b-10" type="button" onclick="back(\'02\');"><i class="fa fa-arrow-left"></i>Kembali</button>';
            $('#back').append(back);
            if ($('#result_username').val() == 0) {
                $('#next').append(next);
            }
        }
    }

    function back(e) {
        if (e == 01) {
            $('#pj').hide();
            $('#profper').fadeIn();
            $('#next').html('');
            $('#back').html('');
            $('#btn_pj').css('background-color', 'white');
            $('#btn_pj').css('color', 'black');
            $('#btn_prof').css('background-color', '#383838');
            $('#btn_prof').css('color', 'white');

            var next = '<span class="pull-right"><button type="button" class="btn btn-sm btn-success addon-btn m-b-10" onclick="next(\'02\');"><i class="fa fa-arrow-right"></i>Selanjutnya</button></span>';
            $('#next').append(next);
        } else {
            $('#koor').hide();
            $('#pj').fadeIn();
            $('#next').html('');
            $('#back').html('');
            $('#btn_pj').css('background-color', '#383838');
            $('#btn_pj').css('color', 'white');
            $('#btn_koor').css('background-color', 'white');
            $('#btn_koor').css('color', 'black');

            var next = '<span class="pull-right"><button class="btn btn-sm btn-success addon-btn m-b-10" type="button" onclick="next(\'03\');"><i class="fa fa-arrow-right"></i>Selanjutnya</button></span>';
            var back = '<button class="btn btn-sm btn-info addon-btn m-b-10" type="button" onclick="back(\'01\');"><i class="fa fa-arrow-left"></i>Kembali</button>';
            $('#back').append(back);
            $('#next').append(next);
        }
    }

    function view_per() {
        $('#profper').fadeIn();
        $('#pj').hide();
        $('#koor').hide();
        $('#btn_koor').css('background-color', 'white');
        $('#btn_koor').css('color', 'black');
        $('#btn_pj').css('background-color', 'white');
        $('#btn_pj').css('color', 'black');
        $('#btn_prof').css('background-color', '#383838');
        $('#btn_prof').css('color', 'white');
        $('#next').html('');
        $('#back').html('');

        var next = '<span class="pull-right"><button type="button" class="btn btn-sm btn-success addon-btn m-b-10" onclick="next(\'02\');"><i class="fa fa-arrow-right"></i>Selanjutnya</button></span>';

        $('#next').append(next);

    }

    function view_pj() {
        $('#profper').hide();
        $('#pj').fadeIn();
        $('#koor').hide();
        $('#btn_koor').css('background-color', 'white');
        $('#btn_koor').css('color', 'black');
        $('#btn_prof').css('background-color', 'white');
        $('#btn_prof').css('color', 'black');
        $('#btn_pj').css('background-color', '#383838');
        $('#btn_pj').css('color', 'white');
        $('#next').html('');
        $('#back').html('');

        var next = '<span class="pull-right"><button type="button" class="btn btn-sm btn-success addon-btn m-b-10" onclick="next(\'03\');"><i class="fa fa-arrow-right"></i>Selanjutnya</button></span>';
        var back = '<button class="btn btn-sm btn-info addon-btn m-b-10" type="button" onclick="back(\'01\');"><i class="fa fa-arrow-left"></i>Kembali</button>';
        $('#back').append(back);
        $('#next').append(next);
    }

    function view_koor() {
        var doc = "'#fsupportdocnew'";
        $('#profper').hide();
        $('#pj').hide();
        $('#koor').fadeIn();
        $('#btn_prof').css('background-color', 'white');
        $('#btn_prof').css('color', 'black');
        $('#btn_pj').css('background-color', 'white');
        $('#btn_pj').css('color', 'black');
        $('#btn_koor').css('background-color', '#383838');
        $('#btn_koor').css('color', 'white');
        $('#next').html('');
        $('#back').html('');

        var next = '<span class="pull-right"><button id="simpan" type="submit" class="btn btn-sm btn-success addon-btn m-b-10" onclick="obj(' + doc + '); return false;"><i class="fa fa-arrow-right"></i>Daftar</button></span>';
        var back = '<button class="btn btn-sm btn-info addon-btn m-b-10" type="button" onclick="back(\'02\');"><i class="fa fa-arrow-left"></i>Kembali</button>';
        $('#back').append(back);
        if ($('#result_username').val() == 0) {
            $('#next').append(next);
        }
    }

    function validate_npwp() {
        var site_url = '<?php echo site_url(); ?>';
        var nib1 = $('#nib1').val();
        $('#username_koor').val(nib1);
        $('#username_koor').attr('readonly', true);
        if (nib1.length == 13) {
            $.post(site_url + 'portal/validate_oss', {nib: nib1, adds: $('#adds').val()}, function (data) {
                // console.log(data);
                // return false;
                var res = data;
                if (res.stat != 1) {
                    BootstrapDialog.show({
                        title: '',
                        type: BootstrapDialog.TYPE_WARNING,
                        message: 'Nomor NIB anda tidak terdaftar.'
                    });
                    $('#npwp1').val('');
                    $('#npwp2').val('');
                    $('#npwp3').val('');
                    $('#npwp4').val('');
                    $('#npwp5').val('');
                    $('#npwp6').val('');
                } else {
                    $('#log').val(res.log);
//                    console.log(nama_per);
                    var tipe_reg = $('#tipe_reg').val();
                    $('#nama_perorangan').val(res.res.nm_perusahaan);
                    $('#npwp1').val(res.res.npwp.substr(0, 2));
                    $('#npwp2').val(res.res.npwp.substr(2, 3));
                    $('#npwp3').val(res.res.npwp.substr(5, 3));
                    $('#npwp4').val(res.res.npwp.substr(8, 1));
                    $('#npwp5').val(res.res.npwp.substr(9, 3));
                    $('#npwp6').val(res.res.npwp.substr(12, 3));
                    $('#tipe_perusahaan').select2("val", res.res.tipe_perusahaan);
                    $('#prop_perorangan').select2("val", res.res.kdprop);

                    $('#kab_perorangan').html('');
                    $.get("http://sipt.kemendag.go.id/training/get/cb/set_kota/" + res.res.kdprop, function ($result) {
                        if ($result) {
                            $('#kab_perorangan').html($result);
                            $('#kab_perorangan').select2("val", res.res.kdkab);
                        }
                    });
                    $('#kec_perorangan').html('');
                    $.get("http://sipt.kemendag.go.id/training/get/cb/set_kec/" + res.res.kdkab, function ($result) {
                        if ($result) {
                            $('#kec_perorangan').html($result);
                        }
                    });
//                    $('#kec_perorangan').val(res.res.kdkec);
//                    $('#kel_perorangan').val(res.res.kdkel);
                    $('#alamat_perorangan').val(res.res.almt_perusahaan);
                    $('#kdpos_perorangan').val(res.res.kdpos);
                    $('#telp_perorangan').val(res.res.telp);
                    $('#nama_pj').val(res.res.na_pj);
                    $('#identitas_pj').select2("val", res.res.identitas_pj);
                    $('#no_identitas_pj').val(res.res.noidentitas_pj);
                    $('#jabatan_pj').val(res.res.jabatan_pj);
                    $('#telp_pj').val(res.res.telp_pj);
                    $('#alamat_pj').val(res.res.alamat_pj);
                    $('#prop_pj').select2("val", res.res.kdprop_pj);

                    $('#kab_pj').html('');
                    $.get("http://sipt.kemendag.go.id/training/get/cb/set_kota/" + res.res.kdprop_pj, function ($result) {
                        if ($result) {
                            $('#kab_pj').html($result);
                            $('#kab_pj').select2("val", res.res.kdkab_pj);
                        }
                    });
                    $('#kec_pj').html('');
                    $.get("http://sipt.kemendag.go.id/training/get/cb/set_kec/" + res.res.kdkab_pj, function ($result) {
                        if ($result) {
                            $('#kec_pj').html($result);
                        }
                    });
//                    $('#kec_pj').val(res.res.kdkec_pj);
//                    $('#kel_pj').val(res.res.kdkel_pj);
                    $('#email_pj').val(res.res.email_pj);

                    if (tipe_reg != 04) {
                        $('#nama_perorangan').attr('readonly', true);
//                          $('#nama_perorangan').attr('readonly', false);
                    }
                    $('#tipe_perusahaan').attr('readonly', true);
                    $('#alamat_perorangan').attr('readonly', true);
                    $('#prop_perorangan').attr('readonly', true);
                    $('#kab_perorangan').attr('readonly', true);
                    $('#kdpos_perorangan').attr('readonly', true);
                    $('#nama_pj').attr('readonly', true);
                    $('#identitas_pj').attr('readonly', true);
                    $('#no_identitas_pj').attr('readonly', true);
                    $('#jabatan_pj').attr('readonly', true);
                    $('#telp_pj').attr('readonly', true);
                    $('#alamat_pj').attr('readonly', true);
                    $('#prop_pj').attr('readonly', true);
                    $('#kab_pj').attr('readonly', true);
                    $('#telp_perorangan').attr('readonly', true);
                    $('#email_pj').attr('readonly', true);

                    $('#flampiran').remove();
                    $('#npwp1').attr('readonly', true);
                    $('#npwp2').attr('readonly', true);
                    $('#npwp3').attr('readonly', true);
                    $('#npwp4').attr('readonly', true);
                    $('#npwp5').attr('readonly', true);
                    $('#npwp6').attr('readonly', true);
                    $('#nib1').attr('readonly', true);
                }
            }, "JSON");
        }
    }

    function validate_email() {
        var email = $('#email_koor').val();
        var res = email.split(",");
        var cekmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //console.log(res);return false;

        for (var i = 0; i < res.length; i++) {
            if (cekmail.test(res[i])) {
                console.log("ok");
            } else {
                console.log("no!");
            }

        }
    }

    function validate_username() {
        var site_url = '<?php echo site_url(); ?>';
        var str = $('#username_koor').val();
        var res = str.toLowerCase();
        var res = res.replace(/\s/g, '');
        $('#username_koor').val(res);
        // console.log(site_url + 'portal/check_username');
        // return false;
        $.post(site_url + 'portal/check_username', {data: $('#username_koor').val()}, function (data) {
            var cek = data;
            if (cek <= 0)
            {
                $('#warn_username').fadeOut();
                $('#simpan').attr('disabled', false);
                $('#result_username').val(0);
                $('#simpan').show();
            } else
            {
                $('#warn_username').fadeIn();
                $('#warn_username').html('Username yang diinput sudah digunakan');
                $('#simpan').hide();
                $('#result_username').val(1);
            }
        });
    }

    function check() {
        if ($('#checkbox-agreement').is(":checked")) {
            $('#nama_koor').val($('#nama_pj').val());
            $('#ktp_koor').val($('#no_identitas_pj').val());
            $('#telp_koor').val($('#telp_pj').val());
            $('#alamat_koor').val($('#alamat_pj').val());
            $('#fax_koor').val($('#fax_pj').val());
            $('#email_koor').val($('#email_pj').val());
            $('.ktp').remove();
            $('.sk').remove();
        } else {
            $('#nama_koor').val('');
            $('#ktp_koor').val('');
            $('#telp_koor').val('');
            $('#alamat_koor').val('');
            $('#fax_koor').val('');
            $('#email_koor').val('');
            var temp = '<div class="form-group sk">';
            temp += '<label class="control-label">File Lampiran Surat Kuasa</label>';
            temp += '<input type="file" class="form-control" name="filesk" wajib="yes" id="filesk">';
            temp += '<font size ="1" color="red">Format file .jpg .png dan .pdf max 2 MB, Semua Dokumen Yang Diunggah Wajib Berwarna Berdasarkan Dokumen Asli</font>';
            temp += '</div>';
            $('.sk-af').append(temp);
            var tupm = '<div class="form-group ktp">';
            tupm += '<label class="control-label">File Lampiran Scan KTP</label>';
            tupm += '<input type="file" class="form-control" name="filektp" wajib="yes" id="filektp">';
            tupm += '<font size ="1" color="red">Format file .jpg .png dan .pdf max 2 MB, Semua Dokumen Yang Diunggah Wajib Berwarna Berdasarkan Dokumen Asli</font>';
            tupm += '</div>';
            $('.ktp-af').append(tupm);
        }

    }

    function cek_regis(e) {
        var regi = document.getElementById("tipe_reg").value;
        var prop = document.getElementById("prop_perusahaan");

        if (regi == "01") {
            $('#perusahaan').attr('hidden', false);
            $('#nama_prorg').attr('hidden', true);
            $('#alamat_prorg').attr('hidden', true);
            $('#kdpos_prorg').attr('hidden', true);
            $('#tlp_prorg').attr('hidden', true);
            $('#fax_prorg').attr('hidden', true);
            
            $('#tipe_perusahaan').attr('readonly', true);
                    $('#alamat_perorangan').attr('readonly', true);
                    $('#prop_perorangan').attr('readonly', true);
                    $('#kab_perorangan').attr('readonly', true);
                    $('#kdpos_perorangan').attr('readonly', true);
                    $('#nama_pj').attr('readonly', true);
                    $('#identitas_pj').attr('readonly', true);
                    $('#no_identitas_pj').attr('readonly', true);
                    $('#jabatan_pj').attr('readonly', true);
                    $('#telp_pj').attr('readonly', true);
                    $('#alamat_pj').attr('readonly', true);
                    $('#prop_pj').attr('readonly', true);
                    $('#kab_pj').attr('readonly', true);
                    $('#telp_perorangan').attr('readonly', true);
                    $('#email_pj').attr('readonly', true);

                    $('#flampiran').remove();
                    $('#npwp1').attr('readonly', true);
                    $('#npwp2').attr('readonly', true);
                    $('#npwp3').attr('readonly', true);
                    $('#npwp4').attr('readonly', true);
                    $('#npwp5').attr('readonly', true);
                    $('#npwp6').attr('readonly', true);

            $('#nama_prush').attr('hidden', false);
            $('#alamat_prush').attr('hidden', false);
            $('#kdpos_prush').attr('hidden', false);
            $('#tlp_prush').attr('hidden', false);
            $('#fax_prush').attr('hidden', false);
            $('#d_npwp').attr('hidden', false);
            $('#flampiran').attr('hidden', false);
            $("#nama_perorangan").attr("placeholder", "ex: Nama Perusahaan (tanpa menuliskan bentuk usaha)");
            $('#nib').show();

            var temp = '<div>NPWP</div><div class="col-lg-2" id="npwp"><input name="NPWP[1]" id="npwp1" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form,this,"npwp2",event,"");" maxlength="2" size="2"  ></div><div class="col-lg-2" id="npwp"><input name="NPWP[2]" id="npwp2" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form,this,"npwp3",event,"");" maxlength="3" size="3"  ></div><div class="col-lg-2" id="npwp"><input name="NPWP[3]" id="npwp3" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form,this,"npwp4",event,"");" maxlength="3" size="3"  ></div><div class="col-lg-2" id="npwp"><input name="NPWP[4]" id="npwp4" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form,this,"npwp5",event,"");" maxlength="1" size="1"  ></div><div class="col-lg-2" id="npwp"><input name="NPWP[5]" id="npwp5" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form,this,"npwp6",event,"");" maxlength="3" size="3"  ></div><div class="col-lg-2" id="npwp"><input name="NPWP[6]" id="npwp6" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form,this,"lampnpwp",event,"");" maxlength="3" size="3"  ></div><div class="col-lg-12 col-sm-12 alert alert-danger alert-dismissable" style="display: none; background: red; color: white;" id="warn_npwp" contenteditable="true"><strong></strong></div>, <div class="form-group" id="flampiran"><label class="control-label">File Lampiran Scan NPWP </label><input type="file" class="form-control" name="filenpwp" id="lampnpwp" wajib="yes"><font size ="1" color="red">Format file .jpg .png dan .pdf max 2 MB, Semua Dokumen Yang Diunggah Wajib Berwarna Berdasarkan Dokumen Asli</font></div>';
            if ($('#d_npwp').is(':empty')) {
                $('#d_npwp').append(temp);
            }

            if ($('#bentuk_usaha').html() == "") {
                location.reload();
            }
        }
        if (regi == "02" || e == "02") {
            $('#perusahaan').attr('hidden', true);
            $('#nama_prush').attr('hidden', true);
            $('#alamat_prush').attr('hidden', true);
            $('#kdpos_prush').attr('hidden', true);
            $('#tlp_prush').attr('hidden', true);
            $('#fax_prush').attr('hidden', true);

            $('#nama_prorg').attr('hidden', false);
            $('#alamat_prorg').attr('hidden', false);
            $('#kdpos_prorg').attr('hidden', false);
            $('#tlp_prorg').attr('hidden', false);
            $('#fax_prorg').attr('hidden', false);
            $('#d_npwp').attr('hidden', false);
            $('#flampiran').attr('hidden', false);
            $('#bentuk_usaha').html('');
            $("#nama_perorangan").attr("placeholder", "");
            $('#nib').html('');
            //$('#alamat_pabrik').removeAttribute("wajib");
            var temp = '<div>NPWP</div><div class="col-lg-2" id="npwp"><input name="NPWP[1]" id="npwp1" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form,this,"npwp2",event,"");" maxlength="2" size="2"  ></div><div class="col-lg-2" id="npwp"><input name="NPWP[2]" id="npwp2" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form,this,"npwp3",event,"");" maxlength="3" size="3"  ></div><div class="col-lg-2" id="npwp"><input name="NPWP[3]" id="npwp3" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form,this,"npwp4",event,"");" maxlength="3" size="3"  ></div><div class="col-lg-2" id="npwp"><input name="NPWP[4]" id="npwp4" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form,this,"npwp5",event,"");" maxlength="1" size="1"  ></div><div class="col-lg-2" id="npwp"><input name="NPWP[5]" id="npwp5" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form,this,"npwp6",event,"");" maxlength="3" size="3"  ></div><div class="col-lg-2" id="npwp"><input name="NPWP[6]" id="npwp6" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form,this,"lampnpwp",event,"");" maxlength="3" size="3"  ></div><div class="col-lg-12 col-sm-12 alert alert-danger alert-dismissable" style="display: none; background: red; color: white;" id="warn_npwp" contenteditable="true"><strong></strong></div>, <div class="form-group" id="flampiran"><label class="control-label">File Lampiran Scan NPWP </label><input type="file" class="form-control" name="filenpwp" id="lampnpwp" wajib="yes"><font size ="1" color="red">Format file .jpg .png dan .pdf max 2 MB, Semua Dokumen Yang Diunggah Wajib Berwarna Berdasarkan Dokumen Asli</font></div>';
            if ($('#d_npwp').is(':empty')) {
                $('#d_npwp').append(temp);
            }
        }
        if (regi == "03") {
            $('#perusahaan').attr('hidden', false);
            $('#nama_prorg').attr('hidden', true);
            $('#alamat_prorg').attr('hidden', true);
            $('#kdpos_prorg').attr('hidden', true);
            $('#tlp_prorg').attr('hidden', true);
            $('#fax_prorg').attr('hidden', true);
            $('#luar_per').html('');
            $('#luar_pj').html('');
            $("#nama_perorangan").attr("placeholder", "");

            $('#nama_prush').attr('hidden', false);
            $('#alamat_prush').attr('hidden', false);
            $('#kdpos_prush').attr('hidden', false);
            $('#tlp_prush').attr('hidden', false);
            $('#fax_prush').attr('hidden', false);
            $('#negara_per').attr('hidden', false);
            $('#negara_pj').attr('hidden', false);
            $('#d_npwp').html('');
            $('#luar_per').html('');
            $('#luar_pj').html('');
            $('#bentuk_usaha').html('');
            $('#nib').html('');
            $('#nik').html('Nomor Paspor');
        }
        if (regi == "04") {
            $('#perusahaan').attr('hidden', false);
            $('#nama_prorg').attr('hidden', true);
            $('#alamat_prorg').attr('hidden', true);
            $('#kdpos_prorg').attr('hidden', true);
            $('#tlp_prorg').attr('hidden', true);
            $('#fax_prorg').attr('hidden', true);
            // $('#luar_per').html('');
            // $('#luar_pj').html('');
            $("#nama_perorangan").attr("placeholder", "");

            $('#nama_prush').attr('hidden', false);
            $('#alamat_prush').attr('hidden', false);
            $('#kdpos_prush').attr('hidden', false);
            $('#tlp_prush').attr('hidden', false);
            $('#fax_prush').attr('hidden', false);
            $('#nama_perorangan').attr('readonly', false);
            $('#nib1').html('');
        }
    }
    function movefocus(nmfrm, nmthis, nextfocus, event, prevfocus) {
        var tempKey = event.keyCode.toString();
        validate_npwp();
        if (tempKey == 8)
        {
            if ((nmthis.value.length == 0) && (prevfocus != ''))
            {
                nmfrm[prevfocus].focus();
            }
        } else
        {
            if ((nmthis.maxLength == nmthis.value.length) && (nextfocus != '')) {
                nmfrm[nextfocus].select();
                nmfrm[nextfocus].focus();
            }
        }
    }


</script>
</body>
</html>
