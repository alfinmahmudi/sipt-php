<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
{_header_}
<title>{_appname_}</title>
</head><body style="background-image: none; background-color: rgb(217, 217, 217);">
<div id="navigation" class="clearfix" style="height:60px;">
  <div><a href="<?= site_url(); ?>portal/regis"> Daftar Hak Akses </a> <a class="button blue small fixbarlink" href="javascript:void(0);" style="padding-bottom:10px;" data-url = "<?=site_url().'portal/login'?>" data-title = "" id = "logbtn"> Masuk </a></div>
</div>
<div id="wrap" style="width: 1020px; max-width: 100%; margin: 0px auto; box-shadow: rgba(0, 0, 0, 0.239216) 0px 0px 4px; background-color: rgb(255, 255, 255);"> 
  <header id="header" class="container"> 
  	<section></section>
    <section id="header-top" class="clearfix"> 
    	<div class="row">
    		<div class="col-md-6 column ui-sortable" >
    			<h2 id="logo"><a href="<?= base_url(); ?>"><img src="<?= base_url(); ?>assets/fend/img/logo.png" style="max-height: 100%; max-width: 80%;" alt="Sistem Informasi Pelayanan Terpadu"></a></h2>
    		</div>
        <div class="col-md-3 column ui-sortable">
          &nbsp;
        </div>
    		<!-- <div class="col-md-3 column ui-sortable" >
    			<h2 id="logo"><a href="<?= base_url(); ?>"><img src="<?= base_url(); ?>assets/fend/img/ind.jpg" style="width: 200px; " alt="100% Indonesia"></a></h2>
    		</div></span> -->
    	</div>
<!--       <div class="one-half">
        <h2 id="logo"><a href="<?= base_url(); ?>"><img src="<?= base_url(); ?>assets/fend/img/logo.png" style="max-height: 80%; max-width: 80%;" alt="Sistem Informasi Pelayanan Terpadu"></a></h2>
      </div> -->
      <!--<div class="one-half column-last"> 
        <div class="contact-info">
          <p class="phone">(123) 456-7890</p>
          <p class="email"><a href="mailto:info@finesse.com">info@finesse.com</a></p>
        </div>
      </div> -->
    </section>
    
    <!-- begin navigation bar -->
	
    <section id="navbar" class="clearfix"> 
      <nav id="nav">
        <ul id="navlist" class="clearfix">
          <li <?php echo ($this->uri->segment(2)  == 'berita') ? 'class=current' :'';?>><a href="<?=site_url('portal/news');?>" data-rel="submenu1">Beranda</a></li>
          <li <?php echo ($this->uri->segment(2)  == 'izin') ? 'class=current' :'';?>><a href="<?=site_url('portal/izin');?>" data-rel="submenu2">Perizinan SIPT</a></li>
          <li <?php  echo ($this->uri->segment(2)  == 'status') ? "class='current'" :'';?> ><a href="<?=site_url('portal/status');?>" data-rel="submenu3">Cek Status</a></li>
          <li <?php  echo ($this->uri->segment(2)  == 'validasi') ? "class='current'" :'';?>><a href="<?=site_url('portal/validasi');?>" data-rel="submenu4">Validasi Perizinan</a></li>
          <li <?php  echo ($this->uri->segment(2)  == 'contact') ? "class='current'" :'';?>><a href="<?=site_url('portal/contact');?>" data-rel="submenu4">Hubungi Kami</a></li>
          <li <?php  echo ($this->uri->segment(2)  == 'regis') ? "class='current'" :'';?>><a href="<?=site_url('portal/regis');?>" class="hidden-sm" data-rel="submenu4">Daftar Hak Akses</a></li>
          <li <?php  echo ($this->uri->segment(2)  == 'login') ? "class='current'" :'';?>><a class="button blue small fixbarlink" href="javascript:void(0);" data-url = "<?=site_url().'portal/login'?>" data-title = "" id = "logbtn"> Masuk </a></li>
        </ul>
      </nav>
      <!-- end navigation --> 

    </section>
    <!-- end navigation bar --> 
    
  </header>
  <section id="content" class="container clearfix">{_content_}</section>
  <footer id="footer" style="position:relative;">
    <div class="container"> 
     
      <div id="footer-bottom">
        <div class="one-half">
          <p>Copyright &copy; <?= date('Y')?></p>
        </div>
        <div class="one-half column-last">
         
        </div>
      </div>
    </div>
  </footer>
</div>
</body>
</html>