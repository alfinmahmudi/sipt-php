<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<link href="<?=base_url();?>assets/bend/css/owl.carousel.css" rel="stylesheet">
<link href="<?=base_url();?>assets/bend/css/bootstrap-reset.css" rel="stylesheet">
<link href="<?=base_url();?>assets/bend/js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
<link href="<?=base_url();?>assets/bend/js/vector-map/jquery-jvectormap-1.1.1.css" rel="stylesheet">
<script src="<?= base_url(); ?>assets/bend/js/owl.carousel.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/vector-map/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>

<section id="slider-home">
  <div class="flex-container">
    <div class="flexslider">
      <ul class="slides">
      <style>
        .fa-file-pdf-o{
          color: white;
          padding-top: 7px;
        }
        .fa-tasks{
        	color: white;
          	padding-top: 7px;	
        }
      </style>
       
      <?php foreach ($data as $key => $val) {
         if($key == "slide"){
        ?>
        <li> <img style="margin-left: auto; margin-right: auto;" src="<?php echo base_url().substr($val['files'], 2); ?>" alt="Jasa Survey">
          <!-- <div class="flex-caption">
            <h2><?php echo $val['judul'];?></h2>
            <p><?php echo $val['isi'];?><a href="<?php echo site_url('portal/news_view').'/'.$val['id']?>"> lebih detil </a>.</p>
          </div> -->
        </li>
      
      </ul>
    </div>
  </div>
</section>
<!-- end slider --> 

<!-- begin infobox -->
<section>
  <div class="infobox">
    <div class="infobox-inner"> <a class="button large" href="<?php echo site_url('portal/news_view').'/'?>00">Daftar Berita</a>
      <div class="with-button">
        <h2><?php echo $val['judul'];?></h2>
        <p><?php echo $val['isi'];?></p>
      </div>
      <?php 
      }
    }
    ?>
      <a class="button large mobile-button" href="<?php echo site_url('portal/news_view').'/'?>00">Selanjutnya</a> </div>
  </div>
</section>
<!-- end infobox --> 

<!-- begin services -->
<section>
<div class="row">
	<div class="col-md-3">
	    <section class="panel">
	        <div class="slick-carousal">
	            <div class="overlay-c-bg" style="background: #ffb85f;"></div>
	            <div id="news-feed1" class="owl-carousel owl-theme">
                        <div class="item">
                            <h2 align="center">Video Tutorial</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/hak_akses.mp4" type="video/mp4">
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">Hak Akses</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>

                        <div class="item">
                            <h2 align="center">SIUJS</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/siujs.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">Surat Izin Usaha Jasa Survey</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>
                        
                        <div class="item">
                            <h2 align="center">SIUP4</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/siup4.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">Surat Izin Usaha Perushaan Perantara Perdagangan Properti</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>

                        <div class="item">
                            <h2 align="center">STP Agen</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/stp_agen.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">STP Agen atau Distributor Barang atau Jasa Dalam atau Luar Negeri</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>

                        <div class="item">
                            <h2 align="center">Manual Garansi</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/manual_garansi.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                                <p style="margin-top: 5px;" align="center">Informasi Pendaftaran Petunjuk Penggunakan (Manual) dan Kartu Garansi (Garansi)</p>
                            </div>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>

                        <div class="item">
                            <h2 align="center">Pameran Dagang</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/pameran_dagang.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">Pameran Dagang, Konvensi, Dan Atau Seminar Dagang Internasional</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>
                        <div class="item">
                            <h2 align="center">STPW</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/stpw.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">Surat Tanda Pendaftaran Waralaba</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>
                        <div class="item">
                            <h2 align="center">SIUPMB ITMB</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/siupitmb.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">Surat Izin Usaha Perdagangan Minuman Berakohol untuk ITMB</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>
                        <div class="item">
                            <h2 align="center">SIUPMB Distributor</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/siupmb_distributor.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">Surat Izin Usaha Perdagangan Minuman Berakohol untuk Distributor</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>
                        <div class="item">
                            <h2 align="center">SIUPMB Sub Distributor</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/siup_subdistributor.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">Surat Izin Usaha Perdagangan Minuman Berakohol untuk Sub Distributor</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>
                        <div class="item">
                            <h2 align="center">SKPA</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/SKPA.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">Surat Keterangan Pengecer Minuman Berakohol Golongan A</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>
                        <div class="item">
                            <h2 align="center">PKAPT</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/pkaptmov.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">Pedagang Kayu Antar Pulau Terdaftar</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>
                        <div class="item">
                            <h2 align="center">SKPLA</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/skpla.mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">Surat Keterangan Penjual Langsung Minuman Berakohol Golongan A</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>
                        <div class="item">
                            <h2 align="center">SIUPB2</h2>
                            <div align="center">
                                <video width="350" style="width: 80%;" controls>
                                    <source src="<?= base_url(); ?>assets/SIUPB2 .mp4" type="video/mp4"> <!-- ganti link video ini pip -->
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <p style="margin-top: 5px;" align="center">Surat Izin Usaha Perdagangan Bahan Berbahaya sebagai DT-B2</p>
                                                        <div class="text-center">
                                                    <a href="<?php echo site_url('portal/um_video'); ?>" class="view-all" style="margin-top: 0px;">View All</a>
                                                </div>
                        </div>
	            </div>
	        </div>
	    </section>
	</div>
	<div class="col-md-3">
		<section class="panel">
	        <div class="slick-carousal">
	            <div class="overlay-c-bg" style="background: #ff7a5a;"></div>
	            <div id="news-feed2" class="owl-carousel owl-theme">
	            	<div class="item">
	                    <h2 align="center">User Manual</h2>
                      <div align="center">
                        <a href=""><i class="fa fa-file-pdf-o fa-4x"></i></a>
                      </div>
                      <h1>UNTUK PELAKU USAHA</h1>
                      <div class="text-center">
                          <a href="<?php echo site_url('portal/um_view');?>" class="view-all">View All</a>
                      </div>
	                </div>
	                <?php foreach ($data['um'] as $key) { ?>
	                <div class="item">
	                    <h2 align="center">User Manual</h2>
                      <div align="center">
                        <a href=""><i class="fa fa-file-pdf-o fa-4x"></i></a>
                      </div>
                      <p style="margin-top: 20px;" align="center"><?= $key['nama_izin']; ?></p>
                      <div class="text-center">
                          <a href="<?php echo site_url('portal/um_view');?>" class="view-all">View All</a>
                      </div>
	                </div>
	                <?php } ?>
                        <div class="item">
	                    <h2 align="center">User Manual</h2>
                      <div align="center">
                        <a href=""><i class="fa fa-file-pdf-o fa-4x"></i></a>
                      </div>
                      <h1>MODUL PELAPORAN ANTAR PULAU</h1>
                      <div class="text-center">
                          <a href="<?php echo site_url('portal/um_view');?>" class="view-all">View All</a>
                      </div>
	                </div>
	            </div>
	        </div>
	    </section>
	</div>
	<div class="col-md-3">
	    <section class="panel">
	        <div class="slick-carousal">
	            <div class="overlay-c-bg" style="background: #4396bf;"></div>
	            <div id="news-feed3" class="owl-carousel owl-theme">
                  <div class="item">
                      <h2 align="center">Regulasi</h2>
                      <div align="center">
                        <a href=""><i class="fa fa-tasks fa-4x"></i></a>
                      </div>
                      <h1>SIPT PDN</h1>
                      <div class="text-center">
                          <a href="<?php echo site_url('portal/regulasi');?>" class="view-all">View All</a>
                      </div>
                  </div>
	            </div>
	        </div>
	    </section>
	</div>
    <div class="col-md-3">
	    <section class="panel">
	        <div class="slick-carousal">
	            <div class="overlay-c-bg" style="background: #5cb85c;"></div>
	            <div id="news-feed4" class="owl-carousel owl-theme">
                  <div class="item">
                      <h2 align="center">Informasi</h2>
                      <div align="center">
                          <a href=""><i class="fa fa-info-circle fa-4x" style="color:white; font-size:48px; margin-top: 5px;"></i></a>
                      </div>
                      <h1>SIPT PDN</h1>
                      <div class="text-center">
                          <a href="<?php echo site_url('portal/informasi');?>" class="view-all">View All</a>
                      </div>
                  </div>
	            </div>
	        </div>
	    </section>
	</div>
</div>

  <!-- <h2>Our Services <span class="more">&ndash; <a href="services.html">View All Services &raquo;</a></span></h2> -->
  
  <!-- begin iconbox carousel -->

  <!-- <ul class="iconbox-carousel">
    <li>
      <div class="iconbox computer">
        <h4><a href="http://www.insw.go.id/"><img src="<?=base_url('banner/insw.jpg');?>">INWS</a></h4>
        
      </div>
    </li>
    <li>
      <div class="iconbox mouse">
        <h4><a href="http://e-ska.kemendag.go.id/cms.php"><img src="<?=base_url('banner/ska.gif');?>" width="228">E-SKA</a></h4>
       
      </div>
    </li>
    <li>
      <div class="iconbox applications">
        <h4><a href="http://www.bappebti.go.id/"><img src="<?=base_url('banner/bappebti_3.gif');?>">BAPPEBTI</a></h4>
        
      </div>
    </li>
    <li>
      <div class="iconbox cog">
        <h4><a href="http://ditjenpdn.kemendag.go.id/"><img src="<?=base_url('banner/pdn_3.gif');?>">DJPDN</a></h4>
       
      </div>
    </li>
  </ul> -->
 
</section>
 <!-- end iconbox carousel
 begin testimonials -->
<section>
  <div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading head-border">
            Izin Yang Sudah Terbit
        </header>
        <table class="table table-striped custom-table table-hover">
            <thead>
            <tr style="background: #383838; color: white;">
                <th> NO </th>
                <th><i class="fa fa-building"></i> Nama Perusahaan </th>
                <th><i class="fa fa-paperclip"></i> Jenis Perizinan </th>
                <th><i class="fa fa-calendar-check-o"></i> Tanggal Terbit </th>
            </tr>
            </thead>
            <tbody>
                <?php $i=1; foreach($data['permohonan'] as $res){?>
                <tr>
                  <td><?=$i;?></td>
                  <td><?=$res['nm_perusahaan'];?></td>
                  <td><?=$res['nama_izin'];?></td>
                  <td><?=$res['izin'];?></td>
                </tr>
                <?php $i++; 
                }
                ?>
            </tbody>
        </table>
    </section>
  </div>

	 <!-- <div class="infobox">
		<div class="infobox-inner">
			<table class="table table-bordered" width="100%">
				<thead>
					<tr>
						<th>NO</th>
						<th>Nama Perusahaan</th>
						<th>Jenis Perizinan</th>	
						<th  align = "center" >Tanggal Terbit</th>	
					</tr>	
				</thead>
				<tbody>
					<?php $i=1; foreach($data['permohonan'] as $res){?>
					<tr>
						<td><?=$i;?></td>
						<td><?=$res['nm_perusahaan'];?></td>
						<td><?=$res['nama_izin'];?></td>
						<td><?=$res['tgl_izin'];?></td>
					</tr>
					<?php $i++; 
					}
					?>
				</tbody>
			</table>
		</div>	
	</div> -->
</section>	
	<!--
  <h2>Testimonials <span class="more">&ndash; <a href="testimonials.html">View All Testimonials &raquo;</a></span></h2>
  
   begin testimonial carousel 
  <ul class="testimonial-carousel">
    <li>
      <blockquote class="speech-bubble">
        <div class="quote-content">
          <p>Great theme! Very intuitive, clean code, very well-organized documentation &ndash; I would highly recommend getting this theme; it’s ideal for further customization!</p>
          <span class="quote-arrow"></span> </div>
        <div class="quote-meta">Harry Jones, Web Designer<br>
          <span class="grey-text">CreativeBrains</span> </div>
      </blockquote>
    </li>
    <li>
      <blockquote class="speech-bubble">
        <div class="quote-content">
          <p>I would rate the template 5 out of 5 and here's why: it has a clean and straightforward look that will work for a variety of target audiences, which is important when you build for ROI.</p>
          <span class="quote-arrow"></span> </div>
        <div class="quote-meta">Andrew Williams, Art Director<br>
          <span class="grey-text">SmartBiz</span> </div>
      </blockquote>
    </li>
    <li>
      <blockquote class="speech-bubble">
        <div class="quote-content">
          <p>The template is really intuitive to customize and, the few instances where I needed help, you were right there to assist, in a timely manner, I might add. Well done, keep up the great work!</p>
          <span class="quote-arrow"></span> </div>
        <div class="quote-meta">Larry Thompson, Web Developer<br>
          <span class="grey-text">BitVenture</span> </div>
      </blockquote>
    </li>
  </ul>
  <!-- end testimonial carousel 
</section>
<!-- end testimonials --> 
<script>
	$(document).ready(function() {
		$("#news-feed1").owlCarousel({
            navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:false
        });
        $("#news-feed2").owlCarousel({
            navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:true
        });
        $("#news-feed3").owlCarousel({
            navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:true
        });
        $("#news-feed4").owlCarousel({
            navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:true
        });
	});
</script>
