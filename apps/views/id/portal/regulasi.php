<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="page-head">
  <h3 class="m-b-less"> List Regulasi </h3>
  <div class="wrapper">
    <div class="row">
      <table class="table table-striped custom-table table-hover">
      <tbody>
        <tr>
        <td >No</td>
        <td >Nomor Regulasi</td>
        <td >Status</td>
        <td >Judul</td>
        <td >Action</td>
        <tr>
          <td >1</td>
          <td ><p>85/M-DAG/PER/12/2016</p></td>
          <td ><p>Baru</p></td>
          <td ><p>Pelayanan Terpadu Perdagangan</p></td>
          <td ><a href="<?= base_url('assets/fend/regulasi/pelayanan_terpadu.pdf') ?>"><i class="fa fa-cloud-download fa-lg"></i></a></td>
        </tr>
        <tr>
          <td >2</td>
          <td ><p>86/M-DAG/PER/12/2016</p></td>
          <td ><p>Baru</p></td>
          <td ><p>Ketentuan Pelayanan Perizinan di Bidang Perdagangan Secara Online dan Tanda Tangan Elektronik (Digital Signature)</p></td>
          <td ><a href="<?= base_url('assets/fend/regulasi/ketentuan_pelayanan_perizinan.pdf') ?>"><i class="fa fa-cloud-download fa-lg"></i></a></td>
        </tr>
        <tr>
          <td >3</td>
          <td ><p>20/M-DAG/PER/3/2017</p></td>
          <td ><p>Baru</p></td>
          <td ><p>Ketentuan Pendaftaran Pelaku Usaha Distribusi Barang Kebutuhan Pokok</p></td>
          <td ><a href="<?= base_url('assets/fend/regulasi/TDPUD.pdf') ?>"><i class="fa fa-cloud-download fa-lg"></i></a></td>
        </tr>
        <tr>
          <td >4</td>
          <td ><p>-</p></td>
          <td ><p>-</p></td>
          <td ><p>SOP Perizinan PDN Online</p></td>
          <td ><a href="<?= base_url('assets/fend/regulasi/SOP.pdf') ?>"><i class="fa fa-cloud-download fa-lg"></i></a></td>
        </tr>
      </tbody>
      </table>
    </div>
  </div>
