<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="page-head">
  <h3 class="m-b-less"> List Informasi </h3>
  <div class="wrapper">
    <div class="row">
      <table class="table table-striped custom-table table-hover">
      <tbody>
        <tr>
        <td >No</td>
        <td >Judul</td>
        <td >Action</td>
        <tr>
          <td >1</td>
          <td ><p>SOP Perizinan PDN Online</p></td>
          <td ><a href="<?= base_url('assets/fend/regulasi/SOP.pdf') ?>"><i class="fa fa-cloud-download fa-lg"></i></a></td>
        </tr>
      </tbody>
      </table>
    </div>
  </div>
