<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<section>
    <div class="page-head">
        <h3 class="m-b-less"> Penginputan Password Kembali </h3>
        <span class="sub-title">Hak Akses Aplikasi SIPT Perdagangan Dalam Negeri</span> 
    </div>
    <?php if ($status == 'FALSE') { ?>
    <div>
        <div style="height: 40px;">&nbsp;</div>
        <p align="center">Username dan Email yang di Kirim Tidak Valid</p>
    </div>
    <? }else{ ?>
    <div class="wrapper">
        <div class="row">
            <form id="persyaratan" name = "persyaratan" action = "<?= site_url(); ?>portal/set_confirm" data-target ="#result" method = "post" autocomplate="off">
                <div style="height:5px;"></div>
                <div class="row">
                    <label class="col-sm-3 control-label">Username <font size ="2" color="red">*</font></label>
                    <div class="col-sm-5">
                        <input type="text" readonly class="form-control mb-10"  name="lupa[username]" wajib="yes" value="<?= $username; ?>" />
                        <input type="hidden" readonly class="form-control mb-10"  name="lupa[id]" wajib="yes" value="<?= $id; ?>" />
                    </div>
                </div>
                <div style="height:5px;"></div>
                <div class="row">
                    <label class="col-sm-3 control-label">Password<font size ="2" color="red">*</font></label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control mb-10"  name="lupa[password]" wajib="yes" />
                    </div>
                </div>
                <div style="height:5px;"></div>
                <div class="row">
                    <label class="col-sm-3 control-label">Re-type Password</label>
                    <div class="col-sm-5">
                        <input type="password" name="lupa[retype]" wajib="yes" wajib="yes" class="form-control"></a>
                    </div>
                    <div class="col-sm-6">
                        &nbsp;
                    </div>
                </div>
                <div style="height:25px;"></div>
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="s('#persyaratan'); return false;"><i class="fa fa-check"></i>Input</button>
                    </div>	
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="result"></div>
            </div>	
        </div>
    </div>
</section>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    
                            function changeImage() {
                                document.getElementById("img-keycode").src = "<?= base_url(); ?>keycode.php?rnd=" + Math.random();
                            }
</script>

<script>
$(document).ready(function (e) {
$('.nomor').keydown(function(event){
            var a = event.keyCode;
            var b = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 9, 11];
            if (jQuery.inArray(a,b) === -1) {
                event.preventDefault();
            }
        });
});

    function s(q) {
        var $this = $(q);
        var $target = $this.attr("data-target");
        var $wajib = 0;
        $.each($("input:visible, select:visible, textarea:visible"), function () {
            if ($(this).attr('wajib')) {
                if ($(this).attr('wajib') == "yes" && ($(this).val() == "" || $(this).val() == null)) {
                    $(this).css('border-color', '#b94a48');
                    $wajib++;
                }
            }
        });
        if ($wajib > 0) {
            BootstrapDialog.show({
                title: '',
                type: BootstrapDialog.TYPE_WARNING,
                message: '<p>Ada <b>' + $wajib + '</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>'
            });
            return false;
        } else {
            BootstrapDialog.confirm('<b>Apakah anda yakin dengan data yang dimasukkan ?</b>', function(r){
                if(r){
                    $.ajax({
                        type: "POST",
                        url: $(q).attr('action'),
                        data: $(q).serialize(),
                        error: function () {
                            BootstrapDialog.show({
                                title: '',
                                type: BootstrapDialog.TYPE_DANGER,
                                message: '<p>Maaf, request halaman tidak ditemukan</p>'
                            });
                        },
                        beforeSend: function(){
                                    if($("#progress").length === 0){
                                        $("body").append($('<div id=\"progress\"></div><div class=\"overlays\"><div class="la-square-spin" style="margin-left:5px; margin-top: 10px; position: fixed; z-index: 19832509; color: #63c5ab;"><div>Loading ... </div></div></div>'));
                                        $('#progress').width((50 + Math.random() * 30) + "%");
                                        $('.overlays').css('width', $('body').css('width'));
                                        $('.overlays').css('height', $('body').css('height'));
                                    }
                                },
                        complete: function(){
                            $("#progress").width("101%").delay(200).fadeOut(400, function(){
                                $("#progress").remove();
                                $(".overlays").remove();
                            });
                        },
                        success: function(data) {
                            if($this.attr('tes')){
                            //  console.log(data);return false;
                                $(".close").click();
                                var dok_id = $this.attr("doc_tipe");
                                $("#no_dok_"+dok_id).html(data[0]['NoDokumen']);
                                $("#penerbit_"+dok_id).html(data[0]['Penerbit']);
                                $("#tgl_dok_"+dok_id).html(data[0]['TglDokumen']);
                                $("#tgl_exp_"+dok_id).html(data[0]['TglAkhir']);
                                $("#View_"+dok_id).html(data[0]['View']);
                            }else{
                                if (data.search("MSG") >= 0) {
                                    var arrdata = data.split('||');
                                    if(arrdata[1] == "YES"){
                                        $(".close").click();
                                        BootstrapDialog.alert({
                                            title:'',
                                            message:arrdata[2],
                                            callback:
                                                function(){
                                                    location.reload();
                                                }
                                        });
                                    }else{
                                        $(".close").click();
                                        BootstrapDialog.alert({
                                            title:'',
                                            message:arrdata[2],
                                            callback:
                                                function(){
                                                    if($this.attr('data-onpost') == "TRUE" ){
                                                        location.reload();
                                                    }else{
                                                        return false;
                                                    }
                                                }
                                        });
                                    }
                                }
                            }   
                        }
                    });
                }
            });
        }
        return false;
    }

    function movefocus(nmfrm, nmthis, nextfocus, event, prevfocus) {
        var tempKey = event.keyCode.toString();
        if (tempKey == 8)
        {
            if ((nmthis.value.length == 0) && (prevfocus != ''))
            {
                nmfrm[prevfocus].focus();
            }
        } else
        {
            if ((nmthis.maxLength == nmthis.value.length) && (nextfocus != '')) {
                nmfrm[nextfocus].select();
                nmfrm[nextfocus].focus();
            }
        }
    }
</script>
<? } ?>
</body>
</html>
