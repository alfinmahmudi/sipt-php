<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<section>
  <div class="page-head">
	<h3 class="m-b-less">Kementerian Perdagangan Republik Indonesia</h3>
	<div class="contact-info" style="margin-top:10px;">
	  <p class="phone"><span class="sub-title">+62-021-3858171</span>&emsp;<span class="sub-title">Ext. 34351</span></p>
	  <p class="email"><span class="sub-title">sipt@kemendag.go.id</span></p> 
	  <span class="sub-title"><i class="ico icon-map"></i>&nbsp;&nbsp;&nbsp;M.I. Ridwan Rais Road, No 5, Jakarta Pusat 10110</span>
	</div>
   </div>
   <div class="row">
	  <div class="col-md-12">
		<div class="w-map-size" id="map"></div>
	  </div>
  </div>
  </section>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script>
      function initMap() {
		  var myLatLng = {lat: -6.181305, lng: 106.833257};

		  var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 18,
			center: myLatLng
		  });

		  var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: 'Direktorat Jenderal Perdagangan Dalam Negeri'
		  });
	}
</script>	
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuvXXgSMhfB0AJgP1I3wQruBjRQwmRHFA&callback=initMap" async defer></script>
<script>$(document).ready(function(e){ $(".datepickers").datepicker({autoclose: true});});</script>
<script>
function s(q){
		var $this = $(q);
		var $target = $this.attr("data-target");
		var $wajib = 0;
		$.each($("input:visible, select:visible, textarea:visible"), function(){
			if($(this).attr('wajib')){
				if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
					$(this).css('border-color','#b94a48');
					$wajib++;
				}
			}
		});
		if($wajib > 0){
			BootstrapDialog.show({
				title: '',
				type: BootstrapDialog.TYPE_WARNING,
				message: '<p>Ada <b>'+$wajib+'</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>',
			});
			return false;
		}else{
			$.ajax({
				  type: "POST",
				  url: $(q).attr('action'),
				  data: $(q).serialize(),
				  error: function() {
					  BootstrapDialog.show({
						  title: '',
						  type: BootstrapDialog.TYPE_DANGER,
						  message: '<p>Maaf, request halaman tidak ditemukan</p>',
					  });
						setTimeout(function(){
							location.reload();
						},2000);
				  },
				  success: function(data){
					  if(data){
						  $($target).html(data);
					  }
				  }
			});
		}
		return false;
}
</script>
</body>
</html>
