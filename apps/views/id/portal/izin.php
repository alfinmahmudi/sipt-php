<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<section>
  <div class="page-head">
				<h3 class="m-b-less"> Perizinan </h3>
				<span class="sub-title">Perizinan Perdagangan Dalam Negeri</span> 
			</div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
<div class="row">
    <form id="divpersyaratan" name = "divpersyaratan" action = "<?= site_url(); ?>portal/get_perizinan" data-target = "#reqlicense" method = "post">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Perizinan Direktorat </header>
          <div class="panel-body">
		  <!--
            <div class="row">
              <label class="col-sm-3 control-label">Direktorat</label>
              <div class="col-sm-9">
                <?//= form_dropdown('direktorat', $direktorat, '', 'id="direktorat" class="form-control input-sm mb-10" data-url = "'.site_url().'get/cb/set_dokumen/" onChange = "combobox($(this), \'#dokumen\'); return false;" wajib = "yes"'); ?>
              </div>
            </div>
			-->
			<?php //?>
			<div class="row">
				 <label class="col-sm-3 control-label">Dokumen Perizinan</label>
				 <div class="col-sm-9" style="overflow:hidden;">
				<div class="input-group select2-bootstrap-append">
						<select name="dokumen" id="single-append-text" onchange="gula()" class="form-control input-sm mb-10 select2-allow-clear select2-offscreen" tabindex="-1" wajib="yes">
							<option></option>
							<?php foreach($dir as $res) {?>
								<optgroup label="<?=$res['uraian'];?>">
									<?php foreach($izin[$res['kode']] as $t){?>
										<option value="<?=$res['kode']."|".$t['id'];?>"><?=$t['nama_izin'];?></option>
									<?php } ?>	
								</optgroup>
							<?php } ?>
						</select>
						<span class="input-group-btn">
							<button class="btn btn-default btn-sm" type="button" data-select2-open="single-append-text">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						</span>	
				   </div>
				   </div>
			</div>
			<div id="garansi">
              
            </div>
			<!--
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Dokumen Perizinan</label>
              <div class="col-sm-9">
                <?//= form_dropdown('dokumen', '', '', 'id="dokumen" class="form-control input-sm  mb-10" wajib="yes"'); ?>
              </div>
            </div>
			-->
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jenis Perizinan</label>
              <div class="col-sm-9">
                <?= form_dropdown('jenis', $jenis, '', 'id="jenis" class="form-control input-sm  mb-10" wajib="yes"'); ?>
              </div>
            </div>
         <!--    <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label"></label>
              <div class="col-sm-9">
                <a href="javascript:changeImage();"><img src="<?= base_url(); ?>keycode.php?<?= md5("YmdHis");  ?>" style="width:130px;" id="img-keycode"></a>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Masukkan Kode</label>
              <div class="col-sm-3">
                <input id="keycode" type="text" name="keycode" wajib="yes" wajib="yes" class="form-control"></a>
              </div>
              <div class="col-sm-6">
              	&nbsp;
              </div>
            </div> -->
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <button class="btn btn-sm btn-warning addon-btn m-b-10" onclick="s('#divpersyaratan'); return false;"><i class="fa fa-archive"></i>Persyaratan</button>
                </span>
              </div>
            </div>
          </div>
        </section>
      </div>
    </form>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading"> Persyaratan Perizinan</header>
        <div class="panel-body" id="reqlicense"></div>
      </section>
    </div>
  </div>
</section>

<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js" type="text/javascript"></script>
<script type="text/javascript"> 
function changeImage(){ 
	document.getElementById("img-keycode").src = "<?= base_url(); ?>keycode.php?rnd="+Math.random(); } 
</script>
<script>
$(document).ready(function(e){
	$(".datepickers").datepicker({
    autoclose: true
  });
	$("#single-append-text").select2({
		placeholder:""
	});
});
	function s(q){
	var $this = $(q);
	var $target = $this.attr("data-target");
	var $wajib = 0;
	$.each($("input:visible, select:visible, textarea:visible"), function(){
		if($(this).attr('wajib')){
			if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
				$(this).css('border-color','#b94a48');
				$wajib++;
			}
		}
	});
	if($wajib > 0){
		BootstrapDialog.show({
			title: '',
			type: BootstrapDialog.TYPE_WARNING,
			message: '<p>Ada <b>'+$wajib+'</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>'
		});
		return false;
	}else{
		$.ajax({
			  type: "POST",
			  url: $(q).attr('action'),
			  data: $(q).serialize(),
			  error: function() {
				  BootstrapDialog.show({
					  title: '',
					  type: BootstrapDialog.TYPE_DANGER,
					  message: '<p>Maaf, request halaman tidak ditemukan</p>'
				  });
			  },
			  
			  success: function(data){
				  if(data){
					  $($target).html(data);
				  }
			  }
		});
	}
	return false;
	}
</script>
<script> 
	function gula(){
	    var get_izin = $('#single-append-text').val();
	    var izin = get_izin.substring(3, 5);
	    var arrgaransi = <?= json_encode($garansi); ?>;
	    //alert(izin);
	    if (izin == '14') {
	      var temp;
	      temp = '<div style="height:5px;"></div>';
	      temp += '<div class="row">';
	      temp += '<label class="col-sm-3 control-label">Jenis Garansi</label>';
	      temp += '<div class="col-sm-9">';
	      temp += '<select class="col-sm-12" name="garansi" style="height: 30px;"">';
	      for (var i = 0; i < <?= count($garansi) ?>; i++) {
	        temp += '<option value="'+arrgaransi[i].kode+'">'+arrgaransi[i].uraian+'</option>';
	      }
	      temp += '</select></div></div>';
	      $('#garansi').append(temp);
	    }else{
	      $('#garansi').html('');
	    }
	}
</script>
</body>
</html>
