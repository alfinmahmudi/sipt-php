<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="author" content="PDN" />
<meta name="keyword" content="PDN" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<?= base_url(); ?>assets/favicon.png"/>
<link href="<?= base_url(); ?>assets/bend/css/style.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/bend/css/style-responsive.css" rel="stylesheet">
<link rel="stylesheet" href="<?= base_url(); ?>assets/fend/js/bootstrap-dialog/bootstrap-dialog.min.css" type="text/css" cache="false" />
<!--[if lt IE 9]>
<script src="<?= base_url(); ?>assets/bend/js/html5shiv.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/respond.min.js"></script>
<![endif]-->
<script src="<?= base_url(); ?>assets/bend/js/jquery-1.10.2.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap.min.js"></script>
<title>Sistem Informasi Perijinan Terpadu - Direktorat Jenderal Perdaganan Dalam Negeri | Kementrian Perdagangan Republik Indonesia</title>
</head>
<body>
<div class="container log-row" style="max-width: 330px; margin: 50px auto 0">
  <div class="row">
    <div class="col-md-12">
      <section class="panel">
        <div class="user-head"> <img src="<?= base_url(); ?>assets/fend/img/logo-large.jpg" alt=""> </div>
        <div class="panel-body">
          <div class="user-desk">
            <div class="avatar"> <img src="<?= base_url(); ?>assets/fend/img/avatarexec.png" alt=""> </div>
            <div class="clearfix"></div>
            <h4 class="text-uppercase">LOGIN SIPT</h4>
            <div style="height:10px;">&nbsp;</div>
            <form id="floginternal" name="floginternal" method="post" action="<?= site_url(); ?>login/timeout/<?= $this->session->userdata('session_id'); ?>" autocomplete="off">
              <div class="form-group">
                <div class="iconic-input"> <i class="icon-user"></i>
                  <input type="text" class="form-control" placeholder="Nama Pengguna" wajib="yes" id="uid" name="uid">
                </div>
              </div>
              <div class="form-group">
                <div class="iconic-input"> <i class="icon-lock"></i>
                  <input type="password" class="form-control" placeholder="Kata Sandi" wajib="yes" id="pwd" name="pwd">
                </div>
              </div>
              
              <div class="form-group">
                <p><img src="<?= base_url(); ?>keycode.php?<?= md5("YmdHis"); ?>" style="width:150px;" id="img-keycode" align="absmiddle"><a href="javascript:changeImage();" title="Ganti / reload kode keamanan"><i class="fa fa-refresh fa-2x"></i></a></p>
              </div>
              
              <div class="form-group">
                <div class="iconic-input"> <i class="icon-key"></i>
                  <input type="text" class="form-control" placeholder="Kode Keamanan" wajib="yes" id="keycode" name="keycode">
                </div>
              </div>
              <div class="form-group">
                <button type="button" class="btn btn-info btn-block w-pad" onClick="login('#floginternal'); return false;"> Masuk </button>
              </div>
              <div class="text-center"> <a href="#" class="w-login">Lupa Kata Sandi ?</a> </div>
            </form>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/bend/js/modernizr.min.js"></script> 
<script src="<?= base_url(); ?>assets/bend/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/fend/js/bootstrap-dialog/bootstrap-dialog.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/fend/js/login.js?ikd10011990" type="text/javascript"></script><script type="text/javascript"> function changeImage(){ document.getElementById("img-keycode").src = "<?= base_url(); ?>keycode.php?rnd="+Math.random(); } </script>
</body>
</html>