<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="page-head">
  <h3 class="m-b-less"> List User Manual</h3>
  <div class="wrapper">
    <div class="row">
      <table class="table table-striped custom-table table-hover">
      <tbody>
        <tr>
        <td >No</td>
        <td >Penggunaan User Manual</td>
        <td >Action</td>
        </tr><?php $a = 2;?>
        <tr>
          <td >1</td>
          <td ><p> Untuk Pelaku Usaha </p></td>
          <td ><a href="<?= base_url('assets/fend/um/pelakuusaha.pdf') ?>"><i class="fa fa-cloud-download fa-lg"></i></a></td>
        </tr>
         <?php 
         foreach ($view as $key) { 
          $wr = array('17','18','19','20'); 
          if (in_array($key['id'], $wr)) {
            $temp = '70efdf2ec9b086079795c442636b55fb';
         }else { 
            $temp = md5($key['id']);
          }
         ?>
        <tr>
          <td ><?= $a ?></td>
          <td ><p><?php echo $key['nama_izin'];?> </p></td>
          <td ><a href="<?= base_url('assets/fend/um').'/'.$temp.'.pdf'?>"><i class="fa fa-cloud-download fa-lg"></i></a></td>
          <?php $a++;?>
        </tr>
        <?php } ?>
        <tr>
          <td >20</td>
          <td ><p> Modul Pelaporan Antar Pulau </p></td>
          <td ><a href="<?= base_url('assets/fend/um/Modul Pelaporan Antar Pulau.pdf') ?>"><i class="fa fa-cloud-download fa-lg"></i></a></td>
        </tr>
      </tbody>
      </table>
    </div>
  </div>
