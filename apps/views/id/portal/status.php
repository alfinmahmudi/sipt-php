<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<section>
    <div class="page-head">
        <h3 class="m-b-less"> Perizinan </h3>
        <span class="sub-title">Perizinan Perdagangan Dalam Negeri</span> 
    </div>
    <div class="wrapper">
        <div class="row">
            <form id="persyaratan" name = "persyaratan" action = "<?= site_url(); ?>portal/serach_status" data-target ="#result" method = "post" autocomplate="off">
                <div class="row">
                    <label class="col-sm-3 control-label">NPWP Perusahaan<font size ="2" color="red">*</font></label>
                    <div class="col-lg-9" id="npwp">
                        <div class="col-lg-2" id="npwp">
                            <input name="NPWP[1]" id="npwp1" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form, this, 'npwp2', event, '');" maxlength="2" size="2" nomor="yes" >
                        </div>
                        <div class="col-lg-2" id="npwp">
                            <input name="NPWP[2]" id="npwp2" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form, this, 'npwp3', event, '');" maxlength="3" size="3" nomor="yes" >
                        </div>
                        <div class="col-lg-2" id="npwp">
                            <input name="NPWP[3]" id="npwp3" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form, this, 'npwp4', event, '');" maxlength="3" size="3" nomor="yes" >
                        </div>
                        <div class="col-lg-2" id="npwp">
                            <input name="NPWP[4]" id="npwp4" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form, this, 'npwp5', event, '');" maxlength="1" size="1" nomor="yes" >
                        </div>
                        <div class="col-lg-2" id="npwp">
                            <input name="NPWP[5]" id="npwp5" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form, this, 'npwp6', event, '');" maxlength="3" size="3" nomor="yes" >
                        </div>
                        <div class="col-lg-2" id="npwp">
                            <input name="NPWP[6]" id="npwp6" wajib="yes" class="form-control nomor" onkeyup="movefocus(this.form, this, 'lampnpwp', event, '');" maxlength="3" size="3" nomor="yes" >
                        </div>
                        <div class="col-lg-12 col-sm-12 alert alert-danger alert-dismissable" style="display: none; background: red; color: white;" id="warn_npwp" contenteditable="true">
                            <strong></strong>
                        </div>
                    </div>
                </div>
                <div style="height:5px;"></div>
                <div class="row">
                    <label class="col-sm-3 control-label">Nomor Permohonan <font size ="2" color="red">*</font></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control mb-10"  name="no_permohonan" wajib="yes" value="<?= $sess['tipe_permohonan']; ?>" />
                    </div>
                </div>

                <!--div class="row">
                         <label class="col-sm-3 control-label">Status Permohonan <font size ="2" color="red">*</font></label>
                         <div class="col-sm-9">
                <?= form_dropdown('jenis', $jenis, '', 'id="jenis" class="form-control input-sm  mb-10" wajib="yes"'); ?>
                         </div>
                </div-->
                <div style="height:5px;"></div>
                <div class="row">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <a href="javascript:changeImage();"><img src="<?= base_url(); ?>keycode.php?<?= md5("YmdHis"); ?>" style="width:130px;" id="img-keycode"></a>
                    </div>
                </div>
                <div style="height:5px;"></div>
                <div class="row">
                    <label class="col-sm-3 control-label">Masukkan Kode</label>
                    <div class="col-sm-3">
                        <input id="keycode" type="text" name="keycode" wajib="yes" wajib="yes" class="form-control"></a>
                    </div>
                    <div class="col-sm-6">
                        &nbsp;
                    </div>
                </div>
                <div style="height:25px;"></div>
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-sm btn-danger addon-btn m-b-10" onclick="s('#persyaratan'); return false;"><i class="fa fa-search"></i>Cari</button>
                    </div>	
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="result"></div>
            </div>	
        </div>
    </div>
</section>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    
                            function changeImage() {
                                document.getElementById("img-keycode").src = "<?= base_url(); ?>keycode.php?rnd=" + Math.random();
                            }
</script>

<script>
$(document).ready(function (e) {
$('.nomor').keydown(function(event){
            var a = event.keyCode;
            var b = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 9, 11];
            if (jQuery.inArray(a,b) === -1) {
                event.preventDefault();
            }
        });
});

    function s(q) {
        var $this = $(q);
        var $target = $this.attr("data-target");
        var $wajib = 0;
        $.each($("input:visible, select:visible, textarea:visible"), function () {
            if ($(this).attr('wajib')) {
                if ($(this).attr('wajib') == "yes" && ($(this).val() == "" || $(this).val() == null)) {
                    $(this).css('border-color', '#b94a48');
                    $wajib++;
                }
            }
        });
        if ($wajib > 0) {
            BootstrapDialog.show({
                title: '',
                type: BootstrapDialog.TYPE_WARNING,
                message: '<p>Ada <b>' + $wajib + '</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>'
            });
            return false;
        } else {
            $.ajax({
                type: "POST",
                url: $(q).attr('action'),
                data: $(q).serialize(),
                error: function () {
                    BootstrapDialog.show({
                        title: '',
                        type: BootstrapDialog.TYPE_DANGER,
                        message: '<p>Maaf, request halaman tidak ditemukan</p>'
                    });
                },
                success: function (data) {
                    if (data) {
                        $($target).html(data);
                    }
                }
            });
        }
        return false;
    }

    function movefocus(nmfrm, nmthis, nextfocus, event, prevfocus) {
        var tempKey = event.keyCode.toString();
        if (tempKey == 8)
        {
            if ((nmthis.value.length == 0) && (prevfocus != ''))
            {
                nmfrm[prevfocus].focus();
            }
        } else
        {
            if ((nmthis.maxLength == nmthis.value.length) && (nextfocus != '')) {
                nmfrm[nextfocus].select();
                nmfrm[nextfocus].focus();
            }
        }
    }
</script>
</body>
</html>
