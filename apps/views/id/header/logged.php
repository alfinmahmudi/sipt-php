<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="author" content="PDN" />
        <meta name="keyword" content="PDN" />
        <meta name="description" content="" />
        <link rel="shortcut icon" href="<?= base_url(); ?>assets/favicon.png"/>
        <link href="<?= base_url(); ?>assets/bend/js/icheck/skins/all.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/bend/css/style.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/bend/css/select2.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/bend/css/style-responsive.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/bend/js/bootstrap-dialog/bootstrap-dialog.min.css" rel="stylesheet"  type="text/css" cache="false" />
        <link href="<?= base_url(); ?>assets/bend/css/introjs.min.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/bend/css/jquery-te-1.4.0.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/bend/css/jquery-te-green.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="<?= base_url(); ?>assets/bend/js/html5shiv.js"></script>
        <script src="<?= base_url(); ?>assets/bend/js/respond.min.js"></script>
        <![endif]-->
        <script src="<?= base_url(); ?>assets/bend/js/jquery-1.10.2.min.js"></script> 
        <script src="<?= base_url(); ?>assets/bend/js/bootstrap.min.js"></script>
        <script src="<?= base_url(); ?>assets/bend/js/bootstrap-dialog/bootstrap-dialog.min.js" type="text/javascript"></script>
        <script> var _base_url = '<?= base_url(); ?>';</script>
        <script src="<?= base_url(); ?>assets/bend/js/jquery-te-1.4.0.js"></script>
        <script src="<?= base_url(); ?>assets/bend/js/jquery-te-1.4.0.min.js"></script>
        <script src="<?= base_url(); ?>assets/bend/js/intro.min.js"></script>
