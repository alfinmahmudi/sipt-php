<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE HTML>
<!--[if IE 8]> <html class="ie8 no-js"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<!-- begin meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=10">
<meta name="description" content="PDN">
<meta name="keywords" content="PDN">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="<?= base_url(); ?>assets/favicon.png"/>
<link href="<?=base_url();?>assets/bend/css/style.css" rel="stylesheet">
<link href="<?=base_url();?>assets/fend/css/css.css" type="text/css" rel="stylesheet" id="main-style">
<!--[if IE]> <link href="<?= base_url(); ?>assets/fend/css/css/ie.css" type="text/css" rel="stylesheet"> <![endif]-->
<link href="<?= base_url(); ?>assets/fend/css/colors/orange.css" type="text/css" rel="stylesheet" id="color-style">
<link rel="stylesheet" href="<?= base_url(); ?>assets/fend/js/bootstrap-dialog/bootstrap-dialog.min.css" type="text/css" cache="false" />
<link href="<?=base_url();?>assets/bend/css/select2.css" rel="stylesheet">
<link href="<?=base_url();?>assets/bend/css/select2-bootstrap.css" rel="stylesheet">

	<!--common style-->
<script src="<?= base_url(); ?>assets/fend/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/fend/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/fend/js/ie.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/fend/js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/fend/js/modernizr.custom.js" type="text/javascript"></script>
<!--[if IE 8]><script src="<?= base_url(); ?>assets/fend/js/respond.min.js" type="text/javascript"></script><![endif]-->
<!-- <script src="<?= base_url(); ?>assets/fend/js/ddlevelsmenu.js" type="text/javascript"></script>
<script type="text/javascript">
		ddlevelsmenu.setup("nav", "topbar");
 </script> !-->
<script src="<?= base_url(); ?>assets/fend/js/tinynav.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/fend/js/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/fend/js/jquery.jcarousel.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/fend/js/jquery.cycle.all.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/fend/js/bootstrap-dialog/bootstrap-dialog.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/fend/js/portal.js?ikd10011990" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/fend/js/jssor.slider-21.1.6.mini.js" type="text/javascript"></script>
<script>
	$(document).ready(function(){
		$(".select2-allow-clear").select({
			placeholder:"Pilih Dokument",
			allowClear: true
		});
	});
</script>


