<?php if ( ! defined( 'BASEPATH')) exit( 'No direct script access allowed'); ?>
    <div id="main-wrapper">
        <form id="ftembusan" name="ftembusan" data-redirect="true" action="<?= $act; ?>" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <div class="panel-title">Pengaturan SLA
                                <br>
                                <br>
                                <?= $nama_izin['nama_izin'];?>
                            </div>
                            <div class="panel-control">&nbsp;</div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th class="col-md-6">Jenis Status</th>
                                                <th class="col-md-4">Lama Waktu Proses</th>
                                                <th class="col-md-2"></th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody class="bodyta">
                                            <tr id="trta_<?= rand(); ?>">
                                                <td>
                                                    <input type="hidden" class="form-control input-sm" name="sla[sla_id][]" value="<?= $sess[0]['sla_id']; ?>" readonly="readonly" />
                                                    <?= form_dropdown('sla[sla_status][]', $status, $sess[0]['sla_status'], 'id="jns_gol_b" wajib="yes" class="form-control input-sm select2" '); ?>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control input-sm" name="sla[sla_waktu][]" value="<?= $sess[0]['sla_waktu']; ?>">
                                                </td>
                                                <td>
                                                    <label>Menit</label>
                                                </td>
                                            </tr>
                                            <?php if(count($sess)>1): for($i = 1; $i < count($sess); $i++):?>
                                                <tr id="trta_<?= rand(); ?>">
                                                    <td>
                                                    <input type="hidden" class="form-control input-sm" name="sla[sla_id][]" value="<?= $sess[$i]['sla_id']; ?>" readonly="readonly" />
                                                    <?= form_dropdown('sla[sla_status][]', $status, $sess[$i]['sla_status'], 'id="jns_gol_b" wajib="yes" class="form-control input-sm select2" '); ?> 
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control input-sm" name="sla[sla_waktu][]" value="<?= $sess[$i]['sla_waktu']; ?>">
                                                    </td>
                                                    <td>
                                                        <label>Menit</label>
                                                    </td>
                                                </tr>
                                                <?php endfor; endif;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <button type="button" class="btn btn-default btn-addon m-b-sm btn-sm" onclick="closedialog();"><i class="fa fa-undo"></i> Batal</button>
                            <button type="button" class="btn btn-info btn-addon m-b-sm btn-sm" onclick="post('#ftembusan'); return false;"><i class="fa fa-check-square-o"></i> Simpan</button>
                            <!-- <button class="btn btn-sm btn-success addon-btn m-b-10" ><i class="fa fa-arrow-right"></i>Simpan</button> -->
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script src="<?= base_url(); ?>assets/bend/js/sipt.js?v=<?= date(" Ymdhis "); ?>"></script>