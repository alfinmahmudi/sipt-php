<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Siupmb_act extends CI_Model{
	
	var $ineng = "";
	
	function get_first($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			$arrdata = array();
			$arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2","id", "nama", TRUE);
			$arrdata['direktorat'] = $dir;
			$arrdata['tipe_permohonan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERMOHONAN'","kode","uraian", TRUE);
			$arrdata['tipe_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'TIPE_PERUSAHAAN'","kode","uraian", TRUE);
			$arrdata['lokasi'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'LOKASI_PERUSAHAAN'","kode","uraian", TRUE);
			$arrdata['status_perusahaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'STATUS_PERUSAHAAN'","kode","uraian", TRUE);
			$arrdata['jenis_siup'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_SIUP'","kode","uraian", TRUE);
			$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
			$nama_izin = $this->main->get_uraian($sql,'nama_izin');
			$arrdata['nama_izin'] = $nama_izin;
			
			if($id==""){
				$arrdata['act'] = site_url('post/licensing/siupmb_act/first/save');
				//$type = hashids_decrypt($type,_HASHIDS_,6);
				$arrdata['sess']['tipe_permohonan'] = $type;
				$arrdata['sess']['kd_izin'] = $doc;
			}else{
				$arrdata['act'] = site_url('post/licensing/siupmb_act/first/update');
				$query = "SELECT id, kd_izin, no_aju, tgl_aju, trader_id, tipe_permohonan, npwp, tipe_perusahaan, nm_perusahaan, almt_perusahaan, 
						kdprop, kdkab, kdkec, kdkel, kdpos, telp, fax, lokasi, status_milik, no_siup, tgl_siup, jenis_siup
						FROM t_siupmb WHERE id = '".$id."'";
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['sess'] = $row;
					}
					$arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '".$row['kdprop']."' ORDER BY 2","id", "nama", TRUE);
					$arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '".$row['kdkab']."' ORDER BY 2","id", "nama", TRUE);
					$arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '".$row['kdkec']."' ORDER BY 2","id", "nama", TRUE);
				}
			}
			return $arrdata;
		}
	}
	
	function set_first($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			$msg = "MSG||NO||Data Permohonan SIUP-MB gagal disimpan";
			$ressiup = FALSE;
			$arrsiup = $this->main->post_to_query($this->input->post('siupmb'));
			$dir = $this->input->post('direktorat');
			$doc = $arrsiup['kd_izin'];
			$type = $arrsiup['tipe_permohonan'];
			if ($doc == "6") {
				$txt = "Surat Keterangan Pengecer Minuman Beralkohol Golongan A (SKP-A)";
			}elseif ($doc == "13") {
				$txt = "Surat Keterangan Penjual Langsung Minuman Beralkohol Golongan A (SKPL-A)";
			}else{
				$txt = "SIUP MB";
			}

			if($act == "save"){
				$arrsiup['no_aju'] = $this->main->set_aju();
				$arrsiup['fl_pencabutan'] = '0';
				$arrsiup['status'] = '0000';
				$arrsiup['tgl_aju'] = 'GETDATE()';
				$arrsiup['trader_id'] = $this->newsession->userdata('trader_id');
				$arrsiup['created'] = 'GETDATE()';
				$arrsiup['created_user'] = $this->newsession->userdata('username');
				$this->db->trans_begin();
				$exec = $this->db->insert('t_siupmb', $arrsiup);
				if($this->db->affected_rows() > 0){
					$ressiup = TRUE;
					$idredir = $this->db->insert_id();
					$msg = "MSG||YES||Data Permohonan ".$txt." berhasil disimpan. Silahkan lanjutkan mengisi data penanggung jawab||".site_url().'licensing/form/second/'.$dir.'/'.$doc.'/'.$type.'/'.$idredir;
					
					/* Log User dan Log Izin */
					$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' dengan nomor permohonan : '.$arrsiup['no_aju'],
								  'url' => '{c}'. site_url().'post/licensing/siupmb_act/first/save'. ' {m} models/licensing/siupmb_act {f} set_first($act, $ajax)');
					$this->main->set_activity($logu);
					$logi = array('kd_izin' => $arrsiup['kd_izin'],
								  'permohonan_id' => $idredir,
								  'keterangan' => 'Menambahkan daftar permohonan '.$txt.' dengan nomor permohonan : '.$arrsiup['no_aju'],
								  'catatan' => '',
								  'status' => '0000',
								  'selisih' => 0);
					$this->main->set_loglicensing($logi);			  
					/* Akhir Log User dan Log Izin */

				}
				if($this->db->trans_status() === FALSE || !$ressiup){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				return $msg;
			}elseif ($act == "update") {
				$id = $this->input->post('id');
				$this->db->where('id',$id);
				$this->db->update('t_siupmb',$arrsiup);
				if($this->db->affected_rows() > 0){
					$ressiup = TRUE;
					$msg = "MSG||YES||Data Permohonan ".$txt." berhasil diupdate.||REFRESH";

				}
				return $msg;
			}
		}
	}

	function get_second($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			$arrdata['act'] = site_url('post/licensing/siupmb_act/second/save');
			$arrdata['direktorat'] = $dir;
			$arrdata['jenis_identitas'] = $this->main->set_combobox("SELECT kode, uraian FROM m_reff WHERE jenis = 'JENIS_IDENTITAS'","kode","uraian", TRUE);
			$arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2","id", "nama", TRUE);
			$arrdata['urifirst'] = site_url('licensing/form/first/'.$dir.'/'.$doc.'/'.$type.'/'.$id);
			$query = "SELECT id, kd_izin, tipe_permohonan, identitas_pj, noidentitas_pj, nama_pj, jabatan_pj, tmpt_lahir_pj, tgl_lahir_pj, alamat_pj, kdprop_pj, kdkab_pj, kdkec_pj,kdkel_pj, telp_pj, fax_pj, kdprop, kdkab, kdkec, kdkel
						FROM t_siupmb WHERE id = '".$id."'";
			$data = $this->main->get_result($query);
			if($data){
				foreach($query->result_array() as $row){
					$arrdata['sess'] = $row;
				}
				if($row['identitas_pj']){
					$arrdata['act'] = site_url('post/licensing/siupmb_act/second/update');
				}
				$arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab WHERE id_prov = '".$row['kdprop_pj']."' ORDER BY 2","id", "nama", TRUE);
				$arrdata['kec'] = $this->main->set_combobox("SELECT id, nama FROM m_kec WHERE id_kabupaten = '".$row['kdkab_pj']."' ORDER BY 2","id", "nama", TRUE);
				$arrdata['kel'] = $this->main->set_combobox("SELECT id, nama FROM m_kel WHERE id_kecamatan = '".$row['kdkec_pj']."' ORDER BY 2","id", "nama", TRUE);
			}
			$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
			$nama_izin = $this->main->get_uraian($sql,'nama_izin');
			$arrdata['nama_izin'] = $nama_izin;
			return $arrdata;
		}
	}

	function set_second($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(($act == "update") || ($act == "save")){
				if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
				}
				$msg = "MSG||NO||Data Penanggung Jawab gagal disimpan";
				$respj = FALSE;
				$arrpj = $this->main->post_to_query($this->input->post('pj'));
				$dir = $this->input->post('direktorat');
				$doc = $this->input->post('kd_izin');
				$type = $this->input->post('tipe_permohonan');
				$id = $this->input->post('id');
				if ($doc == "6") {
					$txt = "Surat Keterangan Pengecer Minuman Beralkohol Golongan A (SKP-A)";
				}elseif ($doc == "13") {
					$txt = "Surat Keterangan Penjual Langsung Minuman Beralkohol Golongan A (SKPL-A)";
				}else{
					$txt = "SIUP MB";
				}
				$this->db->where('id',$id);
				$this->db->update('t_siupmb',$arrpj);
				if($this->db->affected_rows() > 0){
					$respj = TRUE;
					if($act == "update"){
						$msg = "MSG||YES||Data Penanggung Jawab berhasil diupdate.||REFRESH";
					}else{
						$msg = "MSG||YES||Data Penanggung Jawab berhasil disimpan. Silahkan lanjutkan mengisi data nilai modal dan kekayaan||".site_url().'licensing/form/third/'.$dir.'/'.$doc.'/'.$type.'/'.$id;
					}
					/* Log User dan Log Izin */
					$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' (Data Penanggung Jawab)',
								  'url' => '{c}'. site_url().'post/licensing/siupmb_act/second/update'. ' {m} models/licensing/siupmb_act {f} set_second($act, $isajax)');
					$this->main->set_activity($logu);
					/* Akhir Log User */
				}
				return $msg;
			}
		}
	}

	function get_third($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			if($id==""){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}else{
				$arrdata['act'] = site_url('post/licensing/siupmb_act/third/save');
				$arrdata['urisecond'] = site_url('licensing/form/second/'.$dir.'/'.$doc.'/'.$type.'/'.$id);
				$query = "SELECT id, kd_izin, tipe_permohonan, nilai_modal, kegiatan_usaha, kelembagaan, bank1, alamat_bank1, bank2, alamat_bank2, kbli, desc_kbli, 
						  fl_gol_a, fl_gol_b, fl_gol_c, gol_a, gol_b, gol_c, pemasaran
						  FROM t_siupmb WHERE id = '".$id."'";
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['sess'] = $row;
					}
					$arrdata['pemasaran'] = explode(';', $row['pemasaran']);
					if ($row['nilai_modal']) {
						$arrdata['act'] = site_url('post/licensing/siupmb_act/third/update');
					}
				}
				if($doc == '4'){
					$arrdata['propinsi'] = $this->main->set_combobox("SELECT id, nama FROM m_prop ORDER BY 2","id", "nama", TRUE);
				}elseif ($doc == '5') {
					$arrdata['kab'] = $this->main->set_combobox("SELECT id, nama FROM m_kab ORDER BY 2","id", "nama", TRUE);
				}
				$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
				$nama_izin = $this->main->get_uraian($sql,'nama_izin');
				$arrdata['nama_izin'] = $nama_izin;
				$arrdata['direktorat'] = $dir;
				$arrdata['kelembagaan'] = $this->main->set_combobox("SELECT kode, uraian FROM m_kelembagaan","kode","uraian", TRUE);
				return $arrdata;
			}
		}
	}

	function set_third($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(($act == "update") || ($act == "save")){
				if(!$isajax){
					return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
					exit();
				}
				$msg = "MSG||NO||Data gagal disimpan";
				$respj = FALSE;
				$arrsiup = $this->main->post_to_query($this->input->post('siupmb'));
				$arrsiup['fl_gol_a'] = $this->input->post('fl_gol_a');
				$arrsiup['fl_gol_b'] = $this->input->post('fl_gol_b');
				$arrsiup['fl_gol_c'] = $this->input->post('fl_gol_c');
				$pemasaran = implode(';',$this->input->post('pemasaran'));
				$arrsiup['pemasaran'] = $pemasaran;
				//print_r($arrsiup);die();
				$dir = $this->input->post('direktorat');
				$doc = $this->input->post('kd_izin');
				$type = $this->input->post('tipe_permohonan');
				$id = $this->input->post('id');
				if ($doc == "6") {
					$txt = "Surat Keterangan Pengecer Minuman Beralkohol Golongan A (SKP-A)";
				}elseif ($doc == "13") {
					$txt = "Surat Keterangan Penjual Langsung Minuman Beralkohol Golongan A (SKPL-A)";
				}else{
					$txt = "SIUP MB";
				}
				$this->db->where('id',$id);
				$this->db->update('t_siupmb',$arrsiup);
				if($this->db->affected_rows() > 0){
					$respj = TRUE;
					if($act == "update"){
						$msg = "MSG||YES||Data Nilai Modal dan Kekayaan berhasil diupdate.||REFRESH";
					}else{
						$msg = "MSG||YES||Data Nilai Modal dan Kekayaan berhasil disimpan. Silahkan lanjutkan mengisi data persyaratan||".site_url().'licensing/form/fourth/'.$dir.'/'.$doc.'/'.$type.'/'.$id;
					}
					/* Log User dan Log Izin */
					$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' (Data Nilai Modal dan Kekayaan)',
								  'url' => '{c}'. site_url().'post/licensing/siupmb_act/third/update'. ' {m} models/licensing/siupmb_act {f} set_third($act, $isajax)');
					$this->main->set_activity($logu);
					/* Akhir Log User */
				}
				return $msg;
			}
		}
	}

	function get_fourth($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			if ($type == '01') {
				$addCon = "AND a.baru = '1'";
			}elseif ($type=='02') {
				$addCon = "AND a.perpanjangan = '1'";
			}elseif ($type=='03') {
				$addCon = "AND a.perubahan = '1'";
			}
			$sql = "SELECT nama_izin FROM m_izin WHERE id = '".$doc."' AND direktorat_id='".$dir."'";
			$nama_izin = $this->main->get_uraian($sql,'nama_izin');
			$arrdata['nama_izin'] = $nama_izin;
			$arrdata['izin_id'] = $doc;
			$arrdata['direktorat'] = $dir;
			$arrdata['type'] = $type;
			$arrdata['permohonan_id'] = $id;
			$arrdata['urithird'] = site_url('licensing/form/third/'.$dir.'/'.$doc.'/'.$type.'/'.$id);
			$query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							'<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id 
							WHERE a.izin_id = '".$doc."' and a.permohonan_id = '".$id."' and a.detail_id is null";
			$data = $this->main->get_result($query_syarat);
			$arr = array();
			if($data){
				foreach($query_syarat->result_array() as $keys){
					//$arr[$keys['dok_id']] = $keys;
					if (!isset($arr[$keys['dok_id']])){
					  $arr[$keys['dok_id']] = array();
					}
					$arr[$keys['dok_id']][] = $keys;
				}
				$arrdata['sess'] = $arr;
				$arrdata['act'] = site_url('post/licensing/siupmb_act/fourth/update');

			}else{
				$arrdata['act'] = site_url('post/licensing/siupmb_act/fourth/save');
			}

			$query= "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian, a.multi
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.izin_id = '".$doc."' ".$addCon;
			$data_req = $this->main->get_result($query);
			$arrdata['req'] = $query->result_array();

			return $arrdata;
		}
	}

	function set_fourth($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(!$isajax){
				return "MSG||NO||Mohon maaf, direct aksess tidak diperbolehkan.";
				exit();
			}
			$msg = "MSG||NO||Data gagal disimpan";
			$ressiup = FALSE;
			$arrsiup = $this->main->post_to_query($this->input->post('SIUPMB'));
			$arrreq = $this->input->post('REQUIREMENTS');
			$arrkeys = array_keys($arrreq);
			if ($arrsiup['izin_id'] == "6") {
				$txt = "Surat Keterangan Pengecer Minuman Beralkohol Golongan A (SKP-A)";
			}elseif ($arrsiup['izin_id'] == "13") {
				$txt = "Surat Keterangan Penjual Langsung Minuman Beralkohol Golongan A (SKPL-A)";
			}else{
				$txt = "SIUP MB";
			}
			if($act == "save"){
				for($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++){
					$requirements = array('izin_id' => $arrsiup['izin_id'],
					     'permohonan_id' => $arrsiup['permohonan_id'],
					     'created' => 'GETDATE()',
					     'created_user' => $this->newsession->userdata('username'));
					for($j=0;$j<count($arrkeys);$j++){
						$requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
					}
					unset($requirements['id']);
					$this->db->insert('t_upload_syarat', $requirements);
				}
				if($this->db->affected_rows() > 0){
					$ressiup = TRUE;
					$idUpload = $this->db->insert_id();
					/* Log User dan Log Izin */
					$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' (Data Persyaratan)',
								  'url' => '{c}'. site_url().'post/licensing/siupmb_act/fourth/save'. ' {m} models/licensing/siupmb_act {f} set_fourth($act, $isajax)');
					$this->main->set_activity($logu);
					/* Akhir Log User */
					$msg = "MSG||YES||Data Persyaratan berhasil disimpan. Pengajuan Permohonan ".$txt." selesai.||".site_url().'licensing/view/status';
				}
				if($this->db->trans_status() === FALSE || !$ressiup){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
			}elseif($act == "update"){
				$this->db->where('permohonan_id', $arrsiup['permohonan_id']);
				$this->db->where('izin_id', $arrsiup['izin_id']);
        		$this->db->delete('t_upload_syarat');
        		if($this->db->affected_rows() > 0){
        			for($s = 0; $s < count($_POST['REQUIREMENTS']['dok_id']); $s++){
						$requirements = array('izin_id' => $arrsiup['izin_id'],
						     'permohonan_id' => $arrsiup['permohonan_id'],
						     'created' => 'GETDATE()',
						     'created_user' => $this->newsession->userdata('username'));
						for($j=0;$j<count($arrkeys);$j++){
							$requirements [$arrkeys[$j]] = $arrreq[$arrkeys[$j]][$s];
						}
						unset($requirements['id']);
						$this->db->insert('t_upload_syarat', $requirements);
					}
					if($this->db->affected_rows() > 0){
						$ressiup = TRUE;
						$idUpload = $this->db->insert_id();
						/* Log User dan Log Izin */
						$logu = array('aktifitas' => 'Menambahkan daftar permohonan '.$txt.' (Update - Data Persyaratan)',
									  'url' => '{c}'. site_url().'post/licensing/siupmb_act/fourth/update'. ' {m} models/licensing/siupmb_act {f} set_fourth($act, $isajax)');
						$this->main->set_activity($logu);
						/* Akhir Log User */
						$msg = "MSG||YES||Data Persyaratan berhasil diupdate.||REFRESH";
					}
					if($this->db->trans_status() === FALSE || !$ressiup){
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();
					}
        		}
			}
			return $msg;
		}
	}

	function get_preview($dir, $doc, $type, $id){
		if($this->newsession->userdata('_LOGGED')){
			if ($type == '01') {
				$addCon = "AND a.baru = '1'";
			}elseif ($type=='02') {
				$addCon = "AND a.perpanjangan = '1'";
			}elseif ($type=='03') {
				$addCon = "AND a.perubahan = '1'";
			}

			if ($dir == '02') {
				$query = "SELECT a.id, a.kd_izin, a.no_aju, a.trader_id, a.tipe_permohonan, c.uraian AS permohonan ,a.npwp, d.uraian AS tipe_perusahaan, a.nm_perusahaan, a.almt_perusahaan, 
						e.nama AS kdprop, f.nama AS kdkab, g.nama AS kdkec, h.nama AS kdkel, a.kdpos, a.telp, a.fax, 
						i.uraian AS lokasi, j.uraian AS status_milik, a.no_siup, a.tgl_siup, k.uraian AS jenis_siup,
						l.uraian AS identitas_pj, a.nama_pj, a.noidentitas_pj, a.jabatan_pj, a.tmpt_lahir_pj, a.tgl_lahir_pj, a.alamat_pj, 
						m.nama AS kdprop_pj, n.nama AS kdkab_pj, o.nama AS kdkec_pj, p.nama AS kdkel_pj, a.telp_pj, a.fax_pj,
						a.nilai_modal, a.kegiatan_usaha, a.status, q.uraian AS kelembagaan, a.kbli, a.desc_kbli, a.bank1, a.alamat_bank1, a.bank2, a.alamat_bank2, a.pemasaran,
						a.fl_gol_a, a.fl_gol_b, a.fl_gol_c, a.gol_a, a.gol_b, a.gol_c, b.nama_izin, b.disposisi
						FROM t_siupmb a 
						LEFT JOIN m_izin b ON b.id = a.kd_izin 
						LEFT JOIN m_reff c ON c.kode = a.tipe_permohonan AND c.jenis = 'TIPE_PERMOHONAN' 
						LEFT JOIN m_reff d ON d.kode = a.tipe_perusahaan AND d.jenis = 'TIPE_PERUSAHAAN'
						LEFT JOIN m_prop e ON e.id = a.kdprop
						LEFT JOIN m_kab f ON f.id = a.kdkab
						LEFT JOIN m_kec g ON g.id = a.kdkec
						LEFT JOIN m_kel h ON h.id = a.kdkel
						LEFT JOIN m_reff i ON i.kode = a.lokasi AND i.jenis = 'LOKASI_PERUSAHAAN'
						LEFT JOIN m_reff j ON j.kode = a.status_milik AND j.jenis = 'STATUS_PERUSAHAAN'
						LEFT JOIN m_reff k ON k.kode = a.jenis_siup AND k.jenis = 'JENIS_SIUP'
						LEFT JOIN m_reff l ON l.kode = a.identitas_pj AND l.jenis = 'JENIS_IDENTITAS'
						LEFT JOIN m_prop m ON m.id = a.kdprop_pj
						LEFT JOIN m_kab n ON n.id = a.kdkab_pj
						LEFT JOIN m_kec o ON o.id = a.kdkec_pj
						LEFT JOIN m_kel p ON p.id = a.kdkel_pj
						LEFT JOIN m_kelembagaan q ON q.kode = a.kelembagaan WHERE a.id = '".$id."'";
				$data = $this->main->get_result($query);
				if($data){
					foreach($query->result_array() as $row){
						$arrdata['sess'] = $row;
					}
					if($doc=='4'){
						$idprop = str_replace(";", "','", $row['pemasaran']);
						$sqlprop = "SELECT nama FROM m_prop WHERE id IN ('".$idprop."')";
						$dataprop = $this->main->get_result($sqlprop);
						if($dataprop){
							foreach($sqlprop->result_array() as $rprop){
								$arrprop[] = $rprop['nama'];
							}
							$arrdata['pemasaran'] = join(", ", $arrprop);
						}
					}elseif ($doc=='5') {
						$idkab = str_replace(";", "','", $row['pemasaran']);
						$sqlkab = "SELECT nama FROM m_kab WHERE id IN ('".$idkab."')";
						$datakab = $this->main->get_result($sqlkab);
						if($datakab){
							foreach($sqlkab->result_array() as $rkab){
								$arrkab[] = $rkab['nama'];
							}
							$arrdata['pemasaran'] = join(", ", $arrkab);
						}
					}
					$arrdata['act'] = site_url().'post/proccess/siupmb_act/verification';
					$arrdata['input'] = $this->get_input($dir, $doc, $type, $id, $row['status']);
					$arrdata['proses'] = $this->main->set_proses($dir, $row['kd_izin'], $row['disposisi'], $this->newsession->userdata('role'), $row['status'],hashids_encrypt($row['id'],_HASHIDS_,9));
					$arrdata['jmllog'] = $this->main->get_uraian("SELECT COUNT(*) AS JML FROM t_log_izin WHERE permohonan_id = '".$row['id']."' AND kd_izin = '".$row['kd_izin']."'","JML");
					$arrdata['urllog'] = site_url().'get/log/izin/'.hashids_encrypt($row['id'],_HASHIDS_,9).'/'.hashids_encrypt($row['kd_izin'],_HASHIDS_,9);
				}
			}

			$query_syarat = "SELECT a.id, a.izin_id, a.dok_id, a.upload_id, b.nomor, b.tgl_dok, b.tgl_exp, b.penerbit_dok,
							'<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
							FROM t_upload_syarat a
							LEFT join t_upload b on b.id = a.upload_id 
							WHERE a.permohonan_id = '".$id."' AND a.izin_id = ".$doc." AND a.detail_id is null";
			$data = $this->main->get_result($query_syarat);
			$arr = array();
			if($data){
				/*foreach($query_syarat->result_array() as $keys){
					$arr[$keys['dok_id']] = $keys;
				}
				*/
				foreach($query_syarat->result_array() as $keys){
					//$arr[$keys['dok_id']] = $keys;
					if (!isset($arr[$keys['dok_id']])){
					  $arr[$keys['dok_id']] = array();
					}
					$arr[$keys['dok_id']][] = $keys;
				}
				$arrdata['sess_syarat'] = $arr;

			}
			$query= "SELECT a.id, a.dok_id, b.keterangan, b.kode , a.tipe, c.uraian
					FROM m_dok_izin a 
					LEFT JOIN m_dok b ON b.id = a.dok_id
					LEFT JOIN m_reff c ON c.kode = a.tipe and c.jenis='TIPE_DOKIZIN'
					WHERE a.kategori <> '03' and a.izin_id = '".$doc."' ".$addCon;
			$data_req = $this->main->get_result($query);
			$arrdata['req'] = $query->result_array();
			$arrdata['dir'] = $dir;
			return $arrdata;
		}
	}
	
	function get_input($dir, $doc, $type, $id, $stts){
        if($this->newsession->userdata('_LOGGED')){
            $arrstts = array('0102');
            $arrdata = array();
            if(in_array($stts, $arrstts)){
                $query = "SELECT a.id, a.kd_izin, a.no_aju, CONVERT(VARCHAR(10), a.tgl_aju, 103) AS tgl_aju, a.no_izin, CONVERT(VARCHAR(10), a.tgl_izin, 103) as tgl_izin, CONVERT(VARCHAR(10), a.tgl_izin_exp, 103) AS tgl_izin_exp FROM t_siupmb a WHERE a.id = '".$id."' AND a.kd_izin = '".$doc."'";
                $ret = $this->main->get_result($query);
                if($ret){
					$this->ineng = $this->session->userdata('site_lang'); 
                    foreach ($query->result_array() as $row){
                        $arrdata['sess'] = $row;
                    }
					$arrdata['dir'] = $dir;
					$arrdata['doc'] = $doc;
                }
				if(!$this->session->userdata('site_lang')) $this->ineng = "id";
                $data = $this->load->view($this->ineng.'/backend/input/'.$dir.'/'.$stts, $arrdata, true);
            } else {
                $data = "";
            }
        }
        return $data;
    }

	function set_onfly($act, $id, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if($act == "update"){
				if(!$isajax){
					return false;
					exit();
				}
				$msg = "MSG||NO";
				$respon = FALSE;
				$arrsiup = $this->main->post_to_query($this->input->post('dataon'));
				$id = hashids_decrypt($id, _HASHIDS_, 9);
				$this->db->where('id',$id);
				$this->db->update('t_siupmb',$arrsiup);
				if($this->db->affected_rows() == 1){
					$respon = TRUE;
					$logu = array('aktifitas' => 'Melakukan update tanggal izin / tanggal masa berlaku',
								  'url' => '{c}'. site_url().'get/onfly/onfly_act/update'. ' {m} models/licensing/siupmb_act {f} set_onfly($act,  $id, $isajax)');
					$this->main->set_activity($logu);
				}
				if($respon) $msg = "MSG||YES";
				return $msg;
			}
		}
	}
	
	
	function referensi_kbli($target, $callback, $fieldcallback){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable;
			$query = "SELECT kode as kbli, uraian as desc_kbli, kode AS 'Kode KBLI', uraian AS 'Uraian KBLI' FROM m_kbli";
			$table->title("");
			$table->columns(array("kode","Kode KBLI", "uraian", "Uraian KBLI"));
			$this->newtable->width(array('Kode KBLI' => 100, 'Uraian' => 300, '&nbsp;' => 5));
			$this->newtable->search(array(array("kode","Kode KBLI"),
										  array("uraian","Uraian")));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."/licensing/popup_kbli/".$target.'/'.$callback.'/'.$fieldcallback);
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("kode"));
			$table->hiddens(array("kbli","desc_kbli"));
			$table->use_ajax(TRUE);
			$table->show_search(TRUE);
			$table->show_chk(FALSE);
			$table->single(TRUE);
			$table->dropdown(TRUE);
			$table->postmethod(TRUE);
			$table->settrid(TRUE);
			$table->attrid($target);
			if($callback!="")$table->callback(site_url(str_replace(".",'/',$callback)));
			if($fieldcallback!="") $table->fieldcallback($fieldcallback);
			$table->tbtarget("refkbli");
			$arrdata = array('tabel' => $table->generate($query));
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}

	function list_dok($target, $callback, $fieldcallback, $id, $multiple, $putin, $doc){
		if($this->newsession->userdata('_LOGGED')){
			$table = $this->newtable;
			$query = "SELECT id, id AS 'upload_id', nomor AS 'No. Dokumen', penerbit_dok AS 'Penerbit', tgl_dok AS 'Tgl. Dokumen', tgl_exp AS 'Tgl. Akhir', '<a href=\"".site_url()."download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS 'View'
			FROM t_upload WHERE tipe_dok = '".$id."' AND trader_id = '".$this->newsession->userdata('trader_id')."'";
			$table->title("");
			$table->columns(array("id", "nomor", "penerbit_dok", "tgl_dok", "tgl_exp", "<a href=\"".site_url()."download' + SUBSTRING(folder,10, (LEN(folder)-8)) + '/' + nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>"));
			$this->newtable->width(array('Nomor Dokumen' => 100, 'Penerbit' => 300, 'Tanggal Awal' => 100,'Tanggal Akhir' => 100,'&nbsp;' => 5));
			$this->newtable->search(array(array("nomor","Nomor Penerbit"),
										  array("penerbit_dok","Penerbit")));
			$table->cidb($this->db);
			$table->ciuri($this->uri->segment_array());
			$table->action(site_url()."/licensing/popup_syarat/".$id.'/'.$target.'/'.$callback.'/'.$fieldcallback.'/');
			$table->orderby(1);
			$table->sortby("ASC");
			$table->keys(array("id"));
			$table->hiddens(array("id","folder","nama_file","upload_id"));
			$table->show_search(TRUE);
			$table->single(TRUE);
			$table->dropdown(TRUE);
			$table->postmethod(TRUE);
			
			if((int)$multiple == 1){ 
				$table->show_chk(TRUE);
				$table->menu(array('Pilih Data' => array('POSTGET', site_url().'post/document/get_requirements/'.$doc.'/ajax', 'N', 'fa fa-cloud-download', 'isngajax', '#'.$putin)));
			}else{
				$table->show_chk(FALSE);
				if($callback!="")$table->callback(site_url(str_replace(".",'/',$callback)));
				if($fieldcallback!="") $table->fieldcallback($fieldcallback);
				$table->settrid(TRUE);
				$table->attrid($target);
			}
			$table->tbtarget("refreq_".rand());
			$arrdata = array('tabel' => $table->generate($query));
			if($this->input->post("data-post")) return $table->generate($query);
			else return $arrdata;
		}
	}
	
	function get_requirements($doc){
		$id = join("','", $this->input->post('tb_chk'));
		$data = "SELECT a.id AS dok_ids, b.id AS id_upload, b.nomor, b.penerbit_dok, b.tgl_dok, b.tgl_exp, '<a href=\"".site_url()."download' + SUBSTRING(b.folder,10, (LEN(b.folder)-8)) + '/' + b.nama_file + '\" target=\"_blank\"><i class=\"fa fa-download\"></i></a>' AS files, c.tipe FROM m_dok a LEFT JOIN t_upload B ON a.id = b.tipe_dok LEFT JOIN m_dok_izin c ON c.dok_id = a.id WHERE b.id IN('".$id."') AND c.izin_id = '".$doc."'";
		//print_r($data);die();
		return $this->db->query($data)->result_array();
	}
}
?>