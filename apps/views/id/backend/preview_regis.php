<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //print_r($bidang);die();?>
<script src="<?= base_url('assets/bend/js/a.js'); ?>"></script>
<script src="<?= base_url('assets/bend/js/sipt.js'); ?>"></script>
<div class="page-head">
  <h3 class="m-b-less"> Preview Permohonan</h3>
  <span class="sub-title">
  
  </span> </div>
<div class="wrapper">
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading"> Data Permohonan</header>
        <div class="panel-body">
          <section class="isolate-tabs">
            <ul class="nav nav-tabs">
              <li class="active"> <a data-toggle="tab" href="#first">Profile Perusahaan</a> </li>
              <li class=""> <a data-toggle="tab" href="#second">Penanggung Jawab Perusahaan</a> </li>
              <li class=""> <a data-toggle="tab" href="#third">Koordinator Pengguna SIPT</a> </li>
            </ul>
            <div class="panel-body"> 
              <div class="tab-content">
                <div id="first" class="tab-pane active">
                  <div class="row">
                    <div class="col-lg-12">
                      <section class="panel">
                        <header class="panel-heading"> <strong><b>Profile Perusahaan</b></strong></header>
                        <div class="panel-body">
                          <div class="row">
                            <label class="col-sm-3 control-label">Tipe Registrasi <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['registrasi']; ?>
                            </div>
                          </div>
                          <!-- <div class="row">
                            <label class="col-sm-3 control-label">Direktorat <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['direk']; ?>
                            </div>
                          </div> -->
                          <?php if ($DataTrader['tipe_registrasi'] == 01 || $DataTrader['tipe_registrasi'] == 02) { ?>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">NIB <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['nib']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['npwp']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
<!--                          <div class="row">
                            <label class="col-sm-3 control-label">Lampiran NPWP</label>
                            <div class="col-sm-9">
                              <?= $DataTrader['ViewNpwp']; ?>
                            </div>
                          </div>-->
                          <?php } ?>
                          <?php if ($DataTrader['tipe_registrasi'] == 01 || $DataTrader['tipe_registrasi'] == 04) { ?>
                          <!--<div style="height:5px;"></div>-->
                          <div class="row">
                            <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['bentuk']; ?>
                            </div>
                          </div>
                          <?php } ?>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['nm_perusahaan']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['almt_perusahaan']; ?>
                            </div>
                          </div>
                          <?php if ($DataTrader['tipe_registrasi'] == 01 || $DataTrader['tipe_registrasi'] == 02) { ?>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['prop']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['kab']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['kec']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Kelurahan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['kel']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Kode Pos</label>
                            <div class="col-sm-9">
                              <?= $DataTrader['kdpos']; ?>
                            </div>
                          </div>
                          <?php } ?> 
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Telepon</label>
                            <div class="col-sm-9">
                              <?= $DataTrader['telp']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
<!--                          <div class="row">
                            <label class="col-sm-3 control-label">Fax</label>
                            <div class="col-sm-9">
                              <?= $DataTrader['fax']; ?>
                            </div>
                          </div>-->
                          <?php if ($DataTrader['tipe_registrasi'] == 01 || $DataTrader['tipe_registrasi'] == 04) { ?>
                          <!-- <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Jenis Usaha</label>
                            <div class="col-sm-9">
                              <?= $DataTrader['jns_usaha']; ?>
                            </div>
                          </div> -->
                          <?php } ?>
                          <?php if ($DataTrader['tipe_registrasi'] == 01) { ?>
                          <!-- <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Alamat Pabrik <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['alamat_pabrik']; ?>
                            </div>
                          </div> -->
<!--                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Tanggal Terbit TDP</label>
                            <div class="col-sm-9">
                              <?= $DataTrader['tgl_tdp']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Tanggal Akhir TDP</label>
                            <div class="col-sm-9">
                              <?= $DataTrader['tgl_exp_tdp']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">No TDP</label>
                            <div class="col-sm-9">
                              <?= $DataTrader['no_tdp']; ?>
                            </div>
                          </div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Lampiran TDP</label>
                              <div class="col-sm-9">
                              <?= $DataTrader['ViewTDP']; ?>
                              </div>
                            </div>
                          <?php } ?>
                          
                        </div>-->
                      </section>
                    </div>
                  </div>
                </div>
                <div id="second" class="tab-pane">
                  <div class="row">
                    <div class="col-lg-12">
                      <section class="panel">
                        <header class="panel-heading"> <b>Penanggung Jawab Perusahaan</b></header>
                        <div class="panel-body">
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Nama Penanggung Jawab<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['na_pj']; ?>
                            </div>
                          </div>
<!--                          <div style="height:5px;"></div>-->
<!--                          <div class="row">
                            <label class="col-sm-3 control-label">Jenis Identitas<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['identitas_pj']; ?>
                            </div>
                          </div>-->
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Nomor Identitas<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['noidentitas_pj']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Jabatan Penanggung Jawab<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['jabatan_pj']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Alamat Penanggung Jawab <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['alamat_pj']; ?>
                            </div>
                          </div>
                          <?php //if ($DataTrader['identitas_pj'] == 'KTP') { ?>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Propinsi Penanggung Jawab <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['kdprop_pj']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Kabupaten / Kota Penanggung Jawab <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['kdkab_pj']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Kecamatan Penanggung Jawab <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['kdkec_pj']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Kelurahan Penanggung Jawab <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['kdkel_pj']; ?>
                            </div>
                          </div>
                          <?php //} ?>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Email Penanggung Jawab<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataTrader['email_pj']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Telepon Penanggung Jawab</label>
                            <div class="col-sm-2">
                              <?= $DataTrader['telp_pj']; ?>
                            </div>
                          </div>
                          <!-- <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Fax Penanggung Jawab</label>
                            <div class="col-sm-9">
                              <?= $DataTrader['fax_pj']; ?>
                            </div>
                          </div> -->
                          <div style="height:5px;"></div>
<!--                          <div class="row">
                            <label class="col-sm-3 control-label">Tempat Lahir</label>
                            <div class="col-sm-9">
                              <?= $DataTrader['tmpt_lahir_pj']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Tanggal Lahir</label>
                            <div class="col-sm-9">
                              <?= $DataTrader['tgl_lahir_pj']; ?>
                            </div>
                          </div>-->
<!--                          <div style="height:5px;"></div>
                          <div class="row">
                              <label class="col-sm-3 control-label">Lampiran KTP</label>
                              <div class="col-sm-9">
                              <?= $DataTrader['ViewKTP_PJ']; ?>
                              </div>
                            </div>-->
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
              <div id="third" class="tab-pane">
                  <div class="row">
                    <div class="col-lg-12">
                      <section class="panel">
                        <header class="panel-heading"> <b>Koordinator Pengguna SIPT</b></header>
                        <div class="panel-body">
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Nama Koordinator<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataUser['nama']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Alamat Koordinator<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataUser['alamat']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Email Koordinator<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataUser['email']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">No KTP Koordinator<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $DataUser['no_identitas']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Telepon Koordinator</label>
                            <div class="col-sm-2">
                              <?= $DataUser['telp']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
<!--                          <div class="row">
                            <label class="col-sm-3 control-label">Fax Koordinator</label>
                            <div class="col-sm-9">
                              <?= $DataUser['fax']; ?>
                            </div>
                          </div>-->
                          <div style="height:5px;"></div>
                          <?php if ($DataUser['file_kuasa'] != '') { ?>
                          <div class="row">
                            <label class="col-sm-3 control-label">File KTP</label>
                            <div class="col-sm-9">
                              <?= $DataUser['ViewKTP']; ?>
                            </div>
                          </div>
                          
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">File Surat Kuasa</label>
                            <div class="col-sm-9">
                              <?= $DataUser['ViewSK']; ?>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                      </section>
                    </div>
                  </div>
                </div>    
              </div>
            </div>
          </section>
        </div>
      </section>
    </div>
  </div>
  <div class="row">
    <form action="<?= $action['act']; ?>" method="post" id="fpreview" name="fpreview" autocomplete = "off" data-redirect="true">
    <input type="hidden" name="SET[id_trader]" value="<?= hashids_encrypt($DataTrader['id'], _HASHIDS_, 9); ?>">
    <input type="hidden" name="SET[id_user]" value="<?= hashids_encrypt($DataUser['id'], _HASHIDS_, 9); ?>">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Data Proses Permohonan </header>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">Tanggal Permohonan</label>
                <div class="col-sm-9">
                  <?= $DataTrader['created']; ?>
                </div>
              </div>
              <?php if ($DataTrader['status'] == '01' || $DataTrader['status'] == '02' || $DataTrader['status'] == '04') { ?>
                <div style="height:5px;"></div>
                  <div class="row">
                    <label class="col-sm-3 control-label">Tanggal Aktivasi</label>
                    <div class="col-sm-9">
                      <?= $DataTrader['tgl_aktifasi']; ?>
                    </div>
                  </div>
                <div style="height:5px;"></div>
                  <div class="row">
                    <label class="col-sm-3 control-label">Keterangan</label>
                    <div class="col-sm-9">
                      <?= $DataTrader['keterangan']; ?>
                    </div>
                  </div>
                <div style="height:5px;"></div>
                  <div class="row">
                    <label class="col-sm-3 control-label">User Aktivasi</label>
                    <div class="col-sm-9">
                      <?= $DataTrader['usr_aktivasi']; ?>
                    </div>
                  </div>
                  <?php } ?>
                  <div class="form-group">
                      <?php if ($DataTrader['status'] == '00' || $DataTrader['status'] == '03') {?>
                      <label for="catatan">Catatan</label>
                        <textarea class="form-control mb-10" name="SET[catatan]" wajib="yes" id="catatan"></textarea>
                    </div>
                    <?php }?>
              </div>
            </div>
                <button class="btn btn-sm btn-info addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-info pull-right"></i>Kembali</button>
                <?php if ($DataTrader['status'] == '01' || $DataTrader['status'] == '02' || $DataTrader['status'] == '04') { ?>
                  <button id="btn_setujui" class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fpreview'); return false;"><i class="fa fa-check pull-right"></i>Kirim Email</button>
                <?php } ?>
                <?php if ($DataTrader['status'] == '00' || $DataTrader['status'] == '03') {?>
                <button id="btn_setujui" class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fpreview'); return false;"><i class="fa fa-check pull-right"></i>Setujui</button>
                <button id="btn_tolak" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="coba('#fpreview'); return false;"><i class="fa fa-close pull-right"></i>Tolak</button>
                <?php }?>
          </div>
        </section>
      </div>
    </form>
  </div>
</div>


<script>
var site_url = '<?php echo site_url();?>';
var setujui = document.getElementById("checkbox-agreement").value;
$(document).ready(function(e){
  $('#btn_setujui').attr('disabled', true);
});

function coba(a){
  BootstrapDialog.confirm('Apakah anda yakin dengan data yang Anda isikan ?', function(r){
    if(r){
      $.ajax({
              type: "POST",
                url: site_url + 'licensing/tolak_regis' + '/ajax',
                data: $(a).serialize(),
                error: function() {
                  BootstrapDialog.show({
                    title: '',
                    type: BootstrapDialog.TYPE_DANGER,
                    message: '<p>Maaf, request halaman tidak ditemukan</p>'
                  });
                },
                beforeSend: function(){
                  if($("#progress").length === 0){
                    $("body").append($('<div id=\"progress\"></div><div class=\"overlays\"><div class="la-square-spin" style="margin-left:5px; margin-top: 10px; position: fixed; z-index: 19832509; color: #63c5ab;"><div>Loading ... </div></div></div>'));
                    $('#progress').width((50 + Math.random() * 30) + "%");
                    $('.overlays').css('width', $('body').css('width'));
                    $('.overlays').css('height', $('body').css('height'));
                  }
                },
                complete: function(){
                  $("#progress").width("101%").delay(200).fadeOut(400, function(){
                    $("#progress").remove();
                    $(".overlays").remove();
                  });
                },
                success: function(data) {
                  if (data.search("MSG") >= 0) {
                    arrdata = data.split('||');
                    if (arrdata[1] == "YES") {
                      $(".overlays").remove();
                      BootstrapDialog.show({
                        title: '',
                        type: BootstrapDialog.TYPE_SUCCESS,
                        message: '<p>'+arrdata[2]+'</p>'
                      });
                      if(arrdata.length > 3){
                        if(arrdata[3] == "REFRESH"){
                          setTimeout(function(){location.reload(true);}, 2000);
                        }else{
                           setTimeout(function(){
                             if($form.attr("data-redirect")){
                               location.href = arrdata[3];
                             }else{
                               //Belum didefinisikan.
                             }
                          }, 2000);
                        }
                      }
                    } else if (arrdata[1] == "NO") {
                      BootstrapDialog.show({
                        title: '',
                        type: BootstrapDialog.TYPE_DANGER,
                        message: '<p>'+arrdata[2]+'</p>'
                      });
                    }
                  }
                }
              });
    }else{
      return false;
    }
  });
}

function undisabled(cb){
  if (cb.checked == true) {
    $('#btn_setujui').attr('disabled', false);
  }else{
    $('#btn_setujui').attr('disabled', true);
  }
}
</script>