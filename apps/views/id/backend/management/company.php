<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="page-head">
  <h3 class="m-b-less"> Profil Perusahan</h3>
</div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fprofil" name="fprofil" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Perusahan</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id ="id" name="id" wajib="yes" value="<?=$sess['npwp']; ?>" />
            <?php if($this->newsession->userdata('role') == '05'):?>
            <div class="row">
              <label class="col-sm-3 control-label">NPWP</label>
              <div class="col-sm-9">
				<label><?=$sess['npwp'];?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <?php endif; ?>
            <div class="row">
              <label class="col-sm-3 control-label">Bentuk Perusahan</label>
              <div class="col-sm-9">
				<label><?=$sess['tipe_perusahaan'];?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nama Perusahan</label>
              <div class="col-sm-9">
				<label><?=$sess['nm_perusahaan'];?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Perusahan</label>
              <div class="col-sm-9">
				<label><?=$sess['almt_perusahaan'];?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kode Provinsi</label>
              <div class="col-sm-9">
				<label><?=$sess['kdprop'];?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kode Kabupaten</label>
              <div class="col-sm-9">
				<label><?=$sess['kdkab'];?></label>
			  </div>
            </div>
            <div style="height:5px;"></div>
            <?php //endif;?>
            <div class="row">
              <label class="col-sm-3 control-label">Kode Kecamatan</label>
              <div class="col-sm-9">
				<label><?=$sess['kdkec'];?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kode Pos</label>
              <div class="col-sm-9">
				<label><?=$sess['kdpos'];?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telephone</label>
              <div class="col-sm-9">
				<label><?=$sess['telp'];?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Fax</label>
              <div class="col-sm-9">
				<label><?=$sess['fax'];?></label>
			 </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Email</label>
              <div class="col-sm-9">
        <label><?=$sess['email'];?></label>
       </div>
            </div>
            <!-- <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jenis Usaha</label>
              <div class="col-sm-9">
				<label><?=$sess['jns_usaha'];?></label>
              </div>
            </div> -->
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nomor TDP</label>
              <div class="col-sm-9">
				<label><?=$sess['no_tdp'];?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tanggal TDP</label>
              <div class="col-sm-9">
				<label><?=$sess['tgl_tdp'];?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tanggal Akhir TDP</label>
              <div class="col-sm-9">
        <label><?=$sess['tgl_exp_tdp'];?></label>
              </div>
            </div>
          </div>
        </section>
		
		<section class="panel">
          <header class="panel-heading"> <b>Data Penanggung Jawab</b></header>
          <div class="panel-body">
            <?php if($this->newsession->userdata('role') == '05'):?>
            <div class="row">
              <label class="col-sm-3 control-label">Nama Penanggung Jawab</label>
              <div class="col-sm-9">
				<label><?=$sess['na_pj'];?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <?php endif; ?>
            <div class="row">
              <label class="col-sm-3 control-label">Jabatan</label>
              <div class="col-sm-9">
				<label><?=$sess['jabatan_pj'];?></label>
			  </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telephone</label>
              <div class="col-sm-9">
				<label><?=$sess['telp_pj'];?></label>
			 </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Fax</label>
              <div class="col-sm-9">
				<label><?=$sess['fax_pj'];?></label>
			  </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Email</label>
              <div class="col-sm-9">
				<label><?=$sess['email_pj'];?></label>
               
			 </div>
            </div>
            <!-- <div style="height:5px;"></div> -->
            <div style="height:25px;"></div> 
            <div class="row"> 
              <div class="col-sm-12"> 
                <button class="btn btn-sm btn-default addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-undo"></i>Kembali</button>

                  <a href="<?= site_url('setting/edit_per'); ?>"><button id="btn_setujui" class="btn btn-sm btn-success addon-btn m-b-10"><i class="fa fa-check pull-right"></i>Edit</button></a> 
                
              </div> 
            </div>
            <?php// endif;?>
          </div>
        </section>
		
		
      </div>
    </div>
  </form>
</div>