<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //print_r($bidang);die();?>
<script src="<?= base_url('assets/bend/js/a.js'); ?>"></script>
<script src="<?= base_url('assets/bend/js/sipt.js'); ?>"></script>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<script src="<?= base_url('assets/bend/js/jquery.validate.min.js'); ?>"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<div class="page-head">
  <h3 class="m-b-less"> Preview Permohonan</h3>
  <span class="sub-title">
  
  </span> </div>
<div class="wrapper">
  <div class="row">
    <div class="col-lg-12">
    <form role="form" data-redirect = "true" class="form-horizontal" name="fsupportdocnew" id="fsupportdocnew" enctype="multipart/form-data" method="post" action="<?= $act; ?>" autocomplete="off" data-agreement >
      <section class="panel">
        <header class="panel-heading"> Data Permohonan</header>
        <div class="panel-body">
          <section class="isolate-tabs">
            <ul class="nav nav-tabs">
              <li class="active"> <a data-toggle="tab" href="#first">Profile Perusahaan</a> </li>
              <li class=""> <a data-toggle="tab" href="#second">Penanggung Jawab Perushaan</a> </li>
            </ul>
            <div class="panel-body"> 
              <div class="tab-content">
                <div id="first" class="tab-pane active">
                  <div class="row">
                    <div class="col-lg-12">
                      <section class="panel">
                        <header class="panel-heading"> <strong><b>Profile Perusahaan</b></strong></header>
                        <div class="panel-body">
                          <div class="form-group" id="d_reg">
                        <label class=" control-label">Registrasi</label>
                        <?= form_dropdown('REG[tipe_registrasi]', $pendaftaran, '', 'id="tipe_reg" class="form-control select2" wajib="yes" onchange="cek_regis()"'); ?>
                    </div>
                        <div class="form-group" id="nib">
                            <label class=" control-label">NIB</label>
                            <input class="form-control" wajib="yes" id="nib1" name="REG[nib]" onkeyup="validate_npwp();" maxlength="13" size="3">
                        </div>
                    <div class="form-group" id="d_usaha">
                        <label class=" control-label">Bentuk Usaha</label>
                        <?= form_dropdown('REG[tipe_perusahaan]', $usaha, $sess['tipe_perusahaan'], 'id="tipe_perusahaan" wajib="yes" class="form-control select2" '); ?>
                    </div>
                    <div class="form-group">
                            <label class=" control-label" id="npwp_lbl">NPWP</label>
                            <input class="form-control" wajib="yes" readonly id="npwp" value="<?= $sess['npwp']; ?>">
                    </div>
                    <div id="perorangan">
                        <div class="form-group">
                            <label class=" control-label" id="nama_prorg">Nama</label>
                            <label class=" control-label" id="nama_prush">Nama Perusahaan</label>
                            <?php if ($sess['tipe_registrasi'] == '02') {
                                $ro = '';
                            }elseif ($sess['tipe_registrasi'] == '01' && $sess['tipe_perusahaan'] == '05') {
                                $ro = '';
                            }else{
                                $ro = 'readonly';
                            } ?>
                            <input class="form-control" wajib="yes" id="nama_perorangan" <?= $ro; ?> name="REG[nm_perusahaan]" value="<?= $sess['nm_perusahaan']; ?>">
                        </div>
                        <div class="form-group">
                            <label class=" control-label" id="alamat_prorg">Alamat</label>
                            <label class=" control-label" id="alamat_prush">Alamat Perusahaan</label>
                            <textarea class="form-control" wajib="yes" id="alamat_perorangan" name="REG[almt_perusahaan]"><?= $sess['almt_perusahaan']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label class=" control-label">Propinsi</label>
                            <?= form_dropdown('REG[kdprop]', $prop, $sess['kdprop_per'], 'id="prop_perorangan" wajib="yes" data-url="' . site_url() . 'get/cb/set_kota/"  class="form-control select2 " onChange = "combobox($(this), \'#kab_perorangan\'); return false;" '); ?>
                        </div>
                        <div class="form-group">
                            <label class=" control-label">Kabupaten / Kota</label>
                            <?= form_dropdown('REG[kdkab]', $kab_per, $sess['kdkab_per'], 'id="kab_perorangan" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kec/" onChange = "combobox($(this), \'#kec_perorangan\');"'); ?>
                        </div>
                        <div class="form-group">
                            <label class=" control-label">Kecamatan</label>
                            <?= form_dropdown('REG[kdkec]', $kec_per, $sess['kdkec_per'], 'id="kec_perorangan" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kel/" onChange = "combobox($(this), \'#kel_perorangan\');"'); ?>
                        </div>
                        <div class="form-group">
                            <label class=" control-label">Kelurahan</label>
                            <?= form_dropdown('REG[kdkel]', $kel_per, $sess['kdkel_per'], 'id="kel_perorangan" wajib="yes" class="form-control input-sm select2"'); ?>
                        </div>
                        <div class="form-group">
                            <label class=" control-label" id="kdpos_prorg">Kode Pos</label>
                            <label class=" control-label" id="kdpos_prush">Kode Pos Perusahaan</label>
                            <input class="form-control" id="kdpos_perorangan" wajib="yes" name="REG[kdpos]" value="<?= $sess['kdpos_per']; ?>">
                        </div>
                        <div class="form-group">
                            <label class=" control-label" id="tlp_prorg">Telepon</label>
                            <label class=" control-label" id="tlp_prush">Telepon Perusahaan</label>
                            <input class="form-control" id="telp_perorangan" wajib="yes" name="REG[telp]" value="<?= $sess['telp_per']; ?>">
                        </div>
<!--                        <div class="form-group">
                            <label class=" control-label" id="fax_prorg">Fax</label>
                            <label class=" control-label" id="fax_prush">Fax Perusahaan</label>
                            <input class="form-control" id="fax_perorangan" wajib="yes" name="REG[fax]" value="<?= $sess['fax_per']; ?>">
                        </div>
                        <div class="form-group">
                            <label class=" control-label" id="email_prorg">Email</label>
                            <label class=" control-label" id="email_prush">Email Perusahaan</label>
                            <input class="form-control" id="email_perorangan" name="REG[email]" value="<?= $sess['email']; ?>">
                        </div>-->
                    </div>

<!--                     <div id="perusahaan"> -->
<!--                        <div id="TDP">
                            <div class="form-group">
                                <label class=" control-label">No TDP</label>
                                <input class="form-control" id="no_tdp" wajib="yes" name="REG[no_tdp]" value="<?= $sess['no_tdp']; ?>">
                            </div>
                            <div class="form-group">
                                <label class=" control-label">Tanggal Terbit TDP</label>
                                <input type="text" class="form-control mb-10 datepickers" id="tgl_tdp" data-date-format = "yyyy-mm-dd" value="<?= $sess['tgl_tdp']; ?>" wajib="yes" name="REG[tgl_tdp]">
                            </div>
                            <div class="form-group">
                                <label class=" control-label">Tanggal Akhir TDP</label>
                                <input type="text" class="form-control mb-10 datepickers" id="tgl_exp_tdp" data-date-format = "yyyy-mm-dd" value="<?= $sess['tgl_exp_tdp']; ?>" wajib="yes" name="REG[tgl_exp_tdp]">
                            </div>
                            <div class="form-group">
                                <label class="control-label">File Lampiran TDP</label>
                                <input type="file" class="form-control" name="filetdp" wajib="yes" id="filetdp">
                                <font size ="1" color="red">Format file .jpg .png dan .pdf max 2 MB, Semua Dokumen Yang Diunggah Wajib Berwarna Berdasarkan Dokumen Asli</font>
                            </div>
                        </div>-->
<!--                     </div> -->
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
                <div id="second" class="tab-pane">
                  <div class="row">
                    <div class="col-lg-12">
                      <section class="panel">
                        <header class="panel-heading"> <b>Penanggung Jawab Perushaan</b></header>
                        <div class="panel-body">
                          <div class="form-group">
                        <label class=" control-label">Nama</label>
                        <input class="form-control" wajib="yes" id="nama_pj" name="REG[na_pj]" value="<?= $sess['na_pj']; ?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Jabatan</label>
                        <?php //form_dropdown('REG[jabatan_pj]', $jabatan, $sess['jabatan_pj'], 'id="jabatan_pj" wajib="yes" class="form-control select2" '); ?>
                        <input class="form-control" wajib="yes" id="jabatan_pj" name="REG[jabatan_pj]" value="<?= $sess['jabatan_pj']; ?>">
                    </div>
<!--                    <div class="form-group">
                        <label class=" control-label">Jenis Identitas</label>
                        <?= form_dropdown('REG[identitas_pj]', $identitas, $sess['identitas_pj'], 'id="identitas_pj" class="form-control select2" wajib="yes"'); ?>
                    </div>-->
                    <div class="form-group">
                        <label class=" control-label">No Identitas</label>
                        <input class="form-control" wajib="yes" id="no_identitas_pj" value="<?= $sess['noidentitas_pj']; ?>" name="REG[noidentitas_pj]">
                    </div> 
                    <div class="form-group">
                        <label class="control-label">File Lampiran Foto KTP Penanggung Jawab</label>
                        <input type="file" class="form-control" name="filektp_pj" wajib="yes" id="filektp_pj">
                        <font size ="1" color="red">Format file .jpg .png dan .pdf max 2 MB, Semua Dokumen Yang Diunggah Wajib Berwarna Berdasarkan Dokumen Asli</font>
                    </div>
                    <div class="form-group">
                        <label class=" control-label">Telepon</label>
                        <input class="form-control" wajib="yes" id="telp_pj" name="REG[telp_pj]" value="<?= $sess['telp_pj']; ?>">
                    </div>
                    <div class="form-group">
                        <label class=" control-label">Alamat</label>
                        <textarea class="form-control" wajib="yes" id="alamat_pj" name="REG[alamat_pj]"> <?= $sess['alamat_pj']; ?> </textarea>
                    </div>

                    <div class="form-group">
                        <label class=" control-label">Propinsi </label>
                        <?= form_dropdown('REG[kdprop_pj]', $prop, $sess['kdprop_pj'], 'id="prop_pj" readonly="true" wajib="yes" data-url="' . site_url() . 'get/cb/set_kota/"  class="form-control select2 " onChange = "combobox($(this), \'#kab_pj\'); return false;" '); ?>
                    </div>
                    <div class="form-group">
                        <label class=" control-label">Kabupaten / Kota </label>
                            <?= form_dropdown('REG[kdkab_pj]', $kab_pj, $sess['kdkab_pj'], 'id="kab_pj" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kec/" onChange = "combobox($(this), \'#kec_pj\');"'); ?>
                    </div>
                    <div class="form-group">
                        <label class=" control-label">Kecamatan </label>
                        <?= form_dropdown('REG[kdkec_pj]', $kec_pj, $sess['kdkec_pj'], 'id="kec_pj" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kel/" onChange = "combobox($(this), \'#kel_pj\');"'); ?>
                    </div>
                    <div class="form-group">
                        <label class=" control-label">Kelurahan </label>
                        <?= form_dropdown('REG[kdkel_pj]', $kel_pj, $sess['kdkel_pj'], 'id="kel_pj" wajib="yes" class="form-control input-sm select2"'); ?>
                    </div>

<!--                    <div class="form-group">
                        <label class=" control-label">Tempat Lahir</label>
                        <input class="form-control" wajib="yes" id="fax_pj" name="REG[tmpt_lahir_pj]" value="<?= $sess['tmpt_lahir_pj']; ?>">
                    </div>
                    <div class="form-group">
                        <label class=" control-label">Tanggal Lahir </label>
                        <input type="text" class="form-control mb-10 datepickers" id="d" data-date-format="yyyy-mm-dd" value="<?= $sess['tgl_lahir_pj']; ?>" name="REG[tgl_lahir_pj]" wajib="yes">
                    </div>-->
<!--                     <div class="form-group">
                        <label class=" control-label">Telp/HP </label>
                        <input class="form-control" wajib="yes" id="fax_pj" name="REG[ftelp_pj]" value="<?= $sess['telp_pj']; ?>">
                    </div> -->
                    <!-- <div class="form-group">
                        <label class=" control-label">Fax </label>
                        <input class="form-control" wajib="yes" id="fax_pj" name="REG[fax_pj]" value="<?= $sess['fax_pj']; ?>">
                    </div> -->
                    <div class="form-group">
                        <label class=" control-label">Email </label>
                        <input class="form-control" wajib="yes" id="email_pj" name="REG[email_pj]" value="<?= $sess['email_pj']; ?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">File Lampiran Surat Kuasa</label>
                        <input type="file" class="form-control" name="filesk" wajib="yes" id="filesk">
                    </div>
                </div>
                      </section>
                    </div>
                  </div>
                </div>    
              </div>
            </div>
          </section>
          </form>
        </div>
      </section>
    </div>
  </div>
  <div class="row">
    <form action="<?= $action['act']; ?>" method="post" id="fpreview" name="fpreview" autocomplete = "off" data-redirect="true">
    <input type="hidden" name="SET[id_trader]" value="<?= hashids_encrypt($DataTrader['id'], _HASHIDS_, 9); ?>">
    <input type="hidden" name="SET[id_user]" value="<?= hashids_encrypt($DataUser['id'], _HASHIDS_, 9); ?>">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Data Proses Permohonan </header>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <?php if ($DataTrader['status'] == '00') {?>
                      <label for="catatan">Catatan</label>
                        <textarea class="form-control mb-10" name="SET[catatan]" wajib="yes" id="catatan"></textarea>
                    </div>
                    <?php }?>
              </div>
            </div>
                <button class="btn btn-sm btn-info addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-info pull-right"></i>Kembali</button>
                <button id="btn_setujui" class="btn btn-sm btn-success addon-btn m-b-10" onclick="obj('#fsupportdocnew'); return false;"><i class="fa fa-check pull-right"></i>Update</button>
          </div>
        </section>
      </div>
    <input type="hidden" id="adds" value="<?= $addGets; ?>" />
     <input type="hidden" id="log" name="log" />
    </form>
  </div>
</div>


<script src="<?= base_url(); ?>assets/bend/js/select2.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js" type="text/javascript"></script>
<script type="text/javascript">
            function changeImage() {
                document.getElementById("img-keycode").src = "<?= base_url(); ?>keycode.php?rnd=" + Math.random();
            }
</script>
<script>
    $(document).ready(function (e) {
         $('#nib').hide();
        $('#tipe_perusahaan').attr('readonly', true);
                    $('#alamat_perorangan').attr('readonly', true);
                    $('#prop_perorangan').attr('readonly', true);
                    $('#kab_perorangan').attr('readonly', true);
                    $('#kdpos_perorangan').attr('readonly', true);
                    $('#nama_pj').attr('readonly', true);
                    $('#identitas_pj').attr('readonly', true);
                    $('#no_identitas_pj').attr('readonly', true);
                    $('#jabatan_pj').attr('readonly', true);
                    $('#telp_pj').attr('readonly', true);
                    $('#alamat_pj').attr('readonly', true);
                    $('#prop_pj').attr('readonly', true);
                    $('#kab_pj').attr('readonly', true);
                    $('#telp_perorangan').attr('readonly', true);
                    $('#email_pj').attr('readonly', true);
        
        if ($('#tipe_reg').val() == 01 || $('#tipe_reg').val() == 03) {
            $('#perusahaan').attr('hidden', false);
            $('#nama_prorg').attr('hidden', true);
            $('#alamat_prorg').attr('hidden', true);
            $('#kdpos_prorg').attr('hidden', true);
            $('#tlp_prorg').attr('hidden', true);
            $('#fax_prorg').attr('hidden', true);
            $('#email_prorg').attr('hidden', true);
        }else if($('#tipe_reg').val() == 02 || $('#tipe_reg').val() == 04){
            $('#perusahaan').attr('hidden', true);
            $('#nama_prush').attr('hidden', true);
            $('#alamat_prush').attr('hidden', true);
            $('#kdpos_prush').attr('hidden', true);
            $('#tlp_prush').attr('hidden', true);
            $('#fax_prush').attr('hidden', true);
            $('#email_prush').attr('hidden', true);
        }
        if ($('#tipe_reg').val() == '03' ||$('#tipe_reg').val() == '04') {
            $('#d_npwp').html('');
            $('#TDP').html('');
        }
        $(".datepickers").datepicker({
            autoclose: true
        });
    });

    function validate_username() {
        var site_url = '<?php echo site_url(); ?>';
        //console.log(no_lc);
        $.post(site_url + 'licensing/check_username', {data: $('#username_koor').val()}, function (data) {
            var cek = data;
            console.log(cek);
            return false;
            if (cek != 1)
            {
                $('#warn_username').fadeOut();
                $('#simpan').attr('disabled', false);
            } else
            {
                $('#warn_username').fadeIn();
                $('#warn_username').html('Username yang diinput sudah digunakan');
                $('#simpan').attr('disabled', true);
            }
        });
    }

    function cek_regis() {
        var regi = document.getElementById("tipe_reg").value;
        var prop = document.getElementById("prop_perusahaan");

        if (regi == "01") {
            $('#perusahaan').attr('hidden', false);
            $('#nib').show();
            $('#nama_prorg').attr('hidden', true);
            $('#alamat_prorg').attr('hidden', true);
            $('#kdpos_prorg').attr('hidden', true);
            $('#tlp_prorg').attr('hidden', true);
            $('#fax_prorg').attr('hidden', true);

            $('#nama_prush').attr('hidden', false);
            $('#alamat_prush').attr('hidden', false);
            $('#kdpos_prush').attr('hidden', false);
            $('#tlp_prush').attr('hidden', false);
            $('#fax_prush').attr('hidden', false);
            $('#d_npwp').attr('hidden', false);
            $('#flampiran').attr('hidden', false);
            $('#TDP').attr('hidden', false);
        }
        if (regi == "02") {
            $('#perusahaan').attr('hidden', true);
            $('#nama_prush').attr('hidden', true);
            $('#alamat_prush').attr('hidden', true);
            $('#kdpos_prush').attr('hidden', true);
            $('#tlp_prush').attr('hidden', true);
            $('#fax_prush').attr('hidden', true);

            $('#nama_prorg').attr('hidden', false);
            $('#alamat_prorg').attr('hidden', false);
            $('#kdpos_prorg').attr('hidden', false);
            $('#tlp_prorg').attr('hidden', false);
            $('#fax_prorg').attr('hidden', false);
            $('#d_npwp').attr('hidden', false);
            $('#flampiran').attr('hidden', false);
            //$('#alamat_pabrik').removeAttribute("wajib");
        }
        if (regi == "03") {
            $('#perusahaan').attr('hidden', false);
            $('#nama_prorg').attr('hidden', true);
            $('#alamat_prorg').attr('hidden', true);
            $('#kdpos_prorg').attr('hidden', true);
            $('#tlp_prorg').attr('hidden', true);
            $('#fax_prorg').attr('hidden', true);

            $('#nama_prush').attr('hidden', false);
            $('#alamat_prush').attr('hidden', false);
            $('#kdpos_prush').attr('hidden', false);
            $('#tlp_prush').attr('hidden', false);
            $('#fax_prush').attr('hidden', false);
            $('#d_npwp').html('');
            $('#TDP').html('');
        }
        if (regi == "04") {
            $('#perusahaan').attr('hidden', true);
            $('#nama_prush').attr('hidden', true);
            $('#alamat_prush').attr('hidden', true);
            $('#kdpos_prush').attr('hidden', true);
            $('#tlp_prush').attr('hidden', true);
            $('#fax_prush').attr('hidden', true);

            $('#nama_prorg').attr('hidden', false);
            $('#alamat_prorg').attr('hidden', false);
            $('#kdpos_prorg').attr('hidden', false);
            $('#tlp_prorg').attr('hidden', false);
            $('#fax_prorg').attr('hidden', false);
            $('#d_npwp').attr('hidden', true);
            $('#flampiran').attr('hidden', true);
            //$('#alamat_pabrik').removeAttribute("wajib");

            $('#d_npwp').html('');
            $('#TDP').html('');
        }
    }
    
    function validate_npwp() {                
        var site_url = '<?php echo site_url(); ?>';
        var nib1 = $('#nib1').val();        
        //var npwp = '020446449325000';
        // console.log(npwp);
        // return false;
        if (nib1.length == 13) {
            $.post(site_url + 'portal/validate_oss', {nib: nib1, adds: $('#adds').val()}, function (data) {
                // console.log(data);
                // return false;
                var res = data;
                if (res.stat != 1) {
                    BootstrapDialog.show({
                        title: '',
                        type: BootstrapDialog.TYPE_WARNING,
                        message: 'Nomor NPWP anda tidak terdaftar.'
                    });
                } else {
                    $('#log').val(res.log);
//                    console.log(nama_per);
                    var tipe_reg = $('#tipe_reg').val();
                    $('#nama_perorangan').val(res.res.nm_perusahaan);
                    $('#npwp').val(res.res.npwp);
                    $('#tipe_perusahaan').select2("val", res.res.tipe_perusahaan);
                    $('#prop_perorangan').select2("val",res.res.kdprop);
                    
                    $('#kab_perorangan').html('');
                    $.get("http://sipt.kemendag.go.id/training/get/cb/set_kota/" + res.res.kdprop, function($result){
			if($result){
                            $('#kab_perorangan').html($result);
                            $('#kab_perorangan').select2("val",res.res.kdkab);
			}
                    });
                     $('#kec_perorangan').html('');
                    $.get("http://sipt.kemendag.go.id/training/get/cb/set_kec/" + res.res.kdkab, function($result){
			if($result){
                            $('#kec_perorangan').html($result);
			}
                    });
                    $('#alamat_perorangan').val(res.res.almt_perusahaan);
                    $('#kdpos_perorangan').val(res.res.kdpos);
                    $('#telp_perorangan').val(res.res.telp);
                    $('#nama_pj').val(res.res.na_pj);
                    $('#identitas_pj').select2("val", res.res.identitas_pj);
                    $('#no_identitas_pj').val(res.res.noidentitas_pj);
                    $('#jabatan_pj').val(res.res.jabatan_pj);
                    $('#telp_pj').val(res.res.telp_pj);
                    $('#alamat_pj').val(res.res.alamat_pj);
                    $('#prop_pj').select2("val",res.res.kdprop_pj);
                    
                    $('#kab_pj').html('');
                    $.get("http://sipt.kemendag.go.id/training/get/cb/set_kota/" + res.res.kdprop_pj, function($result){
			if($result){
                            $('#kab_pj').html($result);
                            $('#kab_pj').select2("val",res.res.kdkab_pj);
			}
                    });
                    $('#kec_pj').html('');
                    $.get("http://sipt.kemendag.go.id/training/get/cb/set_kec/" + res.res.kdkab_pj, function($result){
			if($result){
                            $('#kec_pj').html($result);
			}
                    });
                    $('#email_pj').val(res.res.email_pj);
                    
                        $('#nama_perorangan').attr('readonly', true);
//                          $('#nama_perorangan').attr('readonly', false);
                    $('#tipe_perusahaan').attr('readonly', true);
                    $('#alamat_perorangan').attr('readonly', true);
                    $('#prop_perorangan').attr('readonly', true);
                    $('#kab_perorangan').attr('readonly', true);
                    $('#kdpos_perorangan').attr('readonly', true);
                    $('#nama_pj').attr('readonly', true);
                    $('#identitas_pj').attr('readonly', true);
                    $('#no_identitas_pj').attr('readonly', true);
                    $('#jabatan_pj').attr('readonly', true);
                    $('#telp_pj').attr('readonly', true);
                    $('#alamat_pj').attr('readonly', true);
                    $('#prop_pj').attr('readonly', true);
                    $('#kab_pj').attr('readonly', true);
                    $('#telp_perorangan').attr('readonly', true);
                    $('#email_pj').attr('readonly', true);
                    $('#kec_pj').select2("val", "");
                    $('#kel_pj').select2("val", "");
                    $('#kec_perorangan').select2("val", "");
                    $('#kel_perorangan').select2("val", "");
                    
                    $('#npwp').attr('readonly', true);
                }
            }, "JSON");
        }
    }
</script>