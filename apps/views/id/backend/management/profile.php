<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //print_r($dir);print_r($doc);die();?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Profil Pengguna</h3>
</div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fprofil" name="fprofil" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Pengguna</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id ="id" name="id" wajib="yes" value="<?= $sess['id']; ?>" />
            <?php if($this->newsession->userdata('role') == '05'):?>
            <div class="row">
              <label class="col-sm-3 control-label">NPWP</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="npwp" name="npwp" wajib="yes" <?= $readonly.' '.$disable ?> value="<?= $this->newsession->userdata('npwp');?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <?php endif; ?>
            <div class="row">
              <label class="col-sm-3 control-label">Username</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="username" readonly name="username" wajib="yes" <?= $readonly.' '.$disable ?> value="<?= ($sess['username']=="")?$this->newsession->userdata('username'):$sess['username'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label" for="password">Password</label>
              <div class="col-sm-9">
                <div class="form-group">
                  <div class="input-group m-b-10" id="<?= rand(); ?>"  data-name = "dataon[password]">
                    <input type="password" class="form-control mb-10" id ="password" name="password" readonly="readonly" value="" /><span class="input-group-addon"><input type="checkbox" class="chkedit" id="<?= rand(); ?>" /></span>
                 </div>
                 <p class="help-block">Ceklist jika ingin merubah password.</p>
                </div>
              </div>
            </div>
            <!-- <div style="height:5px;"></div> -->
            <div id="retypepwd" style="display:none">
              <div class="row">
                <label class="col-sm-3 control-label">Re-type Password</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control mb-10" id ="retypepwd" name="retypepwd" wajib="yes" value="" />
                </div>
              </div>
              <div style="height:5px;"></div>
            </div>
            <?php if($this->newsession->userdata('role') == '05'):?>
            <div id="oldpwd" style="display:none">
              <div class="row">
                <label class="col-sm-3 control-label">Password Lama / Sebelumnya</label>
                <div class="col-sm-9">
                  <input type="password" class="form-control mb-10" id ="oldpwd" name="oldpwd" wajib="yes" value="" />
                </div>
              </div>
              <div style="height:5px;"></div>
            </div>
            <?php endif;?>
            <div class="row">
              <label class="col-sm-3 control-label">Nama Lengkap Pengguna</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="nama" name="PROFIL[nama]" wajib="yes" <?= $readonly.' '.$disable ?> value="<?= ($sess['nama']=="")?$this->newsession->userdata('nama'):$sess['nama'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Pengguna</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="alamat" name="PROFIL[alamat]" wajib="yes" <?= $readonly.' '.$disable ?> value="<?= ($sess['alamat']=="")?$this->newsession->userdata('alamat'):$sess['alamat'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">E-mail Pengguna</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="email" name="PROFIL[email]" wajib="yes" <?= $readonly.' '.$disable ?> value="<?= ($sess['email']=="")?$this->newsession->userdata('email'):$sess['email'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <?php if($this->newsession->userdata('role') != '05'):?>
            <div class="row">
              <label class="col-sm-3 control-label">Jenis Identitas Pengguna</label>
              <div class="col-sm-9">
                <?= form_dropdown('PROFIL[tipe_identitas]', $jenis_identitas, $sess['tipe_identitas'], 'id="tipe_identitas" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <?php endif;?>
            <div class="row">
              <label class="col-sm-3 control-label">Nomor Identitas Pengguna</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="no_identitas" name="PROFIL[no_identitas]" wajib="yes" <?= $readonly.' '.$disable ?> value="<?= ($sess['no_identitas']=="")?$this->newsession->userdata('no_identitas'):$sess['no_identitas'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <?php if($this->newsession->userdata('role') != '05'):?>
            <div class="row">
              <label class="col-sm-3 control-label">Telepon Pengguna</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="telp" name="PROFIL[telp]" wajib="yes" <?= $readonly.' '.$disable ?> value="<?= ($sess['telp']=="")?$this->newsession->userdata('telp'):$sess['telp'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Fax Pengguna</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="fax" name="PROFIL[fax]" wajib="yes" <?= $readonly.' '.$disable ?> value="<?= ($sess['fax']=="")?$this->newsession->userdata('fax'):$sess['fax'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Role</label>
              <div class="col-sm-9">
                <?= $sess['role'];?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Direktorat</label>
              <div class="col-sm-9">
                <?php foreach ($dir as $key => $val) {
                  echo $val['uraian']."<br>";
                } ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Dokumen Perizinan</label>
              <div class="col-sm-9">
                <?php foreach ($doc as $key => $val) {
                  echo $val['nama_izin']."<br>";
                } ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Status</label>
              <div class="col-sm-9">
                <?= $sess['status'];?>
              </div>
            </div>
            <?php endif;?>
            <!-- <div style="height:5px;"></div> -->
            <div style="height:25px;"></div> 
            <div class="row"> 
              <div class="col-sm-12"> 
                <button class="btn btn-sm btn-default addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-undo"></i>Kembali</button>
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fprofil'); return false;"><i class="fa fa-check"></i>Simpan</button>
                </div> 
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script>
$(document).ready(function(e){ 
  $(".chkedit").change(function(e){
    var $this = $(this);
    var $div = $this.parent().parent(); 
    var $input = $this.parent().parent().children().first();
    var $before = $input.val();
    if($this.is(":checked")){
      $input.removeAttr("readonly");
      $input.attr("wajib","yes");
      $('#retypepwd').show();
      $('#oldpwd').show();
      $('#email').removeAttr("readonly");
    }else{
      $input.attr("readonly","readonly");
      $input.removeAttr("wajib");
      $('#retypepwd').hide();
      $('#oldpwd').hide();
    }
    return false;
  });
});
</script>