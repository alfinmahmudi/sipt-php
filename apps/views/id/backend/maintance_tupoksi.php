<?php if ( ! defined( 'BASEPATH')) exit( 'No direct script access allowed'); ?>
    <div id="main-wrapper">
        <form id="ftembusan" name="ftembusan" data-redirect="true" action="<?= $act; ?>" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <div class="panel-title">Pengaturan Tupoksi
                                <br>
                                <br>
                                <?= $nama_izin['nama_izin'];?>
                            </div>
                            <div class="panel-control">&nbsp;</div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th class="col-md-2">Role</th>
                                                <th class="col-md-2">Jenis</th>
                                                <th class="col-md-4">Narasi Tupoksi</th>
                                            </tr>
                                        </thead>
                                        <tbody class="bodyta">
                                            <tr id="trta_<?= rand(); ?>">
                                                <td>
                                                    <p><?= $sess[0]['role']; ?></p>
                                                </td>
                                                <td>
                                                    <?php if ($sess[0]['jenis'] != '1') { ?>
                                                        <p>Check Box</p>
                                                    <?php } else { ?>
                                                        <p>Modal Warning</p>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <input type="hidden" name="sla[id][]" value="<?= $sess[0]['id']; ?>">
                                                    <textarea type="text" class="form-control input-sm" name="sla[sla_waktu][]"> <?= $sess[0]['telaah']; ?> </textarea>
                                                </td>
                                            </tr>
                                            <?php if(count($sess)>1): for($i = 1; $i < count($sess); $i++):?>
                                                <tr id="trta_<?= rand(); ?>">
                                                    <td>
                                                    <p><?= $sess[$i]['role']; ?></p>
                                                    </td>
                                                    <td>
                                                        <?php if ($sess[$i]['jenis'] == '1') { ?>
                                                            <p>Check Box</p>
                                                        <?php } else { ?>
                                                            <p>Modal Warning</p>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="sla[id][]" value="<?= $sess[$i]['id']; ?>">
                                                        <textarea type="text" class="form-control input-sm" name="sla[sla_waktu][]"> <?= $sess[$i]['telaah']; ?> </textarea>
                                                    </td>
                                                </tr>
                                                <?php endfor; endif;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <button type="button" class="btn btn-default btn-addon m-b-sm btn-sm" onclick="closedialog();"><i class="fa fa-undo"></i> Batal</button>
                            <button type="button" class="btn btn-info btn-addon m-b-sm btn-sm" onclick="post('#ftembusan'); return false;"><i class="fa fa-check-square-o"></i> Simpan</button>
                            <!-- <button class="btn btn-sm btn-success addon-btn m-b-10" ><i class="fa fa-arrow-right"></i>Simpan</button> -->
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script src="<?= base_url(); ?>assets/bend/js/sipt.js?v=<?= date(" Ymdhis "); ?>"></script>