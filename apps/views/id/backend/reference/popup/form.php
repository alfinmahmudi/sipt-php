<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
    <div class="row">
        <form id="fnewusers" name="fnewusers" action="<?=$act; ?>" method="post" autocomplete="off">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"> Add Popup News </header>
                    <div class="panel-body">
                        <div class="row">
                            <label class="col-sm-3 control-label">Konten Berita</label>
                            <div class="col-sm-9">
                                <textarea type="text" name="pel" class="form-control mb-10 users" wajib="yes" id="isi" />
                                <?= $sess['isi']; ?>
                                </textarea>
                                <input type="hidden" value="<?=$sess['id']; ?>" name="id" /> 
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Add URL</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control mb-10 users" id="url" />
                            </div>
                            <label class="col-sm-3 control-label">Add Text Url</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control mb-10 users" id="urlkata" />
                            </div>
                            <div class="col-sm-1">
                                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="add_url(); return false;"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Status Berita</label>
                            <div class="col-sm-9">
                                <label class="checkbox-custom inline check-success">
                                    <input value="1" id="cb-dalam" type="checkbox" name="stat" check-agreement="yes">
                                    <label for="cb-dalam">
                                        <p>Aktif</p>
                                    </label>
                                </label>
                                <label class="checkbox-custom inline check-success">
                                    <input value="0" id="cb-luar" type="checkbox" name="stat" check-agreement="yes">
                                    <label for="cb-luar">
                                        <p>Tidak Aktif</p>
                                    </label>
                                </label>
                            </div>
                        <div style="height:25px;"></div>
                        <div class="row">
                            <div class="col-sm-11">
                                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewusers'); return false;"><i class="fa fa-check"></i>Simpan</button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>
    <script>
        $(document).ready(function(){
            $(function() {
                $("input:checkbox").on('click', function() {
                  // in the handler, 'this' refers to the box clicked on
                  var $box = $(this);
                  if ($box.is(":checked")) {
                    // the name of the box is retrieved using the .attr() method
                    // as it is assumed and expected to be immutable
                    var group = "input:checkbox[name='" + $box.attr("name") + "']";
                    // the checked state of the group/box on the other hand will change
                    // and the current value is retrieved using .prop() method
                    $(group).prop("checked", false);
                    $box.prop("checked", true);
                  } else {
                    $box.prop("checked", false);
                  }
                });
            });

        var sescek = "<?= $sess['aktif'] ?>";
        // console.log(sescek);return false;
        if (sescek == '1') {
            $('#cb-dalam').attr("checked", "yes");
        }else if (sescek == '0') {
            $('#cb-luar').attr("checked", "yes");
        }

    });
        function closedialog() {
            BootstrapDialog.closeAll();
            return false;
        };

        function add_url(){
            var isi = $('#isi').val();
            var urlkata = $('#urlkata').val();
            var url = $('#url').val();
            var href = '<a href="'+url+'>'+urlkata+'</a>"';
            var temp = isi+href;
            // console.log(temp);
            // return false;

            $('#isi').val(temp);
            $('#url').val('');
            $('#urlkata').val('');
        }
    </script>