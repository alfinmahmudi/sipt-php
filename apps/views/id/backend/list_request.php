<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-dialog/bootstrap-dialog.min.css" type="text/css" cache="false" />
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-dialog/bootstrap-dialog.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtable.js?v=<?= date("YmdHis"); ?>" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtablehash.js?v=<?= date("YmdHis"); ?>" type="text/javascript"></script>
<div class="page-head">
  <h3 class="m-b-less">
    <?= $judul; ?>
  </h3>
</div>
<div class="wrapper">
  <div class="row">
    <div class="row">
      <div class="col-lg-2" align="center">
          <button class="panel-heading" type="button" id="btn_prof" style="background: none; border: none;"> Baru </button>
      </div>
      <div align="center" class="col-lg-2">
          <button class="panel-heading" type="button" id="btn_prof" style="background: none; border: none;"> Terima </button>
      </div>
      <div align="center" class="col-lg-2">
          <button class="panel-heading" type="button" id="btn_prof" style="background: none; border: none;"> Tolak </button>
      </div>
      <div class="col-lg-3" align="center">
          <button class="panel-heading" type="button" id="btn_pj" style="background: none; border: none;"> Pembaharuan </button>
      </div>
      <div align="center" class="col-lg-3">
          <button class="panel-heading" type="button" id="btn_prof" style="background: none; border: none;"> Tidak Valid </button>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-2" align="center">
          <?= $result['baru']; ?>
      </div>
      <div align="center" class="col-lg-2">
          <?= $result['terima']; ?>
      </div>
      <div align="center" class="col-lg-2">
          <?= $result['tolak']; ?>
      </div>
      <div class="col-lg-3" align="center">
          <?= $result['pembaharuan']; ?>
      </div>
      <div align="center" class="col-lg-3">
          <?= $result['tdk_val']; ?>
      </div>
    </div>
    <div class="col-lg-12">
      <section class="panel">
      <?= $tabel; ?>
      </section>
    </div>
  </div>
</div>
