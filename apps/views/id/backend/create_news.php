<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/summernote.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/bootstrap-wysihtml5.css" type="text/css" cache="false" />

<script src="<?= base_url('assets/bend/js/a.js'); ?>"></script>
<div class="page-head">
  <h3 class="m-b-less"> Form Penambahan Berita</h3>
</div>
<div class="wrapper">
  <form action="<?php echo $act;?>" method="post" id="fsupportdocnew" name="fsupportdocnew" autocomplete = "off" data-redirect="true" enctype="multipart/form-data">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Penambahan</b></header>
          <div class="panel-body">
            <div class="row">
              <label class="col-sm-3 control-label">Judul Berita</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" value="<?php echo $data_news['judul'];?>" name="NEWS[title]" wajib="yes" width="100%">
                <input type="hidden" name="id_news" value="<?php echo $data_news['id']; ?>">
              </div>
            </div>


            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Konten Berita</label>
              <div class="col-sm-9">
                <textarea class="wysihtml5 form-control" name="NEWS[contents]" wajib="yes" width="100%" rows="9"> <?php echo $data_news['isi'];?></textarea>
              </div>
            </div>


            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tanggal Penampilan Berita</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 datepickers" value="<?php echo $data_news['tanggal'];?>" data-date-format = "yyyy-mm-dd" name="NEWS[dateshow]">
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Status Berita</label>
              <div class="col-sm-9">
                <?= form_dropdown('NEWS[status]', $status, $data_news['status'], 'id="VALUTA_DTL" wajibk="yes" class="form-control select2"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <input type="hidden" value="1" id="cek" name="cek[]">
              <label class="col-sm-3 control-label">Gambar Yang Di Tampilan</label>
              <div class="col-sm-8">
                <input type="file" style="margin-bottom: 4px;" class="form-control mb-10" name="gambar[]">
              </div>
              <label class="col-sm-3 control-label">Judul Gambar</label>
              <div class="col-sm-8">
                <input type="text" class="form-control mb-10" name="judul[]">
              </div>
              <div class="col-sm-1">
                <button class="fa fa-plus" type="button" onclick="add_upload()"></button>
              </div>
            </div>
            <div class="row" id="masuk">
              
            </div>
            <?php if ($banyak != null) {?>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Gambar Yang di Upload</label>
              <table class="table table-striped custom-table table-hover">
              <tbody align="center">
                <tr>
                <td >No</td>
                <td >Judul Gambar</td>
                <td colspan="2">File</td>
                <td >Action</td>
                </tr><?php $a = 1;?>
                <?php for ($i=0; $i < $banyak; $i++) { ?> 
                <tr>
                  <input type="hidden" id="id_detil" name="id_detil[]" value="<?= $data_detil[$i]['id'];?>">
                  <td ><?= $a ?></td>
                  <td ><input type="text" class="form-control" name="judul_gambar[]" id="DETIL[<?= $data_detil[$i]['id'];?>][title]" value="<?= $data_detil[$i]['title']; ?>"></td>
                  <td><input type="file" class="form-control mb-10" id="i" name="gambar_detil[]"></td>
                  <td ><a href="<?php echo base_url().substr($data_detil[$i]['files'], 2); ?>" target=blank><i class="fa fa-download"></i></a></td>
                  <td >
                  <button type="button" onclick="delete_detil('<?php echo $data_detil[$i]['id'];?>')">Delete</button>
                  </td>
                  <?php $a++;?>
                </tr>
                <?php } ?>
              </tbody>
              </table>
            </div>
            <?php }?>
          </div>
        </section>
      </div>
    </div>
    <span class="pull-right"><button id="simpan" type="submit" class="btn btn-sm btn-success addon-btn m-b-10" onclick="obj('#fsupportdocnew'); return false;"><i class="fa fa-arrow-right"></i>Simpan</button></span>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/jquery.browser.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/wysihtml5-0.3.0.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-wysihtml5.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/summernote.min.js"></script>
<script>
var site_url = '<?php echo site_url();?>';
$(document).ready(function(e){ 
  $('.wysihtml5').wysihtml5();
  $(".datepickers").datepicker({
    autoclose: true
  });
});

function add_upload(){
  var a = document.getElementById("cek").value;
  var a =  parseInt(a)+1;
  $( "#masuk" ).append("<div class='row cek_"+a+"'>");
  $( "#masuk" ).append("<label class='col-sm-3 control-label cek_"+a+"'>&nbsp;</label>");
  $( "#masuk" ).append("<div class='col-sm-8 cek_"+a+"'><input style=' margin-top: 4px;' type='file' name='gambar[]' class='form-control mb-10 cek_"+a+"'></div>");
  $( "#masuk" ).append("<label class='col-sm-3 cek_"+a+"' control-label'>&nbsp;</label>");
  $( "#masuk" ).append("<div class='col-sm-8'><input type='text' style=' margin-top: 6px;' name='judul[]' class='form-control mb-10 cek_"+a+"'></div>");
  $( "#masuk" ).append("<div class='col-sm-1'><button class='fa fa-minus cek_"+a+"' type='button' onclick='remove_upload("+a+")' ></button></div>");
  $( "#masuk" ).append("</div");
  $("#cek").val(a);
  return false;
}

function remove_upload(id){
  var b = document.getElementById("cek").value;
  var b =  parseInt(b)-1;
  $(".cek_"+id).remove();
  $("#cek").val(b);
}

function delete_detil(id){
  $.post(site_url+'news/delete_detil',{data:id},function(data){
    location.reload();
  });
}

function update_detil(id, a){
	var judul = document.getElementById(a).value;
	var i = $("#i").val();
	console.log(i);
  $.post(site_url+'news/update_detil',{data:id, title:judul},function(data, title){
    return false;
    //location.reload();
  });
}
</script>