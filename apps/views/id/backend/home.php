<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //print_r($_SESSION);die();  ?>
{_header_}
<title>{_appname_}</title>
<!-- </head><body class="sticky-header sidebar-collapsed"> !-->
</head><body class="sticky-header">
<style>
    .introjs-helperLayer{
        background-color: transparent;
        border: 1px solid #03a9f4;
    }
</style>
<?php $pelaku_usaha = '05';
$petugas = array('01', '07', '06', '02');
$verif = array('99', '09'); ?>
    <section>
        <div class="sidebar-left">
            <div class="logo dark-logo-bg visible-xs-* visible-sm-*"> <a href="<?= base_url(); ?>"> <img src="<?= base_url(); ?>assets/bend/img/logo-icon.png" alt=""> <span class="brand-name">SIPT PDN</span> </a> </div>
            <div class="sidebar-left-info">
                <div class=" search-field"> </div>
                <ul class="nav nav-pills nav-stacked side-navigation">
                    <li>
                        <h3 class="navigation-title">Navigasi</h3>
                    </li>
                    <?php
                    if ($this->newsession->userdata('role') == "99"):
                        // $_navmenu_['0306']['2'] = "fa  fa-files-o";
                        // $_navmenu_['02']['2'] = "fa  fa-institution ";
                        // $_navmenu_['02']['1'] = "Perusahaan SPPAGKR";
                        // $_navmenu_['02']['4'] = "reference/view/npwp";
                    endif;
                    //	print_r($_navmenu_[77]);die();
                    $terakhir = 1;
                    $f = false;
                    foreach ($_navmenu_ as $a => $b) {
                        if (strlen($a) == 2) {
                            if ($terakhir > 1)
                                if (!$f)
                                    echo "</ul></li>";
                            $terakhir = $b[0];
                            if ($b[0] > 1) {
                                if (implode($_SESSION['dir_id']) != "01" && $a == "77") {
                                    $f = true;
                                    continue;
                                }
                                if ($a == '88') {
                                    $step = '9';
                                    $intro = 'Menu Pengaturan Untuk Menampilkan dan Mengubah Data Perusahaan Dan Menampilkan dan Mengupload Dokumen Pendukung';
                                }
                                ?>
                                <li class="menu-list" data-step="<?= $step; ?>" data-intro="<?= $intro; ?>"><a href="<?= $b[4]; ?>"><i class="<?= $b[2]; ?>"></i><span><?= $b[1]; ?></span></a>
                                    <ul class="child-list">
                                        <?php
                                        //	}	
                                    } else {
                                        $step = '';
                                        $intro = '';
                                        if ($a == '00') {
                                            $step = '5';
                                            $intro = 'Menu Beranda Untuk Menampilkan Halaman Awal';
                                        }elseif ($a == '01') {
                                            $step = '6';
                                            $intro = 'Menu Persyaratan Untuk Menampilkan Persyaratan Permohonan Dan Upload Dokumen Pendukung';
                                        }elseif ($a == '02') {
                                            $step = '7';
                                            $intro = 'Menu Permohonan Untuk Mencari Permohonan Yang Sudah Di Buat';
                                        }elseif ($a == '04') {
                                            $step = '8';
                                            $intro = 'Menu Pelaporan Untuk Melaporkan Realisasi Atau Pelaporan';
                                        }elseif ($a == '08') {
                                            $step = '10';
                                            $intro = 'Menu Rekap TDPUD';
                                        }elseif ($a == '99') {
                                            $step = '11';
                                            $intro = 'Menu Keluar Untuk Keluar Dari Aplikasi SIPT';
                                        }
                                        ?>
                                        <li data-step="<?= $step; ?>" data-intro="<?= $intro; ?>"><a href="<?= $b[4]; ?>"><i class="<?= $b[2]; ?>"></i><span><?= $b[1]; ?></span></a></li>
                                        <?php
                                    }
                                } else if (strlen($a) == 4) {
                                    if ($this->newsession->userdata('role') == "99" || $this->newsession->userdata('role') == "09") {
                                        ?>
                                        <li><a href="<?= $b[4]; ?>"><i class="<?= $b[2]; ?>"></i><span><?= $b[1]; ?></span></a></li>
                                        <?php
                                        if ($a == 8802) {
                                            echo "</ul></li>";
                                        }
                                        if ($a == '0803') {
                                            echo "</ul></li>";
                                        }
                                    } else {

                                        if (implode($_SESSION['dir_id']) != '01' && $a == 7711) {
                                            continue;
                                        }
                                        ?>	
                                        <li><a href="<?= $b[4]; ?>"><?= $b[1]; ?></a></li>
                                        <?php
                                        if ($a == '0803') {
                                            echo "</ul></li>";
                                        }
                                    }
                                }
                            }
                            //	die();
                            ?>
                        </ul>
                        </div>
                        </div>
                        <div class="body-content" >
                            <div class="header-section">
                                <div class="logo dark-logo-bg hidden-xs hidden-sm"> <a href="<?= base_url(); ?>"> <img src="<?= base_url(); ?>assets/bend/img/logo-icon.png" alt=""> <span class="brand-name">SIPT PDN</span> </a> </div>
                                <div class="icon-logo dark-logo-bg hidden-xs hidden-sm"> <a href="<?= base_url(); ?>"> <img src="<?= base_url(); ?>assets/bend/img/logo-icon.png" alt=""> </a> </div>
                                <a class="toggle-btn"><i class="fa fa-outdent"></i></a>
                                <div class="notification-wrap">
                                        <div class="left-notification">
                                            <ul class="notification-menu">
                                                <?php if (in_array($this->newsession->userdata('role'), $petugas) || in_array($this->newsession->userdata('role'), $verif)) { ?>
                                                <li>
                                                    <a href="javascript:;" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                                                        <i class="fa fa-bell-o"></i>
                                                        <span class="badge bg-warning" id="jml"></span>
                                                    </a>

                                                    <div class="dropdown-menu dropdown-title ">

                                                        <div class="title-row">
                                                            <h5 class="title yellow" id="banyak">
                                                                
                                                            </h5>
                                                            <?php if (in_array($this->newsession->userdata('role'), $petugas)) {
                                                                $url_all = site_url().'licensing/view/inbox';
                                                            }elseif (in_array($this->newsession->userdata('role'), $verif)) {
                                                                $url_all = site_url().'licensing/view/inbox';
                                                            } ?>
                                                            <a href="<?= $url_all; ?>" class="btn-warning btn-view-all">View all</a>
                                                        </div>
                                                        <div class="notification-list-scroll sidebar">
                                                            <div class="notification-list mail-list not-list" id="res_notif">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>

                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <div class="right-notification">
                                        <ul class="notification-menu">
                                            <li> <a data-step="1" data-intro="Dropdown Menu Untuk Menampilkan Dan Mengubah Profile Koordinator Pengguna Aplikasi SIPT" href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <img src="<?= base_url(); ?>assets/bend/img/avatar-mini.jpg" alt=""><?= $this->newsession->userdata('nama'); ?><span class=" fa fa-angle-down"></span> </a>
                                                <ul class="dropdown-menu dropdown-usermenu purple pull-right">
                                                    <li><a href="<?= site_url('setting/view/profil'); ?>"> Profile</a></li>
                                                    <li><a href="<?= site_url('logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Keluar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {_content_}
                            <footer> <?=date('Y')?> &copy; Kementerian Perdagangan Republik Indonesia</footer>
                        </div>
                        </section>
                        <script src="<?= base_url(); ?>assets/bend/js/modernizr.min.js"></script> 
                        <script src="<?= base_url(); ?>assets/bend/js/jquery.nicescroll.js?v=<?= date("Ymdhis"); ?>" type="text/javascript"></script> 
                        <script src="<?= base_url(); ?>assets/bend/js/scripts.js?rnd=10"></script>
                        <script src="<?= base_url(); ?>assets/bend/js/sipt.js?v=<?= date("Ymdhis"); ?>"></script>
                        <script >
                            
                            $(document).ready(function () {
                                $('.jqte-catatan').jqte();
                            });
                            var temp = '';
                            var flag = 0;
                            var site_url = '<?php echo site_url(); ?>';
                            if (<?= $this->newsession->userdata('role'); ?> != '05' && <?= $this->newsession->userdata('role'); ?> != '09' && <?= $this->newsession->userdata('role'); ?> != '99') {
                                $.post(site_url + 'licensing/check_notif', function (data) {
                                    $('#banyak').html('Anda Memiliki ' +data.jml+ ' Permohonan <br> Baru');
                                    $('#jml').html(data.jml);
                                    $.each(data.data, function(index, value){
                                        temp += '<a href="'+site_url+value.path+'" class="single-mail">';
                                        temp += '<p><small>'+value.aka+'</small></p>';
                                        temp += '<span class="icon bg-primary">';
                                        temp += '<i class="fa fa-envelope-o"></i>';
                                        temp += '</span>';
                                        temp += '<strong>'+value.nm_perusahaan+'</strong>';
                                        temp += '<p style="min-width: 190%;"><small>'+value.waktu+'</small></p></a>';
                                    });
                                    $('#res_notif').html(temp);
                                }, 'json');
                            }else if (<?= $this->newsession->userdata('role'); ?> == '09' || <?= $this->newsession->userdata('role'); ?> == '99') {
                                $.post(site_url + 'licensing/check_regis', function (data) {
                                    $('#banyak').html('Anda Memiliki ' +data.jml+ ' Permohonan <br> Baru');
                                    $('#jml').html(data.jml);
                                    $.each(data.data, function(index, value){
                                        temp += '<a href="'+site_url+'licensing/preview_regis/'+data.id[flag]+'" class="single-mail">';
                                        temp += '<p><small>'+value.tipe+'</small></p>';
                                        temp += '<span class="icon bg-primary">';
                                        temp += '<i class="fa fa-envelope-o"></i>';
                                        temp += '</span>';
                                        temp += '<strong>'+value.nm_perusahaan+'</strong>';
                                        temp += '<p style="min-width: 190%;"><small>'+value.waktu+'</small></p></a>';
                                        flag++;
                                    });
                                    $('#res_notif').html(temp);
                                }, 'json');
                            }
                        </script>
                        </body>
                        </html>