<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //print_r($_SESSION);die();  ?>
{_header_}
<title>{_appname_}</title>
<!-- </head><body class="sticky-header sidebar-collapsed"> !-->
</head><body class="sticky-header">
    <section>
        <!-- sidebar left start-->
        <div class="sidebar-left">
            <!--responsive view logo start-->
            <div class="logo dark-logo-bg visible-xs-* visible-sm-*">
                <a href="index.html">
                    <img src="img/logo-icon.png" alt="">
                    <!--<i class="fa fa-maxcdn"></i>-->
                    <span class="brand-name">SlickLab</span>
                </a>
            </div>
            <!--responsive view logo end-->

            <div class="sidebar-left-info">
                <!-- visible small devices start-->
                <div class=" search-field">  </div>
                <!-- visible small devices end-->

                <!--sidebar nav start-->
                <ul class="nav nav-pills nav-stacked side-navigation">
                    <li>
                        <h3 class="navigation-title">Navigation</h3>
                    </li>
                    <li ><a href="<?= site_url(); ?>dashboard/"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                    <li ><a href="<?= site_url(); ?>dashboard/famous"><i class="fa fa-thumbs-up"></i> <span>Favorite</span></a></li>
                </ul>
                <!--sidebar nav end-->

            </div>
        </div>
        <!-- sidebar left end-->
                        <div class="body-content" >
                            <div class="header-section">
                                <div class="logo dark-logo-bg hidden-xs hidden-sm"> <a href="<?= base_url(); ?>"> <img src="<?= base_url(); ?>assets/bend/img/logo-icon.png" alt=""> <span class="brand-name">SIPT PDN</span> </a> </div>
                                <div class="icon-logo dark-logo-bg hidden-xs hidden-sm"> <a href="<?= base_url(); ?>"> <img src="<?= base_url(); ?>assets/bend/img/logo-icon.png" alt=""> </a> </div>
                                <a class="toggle-btn"><i class="fa fa-outdent"></i></a>
                                <div class="notification-wrap"><?php //print_r($_notif_);die($_notif_['total']);  ?>
                                    <?php if (($this->newsession->userdata('role') != '05') && ($this->newsession->userdata('role') != '99')): ?>
                                        <div class="left-notification">
                                            <ul class="notification-menu">
                                                <li>
                                                    <!--a href="javascript:;" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                                                        <i class="fa fa-bell-o"></i>
                                                        <span class="badge bg-danger"><?= $_notif_['total']; ?></span>
                                                    </a-->
                                                    <div class="dropdown-menu dropdown-title">
                                                        <div class="title-row">
                                                            <h5 class="title yellow">Notifikasi</h5>
                                                        </div>
                                                        <div class="notification-list-scroll sidebar">
                                                            <div class="notification-list mail-list not-list">
                                                                <?php
                                                                $jml = count($_notif_['notif']);
                                                                for ($i = 0; $i < $jml; $i++) {
                                                                    echo $_notif_['notif'][$i]['url'];
                                                                    ?>
                                                                    <p><small><?= $_notif_['notif'][$i]['jml']; ?> Dokumen</small></p></a> 
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    <?php endif; ?>
                                    <div class="right-notification">
                                        <ul class="notification-menu">
                                            <li> <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <img src="<?= base_url(); ?>assets/bend/img/avatar-mini.jpg" alt=""><?= $this->newsession->userdata('nama'); ?><span class=" fa fa-angle-down"></span> </a>
                                                <ul class="dropdown-menu dropdown-usermenu purple pull-right">
                                                    <li><a href="<?= site_url('setting/view/profil'); ?>"> Profile</a></li>
                                                    <li><a href="<?= site_url('logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Keluar</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {_content_}
                            <footer> <?=date('Y')?> &copy; Kementerian Perdagangan Republik Indonesia</footer>
                        </div>
                        </section>
                        <script src="<?= base_url(); ?>assets/bend/js/modernizr.min.js"></script> 
                        <script src="<?= base_url(); ?>assets/bend/js/jquery.nicescroll.js" type="text/javascript"></script> 
                        <script src="<?= base_url(); ?>assets/bend/js/scripts.js"></script>
                        <script src="<?= base_url(); ?>assets/bend/js/sipt.js?v=<?= date("Ymdhis"); ?>"></script>
                        </body>
                        </html>
                        <script>
                            $(document).ready(function() {
                                $('.toggle-btn').click();
                            });
                        </script>