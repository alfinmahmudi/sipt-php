<!DOCTYPE html>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/owl.carousel.js" type="text/javascript"></script>
<link href="<?=base_url();?>assets/bend/css/owl.carousel.css" rel="stylesheet">

<style>
    .owl-item{
        min-width: 1000px;
    }
</style>

<div class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <div align="center" class="col-md-12 col-lg-12" id="dir" style="width: 100%; height: 400px; margin: 0 auto"></div>
        </div>
    </div>

    <div style="height: 10px;">
            &nbsp;
        </div>

    <div class="row">
        <div class="col-md-1 col-lg-1">
            &nbsp;
        </div>
        <div class="col-md-10 col-lg-10">
            <section class="panel">
                <div class="slick-carousal" style="background: #f3f3f3;">
                    <div id="news-feed3" style="background: #f3f3f3; width: 100%;" class="owl-carousel owl-theme">
                        <?php foreach ($ket as $cuy) { ?>
                            <div style="width: 480px; height: 300px;" id="dir<?= $cuy['id']; ?>"></div>
                        <?php } ?>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-1 col-lg-1">
            &nbsp;
        </div>
    </div>
</div>

<script>
var binus = <?php echo json_encode($dir['01']); ?>;
var logis = <?php echo json_encode($dir['02']); ?>;
var bapok = <?php echo json_encode($dir['03']); ?>;

    var izin = <?php echo json_encode($izin); ?>;
    var nama = <?php echo json_encode($id['nama_izin']); ?>;
    var i = 0;
    //console.log(nama);
    $.each(izin, function(val, key){
       //console.log(key);
            Highcharts.chart('dir'+val, {
            chart: {
                type: 'column',
                width: 1000px wide
            },
            title: {
                text: 'Direktorat Jenderal Perdagangan Dalam Negeri'
            },
            subtitle: {
                text: 'Sistem Informasi Perizinan Terpadu'
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Rainfall (mm)'
                },
                tickInterval: 1
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Bina Usaha Dan Pelaku Distribusi',
                data: key

            }]
        });
        i++;
    });

Highcharts.chart('dir', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Direktorat Jenderal Perdagangan Dalam Negeri'
    },
    subtitle: {
        text: 'Sistem Informasi Perizinan Terpadu'
    },
    xAxis: {
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Rainfall (mm)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Bina Usaha Dan Pelaku Distribusi',
        data: binus

    }, {
        name: 'Sarana Distribusi dan Logistik',
        data: logis

    }, {
        name: 'Barang Kebutuhan Pokok dan Barang Penting',
        data: bapok

    }]
});
</script>

<script>
    $(document).ready(function() {
        $("#news-feed3").owlCarousel({
            navigation : true,
            slideSpeed : 100,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:true
        });
    });
</script>