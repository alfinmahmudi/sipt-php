<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<div class="wrapper">
    <div class="row">
        <div class="col-md-6">
			<div id="izin" style="min-width: 610px; height: 500px; max-width: 600px; margin: 0 auto"></div>
		</div>

<!-- 		<div class="col-md-12" style="height: 20px;">

		</div> -->

		<div class="col-md-6">
			<div id="daerah" style="min-width: 610px; height: 500px; max-width: 600px; margin: 0 auto"></div>
		</div>
		
	</div>
</div>


<script>
var first_daerah = <?= $first_daerah; ?>;
var sec_daerah = <?= $sec_daerah; ?>;
var third_daerah = <?= $third_daerah; ?>;
var fourth_daerah = <?= $fourth_daerah; ?>;
var fifth_daerah = <?= $fifth_daerah; ?>;

var first_nama = '<?= $first_nama; ?>';
var sec_nama = '<?= $sec_nama; ?>';
var third_nama = '<?= $third_nama; ?>';
var fourth_nama = '<?= $fourth_nama; ?>';
var fifth_nama = '<?= $fifth_name; ?>';

	Highcharts.chart('daerah', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Provinsi Terbanyak Mengajukan Perizinan'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: first_nama,
            y: first_daerah
        }, {
            name: sec_nama,
            y: sec_daerah,
            sliced: true,
            selected: true
        }, {
            name: third_nama,
            y: third_daerah
        }, {
            name: fourth_nama,
            y: fourth_daerah
        }, {
            name: fifth_nama,
            y: fifth_daerah
        }]
    }]
});

var first_jml = <?= $first_jml; ?>;
var sec_jml = <?= $sec_jml; ?>;
var third_jml = <?= $third_jml; ?>;
var fourth_jml = <?= $fourth_jml; ?>;
var fifth_jml = <?= $fifth_jml; ?>;

var first_name = '<?= $first_name; ?>';
var sec_name = '<?= $sec_name; ?>';
var third_name = '<?= $third_name; ?>';
var fourth_name = '<?= $fourth_name; ?>';
var fifth_name = '<?= $fifth_name; ?>';

	Highcharts.chart('izin', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Izin Terbanyak Yang Di Ajukan Tahun 2017'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: first_name,
            y: first_jml
        }, {
            name: sec_name,
            y: sec_jml,
            sliced: true,
            selected: true
        }, {
            name: third_name,
            y: third_jml
        }, {
            name: fourth_name,
            y: fourth_jml
        }, {
            name: fifth_name,
            y: fifth_jml
        }]
    }]
});
</script>