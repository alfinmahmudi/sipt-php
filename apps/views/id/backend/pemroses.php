<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //print_r($dir);print_r($doc);die();?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Profil Pengguna</h3>
</div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fprofil" name="fprofil" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Pengguna</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id ="id" name="id" wajib="yes" value="<?= $sess['id']; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label">Username</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="username" onchange="validate_username()" name="PROFIL[username]" wajib="yes" value="<?= $sess['username'];?>" />
              </div>
            </div>
            <div class="col-lg-12 col-sm-12 alert alert-danger alert-dismissable" style="display: none; background: red; color: white;" id="warn_username" contenteditable="true">
                        <strong></strong>
            </div>
            <?php if ($edit == 'edit') {?>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label" for="password">Password</label>
              <div class="col-sm-9">
                <div class="form-group">
                  <div class="input-group m-b-10" id="<?= rand(); ?>"  data-name = "dataon[password]">
                    <input type="password" class="form-control mb-10" id ="password" name="password" readonly="readonly" value="" /><span class="input-group-addon"><input type="checkbox" class="chkedit" id="<?= rand(); ?>" /></span>
                 </div>
                 <p class="help-block">Ceklist jika ingin merubah password.</p>
                </div>
              </div>
            </div>
            <?php } ?>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nama Lengkap Pengguna</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="nama" name="PROFIL[nama]" wajib="yes" value="<?= $sess['nama'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">NIP</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="nama" name="PROFIL[nip]" wajib="yes" value="<?= $sess['nip'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jenis Role Pengguna</label>
              <div class="col-sm-9">
                <?= form_dropdown('PROFIL[role]', $role, $sess['role'], 'id="role" wajib="yes" onchange="change()" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row" id="form_direktorat">
              <label class="col-sm-3 control-label">Jenis Direktorat Pengguna</label>
              <div class="col-sm-9">
                <?= form_dropdown('direktorat', $direktorat, $dir, 'id="direktorat" onchange="get_izin()" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row" id="form_izin">
              <label class="col-sm-3 control-label">Jenis Izin</label>
              <div class="col-sm-9" id="izin">
                
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Pengguna</label>
              <div class="col-sm-9">
                <!-- <input type="text" class="form-control mb-10" id ="alamat" name="PROFIL[alamat]" wajib="yes" value="<?= $sess['alamat'];?>" /> -->
                <textarea class="form-control mb-10" id ="alamat" name="PROFIL[alamat]" wajib="yes"><?= $sess['alamat'];?></textarea>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">E-mail Pengguna</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="email" name="PROFIL[email]" wajib="yes" aria-required="true" type="email" value="<?= $sess['email'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jenis Identitas Pengguna</label>
              <div class="col-sm-9">
                <?= form_dropdown('PROFIL[tipe_identitas]', $jenis_identitas, $sess['tipe_identitas'], 'id="tipe_identitas" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nomor Identitas Pengguna</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="no_identitas" name="PROFIL[no_identitas]" wajib="yes" value="<?= $sess['no_identitas'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telepon Pengguna</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="telp" name="PROFIL[telp]" wajib="yes" value="<?= $sess['telp'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Fax Pengguna</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="fax" name="PROFIL[fax]" wajib="yes" value="<?= $sess['fax'];?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Status User<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?php if ($sess['status'] == 00) {
                  $aktif = 'checked';
                }elseif ($sess['status'] == 03) {
                  $tidak = 'checked';
                } ?>
                  <label class="checkbox-custom inline check-success">
                    <input value="00" id="cb-dalam" type="checkbox" name="stat[]" <?= $aktif; ?> check-agreement="yes"> 
                    <label for="cb-dalam">
                        <p>Aktif</p>
                    </label>
                  </label>
                  <label class="checkbox-custom inline check-success">
                    <input value="03" id="cb-luar" type="checkbox" name="stat[]" <?= $tidak; ?> check-agreement="yes"> 
                    <label for="cb-luar">
                        <p>Tidak Aktif</p>
                    </label>
                  </label>
               </div>
            </div>
            <!-- <div style="height:5px;"></div> -->
            <div style="height:25px;"></div> 
            <div class="row"> 
              <div class="col-sm-12"> 
                <button class="btn btn-sm btn-default addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-undo"></i>Kembali</button>
                <button class="btn btn-sm btn-success addon-btn m-b-10" id="simpan" onclick="post('#fprofil'); return false;"><i class="fa fa-check"></i>Simpan</button>
                </div> 
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script>
$(document).ready(function(e){ 
  $(".chkedit").change(function(e){
    var $this = $(this);
    var $div = $this.parent().parent(); 
    var $input = $this.parent().parent().children().first();
    var $before = $input.val();
    if($this.is(":checked")){
      $input.removeAttr("readonly");
      $input.attr("wajib","yes");
      $('#retypepwd').show();
    }else{
      $input.attr("readonly","readonly");
      $input.removeAttr("wajib");
      $('#retypepwd').hide();
    }
    return false;
  });
  $.ajax({
		url : '<?= site_url('post/registrasi/get_izin'); ?>',
        data: "dir="+$('#direktorat').val()+"&id="+$('#id').val(),
        type : 'post',
        success : function(e){
        	$('#izin').html(e);	
        }
	})
  $(function() {
    $("input:checkbox").on('click', function() {
      // in the handler, 'this' refers to the box clicked on
      var $box = $(this);
      if ($box.is(":checked")) {
        // the name of the box is retrieved using the .attr() method
        // as it is assumed and expected to be immutable
        var group = "input:checkbox[name='" + $box.attr("name") + "']";
        // the checked state of the group/box on the other hand will change
        // and the current value is retrieved using .prop() method
        $(group).prop("checked", false);
        $box.prop("checked", true);
      } else {
        $box.prop("checked", false);
      }
    });
  });
});

function change(){
  var special_role = ['02', '09', '99', '08', '04'];
  var role = $('#role').val();
  if(jQuery.inArray(role, special_role) !== -1) {
    if (role == '02') {
      $('#form_izin').fadeOut();
      $('#izin').removeAttr("wajib");
    }else{
      $('#form_izin').fadeOut();
      $('#form_direktorat').fadeOut();
      $('#izin').removeAttr("wajib");
      $('#direktorat').removeAttr("wajib");
      //console.log("special");
    }
  }else{
    $('#form_izin').fadeIn();
    $('#form_direktorat').fadeIn();
    $('#izin').attr("wajib", "yes"); 
    $('#direktorat').attr("wajib", "yes"); 
  }
}

function get_izin(){
	$.ajax({
		url : '<?= site_url('post/registrasi/get_izin'); ?>',
        data: "dir="+$('#direktorat').val()+"&id="+$('#id').val(),
        type : 'post',
        success : function(e){
        	$('#izin').html(e);	
        }
	})
}

function validate_username() {
        var site_url = '<?php echo site_url(); ?>';
        //console.log(no_lc);
        $.post(site_url + 'licensing/check_username', {data: $('#username').val()}, function (data) {
            var cek = data;
            if (cek > 0)
            {
              $('#warn_username').fadeIn();
              $('#warn_username').html('Username yang diinput sudah digunakan');
              $('#simpan').attr('disabled', true);   
            } else
            {
              $('#warn_username').fadeOut();
              $('#simpan').attr('disabled', false);
            }
        });
    }
</script>