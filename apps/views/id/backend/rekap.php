<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<meta http-equiv="Cache-control" content="no-cache">
<link rel="stylesheet" href="<?= base_url(); ?>assets/fend/js/bootstrap-dialog/bootstrap-dialog.min.css" type="text/css" cache="false" />
<script src="<?= base_url(); ?>assets/fend/js/bootstrap-dialog/bootstrap-dialog.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtable.js?v=<?= date("YmdHis"); ?>" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtablehash.js?v=<?= date("YmdHis"); ?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
   <?php if($_SESSION['role'] == '98'){?>
  <h3 class="m-b-less"> Pelaku TDPUD </h3>
    <?php }else{ ?>
  <h3 class="m-b-less"> Rekapitulasi </h3>
    <?php } ?>
  <span class="sub-title">Rekapitulasi SIPT PDN</span> </div>
<?php if ($jns_brg == '') { ?>
<div class="wrapper">
  <div class="row">
    <form id="divpersyaratan" name = "divpersyaratan" action = "<?= site_url(); ?>post/licensing/rekap_act/search" data-target = "#reqlicense" method = "post">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Rekap Perizinan</header>
          <div class="panel-body">
			<div class="row">
				 <label class="col-sm-3 control-label">Nama Izin</label>
				 <div class="col-sm-9">
						<select name="dokumen" id="single-append-text" class="form-control input-sm mb-10 select2" tabindex="-1" wajib="yes">
							<option></option>
							<?php foreach($dir as $res) {?>
								<optgroup label="<?=$res['uraian'];?>">
									<?php foreach($izin[$res['kode']] as $t){?>
										<option <?= $selected ?> value="<?=$res['kode']."|".$t['id'];?>"><?=$t['nama_izin'];?></option>
									<?php } ?>	
								</optgroup>
							<?php } ?>
						</select>				   
				</div>
			</div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Priode</label>
              <div class="col-sm-2">
				  <!-- <input type="text" id="tg_awal" class="form-control mb-10 datepickers" name="tgl_awal" wajib="yes"  data-date-format = "yyyy-mm-dd" /> -->
                  <input type="text" class="form-control mb-10 datepickers" data-date-format = "yyyy-mm-dd" id="tg_awal" name="tgl_awal" name="tgl_awal" wajib="yes" value="2017-04-01"/>
              </div>
			  <div class="col-sm-1">
					<span>s/d</span>
              </div>
			  <div class="col-sm-2" style="margin-left:-25px;">
				  <!-- <input type="text" id="tg_akhir" class="form-control mb-10 datepickers" name="tgl_akhir" wajib="yes"  data-date-format = "yyyy-mm-dd" /> -->
				  <input type="text" class="form-control mb-10 datepickers" data-date-format = "yyyy-mm-dd" id="tg_akhir" name="tgl_akhir" name="tgl_akhir" wajib="yes" />
              </div>
            </div>
            <?php if($_SESSION['role'] == '98') { $hidden = 'visibility: hidden;';}else{$hidden = '';} ?>
			<div style="height:5px; <?= $hidden ?>"></div>
            <div class="row" style="<?= $hidden ?>">
              <label class="col-sm-3 control-label">Status</label>
              <div class="col-sm-9">
                <?= form_dropdown('status', $status, $set_status, 'id="status" class="form-control input-sm  mb-10" wajib="yes"'); ?>
              </div>
            </div>
            
            <div id="wilayah" style="<?= $hidden ?>">
            	<div style="height:5px;"></div>
	            <div class="row">
	              <label class="col-sm-3 control-label">Provinsi</label>
	              <div class="col-sm-9">
	                <?= form_dropdown('rekap[kdprop]', $propinsi, $this->newsession->userdata('kdprop'), 'id="kdprop" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab\'); return false;"'); ?>
	              </div>
	            </div>
	            <div style="height:5px;"></div>
	            <div class="row">
	              <label class="col-sm-3 control-label">Kabupaten / Kota</label>
	              <div class="col-sm-9">
	                <?= form_dropdown('rekap[kdkab]', $kabupaten, '', 'id="kdkab" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec\');"'); ?>
	              </div>
	            </div>
                    <div style="height:5px;"></div>
                    <div class="row">
	              <label class="col-sm-3 control-label">Kecamatan </label>
	              <div class="col-sm-9">
	                <?= form_dropdown('rekap[kdkec]', $kecamatan, $this->newsession->userdata('kdkec'), 'id="kdkec" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel\');"'); ?>
	              </div>
	            </div>
	            <div style="height:5px;"></div>
	            <div class="row"">
	              <label class="col-sm-3 control-label">Kelurahan / Desa </label>
	              <div class="col-sm-9">
	                <?= form_dropdown('rekap[kdkel]', $kelurahan, $this->newsession->userdata('kdkel'), 'id="kdkel" class="form-control input-sm select2"'); ?>
	              </div>
	            </div>
                    
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <button class="btn btn-sm btn-primary addon-btn m-b-10" onclick="serializediv('#divpersyaratan'); return false;"><i class="fa fa-search"></i>Cari</button>
                </span>
                <button class="btn btn-sm btn-success addon-btn m-b-10" data-url="<?= site_url(); ?>post/licensing/tes" onclick="excel($(this),'#divpersyaratan'); return false;"><i class="fa fa-file-excel-o"></i>Export Excel</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    </form>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading"></header>
        <div class="panel-body" id="reqlicense">
        	
        </div>
      </section>
    </div>
  </div>
</div>
<?php } elseif ($jns_brg != '' && $jenis_bapok != '') { ?>
<div class="wrapper">
	<div class="row">
            <form id="divbpkstra" name = "divbpkstra" action = "<?= site_url(); ?>post/licensing/rekap_act/search_bapok" data-target = "#reqlicense" method = "post">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading"> Rekap Tabulasi TDPUD</header>
                            <div class="panel-body">
                                 <?php if($_SESSION['role'] != '98'){ ?>
                            <div class="row">
                                <label class="col-sm-3 control-label">Jenis Komoditi</label>
                                <div class="col-sm-9">
                                    <?= form_dropdown('rekap_bapok[jns_brg]', $jns_brg, '', 'id="jns_brg" class="form-control input-sm mb-10 select2" wajib="yes"'); ?>		   
                                </div>
                            </div>
                                <div style="height:5px;"></div>
                            <?php } ?>
                                <div class="row">
                                    <label class="col-sm-3 control-label">Bulan<font size="2" color="red">*</font></label>
                                    <div class="col-sm-2">
                                        <?= form_dropdown('rekap_bapok[bulan]', $bulan, $sess['bulan'], 'id="bulan" placeholder="Bulan" wajib="yes" class="form-control mb-10"'); ?>
                                    </div>
                                    <?php if($_SESSION['role'] != '98'){ ?>
                                <div class="col-sm-1" style="text-align: center">s/d</div> 
                                <div class="col-sm-2">
                                    <?= form_dropdown('rekap_bapok[bulan_akhir]', $bulan, $sess['bulan_akhir'], 'id="bulan_akhir" placeholder="Bulan" wajib="yes" class="form-control mb-10"'); ?>
                                </div>
                                <?php } ?>
                                </div>
                                <div style="height:5px;"></div>
                                <div class="row">
                                    <label class="col-sm-3 control-label">Tahun<font size="2" color="red">*</font></label>
                                    <div class="col-sm-5">
                                        <?= form_dropdown('rekap_bapok[tahun]', $tahun, $sess['tahun'], 'id="tahun" placeholder="Tahun" wajib="yes" class="form-control mb-10"'); ?>
                                    </div>
                                </div>
                                <?php  $hidden = 'visibility: hidden;'; ?>
                                <div style="height:5px; <?= $hidden ?>"></div>
                                <div class="row" style="<?= $hidden ?>">
                                    <label class="col-sm-3 control-label">Provinsi</label>
                                    <div class="col-sm-9">
                                        <?= form_dropdown('rekap_bapok[kdprop]', $propinsi, $this->newsession->userdata('kdprop'), 'id="kdprop" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab\'); return false;"'); ?>
                                    </div>
                                </div>
                                <div style="height:5px;" <?= $hidden ?>></div>
                                <div class="row" style="<?= $hidden ?>">
                                    <label class="col-sm-3 control-label">Kabupaten / Kota</label>
                                    <div class="col-sm-9">
                                        <?= form_dropdown('rekap_bapok[kdkab]', $kabupaten, '', 'id="kdkab" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec\');"'); ?>
                                    </div>
                                </div>

                            </div>
                        </section>
                        <div style="height:25px;"></div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn btn-sm btn-primary addon-btn m-b-10" onclick="serializediv('#divbpkstra'); return false;"><i class="fa fa-search"></i>Cari</button>
                                <button type="button" class="btn btn-sm btn-success addon-btn m-b-10" onclick="excel_bpkstra($(this),'#divbpk'); return false;"><i class="fa fa-file-excel-o"></i>Export Excel</button>
                            </div>
                        </div>
                    </div>
                </form>
	</div>
    <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading"></header>
        <div class="panel-body" id="reqlicense">
        	
        </div>
      </section>
    </div>
  </div>
</div>
<?php }elseif($antarpulau != ""){ ?>
    <div class="wrapper">
	<div class="row">
            <form id="divantrpl" name = "divantrpl" action = "<?= site_url(); ?>post/licensing/rekap_act/search_label" data-target = "#reqlicense" method = "post">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading"> Rekap Label Beras</header>
                                <div style="height:5px;"></div>
                                <div class="panel-body">
                                    <div class="row">
                                        <label class="col-sm-3 control-label">Periode</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control mb-10 datepickers" data-date-format = "yyyy-mm-dd" id="tg_awal" name="tgl_awal"/>
                                        </div>
                                        <div class="col-sm-1">
                                            <span>s/d</span>
                                        </div>
                                        <div class="col-sm-2" style="margin-left:-25px;">
                                            <input type="text" class="form-control mb-10 datepickers" data-date-format = "yyyy-mm-dd" id="tg_akhir" name="tgl_akhir"/>
                                        </div>
                                    </div>
                                    <div style="height:5px;"></div>
                                    <div class="row">
                                    <label class="col-sm-3 control-label">Jenis Perusahaan</label>
                                    <div class="col-sm-5">
                                        <?= form_dropdown('label[jenis]', $beras, $sess['jenis'], 'id="beras" placeholder="Jenis Perusahaan" class="form-control mb-10"'); ?>
                                    </div>
                                    </div>
                                    <div style="height:5px;"></div>
                                   <div class="row" style="<?= $hidden ?>">
			              <label class="col-sm-3 control-label">Provinsi</label>
			              <div class="col-sm-9">
			                <?= form_dropdown('rekap[kdprop]', $propinsi, $this->newsession->userdata('kdprop'), 'id="kdprop" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab\'); return false;"'); ?>
			              </div>
			            </div>
			            <div style="height:5px; <?= $hidden ?>"></div>
			            <div class="row" style="<?= $hidden ?>">
			              <label class="col-sm-3 control-label">Kabupaten / Kota</label>
			              <div class="col-sm-9">
			                <?= form_dropdown('rekap[kdkab]', $kabupaten, '', 'id="kdkab" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec\');"'); ?>
			              </div>
			            </div>
                                </div>
			<div style="height:5px;"></div>
                        </section>
                        <div style="height:25px;"></div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="button" class="btn btn-sm btn-success addon-btn m-b-10" onclick="excel_label($(this),'#divantrpl'); return false;"><i class="fa fa-file-excel-o"></i>Export Excel</button>
                            </div>
                        </div>
                    </div>
                </form>
	</div>
    <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading"></header>
        <div class="panel-body" id="reqlicense">
        	
        </div>
      </section>
    </div>
  </div>
</div>
<?php } elseif ($jns_brg != '' && $prshn != '') { ?>
    <div class="wrapper">
        <div class="row">
            <form id="divbpkstra" name = "divbpkstra" action = "<?= site_url(); ?>post/licensing/rekap_act/search_bapok" data-target = "#reqlicense" method = "post">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading"> Rekap Perusahaan TDPUD</header>
                        <div class="panel-body">
                            <?php if($_SESSION['role'] != '98'){ ?>
                            <div class="row">
                                <label class="col-sm-3 control-label">Jenis Komoditi</label>
                                <div class="col-sm-9">
                                    <?= form_dropdown('rekap_bapok[jns_brg]', $jns_brg, '', 'id="jns_brg" class="form-control input-sm mb-10 select2" wajib="yes"'); ?>		   
                                </div>
                            </div>
                            <?php } ?>
                            <div style="height:5px;"></div>
                            <div class="row">
                                <label class="col-sm-3 control-label">Bulan<font size="2" color="red">*</font></label>
                                <div class="col-sm-2">
                                    <?= form_dropdown('rekap_bapok[bulan]', $bulan, $sess['bulan'], 'id="bulan" placeholder="Bulan" wajib="yes" class="form-control mb-10"'); ?>
                                </div>
                                <?php if($_SESSION['role'] != '98'){ ?>
                                <div class="col-sm-1" style="text-align: center">s/d</div> 
                                <div class="col-sm-2">
                                    <?= form_dropdown('rekap_bapok[bulan_akhir]', $bulan, $sess['bulan_akhir'], 'id="bulan_akhir" placeholder="Bulan" wajib="yes" class="form-control mb-10"'); ?>
                                </div>
                                <?php } ?>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                                <label class="col-sm-3 control-label">Tahun<font size="2" color="red">*</font></label>
                                <div class="col-sm-5">
                                    <?= form_dropdown('rekap_bapok[tahun]', $tahun, $sess['tahun'], 'id="tahun" placeholder="Tahun" wajib="yes" class="form-control mb-10"'); ?>
                                </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                                <label class="col-sm-3 control-label">Provinsi</label>
                                <div class="col-sm-9">
                                    <?= form_dropdown('rekap[kdprop]', $propinsi, $this->newsession->userdata('kdprop'), 'id="kdprop" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab\'); return false;"'); ?>
                                </div>
                            </div>

                        </div>
                    </section>
                    <div style="height:25px;"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="button" class="btn btn-sm btn-success addon-btn m-b-10" onclick="excel_bpk_prshn($(this), '#divbpk'); return false;"><i class="fa fa-file-excel-o"></i>Export Excel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"></header>
                    <div class="panel-body" id="reqlicense">

                    </div>
                </section>
            </div>
        </div>
    </div>
<?php } elseif ($jns_brg != '') { ?>
<div class="wrapper">
	<div class="row">
		<form id="divbpk" name = "divbpk" action = "<?= site_url(); ?>post/licensing/rekap_act/search" data-target = "#reqlicense" method = "post">
			<div class="col-lg-12">
			    <section class="panel">
			      <header class="panel-heading"> Rekap Pelaporan TDPUD</header>
			    	<div class="panel-body">
						<div class="row">
							 <label class="col-sm-3 control-label">Jenis Komoditi</label>
							 <div class="col-sm-9">
								<?= form_dropdown('rekap[jns_brg]', $jns_brg, '', 'id="jns_brg" class="form-control input-sm mb-10 select2" wajib="yes"'); ?>		   
							</div>
						</div>
						<div style="height:5px;"></div>
			        	<div class="row">
			                <label class="col-sm-3 control-label">Tanggal Pelaporan<font size="2" color="red">*</font></label>
			                <div class="col-sm-4">
			                	<?=form_dropdown('gula[bulan]', $bulan, $sess['bulan'], 'id="bulan" placeholder="Bulan" wajib="yes" class="form-control mb-10"'); ?>
			                </div>
			                <div class="col-sm-5">
			                	<?=form_dropdown('gula[tahun]', $tahun, $sess['tahun'], 'id="tahun" placeholder="Tahun" wajib="yes" class="form-control mb-10"'); ?>
			                </div>
			            </div>
                                    <?php if($_SESSION['role'] == '98') { $hidden = 'visibility: hidden;';}else{$hidden = '';} ?>
						<div style="height:5px; <?= $hidden ?>"></div>
                                    
			            <div class="row" style="<?= $hidden ?>">
			              <label class="col-sm-3 control-label">Provinsi</label>
			              <div class="col-sm-9">
			                <?= form_dropdown('rekap[kdprop]', $propinsi, $this->newsession->userdata('kdprop'), 'id="kdprop" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab\'); return false;"'); ?>
			              </div>
			            </div>
			            <div style="height:5px; <?= $hidden ?>"></div>
			            <div class="row" style="<?= $hidden ?>">
			              <label class="col-sm-3 control-label">Kabupaten / Kota</label>
			              <div class="col-sm-9">
			                <?= form_dropdown('rekap[kdkab]', $kabupaten, '', 'id="kdkab" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec\');"'); ?>
			              </div>
			            </div>
                                    
                                    <div style="height:5px; <?= $hidden ?>"></div>
                                    <div class="row" style="<?= $hidden ?>">
			              <label class="col-sm-3 control-label">Kecamatan </label>
			              <div class="col-sm-9">
			                <?= form_dropdown('rekap[kdkec]', $kecamatan, $this->newsession->userdata('kdkec'), 'id="kdkec" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel\');"'); ?>
			              </div>
			            </div>
			            <div style="height:5px; <?= $hidden ?>"></div>
			            <div class="row" style="<?= $hidden ?>">
			              <label class="col-sm-3 control-label">Kelurahan / Desa </label>
			              <div class="col-sm-9">
			                <?= form_dropdown('rekap[kdkel]', $kelurahan, $this->newsession->userdata('kdkel'), 'id="kdkel" class="form-control input-sm select2"'); ?>
			              </div>
			            </div>
					</div>
				</section>
				<div style="height:25px;"></div>
	            <div class="row">
	              <div class="col-sm-12">
	             	<button type="button" class="btn btn-sm btn-success addon-btn m-b-10" onclick="excel_bpk($(this),'#divbpk'); return false;"><i class="fa fa-file-excel-o"></i>Export Excel</button>
	              </div>
	            </div>
			</div>
		</form>
	</div>
</div>
<?php } ?>
<script src="<?= base_url(); ?>assets/bend/js/select2.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script>
	$(document).ready(function(){
		$(".datepickers").datepicker({
			autoclose: true
		});
		$("#single-append-text").select2({
			placeholder: 'Nama Izin'
		});
	});

	// function view_wil(){
	// 	var sel = $('#single-append-text').val();
	// 	var sel_split = sel.split('|');
	// 	// console.log(sel_split);
	// 	var kd_izin = sel_split[1];
	// 	if (kd_izin == 22) {
	// 		$('#wilayah').show();
	// 	}else{
	// 		$('#wilayah').hide();
	// 	}
	// }

	function excel(btn,p){
		var $this = $(p);
		var $wajib = 0;
		$.each($("input:visible, select:visible, textarea:visible"), function(){
			if($(this).attr('wajib')){
				if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
					$(this).css('border-color','#b94a48');
					$wajib++;
				}
			}
		});
		if($wajib > 0){
			BootstrapDialog.show({
			title: '',
			type: BootstrapDialog.TYPE_WARNING,
			message: '<p>Ada <b>'+$wajib+'</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>'
			});
			return false;
		}else{
			
			//var form = document.forms[0];
			//form.method="POST";
			//form.action = $btn;
			//form.submit();
			var nm_izin = $("#single-append-text").val();
			var tg1 = $("#tg_awal").val();
			var tg2 = $("#tg_akhir").val();
			var st = $("#status").val();
			var kdprop = $("#kdprop").val();
			var kdkab = $("#kdkab").val();
			var kdkel = $("#kdkel").val();
			var kdkec = $("#kdkec").val();
                        if (kdprop == '') {
				kdprop = 'null';
			}
			if (kdkab == '') {
				kdkab = 'null';
			}
			if (kdkec == '') {
				kdkec = 'null';
			}
			if (kdkel == '') {
				kdkel = 'null';
			}
			$btn = $(btn).attr('data-url') +'/'+ nm_izin.replace("|",".") +'/'+ tg1 +'/'+ tg2 +'/'+st +'/'+kdprop +'/'+kdkab +'/'+kdkec +'/'+kdkel;
			//window.location=$btn;
			//console.log($btn);return false;
			window.open($btn, '_blank');
			
		}	
	}

	function excel_bpk(btn,p){
		var $this = $(p);
		var $wajib = 0;
		var form_bpk = $this.serialize();
		var jns_brg = $('#jns_brg').val();
		// $.post('<?= site_url(); ?>post/licensing/excel_bpk/ajax', form_bpk, function(data){
		// 	console.log(data);
		// });
		var bulan = $("#bulan").val();
		var tahun = $("#tahun").val();
		var kdprop = $("#kdprop").val();
		var kdkab = $("#kdkab").val();
		var kdkel = $("#kdkel").val();
		var kdkec = $("#kdkec").val();
		if (jns_brg == '') {
			BootstrapDialog.show({
			title: '',
			type: BootstrapDialog.TYPE_WARNING,
			message: '<p>Harap Mengisi Jenis Barang.</p>'
			});
			return false;
		}else{
			if (kdprop == '') {
				kdprop = 'null';
			}
			if (kdkab == '') {
				kdkab = 'null';
			}
			if (kdkec == '') {
				kdkec = 'null';
			}
			if (kdkel == '') {
				kdkel = 'null';
			}
                        
			$btn = '<?= site_url(); ?>post/licensing/excel_bpk/ajax' +'/'+jns_brg+'/'+kdprop +'/'+kdkab +'/'+kdkec +'/'+kdkel+'/'+bulan+'/'+tahun;
			//window.location=$btn;
			//console.log($btn);return false;
			window.open($btn, '_blank');
		}
	}
        
        function excel_bpkstra(btn,p){
		var $this = $(p);
		var $wajib = 0;
		var form_bpk = $this.serialize();
		var bulan = $("#bulan").val();
                                var jns_brg = $("#jns_brg").val();
                                var bulan_akhir = $("#bulan_akhir").val();
                                var tahun = $("#tahun").val();
                                var kdprop = $("#kdprop").val();
                                var kdkab = $("#kdkab").val();
			if (kdprop == '') {
				kdprop = 'null';
			}
			if (kdkab == '') {
				kdkab = 'null';
			}
			if(<?= $_SESSION['role']; ?> != '98')
                                $btn = '<?= site_url(); ?>post/licensing/excel_bpkstra/ajax' + '/' + kdprop + '/' + kdkab + '/' + bulan + '/' + tahun + '/' + bulan_akhir + '/' + jns_brg;
                                else
                                $btn = '<?= site_url(); ?>post/licensing/excel_bpkstra_user/ajax' +'/'+kdprop +'/'+kdkab +'/'+bulan+'/'+tahun;
			//window.location=$btn;
			//console.log($btn);return false;
			window.open($btn, '_blank');
		}
                
        function excel_label(btn,p){
		var $this = $(p);
		var $wajib = 0;
		var form_bpk = $this.serialize();
                var tg1 = $("#tg_awal").val();
		var tg2 = $("#tg_akhir").val();
                var beras = $("#beras").val();
                var kdprop = $("#kdprop").val();
                var kdkab = $("#kdkab").val();
			if (tg1 == '') {
				tg1 = 'null';
			}
			if (tg2 == '') {
				tg2 = 'null';
			}
                        if (kdprop == '') {
				kdprop = 'null';
			}
			if (kdkab == '') {
				kdkab = 'null';
			}
                        if (beras == '') {
				beras = 'null';
			}
			$btn = '<?= site_url(); ?>post/licensing/excel_label/ajax' +'/'+tg1 +'/'+tg2+'/'+beras+'/'+kdprop+'/'+kdkab;
			//window.location=$btn;
			//console.log($btn);return false;
			window.open($btn, '_blank');
		}
                            function excel_bpk_prshn(btn, p) {
                                var $this = $(p);
                                var $wajib = 0;
                                var form_bpk = $this.serialize();
                                var bulan = $("#bulan").val();
                                var jns_brg = $("#jns_brg").val();
                                var bulan_akhir = $("#bulan_akhir").val();
                                var tahun = $("#tahun").val();
                                var kdprop = $("#kdprop").val();
                                if (kdprop == '') {
                                    kdprop = 'null';
                                }
                                
                                $btn = '<?= site_url(); ?>post/licensing/excel_bpk_prshn/ajax' + '/' + kdprop + '/' + bulan + '/' + tahun + '/' + bulan_akhir + '/' + jns_brg;
                                //window.location=$btn;
                                //console.log($btn);return false;
                                window.open($btn, '_blank');
                            }
</script>