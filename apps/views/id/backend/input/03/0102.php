<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="nodaftar">No. Daftar</label>
      <input type="text" class="form-control mb-10" readonly="readonly" value="<?= $sess['no_aju']; ?>" id="nodaftar" />
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="tgldaftar">Tanggal Daftar</label>
      <input type="text" class="form-control mb-10" readonly="readonly" value="<?= $sess['tgl_aju']; ?>" id="tgldaftar" />
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="noizin">Nomor Izin</label>
      <input type="text" class="form-control mb-10" readonly="readonly" value="<?= $sess['no_izin']; ?>" id="noizin" />
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="tglizin">Tanggal Izin</label>
      <div class="input-group m-b-10" id="<?= rand(); ?>" data-action = "<?= base_url() . 'get/onfly/onfly_act/update/'.$dir.'/'.$doc .'/'. hashids_encrypt($sess['id'],_HASHIDS_,9); ?>" data-name = "dataon[tgl_izin]">
        <input type="text" class="form-control mb-10 datepickers" id="tglexpizin" wajib="yes" data-date-format = "dd/mm/yyyy" readonly="readonly" value="<?= $sess['tgl_izin']; ?>" /><span class="input-group-addon"><input type="checkbox" class="chkedit" id="<?= rand(); ?>" <?= ($this->newsession->userdata('role')=="5")?"disabled":""; ?>></span>
     </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="tglexpizin">Tanggal Masa Berlaku</label>
      <div class="input-group m-b-10" id="<?= rand(); ?>" data-action = "<?= base_url() . 'get/onfly/onfly_act/update/'.$dir.'/'.$doc .'/'. hashids_encrypt($sess['id'],_HASHIDS_,9); ?>" data-name = "dataon[tgl_izin_exp]">
        <input type="text" class="form-control mb-10 datepickers" id="tglexpizin" wajib="yes" data-date-format = "dd/mm/yyyy" readonly="readonly" value="<?= $sess['tgl_izin_exp']; ?>" /><span class="input-group-addon"><input type="checkbox" class="chkedit" id="<?= rand(); ?>" <?= ($this->newsession->userdata('role')=="5")?"disabled":""; ?>></span>
     </div>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> 
<script>
	$(document).ready(function(e){ 
		$(".chkedit").change(function(e){
            var $this = $(this);
			var $div = $this.parent().parent(); 
			var $input = $this.parent().parent().children().first();
			var $before = $input.val();
			if($this.is(":checked")){
				$input.removeAttr("readonly");
				$input.datepicker({
					autoclose: true
				}).on('changeDate', function(ev){
					$input.change();
					if($(".modal-backdrop").length === 0){
						BootstrapDialog.confirm('Apakah anda yakin dengan data yang Anda isikan ?', function(r){
							if(r){
								var ParseData = $div.serializeArray();
									ParseData.push({name: $div.attr('data-name'), value: $input.val()});
								$.ajax({
									type: "POST",
									url: $div.attr('data-action') + '/ajax',
									data: ParseData,
									success: function(data){
										if(data.search("MSG") >= 0){
											arrdata = data.split('||');
											if(arrdata[1] == "YES"){
												$input.attr("readonly","readonly");
												$input.datepicker('remove');
												$this.prop('checked', false);
											}else{
												$input.val($before);
											}
										}
									}
								});
							}else{
								$this.prop('checked', false);
								$input.val($before);
								$input.attr("readonly","readonly");
								$input.datepicker('remove');
							}
						});
					}
				});
			}else{
				$input.attr("readonly","readonly");
				$input.datepicker('remove');
			}
			$(".modal-backdrop, .modal, .bootstrap-dialog, .modal-dialog").remove();
			return false;
        });
	});
</script>