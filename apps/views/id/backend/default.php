<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link href="<?= base_url(); ?>assets/bend/css/font-awesome.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/bend/css/font-awesome.min.css" rel="stylesheet">
<div class="page-head">
    <h3> Dashboard </h3>
    <span class="sub-title">Selamat datang di Sistem Informasi Perizinan Terpadu Direktorat Jenderal Perdagangan Dalam Negeri</span> 
    <a class="pull-right" data-step="2" data-intro="Link Untuk Mendownload User Manual Aplikasi SIPT PDN." href="<?= base_url('assets/fend/um/')."/".$this->newsession->userdata('role').".pdf";?> ">Download User Manual</a>
</div>

<?php $pelaku_usaha = '05';
$petugas = array('01', '07', '06', '02');
$verif = array('99', '09'); 
$role = $this->newsession->userdata('role'); ?>

<div class="wrapper">
    <!-- This Dahboard for 'Pelaku Usaha Role' start with validaton session -->
    <?php if ($this->newsession->userdata('role') == '05'){ ?>
        <div class="col-md-12">
            <section class="isolate-tabs w-tabs p-s-info">
                <div class="panel-body">
                    <div class="tab-content">

                        <div id="clock" class="tab-pane active">
                            <div class="media">
                                <a class="media-left media-middle">
                                    <img class="img-circle" alt="" src="<?php echo base_url() . 'assets/fend/img/avatarexec.png'; ?>" data-holder-rendered="true" style="width: 80px; height: 80px;">
                                </a>
                                  
                                <div class="media-body">
                                    <h4 class="media-heading text-uppercase"><?= $this->newsession->userdata('username'); ?></h4>
                                    <div><span class="text-muted"><i class=" icon-envelope-open "></i>&nbsp;&nbsp;&nbsp;<?= $this->newsession->userdata('email_pj'); ?></span></div>
                                    <div><span class="text-muted"><i class="  icon-home  "></i>&nbsp;&nbsp;&nbsp;<?= $this->newsession->userdata('alamat'); ?></span></div>

                                </div>
                            </div>
                        </div>
                        <div id="heart" class="tab-pane">
                        </div>
                        <div id="music" class="tab-pane">


                        </div>
                    </div>
                </div>
                <ul class="nav nav-tabs nav-justified">
                    <li data-step="3" data-intro="Klik disini untuk mengajukan permohonan"  >
                        <a class="btn btn-sm btnpermohonan" act="<?= site_url(); ?>licensing/choice" data-modal="true">
                            <i class="fa fa-plus-square "></i>
                            <h4>Permohonan Baru</h4>
                        </a>
                    </li>
                    <li data-step="4" data-intro="Klik disini untuk melihat persyaratan perizinan" >
                        <a class="btn btn-sm" href="<?= site_url(); ?>licensing/view/requirements">
                            <i class="fa fa-file-archive-o "></i>
                            <h4>Persyaratan Permohonan</h4>
                        </a>
                    </li>
                        <li>
                        <a class="btn btn-sm" href="javascript:void(0);" onclick="javascript:introJs().start();">
                            <i class="fa  fa-spoon "></i>
                            <h4>Bantuan Tutorial</h4>
                        </a>
                    </li>
                </ul>

            </section>
        </div>
        <!-- End Of Dashboard 'Pelaku Usaha Role' -->
        <?php }elseif (in_array($this->newsession->userdata('role'), $petugas) ) { ?>

        <!-- This Dahboard for 'Petugas Role' start with validaton session -->

        <section class="isolate-tabs w-tabs p-s-info">
                <div class="panel-body">
                    <div class="tab-content">

                        <div id="clock" class="tab-pane active">
                            <div class="media">
                                <a class="media-left media-middle">
                                    <img class="img-circle" alt="" src="<?php echo base_url() . 'assets/fend/img/avatarexec.png'; ?>" data-holder-rendered="true" style="width: 80px; height: 80px;">
                                </a>
                                  
                                <div class="media-body">
                                    <h4 class="media-heading text-uppercase"><?= $this->newsession->userdata('username'); ?></h4>
                                    <div><span class="text-muted"><i class=" icon-envelope-open "></i>&nbsp;&nbsp;&nbsp;<?= $this->newsession->userdata('email_pj'); ?></span></div>
                                    <div><span class="text-muted"><i class="  icon-home  "></i>&nbsp;&nbsp;&nbsp;<?= $this->newsession->userdata('alamat'); ?></span></div>

                                </div>
                            </div>
                        </div>
                        <div id="heart" class="tab-pane">
                        </div>
                        <div id="music" class="tab-pane">


                        </div>
                    </div>
                </div>
                <ul class="nav nav-tabs nav-justified">
                    <li>
                        <a class="btn btn-sm"> <!-- onclick="get_table('baru')" data-toggle="modal" data-target="#myModal" -->
                            <i class="fa fa-thumbs-down"></i>
                            <div class="value white">
                                <h1 class="timer" data-from="0" data-to="<?= $jml['lebih_sla']; ?>" 
                                    data-speed="1000">
                                    <!--320-->
                                </h1>
                                <h4>Melewati SLA</h4>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a class="btn btn-sm"> <!-- onclick="get_table('verif')" data-toggle="modal" data-target="#myModal" -->
                            <i class="fa fa-exclamation-triangle"></i>
                            <div class="value white">
                                <h1 class="timer" data-from="0" data-to="<?= $jml['mendekati_sla']; ?>"
                                    data-speed="1000">
                                    <!--320-->
                                </h1>
                                <h4>Mendekati SLA</h4>
                            </div>
                        </a>
                    </li>
                        <li>
                        <a class="btn btn-sm"> <!-- onclick="get_table('tolak')" data-toggle="modal" data-target="#myModal" -->
                            <i class="fa fa-thumbs-up"></i>
                            <div class="value white">
                                <h1 class="timer" data-from="0" data-to="<?= $jml['sesuai_sla']; ?>"
                                    data-speed="1000">
                                    <!--320-->
                                </h1>
                                <h4>Sesuai SLA</h4>
                            </div>
                        </a>
                    </li>
                </ul>
            </section>

            
<!--             <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog modal-lg">

                
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">&nbsp;</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row" style="display: none;" id="disp_table" >
                      <table class="table table-striped custom-table table-hover" style="width: 90%;" align="center">
                      <thead>
                        <tr>
                        <td >No</td>
                        <td >Jenis Perizinan</td>
                        <td >Nama Perusahaan</td>
                        <td id="last_td"></td>
                        <td >Log</td>
                        </tr>
                      </thead>
                      <tbody id="res_table">
                        
                      </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>

            
            <div id="logmodal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">&nbsp;</h4>
                  </div>
                  <div class="modal-body" id="res_log">
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div> -->

            <!-- This END Dahboard for 'Petugas Role' -->
        <? }elseif (in_array($role, $verif)) { ?>
            <!-- This Dahboard for 'Verifikator dan Admin Role' start with validaton session -->
            <section class="isolate-tabs w-tabs p-s-info">
                <div class="panel-body">
                    <div class="tab-content">

                        <div id="clock" class="tab-pane active">
                            <div class="media">
                                <a class="media-left media-middle">
                                    <img class="img-circle" alt="" src="<?php echo base_url() . 'assets/fend/img/avatarexec.png'; ?>" data-holder-rendered="true" style="width: 80px; height: 80px;">
                                </a>
                                  
                                <div class="media-body">
                                    <h4 class="media-heading text-uppercase"><?= $this->newsession->userdata('username'); ?></h4>
                                    <div><span class="text-muted"><i class=" icon-envelope-open "></i>&nbsp;&nbsp;&nbsp;<?= $this->newsession->userdata('email_pj'); ?></span></div>
                                    <div><span class="text-muted"><i class="  icon-home  "></i>&nbsp;&nbsp;&nbsp;<?= $this->newsession->userdata('alamat'); ?></span></div>

                                </div>
                            </div>
                        </div>
                        <div id="heart" class="tab-pane">
                        </div>
                        <div id="music" class="tab-pane">


                        </div>
                    </div>
                </div>
            </section>

            <!--state overview start-->
                <div class="row state-overview">
                    <a onclick="get_user('00')" data-toggle="modal" data-target="#modal_admin">
                        <div class="col-lg-4 col-sm-6">
                            <section class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-send"></i>
                                </div>
                                <div class="value white">
                                    <h1 class="timer" data-from="0" data-to="<?= $jml['baru']; ?>"
                                        data-speed="1000">
                                        <!--320-->
                                    </h1>
                                    <p>Permohonan Baru</p>
                                </div>
                            </section>
                        </div>
                    </a>
                    
                    <a onclick="get_user('01')" data-toggle="modal" data-target="#modal_admin">
                        <div class="col-lg-4 col-sm-6">
                            <section class="panel">
                                <div class="symbol green-color">
                                    <i class="fa fa-thumbs-up"></i>
                                </div>
                                <div class="value gray">
                                    <h1 class="green-color timer" data-from="0" data-to="<?= $jml['terima']; ?>"
                                        data-speed="3000">
                                        <!--2345-->
                                    </h1>
                                    <p>Permohonan Terima</p>
                                </div>
                            </section>
                        </div>
                    </a>

                    <a onclick="get_user('02')" data-toggle="modal" data-target="#modal_admin">
                        <div class="col-lg-4 col-sm-12">
                            <section class="panel green">
                                <div class="symbol ">
                                    <i class="fa fa-thumbs-down"></i>
                                </div>
                                <div class="value white">
                                    <h1 class="timer" data-from="0" data-to="<?= $jml['tolak']; ?>"
                                        data-speed="1000">
                                        <!--432-->
                                    </h1>
                                    <p>Penolakan Permohonan</p>
                                </div>
                            </section>
                        </div>
                    </a>
                    
                </div>
                <!--state overview end-->

                <!--state overview start-->
                <div class="row state-overview">
                    <a onclick="get_user('03')" data-toggle="modal" data-target="#modal_admin">
                        <div class="col-lg-6 col-sm-6">
                            <section class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-exchange"></i>
                                </div>
                                <div class="value white">
                                    <h1 class="timer" data-from="0" data-to="<?= $jml['perubahan']; ?>"
                                        data-speed="1000">
                                        <!--320-->
                                    </h1>
                                    <p>Permohonan Perubahan</p>
                                </div>
                            </section>
                        </div>
                    </a>
                    
                    <a onclick="get_user('04')" data-toggle="modal" data-target="#modal_admin">
                        <div class="col-lg-6 col-sm-6">
                            <section class="panel green">
                                <div class="symbol ">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </div>
                                <div class="value white">
                                    <h1 class="timer" data-from="0" data-to="<?= $jml['tdk_valid']; ?>"
                                        data-speed="1000">
                                        <!--432-->
                                    </h1>
                                    <p>Perubahan Tidak Valid</p>
                                </div>
                            </section>
                        </div>
                    </a>
                    
                </div>
                <!--state overview end-->

            <!-- Modal for table -->
            <div id="modal_admin" class="modal fade" role="dialog">
              <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">&nbsp;</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row" style="display: none;" id="admin_table" >
                      <table class="table table-striped custom-table table-hover" style="width: 90%;" align="center">
                      <thead>
                        <tr>
                        <td >No</td>
                        <td >Nama Perusahaan</td>
                        <td >Alamat Perusahaan</td>
                        <td >Tanggal Permohonan</td>
                        <td >Tanggal Aktivasi</td>
                        <td >Petugas Aktivasi</td>
                        <td >Keterangan</td>
                        </tr>
                      </thead>
                      <tbody id="res_admin">
                        
                      </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>


            <!-- This END Dahboard for 'Verifikator dan Admin Role' -->
        <?php } ?>
</div>
<script src="<?= base_url(); ?>assets/bend/js/jquery-countTo/jquery.countTo.js" type="text/javascript"></script>
<script>
    $(document).ready(function (e) {
        $(".timer").countTo();                                                                     
    });
    $(".btnpermohonan").click(function (e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.attr("data-modal") || $this.attr("data-append")) {
            $.get($this.attr('act'), function (hasil) {
                if (hasil) {
                    if ($this.attr("data-modal")) {
                        if ($(".modal-backdrop").length === 0) {
                            BootstrapDialog.show({
                                title: '',
                                type: BootstrapDialog.TYPE_SUCCESS,
                                size: BootstrapDialog.SIZE_WIDE,
                                message: hasil
                            });
                            return false;
                        }
                    }
                    if ($this.attr("data-append")) {
                        $($this.attr("data-class")).fadeOut();
                        $($this.attr("data-div")).fadeIn(function () {
                            $($this.attr("data-div")).html(hasil);
                            $($this.attr("data-div")).css("margin", "10px");
                        });
                    }
                    return false;
                }
            });
        } else {
            location.href = $(this).attr('act');
        }
        return false;
    });

    //     function get_table(jns){
//      var temp = '';
//      var no = 1;
//      if (role != '05' && role != '<?= $role; ?>' && role != '99') {
//             $.post(site_url + 'licensing/data_table', {jns: jns}, function (data) {
//                 // $('#jml_baru').attr('data-to', data.jml);
//                 $.each(data.data, function(index, value){
//                     temp += '<a href="'+site_url+value.path+'"><tr><td >'+no+'</td>';
//                     temp += '<td ><p>'+value.singkat+'</p></td>';
//                     temp += '<td ><p>'+value.nm_perusahaan+'</p></td>';
//                     temp += '<td ><p>'+value.waktu+'</p></td></a>';
//                     temp += '<td ><p>'+value.Log+'</p></td></a>';
//                     no++;
//                 });
//                 $('#res_table').html(temp);
//                 $('#disp_table').fadeIn();
//                 // view = 1;
//                 if (jns == 'baru') {
//                     $('#last_td').html('Tanggal Kirim');
//                 }else if (jns == 'verif') {
//                     $('#last_td').html('Tanggal Verifikasi');
//                 }else if (jns == 'tolak') {
//                     $('#last_td').html('Tanggal Penolakan');
//                 }
//             }, 'json');
//         }
//     }

    function get_user(status){
        var temp = '';
        var no = 1;
        var flag = 0;
        $.post(site_url + 'licensing/data_petugas', {status: status}, function (data) {
            // $('#jml_baru').attr('data-to', data.jml);
            $.each(data.data, function(index, value){
                if (value.usr_aktivasi == null) {
                    value.usr_aktivasi = '-';
                }
                if (value.ket == null) {
                    value.ket = '-';
                }
                temp += '<tr><td >'+no+'</td>';
                temp += '<td ><p>'+value.nm_perusahaan+'</p></td>';
                temp += '<td ><p>'+value.almt_perusahaan+'</p></td>';
                temp += '<td ><p>'+value.created+'</p></td></a>';
                temp += '<td ><p>'+value.tgl_aktivasi+'</p></td>';
                temp += '<td ><p>'+value.usr_aktivasi+'</p></td>';
                temp += '<td ><p>'+value.ket+'</p></td></tr>';
                no++;
            });
            $('#res_admin').html(temp);
            $('#admin_table').fadeIn();
            // view = 1;
            // if (jns == 'baru') {
            //     $('#last_td').html('Tanggal Kirim');
            // }else if (jns == 'verif') {
            //     $('#last_td').html('Tanggal Verifikasi');
            // }else if (jns == 'tolak') {
            //     $('#last_td').html('Tanggal Penolakan');
            // }
        }, 'json');
    }

// function showlog(l) {
//     var $this = $(l);
//     if ($this.attr("data-url")) {
//         $.get($this.attr("data-url"), function (hasil) {
//             if (hasil) {
//                 $('#res_log').html(hasil);
//                 $('#logmodal').modal('show');
//             }
//         });
//     }
//     return false;
// }

</script>
<script>
//                            $(document).ready(function (e) {
//                                var site_url = '<?php echo site_url(); ?>';
//                                if (<?= $this->newsession->userdata('role'); ?> == '05') {
//                                    $.post(site_url + 'licensing/check_tdp', function (data) {
//                                        var res = data;
//                                        if (res.status == '0') {
//                                            if (res.hari > 0) {
//                                                BootstrapDialog.show({
//                                                title: '',
//                                                type: BootstrapDialog.TYPE_WARNING,
//                                                message: 'Masa Berlaku TDP Akan Berakhir '+res.hari+' Hari Lagi, Harap Memperbaharui Data Perusahaan, klik <a href="'+site_url+'/setting/edit_per">Edit Profile Perusahaan</a>'
//                                                });
//                                            }
//                                            if (res.hari <= 0) {
//                                                BootstrapDialog.show({
//                                                title: '',
//                                                type: BootstrapDialog.TYPE_WARNING,
//                                                message: 'Masa Berlaku TDP Telah Berakhir, Harap Memperbaharui Data Perusahaan klik <a href="'+site_url+'/setting/edit_per">Edit Profile Perusahaan</a>'
//                                                });   
//                                            }
//                                            if (res.tdk_val == '1') {
//                                                BootstrapDialog.show({
//                                                title: '',
//                                                type: BootstrapDialog.TYPE_WARNING,
//                                                message: 'Data Perusahaan Tidak Valid, dengan Keterangan '+res.ket+'.<br> Harap Memperbaharui Data Perusahaan klik <a href="'+site_url+'/setting/edit_per">Edit Profile Perusahaan</a>'
//                                                });   
//                                            }
//                                            if (res.tdk_val == '0'){
//                                                BootstrapDialog.show({
//                                                title: '',
//                                                type: BootstrapDialog.TYPE_WARNING,
//                                                message: 'Data Perusahaan Sedang Dalam Verifikasi, Harap Menunggu Untuk Melanjutkan Proses Permohonan'
//                                                });
//                                            }
//                                        }
//                                    },'json');
//                                }

                                $(document).ready(function (e) {
                                var site_url = '<?php echo site_url(); ?>';
                                if (<?= $this->newsession->userdata('role'); ?> == '05' && <?= $this->newsession->userdata('tipe_registrasi'); ?> == '01') {
                                    $.post(site_url + 'licensing/check_nib', function (data) {
                                        var res = data;
                                        if (res.status == '0') {
                                            
                                                BootstrapDialog.show({
                                                title: '',
                                                type: BootstrapDialog.TYPE_WARNING,
                                                message: 'User anda belum input NIB. Silahkan input NIB dimenu Pengaturan Profil Perusahaan, lalu klik edit, atau klik link <a href="'+site_url+'setting/edit_per">Edit Profile Perusahaan</a>'
                                                });
                                            
                                        }
                                    },'json');
                                }

                                $.post(site_url + 'reference/get_popup', function (data) {
                                    // console.log(data);
                                    // return false;
                                    
                                    var npwp =  'npwp';
                                    npwp = String(<?= $this->newsession->userdata('npwp'); ?>);
                                    if (data.length > 0) {
                                        for (var i = 0; i < data.length; i++) {
                                            var isi = data[i].isi;
                                            isi = isi.replace(/kode_npwp/i, npwp)
                                            BootstrapDialog.show({
                                                title: '',
                                                type: BootstrapDialog.TYPE_WARNING,
                                                message: isi
                                            });
                                        }
                                    }
                                }, 'json');
                            });
                        </script>
