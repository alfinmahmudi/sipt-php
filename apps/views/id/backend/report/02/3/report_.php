<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Laporan Pengadaan dan Realisasi Peredaran Minuman Beralkohol</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewsiupmb" name="fnewsiupmb" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Periode Pelaporan</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id ="id" name="id" wajib="yes" value="<?= $sess['id']; ?>" />
            <input type="hidden" class="form-control mb-10" id="kd_izin" name="siupmb[kd_izin]" wajib="yes" value="<?= $sess['kd_izin']; ?>" />
            <input type="hidden" class="form-control mb-10" id ="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label">Triwulan<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[tipe_permohonan]', $tipe_permohonan, $sess['tipe_permohonan'], 'id="tipe_permohonan" wajib="yes" class="form-control mb-10"'); ?>
                <input type="hidden" class="form-control mb-10" id ="tipe_permohonan" name="siupmb[tipe_permohonan]" wajib="yes" value="<?= $sess['tipe_permohonan']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tahun<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
               <input type="text" class="form-control mb-10" id="no_izin_lama" name="siupmb[no_izin_lama]" wajib="yes" value="<?= $sess['no_izin_lama']; ?>" width="100%" />
              </div>
            </div>
            <div style="height:5px;"></div>
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Realisasi Pengadaan</b></header>
          <div class="panel-body">
            <div class="row">
              <label class="col-sm-3 control-label">Golongan<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[tipe_perusahaan]', $tipe_perusahaan, (array_key_exists('tipe_perusahaan', $sess) ? $sess['tipe_perusahaan'] : $this->newsession->userdata('tipe_perusahaan')), 'id="tipe_perusahaan" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jenis Minuman Beralkohol<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id="no_izin_lama" name="siupmb[no_izin_lama]" wajib="yes" value="<?= $sess['no_izin_lama']; ?>" width="100%" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Pengadaan Produksi Dalam Negeri (Liter)</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id="no_izin_lama" name="siupmb[no_izin_lama]" wajib="yes" value="<?= $sess['no_izin_lama']; ?>" width="100%" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Pengadaan Impor (Liter)<</font></label>
              <div class="col-sm-9">
                 <input type="text" class="form-control mb-10" id="no_izin_lama" name="siupmb[no_izin_lama]" wajib="yes" value="<?= $sess['no_izin_lama']; ?>" width="100%" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Asal Negara Impor<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[kdkab]', $kab, (array_key_exists('kdkab', $sess) ? $sess['kdkab'] : $this->newsession->userdata('kdkab')), 'id="kdkab" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec\');"'); ?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
	<div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Realisasi Penyaluran</b></header>
          <div class="panel-body">
            <div class="row">
              <label class="col-sm-3 control-label">Golongan<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[tipe_perusahaan]', $tipe_perusahaan, (array_key_exists('tipe_perusahaan', $sess) ? $sess['tipe_perusahaan'] : $this->newsession->userdata('tipe_perusahaan')), 'id="tipe_perusahaan" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jenis Minuman Beralkohol<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id="no_izin_lama" name="siupmb[no_izin_lama]" wajib="yes" value="<?= $sess['no_izin_lama']; ?>" width="100%" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jumlah (Liter)</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id="no_izin_lama" name="siupmb[no_izin_lama]" wajib="yes" value="<?= $sess['no_izin_lama']; ?>" width="100%" />
              </div>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-lg-12">
                <?php if($sess['id']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                     <button class="btn btn-sm btn-default addon-btn m-b-10" type="button" onclick="post('#fnewsiupmb'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-undo"></i> Kembali </button>
                  </div>
                  <div class="btn-group dropup">
					<button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewsiupmb'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Simpan </button>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left"><button class="btn btn-sm btn-info addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= site_url(); ?>licensing/view/status"><i class="fa fa-undo"></i>Batal</button></span>  -->
                <span class="pull-right"><button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewsiupmb'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button></span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/jquery.browser.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/autocomplete.js"></script>
<script>
$(document).ready(function(e){ 
  $(".datepickers").datepicker({
    autoclose: true
  });
});
$(document).ready(function(e){ 
  $("#no_izin_lama").attr("url", '<?= site_url(); ?>get/ac/get_permohonan/' + $("#direktorat").val() + '/' + $("#kd_izin").val());
  $("#no_izin_lama").autocomplete($("#no_izin_lama").attr('url'), {width: '67.75%', selectFirst: false});
  $("#no_izin_lama").result(function (event, data, formatted) {
    if (data) {
      $(this).val(data[1]);
      $.get('<?= site_url(); ?>get/ac/set_form/' + data[2] + '/' + data[3], function (hasil) {
        if(hasil){
          var arrhasil = hasil.split("|");
          $("#id_permohonan_lama").val(arrhasil[0]);
          $("#no_izin_lama").val(arrhasil[1]);
          $("#tgl_izin_lama").val(arrhasil[2]);
          $("#tgl_izin_exp_lama").val(arrhasil[3]);
          $('#npwp').attr('value', arrhasil[4]);
          $("#nm_perusahaan").val(arrhasil[5]);
          $("#almt_perusahaan").val(arrhasil[6]);
      $("#kdprop").select2('data',{id:arrhasil[7], text: arrhasil[20]});
          $("#kdpos").val(arrhasil[11]);
          $("#telp").val(arrhasil[12]);
          $("#fax").val(arrhasil[13]);
          $("#lokasi").val(arrhasil[14]);
          $("#status_milik").val(arrhasil[15]);
          $("#no_siup").val(arrhasil[16]);
          $("#tgl_siup").val(arrhasil[17]);
          $("#jenis_siup").val(arrhasil[18]);
          $('#tipe_perusahaan').val(arrhasil[19]);
          $.get('<?= site_url(); ?>get/cb/set_kota/' + arrhasil[7], function (result){
            if(result){
              $("#kdkab").html(result);
              $("#kdkab").select2('data',{id:arrhasil[8], text: arrhasil[21]});
              $.get('<?= site_url(); ?>get/cb/set_kec/' + arrhasil[8], function (result) {
                if (result) {
                  $("#kdkec").html(result);
                  $("#kdkec").select2('data',{id:arrhasil[9], text: arrhasil[22]});
                  $.get('<?= site_url(); ?>get/cb/set_kel/' + arrhasil[9], function (result) {
                    if (result) {
                      $("#kdkel").html(result);
                      $("#kdkel").select2('data',{id:arrhasil[10], text: arrhasil[23]});
                    } else {
                      $("#kdkel").html('');
                    }
                  });
                } else {
                  $("#kdkec").html('');
                }
              });
            } else {
              $("#kdkab").html('');
            }
          });
        }
      });
    }
  });
  
});
</script>