<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //print_r($_SESSION);die();?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-dialog/bootstrap-dialog.min.css" type="text/css" cache="false" />
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-dialog/bootstrap-dialog.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtable.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtablehash.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
<div class="page-head">
  <h3 class="m-b-less">  Form Pelaporan Realisasi</h3>
  <span class="sub-title">
  <?= $sess['nama_izin'];?>
  </span> </div>
<div class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Data Laporan</header>
          <div class="panel-body">
            <section class="isolate-tabs">
              <ul class="nav nav-tabs">
                <li class="active"> <a data-toggle="tab" href="#first">Data Laporan Realisasi</a> </li>
<!--                 <li class=""> <a data-toggle="tab" href="#second">Data Laporan Penyaluran</a> </li> -->
              </ul>
              <div class="panel-body">
                <div class="tab-content">
                  <div id="first" class="tab-pane active">
                    <div class="row">
                      <div class="col-lg-12" id="tb_users">
                        <?= $tblPengadaan;?>
                      </div>
                    </div>
                  </div>
                  <!-- <div id="second" class="tab-pane">
                    <div class="row">
                      <div class="col-lg-12">
                        <?= $lstPenyaluran['tblPenyaluran'];?>
                      </div>
                    </div>
                  </div> -->
                </div>
              </div>
            </section>
          </div>
        </section>
      </div>
    </div>
</div>