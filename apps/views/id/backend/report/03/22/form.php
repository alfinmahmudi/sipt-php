<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
    <link rel="stylesheet" href="<?=base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="<?=base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="<?=base_url(); ?>assets/bend/css/jquery.filer.css?<?=date('His');?>" type="text/css" cache="false" />
    <link rel="stylesheet" href="<?=base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
    <div id="main-wrapper">
        <form action="<?=$act; ?>" enctype="multipart/form-data" method="post" id="fsupportdocnew" name="fsupportdocnew" autocomplete="off" data-redirect="true" data-agreement>
            <input type="hidden" class="form-control mb-10" id="permohonan_id" name="permohonan_id" wajib="yes" value="<?=$permohonan_id; ?>" width="100%" />
            <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?=$id; ?>" width="100%" />
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading"> <b>Data Periode Pelaporan</b></header>
                        <div class="panel-body">
                        <div class="row">
                            <label class="col-sm-3 control-label">Jenis Komoditi<font size="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <?php if ($id != '') { ?>
                                    <?=form_dropdown('gula[bapok]', $jns_bapok, $sess['bapok'], 'id="bapok" wajib="yes" class="form-control mb-10"'); ?>
                                <?php }else{ ?>
                                    <?=form_dropdown('gula[bapok]', $jns_bapok, $sess['bapok'], 'id="bapok" wajib="yes" onchange="cek_sess()" class="form-control mb-10"'); ?>
                                <?php } ?>
                        	</div>
                        </div>
                        <div id="baru">
                        	<div style="height:5px;"></div>
                        	<div class="row">
                                <label class="col-sm-3 control-label">Bulan Pelaporan<font size="2" color="red">*</font></label>
                                <div class="col-sm-4">
                                	<?=form_dropdown('gula[bulan]', $bulan, $sess['bulan'], 'id="triwulan" placeholder="Bulan" wajib="yes" class="form-control mb-10"'); ?>
                                </div>
                                <div class="col-sm-5">
                                	<?=form_dropdown('gula[tahun]', $tahun, $sess['tahun'], 'id="triwulan" placeholder="Tahun" wajib="yes" class="form-control mb-10"'); ?>
                                </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                                <label class="col-sm-3 control-label">Stok Awal<span class="satuan_komoditi"></span><font size="2" color="red">*</font></label>
                                <div class="col-sm-6" id="awl">
                                    <input type="text" id="stok_awl" wajib="yes" class="form-control mb-10" onchange="cek_stock()" name="gula[stok_awl]" value="<?= $sess['stok_awl'] ?>">
                                </div>
                                <div class="col-sm-2">
                                    <p class="res_satuan"> </p>
                                </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                                <label class="col-sm-3 control-label">Stok Akhir<span class="satuan_komoditi"></span><font size="2" color="red">*</font></label>
                                <div class="col-sm-9">
                                	<p id="res"> <?= $sess['stok_akhr'] ?> </p>
                                    <input type="hidden" id="stok_akhr" class="form-control mb-10" name="gula[stok_akhr]" value="<?= $sess['stok_akhr']; ?>">
                            </div>
                            </div>
                        </div>
                        <!-- <div style="height:10px;"></div>
                        <div class="form-group">
                            <label class=" control-label">Upload Excel</label>
                            <input type="file" name="files[]" onchange="upload_beg()" id="filer_input">
                        </div>
                        <div id="field_read">
                            
                        </div> -->


                        <div class="row">
                        	<div class="col-sm-6">
		                        <div style="height:25px;"></div>
		                        <header class="panel-heading"> <b>Pengadaan</b></header>
		                        <div style="height:7px;"></div>
		                        <!-- <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="add_det('1'); return false;" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah </button> -->
		                        <div class="row">
					      			<div class="col-md-5 col-sm-5 col-xs-5 col-lg-5">
					      				<div class="form-group">
						                    <label class=" control-label">Jumlah Pengadaan<span class="satuan_komoditi"></span></label>
						                    <input class="form-control" id="jumlah">
						                    <input type="hidden" id="tipe" value="1">
					                	</div>
					      			</div>
					                <div class="col-md-1 col-sm-1 col-xs-1 col-lg-1">
					                    &nbsp;&nbsp;&nbsp;
					                    <p class="res_satuan"> </p>
					                </div>
					      			<div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                                        <div class="form-group" id="select_men">
                                            <label class=" control-label" id="ref_dar">Daerah Asal</label>
                                            <div>
                                                <select class="form-control" onchange="res_daerah(this)">
                                                    <option value="0">Dalam Negeri</option>
                                                    <option value="1">Luar Negeri</option>
                                                </select>
                                            </div>
                                            <div style="height: 10px;"></div>
                                            <div id="kab_spes">
                                                <?=form_dropdown('', $kab, '', 'id="kab" class="form-control mb-10 select2"'); ?>
                                            </div>
                                            <div id="neg_spes" style="display: none;">
                                                <?=form_dropdown('', $negara, '', 'id="negara" class="form-control mb-10 select2"'); ?>
                                            </div>
                                            <!-- <?=form_dropdown('', $kab, $sess['jns_bapok'], 'id="kab" class="form-control mb-10"'); ?> -->
                                        </div>
					      			</div>
					            </div>
					            <div class="form-group">
					                <button type="button" class="btn btn-danger" id="begin_upload" onclick="set_detil(1)">Tambah</button>
					            </div>
							      <table class="table table-striped custom-table table-hover">
							      <thead>
							      	<tr>
							        <td >&nbsp;</td>
							        <td >Jumlah Pengadaan<span class="satuan_komoditi"></span></td>
							        <td >Daerah Asal</td>
							        </tr>
							      </thead>
							      <tbody id="tipe_1">
							      <?php if (count($detil) > 0) { 
							      	foreach ($detil as $key) { 
							      		if ($key['tipe'] == 1) { $ranzom = rand(); $nama = '';
								      		if (trim($key['kab']) == '') {
								      		 	$nama = $key['prop'];
								      		 }else{
								      		 	$nama = $key['kab'];
								      		 	} ?>
							      			<tr id="tr_<?= $ranzom; ?>">
							      			<td ><button type="button" class="btn btn-danger fa fa-times" onclick="remove_det('<?= $ranzom; ?>', '<?= $key['jumlah']; ?>', '1')"></button></td>
	        								<td ><p><?= $key['jumlah']; ?></p><input type="hidden" name="dtl[jum][]" value='<?= $key['jumlah']; ?>'></td>
                                            <?php if (trim($key['kab']) == '' && trim($key['prop']) == '') { $nama = $key['ngr']; ?>
	        								   <td ><p><?= $nama; ?></p><input type="hidden" name="dtl[negara][]" value='<?= $key['negara']; ?>'><input type="hidden" name="dtl[fl_impor][]" value='<?= $key['fl_impor']; ?>'><input type="hidden" name="dtl[asal][]">
                                            <?php }else{ ?>
                                                <td ><p><?= $nama; ?></p><input type="hidden" name="dtl[asal][]" value='<?= $key['daerah']; ?>'><input type="hidden" name="dtl[negara][]"><input type="hidden" name="dtl[fl_impor][]">
                                            <?php } ?>
	        								<input type="hidden" name="dtl[tipe][]" value='<?= $key['tipe']; ?>'></td>
	        								</tr>
							      <?php } } } ?>
							      </tbody>
							      </table>
						    </div>

						    <div class="col-sm-6">
							    <div style="height:25px;"></div>
		                        <header class="panel-heading"> <b>Penyaluran</b></header>
		                        <div style="height:7px;"></div>
		                        <!-- <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="add_det('2'); return false;" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah </button> -->
		                        <div class="row">
					      			<div class="col-md-5 col-sm-5 col-xs-5 col-lg-5">
					      				<div class="form-group">
						                    <label class=" control-label">Jumlah Penyaluran<span class="satuan_komoditi"></span></label>
						                    <input class="form-control" id="jumlah_pen">
						                    <input type="hidden" id="tipe_pen">
					                	</div>
					      			</div>
					                <div class="col-md-1 col-sm-1 col-xs-1 col-lg-1">
					                    &nbsp;&nbsp;&nbsp;
					                    <p class="res_satuan"> </p>
					                </div>
					      			<div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
					      				<div class="form-group">
						                    <label class=" control-label" id="ref_dar">Daerah Tujuan</label>
					                        <div id="kab_spes">
					                            <?=form_dropdown('', $kab_pem, '', 'id="kab_pen" class="form-control mb-10 select2"'); ?>
					                        </div>
						                    <!-- <?=form_dropdown('', $kab, $sess['jns_bapok'], 'id="kab" class="form-control mb-10"'); ?> -->
					                	</div>
					      			</div>
					            </div>
                                <div style="height: 44px;"></div>
					            <div class="form-group">
					                <button type="button" class="btn btn-danger" id="begin_upload" onclick="set_detil(2)">Tambah</button>
					            </div>
							      <table class="table table-striped custom-table table-hover">
							      <thead>
							      	<tr>
							        <td >&nbsp;</td>
							        <td >Jumlah Penyaluran<span class="satuan_komoditi"></span></td>
							        <td >Daerah Tujuan</td>
							        </tr>
							      </thead>
							      <tbody id="tipe_2">
							      	<?php if (count($detil) > 0) { 
							      	foreach ($detil as $key) { 
							      		if ($key['tipe'] == 2) { $ranzom = rand(); $nama = '';
								      		if (trim($key['kab']) == '') {
								      		 	$nama = $key['prop'];
								      		 }else{
								      		 	$nama = $key['kab'];
								      		 	}?>
							      			<tr id="tr_<?= $ranzom; ?>">
							      			<td ><button type="button" class="btn btn-danger fa fa-times" onclick="remove_det('<?= $ranzom; ?>', '<?= $key['jumlah']; ?>', '2')"></button></td>
	        								<td ><p><?= $key['jumlah']; ?></p><input type="hidden" name="dtl[jum][]" value='<?= $key['jumlah']; ?>'></td>
	        								<td ><p><?= $nama; ?></p><input type="hidden" name="dtl[asal][]" value='<?= $key['daerah']; ?>'><input type="hidden" name="dtl[negara][]"><input type="hidden" name="dtl[fl_impor][]">
	        								<input type="hidden" name="dtl[tipe][]" value='<?= $key['tipe']; ?>'></td>
	        								</tr>
							      <?php } } } ?>
							      </tbody>
							      </table>
						    </div>
						</div>

<!-- Modal Start -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">&nbsp;</h4>
      </div>
      <div class="modal-body">
      		<div class="row">
      			<div class="col-md-5 col-sm-5 col-xs-5 col-lg-5">
      				<div class="form-group">
	                    <label class=" control-label">Jumlah Stock</label>
	                    <input class="form-control" wajib="yes" id="jumlah">
	                    <input type="hidden" id="tipe">
                	</div>
      			</div>
                <div class="col-md-1 col-sm-1 col-xs-1 col-lg-1">
                    &nbsp;&nbsp;&nbsp;
                    <p class="res_satuan"> </p>
                </div>
      			<div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
      				<div class="form-group">
	                    <label class=" control-label" id="ref_dar"></label>
                        <div id="kab_spes">
                            
                        </div>
	                    <!-- <?=form_dropdown('', $kab, $sess['jns_bapok'], 'id="kab" wajib="yes" class="form-control mb-10"'); ?> -->
                	</div>
      			</div>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-danger" id="begin_upload" onclick="set_detil()">Simpan</button>
            </div>
        </div>
      </div>
    </div>
</div>

                            <div style="height:25px;"></div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="checkbox-custom inline check-success">
                                      <input value="1" id="checkbox-agreement" wajib='yes' type="checkbox" check-agreement="yes"> 
                                      <label for="checkbox-agreement">
                                          <p>Apakah Anda Yakin Data Yang Di Input Sudah Benar Dan Sesuai ?</p>
                                      </label>
                                    </label>
                                    <div class="btn-toolbar" role="toolbar">
                                        <div class="btn-group" role="group">
                                            <button class="btn btn-sm btn-default addon-btn m-b-10" onclick="closedialog(); return false;"><i class="fa fa-undo"></i> Kembali </button>
                                        </div>
                                        <div class="btn-group dropup">
                                            <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fsupportdocnew'); return false;" value="Simpan Data" data-modal="true"><i class="fa fa-check"></i> Kirim </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </form>
    </div>
    <script src="<?=base_url(); ?>assets/bend/js/sipt.js?v=<?=date(" Ymdhis "); ?>"></script>
    <script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?=base_url(); ?>assets/bend/js/select2.js"></script>
    <script src="<?=base_url(); ?>assets/bend/js/select2-init.js"></script>
    <script src="<?=base_url();?>assets/bend/js/a.js?<?=date('His');?>"></script>
    <script src="<?=base_url();?>assets/bend/js/jquery.filer.min.js?<?=date('His');?>"></script>
    <script src="<?=base_url();?>assets/bend/js/custom.js?<?=date('His');?>"></script>
    <script>
    	var site_url = '<?= site_url(); ?>';
    	var temp_awal = 0;
    	var flags = 0;
    	var temp_rem = new Array();
        $('.jFiler-item-trash-action').click(function(a){
            $('#field_read').html('');
        });
        $(document).ready(function(e) {
        	temp_awal = $('#stok_awl').val();
            $(".datepickers").datepicker({
                autoclose: true
            });
            <?php if ($id != '') { ?>
                var permohonan_id = '<?= $permohonan_id; ?>';
                $.post(site_url+'pelaporan/val_edit/ajax', {permohonan_id: permohonan_id, bapok: $('#bapok').val()}, function(data){
                    if ($.trim(data) == 'TRUE') {
                        cek_sess();
                    }
                });
            <?php } ?>
            $(".chkedit").change(function(e) {
                var $this = $(this);
                var $div = $this.parent().parent();
                var $input = $this.parent().parent().children().first();
                var $before = $input.val();
                if ($this.is(":checked")) {
                    $('#filepo').show();
                } else {
                    $('#filepo').hide();
                }
                return false;
            });
        });

        function res_daerah(a) {
            var sel = $(a).val();
            if (sel == 0) {
                $('#kab_spes').show();
                $('#neg_spes').hide();
                $('#negara').select2("val", "");
            }else{
                $('#neg_spes').show();
                $('#kab_spes').hide();
                $('#kab').select2("val", "");
            }
        }

		function cek_sess() {
                    if($('#bapok').val() == "6"){
                        $('.satuan_komoditi').html('(Liter)');
                    }else{
                        $('.satuan_komoditi').html('(Ton)');
                    }
            var permohonan_id = '<?= $permohonan_id; ?>';
            var lap_id = '<?= $id ?>';
            if (flags != 1) {
            	$.post(site_url+'pelaporan/cek_sess/ajax', {permohonan_id: permohonan_id, bapok: $('#bapok').val(), id: lap_id}, function(data){
	            	if (Object.keys(data.res).length > 0) {
	            		var next_month = 0;
		            	var next_year = 0;
		            	if (data.res.bulan != 12) {
		            		next_month = parseInt(data.res.bulan) + 1;
		            	}else{
		            		next_month = 1;
		            	}
		            	if (next_month == '1') {
                            next_year = parseInt(data.res.tahun) + 1;
		            	}else{
		            		next_year = data.res.tahun;
		            	}
		            	var temp = '';
		            	temp += '<div style="height:5px;"></div>';
		                temp += '<div class="row">';
		                temp += '<label class="col-sm-3 control-label">Tanggal Pelaporan<font size="2" color="red">*</font></label>';
		                temp += '<div class="col-sm-4">';
		               	temp += '<p> '+data.res.ref_bulan+' </p> <input type="hidden" id="bulan" class="form-control mb-10" name="gula[bulan]" value="'+next_month+'">';
		                temp += '</div>';
		                temp += '<div class="col-sm-5">';
		                temp += '<p> '+next_year+' </p> <input type="hidden" id="tahun" class="form-control mb-10" name="gula[tahun]" value="'+next_year+'">';
		                temp += '</div>';
		                temp += '</div>';
		                temp += '<div style="height:5px;"></div>';
		                temp += '<div class="row">';
		                temp += '<label class="col-sm-3 control-label">Stok Awal<font size="2" color="red">*</font></label>';
		                temp += '<div class="col-sm-9">';
		                temp += '<p> '+data.res.stok_akhr+' </p> <input type="hidden" id="stok_awl" class="form-control mb-10" name="gula[stok_awl]" value="'+data.res.stok_akhr+'">';
		                temp += '</div>';
		                temp += '</div>';
		                temp += '<div style="height:5px;"></div>';
		                temp += '<div class="row">';
		                temp += '<label class="col-sm-3 control-label">Stok Akhir<font size="2" color="red">*</font></label>';
		                temp += '<div class="col-sm-9">';
		                temp += '<p id="res">  </p> <input type="hidden" id="stok_akhr" class="form-control mb-10" name="gula[stok_akhr]" value="'+data.res.stok_akhr+'">';            
		                temp += '</div>';
		                temp += '</div>';

		                $('#baru').html(temp);
		                flags = 1;
	            	}else{
                        $('.res_satuan').html(data.satuan);
                    }
	            }, 'json');
            }else{
            	location.reload();
            }
        }      

        function closedialog() {
            $(".close").click();
            return false;
        };

		function add_det(tipe) {
            $("#tipe").val(tipe);
            $('#kab_spes').html('')
            if (tipe == 1) {
                $('#ref_dar').html("Daerah Asal"); 
                var sel_1 = '';
                sel_1 += '<select id="kab" wajib="yes" class="form-control mb-10">';
                <?php foreach ($kab as $key_1) { ?>
                    sel_1 += '<option value="<?=  $key_1['id'];?>"><?=  $key_1['nama'];?></option>'; 
                <?php } ?>
                sel_1 += '</select>';
                $('#kab_spes').html(sel_1);
            }else{
                $('#ref_dar').html("Daerah Tujuan");
                var sel_1 = '';
                sel_1 += '<select id="kab" wajib="yes" class="form-control mb-10">';
                <?php foreach ($kab_pem as $key_1) { ?>
                    sel_1 += '<option value="<?=  $key_1['id'];?>"><?=  $key_1['nama'];?></option>'; 
                <?php } ?>
                sel_1 += '</select>';
                $('#kab_spes').html(sel_1);
            }
            
            return false;
        };     

        function cek_stock() {
        	var res = 0;
	    	var stok_akhr = 0;
	    	var jumlah = 0;
        	var jumlah = $('#jumlah').val();
        	var tipe = $('#tipe').val();
        	var kab = $('#kab').val();
        	var isi = $("#kab option:selected" ).text();
        	var stok_awl = $('#stok_awl').val();
        	var stok_akhr = $('#stok_akhr').val();

        	if (stok_akhr != '') {
        		stok_akhr = parseFloat(stok_akhr) - parseFloat(temp_awal);
        		// stok_akhr = Math.abs(stok_akhr);
        		var res = parseFloat(stok_awl) + parseFloat(stok_akhr);
        		if (parseFloat(res) < 0) {
	                BootstrapDialog.show({
	                title: '',
	                type: BootstrapDialog.TYPE_WARNING,
	                message: 'Hasil Stok Akhir Tidak Diperbolehkan Minus'
	                });
	                $('#stok_awl').val(temp_awal);
	                return false;
	            }else{
		        	$('#stok_akhr').val(res);
		        	$('#res').html(res);
		        }
        	}else{
        		temp_awal = stok_awl;
                $('#stok_akhr').val(stok_awl);
                $('#res').html(stok_awl);
        	}
        }   

        function remove_det(tr, jumlah, tipe){
        	var stok_akhr = $('#stok_akhr').val();
        	if (tipe == 1) {
        		stok_akhr = parseFloat(stok_akhr) - parseFloat(jumlah);
        	}else if (tipe == 2) {
        		stok_akhr = parseFloat(stok_akhr) + parseFloat(jumlah);
        	}
        	// console.log(stok_akhr);
        	if (parseFloat(stok_akhr) < 0) {
                BootstrapDialog.show({
                title: '',
                type: BootstrapDialog.TYPE_WARNING,
                message: 'Hasil Stok Akhir Tidak Diperbolehkan Minus'
                });
                return false;
            }else{
	        	$('#res').html(stok_akhr);
	        	$('#stok_akhr').val(stok_akhr);
	        	$('#tr_'+tr).remove();
	        }
        }

        function set_detil(tipe) {
        	var res = 0;
	    	var stok_akhr = 0;
	    	var jumlah = 0;
	    	if (tipe == 1) {
	    		var jumlah = $('#jumlah').val();
	        	// var tipe = $('#tipe').val();
	        	var kab = $('#kab').val();
                var negara = $('#negara').val();
	        	var isi = $("#kab option:selected" ).text();
                var isi_neg = $("#negara option:selected" ).text();
	    	}else{
	    		var jumlah = $('#jumlah_pen').val();
	        	// var tipe = $('#tipe_pen').val();
	        	var kab = $('#kab_pen').val();
	        	var isi = $("#kab_pen option:selected" ).text();
	    	}
	    	jumlah = jumlah.replace(',','.');
	    	var tampung = jumlah.split('.');
	    	//console.log(tampung[1]);return false;
	    	if (tampung.length <= 2) {
	    		for (var i = 0; i < tampung.length; i++) {
	    			if (!$.isNumeric(tampung[i])) {
	    				BootstrapDialog.show({
	                    title: '',
	                    type: BootstrapDialog.TYPE_WARNING,
	                    message: 'Inputan Jumlah Harus Angka'
	                    });
	                    return false;
	    			}
	    		}
	    	}else{
	    		BootstrapDialog.show({
                    title: '',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: 'Inputan Jumlah Tidak Sesuai'
                    });
                    return false;
	    	}
	    	
            if ($.isNumeric(kab)) {
                if (jumlah == '' || kab == '') {
                    BootstrapDialog.show({
                    title: '',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: 'Jumlah Atau Daerah Asal Tidak Boleh Kosong'
                    });
                    return false;
                }
            }else{
                if (jumlah == '' || negara == '') {
                    BootstrapDialog.show({
                    title: '',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: 'Jumlah Atau Daerah Asal Tidak Boleh Kosong'
                    });
                    return false;
                }
            }
        	var stok_awl = $('#stok_awl').val();
        	var stok_akhr = $('#stok_akhr').val();
        	if (stok_awl == '') {
        		stok_awl = 0;
        	}
            if (jumlah == '') {
                jumlah = 0;
            }
            var kurang = '';

        	var rand_remove = Math.floor((Math.random() * 1000) + 1);
	        var temp = '';
	        temp += '<tr id="tr_'+rand_remove+'">';
	        temp += '<td ><button type="button" class="btn btn-danger fa fa-times" onclick="remove_det('+rand_remove+', '+jumlah+', '+tipe+')"></button></td>';
	        temp += '<td ><p>'+jumlah+'</p><input type="hidden" name="dtl[jum][]" value='+jumlah+'></td>';
            if ($.isNumeric(kab)) {
	           temp += '<td ><p>'+isi+'</p><input type="hidden" name="dtl[asal][]" value='+kab+'><input type="hidden" name="dtl[negara][]"><input type="hidden" name="dtl[tipe][]" value='+tipe+'><input type="hidden" name="dtl[fl_impor][]"></td>';
            }else{
                temp += '<td ><p>'+isi_neg+'</p><input type="hidden" name="dtl[negara][]" value='+negara+'><input type="hidden" name="dtl[tipe][]" value='+tipe+'><input type="hidden" name="dtl[fl_impor][]" value="1"><input type="hidden" name="dtl[asal][]"></td>';
            }
	        temp += '</tr>';
	        
	        if (tipe == 1) {
	        	if (stok_akhr != '') {
	        		var res = parseFloat(jumlah) + parseFloat(stok_akhr);
		        	$('#stok_akhr').val(res);
		        	$('#res').html(res);
	        	}else{
	        		var res = parseFloat(jumlah) + parseFloat(stok_awl);
		        	$('#stok_akhr').val(res);
		        	$('#res').html(res);
	        	}	
	        }else{
	        	if (stok_akhr != '') {
	        		var res = parseFloat(stok_akhr) - parseFloat(jumlah);
                    if (parseFloat(res) < 0) {
                        BootstrapDialog.show({
                        title: '',
                        type: BootstrapDialog.TYPE_WARNING,
                        message: 'Penyaluran anda tidak boleh melebihi dari stock yang anda miliki'
                        });
                        kurang = 1;
                    }else{
                        $('#stok_akhr').val(res);
                        $('#res').html(res);
                    }
	        	}else{
	        		var res = parseFloat(stok_awl) - parseFloat(jumlah);
		        	$('#stok_akhr').val(res);
		        	$('#res').html(res);
	        	}
	        }
            if (kurang != 1) {
                $('#tipe_'+tipe).append(temp);   
            }
            
            if (tipe == 1) {
            	$('#jumlah').val('');
	        	$('#tipe').val('');
	        	$('#kab').select2("val", "");
            }else{
            	$('#jumlah_pen').val('');
	        	$('#tipe_pen').val('');
	        	$('#kab_pen').select2("val", "");
            }
	        $(".close").click();
	        temp_rem.push(rand_remove);
            return false;
        };

        function upload_beg(){
            var ar = new Array();
            var data = new FormData();
            var fp = $("#filer_input").get(0).files[0];
            data.append('file_upload', fp);
            $.ajax({
                type: "POST",
                url: site_url+'pelaporan/begin_upload/ajax',
                data: data,
                dataType: 'json',
                processData: false,
                contentType: false,
                success : function(result){
                    // console.log(result);
                    if (result.err_status == '1') {
                        var realConfirm=window.confirm;
                        window.confirm=function(){
                          window.confirm=realConfirm;
                          return true;
                        };
                        $('.jFiler-item-trash-action').click();
                        $('#field_read').html(result.err_desc);
                    }else{
                        $('#field_read').html(result.err_desc);   
                    }
                }
            });
        }

        function read_excel(path) {
            var stok_akhr = $('#stok_akhr').val();
            $.post(site_url+'pelaporan/read_excel/ajax', {path: path}, function(data){
                if (data.tipe1 != '') {
                    $('#tipe_1').append(data.tipe1);
                }
                if (data.tipe2 != '') {
                    $('#tipe_2').append(data.tipe2);
                }
                if (data.resjum != '') {
                    if (stok_akhr == '') {
                        $('#res').html(data.resjum);
                        $('#stok_akhr').val(data.resjum);
                    }else{
                        var res = parseFloat(stok_akhr) + parseFloat(data.resjum);
                        $('#stok_akhr').val(res);
                        $('#res').html(res);
                    }
                }
                // console.log(data);
            }, 'json');
        }
        
//        function satuan_komoditi(this){
//            alert(this);
//        }
    </script>
<!--     else{
	            		var temp = '';
		            	temp += '<div style="height:5px;"></div>';
		                temp += '<div class="row">';
		                temp += '<label class="col-sm-3 control-label">Tanggal Pelaporan<font size="2" color="red">*</font></label>';
		                temp += '<div class="col-sm-4">';
		                temp += '<select id="triwulan" wajib="yes" class="form-control mb-10">';
						<?php foreach ($bulan as $key => $value) { ?>
							temp += '<option value="<?= $key; ?>"><?= $key; ?></option>';
						<?php } ?>
						temp += '</select>';
		                temp += '</div>';
		                temp += '<div class="col-sm-5">';
		                temp += '<select id="triwulan" wajib="yes" class="form-control mb-10">';
						<?php foreach ($tahun as $key => $value) { ?>
							temp += '<option value="<?= $key; ?>"><?= $key; ?></option>';
						<?php } ?>
						temp += '</select>';
		                temp += '</div>';
		                temp += '</div>';
		                temp += '<div style="height:5px;"></div>';
		                temp += '<div class="row">';
		                temp += '<label class="col-sm-3 control-label">Stok Awal<font size="2" color="red">*</font></label>';
		                temp += '<div class="col-sm-9">';
		                temp += '<input type="text" id="stok_awl" wajib="yes" class="form-control mb-10" onchange="cek_stock()" name="gula[stok_awl]" value="<?= $sess['stok_awl'] ?>">';
		                temp += '</div>';
		                temp += '</div>';
		                temp += '<div style="height:5px;"></div>';
		                temp += '<div class="row">';
		                temp += '<label class="col-sm-3 control-label">Stok Akhir<font size="2" color="red">*</font></label>';
		                temp += '<div class="col-sm-9">';
		                temp += '<p id="res"> <?= $sess['stok_akhr'] ?> </p>';
		                temp += '<input type="hidden" id="stok_akhr" class="form-control mb-10" name="gula[stok_akhr]" value="<?= $sess['stok_akhr']; ?>">';       
		                temp += '</div>';
		                temp += '</div>';

		                $('#baru').html(temp);
	            	} -->