<?php
error_reporting(0);
define('FPDF_FONTPATH', 'font/');
$this->load->library('fpdf/newfpdf');
$this->load->library('QRcode');
class pdf {
    var $kertas;
    var $orientasi;
    var $ukuran;
    var $ukuranhuruf;
    var $ukuranhurufdetil;
    var $ukuranhurufkop;
    var $huruf;
    var $def_margin = array(15, 40, 20, 15);
    function pdf($orientasi, $ukuran, $kertas, $margins, $data) {
        $this->ukuranhuruf = 12;
        $this->ukuranhurufdetil = 10;
        $this->ukuranhurufkop = 14;
        $this->orientasi = $orientasi;
        $this->ukuran = $ukuran;
        $this->kertas = $kertas;
        $this->huruf = 'Arial';
        $this->qrcode = new QRcode();
        $this->pdf = new NEWFPDF($this->orientasi, $this->ukuran, $this->kertas);
        $this->pdf->footer = 1;
        if (is_array($margins) and count($margins) == 4) {
            $this->pdf->SetMargins($margins[0], $margins[1], $margins[2], $margins[3]);
        } else {
            $this->pdf->SetMargins($this->def_margin[0], $this->def_margin[1], $this->def_margin[2], $this->def_margin[3]);
        }
        $this->data($data, $margins);
    }
    function Footer($arrdata) {
        if ($this->pdf->footer == 1) {
            $this->pdf->SetY(-35);
            $this->pdf->SetFont('Arial', '', 9);
            /* $Yget2 = -60;
              $this->pdf->SetY($Yget2); */
            #$this->pdf->Cell(0, 20, 'Halaman ' . $this->pdf->PageNo() . ' dari {nb}', 0, 0, 'R');
        }
    }
    /* ----------------------------------------------------------- Common Function --------------------------------------------------------- */
    function NewIkd($arrdata) {
        $this->pdf->AddPage();
        $Yget = $this->pdf->GetY();
        $this->Footer($arrdata);
        $this->pdf->SetY($Yget);
    }
    function NewIkdLandscape() {
        $this->pdf->AddPage('L', 'mm', 'Legal');
        $Yget = $this->pdf->GetY();
        $this->Footer();
        $this->pdf->SetY($Yget);
    }
    function iif($kondisi, $benar, $salah) {
        if ($kondisi)
            return $benar;
        else
            return $salah;
    }
    function convert_date($date) {
        $ret = "";
        $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $tgl = explode('-', $date);
        $tgla = (int) $tgl[1];
        $tgla = $bulan[$tgla];
        $ret = "$tgl[2] $tgla $tgl[0]";
        return $ret;
    }
    /* ------------------------------------------------------------ End Common Function ---------------------------------------------------- */
    /* ------------------------------------------------------------ ng'Batik Function ------------------------------------------------------- */
    function tulis($xpos, $ypos, $teks, $align, $lebar, $tipe, $huruf, $model, $ukuran, $garis = 0, $tinggi = 5) {
        if ($huruf != "")
            $this->pdf->SetFont($huruf, $model, $ukuran);
        if (substr($xpos, 0, 1) == "+")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        else if (substr($xpos, 0, 1) == "-")
            $this->pdf->SetX($this->pdf->GetX() + intval($xpos));
        if (substr($ypos, 0, 1) == "+")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        else if (substr($ypos, 0, 1) == "-")
            $this->pdf->SetY($this->pdf->GetY() + intval($ypos));
        if (($xpos > 0) && ($ypos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-") && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetXY($xpos, $ypos);
        else if (($xpos > 0) && (substr($xpos, 0, 1) != "+") && (substr($xpos, 0, 1) != "-"))
            $this->pdf->SetX($xpos);
        else if (($ypos > 0) && (substr($ypos, 0, 1) != "+") && (substr($ypos, 0, 1) != "-"))
            $this->pdf->SetY($ypos);
        if ($tipe == "W")
            $this->pdf->WriteFlowingBlock($teks);
        else if ($tipe == "M")
        //$this->pdf->MultiCell($lebar, 4, $teks, 0, $align, 0);
            $this->pdf->MultiCell($lebar, $tinggi, $teks, 0, $align, 0);
        else if ($tipe == "I")
            $this->pdf->Image('upL04d5' . $teks, $xpos, $ypos, 30, 40);
        else
            $this->pdf->Cell($lebar, $tinggi, $teks, $garis, 0, $align);
    }
    function SetWidths($w) {
        //Set the array of column widths
        $this->pdf->widths = $w;
    }
    function SetAligns($a) {
        //Set the array of column alignments
        $this->pdf->aligns = $a;
    }
    function Row($data, $string) {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->pdf->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h, $string);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->pdf->widths[$i];
            $a = isset($this->pdf->aligns[$i]) ? $this->pdf->aligns[$i] : 'L';
            //Save the current position
            $x = $this->pdf->GetX();
            $y = $this->pdf->GetY();
            //Draw the border
            $this->pdf->Rect($x, $y, $w, $h);
            //Print the text
            $this->pdf->MultiCell($w, 5, $data[$i], 0, $a);
            //Put the position to the right of the cell
            $this->pdf->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->pdf->Ln($h);
    }
    function CheckPageBreak($h, $string) {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->pdf->GetY() + $h + 40 > $this->pdf->PageBreakTrigger) {
            $this->pdf->AddPage($this->pdf->CurOrientation);
            $this->Footer();
            $this->qrcode->genQRcode($string, 'L');
            $this->qrcode->disableBorder();
            $this->qrcode->displayFPDF($this->pdf, $x + 165, ($this->pdf->GetY()-10), 25); #Coba diganti ganti aja 30 dan 15 nya.
//            $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 150, 35);
            $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
//             $this->tulis(10, 285, 'Dokumen ini sah, diterbitkan secara elektronik melalui sistem SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'I', 7);
        }
    }
    function NbLines($w, $txt) {
        //Computes the number of lines a MultiCell of width w will take
        $cw = &$this->pdf->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->pdf->rMargin - $this->pdf->x;
        $wmax = ($w - 2 * $this->pdf->cMargin) * 1000 / $this->pdf->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l+=$cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                } else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            } else
                $i++;
        }
        return $nl;
    }
    /* ------------------------------------------------------------ End ngBatik Function ------------------------------------------------------- */
    /* ------------------------------------------------------------ Parsing Data Function ------------------------------------------------------- */
    function data($arrdata, $margin) {
        $this->NewIkd($arrdata);
        $dflength = 192;
        $kiri = $this->def_margin[0];
        $kanan = $this->def_margin[3];
        $lebarlabelstatic = 70;
        $lebarnomor = 8;
        if (is_array($margin) and count($margin) == 4) {
            $kiri = $margin[0];
            $kanan = $margin[3];
            if ($kiri > $this->def_margin[0]) {
                $ldef = $kiri - $this->def_margin[0];
                $dflength = $dflength - $ldef;
            } else {
                $ldef = $this->def_margin[0] - $kiri;
                $dflength = $dflength + $ldef;
            }
            if ($kanan > $this->def_margin[3]) {
                $rdef = $kanan - $this->def_margin[3];
                $dflength = $dflength - $rdef;
            } else {
                $rdef = $this->def_margin[3] - $kanan;
                $dflength = $dflength + $rdef;
            }
        }
        $x = $kiri;
        $this->tulis($x, '+6', 'Pelaporan Manifes Domestik Perdagangan Antarpulau', 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);
        // $this->tulis($x, '+6', '(SIUP-MB) UNTUK IT-MB', 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);
        $this->tulis($x, '+8', $arrdata['row']['jenis_pelapor'], 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);
        $this->tulis($x, '+8', 'Nomor : ' . $arrdata['row']['no_aju'], 'C', $dflength, '', $this->huruf, 'B', $this->ukuranhuruf + 2);
        $this->pdf->Ln(8);
        $this->tulis($x, '+8', 'I.', 'L', $lebarnomor, '', $this->huruf, 'B', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'DATA PERUSAHAAN PENGIRIM', 'L', $lebarlabelstatic, '', $this->huruf, 'B', $this->ukuranhuruf);
        #Nama Perusahaan Pengirim
        $this->tulis($x, '+8', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'Nama', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['tipe_perushaan'] . '. ' . ucwords($arrdata['row']['nama']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, 'B', $this->ukuranhuruf);
        $this->tulis($x, '+8', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Alamat', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, trim($arrdata['row']['alamat']) . ', ' . $arrdata['row']['kdprop'] . ', ' . $arrdata['row']['kdkab'] . ', ' . $arrdata['row']['kdkec'] . ', ' . $arrdata['row']['kdkel'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis($x, '+3', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Fax', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['fax']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis($x, '+8', '4.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Telp', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['telp']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis($x, '+8', '5.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Email', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['email'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        #data perusahaan penerima
        $this->tulis($x, '+8', 'II.', 'L', $lebarnomor, '', $this->huruf, 'B', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'DATA PERUSAHAAN PENERIMA', 'L', $lebarlabelstatic, '', $this->huruf, 'B', $this->ukuranhuruf);
        $this->tulis($x, '+8', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Nama', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['nama_penerima']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, 'B', $this->ukuranhuruf);
        $this->tulis($x, '+8', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Alamat', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, trim($arrdata['row']['alamat_penerima']) . ', ' . $arrdata['row']['kdprop_penerima'] . ', ' . $arrdata['row']['kdkab_penerima'] . ', ' . $arrdata['row']['kdkec_penerima'] . ',' . $arrdata['row']['kdkel_penerima'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis($x, '+2', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Telp', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['telp_penerima'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis($x, '+8', '4.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Fax', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['fax_penerima'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis($x, '+8', '5.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Email', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['email_penerima'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        // asal dan tujuan
        $this->tulis($x, '+8', 'III.', 'L', $lebarnomor, '', $this->huruf, 'B', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), 0, 'DATA ASAL DAN TUJUAN BARANG', 'L', $lebarlabelstatic, '', $this->huruf, 'B', $this->ukuranhuruf);
        $this->tulis($x, '+8', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Nama Kapal', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['nama_kapal'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis($x, '+8', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Jenis Asal', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['jenis_asal']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        if ($arrdata['row']['kd_asal'] == '01') {
            $this->tulis($x, '+8', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + $lebarnomor), '0', 'Alamat Asal', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['alamat_asal'] . ', ' . ucwords($arrdata['row']['negara_asal']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf);
        } elseif ($arrdata['row']['kd_asal'] == '02') {
            $this->tulis($x, '+8', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + $lebarnomor), '0', 'Alamat Asal', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['alamat_asal'] . ', ' . ucwords($arrdata['row']['prop_asal']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf);
        }
        $this->tulis($x, '+2', '4.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Waktu Keberangkatan', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['berangkat'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis($x, '+8', '5.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Pelabuhan Muat', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['pel_asal'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis($x, '+8', '6.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Jenis Tujuan', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, ucwords($arrdata['row']['jenis_tujuan']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        if ($arrdata['row']['kd_tujuan'] == '01') {
            $this->tulis($x, '+8', '7.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + $lebarnomor), '0', 'Alamat Tujuan', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['alamat_tujuan'] . ', ' . ucwords($arrdata['row']['negara_tujuan']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf);
        } elseif ($arrdata['row']['kd_tujuan'] == '02') {
            $this->tulis($x, '+8', '7.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + $lebarnomor), '0', 'Alamat Tujuan', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
            $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['alamat_tujuan'] . ', ' . ucwords($arrdata['row']['prop_tujuan']), 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf);
        }
        $this->tulis($x, '+2', '8.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Waktu Kedatangan', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['sampai'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis($x, '+8', '9.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + $lebarnomor), '0', 'Pelabuhan Bongkar', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
        $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $arrdata['row']['pel_tujuan'], 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
        $string = utf8_decode($arrdata['row']['nama'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
        $this->qrcode->genQRcode($string, 'L');
        $this->qrcode->disableBorder();
        $this->qrcode->displayFPDF($this->pdf, $x + 150, ($this->pdf->GetY() + 20), 25); #Coba diganti ganti aja 30 dan 15 nya.
        $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
        $this->tulis(20, 340, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
        if (count($arrdata['eks']) > 0) {
            #ekspedisi
            $this->pdf->Addpage();
            $this->tulis($x, '+8', 'III.', 'L', $lebarnomor, '', $this->huruf, 'B', $this->ukuranhuruf);
            $this->tulis(($x + $lebarnomor), 0, 'DATA PERUSAHAAN EKSPEDISI', 'L', $lebarlabelstatic, '', $this->huruf, 'B', $this->ukuranhuruf);
            $no = 1;
            foreach ($arrdata['eks'] as $value) {
                if ($no == 1) {
                    $kata = 'I';
                } elseif ($no == 2) {
                    $kata = 'II';
                } elseif ($no == 3) {
                    $kata = 'III';
                } elseif ($no == 4) {
                    $kata = 'IV';
                } elseif ($no == 5) {
                    $kata = 'V';
                } elseif ($no == 6) {
                    $kata = 'VI';
                } elseif ($no == 7) {
                    $kata = 'VII';
                } elseif ($no == 8) {
                    $kata = 'VIII';
                } elseif ($no == 9) {
                    $kata = 'IX';
                } elseif ($no == 10) {
                    $kata = 'X';
                } elseif ($no == 11) {
                    $kata = 'XI';
                } elseif ($no == 12) {
                    $kata = 'XII';
                } elseif ($no == 14) {
                    $kata = 'XVI';
                } elseif ($no == 15) {
                    $kata = 'XV';
                } elseif ($no == 16) {
                    $kata = 'XVI';
                } elseif ($no == 17) {
                    $kata = 'XVII';
                } elseif ($no == 18) {
                    $kata = 'XVIII';
                } elseif ($no == 19) {
                    $kata = 'XIX';
                } elseif ($no == 20) {
                    $kata = 'XX';
                } else {
                    $kata = $no;
                }
                $this->pdf->Ln(8);
                $this->tulis(($x + $lebarnomor), 0, 'PERUSAHAAN EKSPEDISI ' . $kata, 'L', $lebarlabelstatic, '', $this->huruf, 'B', $this->ukuranhuruf);
                $this->tulis($x, '+8', '1.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + $lebarnomor), '0', 'Nama', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $value->tipe_perushaan . '. ' . $value->nama, 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, 'B', $this->ukuranhuruf);
                $this->tulis($x, '+8', '2.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + $lebarnomor), '0', 'Alamat', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, trim($value->alamat) . ', ' . $value->kdprop . ', ' . $value->kdkab . ', ' . $value->kdkec . ', ' . $value->kdkel, 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), 'M', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis($x, '+3', '3.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + $lebarnomor), '0', 'Telp', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $value->telp, 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis($x, '+8', '4.', 'L', $lebarnomor, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + $lebarnomor), '0', 'Email', 'L', $lebarlabelstatic, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + ($lebarlabelstatic + $lebarnomor)), 0, ':', 'L', 5, '', $this->huruf, '', $this->ukuranhuruf);
                $this->tulis(($x + ($lebarlabelstatic + $lebarnomor + 5)), 0, $value->email, 'J', ($dflength - ($lebarlabelstatic + $lebarnomor + 5)), '', $this->huruf, '', $this->ukuranhuruf);
                $this->pdf->Ln(8);
                $no++;
            }
            $string = utf8_decode($arrdata['row']['nama'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
            $this->qrcode->genQRcode($string, 'L');
            $this->qrcode->disableBorder();
            $this->qrcode->displayFPDF($this->pdf, $x + 150, ($this->pdf->GetY() + 20), 25); #Coba diganti ganti aja 30 dan 15 nya.
            $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
            $this->tulis(20, 340, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
        }
        #komoditi
        if (count($arrdata['detil']) > 0) {
            $this->pdf->Addpage();
//            $this->tulis(20, 340, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
            $b = 14;
            // if ($no > 14) {
            // for ($a=0; $a < $banyakEvent; $a++) {
            $terusan = $banyakEvent - 14;
            // $this->pdf->AddPage();
            // $this->pdf->SetFont('Arial', '', 10);
            // $this->tulis($x, '+0', 'Lampiran Surat No', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
            // $this->tulis($x + 35, '+0', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
            // $this->tulis($x + 40, '+0', $arrdata['row']['no_izin'], 'L', $dflength, '', 'Arial', '', 10);
            // $this->tulis($x, '+5', 'Tanggal', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
            // $this->tulis($x + 35, '+0', ':', 'L', ($dflength - $lebarstaticpendaftaran), '', 'Arial', '', 10);
            // $this->tulis($x + 40, '+0', $arrdata['row']['izin'], 'L', $dflength, '', 'Arial', '', 10);
            $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
            $this->tulis($x, '+10', 'Data Komoditi', 'C', ($dflength - $lebarstaticpendaftaran), '', 'Arial', 'B', 12);
            $this->pdf->Ln(5);
            $this->pdf->SetX(15);
            $this->SetWidths(array(10, 40, 40, 40, 20, 20));
            $this->SetAligns(array(C, C, C, C, C, C));
            $this->pdf->SetFont('Arial', 'B', 10);
            $datas = array('No', 'HS', 'Uraian', 'Nomor Container', 'Jumlah', 'Satuan');
            $this->SetAligns(array(L, L, L));
            $this->Row($datas);
            $no = 1;
            $this->pdf->SetFont('Arial', '', 10);
            // for ($i = 0; $i < $banyakEvent; $i++) {
            //     $this->pdf->SetFont('', '', '');
            //     $datas = array($no, $arrdata['event'][$i]['nama'], $arrdata['event'][$i]['organizer'], $arrdata['event'][$i]['mulai'], $arrdata['event'][$i]['berakhir']);
            //     // $this->pdf->SetX(20);
            //     $this->Row($datas, $string);
            //     $no++;
            // }
            $string = utf8_decode($arrdata['row']['nama'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
            $this->qrcode->genQRcode($string, 'L');
            $this->qrcode->disableBorder();
            $this->qrcode->displayFPDF($this->pdf, $x + 150, ($this->pdf->GetY() + 253), 25); #Coba diganti ganti aja 30 dan 15 nya.
            foreach ($arrdata['detil'] as $kom) {
                $sRepFirst = str_replace("<b>", " ", $kom['Uraian_Barang']);
                $sRepSec = str_replace("</b>", " ", $sRepFirst);
                $this->pdf->SetFont('', '', '');
                $datas = array($no, $kom['HS'], $kom['Uraian_Barang'], $kom['nama_container'], $kom['Jumlah'], $kom['Satuan']);
                // $this->pdf->SetX(20);
                $this->Row($datas, $string);
                $no++;
            }
            $string = utf8_decode($arrdata['row']['nama'] . "\n" . $this->iif($arrdata['row']['no_izin'], $arrdata['row']['no_izin'], '-') . "\n" . $arrdata['row']['tgl_izin'] . "\n" . $arrdata['link']);
//            $this->qrcode->genQRcode($string, 'L');
//            $this->qrcode->disableBorder();
//            $this->qrcode->displayFPDF($this->pdf, $x + 150, ($this->pdf->GetY() + 20), 25); #Coba diganti ganti aja 30 dan 15 nya.
//            $this->pdf->Image('assets/kopsurat.jpg', 25, 4, 120, 35);
            $this->tulis(20, 340, 'Dokumen ini sah, diterbitkan secara elektronik melalui SIPT PDN Kementerian Perdagangan sehingga tidak memerlukan cap dan tanda tangan basah', '', 25, '', 'Arial', 'IB', 7);
        }
    }
    /* ------------------------------------------------------------ End Parsing Data Function --------------------------------------------------- */
    /* ------------------------------------------------------------ Generate Function ----------------------------------------------------------- */
    function generate($arrdata) {
        if ($arrdata['cetak'] != '') {
            $namafile = $arrdata['namafile'];
            $dir = $arrdata['dir'];
            if (file_exists($dir) && is_dir($dir)) {
                $path = 'upL04d5/document/LAPAN/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
            } else {
                if (mkdir($dir, 0777, true)) {
                    if (chmod($dir, 0777)) {
                        $path = 'upL04d5/document/LAPAN/' . date("Y") . "/" . date("m") . "/" . date("d") . '/' . $namafile;
                    }
                }
            }
            $this->pdf->Output($path, 'F');
        } else {
            $this->pdf->Output();
        }
    }
    /* ------------------------------------------------------------ End Generate Function ------------------------------------------------------- */
}
?>