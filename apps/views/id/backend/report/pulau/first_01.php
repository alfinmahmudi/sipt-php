<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
    <h3 class="m-b-less"> Form Permohonan</h3>
    <span class="sub-title"><b>
            <?= $nama_izin; ?>
        </b></span> </div>
<div class="wrapper">
    <form action="<?= $act; ?>" method="post" id="fnewsiujs" name="fnewsiujs" autocomplete = "off" data-redirect="true">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"> <b>Data Perusahaan Pengirim</b></header>
                    <input type="hidden" name="id_lap" value="<?= $id; ?>">
                    <input type="hidden" id="jenis" name="jenis" value="<?= $jenis; ?>">
                    <div class="panel-body">
                        <div class="row">
                            <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" id="npwp" name="siupmb[npwp]" readonly wajib="yes" value="<?= $this->newsession->userdata('npwp'); ?>" />
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <?= form_dropdown('siupmb[tipe_perushaan]', $tipe_perusahaan, $this->newsession->userdata('tipe_perusahaan'), 'id="tipe_perusahaan" wajib="yes" class="form-control mb-10"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" id="nm_perusahaan" name="siupmb[nama]" readonly wajib="yes" value="<?= $this->newsession->userdata('nm_perusahaan'); ?>" />
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <textarea class="form-control mb-10" wajib="yes" id="almt_perusahaan" readonly name="siupmb[alamat]"><?= $this->newsession->userdata('almt_perusahaan'); ?>
                                </textarea>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <?= form_dropdown('siupmb[kdprop]', $propinsi, $this->newsession->userdata('kdprop'), 'id="kdprop" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab\'); return false;"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <?= form_dropdown('siupmb[kdkab]', $kabupaten, $this->newsession->userdata('kdkab'), 'id="kdkab" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec\');"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <?= form_dropdown('siupmb[kdkec]', $kecamatan, $this->newsession->userdata('kdkec'), 'id="kdkec" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel\');"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <?= form_dropdown('siupmb[kdkel]', $kelurahan, $this->newsession->userdata('kdkel'), 'id="kdkel" wajib="yes" class="form-control input-sm select2"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" id="email" name="siupmb[email]" value="<?= $this->newsession->userdata('email'); ?>" />
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Telp/Hp <font size ="2" color="red">*</font></label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control mb-10" wajib="yes" id="telp" wajib="yes" name="siupmb[telp]" readonly value="<?= $this->newsession->userdata('telp'); ?>" />
                            </div>
                            <label class="col-xs-2 control-label">Fax <font size ="2" color="red">*</font></label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control mb-10" wajib="yes" id="fax" wajib="yes" name="siupmb[fax]" readonly value="<?= $this->newsession->userdata('fax'); ?>" />
                            </div>
                            <!-- <label class="col-xs-2 control-label">Kode Pos</label>
                            <div class="col-sm-1">
                              <input type="text" class="form-control mb-10" id="kdpos" name="siupmb[kdpos]" readonly value="<?= $this->newsession->userdata('kdpos'); ?>" />
                            </div> -->
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"> <b>Data Perusahaan Penerima</b></header>
                    <div class="panel-body">
                        <div class="row">
                            <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" id="npwp" name="siupmb[npwp_penerima]" wajib="yes" value="<?= $sess['npwp_penerima']; ?>" />
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <?= form_dropdown('siupmb[tipe_perushaan_penerima]', $tipe_perusahaan_pen, $sess['tipe_perushaan_penerima'], 'id="tipe_perusahaan" wajib="yes" class="form-control mb-10"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" id="nm_perusahaan" value="<?= $sess['nama_penerima']; ?>" name="siupmb[nama_penerima]" wajib="yes" />
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <textarea class="form-control mb-10" wajib="yes" id="almt_perusahaan" name="siupmb[alamat_penerima]"> <?= $sess['alamat_penerima']; ?> </textarea>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <?= form_dropdown('siupmb[kdprop_penerima]', $propinsi_ter, $sess['kdprop_penerima'], 'id="kdprop_penerima" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab_penerima\'); return false;"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <?= form_dropdown('siupmb[kdkab_penerima]', $kab, $sess['kdkab_penerima'], 'id="kdkab_penerima" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec_penerima\');"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <?= form_dropdown('siupmb[kdkec_penerima]', $kec, $sess['kdkec_penerima'], 'id="kdkec_penerima" wajib="yes" class="form-control input-sm select2" data-url = "' . site_url() . 'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel_penerima\');"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <?= form_dropdown('siupmb[kdkel_penerima]', $kel, $sess['kdkel_penerima'], 'id="kdkel_penerima" wajib="yes" class="form-control input-sm select2"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" id="email" name="siupmb[email_penerima]" value="<?= $sess['email_penerima']; ?>" />
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Telp/Hp <font size ="2" color="red">*</font></label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control mb-10" wajib="yes" id="telp" name="siupmb[telp_penerima]" value="<?= $sess['telp_penerima']; ?>"/>
                            </div>
                            <label class="col-xs-2 control-label">Fax <font size ="2" color="red">*</font></label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control mb-10" wajib="yes" id="fax" name="siupmb[fax_penerima]" value="<?= $sess['fax_penerima']; ?>"/>
                            </div>
                            <!-- <label class="col-xs-2 control-label">Kode Pos</label>
                            <div class="col-sm-1">
                              <input type="text" class="form-control mb-10" id="kdpos" name="siupmb[kdpos_penerima]" />
                            </div> -->
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <label class="checkbox-custom inline check-success">
            <input value="1" id="checkbox-agreement" type="checkbox" onclick="show_eks()" > 
            <label for="checkbox-agreement">
                <p>Menggunakan Jasa Ekspedisi (Pihak Ketiga)</p>
                <p>Isikan seluruh data ekspedisi jika menggunakan jasa pengiriman lebih dari satu ekspedisi.</p>
            </label>
        </label>
            <div id="next_form">
                <?= $detil['temp']; ?>
                
            </div>
            <div class="row">
	            <div class="col-lg-12" id="tambah" style="display: none;">
	                <input id="idf" value="1" type="hidden" />
	                <span class="pull-left"><button class="btn btn-sm btn-success addon-btn m-b-10" onclick="add_eks(); return false;"><i class="fa fa-plus"></i>Tambah</button></span>
	                <input type="hidden" name="num_form" id="num_form" value="0">
	            </div>
	        </div>

        <div style="height:25px;">&nbsp;</div>
        <div class="row">
            <div class="col-lg-12">
<?php if ($sess['id']): ?>
                    <div class="btn-toolbar" role="toolbar">
                        <div class="btn-group" role="group">
                            <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewsiujs'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                        </div>
                        <div class="btn-group dropup">
                            <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>pulau/add_report/second/<?= $jenis ?>/<?= $id ?>" onclick="redirect($(this)); return false;">Data Barang</a></li>
                                <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>pulau/add_report/third/<?= $jenis ?>/<?= $id ?>" onclick="redirect($(this)); return false;">Data Komoditi</a></li>
                                <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>pulau/preview_report/<?= $id ?>" onclick="redirect($(this)); return false;">Preview Pelaporan</a></li>
                            </ul>
                        </div>
                    </div>
                <?php else: ?>
                    <!-- <span class="pull-left"><button class="btn btn-sm btn-info addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= site_url(); ?>licensing/view/status"><i class="fa fa-undo"></i>Batal</button></span>  -->
                    <span class="pull-right"><button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewsiujs'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button></span>
<?php endif; ?>
            </div>
        </div>
    </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/jquery.browser.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/autocomplete.js"></script>
<script>
var site_url = '<?= site_url(); ?>';
$(document).ready(function (e) {
    $(".datepickers").datepicker({
        autoclose: true
    });
    var edit_detil = '<?= $flag; ?>';
    if (edit_detil != '') {
        $('#checkbox-agreement').prop('checked', true);
        $('#tambah').show();
    }

    $("body").on("click", "#remove", function () {
        $(this).parents("#control-group").remove();
    });
});

function add_eks(){
    var num_form = $('#num_form').val();
    var jenis = $('#jenis').val();
    // console.log(jenis);
    // return false;
    $.post(site_url + 'pulau/add_eks', {num_form: num_form, jenis: jenis}, function(data){
        // console.log(data);
        // return false;
        $("#next_form").append(data.temp);
        // $('.select2').select2();
        if (jenis == '02' && num_form == '0') {
            num_form = 1;
        }

        num_form = parseInt(num_form) + 1;

        // $('#num_form').val(num_form);
        // var url_select2 = "<?= base_url(); ?>assets/bend/js/select2.js";
        // $.getScript(url_select2);
        // var url_init = "<?= base_url(); ?>assets/bend/js/select2-init.js";
        // $.getScript(url_init);
    }, 'json');
}

function show_eks() {
    if ($('#checkbox-agreement').is(':checked')) {
        add_eks();
        $('#tambah').show();
    } else {
        var num_form = $('#num_form').val();
        num_form = parseInt(num_form) - 1;
        $('#num_form').val(num_form);
        $('#next_form').html('');
        $('#tambah').hide();
    }
}

function remove_eks(id_form) {
	var num_form = $('#num_form').val();
    $(id_form).remove();
    // num_form = parseInt(num_form) - 1;
    // $('#num_form').val(num_form);
}

function tambahHobi() {
    var idf = document.getElementById("idf").value;
    var stre;
//                        stre = "<p id='srow" + idf + "'><input type='text' size='40' name='rincian_hobi[]' placeholder='Masukkan Hobi' /> <a href='#' style=\"color:#3399FD;\" onclick='hapusElemen(\"#srow" + idf + "\"); return false;'>Hapus</a></p>";
    stre = $("#copy").html();
    $("#divHobi").append(stre);
    idf = (idf - 1) + 2;
    document.getElementById("idf").value = idf;
}
</script>