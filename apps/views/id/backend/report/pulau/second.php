<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //print_r($sess);die();?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewpj" name="fnewpj" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Asal Barang</b></header>
          <input type="hidden" name="id_lap" value="<?= $id; ?>">
          <input type="hidden" name="jenis" value="<?= $jenis; ?>">
          <div class="panel-body">
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Asal Barang<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('pj[jenis_asal]', $asal, $sess['jenis_asal'], 'id="jenis_asal" wajib="yes" onChange="asal()" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Asal Barang<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
              <textarea name="pj[alamat_asal]" id="alamat_asal" wajib="yes"  class="form-control" id="" cols="30" rows="3"><?= $sess['alamat_asal'] ?></textarea>
              </div>
            </div>
            <div id="asal_dalam" style="display: none;">
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">Provinsi Asal Barang<font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <?= form_dropdown('pj[prop_asal]', $propinsi_asal, $sess['prop_asal'], 'id="kdprop_asal" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab_asal\'); return false;"'); ?>
                </div>
              </div>
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">Kabupaten / Kota Asal Barang<font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <?= form_dropdown('pj[kab_asal]', $kab, $sess['kab_asal'], 'id="kdkab_asal" class="form-control input-sm select2"'); ?>
                </div>
              </div>
            </div>
            <div id="asal_luar" style="display: none;">
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">Negara Asal Barang<font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <?= form_dropdown('pj[negara_asal]', $negara, $sess['negara_asal'], 'id="negara_asal" class="form-control input-sm select2"'); ?>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Tujuan Barang</b></header>
          <div class="panel-body">
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tujuan Barang<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('pj[jenis_tujuan]', $tujuan, $sess['jenis_tujuan'], 'id="jenis_tujuan" wajib="yes" onChange="tujuan()" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Tujuan Barang<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
              <textarea name="pj[alamat_tujuan]" id="alamat_tujuan" wajib="yes"  class="form-control" id="" cols="30" rows="3"><?= $sess['alamat_tujuan']?></textarea>
              </div>
            </div>
            <div id="tujuan_dalam" style="display: none;">
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">Provinsi Tujuan Barang<font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <?= form_dropdown('pj[prop_tujuan]', $propinsi_tujuan, $sess['prop_tujuan'], 'id="kdprop_tujuan" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab_tujuan\'); return false;"'); ?>
                </div>
              </div>
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">Kabupaten / Kota Tujuan Barang<font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <?= form_dropdown('pj[kab_tujuan]', $kab, $sess['kab_tujuan'], 'id="kdkab_tujuan" wajib="yes" class="form-control input-sm select2" '); ?>
                </div>
              </div>
            </div>
            <div id="tujuan_luar" style="display: none;">
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">Negara Tujuan Barang<font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <?= form_dropdown('pj[negara_tujuan]', $negara, $sess['negara_tujuan'], 'id="negara_tujuan" class="form-control input-sm select2"'); ?>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Alat Angkut</b></header>
          <div class="panel-body">
            <div class="row">
              <label class="col-sm-3 control-label">Nama Kapal <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" value="<?= $sess['nama_kapal'] ?>" id="nm_perusahaan" name="pj[nama_kapal]"/>
              </div>
            </div>
            <div style="height:5px;"></div>
            <label class="checkbox-custom inline check-success">
                <input value="1" id="checkbox-agreement" type="checkbox" onclick="show_eks()" > 
                <label for="checkbox-agreement">
                    <p>Termasuk penimbunan kayu hutan (TPK hutan, antara, industri) dan tempat penampungan terdaftar kayu (TPT KB dan KO).</p>
                </label>
            </label>
            <div class="row" id="pel">
              <label class="col-sm-3 control-label">Pelabuhan Muat <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('pj[pel_asal]', $pelabuhan_muat, $sess['pel_asal'], 'id="pelabuhan_muat" wajib="yes" class="form-control mb-10 select2"'); ?>
              </div>
            </div>
            <div class="row" id="pel_kayu">
              <label class="col-sm-3 control-label">Pelabuhan Muat <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('pj[pel_asal]', $pelabuhan_kayu, $sess['pel_asal'], 'id="pelabuhan_muat_kayu" wajib="yes" class="form-control mb-10 select2"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Waktu Keberangkatan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 datepickers" value="<?= $sess['etd'] ?>" name="pj[etd]" wajib="yes" data-date-format = "yyyy-mm-dd" />
               </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row" id="pel_kayub">
              <label class="col-sm-3 control-label">Pelabuhan Bongkar <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('pj[pel_tujuan]', $pelabuhan_kayu, $sess['pel_tujuan'], 'id="pelabuhan_bongkar_kayu" wajib="yes" class="form-control mb-10 select2"'); ?>
              </div>
            </div>
            <div class="row" id="pel_b">
              <label class="col-sm-3 control-label">Pelabuhan Bongkar <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('pj[pel_tujuan]', $pelabuhan_bongkar, $sess['pel_tujuan'], 'id="pelabuhan_bongkar" wajib="yes" class="form-control mb-10 select2"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Waktu Kedatangan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 datepickers" name="pj[eta]" value="<?= $sess['eta'] ?>" wajib="yes" data-date-format = "yyyy-mm-dd" />
               </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if($sess['alamat_tujuan']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewpj'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>pulau/add_report/first/<?= $jenis ?>/<?= $id ?>" onclick="redirect($(this)); return false;">Data Permohonan Pelaporan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>pulau/add_report/third/<?= $jenis ?>/<?= $id ?>" onclick="redirect($(this)); return false;">Data Komoditi</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>pulau/preview_report/<?= $id ?>" onclick="redirect($(this)); return false;">Preview Pelaporan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= $urifirst; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> --> 
                <span class="pull-right">
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewpj'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
                </span>
                <?php endif;?>
              </div>
            </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script><script>$(document).ready(function(e){ $(".datepickers").datepicker({autoclose: true});
  $('.nomor').keydown(function(event){
            var a = event.keyCode;
            var b = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 9, 11];
            if (jQuery.inArray(a,b) === -1) {
                event.preventDefault();
            }
        });
  });
  
$(document).ready(function (e) {
  $('#pel_kayu').hide();
  $('#pel_kayub').hide();
  $('#pelabuhan_muat_kayu').removeAttr('name');
  $('#pelabuhan_bongkar_kayu').removeAttr('name');
    var edit_detil = '<?= $flag; ?>';
    if (edit_detil != '') {
        $('#checkbox-agreement').prop('checked', true);
        show_eks();
    }
});
  
function asal(){
  var jenis_asal = $('#jenis_asal').val();
  if (jenis_asal == '01') {
    $('#asal_luar').show();
    $('#negara_asal').attr('wajib', 'yes');
    $('#asal_dalam').hide();
    $('#kdprop_asal').removeAttr('wajib', 'yes');
    $('#kdkab_asal').removeAttr('wajib', 'yes');
    // reset value
    $("#kdprop_asal").select2("val", "");
    $("#kdkab_asal").select2("val", "");
  }else{
    $('#asal_dalam').show();
    $('#kdprop_asal').attr('wajib', 'yes');
    $('#kdkab_asal').attr('wajib', 'yes');
    $('#asal_luar').hide();
    $('#negara_asal').removeAttr('wajib');
    // reset value
    $("#negara_asal").select2("val", "");
  }
}
function tujuan(){
  var jenis_tujuan = $('#jenis_tujuan').val();
  if (jenis_tujuan == '01') {
    $('#tujuan_luar').show();
    $('#negara_tujuan').attr('wajib', 'yes');
    $('#tujuan_dalam').hide();
    $('#kdprop_tujuan').removeAttr('wajib', 'yes');
    $('#kdkab_tujuan').removeAttr('wajib', 'yes');
    // reset value
    $("#kdprop_tujuan").select2("val", "");
    $("#kdkab_tujuan").select2("val", "");
  }else{
    $('#tujuan_dalam').show();
    $('#kdprop_tujuan').attr('wajib', 'yes');
    $('#kdkab_tujuan').attr('wajib', 'yes');
    $('#tujuan_luar').hide();
    $('#negara_tujuan').removeAttr('wajib');
    // reset value
    $("#negara_tujuan").select2("val", "");
  }
}

    var jenis_asal = $('#jenis_asal').val();
  if (jenis_asal == '01') {
    $('#asal_luar').show();
    $('#negara_asal').attr('wajib', 'yes');
    $('#asal_dalam').hide();
    $('#kdprop_asal').removeAttr('wajib', 'yes');
    $('#kdkab_asal').removeAttr('wajib', 'yes');
    // reset value
    $("#kdprop_asal").select2("val", "");
    $("#kdkab_asal").select2("val", "");
  }else{
    $('#asal_dalam').show();
    $('#kdprop_asal').attr('wajib', 'yes');
    $('#kdkab_asal').attr('wajib', 'yes');
    $('#asal_luar').hide();
    $('#negara_asal').removeAttr('wajib');
    // reset value
    $("#negara_asal").select2("val", "");
  }
  
  var jenis_tujuan = $('#jenis_tujuan').val();
  if (jenis_tujuan == '01') {
    $('#tujuan_luar').show();
    $('#negara_tujuan').attr('wajib', 'yes');
    $('#tujuan_dalam').hide();
    $('#kdprop_tujuan').removeAttr('wajib', 'yes');
    $('#kdkab_tujuan').removeAttr('wajib', 'yes');
    // reset value
    $("#kdprop_tujuan").select2("val", "");
    $("#kdkab_tujuan").select2("val", "");
  }else{
    $('#tujuan_dalam').show();
    $('#kdprop_tujuan').attr('wajib', 'yes');
    $('#kdkab_tujuan').attr('wajib', 'yes');
    $('#tujuan_luar').hide();
    $('#negara_tujuan').removeAttr('wajib');
    // reset value
    $("#negara_tujuan").select2("val", "");
  }
  
  function show_eks() {
    if ($('#checkbox-agreement').is(':checked')) {
        $('#pel_kayu').show();
        $('#pel').hide();
        $('#pel_kayub').show();
        $('#pel_b').hide();
        $('#pelabuhan_bongkar').removeAttr('name');
        $('#pelabuhan_muat').removeAttr('name');
        $('#pelabuhan_bongkar_kayu').attr('name', 'pj[pel_tujuan]');
        $('#pelabuhan_muat_kayu').attr('name', 'pj[pel_asal]');
    } else {
        $('#pel_kayu').hide();
        $('#pel').show();
        $('#pel_kayub').hide();
        $('#pel_b').show();
        $('#pelabuhan_bongkar_kayu').removeAttr('name');
        $('#pelabuhan_muat_kayu').removeAttr('name');
        $('#pelabuhan_bongkar').attr('name', 'pj[pel_tujuan]');
        $('#pelabuhan_muat').attr('name', 'pj[pel_asal]');
    }
}
</script>