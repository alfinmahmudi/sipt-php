<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="page-head">
    <h3 class="m-b-less"> Preview Pelaporan Perdagangan Antar Pulau</h3>
</div>
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading"> Data Pelaporan</header>
                <div class="panel-body">
                    <section class="isolate-tabs">
                        <ul class="nav nav-tabs">
                            <li class="active"> <a data-toggle="tab" href="#first">Data Pelaporan</a> </li>
                            <li class=""> <a data-toggle="tab" href="#second">Data Asal dan Tujuan</a> </li>
                            <li class=""> <a data-toggle="tab" href="#third">Data Komoditi</a> </li>
                        </ul>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="first" class="tab-pane active">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <strong><b>Data Perusahaan Pengirim</b></strong></header>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['npwp']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['tipe_perusahaan']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['nama']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['alamat']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdprop']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdkab']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdkec']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdkel']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Email</label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['email']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Telp/HP</label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['telp']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Fax</label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['fax']; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <strong><b>Data Perusahaan Penerima</b></strong></header>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['npwp_penerima']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['tipe_perusahaan_penerima']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['nama_penerima']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['alamat_penerima']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdprop_penerima']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdkab_penerima']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdkec_penerima']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdkel_penerima']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Email</label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['email_penerima']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Telp/HP</label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['telp_penerima']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Fax</label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['fax_penerima']; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>

                                    <?php if (count($ekspedisi) != 0) { 
                                        ?>
                                    <header class="panel-heading"> <strong><b>Data Perusahaan Ekspedisi</b></strong></header>
                                       <?php  foreach($ekspedisi as $dteks){
                                        ?>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">NPWP Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $dteks['npwp']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Bentuk Usaha<font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $dteks['tipe_perushaan']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $dteks['nama']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $dteks['alamat']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Propinsi Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $dteks['kdprop']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kabupaten Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $dteks['kdkab']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kecamatan Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $dteks['kdkec']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kelurahan Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $dteks['kdkel']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Telp/HP</label>
                                                        <div class="col-sm-9">
                                                            <?= $dteks['telp']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Email</label>
                                                        <div class="col-sm-9">
                                                            <?= $dteks['email']; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>

                                <?php  } 
                                } ?>
                                </div>
                                <div id="second" class="tab-pane">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <b>Data Asal Barang</b></header>
                                                <div class="panel-body">
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                      <label class="col-sm-3 control-label">Asal Barang</label>
                                                      <div class="col-sm-9">
                                                        <?= $sess['jenis_asal'] ?>
                                                      </div>
                                                    </div>
                                                    <?php if ($sess['negara_asal'] == '') { ?>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                          <label class="col-sm-3 control-label">Provinsi Asal Barang</label>
                                                          <div class="col-sm-9">
                                                            <?= $sess['prop_asal'] ?>
                                                          </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                          <label class="col-sm-3 control-label">Kabupaten / Kota Asal Barang</label>
                                                          <div class="col-sm-9">
                                                            <?= $sess['kab_asal'] ?>
                                                          </div>
                                                        </div>
                                                    <?php }else{ ?>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                          <label class="col-sm-3 control-label">Negara Asal Barang</label>
                                                          <div class="col-sm-9">
                                                            <?= $sess['negara_asal'] ?>
                                                          </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </section>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <b>Data Tujuan Barang</b></header>
                                                <div class="panel-body">
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                      <label class="col-sm-3 control-label">Jenis Tujuan Barang</label>
                                                      <div class="col-sm-9">
                                                        <?= $sess['jenis_tujuan'] ?>
                                                      </div>
                                                    </div>
                                                    <?php if ($sess['negara_tujuan'] == '') { ?>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                          <label class="col-sm-3 control-label">Provinsi Tujuan Barang</label>
                                                          <div class="col-sm-9">
                                                            <?= $sess['prop_tujuan'] ?>
                                                          </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                          <label class="col-sm-3 control-label">Kabupaten / Kota Tujuan Barang</label>
                                                          <div class="col-sm-9">
                                                            <?= $sess['kab_tujuan'] ?>
                                                          </div>
                                                        </div>
                                                    <?php }else{ ?>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                          <label class="col-sm-3 control-label">Negara Tujuan Barang</label>
                                                          <div class="col-sm-9">
                                                            <?= $sess['negara_tujuan'] ?>
                                                          </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </section>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <b>Data Alat Angkut</b></header>
                                                <div class="panel-body">
                                                    <div class="row">
                                                      <label class="col-sm-3 control-label">Nama Kapal <font size ="2" color="red">*</font></label>
                                                      <div class="col-sm-9">
                                                        <?= $sess['nama_kapal'] ?>
                                                      </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                      <label class="col-sm-3 control-label">Pelabuhan Muat <font size ="2" color="red">*</font></label>
                                                      <div class="col-sm-9">
                                                        <?= $sess['pel_asal'] ?>
                                                      </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                      <label class="col-sm-3 control-label">Waktu Keberangkatan <font size ="2" color="red">*</font></label>
                                                      <div class="col-sm-9">
                                                        <?= $sess['etd'] ?>
                                                       </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                      <label class="col-sm-3 control-label">Pelabuhan Bongkar <font size ="2" color="red">*</font></label>
                                                      <div class="col-sm-9">
                                                        <?= $sess['pel_tujuan'] ?>
                                                      </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                      <label class="col-sm-3 control-label">Waktu Kedatangan <font size ="2" color="red">*</font></label>
                                                      <div class="col-sm-9">
                                                        <?= $sess['eta'] ?>
                                                       </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div id="third" class="tab-pane">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <b>Data Komoditi</b></header>
                                                <div class="panel-body">
                                                    <div class="row col-lg-12">
                                                       <!-- <script src="<?= base_url(); ?>assets/bend/js/newtable/newtable.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
                                                        <?= $lstTngAhli; ?> -->
                                                        <table class="table table-striped custom-table table-hover">
                                                            <thead>
                                                            <tr style="background: #383838; color: white;">
                                                                <th>HS</th>
                                                                <th>Uraian Barang</th>
                                                                <th>Nomor Container</th>
                                                                <th>Jumlah</th>
                                                                <th>Satuan</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php foreach ($detil as $tng_ahli) { ?>
                                                                <tr>
                                                                  <td><?= $tng_ahli['HS'];?></td>
                                                                  <td><?= $tng_ahli['Uraian_Barang'];?></td>
                                                                  <td><?= $tng_ahli['nama_container'];?></td>
                                                                  <td><?= $tng_ahli['Jumlah'];?></td>
                                                                  <td><?= $tng_ahli['Satuan'];?></td>
                                                                </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div> 
                                </div>                   
                                <div id="fourth" class="tab-pane">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <b>Data Legalitas dan Persyaratan</b></header>
                                                <div class="panel-body">
                                                    <div class="row col-lg-12">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr class="control-label">
                                                                    <th class="control-label" width= "3%">No</th>
                                                                    <th>Nama Dokumen</th>
                                                                    <th width= "20%">Status</th>
                                                                    <th width= "5%">&nbsp;</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php if (count($req) > 0):$no = 1;
                                                                    foreach ($req as $reqs): ?>
                                                                        <tr>
                                                                            <td align="center"><?php echo $no++; ?></td>
                                                                            <td><?php echo $reqs['keterangan'];
                                                                        echo ($reqs['tipe'] == '1') ? ' <font size ="2" color="red">*</font>' : ""; ?></td>
                                                                            <td><?= $reqs['uraian'] ?></td>
                                                                            <td><center><div id="btn"> </div></center></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td nowrap="nowrap"></td>
                                                                        <td colspan="3">
                                                                            <table width="100%" class="table">
                                                                                <tr>
                                                                                    <th><center>No. Dokumen</center></th>
                                                                                <th><center>Penerbit</center></th>
                                                                                <th><center>Tgl. Dokumen</center></th>
                                                                                <th><center>Tgl. Akhir</center></th>
                                                                                <th><center>File</center></th>
                                                                    </tr>
        <?php if ($sess_syarat):for ($i = 0; $i < count($sess_syarat[$reqs['dok_id']]); $i++): ?>
                                                                            <tr>
                                                                                <td width="20%"><center><span id="no_dok_<?= $reqs['dok_id']; ?>"><?= $sess_syarat[$reqs['dok_id']][$i]['nomor']; ?></span></center></td>
                                                                            <td width="35%"><center><span id="penerbit_<?= $reqs['dok_id']; ?>"><?= $sess_syarat[$reqs['dok_id']][$i]['penerbit_dok']; ?></span></center></td>
                                                                            <td width="20%"><center><span id="tgl_dok_<?= $reqs['dok_id']; ?>"><?= date_indo($sess_syarat[$reqs['dok_id']][$i]['tgl_dok']); ?></span></center></td>
                                                                            <td width="20%"><center><span id="tgl_exp_<?= $reqs['dok_id']; ?>"><?= date_indo($sess_syarat[$reqs['dok_id']][$i]['tgl_exp']); ?></span></center></td>
                                                                            <td width="10%"><center><span id="View_<?= $reqs['dok_id']; ?>"><?= $sess_syarat[$reqs['dok_id']][$i]['View']; ?></span></center></td>
                                                                            </tr>
            <?php endfor;
        else: ?>
                                                                        <tr>
                                                                            <td colspan="5"><center><b>Tidak Terdapat Data</b></center></td>
                                                                        </tr>
                                                                    <?php endif; ?>
                                                                </table>
                                                                </td>
                                                                <tr>
                                                            <?php endforeach;
                                                        else: ?>
                                                            <tr>
                                                                <td colspan="4"><center><b>Tidak Terdapat Data</b></center></td>
                                                            </tr>
<?php endif; ?>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <form action="<?= $act; ?>" method="post" id="fpreview" name="fpreview" autocomplete = "off" data-redirect="true">
            <input type="hidden" name="data[id]" value="<?= $sess['id']; ?>">
            <input type="hidden" name="data[status]" value="<?= $sess['status']; ?>">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"> Data Proses Permohonan </header>
                    <div class="panel-body">
                        <button class="btn btn-sm btn-info addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-info pull-right"></i>Kembali</button>
                        <?php $role = $this->newsession->userdata('role');
                         if ($sess['status'] == 0000 && $role == '05') { ?>
                            <!-- <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fpreview'); return false;"><i class="fa fa-check pull-right"></i>Kirim</button> -->
                        <?php }else{ ?>
                            <button class="btn btn-sm btn-warning addon-btn m-b-10" data-url = "<?php echo site_url().'pulau/prints/'.hashids_encrypt($sess['id'], _HASHIDS_, 9); ?>" onclick="redirectblank($(this)); return false;"><i class="fa fa-print pull-right"></i>Cetak Pelaporan</button>
                            <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fpreview'); return false;"><i class="fa fa-check pull-right"></i>Kirim Ulang Pelaporan</button>
                        <?php } ?>
                        <div class="row">&nbsp;</div>
                        <div class="row">
                            <div class="col-md-12">
                                <section class="panel-timeline">
                                    <div class="time-line-wrapper">
                                        <div class="time-line-caption">
                                            <h3 class="time-line-title">Histori Permohonan <sup><a href="javascript:void(0);" id="<?= rand(); ?>" class="log-izin" data-url = "<?= $urllog; ?>" data-target="#tmplog"><span class="badge bg-info"> <?= $jmllog; ?> </span></a></sup></h3>
                                        </div>
                                        <div id="tmplog"></div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>

</div>
<script>
    function get_dialog(id_ahli, id_per, izin_id){
        var site_url = '<?php echo site_url(); ?>';
        $.post(site_url + 'licensing/popup_ahli/' + id_ahli + '/' + izin_id + '/'+ id_per, {data: id_ahli}, function (data) {
            console.log(data);
            BootstrapDialog.show({
                title: '',
                type: BootstrapDialog.TYPE_WARNING,
                message: data,
                size: BootstrapDialog.SIZE_WIDE
            });
        });
    }
</script>
<script>
                                // $(document).ready(function () {
                                //     if (<?= $this->newsession->userdata('role') ?> == '07') {
                                //         BootstrapDialog.show({
                                //             title: '',
                                //             type: BootstrapDialog.TYPE_WARNING,
                                //             message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                //         });
                                //     }
                                //     if (<?= $this->newsession->userdata('role') ?> == '06') {
                                //         BootstrapDialog.show({
                                //             title: '',
                                //             type: BootstrapDialog.TYPE_WARNING,
                                //             message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                //         });
                                //     }
                                //     if (<?= $this->newsession->userdata('role') ?> == '02') {
                                //         BootstrapDialog.show({
                                //             title: '',
                                //             type: BootstrapDialog.TYPE_WARNING,
                                //             message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                //         });
                                //     }
                                //     if (<?= $this->newsession->userdata('role') ?> == '01') {
                                //         BootstrapDialog.show({
                                //             title: '',
                                //             type: BootstrapDialog.TYPE_WARNING,
                                //             message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                //         });
                                //     }
                                // });
            </script>