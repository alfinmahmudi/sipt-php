<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<meta http-equiv="Cache-control" content="no-cache">
<link rel="stylesheet" href="<?= base_url(); ?>assets/fend/js/bootstrap-dialog/bootstrap-dialog.min.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />

<!--Data Table-->
<link href="<?= base_url(); ?>assets/bend/js/data-table/css/jquery.dataTables.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/bend/js/data-table/css/dataTables.tableTools.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/bend/js/data-table/css/dataTables.colVis.min.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/bend/js/data-table/css/dataTables.responsive.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/bend/js/data-table/css/dataTables.scroller.css" rel="stylesheet">

<div class="page-head">
    <h3 class="m-b-less"> Rekapitulasi </h3>
    <span class="sub-title">Rekapitulasi SIPT PDN</span> 
</div>
<div class="wrapper">
    <div class="row">
        <form id="divantrpl" name = "divantrpl" action = "<?= site_url(); ?>post/licensing/rekap_act/search_antarpulau" data-target = "#reqlicense" method = "post">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"> Rekap Pelaporan Antar Pulau</header>
                    <div style="height:5px;"></div>
                    <div class="panel-body">
                        <div class="row">
                            <label class="col-sm-3 control-label">Pelabuhan Muat</label>
                            <div class="col-sm-5">
                                <?= form_dropdown('antar_pulau[pel_muat]', $pel_muat, $sess['pel_muat'], 'id="pel_muat" placeholder="Pelabuhan Muat" class="form-control mb-10 select2"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Pelabuhan Bongkar</label>
                            <div class="col-sm-5">
                                <?= form_dropdown('antar_pulau[pel_bongkar]', $pel_bongkar, $sess['pel_bongkar'], 'id="pel_bongkar" placeholder="Pelabuhan Bongkar" class="form-control mb-10 select2"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <div class="col-sm-3">
                                <select id="search_by" name="search_by" wajib="yes" class="form-control mb-10 select2">
                                    <option value="1">Kode</option>
                                    <option value="2">Uraian Indonesia</option>
                                    <option value="3">Uraian English</option>
                                </select>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control mb-10" id="search_name" onsubmit="search_hs(); return false;" name="search_name">
                            </div>
                            <div class="col-sm-1">
                                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="search_hs(); return false;"><i class="fa fa-search"></i>Cari</button>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div id="res_hs">

                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Kode HS<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" id="hs" readonly="" name="antar_pulau[hs]" />
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Uraian HS<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" wajib="yes" readonly="readonly" id="hs_uraian"/>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Tanggal Muat</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control mb-10 datepickers" data-date-format = "yyyy-mm-dd" id="tg_awal2" name="antar_pulau[tgl_awal2]" name="tgl_awal2"/>
                            </div>
                            <div class="col-sm-1">
                                <span>s/d</span>
                            </div>
                            <div class="col-sm-2" style="margin-left:-25px;">
                                <input type="text" class="form-control mb-10 datepickers" data-date-format = "yyyy-mm-dd" id="tg_akhir2" name="antar_pulau[tgl_akhir2]" name="tgl_akhir"/>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Tanggal Bongkar</label>
                            <div class="col-sm-2">
                                <!-- <input type="text" id="tg_awal" class="form-control mb-10 datepickers" name="tgl_awal" wajib="yes"  data-date-format = "yyyy-mm-dd" /> -->
                                <input type="text" class="form-control mb-10 datepickers" data-date-format = "yyyy-mm-dd" id="tg_awal" name="antar_pulau[tgl_awal]" name="tgl_awal"/>
                            </div>
                            <div class="col-sm-1">
                                <span>s/d</span>
                            </div>
                            <div class="col-sm-2" style="margin-left:-25px;">
                                <!-- <input type="text" id="tg_akhir" class="form-control mb-10 datepickers" name="tgl_akhir" wajib="yes"  data-date-format = "yyyy-mm-dd" /> -->
                                <input type="text" class="form-control mb-10 datepickers" data-date-format = "yyyy-mm-dd" id="tg_akhir" name="antar_pulau[tgl_akhir]" name="tgl_akhir"/>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">NPWP Perusahaan</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control mb-10" id="npwp" name="antar_pulau[npwp]"/>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Nama Perusahaan</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control mb-10" id="nama" name="antar_pulau[nama]"/>
                            </div>
                        </div>
                        <!-- <div style="height:5px;"></div>
                        <div class="row">
                          <div class="col-sm-3">
                          <select id="search_by" name="search_by" wajib="yes" class="form-control mb-10 select2">
                            <option value="1">Kode</option>
                            <option value="2">Uraian Indonesia</option>
                            <option value="3">Uraian English</option>
                          </select>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" class="form-control mb-10" id="search_name" onsubmit="search_hs(); return false;" name="search_name">
                          </div>
                          <div class="col-sm-1">
                            <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="search_hs(); return false;"><i class="fa fa-search"></i>Cari</button>
                          </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div id="res_hs">

                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                          <label class="col-sm-3 control-label">Kode HS</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control mb-10" id="hs" readonly="" name="antar_pulau[hs]"/>
                          </div>
                        </div> -->
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Provinsi Asal Barang</label>
                            <div class="col-sm-5">
                                <?= form_dropdown('antar_pulau[prop_asal]', $propinsi, '', 'id="prop_asal" class="form-control mb-10 select2"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Provinsi Tujuan Barang</label>
                            <div class="col-sm-5">
                                <?= form_dropdown('antar_pulau[prop_tujuan]', $propinsi, '', 'id="prop_tujuan" class="form-control mb-10 select2"'); ?>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Nama Kapal</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control mb-10" id="nama" name="antar_pulau[kapal]"/>
                            </div>
                        </div>
                        <!-- <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Nama Container</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control mb-10" id="nama" name="antar_pulau[container]"/>
                            </div>
                        </div> -->
                    </div>
                    <div style="height:5px;"></div>
                </section>
                <div style="height:25px;"></div>
                <div class="row">
                    <div class="col-sm-12">
                        <button class="btn btn-sm btn-primary addon-btn m-b-10" onclick="serializediv('#divantrpl'); return false;"><i class="fa fa-search"></i>Cari</button>
                        <button type="button" class="btn btn-sm btn-success addon-btn m-b-10" onclick="excel_antrpl($(this), 'divantrpl'); return false;"><i class="fa fa-file-excel-o"></i>Export Excel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <div id="reqlicense">

                </div>                
            </section>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/fend/js/bootstrap-dialog/bootstrap-dialog.min.js" type="text/javascript"></script>

<!--Data Table-->
<script src="<?= base_url(); ?>assets/bend/js/data-table/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/data-table/js/dataTables.tableTools.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/data-table/js/bootstrap-dataTable.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/data-table/js/dataTables.colVis.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/data-table/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/data-table/js/dataTables.scroller.min.js"></script>
<!--data table init-->
<script src="<?= base_url(); ?>assets/bend/js/data-table-init.js"></script>
<script>
                            $(document).ready(function () {
                                $(".datepickers").datepicker({
                                    autoclose: true
                                });
                                $("#single-append-text").select2({
                                    placeholder: 'Nama Izin'
                                });
                            });

                            function excel(btn, p) {
                                var $this = $(p);
                                var $wajib = 0;
                                $.each($("input:visible, select:visible, textarea:visible"), function () {
                                    if ($(this).attr('wajib')) {
                                        if ($(this).attr('wajib') == "yes" && ($(this).val() == "" || $(this).val() == null)) {
                                            $(this).css('border-color', '#b94a48');
                                            $wajib++;
                                        }
                                    }
                                });
                                if ($wajib > 0) {
                                    BootstrapDialog.show({
                                        title: '',
                                        type: BootstrapDialog.TYPE_WARNING,
                                        message: '<p>Ada <b>' + $wajib + '</b> kolom yang harus diisi. <br>Silahkan periksa kembali isian form anda.</p>'
                                    });
                                    return false;
                                } else {

                                    //var form = document.forms[0];
                                    //form.method="POST";
                                    //form.action = $btn;
                                    //form.submit();
                                    var nm_izin = $("#single-append-text").val();
                                    var tg1 = $("#tg_awal").val();
                                    var tg2 = $("#tg_akhir").val();
                                    var st = $("#status").val();
                                    var kdprop = $("#kdprop").val();
                                    var kdkab = $("#kdkab").val();
                                    var kdkel = $("#kdkel").val();
                                    var kdkec = $("#kdkec").val();
                                    if (kdprop == '') {
                                        kdprop = 'null';
                                    }
                                    $btn = $(btn).attr('data-url') + '/' + nm_izin.replace("|", ".") + '/' + tg1 + '/' + tg2 + '/' + st + '/' + kdprop + '/' + kdkab + '/' + kdkec + '/' + kdkel;
                                    //window.location=$btn;
                                    //console.log($btn);return false;
                                    window.open($btn, '_blank');

                                }
                            }

                            function excel_antrpl(btn, p) {
                                var $this = $(p);
                                var $wajib = 0;
                                var form_bpk = $this.serialize();
                                document.getElementById(p).target = "_blank";
                                document.getElementById(p).action = "<?= site_url(); ?>post/post_rekap_pulau/excel";
                                document.getElementById(p).submit();
                            }

                            function search_hs() {
                                var by = $('#search_by').val();
                                var name = $('#search_name').val();

                                $.post(site_url + 'pulau/search_hs', {by: by, name: name}, function (data) {
                                    // console.log(data);
                                    // return false;
                                    $('#res_hs').html(data);
                                    var url = "<?= base_url(); ?>assets/bend/js/data-table-init.js";
                                    $.getScript(url);
                                    $('.tbl-head').remove();
                                });
                            }

                            function set_search(fl) {
                                var hs = $('#hs_' + fl).val();
                                var uraian = $('#uraiInd_' + fl).val();
                                var flagy = 0;
                                var cut = '';
                                var uraian_potong = '';
                                // console.log(uraian.substring(0, 1));
                                // return false;
                                for (var i = 0; i < uraian.length; i++) {
                                    var plus = parseInt(i) + 1;
                                    cut = uraian.substring(i, plus);
                                    if (cut == '-' || cut == ' ' || cut == '') {
                                        uraian_potong = uraian.substring(plus, uraian.length);
                                    } else {
                                        break;
                                    }
                                }
                                if (uraian_potong == '') {
                                    uraian_potong = uraian;
                                }
                                $('#res_hs').html('');
                                $('#hs').val(hs);
                                $('#hs').attr("readonly", "");
                                $('#hs_uraian').val(uraian_potong);
                            }

</script>