<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //print_r($sess);die();                   ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />

<!--Data Table-->
<link href="<?= base_url(); ?>assets/bend/js/data-table/css/jquery.dataTables.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/bend/js/data-table/css/dataTables.tableTools.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/bend/js/data-table/css/dataTables.colVis.min.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/bend/js/data-table/css/dataTables.responsive.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/bend/js/data-table/css/dataTables.scroller.css" rel="stylesheet">

<div class="page-head">
    <h3 class="m-b-less"> Form Permohonan</h3>
    <span class="sub-title"><b>
            <?= $nama_izin; ?>
        </b></span> </div>
<div class="wrapper">
    <form action="<?= $act; ?>" method="post" id="fnewpj" name="fnewpj" autocomplete = "off" data-redirect="true">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"> <b>Data Komoditi</b></header>
                    <input type="hidden" class="form-control mb-10" id="id" name="id_lap" wajib="yes" value="<?= $id_lap; ?>" />
                    <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $id; ?>" />
                    <input type="hidden" class="form-control mb-10" id="jenis" name="jenis" wajib="yes" value="<?= $jenis; ?>" />
                    <div class="panel-body">
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Kode HS<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                &nbsp;
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <div class="col-sm-3">
                                <select id="search_by" name="search_by" wajib="yes" class="form-control mb-10 select2">
                                    <option value="1">Kode</option>
                                    <option value="2">Uraian Indonesia</option>
                                    <option value="3">Uraian English</option>
                                </select>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control mb-10" id="search_name" onsubmit="search_hs(); return false;" name="search_name">
                            </div>
                            <div class="col-sm-1">
                                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="search_hs(); return false;"><i class="fa fa-search"></i>Cari</button>
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div id="res_hs">

                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Kode HS<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" id="hs" readonly="" name="pj[hs]" value="<?= $sess['hs'] ?>" />
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Uraian HS<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" wajib="yes" readonly="readonly" id="hs_uraian" name="pj[uraian]" value="<?= $sess['uraian'] ?>" />
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Uraian Barang<font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10"  id="barang_uraian" name="pj[uraian_user]" value="<?= $sess['uraian_user'] ?>" />
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Nomor Container</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" id="nama_container" name="pj[nama_container]" value="<?= $sess['nama_container'] ?>" />
                            </div>
                        </div>
                        <div style="height:5px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Jumlah<font size ="2" color="red">*</font></label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control mb-10 angka" wajib="yes" id="nm_perusahaan" name="pj[jumlah]" value="<?= $sess['jumlah'] ?>" />
                            </div>
                            <div class="col-sm-4">
                                <?= form_dropdown('pj[satuan]', $satuan, $sess['satuan'], 'id="pelabuhan_muat" wajib="yes" class="form-control mb-10 select2"'); ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <div style="height:25px;"></div>
        <div class="row">
            <div class="col-sm-12">
              <!-- <span class="pull-left">
              <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= $urifirst; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
              </span> --> 
                <span class="pull-right">
                    <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewpj'); return false;"><i class="fa fa-arrow-right"></i>Simpan</button>
                    <!-- <button type="button" class="btn btn-sm btn-success addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= $urifirst; ?>"><i class="fa fa-arrow-left"></i> Simpan </button> -->
                </span>
            </div>
        </div>
    </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!--Data Table-->
<script src="<?= base_url(); ?>assets/bend/js/data-table/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/data-table/js/dataTables.tableTools.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/data-table/js/bootstrap-dataTable.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/data-table/js/dataTables.colVis.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/data-table/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/data-table/js/dataTables.scroller.min.js"></script>
<!--data table init-->
<script src="<?= base_url(); ?>assets/bend/js/data-table-init.js"></script>

<script>
                        var site_url = '<?= site_url(); ?>';
                        $(document).ready(function (e) {
                            $(".datepickers").datepicker({autoclose: true});
                            $('.angka').keydown(function (event) {
                                var a = event.keyCode;
                                var b = [190, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 9, 11];
                                if (jQuery.inArray(a, b) === -1) {
                                    event.preventDefault();
                                }
                            });
                        });

                        function search_hs() {
                            var by = $('#search_by').val();
                            var name = $('#search_name').val();

                            $.post(site_url + 'pulau/search_hs', {by: by, name: name}, function (data) {
                                // console.log(data);
                                // return false;
                                $('#res_hs').html(data);
                                var url = "<?= base_url(); ?>assets/bend/js/data-table-init.js";
                                $.getScript(url);
                                $('.tbl-head').remove();
                            });
                        }

                        function set_search(fl) {
                            var hs = $('#hs_' + fl).val();
                            var uraian = $('#uraiInd_' + fl).val();
                            var flagy = 0;
                            var cut = '';
                            var uraian_potong = '';
                            // console.log(uraian.substring(0, 1));
                            // return false;
                            for (var i = 0; i < uraian.length; i++) {
                                var plus = parseInt(i) + 1;
                                cut = uraian.substring(i, plus);
                                if (cut == '-' || cut == ' ' || cut == '') {
                                    uraian_potong = uraian.substring(plus, uraian.length);
                                } else {
                                    break;
                                }
                            }
                            if (uraian_potong == '') {
                                uraian_potong = uraian;
                            }
                            $('#res_hs').html('');
                            $('#hs').val(hs);
                            $('#hs').attr("readonly", "");
                            $('#hs_uraian').val(uraian_potong);
                        }

</script>