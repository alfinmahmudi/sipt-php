<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<table class="table table-striped custom-table table-hover">
  <thead>
    <tr>
      <th width="65%">Dokumen Persyaratan</th>
      <th width="15%">Kategori Dokumen</th>
      <th width="5%">Syarat</th>
      <th width="10%">Upload File</th>
      <th width="10%">Status</th>
    </tr>
  </thead>
  <?php
  $jml = count($sess);
  if($jml > 0){
	  for($x = 0; $x < $jml; $x++){
	  ?>
      <tr>
        <td><?= $sess[$x]['Dokumen'] ; ?></td>
        <td><?= $sess[$x]['Kategori'] ; ?></td>
        <td><span class="label <?= $sess[$x]['tipe'] == "01" ? "label-danger" : "label-success"; ?> label-mini"><?= $sess[$x]['Syarat'] ; ?></span></td>
        <td><button type="button" class="btn btn-xs btn-default " data-url="<?= site_url(); ?>setting/popup_dok2/<?= $sess[$x]['izin_id'] ; ?>/<?= $sess[$x]['dok_id'] ; ?>/<?= $x; ?>" onclick="popuptabel($(this));" ><i class="fa fa-cloud-upload"></i> Upload</button></td>
        <td id="status_<?= $x; ?>">
            <?php $cek_dokumen = cek_dokumen($sess[$x]['dok_id'], $this->newsession->userdata('trader_id'));
              if ($cek_dokumen == 1) { ?>
                <i class='fa fa-check'></i>
             <?php } ?>
        </td>
      </tr>
	  <?php
	  }
  }else{
	  ?>
  <tr>
    <td colspan="4"><span class="label label-danger label-mini">Tidak Ditemukan Data</span></td>
  </tr>
  <?php
  }
  ?>
  <tbody>
  </tbody>
</table>
<script type="text/javascript">
function appendrow(k){
	var $this = $(k);
	var $exrow = $this.closest("tr");
	var $jmltd = $('td', $exrow).length;
	if($this.attr("data-url")){
		$('#newtr').remove();
		var $addtd = '';
		var $cls = '';
		$exrow.after('<tr id="newtr">' + $addtd + '<td id="filltd" style="font-size:12px;" colspan="' + $jmltd + '"' + $cls + '></td></tr>'); 
		$('#filltd').html('Loading..');
		$.get($this.attr("data-url"), function(data){
			$('#filltd').html(data);
			$('#newtr').removeClass("selected");
		});
				
	}
	return false;
}
</script>