<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="page-head">
  <h3 class="m-b-less"> <?= $judul ?> </h3>
  <span class="sub-title">Perizinan Perdagangan Dalam Negeri</span> </div>
<div class="wrapper">
  <div class="row">
    <form id="divpersyaratan" name = "divpersyaratan" action = "<?= $act; ?>" data-target = "#reqlicense" method = "post" choice = "<?= site_url(); ?>post/licensing/choice">
      <div class="col-lg-12">
        <section class="panel">
          <?php if ($sla == 'sla') { ?>
            <header class="panel-heading"> Pengaturan SLA </header>
          <?php } else { ?>
            <header class="panel-heading"> Pengaturan Perizinan Direktorat </header>
          <?php } ?>  
          <div class="panel-body">
		  <!--
            <div class="row">
              <label class="col-sm-3 control-label">Direktorat</label>
              <div class="col-sm-9">
                <?//= form_dropdown('direktorat', $direktorat, '', 'id="direktorat" class="form-control input-sm mb-10" data-url = "'.site_url().'get/cb/set_dokumen/" onChange = "combobox($(this), \'#dokumen\'); return false;" wajib = "yes"'); ?>
              </div>
            </div>
			-->
			<?php// print_r($izin);?>
			<div class="row">
				 <label class="col-sm-3 control-label">Dokumen Perizinan</label>
				 <div class="col-sm-9">
						<select name="dokumen" id="single-append-text" onchange="gula()" class="form-control input-sm mb-10 select2" tabindex="-1" wajib="yes">
							<option></option>
							<?php foreach($dir as $res) {?>
								<optgroup label="<?=$res['uraian'];?>">
									<?php foreach($izin[$res['kode']] as $t){?>
										<option value="<?=$res['kode']."|".$t['id'];?>"><?=$t['nama_izin'];?></option>
									<?php } ?>	
								</optgroup>
							<?php } ?>
					</select>				   
				</div>
			</div>
            <!-- <div style="height:5px;"></div> -->
            <!-- <div class="row">
              <label class="col-sm-3 control-label">Jenis Perizinan</label>
              <div id="biasa" class="col-sm-9">
                <?= form_dropdown('jenis', $jenis, '', 'id="jenis" class="form-control input-sm  mb-10" wajib="yes"'); ?>
              </div>
              <div id="gula" class="col-sm-9">
              select name="jenis" id="jenis" class="form-control input-sm  mb-10" wajib="yes">
                <option value="01">Baru</option>
                <option value="02">Perpajangan</option>
                <option value="03">Pembaharuan</option>
              </select
              ini jangan di hide tapi di hapus dan di timpah
              
              
            </div> -->
            <div style="height:20px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <!-- <button class="btn btn-sm btn-warning addon-btn m-b-10" onclick="serializediv('#divpersyaratan'); return false;"><i class="fa fa-archive"></i>Persyaratan</button>
                </span> -->
                <?php if ($sla == 'sla') { ?>
                  <button style="margin-left: 25px;" type="submit" class="btn btn-sm btn-info addon-btn m-b-10"><i class="fa fa-plus"></i>Atur SLA</button>
                <?php }elseif ($tupoksi == 'tupoksi') { ?>
                  <button style="margin-left: 25px;" type="submit" class="btn btn-sm btn-info addon-btn m-b-10"><i class="fa fa-plus"></i>Atur Tupoksi</button>
                <?php } else { ?>
                  <button style="margin-left: 25px;" type="submit" class="btn btn-sm btn-info addon-btn m-b-10"><i class="fa fa-plus"></i>Atur Persyaratan</button>
                <?php } ?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </form>
  </div>
<!--   <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading"> Persyaratan Perizinan</header>
        <div class="panel-body" id="reqlicense"></div>
      </section>
    </div>
  </div> -->
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js" type="text/javascript"></script>
<script>
  $(document).ready(function(){
    var get_izin = $('#single-append-text').val();
    var izin = get_izin.substring(3, 5);
    if (izin != '10') {
      $('#gula').hide();
    }
  });
  function gula(){
    var get_izin = $('#single-append-text').val();
    var izin = get_izin.substring(3, 5);
    //console.log(izin);
    if (izin == '10') {
      $('#biasa').fadeOut();
      $('#gula').fadeIn();
    }else{
      $('#biasa').fadeIn();
      $('#gula').fadeOut();
    }
  }
</script>
