<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="page-head">
  <h3 class="m-b-less"> Perizinan </h3>
  <span class="sub-title">Perizinan Perdagangan Dalam Negeri</span> </div>
<div class="wrapper">
  <div class="row">
    <form id="divpersyaratan" name = "divpersyaratan" action = "<?= site_url(); ?>post/licensing/requirements" data-target = "#reqlicense" method = "post" choice = "<?= site_url(); ?>post/licensing/choice">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Perizinan Direktorat </header>
          <div class="panel-body">
		  <!--
            <div class="row">
              <label class="col-sm-3 control-label">Direktorat</label>
              <div class="col-sm-9">
                <?//= form_dropdown('direktorat', $direktorat, '', 'id="direktorat" class="form-control input-sm mb-10" data-url = "'.site_url().'get/cb/set_dokumen/" onChange = "combobox($(this), \'#dokumen\'); return false;" wajib = "yes"'); ?>
              </div>
            </div>
			-->
			<?php// print_r($izin);?>
			<div class="row">
				 <label class="col-sm-3 control-label">Dokumen Perizinan</label>
				 <div class="col-sm-9">
						<select name="dokumen" id="single-append-text" onchange="gula()" class="form-control input-sm mb-10 select2" tabindex="-1" wajib="yes">
							<option></option>
							<?php foreach($dir as $res) {?>
								<optgroup label="<?=$res['uraian'];?>">
									<?php foreach($izin[$res['kode']] as $t){?>
										<option value="<?=$res['kode']."|".$t['id'];?>"><?=$t['nama_izin'];?></option>
									<?php } ?>	
								</optgroup>
							<?php } ?>
					</select>				   
				</div>
			</div>
            <div id="garansi">
              
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jenis Perizinan</label>
              <div class="col-sm-9">
                <?= form_dropdown('jenis', $jenis, '', 'id="jenis" class="form-control input-sm  mb-10" wajib="yes"'); ?>
              </div>
            </div>
            <div style="height:40px;"></div>
            <div class="row">
            <?php if ($this->newsession->userdata('role') == '99') { ?>
            <div class="col-sm-12">
              <button class="btn btn-sm btn-warning addon-btn m-b-10" onclick="serializediv('#divpersyaratan'); return false;"><i class="fa fa-archive"></i>Persyaratan</button>
              <a href="<?php echo site_url('licensing/add_persyaratan'); ?>" class="btn btn-sm btn-info addon-btn m-b-10" role="button"><i class="fa fa-plus"></i>Persyaratan Baru</a>
            </div>
            <?php }else{ ?>
              <div class="col-sm-12">
                <button class="btn btn-sm btn-warning addon-btn m-b-10" onclick="serializediv('#divpersyaratan'); return false;"><i class="fa fa-archive"></i>Persyaratan</button>
                </span>
                <button class="btn btn-sm btn-info addon-btn m-b-10" onclick="postdirect('#divpersyaratan'); return false;"><i class="fa fa-plus"></i>Permohonan Baru</button>
              </div>
             <?php } ?> 
            </div>
          </div>
        </section>
      </div>
    </form>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading"> Persyaratan Perizinan</header>
        <div class="panel-body" id="reqlicense"></div>
      </section>
    </div>
  </div>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js" type="text/javascript"></script>
<script>
  // $(document).ready(function(){
  //   var get_izin = $('#single-append-text').val();
  //   var izin = get_izin.substring(3, 5);
  // });
  function gula(){
    var get_izin = $('#single-append-text').val();
    var izin = get_izin.substring(3, 5);
    var arrgaransi = <?= json_encode($garansi); ?>;
    //alert(izin);
    if (izin == '14') {
      var temp;
      temp = '<div style="height:5px;"></div>';
      temp += '<div class="row">';
      temp += '<label class="col-sm-3 control-label">Jenis Garansi</label>';
      temp += '<div class="col-sm-9">';
      temp += '<select class="col-sm-12" name="garansi" style="height: 30px;"">';
      for (var i = 0; i < <?= count($garansi) ?>; i++) {
        temp += '<option value="'+arrgaransi[i].kode+'">'+arrgaransi[i].uraian+'</option>';
      }
      temp += '</select></div></div>';
      $('#garansi').append(temp);
    }else{
      $('#garansi').html('');
    }
  } 
</script>
<script>$(document).ready(function(e){$("#single-append-text").select2({placeholder:"Pilih Izin"});});</script>
