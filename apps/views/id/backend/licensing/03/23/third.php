<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/multiple-select.css" type="text/css" cache="false" />
<div class="page-head">
    <h3 class="m-b-less"> Form Permohonan</h3>
    <span class="sub-title"><b>
            <?= $nama_izin; ?>
        </b></span> </div>
<div class="wrapper">
    <form action="<?= $act; ?>" method="post" id="fnewnilaimodal" name="fnewnilaimodal" autocomplete = "off" data-redirect="true">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"> <b>Data Label Beras</b></header>
                    <div class="panel-body">
                        <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" value="<?= $direktorat; ?>" />
                        <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $sess['id']; ?>" />
                        <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" value="<?= $sess['kd_izin']; ?>" />
                        <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" value="<?= $sess['tipe_permohonan']; ?>" />            
                        <div style="height:10px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Merek Beras</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" name="label[merk]" id="merek">
                            </div>
                        </div>
                        <div style="height:10px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Jenis Beras</label>
                            <div class="col-sm-9">
                                <?= form_dropdown('label[kd_jenis_beras]', $kd_jenis_beras, $sess['kd_jenis_beras'], 'id="kd_jenis_beras" class="form-control select2 " onChange = "change($(this), \'#khusus\'); return false;" '); ?>
                            </div>
                        </div>

                        <div class="row" id="label_khusus">
                            <div style="height:10px;"></div>
                            <label class="col-sm-3 control-label">Jenis Beras Khusus</label>
                            <div class="col-sm-9">
                                <?= form_dropdown('label[kd_jenis_khusus]', $kd_jenis_khusus, $sess['kd_jenis_khusus'], 'id="khusus" class="form-control select2"'); ?>
                            </div>
                        </div>
                        <div style="height:10px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Keterangan Campuran</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control mb-10" name="label[campuran]" id="campuran">
                            </div>
                        </div>
                        <div style="height:10px;"></div>
                        <div class="row">
                            <label class="col-sm-3 control-label">Berat/Isi Beras</label>
                            <div class="col-sm-9">
                                <?= form_multiselect('label[kd_kemasan]', $kd_kemasan, $sess['kd_kemasan'], 'id="kd_kemasan" multiple class="form-control input-sm select2"'); ?>
                            </div>
                        </div>
                        <div style="height:25px;"></div>
                        <div class="form-group">
                            <button type="button" class="btn btn-danger" id="begin_upload" onclick="set_detil(1)">Tambah</button>
                        </div>
                        <div style="height:25px;"></div>
                        <table class="table table-striped custom-table table-hover">
                            <thead>
                                <tr>
                                    <td >&nbsp;</td>
                                    <td >Merk<span class="satuan_komoditi"></span></td>
                                    <td >Jenis Beras</td>
                                    <td >Jenis Beras Khusus</td>
                                    <td >Keterangan Campuran</td>
                                    <td >Berat Bersih</td>
                                </tr>
                            </thead>
                            <tbody id="tipe_1">
                                <?php
                                //var_dump($label);
                                if (count($label) > 0) {
                                    foreach ($label as $key) {
                                        $ranzom = rand();
                                        ?>
                                        <tr id="tr_<?= $ranzom; ?>">
                                            <td ><button type="button" class="btn btn-danger fa fa-times" onclick="remove_det('<?= $ranzom; ?>', '<?= $key['merk']; ?>', '1')"></button></td>
                                            <td ><p><?= $key['merk']; ?></p><input type="hidden" name="dtl[merek][]" value='<?= $key['merk']; ?>'></td>
                                            <td ><p><?= $key['beras']; ?></p><input type="hidden" name="dtl[kd_jenis_beras][]" value='<?= $key['kd_jns_beras']; ?>'></td>
                                            <td ><p><?= $key['khusus']; ?></p><input type="hidden" name="dtl[kd_khusus][]" value='<?= $key['kd_jns_khusus']; ?>'></td>
                                            <td ><p><?= $key['campuran']; ?></p><input type="hidden" name="dtl[kd_campuran][]" value='<?= $key['campuran']; ?>'></td>
                                            <td ><p><?= substr($key['kemasan'], 1, -1); ?></p><input type="hidden" name="dtl[kd_kemasan][]" value='<?= substr($key['kemval'], 0, -1); ?>'></td>
                                    <input type="hidden" name="dtl[merk_id][]" value='<?= $key['merk_id']; ?>'>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php if ($label): ?>
                                    <div class="btn-toolbar" role="toolbar">
                                        <div class="btn-group" role="group">
                                            <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewnilaimodal'); return false;" value="Edit Data" data-modal="true" style="margin-top:10px;"><i class="fa fa-check"></i> Update </button>
                                        </div>
                                        <div class="btn-group dropup">
                                            <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;margin-top:10px;"><span class="fa fa-list"></span> Form Permohonan </button>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                                                <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Pemilik/Penanggung Jawab</a></li>
                                                <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                                                <li class="divider"></li>
                                                <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php else: ?>
                                   <!--  <span class="pull-left">
                                    <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectsecond" data-url = "<?= $urisecond; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                                    </span> -->
                                    <span class="pull-right">
                                        <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewnilaimodal'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/jquery.formatCurrency-1.4.0.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/multiple-select.js"></script>
<script>
                                            $(document).ready(function (e) {
                                                $(".datepickers").datepicker({
                                                    autoclose: true
                                                });
                                                $('#label_khusus').hide();
                                            });
</script>
<script>
    var temp_rem = new Array();
    function change(x, y) {
        if ($("#kd_jenis_beras option:selected").attr("value") == 3) {
            $('#label_khusus').show();
        } else {
            $('#label_khusus').hide();
            $("#khusus").val("");
        }
    }

    function remove_det(tr, jumlah, tipe) {
        var stok_akhr = $('#stok_akhr').val();
        if (tipe == 1) {
            stok_akhr = parseFloat(stok_akhr) - parseFloat(jumlah);
        } else if (tipe == 2) {
            stok_akhr = parseFloat(stok_akhr) + parseFloat(jumlah);
        }
        // console.log(stok_akhr);
        if (parseFloat(stok_akhr) < 0) {
            BootstrapDialog.show({
                title: '',
                type: BootstrapDialog.TYPE_WARNING,
                message: 'Hasil Stok Akhir Tidak Diperbolehkan Minus'
            });
            return false;
        } else {
            $('#res').html(stok_akhr);
            $('#stok_akhr').val(stok_akhr);
            $('#tr_' + tr).remove();
        }
    }

    function set_detil(tipe) {
        if (tipe == 1) {
            var merek = $('#merek').val();
            var kd_campuran = $("#campuran").val();
            var kd_jenis_beras = $("#kd_jenis_beras option:selected").text();
            var kd_jenis_beras1 = $("#kd_jenis_beras").val();
            var khusus = $("#khusus option:selected").text();
            var khusus1 = $("#khusus").val();
            var kd_kemasan = $("#kd_kemasan option:selected").text();
            var kd_kemasan1 = $("#kd_kemasan").val();
            var tampung1 = kd_kemasan.replace(/kg/g, "kg,");
            var tampung = tampung1.substr(1, tampung1.length - 2);
        }
        if (merek == '' || kd_jenis_beras == '' || tampung == '') {
            BootstrapDialog.show({
                title: '',
                type: BootstrapDialog.TYPE_WARNING,
                message: 'Isi Data yang Kosong'
            });
            return false;
        }
        if (kd_jenis_beras == 'Khusus') {
            if (khusus.substr(1, khusus.length) == "") {
                BootstrapDialog.show({
                    title: '',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: 'Jenis Beras Khusus Tidak Boleh Kosong'
                });
                return false;
            }
        }

        var rand_remove = Math.floor((Math.random() * 1000) + 1);
        var temp = '';
        temp += '<tr id="tr_' + rand_remove + '">';
        temp += '<td ><button type="button" class="btn btn-danger fa fa-times" onclick="remove_det(' + rand_remove + ', ' + merek + ', ' + tipe + ')"></button></td>';
        temp += '<td ><p>' + merek + '</p><input type="hidden" name="dtl[merek][]" value="' + merek + '"></td>';
        temp += '<td ><p>' + kd_jenis_beras + '</p><input type="hidden" name="dtl[kd_jenis_beras][]" value="' + kd_jenis_beras1 + '"></td>';
        temp += '<td ><p>' + khusus + '</p><input type="hidden" name="dtl[kd_khusus][]" value="' + khusus1 + '"></td>';
        temp += '<td ><p>' + kd_campuran + '</p><input type="hidden" name="dtl[kd_campuran][]" value="' + kd_campuran + '"></td>';
        temp += '<td ><p>' + tampung + '</p><input type="hidden" name="dtl[kd_kemasan][]" value="' + kd_kemasan1 + '"></td>';

        temp += '</tr>';


        $('#tipe_1').append(temp);
        $(".close").click();
        $("#merek").val("");
        $("#campuran").val("");
//        $("#kd_kemasan option:selected").removeAttr("selected");
//        $("#kd_jenis_beras option:selected").prop("selected", false);
        $('#kd_kemasan').select2("val", "");
        $('#kd_jenis_beras').select2("val", "");
        $('#khusus').select2("val", "");
        $('#label_khusus').hide();
//        alert('asd');
        temp_rem.push(rand_remove);
        return false;
    }
</script>