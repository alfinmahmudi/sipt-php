<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewbapok" name="fnewbapok" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Permohonan</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id ="id" name="id" wajib="yes" value="<?= $sess['id']; ?>" />
            <input type="hidden" class="form-control mb-10" id="kd_izin" name="bapok[kd_izin]" wajib="yes" value="<?= $sess['kd_izin']; ?>" />
            <input type="hidden" class="form-control mb-10" id ="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label">Status Permohonan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('bapok[tipe_permohonan]', $tipe_permohonan, $sess['tipe_permohonan'], 'id="tipe_permohonan" disabled wajib="yes" class="form-control mb-10"'); ?>
                <input type="hidden" class="form-control mb-10" id ="tipe_permohonan" name="bapok[tipe_permohonan]" wajib="yes" value="<?= $sess['tipe_permohonan']; ?>"  />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jenis Perusahaan<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('bapok[jns_usaha]', $impor, $sess['jns_usaha'], 'id="jns_usaha" wajib="yes" class="select2 form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>          
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Perusahaan</b></header>
          <div class="panel-body">
            <div class="row">
              <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <label><?= (array_key_exists('npwp', $sess) ? $sess['npwp'] : $this->newsession->userdata('npwp')); ?></label>
                <input type="hidden" class="form-control mb-10" name="bapok[npwp]" readonly wajib="yes" value="<?= (array_key_exists('npwp', $sess) ? $sess['npwp'] : $this->newsession->userdata('npwp')); ?>" id="npwp" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <!-- <?= form_dropdown('bapok[tipe_perusahaan]', $tipe_perusahaan, (array_key_exists('tipe_perusahaan', $sess) ? $sess['tipe_perusahaan'] : $this->newsession->userdata('tipe_perusahaan')), 'id="tipe_perusahaan" wajib="yes" class="form-control mb-10"'); ?> -->
                <input type="hidden" name="bapok[tipe_perusahaan]" value="<?= $this->newsession->userdata('tipe_perusahaan'); ?>">
                <label><?= $drop['nm_tipe']; ?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <label><?= (array_key_exists('nm_perusahaan', $sess) ? $sess['nm_perusahaan'] : $this->newsession->userdata('nm_perusahaan')); ?></label>
                <input type="hidden" class="form-control mb-10" name="bapok[nm_perusahaan]" readonly wajib="yes" value="<?= (array_key_exists('nm_perusahaan', $sess) ? $sess['nm_perusahaan'] : $this->newsession->userdata('nm_perusahaan')); ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <label><?= array_key_exists('almt_perusahaan', $sess) ? $sess['almt_perusahaan'] : $this->newsession->userdata('almt_perusahaan'); ?></label>
                <input type="hidden" class="form-control mb-10" wajib="yes" readonly name="bapok[almt_perusahaan]" id="almt_perusahaan" value="<?= array_key_exists('almt_perusahaan', $sess) ? $sess['almt_perusahaan'] : $this->newsession->userdata('almt_perusahaan'); ?>" >
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <!-- <?= form_dropdown('bapok[kdprop]', $propinsi, $this->newsession->userdata('kdprop'), 'id="kdprop" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab\'); return false;"'); ?> -->
                <input type="hidden" name="bapok[kdprop]" value="<?= $this->newsession->userdata('kdprop'); ?>">
                <label><?= $drop['nm_prop']; ?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <!-- <?= form_dropdown('bapok[kdkab]', $kabupaten, $this->newsession->userdata('kdkab'), 'id="kdkab" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec\');"'); ?> -->
                <input type="hidden" name="bapok[kdkab]" value="<?= $this->newsession->userdata('kdkab'); ?>">
                <label><?= $drop['nm_kab']; ?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <!-- <?= form_dropdown('bapok[kdkec]', $kecamatan, $this->newsession->userdata('kdkec'), 'id="kdkec" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel\');"'); ?> -->
                <input type="hidden" name="bapok[kdkec]" value="<?= $this->newsession->userdata('kdkec'); ?>">
                <label><?= $drop['nm_kec']; ?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <!-- <?= form_dropdown('bapok[kdkel]', $kelurahan, $this->newsession->userdata('kdkel'), 'id="kdkel" wajib="yes" class="form-control input-sm select2"'); ?> -->
                <input type="hidden" name="bapok[kdkel]" value="<?= $this->newsession->userdata('kdkel'); ?>">
                <label><?= $drop['nm_kel']; ?></label>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telp/Hp</label>
              <div class="col-sm-2">
                <label><?= (array_key_exists('telp', $sess) ? $sess['telp'] : $this->newsession->userdata('telp')); ?></label>
                <input type="hidden" class="form-control mb-10" readonly name="bapok[telp]" value="<?= (array_key_exists('telp', $sess) ? $sess['telp'] : $this->newsession->userdata('telp')); ?>" id="telp" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Fax</label>
              <div class="col-sm-2">
                <label><?= (array_key_exists('fax', $sess) ? $sess['fax'] : $this->newsession->userdata('fax')); ?></label>
                <input type="hidden" class="form-control mb-10" name="bapok[fax]" readonly value="<?= (array_key_exists('fax', $sess) ? $sess['fax'] : $this->newsession->userdata('fax')); ?>" id="fax" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kode Pos</label>
              <div class="col-sm-1">
                <label><?= (array_key_exists('fax', $sess) ? $sess['kdpos'] : $this->newsession->userdata('kdpos')); ?></label>
                <input type="hidden" class="form-control mb-10" name="bapok[kdpos]" readonly value="<?= (array_key_exists('fax', $sess) ? $sess['kdpos'] : $this->newsession->userdata('kdpos')); ?>" id="kdpos" />
              </div>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-lg-12">
                <?php if($sess['id']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewbapok'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Pemilik / Penanggung Jawab</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/third/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Label Beras</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left"><button class="btn btn-sm btn-info addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= site_url(); ?>licensing/view/status"><i class="fa fa-undo"></i>Batal</button></span>  -->
                <span class="pull-right"><button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewbapok'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button></span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/jquery.browser.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/autocomplete.js"></script>
<script>
$(document).ready(function(e){ 
  $(".datepickers").datepicker({
    autoclose: true
  });
});
$(document).ready(function(e){ 
  $("#no_izin_lama").attr("url", '<?= site_url(); ?>get/ac/get_permohonan/' + $("#direktorat").val() + '/' + $("#kd_izin").val());
  $("#no_izin_lama").autocomplete($("#no_izin_lama").attr('url'), {width: '67.75%', selectFirst: false});
  $("#no_izin_lama").result(function (event, data, formatted) {
    if (data) {
      $(this).val(data[1]);
      $.get('<?= site_url(); ?>get/ac/set_form/' + data[2] + '/' + data[3], function (hasil) {
        if(hasil){
          var arrhasil = hasil.split("|");
          $("#id_permohonan_lama").val(arrhasil[0]);
          $("#no_izin_lama").val(arrhasil[1]);
          $("#tgl_izin_lama").val(arrhasil[2]);
          $("#tgl_izin_exp_lama").val(arrhasil[3]);
          $('#npwp').attr('value', arrhasil[4]);
          $("#nm_perusahaan").val(arrhasil[5]);
          $("#almt_perusahaan").val(arrhasil[6]);
		  $("#kdprop").select2('data',{id:arrhasil[7], text: arrhasil[20]});
          $("#kdpos").val(arrhasil[11]);
          $("#telp").val(arrhasil[12]);
          $("#fax").val(arrhasil[13]);
          $("#lokasi").val(arrhasil[14]);
          $("#status_milik").val(arrhasil[15]);
          $("#no_siup").val(arrhasil[16]);
          $("#tgl_siup").val(arrhasil[17]);
          $("#jenis_siup").val(arrhasil[18]);
          $('#tipe_perusahaan').val(arrhasil[19]);
          $.get('<?= site_url(); ?>get/cb/set_kota/' + arrhasil[7], function (result){
            if(result){
              $("#kdkab").html(result);
              $("#kdkab").select2('data',{id:arrhasil[8], text: arrhasil[21]});
              $.get('<?= site_url(); ?>get/cb/set_kec/' + arrhasil[8], function (result) {
                if (result) {
                  $("#kdkec").html(result);
                  $("#kdkec").select2('data',{id:arrhasil[9], text: arrhasil[22]});
                  $.get('<?= site_url(); ?>get/cb/set_kel/' + arrhasil[9], function (result) {
                    if (result) {
                      $("#kdkel").html(result);
                      $("#kdkel").select2('data',{id:arrhasil[10], text: arrhasil[23]});
                    } else {
                      $("#kdkel").html('');
                    }
                  });
                } else {
                  $("#kdkec").html('');
                }
              });
            } else {
              $("#kdkab").html('');
            }
          });
        }
      });
    }
  });
  $('.nomor').keydown(function (event) {
            var a = event.keyCode;
            var b = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 9, 11];
            if (jQuery.inArray(a, b) === -1) {
                event.preventDefault();
            }
        });
});
</script>