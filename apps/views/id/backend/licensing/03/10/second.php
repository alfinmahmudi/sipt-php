<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewkomoditi" name="fnewkomoditi" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Komoditi</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
            <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $id; ?>" />
            <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" wajib="yes" value="<?= $kd_izin; ?>" />
            <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?= $type; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label">Jenis Gula <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
				  <input type="text" readonly class="form-control mb-10" name="KOMODITI[jns_gula]" wajib="yes" value="Gula Kristal Rafinasi" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jumlah(Ton) <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="KOMODITI[jml_gula]" wajib="yes" value="<?= $sess['jml_gula']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Asal Gula <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
               <?= form_dropdown('KOMODITI[asal_gula]', $asal_gula, $sess['asal_gula'], 'id="asal_gula" wajib="yes" class="form-control input-sm select2" '); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Daerah Asal <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('KOMODITI[kdprop_asal_gula]', $propinsi, $sess['kdprop_asal_gula'], 'id="kdprop_asal_gula" wajib="yes" class="form-control input-sm select2" '); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Daerah Tujuan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                 <?= form_dropdown('KOMODITI[kdprop_tujuan_gula]', $propinsi, $sess['kdprop_tujuan_gula'], 'id="kdprop_tujuan_gula" wajib="yes" class="form-control input-sm select2" '); ?>
              </div>
			</div>  
			<div style="height:5px;"></div>
            <div class="row">  
              <label class="col-xs-3 control-label">Pelabuhan Asal / Muat <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('KOMODITI[loading_gula]', $pelabuhan, $sess['loading_gula'], 'id="loading_gula" wajib="yes" class="form-control input-sm select2" '); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Pelabuhan Tujuan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
               <?= form_dropdown('KOMODITI[discharge_gula]', $pelabuhan, $sess['discharge_gula'], 'id="discharge_gula" wajib="yes" class="form-control input-sm select2" '); ?>
              </div>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if($sess['jns_gula']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewkomoditi'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $type; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/third/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $type; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Produsen dan Distributor</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $type; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $type; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= $urifirst; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> --> 
                <span class="pull-right">
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewkomoditi'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
                </span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script><script>$(document).ready(function(e){ $(".datepickers").datepicker({autoclose: true});});</script>