<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-dialog/bootstrap-dialog.min.css" type="text/css" cache="false" />
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-dialog/bootstrap-dialog.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtable.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtablehash.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
 <!-- <form action="<?//= $act; ?>" method="post" id="fnewkomoditi" name="fnewkomoditi" autocomplete = "off" data-redirect="true"> -->
    <!-- <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Produsen</b></header>
          <div class="panel-body">
            <div class="row">
              <?= $lstProdusen; ?>
            </div>
          </div>
        </section>
      </div>
    </div> -->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Permintaan</b></header>
          <div class="panel-body">
            <div class="row">
              <?= $lstProdusen; ?>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Distributor</b></header>
          <div class="panel-body">
            <div class="row">
              <?= $lstDistributor; ?>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if($sess['jns_gula']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Komoditi</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= $urifirst; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> --> 
				<?php
					if($banyakPermintaan == 0){
						$disabled="disabled";
					}else{
						$disabled="";
					} 
					//echo $id;
				?>
                <span class="pull-right">
                <button type="button" class="btn btn-sm btn-success addon-btn m-b-10" <?=$disabled;?> onclick="redirect($(this)); return false;" id="btnredirectfourth" data-url = "<?= $urifourth; ?>"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
                </span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    
 <!-- </form> -->
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script><script>$(document).ready(function(e){ $(".datepickers").datepicker({autoclose: true});});</script>