<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/multiple-select.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewnilaimodal" name="fnewnilaimodal" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Jenis Barang Kebutuhan Pokok Yang Di Perdagangkan</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
            <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $sess['id']; ?>" />
            <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" wajib="yes" value="<?= $sess['kd_izin']; ?>" />
            <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?= $sess['tipe_permohonan']; ?>" />
            <!--div class="row">
              <label class="col-sm-3 control-label">Kelembagaan<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('bapok[kelembagaan]', $kelembagaan, $sess['kelembagaan'], 'id="jenis" wajib="yes" class="form-control input-sm select2" '); ?>
              </div>
            </div-->
            <!-- <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label"><b>Bidang Usaha</b><font size ="2" color="red">*</font></label>
              <div class="col-sm-9">

              </div>
            </div> -->
                        <div style="height:10px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label"><?= $bapok[0]['uraian'] ?></label>
              <div class="col-sm-9">
                <?php foreach ($urai1 as $jns) { ?>
                  <input type="checkbox" id="jen_<?= $jns['id']; ?>" name="jns_<?= $jns['jenis_bapok']; ?>[]" value="<?= $jns['id']; ?>" > <?= $jns['uraian']; ?> <br>
                <?php } ?>
              </div>
            </div>

            <div style="height:10px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label"><?= $bapok[1]['uraian'] ?></label>
              <div class="col-sm-9">
                <?php foreach ($urai2 as $jns) { ?>
                  <input type="checkbox" id="jen_<?= $jns['id']; ?>" name="jns_<?= $jns['jenis_bapok']; ?>[]" value="<?= $jns['id']; ?>" > <?= $jns['uraian']; ?> <br>
                <?php } ?>
              </div>
            </div>

            <div style="height:10px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label"><?= $bapok[2]['uraian'] ?></label>
              <div class="col-sm-9">
                <?php foreach ($urai3 as $jns) { ?>
                  <input type="checkbox" id="jen_<?= $jns['id']; ?>" name="jns_<?= $jns['jenis_bapok']; ?>[]" value="<?= $jns['id']; ?>" > <?= $jns['uraian']; ?> <br>
                <?php } ?>
              </div>
            </div>

            <div style="height:5px;"></div>
            <div class="row" id="def">
              <label class="col-sm-3 control-label">Wilayah Distribusi</label>
              <div class="col-sm-9">
                <?php foreach ($pemasaran as $pem) { ?>
                  <div id="divpem_<?= $pem['kode']; ?>" >
                  <input type="checkbox" id="pem_<?= $pem['kode']; ?>" name="pemasaran[]" value="<?= $pem['kode']; ?>" onclick="showHideText(this,'wil_<?= $pem['kode']; ?>')"> <?= $pem['uraian'] ?> <br>
                  </div>
                <?php } ?>
              </div>
            </div>
            <div class="row" id="indo" style="display: none;">
              <label class="col-sm-3 control-label">Wilayah Distribusi</label>
              <div class="col-sm-9">
                <input type="checkbox" id="pem_01_hide" name="pemasaran[]" value="01" onclick="showDef()"> Seluruh Wilayah Indonesia <br>
              </div>
            </div>

            <div id ="wil_02"  style="display:none;">
              <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="col-md-6">Propinsi Wilayah Distribusi</th>
                            </tr>
                        </thead>
                        <tbody class="bodyta_a">
                            <tr id="trta_<?= rand(); ?>">
                                <td>
                                    <input type="hidden" class="form-control input-sm" name="jns_gol_a[id][]" value="" readonly="readonly"/>
                                    <?= form_multiselect('prop[]', $prop, $sess_prop, 'id="prop" wajib="yes" multiple class="form-control input-sm select2" '); ?>
                                <td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div id ="wil_03"  style="display:none;">
              <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="col-md-6">Kabupaten Wilayah Distribusi</th>
                            </tr>
                        </thead>
                        <tbody class="bodyta_a">
                            <tr id="trta_<?= rand(); ?>">
                                <td>
                                    <input type="hidden" class="form-control input-sm" name="jns_gol_a[id][]" value="" readonly="readonly"/>
                                    <?= form_multiselect('kab[]', $kab, $sess_kab, 'id="prop" wajib="yes" multiple class="form-control input-sm select2" '); ?>
                                <td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- <div class="row">
              <label class="col-sm-3 control-label">Jenis Barang Kebutuhan Pokok Yang Di Perdagangkan</label>
              <div class="col-sm-9">
                <?php foreach ($jenis as $jns) { ?>
                  <input type="checkbox" id="cb_<?= $jns['kode']; ?>" name="jns_brg[]" value="<?= $jns['kode']; ?>" onclick="get_jenis(this,'<?= $jns['kode']; ?>')"> <?= $jns['uraian']; ?> <br>
                <?php } ?>
              </div>
            </div> -->

            <!-- <div style="height:10px;"></div> -->
            <!-- <div id = "div_01" style="display: none;">
              <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="col-md-6" id="nama_01"></th>
                            </tr>
                        </thead>
                        <tbody class="bodyt01">
                            
                        </tbody>
                    </table>
                </div>
            </div> -->
            <!-- <div class="row" id="div_01" style="display: none;">
              <label class="col-sm-3 control-label" id="nama_01"> </label>
              <div class="col-sm-9 bodyt01">
                
              </div>
            </div> -->

            <!-- <div style="height:10px;"></div> -->
            <!-- <div id = "div_02" class="row" style="display: none;">
              <div class="col-md-6">
              	&nbsp;
              </div>
              <div class="col-md-6">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="col-md-6" id="nama_02"></th>
                            </tr>
                        </thead>
                        <tbody class="bodyt02">
                            
                        </tbody>
                    </table>
                </div>
            </div> -->
            <!-- <div class="row" id="div_02" style="display: none;">
              <label class="col-sm-3 control-label" id="nama_02"> </label>
              <div class="col-sm-9 bodyt02">
                
              </div>
            </div> -->

            <!-- <div style="height:10px;"></div> -->
            <!-- <div id = "div_03" style="display: none;">
              <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="col-md-6" id="nama_03"></th>
                            </tr>
                        </thead>
                        <tbody class="bodyt03">
                            
                        </tbody>
                    </table>
                </div>
            </div> -->
            <!-- <div class="row" id="div_03" style="display: none;">
              <label class="col-sm-3 control-label" id="nama_03"> </label>
              <div class="col-sm-9 bodyt03">
                
              </div>
            </div> -->

            <div style="height:25px;"></div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if($sess['bidang_usaha']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewnilaimodal'); return false;" value="Edit Data" data-modal="true" style="margin-top:10px;"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;margin-top:10px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Pemilik/Penanggung Jawab</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
               <!--  <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectsecond" data-url = "<?= $urisecond; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> -->
                <span class="pull-right">
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewnilaimodal'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
                </span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/jquery.formatCurrency-1.4.0.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/multiple-select.js"></script>
<script>
$(document).ready(function(e){ 
  $(".datepickers").datepicker({ 
    autoclose: true 
  }); 
  
  
  <?php $arrpem =  explode(",", $sess['pemasaran']);
    foreach ($arrpem as $kuy) {
      if ($kuy == '01') { ?>
        $('#pem_01').click();
      <?php }elseif ($kuy == '02') { ?>
        $('#pem_02').click();
      <?php }elseif ($kuy == '03') { ?>
        $('#pem_03').click();
      <?php } } ?>

  <?php foreach ($detil as $det) { ?>
  	$('#jen_<?= $det['jns_brg']; ?>').prop('checked', true);
  <?php }  ?>
});
</script>
<script>
function showDef(){
	$('#def').show();
  	$('#indo').hide();
  	$('#pem_01_hide').prop('checked', false);
  	$('#pem_01').prop('checked', false);
}

function showHideText(boxName,divName) {
  if(boxName.checked == true) {
  	if (divName == "wil_01") {
  		$('#def').hide();
  		$('#indo').show();
  		$('#pem_01_hide').prop('checked', true);
  	}else if (divName == "wil_03" || divName == "wil_02") {
      $('#divpem_01').hide();
    }
    $('#'+divName).show();
  }
  else {
    if (divName == "wil_03" || divName == "wil_02") {
      if ($('#pem_02').is(':checked') || $('#pem_03').is(':checked')) {

      }else{
        $('#divpem_01').show();
      }
    }
    $('#'+divName).hide();
  }
}

</script>
<script>
  var site_url = '<?php echo site_url(); ?>';
  function get_jenis(boxName, a){
    if(boxName.checked == true) {
      // console.log(site_url);
      $.post(site_url+"licensing/get_jenis_bapok", {id_jenis: a}, function(hasil){
        $('#div_'+a).show();
        $('#nama_'+a).html(hasil.nama);
        $('.bodyt'+a).append(hasil.temp);
      }, 'json');
    } else if ($.isNumeric(boxName) && a != '') {
      $.post(site_url+"licensing/get_jenis_bapok", {id_jenis: a, value: boxName}, function(hasil){
        $('#div_'+a).show();
        $('#nama_'+a).html(hasil.nama);
        $('.bodyt'+a).append(hasil.temp);
      }, 'json'); 
    }
    else {
      $('#div_'+a).hide();
      $('#nama_'+a).html('');
      $('.bodyt'+a).html('');
    }
  }

  function add(a){
    var flag = '1';
    // console.log(site_url);
    $.post(site_url+"licensing/get_jenis_bapok", {id_jenis: a, flag: flag}, function(hasil){
      console.log(a);
      $('.bodyt'+a).append(hasil.temp);
    }, 'json');
  }

  function rem(id){
    $('#'+id).remove();
  }
</script>