<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewsiupmb" name="fnewsiupmb" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Permohonan</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id ="id" name="id" wajib="yes" value="<?=$sess['id']; ?>" />
            <input type="hidden" class="form-control mb-10" id="kd_izin" name="siupbb[kd_izin]" wajib="yes" value="<?= $sess['kd_izin']; ?>" />
            <input type="hidden" class="form-control mb-10" id ="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label">Status Permohonan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('siupbb[tipe_permohonan]', $tipe_permohonan, $sess['tipe_permohonan'], 'id="tipe_permohonan" disabled wajib="yes" class="form-control mb-10"'); ?>
                <input type="hidden" class="form-control mb-10" id ="tipe_permohonan" name="siupbb[tipe_permohonan]" wajib="yes" value="<?= $sess['tipe_permohonan']; ?>" />
              </div>
            </div>
            <?php if($tipe != '01'):?>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nomor Dokumen Lama <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="hidden" class="form-control mb-10" id="id_permohonan_lama" name="siupbb[id_permohonan_lama]" value="<?= $sess['id_permohonan_lama']; ?>" width="100%" readonly />
               <input type="text" class="form-control mb-10" id="no_izin_lama" name="siupbb[no_izin_lama]" wajib="yes" value="<?= $sess['no_izin_lama']; ?>" width="100%" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tanggal Dokumen Lama <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 datepickers" data-date-format = "yyyy-mm-dd" id="tgl_izin_lama" name="siupbb[tgl_izin_lama]" value="<?= $sess['tgl_izin_lama']; ?>"  />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tanggal Akhir Dokumen Lama <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 datepickers" data-date-format = "yyyy-mm-dd" id="tgl_izin_exp_lama" name="siupbb[tgl_izin_exp_lama]" value="<?= $sess['tgl_izin_exp_lama']; ?>"  />
              </div>
            </div>
            <div style="height:5px;"></div>
            <?php if ($no_pkapt == "") { ?>
              <div class="row">
                <label class="col-sm-3 control-label">Nomor PKAPT <font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control mb-10" id="no_pkapt" name="siupbb[no_pkapt]" wajib="yes" width="100%" />
                </div>
              </div>
            <?php }else{ ?>
                <input type="hidden" class="form-control mb-10" id="no_pkapt" name="siupbb[no_pkapt]" value="<?= $no_pkapt; ?>" wajib="yes" width="100%" />
             <?php } ?>
            <?php endif;?>
			
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Perusahaan</b></header>
          <div class="panel-body">
            <div class="row">
              <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siupbb[npwp]" readonly wajib="yes" value="<?= $this->newsession->userdata('npwp'); ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('siupbb[tipe_perusahaan]', $tipe_perusahaan, $this->newsession->userdata('tipe_perusahaan'), 'id="tipe_perusahaan" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id="nm_perusahaan" name="siupbb[nm_perusahaan]" readonly wajib="yes" value="<?= (array_key_exists('nm_perusahaan', $sess) ? $sess['nm_perusahaan'] : $this->newsession->userdata('nm_perusahaan')); ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" wajib="yes" readonly id="almt_perusahaan" name="siupbb[almt_perusahaan]"><?= array_key_exists('almt_perusahaan', $sess) ? $sess['almt_perusahaan'] : $this->newsession->userdata('almt_perusahaan'); ?>
				</textarea>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('siupbb[kdprop]', $propinsi, (array_key_exists('kdprop', $sess) ? $sess['kdprop'] : $this->newsession->userdata('kdprop')), 'id="kdprop" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab\'); return false;"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('siupbb[kdkab]', $kabupaten, $this->newsession->userdata('kdkab'), 'id="kdkab" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('siupbb[kdkec]', $kecamatan, $this->newsession->userdata('kdkec'), 'id="kdkec" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('siupbb[kdkel]', $kelurahan, $this->newsession->userdata('kdkel'), 'id="kdkel" wajib="yes" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telp/Hp</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" id="telp" readonly name="siupbb[telp]" value="<?= (array_key_exists('telp', $sess) ? $sess['telp'] : $this->newsession->userdata('telp')); ?>" />
              </div>
			</div>  
			<div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Fax</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" id="fax" readonly name="siupbb[fax]" value="<?= (array_key_exists('fax', $sess) ? $sess['fax'] : $this->newsession->userdata('fax')); ?>" />
              </div>
			</div>  
			<div style="height:5px;"></div>
            <div class="row"> 
              <label class="col-xs-3 control-label">Kode Pos</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" id="kdpos" readonly name="siupbb[kdpos]" value="<?= (array_key_exists('fax', $sess) ? $sess['kdpos'] : $this->newsession->userdata('kdpos')); ?>" />
              </div>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-lg-12">
                <?php if($sess['id']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewsiupmb'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Pemilik / Penanggung Jawab</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/third/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left"><button class="btn btn-sm btn-info addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= site_url(); ?>licensing/view/status"><i class="fa fa-undo"></i>Batal</button></span>  -->
                <span class="pull-right"><button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewsiupmb'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button></span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/jquery.browser.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/autocomplete.js"></script>
<script>
$(document).ready(function(e){ 
  $(".datepickers").datepicker({
    autoclose: true});

  // $("#no_izin_lama").attr("url", '<?= site_url(); ?>get/ac/get_permohonan/' + $("#direktorat").val() + '/' + $("#kd_izin").val());
  // $("#no_izin_lama").autocomplete($("#no_izin_lama").attr('url'), {width: '67.75%', selectFirst: false});
  // $("#no_izin_lama").result(function (event, data, formatted) {
  //   if (data) {
  //     $(this).val(data[1]);
  //     $.get('<?= site_url(); ?>get/ac/set_form/' + data[2] + '/' + data[3], function (hasil) {
  //       if(hasil){
  //         var arrhasil = hasil.split("|");
  //         $("#id_permohonan_lama").val(arrhasil[0]);
  //         $("#no_izin_lama").val(arrhasil[1]);
  //         $("#tgl_izin_lama").val(arrhasil[2]);
  //         $("#tgl_izin_exp_lama").val(arrhasil[3]);
  //         $('#npwp').attr('value', arrhasil[4]);
  //         $("#nm_perusahaan").val(arrhasil[5]);
  //         $("#almt_perusahaan").val(arrhasil[6]);
  //     $("#kdprop").select2('data',{id:arrhasil[7], text: arrhasil[20]});
  //         $("#kdpos").val(arrhasil[11]);
  //         $("#telp").val(arrhasil[12]);
  //         $("#fax").val(arrhasil[13]);
  //         $("#lokasi").val(arrhasil[14]);
  //         $("#status_milik").val(arrhasil[15]);
  //         $("#no_siup").val(arrhasil[16]);
  //         $("#tgl_siup").val(arrhasil[17]);
  //         $("#jenis_siup").val(arrhasil[18]);
  //         $('#tipe_perusahaan').val(arrhasil[19]);
  //         $.get('<?= site_url(); ?>get/cb/set_kota/' + arrhasil[7], function (result){
  //           if(result){
  //             $("#kdkab").html(result);
  //             $("#kdkab").select2('data',{id:arrhasil[8], text: arrhasil[21]});
  //             $.get('<?= site_url(); ?>get/cb/set_kec/' + arrhasil[8], function (result) {
  //               if (result) {
  //                 $("#kdkec").html(result);
  //                 $("#kdkec").select2('data',{id:arrhasil[9], text: arrhasil[22]});
  //                 $.get('<?= site_url(); ?>get/cb/set_kel/' + arrhasil[9], function (result) {
  //                   if (result) {
  //                     $("#kdkel").html(result);
  //                     $("#kdkel").select2('data',{id:arrhasil[10], text: arrhasil[23]});
  //                   } else {
  //                     $("#kdkel").html('');
  //                   }
  //                 });
  //               } else {
  //                 $("#kdkec").html('');
  //               }
  //             });
  //           } else {
  //             $("#kdkab").html('');
  //           }
  //         });
  //       }
  //     });
  //   }
  // });

});
</script>