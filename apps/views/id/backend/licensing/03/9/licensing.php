<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Licensing extends CI_Controller{
	var $ineng = "";
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('site_lang')){
			$this->lang->load('message',$this->session->userdata('site_lang'));
			$this->ineng = $this->session->userdata('site_lang'); 
		}else{
			$this->lang->load('message','id');
			$this->ineng = "id";
		}
    }
	
	public function index(){
		echo "Forbidden";
	}
	
	
	public function requirements(){
		if($this->newsession->userdata('_LOGGED')){
			$this->load->model('licensing/licensing_act');
			$arrdata = $this->licensing_act->view();
			echo $this->load->view($this->ineng.'/backend/licensing/requirements-result', $arrdata, true);
		}
	}
	
	public function choice(){
		//print_r($_POST);die();
		if($this->newsession->userdata('_LOGGED')){
			/*[direktorat] => 02
    [dokumen] => 4
    [jenis] => 01*/
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$dir = $this->input->post('direktorat');
				$doc = $this->input->post('dokumen');
				$type = $this->input->post('jenis');
				//echo "MSG||YES||".site_url().'licensing/form/first/'.$dir.'/'.hashids_encrypt($doc, _HASHIDS_, 6).'/'.hashids_encrypt($type, _HASHIDS_, 6);
				echo "MSG||YES||".site_url().'licensing/form/first/'.$dir.'/'.$doc.'/'.$type;
			}
		}
	}
	
	public function siupmb_act($step, $act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('licensing/siupmb_act');
				if($step == "first"){ 
					$ret = $this->siupmb_act->set_first($act, $isajax);
				}else if($step == "second"){ 
					$ret = $this->siupmb_act->set_second($act, $isajax);
				}else if($step == "third"){ 
					$ret = $this->siupmb_act->set_third($act, $isajax);
				}else if($step == "fourth"){ 
					$ret = $this->siupmb_act->set_fourth($act, $isajax);
				}
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}

	public function siup4_act($step, $act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('licensing/siup4_act');
				if($step == "first"){ 
					$ret = $this->siup4_act->set_first($act, $isajax);
				}else if($step == "second"){ 
					$ret = $this->siup4_act->set_second($act, $isajax);
				}else if($step == "third"){ 
					$ret = $this->siup4_act->set_third($act, $isajax);
				}else if($step == "fourth"){ 
					$ret = $this->siup4_act->set_fourth($act, $isajax);
				}
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}

	public function siujs_act($step, $act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('licensing/siujs_act');
				if($step == "first"){ 
					$ret = $this->siujs_act->set_first($act, $isajax);
				}else if($step == "second"){ 
					$ret = $this->siujs_act->set_second($act, $isajax);
				}else if($step == "third"){ 
					$ret = $this->siujs_act->set_third($act, $isajax);
				}else if($step == "fourth"){ 
					$ret = $this->siujs_act->set_fourth($act, $isajax);
				}else if($step == "fifth"){ 
					$ret = $this->siujs_act->set_fifth($act, $isajax);
				}else if($step == "sixth"){ 
					$ret = $this->siujs_act->set_sixth($act, $isajax);
				}
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}

	public function siupagen_act($step, $act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('licensing/siupagen_act');
				if($step == "first"){ 
					$ret = $this->siupagen_act->set_first($act, $isajax);
				}else if($step == "second"){ 
					$ret = $this->siupagen_act->set_second($act, $isajax);
				}else if($step == "third"){ 
					$ret = $this->siupagen_act->set_third($act, $isajax);
				}else if($step == "fourth"){ 
					$ret = $this->siupagen_act->set_fourth($act, $isajax);
				}
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}

	public function siupbb_act($step, $act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('licensing/siuppgapt_act');
				if($step == "first"){ 
					$ret = $this->siupbb_act->set_first($act, $isajax);
				}else if($step == "second"){ 
					$ret = $this->siupbb_act->set_second($act, $isajax);
				}else if($step == "third"){ 
					$ret = $this->siupbb_act->set_third($act, $isajax);
				}else if($step == "fourth"){ 
					$ret = $this->siupbb_act->set_fourth($act, $isajax);
				}
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}
	
	public function siuppgapt_act($step, $act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('licensing/siuppgapt_act');
				if($step == "first"){ 
					print_r($act);die();
					$ret = $this->siuppgapt_act->set_first($act, $isajax);
				}else if($step == "second"){ 
					$ret = $this->siuppgapt_act->set_second($act, $isajax);
				}else if($step == "third"){ 
					$ret = $this->siuppgapt_act->set_third($act, $isajax);
				}
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}
	}

	public function tembusan($act, $isajax){
		if($this->newsession->userdata('_LOGGED')){
			//print_r($_POST);die();
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();
			}else{
				$this->load->model('licensing/status_act');
				$ret = $this->status_act->set_tembusan($act, $isajax);
			}
			if($isajax!="ajax"){
				redirect(base_url());
			}
			echo $ret;
		}

	}
	
}