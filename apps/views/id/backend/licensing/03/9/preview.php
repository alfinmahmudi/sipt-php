<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="page-head">
  <h3 class="m-b-less"> Preview Permohonan</h3>
  <span class="sub-title">
  <?= $sess['nama_izin'];?>
  </span> </div>
<div class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Data Permohonan</header>
          <div class="panel-body">
            <section class="isolate-tabs">
              <ul class="nav nav-tabs">
                <li class="active"> <a data-toggle="tab" href="#first">Data Permohonan</a> </li>
                <li class=""> <a data-toggle="tab" href="#second">Data Komoditas</a> </li>
                <li class=""> <a data-toggle="tab" href="#third">Data Produsen dan Distributor</a> </li>
                <li class=""> <a data-toggle="tab" href="#fourth">Data Legalitas dan Persyaratan</a> </li>
              </ul>
              <div class="panel-body">
                <div class="tab-content">
                  <div id="first" class="tab-pane active">
                    <div class="row">
                      <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading"> <b>Data Permohonan</b></header>
                          <div class="panel-body">
                            <div class="row">
                              <label class="col-sm-3 control-label">Status Permohonan <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['permohonan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Nomor Pengajuan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $sess['no_aju']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Tanggal Pengajuan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $sess['tgl_aju']; ?>
                            </div>
                          </div>
                          </div>
                        </section>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading"> <strong><b>Data Perusahaan</b></strong></header>
                          <div class="panel-body">
                            <div class="row">
                              <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['npwp']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['tipe_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['nm_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['almt_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['prop_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['kab_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['kec_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['kel_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Telp/HP</label>
                              <div class="col-sm-9">
                                <?= $sess['telp']; ?>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Fax</label>
                              <div class="col-sm-9">
                                <?= $sess['fax']; ?>
                              </div>
                            </div>
							<div class="row">
                              <label class="col-sm-3 control-label">Kode Pos</label>
                              <div class="col-sm-9">
                                <?= $sess['kdpos']; ?>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                  <div id="second" class="tab-pane">
                    <div class="row">
                      <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading"> <b>Data Komoditas</b></header>
                          <div class="panel-body">
                            <div class="row">
                              <label class="col-sm-3 control-label">Jenis Gula <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['jns_gula']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Jumlah Gula <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['jml_gula']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Asal Gula <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['asal_gula']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Daerah Asal <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['kdprop_asal_gula']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Daerah Tujuan <font size ="2" color="red">*</font></label>
                              <div class="col-sm-3">
                                <?= $sess['kdprop_tujuan_gula']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Pelabuhan Asal/Muat <font size ="2" color="red">*</font></label>
                              <div class="col-sm-3">
                                <?= $sess['loading_gula']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Pelabuhan Tujuan/Bongkar <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['discharge_gula']; ?>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                  <div id="third" class="tab-pane">
                    <div class="row">
                      <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading"> <b>Data Produsen </b></header>
                          <div class="panel-body">
                            <div class="row col-lg-12">
                              <script src="<?= base_url(); ?>assets/bend/js/newtable/newtable.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
                              <script src="<?= base_url(); ?>assets/bend/js/newtable/newtablehash.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
                               <?= $lstProdusen; ?>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading"> <b>Data Distributor </b></header>
                          <div class="panel-body">
                            <div class="row col-lg-12">
                              <script src="<?= base_url(); ?>assets/bend/js/newtable/newtable.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
                              <script src="<?= base_url(); ?>assets/bend/js/newtable/newtablehash.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
                              <?= $lstDistributor; ?>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                  <div id="fourth" class="tab-pane">
                    <div class="row">
                      <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading"> <b>Data Legalitas dan Persyaratan</b></header>
                          <div class="panel-body">
                            <div class="row col-lg-12">
                              <table class="table table-striped">
                                <thead>
                                  <tr class="control-label">
                                    <th class="control-label" width= "3%">No</th>
                                    <th>Nama Dokumen</th>
                                    <th width= "20%">Status</th>
                                    <th width= "5%">&nbsp;</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php if(count($req) > 0):$no = 1;foreach($req as $reqs):?>
                                <td align="center"><?php echo $no++; ?></td>
                                  <td><?php echo $reqs['keterangan']; echo ($reqs['tipe'] =='1')?' <font size ="2" color="red">*</font>':"";?></td>
                                  <td><?= $reqs['uraian']?></td>
                                  <td><center>
                                      <div id="btn"> </div>
                                    </center></td>
                                </tr>
                                <tr>
                                  <td nowrap="nowrap"></td>
                                  <td colspan="3"><table width="100%" class="table table-nomargin table-condensed table-striped">
                                      <tr>
                                        <th><center>
                                            No. Dokumen
                                          </center></th>
                                        <th><center>
                                            Penerbit
                                          </center></th>
                                        <th><center>
                                            Tgl. Dokumen
                                          </center></th>
                                        <th><center>
                                            Tgl. Akhir
                                          </center></th>
                                        <th><center>
                                            File
                                          </center></th>
                                      </tr>
                                      <?php if($sess_syarat):for($i = 0; $i < count($sess_syarat[$reqs['dok_id']]); $i++):?>
                                      <tr>
                                        <td width="20%"><center>
                                            <span id="no_dok_<?= $reqs['dok_id']; ?>">
                                            <?= $sess_syarat[$reqs['dok_id']][$i]['nomor'];?>
                                            </span>
                                          </center></td>
                                        <td width="35%"><center>
                                            <span id="penerbit_<?= $reqs['dok_id']; ?>">
                                            <?= $sess_syarat[$reqs['dok_id']][$i]['penerbit_dok'];?>
                                            </span>
                                          </center></td>
                                        <td width="20%"><center>
                                            <span id="tgl_dok_<?= $reqs['dok_id']; ?>">
                                            <?= $sess_syarat[$reqs['dok_id']][$i]['tgl_dok'];?>
                                            </span>
                                          </center></td>
                                        <td width="20%"><center>
                                            <span id="tgl_exp_<?= $reqs['dok_id']; ?>">
                                            <?= $sess_syarat[$reqs['dok_id']][$i]['tgl_exp'];?>
                                            </span>
                                          </center></td>
                                        <td width="10%"><center>
                                            <span id="View_<?= $reqs['dok_id']; ?>">
                                            <?= $sess_syarat[$reqs['dok_id']][$i]['View'];?>
                                            </span>
                                          </center></td>
                                        </tr>
                                        <?php endfor;else:?>
                                        <tr>
                                        <td colspan="5"><center>
                                            <b>Tidak Terdapat Data</b>
                                          </center></td>
                                        </tr>
                                        <?php endif;?>
                                    </table></td>
                                <tr>
                                  <?php endforeach; else: ?>
                                <tr>
                                  <td colspan="4"><center>
                                      <b>Tidak Terdapat Data</b>
                                    </center></td>
                                </tr>
                                <?php endif; ?>
                                  </tbody>
                                
                              </table>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <form action="<?= $act; ?>" method="post" id="fpreview" name="fpreview" autocomplete = "off" data-redirect="true">
      <input type="hidden" name="data[id]" value="<?= hashids_encrypt($sess['id'], _HASHIDS_, 9); ?>">
      <input type="hidden" name="data[kd_izin]" value="<?= hashids_encrypt($sess['kd_izin'], _HASHIDS_, 9); ?>">
      <input type="hidden" name="data[no_aju]" value="<?=$sess['no_aju']?>">
      <input type="hidden" name="direktorat" value="<?= hashids_encrypt($sess['direktorat_id'], _HASHIDS_, 9); ?>">
      <input type="hidden" name="tipe" value="<?= base64_encode($sess['permohonan']);?>">
      <input type="hidden" name="SEBELUM" value="<?=$sess['status']?>">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Data Proses Permohonan </header>
          <div class="panel-body">
            <?= $input; ?>
            <?php
            if(count($proses) > 0){
            ?>
            <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label for="catatan">Catatan</label>
                      <?php if ($this->newsession->userdata('role') == '05') {
                        $readonly = 'readonly';
                        $wajib = 'no';
                      }else{
                        $wajib = 'yes';
                        } ?>
                        <textarea class="jqte-catatan form-control mb-10" name="catatan" <?= $readonly; ?> wajib="<?= $wajib; ?>" id="catatan"></textarea>
                        <?php if($this->newsession->userdata('role') == '05'):?>
                        <label class="checkbox-custom inline check-success">
                          <input value="1" id="checkbox-agreement" type="checkbox" <?= ($this->newsession->userdata('role') == '05')?'check-agreement="yes"':''; ?>> <label for="checkbox-agreement"><b>Menyatakan bahwa semua data yang di masukkan ke portal SIPT PDN adalah <font color="red">Benar</font> dan <font color="red">Asli</font>.<br>
Apabila dikemudian hari data yang dimasukkan tidak benar maka siap diberikan sanksi sesuai dengan peraturan yang berlaku.</b></label>
                      </label>
                      <?php endif;?>
                    </div>
                <?php 
                      if(count($proses) > 0){
                          foreach($proses as $x){
                              echo $x . ' ';
                          }
                      }
                      ?>

              </div>
            </div>
            <?php
            }else{
        ?>
                <button class="btn btn-sm btn-info addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-info pull-right"></i>Kembali</button>
                <?php
      }
            ?>
            <div class="row">&nbsp;</div>
            <div class="row">
              <div class="col-md-12">
                <section class="panel-timeline">
                  <div class="time-line-wrapper">
                    <div class="time-line-caption">
                      <h3 class="time-line-title">Histori Permohonan <sup><a href="javascript:void(0);" id="<?= rand(); ?>" class="log-izin" data-url = "<?= $urllog; ?>" data-target="#tmplog"><span class="badge bg-info"> <?= $jmllog; ?> </span></a></sup></h3>
                    </div>
                    <div id="tmplog"><div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </section>
      </div>
    </form>
  </div>
</div>
