<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan </h3>
  <span class="sub-title">Surat Izin Usaha Perdagangan Minuman Beralkohol (SIUP - MB)</span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewsiupmb" name="fnewsiupmb" autocomplete = "off">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Data Permohonan</header>
          <div class="panel-body"> 
            <div class="row">
              <label class="col-sm-3 control-label">Status Permohonan</label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[tipe_permohonan]', $tipe_permohonan, $sess['tipe_permohonan'], 'id="tipe_permohonan" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Data Perusahaan</header>
          <div class="panel-body"> 
          	<div class="row">
              <label class="col-sm-3 control-label">NPWP</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siupmb[npwp]" wajib="yes" value="<?= $this->newsession->userdata('npwp'); ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Bentuk Usaha</label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[tipe_perusahaan]', $tipe_perusahaan, (array_key_exists('tipe_perusahaan', $sess) ? $sess['tipe_perusahaan'] : $this->newsession->userdata('tipe_perusahaan')), 'id="tipe_perusahaan" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
          	<div class="row">
              <label class="col-sm-3 control-label">Nama Perusahaan</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siupmb[nm_perusahaan]" wajib="yes" value="<?= (array_key_exists('nm_perusahaan', $sess) ? $sess['nm_perusahaan'] : $this->newsession->userdata('nm_perusahaan')); ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
          	<div class="row">
              <label class="col-sm-3 control-label">Alamat Perusahaan</label>
              <div class="col-sm-9">
              	<textarea class="form-control mb-10" name="siupmb[almt_perusahaan]"><?= array_key_exists('almt_perusahaan', $sess) ? $sess['almt_perusahaan'] : $this->newsession->userdata('almt_perusahaan'); ?></textarea>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Propinsi</label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[kdprop]', $propinsi, (array_key_exists('kdprop', $sess) ? $sess['kdprop'] : $this->newsession->userdata('kdprop')), 'id="kdprop" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab\'); return false;"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kabupaten / Kota</label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[kdkab]', $kab, (array_key_exists('kdkab', $sess) ? $sess['kdkab'] : $this->newsession->userdata('kdkab')), 'id="kdkab" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kecamatan</label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[kdkec]', $kec, (array_key_exists('kdkec', $sess) ? $sess['kdkec'] : $this->newsession->userdata('kdkec')), 'id="kdkec" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel\');"'); ?>
              </div>
            </div><div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kelurahan / Desa</label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[kdkel]', $kel, (array_key_exists('kdkel', $sess) ? $sess['kdkel'] : $this->newsession->userdata('kdkec')), 'id="kdkel" wajib="yes" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
          	<div class="row">
              <label class="col-sm-3 control-label">Telp/Hp</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" name="siupmb[telp]" wajib="yes" value="<?= (array_key_exists('telp', $sess) ? $sess['telp'] : $this->newsession->userdata('telp')); ?>" />
              </div>
              <label class="col-xs-2 control-label">Fax</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" name="siupmb[fax]" wajib="yes" value="<?= (array_key_exists('fax', $sess) ? $sess['fax'] : $this->newsession->userdata('fax')); ?>" />
              </div>
              <label class="col-xs-2 control-label">Kode Pos</label>
              <div class="col-sm-1">
                <input type="text" class="form-control mb-10" name="siupmb[kdpos]" wajib="yes" value="<?= (array_key_exists('fax', $sess) ? $sess['kdpos'] : $this->newsession->userdata('kdpos')); ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Lokasi Perusahaan</label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[lokasi]', $lokasi, (array_key_exists('lokasi', $sess) ? $sess['lokasi'] : ''), 'id="lokasi" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Status Perusahaan</label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[status]', $status_perusahaan, (array_key_exists('status', $sess) ? $sess['status'] : ''), 'id="lokasi" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            
            <div style="height:5px;"></div>
          	<div class="row">
              <label class="col-sm-3 control-label">No. SIUP</label>
              <div class="col-sm-3">
                <input type="text" class="form-control mb-10" name="siupmb[no_siup]" wajib="yes" value="<?= (array_key_exists('no_siup', $sess) ? $sess['no_siup'] : $this->newsession->userdata('no_siup')); ?>" />
              </div>
              <label class="col-xs-3 control-label">Tanggal SIUP</label>
              <div class="col-sm-3">
                <input type="text" class="form-control mb-10 datepickers" name="siupmb[tgl_siup]" wajib="yes" value="<?= (array_key_exists('tgl_siup', $sess) ? $sess['tgl_siup'] : $this->newsession->userdata('tgl_siup')); ?>" data-date-format = "dd/mm/yyyy" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Klasifikasi SIUP</label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[jenis_siup]', $jenis_siup, (array_key_exists('jenis_siup', $sess) ? $sess['jenis_siup'] : ''), 'id="jenis_siup" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            
            <div style="height:25px;"></div>
            <div class="row">
            	<div class="col-sm-12">
                    <span class="pull-left"><button class="btn btn-sm btn-info addon-btn m-b-10"><i class="fa fa-undo"></i>Batal</button></span>
                    <span class="pull-right"><button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewsiupmb'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button></span>
                </div>
            </div>
            
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script>
	$(document).ready(function(e){ 
		$(".datepickers").datepicker({
			autoclose: true
		});
	});
</script>
