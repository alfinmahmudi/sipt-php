<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //print_r($bank);die();?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewthird" name="fnewthird" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Legalitas dan Persyaratan</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
            <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $sess['id']; ?>" />
            <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" wajib="yes" value="<?= $sess['kd_izin']; ?>" />
            <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?= $sess['tipe_permohonan']; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label">Produksi Barang dan atau Jasa <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siupbb[produksi]" wajib="yes" value="<?= $sess['produksi']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Perdagangan Barang dan atau Jasa <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siupbb[perdagangan]" wajib="yes" value="<?= $sess['perdagangan']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jenis Barang/Jasa Dagangan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siupbb[jenis_dagangan]" wajib="yes" value="<?= $sess['jenis_dagangan']; ?>" />
              </div>
            </div>          
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Hubungan Dengan Bank</b></header>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="hidden-md col-md-3">Nama Bank </th>
                        <th class="hidden-md col-md-6">Alamat Bank </th>
                        <th class="hidden-md col-md-3">Lokasi </th>
                    </tr>
                    </thead>
                    <tbody id="bodyta">
                    <tr id="trta_<?= rand(); ?>">
                      <td>
                        <input type="hidden" class="form-control mb-10" name="bank[id][]" value="<?= $bank[0]['id']; ?>" readonly="readonly"/>
                        <input type="text" class="form-control mb-10" name="bank[nama][]" value="<?= $bank[0]['nama']; ?>">
                      </td>
                      <td>
                        <textarea class="form-control mb-10" name="bank[alamat][]"><?= $bank[0]['alamat']; ?></textarea>
                      </td>
                      <td>
                        <div class="input-group m-b-10">
                          <select class="form-control m-b-10" id="lokasi" name="bank[lokasi][]">
                            <option value="">Pilih Lokasi</option>
                             <option value="01" <?php echo ($bank[0]['lokasi']=="01")?"selected":""?>>Dalam Negeri</option>
                             <option value="02" <?php echo ($bank[0]['lokasi']=="02")?"selected":""?>>Luar Negeri</option>
                          </select>
                          <span class="input-group-btn">
                            <button type="button" class="btn btn btn-info newta" id="<?=rand(); ?>" data-target = "#bodyta">
                              <i class="fa fa-plus-square"></i>
                            </button>
                          </span>
                        </div>
                      </td>
                    </tr>
                    <?php if(count($bank) > 1): for($i = 1; $i < count($bank); $i++):?>
                    <tr id="trta_<?= rand(); ?>">
                      <td>
                        <input type="hidden" class="form-control mb-10" name="bank[id][]" value="<?= $bank[$i]['id']; ?>" readonly="readonly"/>
                        <input type="text" class="form-control mb-10" name="bank[nama][]" value="<?= $bank[$i]['nama']; ?>">
                      </td>
                      <td>
                        <textarea class="form-control mb-10" name="bank[alamat][]"><?= $bank[$i]['alamat']; ?></textarea>
                      </td>
                      <td>
                        <div class="input-group m-b-10">
                          <select class="form-control m-b-10" id="lokasi" name="bank[lokasi][]">
                            <option value="">Pilih Lokasi</option>
                             <option value="01" <?php echo ($bank[$i]['lokasi']=="01")?"selected":""?>>Dalam Negeri</option>
                             <option value="02" <?php echo ($bank[$i]['lokasi']=="02")?"selected":""?>>Luar Negeri</option>
                          </select>
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-sm btn-info minta" id="'+<?= rand(); ?>+'">
                              <i class="fa fa-minus-square"></i>
                            </button>
                          </span>
                        </div>
                      </td>
                    </tr>
                    <?php endfor; endif;?>
                    </tbody>
                </table>
                </div>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if($sess['produksi']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewthird'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Pemilik/Penanggung Jawab</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
               <!--  <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectsecond" data-url = "<?= $urisecond; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> -->
                <span class="pull-right">
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewthird'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
                </span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script><script> $(document).ready(function(e){ $(".datepickers").datepicker({ autoclose: true }); });</script>
<script> 
  $(document).ready(function(e){ 
    $(".datepickers").datepicker({ autoclose: true }); 
    $(".newta").click(function(e){
            var $this = $(this);
      var $parent = $(this).closest("tr");
      $($this.attr("data-target")).append('<tr id="trta_'+<?= rand(); ?>+'"><td><input type="hidden" class="form-control mb-10" name="bank[id][]" value="" readonly="readonly"/><input type="text" class="form-control mb-10" name="bank[nama][]"></td><td><textarea class="form-control mb-10" name="bank[alamat][]" ></textarea></td><td><div class="input-group m-b-10"><select class="form-control m-b-10" id="lokasi" name="bank[lokasi][]"><option value="">Pilih Lokasi</option><option value="01">Dalam Negeri</option><option value="02">Luar Negeri</option></select><span class="input-group-btn"><button type="button" class="btn btn btn-info minta" id="'+<?= rand(); ?>+'"><i class="fa fa-minus-square"></i></button></span></div></td></tr>');
      $(".minta").click(function(e){
        var $this = $(this);
        var $parentx = $(this).closest("tr");
        if($(".modal-backdrop").length === 0){
          BootstrapDialog.confirm('Apakah anda menghapus data terpilih ?', function(r){
            if(r){
              $($parentx).remove();
            }else{
              return false;
            }
          });
        }
            });
      return false;
    });
    $(".minta").click(function(e){
      var $this = $(this);
      var $parentx = $(this).closest("tr");
      if($(".modal-backdrop").length === 0){
        BootstrapDialog.confirm('Apakah anda menghapus data terpilih ?', function(r){
          if(r){
            $($parentx).remove();
          }else{
            return false;
          }
        });
      }
    });
    return false;
  });
</script>