<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="page-head">
    <h3 class="m-b-less"> Preview Permohonan</h3>
    <span class="sub-title">
        <?= $sess['nama_izin']; ?>
    </span> </div>
<div class="wrapper">
    <form action="<?= $act; ?>" method="post" id="fpreview" name="fpreview" autocomplete = "off" data-redirect="true">
        <input type="hidden" name="data[id]" value="<?= hashids_encrypt($sess['id'], _HASHIDS_, 9); ?>">
        <input type="hidden" name="data[kd_izin]" value="<?= hashids_encrypt($sess['kd_izin'], _HASHIDS_, 9); ?>">
        <input type="hidden" name="data[no_aju]" value="<?= $sess['no_aju'] ?>">
        <input type="hidden" name="direktorat" value="<?= hashids_encrypt($sess['direktorat_id'], _HASHIDS_, 9); ?>">
        <input type="hidden" name="tipe" value="<?= base64_encode($sess['permohonan']); ?>">
        <input type="hidden" name="SEBELUM" value="<?= $sess['status'] ?>">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"> Data Permohonan</header>
                    <div class="panel-body">
                        <section class="isolate-tabs">
                            <ul class="nav nav-tabs">
                                <li class="active"> <a data-toggle="tab" href="#first">Data Permohonan</a> </li>
                                <li class=""> <a data-toggle="tab" href="#second">Data Pemilik/Penanggung Jawab</a> </li>
                                <li class=""> <a data-toggle="tab" href="#third">Data Nilai dan Kekayaan</a> </li>
                                <li class=""> <a data-toggle="tab" href="#fourth">Data Persyaratan</a> </li>
                            </ul>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div id="first" class="tab-pane active">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <b>Data Permohonan</b></header>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Status Permohonan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['permohonan']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Nomor Pengajuan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $sess['no_aju']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Tanggal Pengajuan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $sess['tgl_aju']; ?>
                            </div>
                          </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <strong><b>Data Perusahaan</b></strong></header>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['npwp']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['tipe_perusahaan']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['nm_perusahaan']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['almt_perusahaan']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdprop']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdkab']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdkec']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdkel']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kode Pos Perusahaan</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdpos']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Telp/HP Perusahaan</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['telp']; ?>
                                                            </div>
                                                        </div>                   
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Fax Perusahaan</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['fax']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">No. SIUP <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-3">
                                                                <?= $sess['no_siup']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Tanggal SIUP <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['tgl_siup']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Klasifikasi SIUP <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['jenis_siup']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <strong><b>Data Lokasi Usaha</b></strong></header>
                                                    <div class="panel-body">                                                        
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Nama Usaha <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['nm_usaha']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>  
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Alamat Usaha <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['almt_usaha']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdprop_usaha']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdkab_usaha']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdkec_usaha']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdkel_usaha']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kode Pos Usaha</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdpos_usaha']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Telp/HP Usaha</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['telp_usaha']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Fax Usaha</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['fax_usaha']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>  
                                    </div>
                                    <div id="second" class="tab-pane">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <b>Data Pemilik/Penanggung Jawab</b></header>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Jenis Identitas <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['identitas_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Nomor Identitas <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['noidentitas_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Jabatan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['jabatan_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Nama Lengkap <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['nama_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Tempat Lahir <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-3">
                                                                <?= $sess['tmpt_lahir_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Tanggal Lahir <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-3">
                                                                <?= $sess['tgl_lahir_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Alamat <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['alamat_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdprop_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdkab_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdkec_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdkel_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Telp/Hp</label>
                                                            <div class="col-sm-2">
                                                                <?= $sess['telp_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Email Penanggung Jawab</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['email_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Fax</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['fax_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Email Pengguna</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['email']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="third" class="tab-pane">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <b>Data Nilai Modal dan Kekayaan</b></header>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Data Nilai Modal dan Kekayaan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= number_format($sess['nilai_modal']); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <b>Data Kegiatan Usaha</b></header>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Wilayah Pemasaran <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $pemasaran; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Golongan Minuman Beralkohol</label>
                                                            <div class="col-sm-9">
                                                                <input type="checkbox" id="fl_gol_a" name="fl_gol_a" value="1" <?php echo ($sess['fl_gol_a'] == '1') ? "checked" : ""; ?> disabled> Golongan A &nbsp;&nbsp;&nbsp;
                                                                <input type="checkbox" id="fl_gol_b" name="fl_gol_b" value="1" <?php echo ($sess['fl_gol_b'] == '1') ? "checked" : ""; ?> disabled> Golongan B &nbsp;&nbsp;&nbsp;
                                                                <input type="checkbox" id="fl_gol_c" name="fl_gol_c" value="1" <?php echo ($sess['fl_gol_c'] == '1') ? "checked" : ""; ?> disabled> Golongan C
                                                            </div>
                                                        </div>
                                                        <?php if ($sess['fl_gol_a'] == '1'): ?>
                                                            <div style="height:10px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-12 control-label"><b>Minuman Beralkohol Golongan A</b></label>
                                                            </div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Jenis</label>
                                                                <div class="col-sm-9">
                                                                    <?= implode(",", $jns_gol_a); ?>
                                                                </div>
                                                            </div>
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Merek</label>
                                                                <div class="col-sm-9">
                                                                    <?= $sess['gol_a']; ?>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if ($sess['fl_gol_b'] == '1'): ?>
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-12 control-label"><b>Minuman Beralkohol Golongan B</b></label>
                                                            </div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Jenis</label>
                                                                <div class="col-sm-9">
                                                                    <?= implode(",", $jns_gol_b); ?>
                                                                </div>
                                                            </div>
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Merek</label>
                                                                <div class="col-sm-9">
                                                                    <?= $sess['gol_b']; ?>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if ($sess['fl_gol_c'] == '1'): ?>
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-12 control-label"><b>Minuman Beralkohol Golongan C</b></label>
                                                            </div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Jenis</label>
                                                                <div class="col-sm-9">
                                                                    <?= implode(",", $jns_gol_b); ?>
                                                                </div>
                                                            </div>
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Merek</label>
                                                                <div class="col-sm-9">
                                                                    <?= $sess['gol_c']; ?>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?> 
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="fourth" class="tab-pane">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <b>Data Legalitas dan Persyaratan</b></header>
                                                    <div class="panel-body">
                                                        <div class="row col-lg-12">
                                                            <table class="table table-striped">
                                                                <thead>
                                                                    <tr class="control-label">
                                                                        <th class="control-label" width= "3%">No</th>
                                                                        <th>Nama Dokumen</th>
                                                                        <th width= "20%">Status</th>
                                                                        <th width= "5%">&nbsp;</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php if (count($req) > 0):$no = 1;
                                                                        foreach ($req as $reqs): ?>
                                                                        <td align="center"><?php echo $no++; ?></td>
                                                                        <td><?php echo $reqs['keterangan'];
                                                                            echo ($reqs['tipe'] == '1') ? ' <font size ="2" color="red">*</font>' : ""; ?></td>
                                                                        <td><?= $reqs['uraian'] ?></td>
                                                                        <td><center>
                                                                            <div id="btn"> </div>
                                                                        </center></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td nowrap="nowrap"><div id="divHapus_<?php echo $dokWajib['idPendukung']; ?>"></div></td>
                                                                            <td colspan="3"><table width="100%" class="table table-nomargin table-condensed table-striped">
                                                                                    <tr>
                                                                                        <th><center>
                                                                                        No. Dokumen
                                                                                    </center></th>
                                                                                    <th><center>
                                                                                        Penerbit
                                                                                    </center></th>
                                                                                    <th><center>
                                                                                        Tgl. Dokumen
                                                                                    </center></th>
                                                                                    <th><center>
                                                                                        Tgl. Akhir
                                                                                    </center></th>
                                                                                    <th><center>
                                                                                        File
                                                                                    </center></th>
                                                                        </tr>
        <?php if ($sess_syarat):for ($i = 0; $i < count($sess_syarat[$reqs['dok_id']]); $i++): ?>
                                                                                <tr>
                                                                                    <td width="20%"><center>
                                                                                    <span id="no_dok_<?= $reqs['dok_id']; ?>">
                <?= $sess_syarat[$reqs['dok_id']][$i]['nomor']; ?>
                                                                                    </span>
                                                                                </center></td>
                                                                                <td width="35%"><center>
                                                                                    <span id="penerbit_<?= $reqs['dok_id']; ?>">
                <?= $sess_syarat[$reqs['dok_id']][$i]['penerbit_dok']; ?>
                                                                                    </span>
                                                                                </center></td>
                                                                                <td width="20%"><center>
                                                                                    <span id="tgl_dok_<?= $reqs['dok_id']; ?>">
                <?= $sess_syarat[$reqs['dok_id']][$i]['tgl_dok']; ?>
                                                                                    </span>
                                                                                </center></td>
                                                                                <td width="20%"><center>
                                                                                    <span id="tgl_exp_<?= $reqs['dok_id']; ?>">
                <?= $sess_syarat[$reqs['dok_id']][$i]['tgl_exp']; ?>
                                                                                    </span>
                                                                                </center></td>
                                                                                <td width="10%"><center>
                                                                                    <span id="View_<?= $reqs['dok_id']; ?>">
                <?= $sess_syarat[$reqs['dok_id']][$i]['View']; ?>
                                                                                    </span>
                                                                                </center></td>
                                                                                </tr>
            <?php endfor;
        else: ?>
                                                                            <tr>
                                                                                <td colspan="5"><center>
                                                                                <b>Tidak Terdapat Data</b>
                                                                            </center></td>
                                                                            </tr>
                                                                        <?php endif; ?>
                                                                    </table></td>
                                                                    <tr>
    <?php endforeach;
else: ?>
                                                                <tr>
                                                                    <td colspan="4"><center>
                                                                    <b>Tidak Terdapat Data</b>
                                                                </center></td>
                                                                </tr>
<?php endif; ?>
                                                            </tbody>

                                                            </table>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"> Data Proses Permohonan </header>
                    <div class="panel-body">
<?= $input; ?>
<?php
if (count($proses) > 0) {
    ?>
                            <div class="row">
                                <div class="col-md-12">
                                <?php if ($sess['status'] != '1000') { ?>
                                    <div class="form-group">
                                        <?php if ($this->newsession->userdata('role') != '05') { ?>
                                          <label for="catatan">Catatan</label>
                                          <textarea class="jqte-catatan form-control mb-10" name="catatan"  id="catatan"></textarea>
                                      <?php } ?>
                                      <label class="checkbox-custom inline check-success">
                                          <input value="1" id="checkbox-agreement" type="checkbox" check-agreement="yes"> 
                                          <label for="checkbox-agreement">
                                              <?php echo $agrement ?>
                                          </label>
                                      </label>
                                    </div>
                                    <?php }
                                    if (count($proses) > 0) {
                                        foreach ($proses as $v => $x) {
                                            //if ($v != "3") {
                                                echo $x . ' ';
                                            //} else {
                                                // echo '<button class="btn btn-sm btn-danger addon-btn m-b-10" id= "' . rand() . '" onclick="proccess(\'#fpreview\',$(this)); return false;" data-status = "0706"><i class="fa fa-undo pull-right"></i>Tolak Permohonan</button> ';
                                            //}
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                        } else {
                            if ($sess['status'] != "0104"):
                                if ($this->newsession->userdata("role") != "05"):
                                    ?>
                                    <label for="catatan">Catatan</label>
                                    <textarea class="form-control mb-10" name="catatan" wajib="yes" id="catatan"></textarea><br/>
                                    <button class="btn btn-sm btn-info addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-arrow-left pull-left"></i>Kembali</button>
                                    <?php
                                    echo $tolak . ' ' . $cetak;
                                else:
                                    ?>
                                    <button class="btn btn-sm btn-info addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-arrow-left pull-left"></i>Kembali</button>
                                <?php
                                endif;
                            else:
                                ?>	
                                <button class="btn btn-sm btn-info addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-arrow-left pull-left"></i>Kembali</button>
                                <?php
                                if ($this->newsession->userdata("role") != "05")
                                    echo $kirimUpp . ' ' . $cetak;
                            endif;
                        }
                        ?>
                        <div class="row">&nbsp;</div>
                        <div class="row">
                            <div class="col-md-12">
                                <section class="panel-timeline">
                                    <div class="time-line-wrapper">
                                        <div class="time-line-caption">
                                            <h3 class="time-line-title">Histori Permohonan <sup><a href="javascript:void(0);" id="<?= rand(); ?>" class="log-izin" data-url = "<?= $urllog; ?>" data-target="#tmplog"><span class="badge bg-info"> <?= $jmllog; ?> </span></a></sup></h3>
                                        </div>
                                        <div id="tmplog"><div>
                                            </div>
                                            </section>
                                        </div>
                                    </div>
                            </div>
                            </section>
                        </div>
                    </div>
                    </form>
            </div>
            <script src="<?= base_url(); ?>assets/bend/js/a.js?v=<?= date('Y-m-d-s'); ?>"></script>
<script>
if (<?= $sess['status']; ?> != '1000') {
                    $(document).ready(function () {
                        if (<?= $this->newsession->userdata('role') ?> == '07') {
                            BootstrapDialog.show({
                                title: '',
                                type: BootstrapDialog.TYPE_WARNING,
                                message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                            });
                        }
                        if (<?= $this->newsession->userdata('role') ?> == '06') {
                            BootstrapDialog.show({
                                title: '',
                                type: BootstrapDialog.TYPE_WARNING,
                                message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                            });
                        }
                        if (<?= $this->newsession->userdata('role') ?> == '02') {
                            BootstrapDialog.show({
                                title: '',
                                type: BootstrapDialog.TYPE_WARNING,
                                message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                            });
                        }
                        if (<?= $this->newsession->userdata('role') ?> == '01') {
                            BootstrapDialog.show({
                                title: '',
                                type: BootstrapDialog.TYPE_WARNING,
                                message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                            });
                        }
                    });
                }
</script>