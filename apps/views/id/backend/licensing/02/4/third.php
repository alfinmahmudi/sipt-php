<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewnilaimodal" name="fnewnilaimodal" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Nilai Modal dan Kekayaan</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
            <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $sess['id']; ?>" />
            <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" wajib="yes" value="<?= $sess['kd_izin']; ?>" />
            <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?= $sess['tipe_permohonan']; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label"> Data Nilai Modal dan Kekayaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 nomor" id="nilai_modal" name="nilai_modal" wajib="yes" value="<?= number_format($sess['nilai_modal']); ?>" />
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Kegiatan Usaha</b></header>
          <div class="panel-body">
            <?php/*
            <div class="row">
              <label class="col-sm-3 control-label">Kegiatan Usaha <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siupmb[kegiatan_usaha]" wajib="yes" value="<?= $sess['kegiatan_usaha']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kelembagaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('siupmb[kelembagaan]', $kelembagaan, $sess['kelembagaan'], 'id="kelembagaan" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">KBLI <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="hidden" class="form-control mb-10" id="kbli" name="siupmb[kbli]" wajib="yes" value="<?= $sess['kbli']; ?>" readonly />
                <button type="button" class="btn btn-sm btn-success addon-btn m-b-10" data-url="<?= site_url(); ?>licensing/popup_kbli/" data-target="{kbli}.{desc_kbli}" data-title = "Referensi Data KBLI" style="cursor:pointer;" id="btn-kbli" data-callback = "get.search.kbli.{kbli}" data-fieldcallback = "kbli.desc_kbli" onclick="popuptabel($(this));"><i class="fa fa-search"></i> Pilih </button>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Uraian KBLI <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" wajib="yes" id="desc_kbli" name="siupmb[desc_kbli]" readonly><?= $sess['desc_kbli']; ?></textarea>
              </div>
            </div>
            <div style="height:5px;"></div>
            */?>
            <!-- <?php if($sess['kd_izin'] == '4'):?>
            
            <div class="row">
              <label class="col-sm-3 control-label">Wilayah Pemasaran <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_multiselect('pemasaran[]', $propinsi, $pemasaran, 'id="pemasaran" wajib="yes" multiple class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" '); ?>
              </div>
            </div>
            <?php elseif($sess['kd_izin'] == '5'):?>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Wilayah Pemasaran <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_multiselect('pemasaran[]', $kab, $pemasaran, 'id="pemasaran" multiple class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" '); ?>
              </div>
            </div>
            <?php endif;?> -->
            <div class="row">
              <label class="col-sm-3 control-label">Wilayah Pemasaran <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="radio" name="indo" id="indo" value="00" onclick="show_kab('00')" > Seluruh Indonesia<br>
                <input type="radio" name="indo" id="prop" onclick="show_kab('11')" > Propinsi / Kabupaten
              </div>
            </div>
            <div style="height:10px;"></div>
            <div class="row" id="kab" style="display: none;">
              <label class="col-sm-3 control-label">&nbsp;</label>
              <div class="col-sm-9">
                <?= form_multiselect('pemasaran[]', $propinsi, $pemasaran, 'id="pemasaran" wajib="yes" multiple class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" '); ?>
              </div>
            </div>
            <div style="height:10px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Golongan Minuman Beralkohol </label>
              <div class="col-sm-9">
                <input type="checkbox" id="fl_gol_a" name="fl_gol_a" value="1" <?php echo ($sess['fl_gol_a'] == '1')?"checked":"";?> onclick="showHideText(this,'gola')"> Golongan A &nbsp;&nbsp;&nbsp; <b>(Kurang dari 5%)</b> <br>
                <input type="checkbox" id="fl_gol_b" name="fl_gol_b" value="1" <?php echo ($sess['fl_gol_b'] == '1')?"checked":"";?> onclick="showHideText(this,'golb')"> Golongan B &nbsp;&nbsp;&nbsp; <b>(5% s.d. 20%)</b> <br>
                <input type="checkbox" id="fl_gol_c" name="fl_gol_c" value="1" <?php echo ($sess['fl_gol_c'] == '1')?"checked":"";?> onclick="showHideText(this,'golc')"> Golongan C &nbsp;&nbsp;&nbsp; <b>(20% s.d. 55%)</b> 
              </div>
            </div>
             <div id = "gola"  style="<?php echo ($sess['fl_gol_a'] == '1')?"":"display:none;";?>">
              <div style="height:5px;"></div>
				<!-- <div class="row">
					<label class="col-sm-2 control-label">Jenis Minuman Beralkohol Golongan A <font size ="2" color="red">*</font></label>
					<div class="col-sm-3">
					  <?= form_multiselect('jns_gol_a[]', $jns_a, $jns_gol_a, 'id="jns_gol_a" wajib="yes" class="form-control input-sm select2-allow-clear select2-offscreen"'); ?>
					</div>
					<label class="col-sm-2 control-label">Merek Minuman Beralkohol Golongan A </label>
					<div class="col-sm-3">
					  <input type="text" class="form-control mb-10" name="siupmb[gol_a]" value="<?= $sess['gol_a']; ?>" />
					</div>
				</div> -->
            <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="col-md-6">Jenis Minuman Beralkohol Golongan A</th>
                                <th class="col-md-6">Merek Minuman Beralkohol Golongan A</th>
                            </tr>
                        </thead>
                        <tbody class="bodyta_a">
                            <!-- <input type="hidden" class="form-control input-sm" name="data[permohonan_id]" value="<?= $permohonan_id; ?>" readonly="readonly" />
                            <input type="hidden" class="form-control input-sm" name="data[kd_izin]" value="<?= hashids_encrypt($izin_id,_HASHIDS_,9); ?>" readonly="readonly" /> -->
                            <tr id="trta_<?= rand(); ?>">
                                <td>
                                    <input type="hidden" class="form-control input-sm" name="jns_gol_a[id][]" value="" readonly="readonly"/>
                                    <?= form_dropdown('jns_gol_a[jns_a][]', $jns_adrop, $jns_gol_a[0], 'id="jns_c" wajib="yes" class="form-control input-sm select2" '); ?>
                                <td>
                                    <div class="input-group m-b-10">
                                        <input type="text" class="form-control input-sm" name="jns_gol_a[merk][]" value="<?= $merk_gol_a[0]; ?>"> <span class="input-group-btn"> <button type="button"  class="btn btn-sm btn-info siup_a" id="<?=rand(); ?>" data-target = "#bodyta"> <i class="fa fa-plus-square"></i> </button> </span> </div>
                                </td>
                            </tr>
                            <?php if(count($jns_gol_a)>1): for($i = 1; $i < count($jns_gol_a); $i++):?>
                                <tr id="trta_<?= rand(); ?>">
                                    <td>
                                        <!-- <input type="hidden" class="form-control input-sm" name="jns_gol_a[id][]" value="<?= $jns_gol_a[$i]['id']; ?>" readonly="readonly" /> -->
                                        <?= form_dropdown('jns_gol_a[jns_a][]', $jns_adrop, $jns_gol_a[$i], 'id="jns_c" wajib="yes" class="form-control input-sm select2" '); ?>
                                    <td>
                                        <div class="input-group m-b-10">
                                            <input type="text" class="form-control input-sm" name="jns_gol_a[merk][]" value="<?= $merk_gol_a[$i]; ?>"> <span class="input-group-btn"> <button type="button" class="btn btn-sm btn-info minta" id="'+<?= rand(); ?>+'"> <i class="fa fa-minus-square"></i> </button> </span> </div>
                                    </td>
                                </tr>
                                <?php endfor; endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id = "golb"  style="<?php echo ($sess['fl_gol_b'] == '1')?"":"display:none;";?>">
              <div style="height:5px;"></div>
              <!-- <div class="row">
                <label class="col-sm-2 control-label">Jenis Minuman Beralkohol Golongan B <font size ="2" color="red">*</font></label>
                <div class="col-sm-3">
                  <?= form_multiselect('jns_gol_b[]', $jns_b, $jns_gol_b, 'id="jns_gol_b" wajib="yes" class="form-control input-sm select2-allow-clear select2-offscreen"'); ?>
                </div>
                <label class="col-sm-2 control-label">Merek Minuman Beralkohol Golongan B</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control mb-10" name="siupmb[gol_b]" value="<?= $sess['gol_b']; ?>" />
                </div>
              </div> -->
              <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="col-md-6">Jenis Minuman Beralkohol Golongan B</th>
                                <th class="col-md-6">Merek Minuman Beralkohol Golongan B</th>
                            </tr>
                        </thead>
                        <tbody class="bodyta_b">
                            <!-- <input type="hidden" class="form-control input-sm" name="data[permohonan_id]" value="<?= $permohonan_id; ?>" readonly="readonly" />
                            <input type="hidden" class="form-control input-sm" name="data[kd_izin]" value="<?= hashids_encrypt($izin_id,_HASHIDS_,9); ?>" readonly="readonly" /> -->
                            <tr id="trta_<?= rand(); ?>">
                                <td>
                                    <input type="hidden" class="form-control input-sm" name="jns_gol_b[id][]" value="" readonly="readonly"/>
                                    <?= form_dropdown('jns_gol_b[jns_b][]', $jns_bdrop, $jns_gol_b[0], 'id="jns_gol_b" wajib="yes" class="form-control input-sm select2" '); ?>
                                <td>
                                    <div class="input-group m-b-10">
                                        <input type="text" class="form-control input-sm" name="jns_gol_b[merk][]" value="<?= $merk_gol_b[0]; ?>"> <span class="input-group-btn"> <button type="button"  class="btn btn-sm btn-info siup_b" id="<?=rand(); ?>" data-target = "#bodyta"> <i class="fa fa-plus-square"></i> </button> </span> </div>
                                </td>
                            </tr>
                            <?php if(count($jns_gol_b)>1): for($i = 1; $i < count($jns_gol_b); $i++):?>
                                <tr id="trta_<?= rand(); ?>">
                                    <td>
                                        <!-- <input type="hidden" class="form-control input-sm" name="jns_gol_b[id][]" value="<?= $jns_gol_b[$i]; ?>" readonly="readonly" /> -->
                                        <?= form_dropdown('jns_gol_b[jns_b][]', $jns_bdrop, $jns_gol_b[$i], 'id="jns_gol_b" wajib="yes" class="form-control input-sm select2" '); ?>
                                    <td>
                                        <div class="input-group m-b-10">
                                            <input type="text" class="form-control input-sm" name="jns_gol_b[merk][]" value="<?= $merk_gol_b[$i]; ?>"> <span class="input-group-btn"> <button type="button" class="btn btn-sm btn-info minta" id="'+<?= rand(); ?>+'"> <i class="fa fa-minus-square"></i> </button> </span> </div>
                                    </td>
                                </tr>
                                <?php endfor; endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
             <div id = "golc"  style="<?php echo ($sess['fl_gol_c'] == '1')?"":"display:none;";?>height:5px;">
              <div style="height:5px;"></div>
                <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="col-md-6">Jenis Minuman Beralkohol Golongan C</th>
                                <th class="col-md-6">Merek Minuman Beralkohol Golongan C</th>
                            </tr>
                        </thead>
                        <tbody class="bodyta_c">
                            <!-- <input type="hidden" class="form-control input-sm" name="data[permohonan_id]" value="<?= $permohonan_id; ?>" readonly="readonly" />
                            <input type="hidden" class="form-control input-sm" name="data[kd_izin]" value="<?= hashids_encrypt($izin_id,_HASHIDS_,9); ?>" readonly="readonly" /> -->
                            <tr id="trta_<?= rand(); ?>">
                                <td>
                                    <input type="hidden" class="form-control input-sm" name="jns_gol_c[id][]" value="" readonly="readonly"/>
                                    <?= form_dropdown('jns_gol_c[jns_c][]', $jns_cdrop, $jns_gol_c[0], 'id="jns_gol_c" wajib="yes" class="form-control input-sm select2" '); ?>
                                <td>
                                    <div class="input-group m-b-10">
                                        <input type="text" class="form-control input-sm" name="jns_gol_c[merk][]" value="<?= $merk_gol_c[0]; ?>"> <span class="input-group-btn"> <button type="button"  class="btn btn-sm btn-info siup_c" id="<?=rand(); ?>" data-target = "#bodyta"> <i class="fa fa-plus-square"></i> </button> </span> </div>
                                </td>
                            </tr>
                            <?php if(count($jns_gol_c)>1): for($i = 1; $i < count($jns_gol_c); $i++):?>
                                <tr id="trta_<?= rand(); ?>">
                                    <td>
                                        <!-- <input type="hidden" class="form-control input-sm" name="jns_gol_c[id][]" value="<?= $jns_gol_c[$i]; ?>" readonly="readonly" /> -->
                                        <?= form_dropdown('jns_gol_c[jns_c][]', $jns_cdrop, $jns_gol_c[$i], 'id="jns_gol_c" wajib="yes" class="form-control input-sm select2" '); ?>
                                    <td>
                                        <div class="input-group m-b-10">
                                            <input type="text" class="form-control input-sm" name="jns_gol_c[merek][]" value="<?= $merk_gol_c[$i]; ?>"> <span class="input-group-btn"> <button type="button" class="btn btn-sm btn-info minta" id="'+<?= rand(); ?>+'"> <i class="fa fa-minus-square"></i> </button> </span> </div>
                                    </td>
                                </tr>
                                <?php endfor; endif;?>
                        </tbody>
                    </table>
                </div>
             </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if($sess['nilai_modal']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewnilaimodal'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Pemilik/Penanggung Jawab</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
               <!--  <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectsecond" data-url = "<?= $urisecond; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> -->
                <span class="pull-right">
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewnilaimodal'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
                </span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <?php /*
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Hubungan Dengan Bank</b></header>
          <div class="panel-body">
            <div class="row">
              <label class="col-sm-3 control-label">Nama Bank 1</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siupmb[bank1]" value="<?= $sess['bank1']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Bank 1</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siupmb[alamat_bank1]" value="<?= $sess['alamat_bank1']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nama Bank 2</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siupmb[bank2]" value="<?= $sess['bank2']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Bank 2</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siupmb[alamat_bank2]" value="<?= $sess['alamat_bank2']; ?>" />
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    */?>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/jquery.formatCurrency-1.4.0.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/multiple-select.js"></script>
<script> 
$(document).ready(function(e){ 
  $(".datepickers").datepicker({ 
    autoclose: true 
  });
  get_mask_number('nilai_modal',''); 
  $(".select2-allow-clear").select2({
	placeholder:"Pilih Jenis Minuman"
  });

  var indo = '<?= $pemasaran[0]; ?>';
  if (indo == '00' && indo != '') {
    $('#indo').click();
  }else{
    $('#prop').click();
  }

});
</script>
<script>
function show_kab(a){
  if (a == '11') {
    $('#kab').show();
    $('#pemasaran').attr('wajib', 'yes');
  }else{
    $('#kab').hide();
    $('#pemasaran').attr('wajib', 'no');
    $("#pemasaran").select2("val", "");
  }
}

function showHideText(boxName,divName) {
  if(boxName.checked == true) {
    $('#'+divName).show();
  }
  else {
    $('#'+divName).hide();
  }
}

$(document).ready(function(e) {
            $(".siup_a").click(function(e) {
                var $this = $(this);
                var $parent = $(this).closest("tr");
                var temp = <?= json_encode($jns_a); ?>;
                //console.log(temp.length);
                // alert(temp.length);
                var appe = '<tr id="trta_' + <?= rand(); ?> + '"><td><input type="hidden" class="form-control input-sm" name="jns_gol_a[id][]" value="" readonly="readonly"/><select name="jns_gol_a[jns_a][]" id="jns_a" wajib="yes" class="form-control input-sm select2">';
                for (var i = 0; i < temp.length; i++) {
                appe += '<option value='+temp[i].id+'>'+temp[i].jenis+'</option>';
                }
                appe += '</select></td>';
                appe += '<td><div class="input-group m-b-10"><input type="text" class="form-control input-sm" name="jns_gol_a[merk][]"><span class="input-group-btn"><button type="button" class="btn btn-sm btn-info minta" id="'+ <?= rand(); ?> +'"><i class="fa fa-minus-square"></i></button></span></div></td></tr>';
                $(".bodyta_a").append(appe);
                // $(".bodyta").append('<tr id="trta_' + <?= rand(); ?> + '"><td><input type="hidden" class="form-control input-sm" name="tembusan[id][]" value="" readonly="readonly"/><select>"+for (var i = temp.length - 1; i >= 0; i--) {+"<option value="+temp[i].id+">"+temp[i].jenis+"</option>"+}+"</select><input type="text" class="form-control input-sm" name="tembusan[keterangan][]"><span class="input-group-btn"><button type="button" class="btn btn-sm btn-info minta" id="' + <?= rand(); ?> + '"><i class="fa fa-minus-square"></i></button></span></div></td></tr>');
                $(".minta").click(function(e) {
                    var $this = $(this);
                    var $parentx = $(this).closest("tr");
                    $($parentx).remove();
                });
                return false;
            });
            $(".minta").click(function(e) {
                var $this = $(this);
                var $parentx = $(this).closest("tr");
                $($parentx).remove();
            });
            return false;
        });

  $(document).ready(function(e) {
            $(".siup_b").click(function(e) {
                var $this = $(this);
                var $parent = $(this).closest("tr");
                var temp = <?= json_encode($jns_b); ?>;
                //console.log(temp);
                var appe = '<tr id="trta_' + <?= rand(); ?> + '"><td><input type="hidden" class="form-control input-sm" name="jns_gol_b[id][]" value="" readonly="readonly"/><select name="jns_gol_b[jns_b][]" id="jns_c" wajib="yes" class="form-control input-sm select2">';
                for (var i = 0; i < temp.length; i++) {
                appe += '<option value='+temp[i].id+'>'+temp[i].jenis+'</option>';
                }
                appe += '</select></td>';
                appe += '<td><div class="input-group m-b-10"><input type="text" class="form-control input-sm" name="jns_gol_b[merk][]"><span class="input-group-btn"><button type="button" class="btn btn-sm btn-info minta" id="'+ <?= rand(); ?> +'"><i class="fa fa-minus-square"></i></button></span></div></td></tr>';
                $(".bodyta_b").append(appe);
                // $(".bodyta").append('<tr id="trta_' + <?= rand(); ?> + '"><td><input type="hidden" class="form-control input-sm" name="tembusan[id][]" value="" readonly="readonly"/><select>"+for (var i = temp.length - 1; i >= 0; i--) {+"<option value="+temp[i].id+">"+temp[i].jenis+"</option>"+}+"</select><input type="text" class="form-control input-sm" name="tembusan[keterangan][]"><span class="input-group-btn"><button type="button" class="btn btn-sm btn-info minta" id="' + <?= rand(); ?> + '"><i class="fa fa-minus-square"></i></button></span></div></td></tr>');
                $(".minta").click(function(e) {
                    var $this = $(this);
                    var $parentx = $(this).closest("tr");
                    $($parentx).remove();
                });
                return false;
            });
            $(".minta").click(function(e) {
                var $this = $(this);
                var $parentx = $(this).closest("tr");
                $($parentx).remove();
            });
            return false;
        });

  $(document).ready(function(e) {
            $(".siup_c").click(function(e) {
                var $this = $(this);
                var $parent = $(this).closest("tr");
                var temp = <?= json_encode($jns_c); ?>;
                //console.log(temp);
                var appe = '<tr id="trta_' + <?= rand(); ?> + '"><td><input type="hidden" class="form-control input-sm" name="jns_gol_c[id][]" value="" readonly="readonly"/><select name="jns_gol_c[jns_c][]" id="jns_c" wajib="yes" class="form-control input-sm select2">';
                for (var i = 0; i < temp.length; i++) {
                appe += '<option value='+temp[i].id+'>'+temp[i].jenis+'</option>';
                }
                appe += '</select></td>';
                appe += '<td><div class="input-group m-b-10"><input type="text" class="form-control input-sm" name="jns_gol_c[merk][]"><span class="input-group-btn"><button type="button" class="btn btn-sm btn-info minta" id="'+ <?= rand(); ?> +'"><i class="fa fa-minus-square"></i></button></span></div></td></tr>';
                $(".bodyta_c").append(appe);
                // $(".bodyta").append('<tr id="trta_' + <?= rand(); ?> + '"><td><input type="hidden" class="form-control input-sm" name="tembusan[id][]" value="" readonly="readonly"/><select>"+for (var i = temp.length - 1; i >= 0; i--) {+"<option value="+temp[i].id+">"+temp[i].jenis+"</option>"+}+"</select><input type="text" class="form-control input-sm" name="tembusan[keterangan][]"><span class="input-group-btn"><button type="button" class="btn btn-sm btn-info minta" id="' + <?= rand(); ?> + '"><i class="fa fa-minus-square"></i></button></span></div></td></tr>');
                $(".minta").click(function(e) {
                    var $this = $(this);
                    var $parentx = $(this).closest("tr");
                    $($parentx).remove();
                });
                return false;
            });
            $(".minta").click(function(e) {
                var $this = $(this);
                var $parentx = $(this).closest("tr");
                $($parentx).remove();
            });
            return false;
        });
  $('.nomor').keydown(function (event) {
            var a = event.keyCode;
            var b = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 9, 11, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106];
            if (jQuery.inArray(a, b) === -1) {
                event.preventDefault();
            }
        });
</script>