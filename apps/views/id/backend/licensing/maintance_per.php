<?php if ( ! defined( 'BASEPATH')) exit( 'No direct script access allowed'); ?>
    <div id="main-wrapper">
        <form id="ftembusan" name="ftembusan" data-redirect="true" action="<?= $act; ?>" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <div class="panel-title">Pengaturan Persyaratan
                                <br>
                                <br>
                                <?= $nama_izin['nama_izin'];?>
                            </div>
                            <div class="panel-control">&nbsp;</div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <!-- <th class="col-md-1">Kode</th> -->
                                                <th class="col-md-3">Kode dan Keterangan</th>
                                                <th class="col-md-1">Kategori</th>
                                                <th class="col-md-1">Urutan</th>
                                                <th class="col-md-1">Tipe Permohonan</th>
                                                <th class="col-md-1"></th>
                                                <th class="col-md-1"></th>
                                                <th class="col-md-1"></th>
                                                <th class="col-md-1"></th>
                                            </tr>
                                        </thead>
                                        <tbody class="bodyta">
                                            <input type="hidden" class="form-control input-sm" name="data[permohonan_id]" value="<?= $permohonan_id; ?>" readonly="readonly" />
                                            <input type="hidden" class="form-control input-sm" name="data[kd_izin]" value="<?= hashids_encrypt($izin_id,_HASHIDS_,9); ?>" readonly="readonly" />
                                            <tr id="trta_<?= rand(); ?>">
                                                <!-- <td>
                                                    
                                                    <input type="text" class="form-control input-sm" name="persyaratan[kode][]" value="<?= $sess[0]['kode']; ?>"> 
                                                </td> -->
                                                <td>
                                                    <input type="hidden" class="form-control input-sm" name="persyaratan[0][id]" value="<?= $sess[0]['id']; ?>" readonly="readonly" />
                                                    <input type="hidden" class="form-control input-sm" name="banyak" id="banyak" value="<?= count($sess); ?>" readonly="readonly" />
                                                    <input type="hidden" id="flag" value="1">
                                                    <!-- <?= form_dropdown('persyaratan[0][keterangan]', $keterangan, $sess[0]['dok_id'], 'id="jns_gol_b" wajib="yes" class="form-control input-sm select2" '); ?> -->
                                                    <select name="persyaratan[0][keterangan]" id="jns_a_1" wajib="yes" class="form-control input-sm select2">
                                                    <?php foreach ($keterangan as $key) { 
                                                        if ($key['id'] == $sess[0]['dok_id']) { $select = "selected"; }?>
                                                        <option value='<?= $key['id']; ?>' <?php if($sess[0]['dok_id'] == $key['id']) echo "selected";?> ><?= $key['keterangan']; ?></option>
                                                    <?php }  ?>
                                                <td>
                                                    <!-- <?= form_dropdown('persyaratan[0][kategori]', $kategori, $sess[0]['kategori'], 'id="jns_gol_b" wajib="yes" class="form-control input-sm select2" '); ?> -->
                                                    <select name="persyaratan[0][kategori]" id="jns_a" wajib="yes" class="form-control input-sm select2">
                                                    <?php foreach ($kategori as $key) { ?>
                                                        <option value='<?= $key['kode']; ?>' <?php if($sess[0]['kategori'] == $key['kode']) echo "selected";?> ><?= $key['uraian']; ?></option>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <input type="text" name="persyaratan[0][urutan]" value="<?= $sess[0]['urutan']; ?>">
                                                </td>
                                                <td>
                                                    <label class="checkbox-custom inline check-success">
                                                    <input type="checkbox" name="persyaratan[0][baru]" value="1" id="checkbox1" <?php if($sess[0]['baru']=="1") echo "checked";?> > <label for="checkbox1">Baru</label>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label class="checkbox-custom inline check-success">
                                                    <input type="checkbox" name="persyaratan[0][perpanjangan]" value="1" id="checkbox2" <?php if($sess[0]['perpanjangan']=="1") echo "checked";?> > <label for="checkbox2">Perpanjangan</label>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label class="checkbox-custom inline check-success">
                                                    <input type="checkbox" name="persyaratan[0][perubahan]" value="1" id="checkbox3" <?php if($sess[0]['perubahan']=="1") echo "checked";?> > <label for="checkbox3">Perubahan</label>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label class="checkbox-custom inline check-success">
                                                    <input type="checkbox" name="persyaratan[0][multi]" value="1" id="checkbox5" <?php if($sess[0]['multi']=="1") echo "checked";?> > <label for="checkbox5">Multi</label>
                                                    </label>
                                                </td>
                                                <td>
                                                    <div class="input-group m-b-3">
                                                        <label class="checkbox-custom inline check-success">
                                                            <input type="checkbox" name="persyaratan[0][tipe]" value="1" id="checkbox4" <?php if($sess[0]['tipe']=="1") echo "checked";?> > <label for="checkbox4">Mandatory</label>
                                                        </label>
                                                        <span class="input-group-btn"> <button type="button"  class="btn btn-sm btn-info newta" id="<?=rand(); ?>" data-target = "#bodyta"> <i class="fa fa-plus-square"></i> </button> </span> 
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php if(count($sess)>1): for($i = 1; $i < count($sess); $i++):?>
                                                <?php $z = (int)$i+1; $itung = count($sess); ?>
                                                <tr id="trta_<?= rand(); ?>">
                                                    <!-- <td>
                                                        
                                                        <input type="text" class="form-control input-sm" name="persyaratan[kode][]" value="<?= $sess[$i]['kode']; ?>"> 
                                                    </td> -->
                                                    <td>
                                                    <input type="hidden" class="form-control input-sm" name="persyaratan[<?= $i; ?>][id]" value="<?= $sess[$i]['id']; ?>" readonly="readonly" />
                                                    <select name="persyaratan[<?= $i; ?>][keterangan]" id="jns_a_<?= $z; ?>" wajib="yes" class="form-control input-sm select2">
                                                    <?php foreach ($keterangan as $key) { ?>
                                                        <option value='<?= $key['id']; ?>' <?php if($sess[$i]['dok_id'] == $key['id']) echo "selected";?> ><?= $key['keterangan']; ?></option>
                                                    <?php } ?> 
                                                    </td>
                                                    <td>
                                                        <select name="persyaratan[<?= $i; ?>][kategori]" id="jns_a" wajib="yes" class="form-control input-sm select2">
                                                        <?php foreach ($kategori as $key) { ?>
                                                            <option value='<?= $key['kode']; ?>' <?php if($sess[$i]['kategori'] == $key['kode']) echo "selected";?> ><?= $key['uraian']; ?></option>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="persyaratan[<?= $i; ?>][urutan]" value="<?= $sess[$i]['urutan']; ?>">
                                                    </td>
                                                    <td>
                                                        <label class="checkbox-custom inline check-success">
                                                        <input type="checkbox" name="persyaratan[<?= $i; ?>][baru]" value="1" id="checkbox<?= $i+100; ?>" <?php if($sess[$i]['baru']=="1") echo "checked";?> > <label for="checkbox<?= $i+100; ?>">Baru</label>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label class="checkbox-custom inline check-success">
                                                        <input type="checkbox" name="persyaratan[<?= $i; ?>][perpanjangan]" value="1" id="checkbox<?= $i+200; ?>" <?php if($sess[$i]['perpanjangan']=="1") echo "checked";?> > <label for="checkbox<?= $i+200; ?>">Perpanjangan</label>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label class="checkbox-custom inline check-success">
                                                        <input type="checkbox" name="persyaratan[<?= $i; ?>][perubahan]" value="1" id="checkbox<?= $i+300; ?>" <?php if($sess[$i]['perubahan']=="1") echo "checked";?> > <label for="checkbox<?= $i+300; ?>">Perubahan</label>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label class="checkbox-custom inline check-success">
                                                        <input type="checkbox" name="persyaratan[<?= $i; ?>][multi]" value="1" id="checkbox<?= $i+500; ?>" <?php if($sess[$i]['multi']=="1") echo "checked";?> > <label for="checkbox<?= $i+500; ?>">Multi</label>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <div class="input-group m-b-10">
                                                            <label class="checkbox-custom inline check-success">
                                                                <input type="checkbox" name="persyaratan[<?= $i; ?>][tipe]" value="1" id="checkbox<?= $i+400; ?>" <?php if($sess[$i]['tipe']=="1") echo "checked";?> > <label for="checkbox<?= $i+400; ?>">Mandatory</label>
                                                            </label> 
                                                            <span class="input-group-btn"> <button type="button" onclick="kurang('<?= $sess[$i]['id']; ?>')" class="btn btn-sm btn-info minta" id="'+<?= rand(); ?>+'"> <i class="fa fa-minus-square"></i> </button> </span> </div>
                                                    </td>
                                                </tr>
                                                <?php endfor; endif;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-white">
                        <div class="panel-body">
                            <button type="button" class="btn btn-default btn-addon m-b-sm btn-sm" onclick="closedialog();"><i class="fa fa-undo"></i> Batal</button>
                            <button type="button" class="btn btn-info btn-addon m-b-sm btn-sm" onclick="post('#ftembusan'); return false;"><i class="fa fa-check-square-o"></i> Simpan</button>
                            <!-- <button class="btn btn-sm btn-success addon-btn m-b-10" ><i class="fa fa-arrow-right"></i>Simpan</button> -->
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script src="<?= base_url(); ?>assets/bend/js/sipt.js?v=<?= date(" Ymdhis "); ?>"></script>
    <script>
        $(document).ready(function(e) {
            ab = $('#banyak').val();
            for (var i = 0; i > <?= $itung; ?>; i++) {
                console.log(i);
                $('#jns_a_'+i).select2();
            }

            $(".newta").click(function(e) {
                var $this = $(this);
                var $parent = $(this).closest("tr");
                var arrket = <?= json_encode($arrket); ?>;
                var arrkat = <?= json_encode($arrkat); ?>;
                var a = Math.random();
                var b = Math.random();
                var c = Math.random();
                var d = Math.random();
                var e = Math.random();
                var flag = $('#flag').val();
                var temp = '<tr id="trta_<?= rand(); ?>">';
                temp += '<td><input type="hidden" class="form-control input-sm" name="persyaratan['+ab+'][id]" readonly="readonly" /><select name="persyaratan['+ab+'][keterangan]" id="jns_a" wajib="yes" class="form-control input-sm select2">';
                for (var i = 0; i < arrket.length; i++) {
                    temp += '<option value='+arrket[i].id+'>'+arrket[i].keterangan+'</option>';
                }
                temp += '</td>';
                temp += '<td><select name="persyaratan['+ab+'][kategori]" id="jns_a" wajib="yes" class="form-control input-sm select2">';
                for (var i = 0; i < arrkat.length; i++) {
                    temp += '<option value='+arrkat[i].kode+'>'+arrkat[i].uraian+'</option>';
                }
                temp += '</td><td><input type="text" name="persyaratan['+ab+'][urutan]"></td>';
                temp += '<td><label class="checkbox-custom inline check-success"><input type="checkbox" name="persyaratan['+ab+'][baru]" value="1" id="checkbox'+a+'"><label for="checkbox'+a+'">Baru</label></label></td> <td><label class="checkbox-custom inline check-success"><input type="checkbox" name="persyaratan['+ab+'][perpanjangan]" value="1" id="checkbox'+b+'"><label for="checkbox'+b+'">Perpanjangan</label></label></td> <td><label class="checkbox-custom inline check-success"><input type="checkbox" name="persyaratan['+ab+'][perubahan]" value="1" id="checkbox'+c+'"> <label for="checkbox'+c+'">Perubahan</label></label></td> <td><label class="checkbox-custom inline check-success"><input type="checkbox" name="persyaratan['+ab+'][multi]" value="1" id="checkbox'+e+'"> <label for="checkbox'+e+'">Multi</label></label></td> <td><div class="input-group m-b-3"><label class="checkbox-custom inline check-success"><input type="checkbox" name="persyaratan['+ab+'][tipe]" value="1" id="checkbox'+d+'"><label for="checkbox'+d+'">Mandatory</label></label><span class="input-group-btn"><button type="button" class="btn btn-sm btn-info minta" id="' + <?= rand(); ?> + '"><i class="fa fa-minus-square"></i></button></span></div></td></tr>';
                    $(".bodyta").append(temp);
                    ab++;
                // $(".bodyta").append('<tr id="trta_' + <?= rand(); ?> + '"><td><input type="hidden" class="form-control input-sm" name="tembusan[id][]" value="" readonly="readonly"/><input type="text" class="form-control input-sm" name="tembusan[urutan][]"></td><td><div class="input-group m-b-10"><input type="text" class="form-control input-sm" name="tembusan[keterangan][]"><span class="input-group-btn"><button type="button" class="btn btn-sm btn-info minta" id="' + <?= rand(); ?> + '"><i class="fa fa-minus-square"></i></button></span></div></td></tr>');
                $(".minta").click(function(e) {
                    var $this = $(this);
                    var $parentx = $(this).closest("tr");
                    $($parentx).remove();
                });
                return false;
            });
            $(".minta").click(function(e) {
                var $this = $(this);
                var $parentx = $(this).closest("tr");
                $($parentx).remove();
            });
            return false;
        });

        function closedialog() {
            BootstrapDialog.closeAll();
            return false;
        };

        function kurang(id) {
            $.ajax({
                url : '<?= site_url('licensing/delete_lic') ?>',
                data : 'id='+id,
                type : 'post',
                success : function(){

                }
            });

        }
    </script>