<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewpj" name="fnewpj" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Prinsipal Produsen</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
            <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $id; ?>" />
            <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" wajib="yes" value="<?= $kd_izin; ?>" />
            <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?= $tipe_permohonan; ?>" />
            <input type="hidden" class="form-control mb-10" id="produksi" name="produksi" value="<?= $sess['produksi']; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="prinsipal[nm_perusahaan_prod]" wajib="yes" value="<?= $sess['nm_perusahaan_prod']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Bentuk Badan Hukum <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('prinsipal[tipe_perusahaan_prod]', $tipe_perusahaan, $sess['tipe_perusahaan_prod'], 'id="tipe_perusahaan_prod" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tanggal Pendirian <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
               <input type="text" class="form-control mb-10 datepickers" name="prinsipal[tgl_pendirian_prod]" wajib="yes" value="<?= $sess['tgl_pendirian_prod']; ?>" data-date-format = "yyyy-mm-dd" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Kantor Pusat <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" wajib="yes" name="prinsipal[almt_perusahaan_prod]"><?= $sess['almt_perusahaan_prod']; ?></textarea>
              </div>
            </div>
            <div id="prov_prod" style="<?php echo ($sess['produksi'] == '01')?"":"display:none;";?>">
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('prinsipal[kdprop_prod]', $propinsi, $sess['kdprop_prod'], 'id="kdprop_prod" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab_prod\'); return false;"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('prinsipal[kdkab_prod]', $kab_prod, $sess['kdkab_prod'], 'id="kdkab_prod" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec_prod\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('prinsipal[kdkec_prod]', $kec_prod, $sess['kdkec_prod'], 'id="kdkec_prod" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel_prod\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('prinsipal[kdkel_prod]', $kel_prod, $sess['kdkel_prod'], 'id="kdkel_prod" wajib="yes" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telepon</label>
              <div class="col-sm-2">
                 <input type="text" class="form-control mb-10" name="prinsipal[telp_prod]" value="<?= $sess['telp_prod']; ?>" />
              </div>
              <label class="col-xs-2 control-label">Fax</label>
              <div class="col-sm-2">
                 <input type="text" class="form-control mb-10" name="prinsipal[fax_prod]" value="<?= $sess['fax_prod']; ?>" />
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">
                <section class="panel">
                  <header class="panel-heading"> <b>Data Legalitas</b></header>
                  <div class="panel-body">
                    <div class="row col-lg-12">
                      <input type="hidden" name="SIUPMB[izin_id]" value="<?= $izin_id;?>" readonly="readonly"/>
                      <input type="hidden" name="SIUPMB[permohonan_id]" value="<?= $permohonan_id;?>"/>
                      <table class="table">
                        <thead>
                          <tr class="control-label">
                            <th class="control-label" width= "3%">No</th>
                            <th>Nama Dokumen</th>
                            <th>Status</th>
                            <th><center>
                                Pilih
                              </center></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php if(count($req) > 0):$no = 1;foreach($req as $reqs):?>
                        <td align="center"><?php echo $no++; ?></td>
                          <td><?php echo $reqs['keterangan']; echo ($reqs['tipe'] =='1')?' <font size ="2" color="red">*</font>':""; ?></td>
                          <td><?= $reqs['uraian'];?></td>
                          <td><center>
                              <div id="btn">
                                <button type="button" class="btn btn-sm btn-success addon-btn m-b-10" data-url="<?= site_url(); ?>licensing/popup_syarat/<?= $reqs['dok_id']; ?>/" data-target="{upload_id_<?= $reqs['dok_id']; ?>}.{no_dok_<?= $reqs['dok_id']; ?>}.{penerbit_<?= $reqs['dok_id']; ?>}.{tgl_dok_<?= $reqs['dok_id']; ?>}.{tgl_exp_<?= $reqs['dok_id']; ?>}.{View_<?= $reqs['dok_id']; ?>}" data-title = "List Data <?= $reqs['keterangan']?>" style="cursor:pointer;" id="btn-syarat-<?= $reqs['dok_id']; ?>" data-callback = "get.search.requirements.{upload_id}.<?= $reqs['dok_id']; ?>" data-fieldcallback = "upload_id_<?= $reqs['dok_id']; ?>.no_dok_<?= $reqs['dok_id']; ?>.penerbit_<?= $reqs['dok_id']; ?>.tgl_dok_<?= $reqs['dok_id']; ?>.tgl_exp_<?= $reqs['dok_id']; ?>.View_<?= $reqs['dok_id']; ?>" onclick="popuptabel($(this));" data-multi = "<?= $reqs['multi']; ?>" <?= (int)$reqs['multi'] == 1 ? 'data-body="bodysyarat" data-doc="'.$izin_id.'"' : ''; ?>><i class="fa fa-search"></i> Pilih </button>
                              </div>
                            </center></td>
                        </tr>
                        <tr>
                          <td nowrap="nowrap">&nbsp;</td>
                          <td colspan="3">
                            <table width="100%" class="table table-nomargin table-condensed table-striped">
                              <thead>
                                <tr>
                                <th><center>
                                    No. Dokumen
                                  </center></th>
                                <th><center>
                                    Penerbit
                                  </center></th>
                                <th><center>
                                    Tgl. Dokumen
                                  </center></th>
                                <th><center>
                                    Tgl. Akhir
                                  </center></th>
                                <th><center>
                                    File
                                  </center></th>
                                 </tr> 
                              </thead>
                              <tbody <?= (int)$reqs['multi'] == 1 ? 'id="bodysyarat"' : ''; ?>>
                                <?php $banyakSess = count($sess_legal[$reqs['dok_id']]); if($banyakSess > 0){ 
                    for($i = 0; $i < count($sess_legal[$reqs['dok_id']]); $i++){ 
                    ?>
                              <tr>
                                <td width="20%"><center>
                                <input type="hidden" id="id[]" name="REQUIREMENTS[id][]" value="<?= $sess_legal[$reqs['dok_id']][$i]['id'];?>" readonly="readonly"/>
                                <input type="hidden" name="REQUIREMENTS[dok_id][]" value="<?= $reqs['dok_id'];?>" readonly="readonly"/>
                                <input type="hidden" name="REQUIREMENTS[upload_id][]" <?= ($reqs['tipe'] =='1')?'wajib="yes"':""; ?> id="upload_id_<?= $reqs['dok_id']; ?>" readonly="readonly" value="<?= $sess_legal[$reqs['dok_id']][$i]['upload_id'];?>"/>
                                
                                <span id="no_dok_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['nomor'];?></span></center></td>
                                <td width="35%"><center><span id="penerbit_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['penerbit_dok'];?></span></center></td>
                                <td width="20%"><center><span id="tgl_dok_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['tgl_dok'];?></span></center></td>
                                <td width="20%"><center><span id="tgl_exp_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['tgl_exp'];?></span></center></td>
                                <td width="10%"><center><span id="View_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['View'];?></span></center></td>
                              </tr>
                              <?php
                    } }else{
                    ?>
                    <tr>
                                <td width="20%"><center>
                                <input type="hidden" id="id[]" name="REQUIREMENTS[id][]" value="<?= $sess_legal[$reqs['dok_id']][$i]['id'];?>" readonly="readonly"/>
                                <input type="hidden" name="REQUIREMENTS[dok_id][]" value="<?= $reqs['dok_id'];?>" readonly="readonly"/>
                                <input type="hidden" name="REQUIREMENTS[upload_id][]" <?= ($reqs['tipe'] =='1')?'wajib="yes"':""; ?> id="upload_id_<?= $reqs['dok_id']; ?>" readonly="readonly" value="<?= $sess_legal[$reqs['dok_id']][$i]['upload_id'];?>"/>
                                
                                <span id="no_dok_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['nomor'];?></span></center></td>
                                <td width="35%"><center><span id="penerbit_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['penerbit_dok'];?></span></center></td>
                                <td width="20%"><center><span id="tgl_dok_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['tgl_dok'];?></span></center></td>
                                <td width="20%"><center><span id="tgl_exp_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['tgl_exp'];?></span></center></td>
                                <td width="10%"><center><span id="View_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['View'];?></span></center></td>
                              </tr>
                    <?php
                    }
                    ?>
                              </tbody>
                            </table>
                            </td>
                        <tr>
                          <?php endforeach; else: ?>
                        <tr>
                          <td colspan="4"><center>
                              <b>Tidak Terdapat Data</b>
                            </center></td>
                        </tr>
                        <?php endif; ?>
                          </tbody>
                        
                      </table>
                    </div>                    
                  </div>
                </section>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Prinsipal Supplier</b></header>
          <div class="panel-body">
            <div class="row">
              <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="prinsipal[nm_perusahaan_supl]" wajib="yes" value="<?= $sess['nm_perusahaan_supl']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Bentuk Badan Hukum <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('prinsipal[tipe_perusahaan_supl]', $tipe_perusahaan, $sess['tipe_perusahaan_supl'], 'id="tipe_perusahaan_supl" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tanggal Pendirian <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
               <input type="text" class="form-control mb-10 datepickers" name="prinsipal[tgl_pendirian_supl]" wajib="yes" value="<?= $sess['tgl_pendirian_supl']; ?>" data-date-format = "yyyy-mm-dd" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Kantor Pusat <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" wajib="yes" name="prinsipal[almt_perusahaan_supl]"><?= $sess['almt_perusahaan_supl']; ?></textarea>
              </div>
            </div>
            <div id="prov_supl" style="<?php echo ($sess['produksi'] == '01')?"":"display:none;";?>">
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('prinsipal[kdprop_supl]', $propinsi, $sess['kdprop_supl'], 'id="kdprop_supl" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab_supl\'); return false;"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('prinsipal[kdkab_supl]', $kab_supl, $sess['kdkab_supl'], 'id="kdkab_supl" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec_supl\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('prinsipal[kdkec_supl]', $kec_supl, $sess['kdkec_supl'], 'id="kdkec_supl" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel_supl\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('prinsipal[kdkel_supl]', $kel_supl, $sess['kdkel_supl'], 'id="kdkel_supl" wajib="yes" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telepon</label>
              <div class="col-sm-2">
                 <input type="text" class="form-control mb-10" name="prinsipal[telp_supl]" value="<?= $sess['telp_supl']; ?>" />
              </div>
              <label class="col-xs-2 control-label">Fax</label>
              <div class="col-sm-2">
                 <input type="text" class="form-control mb-10" name="prinsipal[fax_supl]" value="<?= $sess['fax_supl']; ?>" />
              </div>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if($sess['nm_perusahaan_prod']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewpj'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/third/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Prinsipal Produsen dan Supplier</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= $urifirst; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> --> 
                <span class="pull-right">
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewpj'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
                </span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script><script>$(document).ready(function(e){ $(".datepickers").datepicker({autoclose: true});});</script>