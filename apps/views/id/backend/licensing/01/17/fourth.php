<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewsyarat" name="fnewsyarat" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Tempat Usaha</b></header>
          <div class="panel-body">
			 <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
             <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $id; ?>" />
             <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" wajib="yes" value="<?= $kd_izin; ?>" />
             <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?= $tipe_permohonan; ?>" />
			   <div class="row">
              <label class="col-sm-3 control-label">Jumlah Tempat Dikelola Sendiri<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 nomor" id ="outlet_sendiri" name="STPW[outlet_sendiri]" value="<?=array_key_exists('outlet_sendiri', $sess) ? $sess['outlet_sendiri']:'';?>" wajib="yes" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jumlah Tempat Diwaralabakan<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 nomor" id ="outlet_waralaba" name="STPW[outlet_waralaba]" value="<?=array_key_exists('outlet_waralaba', $sess) ? $sess['outlet_waralaba']:'';?>" wajib="yes" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jumlah Penerima Waralaba Dalam Negeri<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 nomor" name="STPW[jum_dalam]" value="<?= $sess['jum_dalam']; ?>" id ="outlet_waralaba"/>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jumlah Penerima Waralaba Luar Negeri<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 nomor" name="STPW[jum_luar]" id ="jum_dalam" value="<?= $sess['jum_luar']; ?>" />
              </div>
            </div>
            <!-- <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Sejarah kegiatan Usaha<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" name="STPW[sejarah]"> <?= $sess['sejarah']; ?>
        </textarea>
              </div>
            </div> -->
            <p>Sejarah kegiatan Usaha</p>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nama Pendiri<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="STPW[persh_pendiri]" id ="jum_dalam" value="<?= $sess['persh_pendiri']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tahun Berdiri<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 datepickers" name="STPW[tgl_pendiri]" wajib="yes" value="<?= $sess['tgl_pendiri']; ?>" data-date-format = "dd-mm-yyyy" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Lokasi Berdiri<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="STPW[lokasi_pendiri]" id ="jum_dalam" value="<?= $sess['lokasi_pendiri']; ?>" />
              </div>
            </div>
		  </div>
         </section>
      </div>
	 </div> 
	  <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Jenis Dan Merk Usaha</b></header>
          <div class="panel-body">
			   <!-- <div class="row">
              <label class="col-sm-3 control-label">Kegiatan Usaha Waralaba<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="jenis_dagang" value="<?=array_key_exists('jenis_dagang', $sess) ? $sess['jenis_dagang']:'';?>" name="STPW[jenis_dagang]" wajib="yes" />
              </div>
            </div> -->
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Merk Usaha waralaba<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="merk_dagang" name="STPW[merk_dagang]" value="<?=array_key_exists('merk_dagang', $sess) ? $sess['merk_dagang']:'';?>" wajib="yes" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kegiatan Usaha waralaba<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[jns_usaha]',$kegiatan_usaha, $sess['jns_usaha'], 'id="jns_usaha" onclick="lain()" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row" id="lain">
              <label class="col-sm-3 control-label">Kegiatan Usaha waralaba lain</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="jenis_dagang" value="<?=array_key_exists('jenis_dagang', $sess) ? $sess['jenis_dagang']:'';?>" name="STPW[jenis_dagang]" wajib="yes" />
              </div>
            </div>
			<!-- <div style="height:5px;"></div>
            <div class="row"> -->
              <!-- <label class="col-sm-3 control-label">Wilayah Pemasaran<font size ="2" color="red">*</font></label> -->
              <!-- <label class="col-sm-3 control-label">Jenis Pemasaran<font size ="2" color="red">*</font></label>
              <div class="col-sm-9"> -->
				 <?//= form_dropdown('STPW[wilayah_dagang]',$wilayah_dagang, (array_key_exists('wilayah_dagang', $sess) ? $sess['wilayah_dagang'] : $this->newsession->userdata('wilayah_dagang')), 'id="wilayah_dagang" wajib="yes" class="form-control mb-10"'); ?>
				  <?//= form_multiselect('wilayah[]', $propinsi, $pemasaran, 'id="pemasaran" wajib="yes" multiple class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" '); ?>
				<!-- <input type="text" class="form-control mb-10" id ="pemasaran" name="STPW[wilayah_dagang]" value="<?=(array_key_exists('wilayah_dagang', $sess)) ? $sess['wilayah_dagang'] :'';?>" wajib="yes" /> -->
                <!-- <?= form_dropdown('STPW[wilayah_dagang]',$jenis_pemasaran, $sess['wilayah_dagang'], 'id="negara_perusahaan" wajib="yes" class="form-control input-sm select2"'); ?>
              </div>
			         </div> -->
             <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if($sess['outlet_sendiri']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewsyarat'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Pemilik/Penanggung Jawab</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fifth/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Legalitas dan Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectthird" data-url = "<?= $urithird; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> --> <span class="pull-right">
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewsyarat'); return false;"><i class="fa fa-arrow-right"></i>selanjutnya</button>
                </span>
                <?php endif;?>
              </div>
            </div>
		  </div>
         </section>
      </div>
	 </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script><script> $(document).ready(function(e){ $(".datepickers").datepicker({autoclose: true }); });</script>
<script>
  $(document).ready(function(e){
    $('.nomor').keydown(function (event) {
        var a = event.keyCode;
        var b = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 9, 11];
        if (jQuery.inArray(a, b) === -1) {
            event.preventDefault();
        }
    });
    $('#lain').hide();
  });
  function lain(){
    var usaha = $('#jns_usaha').val();
    if (usaha == '99') {
      $('#lain').show();
    }else{
      $('#lain').hide();
    }
  }
</script>