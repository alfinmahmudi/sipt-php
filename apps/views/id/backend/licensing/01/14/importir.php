<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-dialog/bootstrap-dialog.min.css" type="text/css" cache="false" />
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-dialog/bootstrap-dialog.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtable.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtablehash.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <!--<form action="<?//= $act; ?>" method="post" id="refkbli" name="refkbli" autocomplete = "off" data-redirect="true">-->
  <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
  <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?=$id;?>" />
  <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" wajib="yes" value="<?= $kd_izin; ?>" />
  <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?=$tipe_permohonan; ?>" />
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
		  <?=$tabel;?>
        </section>
      </div>
    </div>
    <div class="btn-group dropup">
      <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
      <ul role="menu" class="dropdown-menu">
        <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
        <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Pemilik/Penanggung Jawab</a></li>
        <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/third/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Produk</a></li>
        <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Legalitas dan Persyaratan</a></li>
        <li class="divider"></li>
        <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
      </ul>
    </div>
	 <span class="pull-right">
         <button class="btn btn-sm btn-success addon-btn m-b-10" data-url="<?=$act;?>" onclick="redirect($(this));return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
     </span>
  <!--</form>-->
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/jquery.browser.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/autocomplete.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/a.js?v=<?=date('Ymds')?>"></script>
<script>
function showHideDiv(val){
  var  valNegara= val.value;
  if(valNegara == "ID"){
    $('.ID').show();
  }else{
    $('.ID').hide();
    $("#tipe_perusahaan").val("");
    $("#kdpos").val("");
  }
}
</script>
<script>
$(document).ready(function(e){ 
  $(".datepickers").datepicker({
    autoclose: true
  });

  if($("#negara_perusahaan_pw").val() != "ID")
  {
    $('.ID').hide();
    $("#tipe_perusahaan").val("");
    $("#kdpos").val("");
  }

  $("#no_izin_lama").attr("url", '<?= site_url(); ?>get/ac/get_permohonan/' + $("#direktorat").val() + '/' + $("#kd_izin").val());
  $("#no_izin_lama").autocomplete($("#no_izin_lama").attr('url'), {width: '67.75%', selectFirst: false});
  $("#no_izin_lama").result(function (event, data, formatted) {
    if (data) {
      $(this).val(data[1]);
      $.get('<?= site_url(); ?>get/ac/set_form/' + data[2] + '/' + data[3], function (hasil) {
        if(hasil){
          var arrhasil = hasil.split("|");
          $("#id_permohonan_lama").val(arrhasil[0]);
          $("#no_izin_lama").val(arrhasil[1]);
          $("#tgl_izin_lama").val(arrhasil[2]);
          $("#tgl_izin_exp_lama").val(arrhasil[3]);
          $('#npwp_pw').attr('value', arrhasil[4]);
          $("#nm_perusahaan_pw").val(arrhasil[5]);
          $("#almt_perusahaan_pw").val(arrhasil[6]);
      $("#kdprop").select2('data',{id:arrhasil[7], text: arrhasil[20]});
          $("#kdpos_pw").val(arrhasil[11]);
          $("#telp_pw").val(arrhasil[12]);
          $("#fax_pw").val(arrhasil[13]);
          $("#lokasi").val(arrhasil[14]);
          $("#status_milik").val(arrhasil[15]);
          $("#no_siup").val(arrhasil[16]);
          $("#tgl_siup").val(arrhasil[17]);
          $("#jenis_siup").val(arrhasil[18]);
          $('#tipe_perusahaan').val(arrhasil[19]);
          $.get('<?= site_url(); ?>get/cb/set_kota/' + arrhasil[7], function (result){
            if(result){
              $("#kdkab").html(result);
              $("#kdkab").select2('data',{id:arrhasil[8], text: arrhasil[21]});
              $.get('<?= site_url(); ?>get/cb/set_kec/' + arrhasil[8], function (result) {
                if (result) {
                  $("#kdkec").html(result);
                  $("#kdkec").select2('data',{id:arrhasil[9], text: arrhasil[22]});
                  $.get('<?= site_url(); ?>get/cb/set_kel/' + arrhasil[9], function (result) {
                    if (result) {
                      $("#kdkel").html(result);
                      $("#kdkel").select2('data',{id:arrhasil[10], text: arrhasil[23]});
                    } else {
                      $("#kdkel").html('');
                    }
                  });
                } else {
                  $("#kdkec").html('');
                }
              });
            } else {
              $("#kdkab").html('');
            }
          });
        }
      });
    }
  });
});
</script>

