<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<script src="<?= base_url(); ?>assets/bend/js/newtable/newtable.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewnilaimodal" name="fnewnilaimodal" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Cabang</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
            <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $id; ?>" />
            <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" wajib="yes" value="<?= $kd_izin; ?>" />
            <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?= $tipe_permohonan; ?>" />
            <div class="row">
              <?= $lstCabang; ?>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php //if($jml_ahli>0):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= $urifirst; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= $urisecond; ?>" onclick="redirect($(this)); return false;">Data Pemilik/Penanggung Jawab</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= $urithird; ?>" onclick="redirect($(this)); return false;">Bidang Kegiatan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= $urifourth; ?>" onclick="redirect($(this)); return false;">Data Tenaga Ahli</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= $urisixth; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= $uripreview; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php //else:?>
               <!--  <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectsecond" data-url = "<?= $urisecond; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> -->
                <span class="pull-right">
                <button type="button" class="btn btn-sm btn-success addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectsecond" data-url = "<?= $urisixth; ?>"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
                </span>
                <?php //endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script><script> $(document).ready(function(e){ $(".datepickers").datepicker({ autoclose: true }); });</script>