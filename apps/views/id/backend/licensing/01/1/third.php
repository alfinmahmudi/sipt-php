<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //print_r($bidang);die();?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewthird" name="fnewthird" autocomplete = "off" data-redirect="true">
    <!-- <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Daftar Tenaga Surveyor</b></header>
          <div class="panel-body">
            
            <div class="row">
              <label class="col-sm-3 control-label"> Jumlah Tenaga Surveyor <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siujs[pegawai_tetap]" wajib="yes" value="<?= $sess['pegawai_tetap']; ?>" />
              </div>
            </div>
             <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label"> Jumlah Honorer <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siujs[pegawai_honor]" wajib="yes" value="<?= $sess['pegawai_honor']; ?>" />
              </div>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label"> Jumlah Pegawai Asing <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siujs[pegawai_asing]" wajib="yes" value="<?= $sess['pegawai_asing']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label"> Jumlah Pegawai Nasional <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="siujs[pegawai_lokal]" wajib="yes" value="<?= $sess['pegawai_lokal']; ?>" />
              </div>
            </div>
            <div style="height:25px;"></div> 
            
            <div class="row">
              <label class="col-sm-12 control-label">Tenaga Surveyor <font size ="2" color="red">*</font></label>
            </div>
            <div style="height:5px;"></div>
            
            <div class="row">
            	<div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="hidden-xs col-xs-9">Bidang <font size ="2" color="red">*</font></th>
                        <th class="hidden-xs col-xs-9">Jumlah <font size ="2" color="red">*</font></th>
                    </tr>
                    </thead>
                    <tbody id="bodyta">
                    <tr id="trta_<?= rand(); ?>">
                      <td>
                        <input type="hidden" class="form-control input-sm" name="ahli[id][]" value="<?= $bidang[0]['id']; ?>" readonly="readonly"/>
                        <input type="text" class="form-control input-sm" wajib="yes" name="ahli[keahlian][]" value="<?= $bidang[0]['keahlian']; ?>">
                      </td>
                      <td>
                        <div class="input-group m-b-10">
                          <input type="text" class="form-control input-sm" wajib="yes" name="ahli[jumlah][]" value="<?= $bidang[0]['jumlah']; ?>">
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-sm btn-info newta" id="<?=rand(); ?>" data-target = "#bodyta">
                              <i class="fa fa-plus-square"></i>
                            </button>
                          </span>
                        </div>
                      </td>
                    </tr>
                    <?php if(count($bidang) > 1): for($i = 1; $i < count($bidang); $i++):?>
                    <tr id="trta_<?= rand(); ?>">
                      <td>
                        <input type="hidden" class="form-control input-sm" name="ahli[id][]" value="<?= $bidang[$i]['id']; ?>" readonly="readonly"/>
                        <input type="text" class="form-control input-sm" wajib="yes" name="ahli[keahlian][]" value="<?= $bidang[$i]['keahlian']; ?>">
                      </td>
                      <td>
                        <div class="input-group m-b-10">
                          <input type="text" class="form-control input-sm" wajib="yes" name="ahli[jumlah][]" value="<?= $bidang[$i]['jumlah']; ?>">
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-sm btn-info minta" id="'+<?= rand(); ?>+'">
                              <i class="fa fa-minus-square"></i>
                            </button>
                          </span>
                        </div>
                      </td>
                    </tr>
                    <?php endfor; endif;?>
                    </tbody>
                </table>
                </div>
            </div>
            
            <div style="height:5px;"></div>
          </div>
        </section>
      </div>
    </div> -->
    
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Bidang Kegiatan</b></header>
          <div class="panel-body">
            <div class="row">
              <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
              <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $sess['id']; ?>" />
              <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" wajib="yes" value="<?= $sess['kd_izin']; ?>" />
              <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?= $sess['tipe_permohonan']; ?>" />
              <!-- <label class="col-sm-3 control-label"> Kegiatan-kegiatan yang dilakukan dan Rencana Perluasan Kegiatan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" name="siujs[kegiatan_usaha]" wajib="yes"><?= $sess['kegiatan_usaha'];?></textarea>
              </div> -->
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label"> Relasi yang Mempergunakan Jasa Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" name="siujs[relasi]" wajib="yes"><?= $sess['relasi'];?></textarea>
              </div>
            </div>
			<div style="height:25px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label"> Kategori Survey <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
              <?= form_multiselect('kategori_survey[]', $survey, $kategori_survey, 'id="survey" wajib="yes" multipe class="form-control input-sm select2"'); ?>
              </div>
            </div>
			
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if($sess['relasi']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewthird'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Pemilik/Penanggung Jawab</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Tenaga Ahli</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fifth/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Cabang</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/sixth/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
               <!--  <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectsecond" data-url = "<?= $urisecond; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> -->
                <span class="pull-right">
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewthird'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
                </span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script> 
	$(document).ready(function(e){ 
		$(".datepickers").datepicker({ autoclose: true }); 
		$(".newta").click(function(e){
            var $this = $(this);
			var $parent = $(this).closest("tr");
			$($this.attr("data-target")).append('<tr id="trta_'+<?= rand(); ?>+'"><td><input type="hidden" class="form-control input-sm" name="ahli[id][]" value="" readonly="readonly"/><input type="text" class="form-control input-sm" wajib="yes" name="ahli[keahlian][]"></td><td><div class="input-group m-b-10"><input type="text" class="form-control input-sm" wajib="yes" name="ahli[jumlah][]"><span class="input-group-btn"><button type="button" class="btn btn-sm btn-info minta" id="'+<?= rand(); ?>+'"><i class="fa fa-minus-square"></i></button></span></div></td></tr>');
			$(".minta").click(function(e){
				var $this = $(this);
				var $parentx = $(this).closest("tr");
				if($(".modal-backdrop").length === 0){
					BootstrapDialog.confirm('Apakah anda menghapus data terpilih ?', function(r){
						if(r){
							$($parentx).remove();
						}else{
							return false;
						}
					});
				}
            });
			return false;
    });
    $(".minta").click(function(e){
      var $this = $(this);
      var $parentx = $(this).closest("tr");
      if($(".modal-backdrop").length === 0){
        BootstrapDialog.confirm('Apakah anda menghapus data terpilih ?', function(r){
          if(r){
            $($parentx).remove();
          }else{
            return false;
          }
        });
      }
    });
    return false;
	});
	function showHideText(boxName,divName){
		if(boxName.checked == true){
			$('#'+divName).show();
		}else{
			$('#'+divName).hide();
		}
	}
</script>
