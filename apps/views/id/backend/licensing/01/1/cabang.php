<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php //print_r($cabang);die();?>

<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $sess['nama_izin'];?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewcabang" name="fnewcabang" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Kantor Cabang</b></header>
          <div class="panel-body">
            <input type="hidden" id="id" name="id" value="<?= $cabang['id'];?>" readonly="readonly"/>
            <input type="hidden" id="permohonan_id" name="cabang[permohonan_id]" wajib="yes" value="<?= $sess['id']; ?>" />
            <input type="hidden" id="dir" name="siujs[dir]" wajib="yes" value="<?= $sess['direktorat_id']; ?>" />
            <input type="hidden" id="doc" name="siujs[doc]" wajib="yes" value="<?= $sess['kd_izin']; ?>" />
            <input type="hidden" id="type" name="siujs[type]" wajib="yes" value="<?= $sess['tipe_permohonan']; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label">Nama Cabang <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="cabang[nama_cabang]" wajib="yes" value="<?= $cabang['nama_cabang']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Lokasi Cabang <font size ="2" color="red">*</font></label>
              <div class="col-sm-3">
                <select class="form-control m-b-10" id="lokasi" name="cabang[lokasi]" wajib="yes" onChange="showHide(this)">
                  <option value="">Pilih Lokasi</option>
                   <option value="Dalam Negeri" <?php echo ($cabang['lokasi']=="Dalam Negeri")?"selected":""?>>Dalam Negeri</option>
                   <option value="Luar Negeri" <?php echo ($cabang['lokasi']=="Luar Negeri")?"selected":""?>>Luar Negeri</option>
                </select>
              </div>
            </div>
            <div id="luar_negeri" style="<?php echo ($cabang['lokasi'] == 'Luar Negeri')?"":"display:none;";?>">
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Negara <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('cabang[negara]', $negara, $cabang['negara'], 'id="negara" wajib="yes" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" wajib="yes" name="cabang[alamat]"><?= $cabang['alamat']; ?></textarea>
              </div>
            </div>
            <div id="dalam_negeri" style="<?php echo ($cabang['lokasi'] == 'Dalam Negeri')?"":"display:none;";?>">
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('cabang[kdprop]', $propinsi, $cabang['kdprop'], 'id="kdprop" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab\'); return false;"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row" id="dalam_negeri">
              <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('cabang[kdkab]', $kab, $cabang['kdkab'], 'id="kdkab" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row" id="dalam_negeri">
              <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('cabang[kdkec]', $kec, $cabang['kdkec'], 'id="kdkec" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row" id="dalam_negeri">
              <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('cabang[kdkel]', $kel, $cabang['kdkel'], 'id="kdkel" wajib="yes" class="form-control input-sm select2"'); ?>
              </div>
            </div>
          </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telp <font size ="2" color="red">*</font></label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" name="cabang[telp]" wajib="yes" value="<?= $cabang['telp']; ?>" />
              </div>
              <label class="col-xs-2 control-label">Fax <font size ="2" color="red">*</font></label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" name="cabang[fax]" wajib="yes" value="<?= $cabang['fax']; ?>" />
              </div>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-lg-12">
                <center>
                  <button class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= $urififth;?>"><i class="fa fa-undo"></i>Kembali</button>
                  <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewcabang'); return false;"><i class="fa fa-save"></i>Simpan</button>
                </center>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script>
  function showHide(a) {
    var val = a.value;
    if (val == "Dalam Negeri")
    {
      $('#dalam_negeri').show();
      $('#luar_negeri').hide();
    }
    else
    {
      $('#dalam_negeri').hide();
      $('#luar_negeri').show();
    }
  }

</script>