<div class="row">
  <div class="col-lg-12">
    <table class="table">
      <thead>
        <tr>
          <th colspan="3">Detail <?= $sess['nama_cabang']; ?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td width="13%"><strong>Alamat</strong></td><td width="1%"><strong>:</strong></td><td width="40%"><?= $sess['alamat']; ?></td>
        </tr>
        <tr>
          <td><strong>Telepon</strong></td><td ><strong>:</strong></td><td><?= $sess['telp']; ?></td>
        </tr>
        <tr>
          <td><strong>Fax</strong></td><td><strong>:</strong></td><td><?= $sess['fax']; ?></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>