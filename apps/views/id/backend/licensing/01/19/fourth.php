<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewsyarat" name="fnewsyarat" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Tempat Usaha</b></header>
          <div class="panel-body">
			 <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
             <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $id; ?>" />
             <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" wajib="yes" value="<?= $kd_izin; ?>" />
             <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?= $tipe_permohonan; ?>" />
			   <div class="row">
              <label class="col-sm-3 control-label">Jumlah Tempat Dikelola Sendiri<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 nomor" id ="outlet_sendiri" name="STPW[outlet_sendiri]" wajib="yes" value="<?=(array_key_exists('outlet_sendiri', $sess) ? $sess['outlet_sendiri'] :'');?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jumlah Gerai Yang Akan Dikelola<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 nomor" id ="outlet_waralaba" name="STPW[outlet_waralaba]" wajib="yes" value="<?=(array_key_exists('outlet_waralaba', $sess) ? $sess['outlet_waralaba'] :'');?>" />
              </div>
            </div>
		  </div>
         </section>
      </div>
	 </div> 
	  <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Jenis Dan Merk Usaha</b></header>
          <div class="panel-body">
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Merk Usaha waralaba<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="merk_dagang" name="STPW[merk_dagang]" wajib="yes" value="<?=(array_key_exists('merk_dagang', $sess) ? $sess['merk_dagang'] :'');?>" />
              </div>
            </div>
			 <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Wilayah Usaha<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
				 <?//= form_dropdown('STPW[wilayah_dagang]',$wilayah_dagang, (array_key_exists('wilayah_dagang', $sess) ? $sess['wilayah_dagang'] : $this->newsession->userdata('wilayah_dagang')), 'id="wilayah_dagang" wajib="yes" class="form-control mb-10"'); ?>
				  <?//= form_multiselect('wilayah[]', $propinsi, $pemasaran, 'id="pemasaran" wajib="yes" multiple class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" '); ?>
          <?php if ($sess['tipe_pemasaran'] == 1) {
            $cek_dn = "checked";
          }elseif ($sess['tipe_pemasaran'] == 2) {
            $cek_ln = "checked";
          } ?>
                  <label class="checkbox-custom inline check-success">
                    <input value="1" id="cb-dalam" onchange="check()" type="checkbox" name="wilpem[]" <?= $cek_dn; ?> onclick="dalam()" check-agreement="yes"> 
                    <label for="cb-dalam">
                        <p>Wilayah Tertentu Indonesia</p>
                    </label>
                  </label>
                  <label class="checkbox-custom inline check-success">
                    <input value="2" id="cb-luar" onchange="check()" type="checkbox" name="wilpem[]" <?= $cek_ln; ?> onclick="luar()" check-agreement="yes"> 
                    <label for="cb-luar">
                        <p>Seluruh Wilayah Indonesia</p>
                    </label>
                  </label>
        			 </div>
            </div>
            <div id="dalam" style="display: none;">
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">Wilayah Pemasaran Kabupaten / Kota<font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <?= form_multiselect('wilayah[]', $propinsi, $pemasaran, 'id="pemasaran" wajib="yes" multiple class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" '); ?>
                </div>
              </div>
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">Alamat Pemasaran<font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control mb-10" id ="alamat_dn" name="STPW[alamat_dn]" wajib="yes" value="<?= $sess['alamat_dn'];?>" />
                </div>
              </div>
              <div class="row">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                  <p style="color: red; font-size: 11px;">Pemisah alamat menggunakan tanda ' , ' (koma).</p>
                </div>
              </div>
            </div>
            <!-- <div id="luar" style="display: none;">
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">Wilayah Pemasaran Negara<font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <?= form_multiselect('negara[]', $negara, $sess_negara, 'id="pemasaran" wajib="yes" multiple class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" '); ?>
                </div>
              </div>
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">Alamat Usaha<font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control mb-10" id ="alamat_dn" name="STPW[alamat_ln]" wajib="yes" value="<?= $sess['alamat_ln'];?>" />
                </div>
              </div>
              <div class="row">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                  <p style="color: red; font-size: 11px;">Pemisah alamat menggunakan tanda ' , ' (koma).</p>
                </div>
              </div>
            </div> -->
      <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kegiatan Usaha waralaba<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[jns_usaha]',$kegiatan_usaha, $sess['jns_usaha'], 'id="jns_usaha" onclick="lain()" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row" id="lain">
              <label class="col-sm-3 control-label">Kegiatan Usaha waralaba lain</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id ="jenis_dagang" value="<?=array_key_exists('jenis_dagang', $sess) ? $sess['jenis_dagang']:'';?>" name="STPW[jenis_dagang]" />
              </div>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if($sess['outlet_sendiri']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewsyarat'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Pemilik/Penanggung Jawab</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/third/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Perusahaan dan Pemberi Waralaba</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fifth/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Legalitas dan Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $izin_id; ?>/<?= $type; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectthird" data-url = "<?= $urithird; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> --> <span class="pull-right">
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewsyarat'); return false;"><i class="fa fa-arrow-right"></i>selanjutnya</button>
                </span>
                <?php endif;?>
              </div>
            </div>
		  </div>
         </section>
      </div>
	 </div> 
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script><script> $(document).ready(function(e){ $(".datepickers").datepicker({autoclose: true }); });</script>
<script>
$(document).ready(function(e){
  if ($('#cb-dalam').is(":checked")) {
      $('#dalam').show();
    }else{
      $('#dalam').hide();
    }

  if ($('#cb-luar').is(":checked")) {
      $('#luar').show();
    }else{
      $('#luar').hide();
    }
  $('#lain').hide(); 
  });
  function dalam(){
    if ($('#cb-dalam').is(":checked")) {
      $('#dalam').show();
    }else{
      $('#dalam').hide();
      $('#pemasaran').select2("val", "");
      $('#alamat_dn').val('');
    }
  }

  $(function() {
    $("input:checkbox").on('click', function() {
      // in the handler, 'this' refers to the box clicked on
      var $box = $(this);
      if ($box.is(":checked")) {
        // the name of the box is retrieved using the .attr() method
        // as it is assumed and expected to be immutable
        var group = "input:checkbox[name='" + $box.attr("name") + "']";
        // the checked state of the group/box on the other hand will change
        // and the current value is retrieved using .prop() method
        $(group).prop("checked", false);
        $box.prop("checked", true);
      } else {
        $box.prop("checked", false);
      }
    });
  });

  function luar(){
    if ($('#cb-luar').is(":checked")) {
      $('#dalam').hide();
      $('#pemasaran').select2("val", "");
      $('#alamat_dn').val('');
    }
  }

  // function luar(){
  //   if ($('#cb-luar').is(":checked")) {
  //     $('#luar').show();
  //   }else{
  //     $('#luar').hide();
  //   }
  // }
  $('.nomor').keydown(function (event) {
        var a = event.keyCode;
        var b = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 9, 11];
        if (jQuery.inArray(a, b) === -1) {
            event.preventDefault();
        }
    });
  function lain(){
    var usaha = $('#jns_usaha').val();
    if (usaha == '99') {
      $('#lain').show();
    }else{
      $('#lain').hide();
    }
  }
</script>