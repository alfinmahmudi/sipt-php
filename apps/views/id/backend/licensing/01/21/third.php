<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewpj" name="fnewpj" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Perusahaan Pemberi Waralaba</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
            <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $id; ?>" />
            <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" wajib="yes" value="<?= $kd_izin; ?>" />
            <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?= $tipe_permohonan; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label">Negara Asal<font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
               <?= form_dropdown('STPW[negara_perusahaan_pw]', $negara_perusahaan_pw, $sess['negara_perusahaan_pw'], 'id="negara_perusahaan_pw" wajib="yes" class="form-control input-sm select2" onChange = "showHideDiv(this); return false;"'); ?>
              </div>
            </div>
            <div class="ID">
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control mb-10" id="npwp_pw" name="STPW[npwp_pw]"  wajib="yes" value="<?= $sess['npwp_pw']; ?>" />
                </div>
              </div>
              <div style="height:5px;"></div>
              <div class="row">
                <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
                <div class="col-sm-9">
                  <?= form_dropdown('STPW[tipe_perusahaan_pw]', $tipe_perusahaan, $sess['tipe_perusahaan_pw'], 'id="tipe_perusahaan_pw" wajib="yes" class="form-control mb-10"'); ?>
                </div>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" id="nm_perusahaan_pw" name="STPW[nm_perusahaan_pw]"  wajib="yes" value="<?= $sess['nm_perusahaan_pw']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" wajib="yes" id="almt_perusahaan_pw" name="STPW[almt_perusahaan_pw]"><?= $sess['almt_perusahaan_pw']; ?>
</textarea>
              </div>
            </div>
            <div class="ID">
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[kdprop_pw]', $propinsi, $sess['kdprop_pw'], 'id="kdprop_pw" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab_pw\'); return false;"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[kdkab_pw]', $kab, $sess['kdkab_pw'], 'id="kdkab_pw" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec_pw\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[kdkec_pw]', $kec, $sess['kdkec_pw'], 'id="kdkec_pw" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel_pw\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[kdkel_pw]', $kel, $sess['kdkel_pw'], 'id="kdkel_pw" wajib="yes" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telp/Hp</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" id="telp" name="STPW[telp_pw]" value="<?= (array_key_exists('telp_pw', $sess) ? $sess['telp_pw'] : $this->newsession->userdata('telp_pw')); ?>" />
              </div>
       </div> 
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Fax</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" id="fax" name="STPW[fax_pw]" value="<?= (array_key_exists('fax_pw', $sess) ? $sess['fax_pw'] : $this->newsession->userdata('fax_pw')); ?>" />
              </div>
      </div>  
              <div class="ID">
        <div style="height:5px;"></div>
        <div class="row">
              <label class="col-sm-3 control-label">Kode Pos</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" id="kdpos" name="STPW[kdpos_pw]" value="<?= (array_key_exists('kdpos_pw', $sess) ? $sess['kdpos_pw'] : $this->newsession->userdata('kdpos_pw')); ?>" />
              </div>
            </div>
      </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if($sess['nm_perusahaan_pw']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewpj'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Pemilik / Penanggung Jawab</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Tempat Usaha</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fifth/<?= $direktorat; ?>/<?= $kd_izin; ?>/<?= $tipe_permohonan; ?>/<?= $id; ?>" onclick="redirect($(this)); return false;">Data Legalitas dan Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= $urifirst; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> --> 
                <span class="pull-right">
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewpj'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
                </span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/jquery.browser.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/autocomplete.js"></script>
<script>
function showHideDiv(val){
  var  valNegara= val.value;
  if(valNegara == "ID"){
    $('.ID').show();
  }else{
    $('.ID').hide();
  }
}
</script>
<script>
$(document).ready(function(e){ 
  $(".datepickers").datepicker({
    autoclose: true
  });

  if($("#negara_perusahaan_pw").val() != "ID")
  {
    $('.ID').hide();
  }
 
});
</script>

