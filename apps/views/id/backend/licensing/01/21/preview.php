<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="page-head">
  <h3 class="m-b-less"> Preview Permohonan</h3>
  <span class="sub-title">
  <?= $sess['nama_izin'];?>
  </span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fpreview" name="fpreview" autocomplete = "off" data-redirect="true">
    <input type="hidden" name="data[id]" value="<?= hashids_encrypt($sess['id'], _HASHIDS_, 9); ?>">
    <input type="hidden" name="data[kd_izin]" value="<?= hashids_encrypt($sess['kd_izin'], _HASHIDS_, 9); ?>">
    <input type="hidden" name="data[no_aju]" value="<?=$sess['no_aju']?>">
    <input type="hidden" name="direktorat" value="<?= hashids_encrypt($sess['direktorat_id'], _HASHIDS_, 9); ?>">
    <input type="hidden" name="tipe" value="<?= base64_encode($sess['permohonan']);?>">
    <input type="hidden" name="SEBELUM" value="<?=$sess['status']?>">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Data Permohonan</header>
          <div class="panel-body">
            <section class="isolate-tabs">
              <ul class="nav nav-tabs">
                <li class="active"> <a data-toggle="tab" href="#first">Data Permohonan</a> </li>
                <li class=""> <a data-toggle="tab" href="#second">Data Pemilik/Penanggung Jawab</a> </li>
                <li class=""> <a data-toggle="tab" href="#third">Data Tempat dan Jenis Usaha</a> </li>
                <li class=""> <a data-toggle="tab" href="#fourth">Data Legalitas dan Persyaratan</a> </li>
              </ul>
              <div class="panel-body">
                <div class="tab-content">
                  <div id="first" class="tab-pane active">
                    <div class="row">
                      <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading"> <b>Data Permohonan</b></header>
                          <div class="panel-body">
                            <div class="row">
                              <label class="col-sm-3 control-label">Status Permohonan <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['permohonan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Produk Waralaba<font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['produk_wrlb']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Nomor Pengajuan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $sess['no_aju']; ?>
                            </div>
                          </div>
                          <div style="height:5px;"></div>
                          <div class="row">
                            <label class="col-sm-3 control-label">Tanggal Pengajuan <font size ="2" color="red">*</font></label>
                            <div class="col-sm-9">
                              <?= $sess['tgl_aju']; ?>
                            </div>
                          </div>
                          </div>
                        </section>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading"> <strong><b>Data Perusahaan</b></strong></header>
                          <div class="panel-body">
                            <div class="row">
                              <label class="col-sm-3 control-label">Negara Asal <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['negara_asal']; ?>
                              </div>
                            </div>
							<?php if($sess['negara_perusahaan'] == "ID"){?>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">NPWP<font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['npwp']; ?>
                              </div>
                            </div>
							<div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['tipe_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['nm_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['almt_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['prop_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['kab_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['kec_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['kel_perusahaan']; ?>
                              </div>
                            </div>
							<div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Telp</label>
                              <div class="col-sm-9">
                                <?= $sess['telp']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Fax</label>
                              <div class="col-sm-9">
                                <?= $sess['fax']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Kode Pos</label>
                              <div class="col-sm-9">
                                <?= $sess['kdpos']; ?>
                              </div>
                            </div>
                            
							<?php
							}else{
							?>
							<div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Nama Perusahaan<font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['nm_perusahaan']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Alamat<font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?=$sess['almt_perusahaan']; ?>
                              </div>
                            </div>
							<div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Telp</label>
                              <div class="col-sm-9">
                                <?= $sess['telp']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Fax</label>
                              <div class="col-sm-9">
                                <?= $sess['fax'];?>
                            </div>
							<?php
							}
							?>
                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                  <div id="second" class="tab-pane">
                    <div class="row">
                      <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading"> <b>Data Pemilik/Penanggung Jawab</b></header>
                          <div class="panel-body">
                            <div class="row">
                              <label class="col-sm-3 control-label">Kewarganegaraan<font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['warganegara']; ?>
                              </div>
                            </div>
                            <div id="negara">
                              <div style="height:5px;"></div>
                              <div class="row">
                                <label class="col-sm-3 control-label">Asal Negara<font size ="2" color="red">*</font></label>
                                <div class="col-sm-9">
                                  <?= $sess['negara_pj']; ?>
                                </div>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Jenis Identitas<font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['identitas_pj']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Nomor Identitas<font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['noidentitas_pj']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Jabatan<font size ="2" color="red">*</font></label>
                              <div class="col-sm-3">
                                <?= $sess['jabatan_pj']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Nama Lengkap<font size ="2" color="red">*</font></label>
                              <div class="col-sm-3">
                                <?= $sess['nama_pj']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Tempat Lahir<font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['tmpt_lahir_pj']; ?>
                              </div>
                            </div>
							 <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Tanggal Lahir<font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['tgl_lahir_pj']; ?>
                              </div>
                            </div>
							 <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Alamat<font size ="2" color="red">*</font></label>
                              <div class="col-sm-9">
                                <?= $sess['alamat_pj']; ?>
                              </div>
                            </div>
                            <div id="ID">
                              <div style="height:5px;"></div>
                              <div class="row">
                                <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                                <div class="col-sm-9">
                                  <?= $sess['prop_pj']; ?>
                                </div>
                              </div>
                              <div style="height:5px;"></div>
                              <div class="row">
                                <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                                <div class="col-sm-9">
                                  <?= $sess['kab_pj']; ?>
                                </div>
                              </div>
                              <div style="height:5px;"></div>
                              <div class="row">
                                <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                                <div class="col-sm-9">
                                  <?= $sess['kec_pj']; ?>
                                </div>
                              </div>
                              <div style="height:5px;"></div>
                              <div class="row">
                                <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                                <div class="col-sm-9">
                                  <?= $sess['kel_pj']; ?>
                                </div>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Telp/Hp</label>
                              <div class="col-sm-2">
                                <?= $sess['telp_pj']; ?>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Fax</label>
                              <div class="col-sm-9">
                                <?= $sess['fax_pj']; ?>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
				  <div id="third" class="tab-pane">
                    <div class="row">
                      <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading"> <b>Data Jenis dan Bentuk Usaha</b></header>
                          <div class="panel-body">
                             <div class="row">
                              <label class="col-sm-3 control-label">Jumlah Tempat Dikelola Sendiri</label>
                              <div class="col-sm-9">
                                <?= $sess['outlet_sendiri']; ?>
                              </div>
                            </div>
							<div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Jumlah Tempat Diwaralabakan</label>
                              <div class="col-sm-9">
                                <?= $sess['outlet_waralaba']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Jumlah Penerima Waralaba Dalam Negeri</label>
                              <div class="col-sm-9">
                                <?= $sess['jum_dalam']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Jumlah Penerima Waralaba Luar Negri</label>
                              <div class="col-sm-9">
                                <?= $sess['jum_luar']; ?>
                              </div>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
					<div class="row">
                      <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading"> <strong><b>Data dan Jenis Merk Usaha</b></strong></header>
                          <div class="panel-body">
							 <div class="row">
                              <label class="col-sm-3 control-label">Jumlah Usaha Waralaba</label>
                              <div class="col-sm-9">
                                <?= $sess['jenis_dagang']; ?>
                              </div>
                            </div>
							<div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Merk Usaha Waralaba</label>
                              <div class="col-sm-9">
                                <?= $sess['merk_dagang']; ?>
                              </div>
                            </div>
                            <div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Jenis Usaha Waralaba</label>
                              <div class="col-sm-9">
                                <?= $sess['jns_usaha']; ?>
                              </div>
                            </div>
							<div style="height:5px;"></div>
                            <div class="row">
                              <label class="col-sm-3 control-label">Jenis Pemasaran</label>
                              <div class="col-sm-9">
                                <?= $sess['wilayah_dagang'];?>
                              </div>
                            </div>
						  </div>
						 </section>
					  </div>
					</div> 
                  </div>
                  <div id="fourth" class="tab-pane">
                    <div class="row">
                      <div class="col-lg-12">
                        <section class="panel">
                          <header class="panel-heading"> <b>Data Legalitas dan Persyaratan</b></header>
                          <div class="panel-body">
                            <div class="row col-lg-12">
                              <table class="table table-striped">
                                <thead>
                                  <tr class="control-label">
                                    <th class="control-label" width= "3%">No</th>
                                    <th>Nama Dokumen</th>
                                    <th width= "20%">Status</th>
                                    <th width= "5%">&nbsp;</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php if(count($req) > 0):$no = 1;foreach($req as $reqs):?>
                                <td align="center"><?php echo $no++; ?></td>
                                  <td><?php echo $reqs['keterangan']; echo ($reqs['tipe'] =='1')?' <font size ="2" color="red">*</font>':"";?></td>
                                  <td><?= $reqs['uraian']?></td>
                                  <td><center>
                                      <div id="btn"> </div>
                                    </center></td>
                                </tr>
                                <tr>
                                  <td nowrap="nowrap"></td>
                                  <td colspan="3"><table width="100%" class="table table-nomargin table-condensed table-striped">
                                      <tr>
                                        <th><center>No. Dokumen</center></th>
                                        <th><center>Penerbit</center></th>
                                        <th><center>Tgl. Dokumen</center></th>
                                        <th><center>Tgl. Akhir</center></th>
                                        <th><center>File</center></th>
                                      </tr>
                                      <?php if($sess_syarat):for($i = 0; $i < count($sess_syarat[$reqs['dok_id']]); $i++):?>
                                      <tr>
                                        <td width="20%"><center>
                                            <span id="no_dok_<?= $reqs['dok_id']; ?>">
                                            <?= $sess_syarat[$reqs['dok_id']][$i]['nomor'];?>
                                            </span>
                                          </center></td>
                                        <td width="35%"><center>
                                            <span id="penerbit_<?= $reqs['dok_id']; ?>">
                                            <?= $sess_syarat[$reqs['dok_id']][$i]['penerbit_dok'];?>
                                            </span>
                                          </center></td>
                                        <td width="20%"><center>
                                            <span id="tgl_dok_<?= $reqs['dok_id']; ?>">
                                            <?= $sess_syarat[$reqs['dok_id']][$i]['tgl_dok'];?>
                                            </span>
                                          </center></td>
                                        <td width="20%"><center>
                                            <span id="tgl_exp_<?= $reqs['dok_id']; ?>">
                                            <?= $sess_syarat[$reqs['dok_id']][$i]['tgl_exp'];?>
                                            </span>
                                          </center></td>
                                        <td width="10%"><center>
                                            <span id="View_<?= $reqs['dok_id']; ?>">
                                            <?= ($sess_syarat[$reqs['dok_id']][$i]['upload_id']==0)?"":$sess_syarat[$reqs['dok_id']][$i]['View'];?>
                                            </span>
                                          </center></td>
                                        </tr>
                                        <?php endfor;else:?>
                                        <tr>
                                        <td colspan="5"><center>
                                            <b>Tidak Terdapat Data</b>
                                          </center></td>
                                        </tr>
                                        <?php endif;?>
                                    </table></td>
                                <tr>
                                  <?php endforeach; else: ?>
                                <tr>
                                  <td colspan="4"><center>
                                      <b>Tidak Terdapat Data</b>
                                    </center></td>
                                </tr>
                                <?php endif; ?>
                                  </tbody>
                                
                              </table>
                            </div>
                          </div>
                        </section>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Data Proses Permohonan </header>
          <div class="panel-body">
            <?= $input; ?>
            <?php
            if(count($proses) > 0){
            ?>
            <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <?php if ($this->newsession->userdata('role') != '05') { ?>
                          <label for="catatan">Catatan</label>
                          <textarea class="jqte-catatan form-control mb-10" name="catatan"  id="catatan"></textarea>
                      <?php } ?>
                      <label class="checkbox-custom inline check-success">
                          <input value="1" id="checkbox-agreement" type="checkbox" check-agreement="yes"> 
                          <label for="checkbox-agreement">
                              <?php echo $agrement ?>
                          </label>
                      </label>
                    </div>
                <?php 
                      if(count($proses) > 0){
                          foreach($proses as $x){
                              echo $x . ' ';
                          }
                      }
                      ?>

              </div>
            </div>
            <?php
            }else{
        ?>
                <button class="btn btn-sm btn-info addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-info pull-right"></i>Kembali</button>
                <?php
      }
            ?>
            <div class="row">&nbsp;</div>
            <div class="row">
              <div class="col-md-12">
                <section class="panel-timeline">
                  <div class="time-line-wrapper">
                    <div class="time-line-caption">
                      <h3 class="time-line-title">Histori Permohonan <sup><a href="javascript:void(0);" id="<?= rand(); ?>" class="log-izin" data-url = "<?= $urllog; ?>" data-target="#tmplog"><span class="badge bg-info"> <?= $jmllog; ?> </span></a></sup></h3>
                    </div>
                    <div id="tmplog"><div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script>
$(document).ready(function(e){
  var wg = "<?= $sess['warganegara'];?>";
  if (wg == "WNI") {
    $('#negara').attr('hidden', true);
    $('#ID').attr('hidden', false);
  }
  if (wg == "WNA"){
    $('#negara').attr('hidden', false);
    $('#ID').attr('hidden', true);
  }
});
</script>
<script>
                                $(document).ready(function () {
                                    if (<?= $this->newsession->userdata('role') ?> == '07') {
                                        BootstrapDialog.show({
                                            title: '',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                        });
                                    }
                                    if (<?= $this->newsession->userdata('role') ?> == '06') {
                                        BootstrapDialog.show({
                                            title: '',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                        });
                                    }
                                    if (<?= $this->newsession->userdata('role') ?> == '02') {
                                        BootstrapDialog.show({
                                            title: '',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                        });
                                    }
                                    if (<?= $this->newsession->userdata('role') ?> == '01') {
                                        BootstrapDialog.show({
                                            title: '',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                        });
                                    }
                                });
            </script>