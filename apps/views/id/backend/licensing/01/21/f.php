<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewsyarat" name="fnewsyarat" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Permohonan</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id ="id" name="id" wajib="yes" value="<?= $sess['id']; ?>" />
            <input type="hidden" class="form-control mb-10" id="kd_izin" name="STPW[kd_izin]" wajib="yes" value="<?= $sess['kd_izin']; ?>" />
            <input type="hidden" class="form-control mb-10" id ="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label">Status Permohonan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[tipe_permohonan]', $tipe_permohonan, $sess['tipe_permohonan'], 'id="tipe_permohonan" disabled wajib="yes" class="form-control mb-10"'); ?>
                <input type="hidden" class="form-control mb-10" id ="tipe_permohonan" name="STPW[tipe_permohonan]" wajib="yes" value="<?= $sess['tipe_permohonan']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Produk Waralaba <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[produk_waralaba]',$produk_waralaba, (array_key_exists('produk_waralaba', $sess) ? $sess['produk_waralaba'] : $this->newsession->userdata('produk_waralaba')), 'id="produk_waralaba" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>

          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Perusahaan</b></header>
		  <div class="panel-body">
			<div class="row">
              <label class="col-sm-3 control-label">Negara Asal <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                  <?= form_dropdown('STPW[negara_perusahaan]',$negara_perusahaan, (array_key_exists('negara_perusahaan', $sess) ? $sess['negara_perusahaan'] : $this->newsession->userdata('negara_perusahaan')), 'id="negara_perusahaan" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
		  </div>
          <div class="panel-body">
            <div class="row">
              <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="STPW[npwp]" readonly wajib="yes" value="<?= $this->newsession->userdata('npwp'); ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[tipe_perusahaan]', $tipe_perusahaan, (array_key_exists('tipe_perusahaan', $sess) ? $sess['tipe_perusahaan'] : $this->newsession->userdata('tipe_perusahaan')), 'id="tipe_perusahaan" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="STPW[nm_perusahaan]" wajib="yes" value="<?= (array_key_exists('nm_perusahaan', $sess) ? $sess['nm_perusahaan'] : $this->newsession->userdata('nm_perusahaan')); ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" name="STPW[almt_perusahaan]"><?= array_key_exists('almt_perusahaan', $sess) ? $sess['almt_perusahaan'] : $this->newsession->userdata('almt_perusahaan'); ?>
</textarea>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[kdprop]', $propinsi, (array_key_exists('kdprop', $sess) ? $sess['kdprop'] : $this->newsession->userdata('kdprop')), 'id="kdprop" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab\'); return false;"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[kdkab]', $kab, (array_key_exists('kdkab', $sess) ? $sess['kdkab'] : $this->newsession->userdata('kdkab')), 'id="kdkab" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[kdkec]', $kec, (array_key_exists('kdkec', $sess) ? $sess['kdkec'] : $this->newsession->userdata('kdkec')), 'id="kdkec" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('STPW[kdkel]', $kel, (array_key_exists('kdkel', $sess) ? $sess['kdkel'] : $this->newsession->userdata('kdkec')), 'id="kdkel" wajib="yes" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telp/Hp</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" name="STPW[telp]" wajib="yes" value="<?= (array_key_exists('telp', $sess) ? $sess['telp'] : $this->newsession->userdata('telp')); ?>" />
              </div>
			</div>  
			 <div style="height:5px;"></div>
             <div class="row">  
              <label class="col-sm-3 control-label">Fax</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" name="STPW[fax]" wajib="yes" value="<?= (array_key_exists('fax', $sess) ? $sess['fax'] : $this->newsession->userdata('fax')); ?>" />
              </div>
			</div>  
			 <div style="height:5px;"></div>
            <div class="row">
               <label class="col-sm-3 control-label">Kode Pos</label>
               <div class="col-sm-2">
                <input type="text" class="form-control mb-10" name="STPW[kdpos]" wajib="yes" value="<?= (array_key_exists('fax', $sess) ? $sess['kdpos'] : $this->newsession->userdata('kdpos')); ?>" />
              </div>
            </div>
        </div>
		
		<div class="panel-body" id="no-idn">
		<!--
			  <div class="row">
              <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="STPW[nm_perusahaan]"  wajib="yes" value="<?//= (array_key_exists('nm_perusahaan', $sess) ? $sess['nm_perusahaan'] : $this->newsession->userdata('nm_perusahaan')); ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" name="STPW[almt_perusahaan]"><?//= array_key_exists('almt_perusahaan', $sess) ? $sess['almt_perusahaan'] : $this->newsession->userdata('almt_perusahaan'); ?>
</textarea>
              </div>
            </div>
			-->
		  </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-lg-12">
                <?php if($sess['id']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewsyarat'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Pemilik / Penanggung Jawab</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/third/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Prinsipal Produsen dan Supplier</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left"><button class="btn btn-sm btn-info addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= site_url(); ?>licensing/view/status"><i class="fa fa-undo"></i>Batal</button></span>  -->
                <span class="pull-right"><button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewsyarat'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button></span>
                <?php endif;?>
              </div>
            </div> 
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script><script>$(document).ready(function(e){  $(".datepickers").datepicker({autoclose: true});});</script>
