		<div style="height:5px;"></div>
			<div class="row">
              <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="STPW[nm_perusahaan_pw]"  wajib="yes" value="<?= (array_key_exists('nm_perusahaan_pw', $sess) ? $sess['nm_perusahaan_pw'] : ''); ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" name="STPW[almt_perusahaan_pw]"><?= (array_key_exists('almt_perusahaan_pw', $sess) ? $sess['almt_perusahaan_pw'] : ''); ?>
</textarea>
              </div>
            </div>
			<div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telp/Hp</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" name="STPW[telp_pw]" wajib="yes" value="<?= (array_key_exists('telp_pw', $sess) ? $sess['telp_pw'] : ''); ?>" />
              </div>
			</div>  
			 <div style="height:5px;"></div>
             <div class="row">  
              <label class="col-sm-3 control-label">Fax</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" name="STPW[fax_pw]" wajib="yes" value="<?= (array_key_exists('fax_pw', $sess) ? $sess['fax_pw'] : ''); ?>" />
              </div>
			</div>