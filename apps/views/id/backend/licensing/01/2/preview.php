<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="page-head">
    <h3 class="m-b-less"> Preview Permohonan</h3>
    <span class="sub-title"><?= $sess['nama_izin']; ?></span>
</div>
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading"> Data Permohonan</header>
                <div class="panel-body">
                    <section class="isolate-tabs">
                        <ul class="nav nav-tabs">
                            <li class="active"> <a data-toggle="tab" href="#first">Data Permohonan</a> </li>
                            <li class=""> <a data-toggle="tab" href="#second">Data Pemilik/Penanggung Jawab</a> </li>
                            <li class=""> <a data-toggle="tab" href="#third">Data Tenaga Ahli</a> </li>
                            <li class=""> <a data-toggle="tab" href="#fourth">Data Persyaratan</a> </li>
                        </ul>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="first" class="tab-pane active">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <b>Data Permohonan</b></header>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Status Permohonan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['permohonan']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                      <div class="row">
                                                        <label class="col-sm-3 control-label">Nomor Pengajuan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                          <?= $sess['no_aju']; ?>
                                                        </div>
                                                      </div>
                                                      <div style="height:5px;"></div>
                                                      <div class="row">
                                                        <label class="col-sm-3 control-label">Tanggal Pengajuan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                          <?= $sess['tgl_aju']; ?>
                                                        </div>
                                                      </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <strong><b>Data Perusahaan</b></strong></header>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['npwp']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['tipe_perusahaan']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['nm_perusahaan']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Merk Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['merk_perusahaan']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['almt_perusahaan']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdprop']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdkab']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdkec']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdkel']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kode Pos</label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdpos']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Telp/HP</label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['telp']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Fax</label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['fax']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Jenis Waralaba</label>
                                                        <div class="col-sm-9">
                                                            <?php if($sess['jenis_waralaba']){ echo $sess['jenis_waralaba'];}else{ echo 'Tidak Memiliki Waralaba';} ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div id="second" class="tab-pane">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <b>Data Pemilik/Penanggung Jawab</b></header>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Jenis Identitas <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['identitas_pj']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Nomor Identitas <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['noidentitas_pj']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Jabatan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['jabatan_pj']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Nama Lengkap <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['nama_pj']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Tempat Lahir <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-3">
                                                            <?= $sess['tmpt_lahir_pj']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Tanggal Lahir <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-3">
                                                            <?= date_indo($sess['tgl_lahir_pj']); ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Alamat <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['alamat_pj']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdprop_pj']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdkab_pj']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdkec_pj']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['kdkel_pj']; ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Telp/Hp</label>
                                                        <div class="col-sm-2">
                                                            <?= $sess['telp_pj']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Fax</label>
                                                        <div class="col-sm-9">
                                                            <?= $sess['fax']; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <b>Data Modal dan Saham</b></header>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Modal dan Nilai Kekayaan Bersih Perusahaan (Tidak Termasuk Tanah dan Bangunan Tempat Usaha)<font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= number_format($sess['nilai_modal'], 2); ?>
                                                        </div>
                                                    </div>
                                                    <div style="height:5px;"></div>
                                                    <div class="row">
                                                        <label class="col-sm-3 control-label">Total Nilai dan Jumlah Saham<font size ="2" color="red">*</font></label>
                                                        <div class="col-sm-9">
                                                            <?= number_format($sess['nilai_saham'], 2); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div id="third" class="tab-pane">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <b>Data Tenaga Ahli</b></header>
                                                <div class="panel-body">
                                                    <div class="row col-lg-12">
                                                       <!-- <script src="<?= base_url(); ?>assets/bend/js/newtable/newtable.js?v=<?= date("Ymd"); ?>" type="text/javascript"></script>
                                                        <?= $lstTngAhli; ?> -->
                                                        <table class="table table-striped custom-table table-hover">
                                                            <thead>
                                                            <tr style="background: #383838; color: white;">
                                                                <th>Nama</th>
                                                                <th>Warganegara</th>
                                                                <th>No Sertifikat</th>
                                                                <th>Penerbit</th>
                                                                <th>Detail</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php foreach ($lstTngAhli as $tng_ahli) { ?>
                                                                <tr>
                                                                  <td><?= $tng_ahli['nama'];?></td>
                                                                  <td><?= $tng_ahli['warganegara'];?></td>
                                                                  <td><?= $tng_ahli['no_sertifikat'];?></td>
                                                                  <td><?= $tng_ahli['penerbit'];?></td>
                                                                  <td><button onclick="get_dialog(<?= $tng_ahli['id'].','.$sess['id'].', 2'?>)">Detail</button></td>
                                                                </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div> 
                                </div>                   
                                <div id="fourth" class="tab-pane">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <section class="panel">
                                                <header class="panel-heading"> <b>Data Legalitas dan Persyaratan</b></header>
                                                <div class="panel-body">
                                                    <div class="row col-lg-12">
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr class="control-label">
                                                                    <th class="control-label" width= "3%">No</th>
                                                                    <th>Nama Dokumen</th>
                                                                    <th width= "20%">Status</th>
                                                                    <th width= "5%">&nbsp;</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php if (count($req) > 0):$no = 1;
                                                                    foreach ($req as $reqs): ?>
                                                                        <tr>
                                                                            <td align="center"><?php echo $no++; ?></td>
                                                                            <td><?php echo $reqs['keterangan'];
                                                                        echo ($reqs['tipe'] == '1') ? ' <font size ="2" color="red">*</font>' : ""; ?></td>
                                                                            <td><?= $reqs['uraian'] ?></td>
                                                                            <td><center><div id="btn"> </div></center></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td nowrap="nowrap"></td>
                                                                        <td colspan="3">
                                                                            <table width="100%" class="table">
                                                                                <tr>
                                                                                    <th><center>No. Dokumen</center></th>
                                                                                <th><center>Penerbit</center></th>
                                                                                <th><center>Tgl. Dokumen</center></th>
                                                                                <th><center>Tgl. Akhir</center></th>
                                                                                <th><center>File</center></th>
                                                                    </tr>
        <?php if ($sess_syarat):for ($i = 0; $i < count($sess_syarat[$reqs['dok_id']]); $i++): ?>
                                                                            <tr>
                                                                                <td width="20%"><center><span id="no_dok_<?= $reqs['dok_id']; ?>"><?= $sess_syarat[$reqs['dok_id']][$i]['nomor']; ?></span></center></td>
                                                                            <td width="35%"><center><span id="penerbit_<?= $reqs['dok_id']; ?>"><?= $sess_syarat[$reqs['dok_id']][$i]['penerbit_dok']; ?></span></center></td>
                                                                            <td width="20%"><center><span id="tgl_dok_<?= $reqs['dok_id']; ?>"><?= date_indo($sess_syarat[$reqs['dok_id']][$i]['tgl_dok']); ?></span></center></td>
                                                                            <td width="20%"><center><span id="tgl_exp_<?= $reqs['dok_id']; ?>"><?= date_indo($sess_syarat[$reqs['dok_id']][$i]['tgl_exp']); ?></span></center></td>
                                                                            <td width="10%"><center><span id="View_<?= $reqs['dok_id']; ?>"><?= $sess_syarat[$reqs['dok_id']][$i]['View']; ?></span></center></td>
                                                                            </tr>
            <?php endfor;
        else: ?>
                                                                        <tr>
                                                                            <td colspan="5"><center><b>Tidak Terdapat Data</b></center></td>
                                                                        </tr>
                                                                    <?php endif; ?>
                                                                </table>
                                                                </td>
                                                                <tr>
                                                            <?php endforeach;
                                                        else: ?>
                                                            <tr>
                                                                <td colspan="4"><center><b>Tidak Terdapat Data</b></center></td>
                                                            </tr>
<?php endif; ?>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <form action="<?= $act; ?>" method="post" id="fpreview" name="fpreview" autocomplete = "off" data-redirect="true">
            <input type="hidden" name="data[id]" value="<?= hashids_encrypt($sess['id'], _HASHIDS_, 9); ?>">
            <input type="hidden" name="data[kd_izin]" value="<?= hashids_encrypt($sess['kd_izin'], _HASHIDS_, 9); ?>">
            <input type="hidden" name="data[no_aju]" value="<?= $sess['no_aju'] ?>">
            <input type="hidden" name="direktorat" value="<?= hashids_encrypt($sess['direktorat_id'], _HASHIDS_, 9); ?>">
            <input type="hidden" name="tipe" value="<?= base64_encode($sess['permohonan']); ?>">
            <input type="hidden" name="SEBELUM" value="<?= $sess['status'] ?>">	
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"> Data Proses Permohonan </header>
                    <div class="panel-body">
<?= $input; ?>
<?php
if (count($proses) > 0) {
    ?>
                            <div class="row">
                                <div class="col-md-12">
                                <?php if ($sess['status'] != '1000') { ?>
                                    <div class="form-group">
                                        <?php if ($this->newsession->userdata('role') != '05') { ?>
                                          <label for="catatan">Catatan</label>
                                          <textarea class="form-control mb-10" name="catatan"  id="catatan"></textarea>
                                      <?php } ?>
                                      <label class="checkbox-custom inline check-success">
                                          <input value="1" id="checkbox-agreement" type="checkbox" check-agreement="yes"> 
                                          <label for="checkbox-agreement">
                                              <?php echo $agrement ?>
                                          </label>
                                      </label>
                                    </div>
                                    <?php }
                                    if (count($proses) > 0) {
                                        foreach ($proses as $x) {
                                            echo $x . ' ';
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>
                            <button class="btn btn-sm btn-info addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-info pull-right"></i>Kembali</button>
    <?php
}
?>
                        <div class="row">&nbsp;</div>
                        <div class="row">
                            <div class="col-md-12">
                                <section class="panel-timeline">
                                    <div class="time-line-wrapper">
                                        <div class="time-line-caption">
                                            <h3 class="time-line-title">Histori Permohonan <sup><a href="javascript:void(0);" id="<?= rand(); ?>" class="log-izin" data-url = "<?= $urllog; ?>" data-target="#tmplog"><span class="badge bg-info"> <?= $jmllog; ?> </span></a></sup></h3>
                                        </div>
                                        <div id="tmplog"></div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>

</div>
<script>
    function get_dialog(id_ahli, id_per, izin_id){
        var site_url = '<?php echo site_url(); ?>';
        $.post(site_url + 'licensing/popup_ahli/' + id_ahli + '/' + izin_id + '/'+ id_per, {data: id_ahli}, function (data) {
            console.log(data);
            BootstrapDialog.show({
                title: '',
                type: BootstrapDialog.TYPE_WARNING,
                message: data,
                size: BootstrapDialog.SIZE_WIDE
            });
        });
    }
</script>
<script>
                                $(document).ready(function () {
                                    if (<?= $this->newsession->userdata('role') ?> == '07') {
                                        BootstrapDialog.show({
                                            title: '',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                        });
                                    }
                                    if (<?= $this->newsession->userdata('role') ?> == '06') {
                                        BootstrapDialog.show({
                                            title: '',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                        });
                                    }
                                    if (<?= $this->newsession->userdata('role') ?> == '02') {
                                        BootstrapDialog.show({
                                            title: '',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                        });
                                    }
                                    if (<?= $this->newsession->userdata('role') ?> == '01') {
                                        BootstrapDialog.show({
                                            title: '',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                        });
                                    }
                                });
            </script>