<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $req[0]['nama_izin'];?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewtngahli" name="fnewtngahli" autocomplete = "off" data-redirect="true" data-hidden="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Tenaga Ahli</b></header>
          <div class="panel-body">
            <input type="hidden" name="SIUPMB[izin_id]" value="<?= $req[0]['kd_izin'];?>" readonly="readonly"/>
            <input type="hidden" name="SIUPMB[permohonan_id]" value="<?= $permohonan_id;?>"/>
            <input type="hidden" id="direktorat" name="direktorat" wajib="yes" value="<?= $req[0]['direktorat_id']; ?>" />
            <input type="hidden" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?= $req[0]['tipe_permohonan']; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label">Nama Lengkap <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="hidden" class="form-control mb-10" id="id_ahli" name="id_ahli" value="<?= $ahli['id']; ?>" />
                <input type="text" class="form-control mb-10" name="ahli[nama]" wajib="yes" value="<?= $ahli['nama']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kewarganegaraan <font size ="2" color="red">*</font></label>
              <div class="col-sm-3">
                <select class="form-control m-b-10" name="ahli[warganegara]" wajib="yes">
                   <option value="WNI" <?php echo ($ahli['warganegara']=="WNI")?"selected":""?>>WNI</option>
                   <!-- <option value="WNA" <?php //echo ($ahli['warganegara']=="WNA")?"selected":""?>>WNA</option> -->
                </select>
              </div>
            </div>
            <div style="height:5px;"></div>
             <div class="row">
              <label class="col-sm-3 control-label">Jenis Identitas <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('ahli[jenis_identitas]', $jenis_identitas, $ahli['jenis_identitas'], 'id="jenis_identitas" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nomor Identitas <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="ahli[no_identitas]" wajib="yes" value="<?= $ahli['no_identitas']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nomor Sertifikat <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="ahli[no_sertifikat]" wajib="yes" value="<?= $ahli['no_sertifikat']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Lembaga Penerbit Sertifikat <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="ahli[penerbit]" wajib="yes" value="<?= $ahli['penerbit']; ?>" />
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Persyaratan Tenaga Ahli</b></header>
          <div class="panel-body">
          	<div class="row col-lg-12">
          		<table class="table">
                <thead>
                  <tr class="control-label">
                    <th class="control-label" width= "3%">No</th>
                    <th>Nama Dokumen</th>
                    <th>Status</th>
                    <th><center>
                        Pilih
                      </center></th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(count($req) > 0):$no = 1;foreach($req as $reqs):?>
                <td align="center"><?php echo $no++; ?></td>
                  <td><?php echo $reqs['keterangan']; echo ($reqs['tipe'] =='1')?' <font size ="2" color="red">*</font>':""; ?></td>
                  <td><?= $reqs['uraian']?></td>
                  <td><center>
                      <div id="btn">
                        <button type="button" class="btn btn-sm btn-success addon-btn m-b-10" data-url="<?= site_url(); ?>licensing/popup_syarat/<?= $reqs['dok_id']; ?>/" data-target="{upload_id_<?= $reqs['dok_id']; ?>}.{no_dok_<?= $reqs['dok_id']; ?>}.{penerbit_<?= $reqs['dok_id']; ?>}.{tgl_dok_<?= $reqs['dok_id']; ?>}.{tgl_exp_<?= $reqs['dok_id']; ?>}.{View_<?= $reqs['dok_id']; ?>}" data-title = "List Data <?= $reqs['keterangan']?>" style="cursor:pointer;" id="btn-syarat-<?= $reqs['dok_id']; ?>" data-callback = "get.search.requirements.{upload_id}.<?= $reqs['dok_id']; ?>" data-fieldcallback = "upload_id_<?= $reqs['dok_id']; ?>.no_dok_<?= $reqs['dok_id']; ?>.penerbit_<?= $reqs['dok_id']; ?>.tgl_dok_<?= $reqs['dok_id']; ?>.tgl_exp_<?= $reqs['dok_id']; ?>.View_<?= $reqs['dok_id']; ?>" onclick="popuptabel($(this));"><i class="fa fa-search"></i> Pilih </button>
                      </div>
                      <input type="hidden" id="id[]" name="REQUIREMENTS[id][]" value="<?= $sess[$reqs['dok_id']]['id'];?>" readonly="readonly"/>
                      <input type="hidden" name="REQUIREMENTS[dok_id][]" value="<?= $reqs['dok_id'];?>" readonly="readonly"/>
                      <input type="hidden" name="REQUIREMENTS[upload_id][]" <?= ($reqs['tipe'] =='1')?'wajib="yes"':""; ?> id="upload_id_<?= $reqs['dok_id']; ?>" readonly="readonly" value="<?= $sess[$reqs['dok_id']]['upload_id'];?>"/>
                    </center></td>
                </tr>
                <tr>
                  <td nowrap="nowrap"><div id="divHapus_<?php echo $dokWajib['idPendukung']; ?>"></div></td>
                  <td colspan="3"><table width="100%" class="table table-nomargin table-condensed table-striped">
                      <tr>
                        <th><center>
                            No. Dokumen
                          </center></th>
                        <th><center>
                            Penerbit
                          </center></th>
                        <th><center>
                            Tgl. Dokumen
                          </center></th>
                        <th><center>
                            Tgl. Akhir
                          </center></th>
                        <th><center>
                            File
                          </center></th>
                      </tr>
                      <tr>
                        <td width="20%"><center>
                            <span id="no_dok_<?= $reqs['dok_id']; ?>">
                            <?= $sess[$reqs['dok_id']]['nomor'];?>
                            </span>
                          </center></td>
                        <td width="35%"><center>
                            <span id="penerbit_<?= $reqs['dok_id']; ?>">
                            <?= $sess[$reqs['dok_id']]['penerbit_dok'];?>
                            </span>
                          </center></td>
                        <td width="20%"><center>
                            <span id="tgl_dok_<?= $reqs['dok_id']; ?>">
                            <?= $sess[$reqs['dok_id']]['tgl_dok'];?>
                            </span>
                          </center></td>
                        <td width="20%"><center>
                            <span id="tgl_exp_<?= $reqs['dok_id']; ?>">
                            <?= $sess[$reqs['dok_id']]['tgl_exp'];?>
                            </span>
                          </center></td>
                        <td width="10%"><center>
                            <span id="View_<?= $reqs['dok_id']; ?>">
                            <?= $sess[$reqs['dok_id']]['View'];?>
                            </span>
                          </center></td>
                      </tr>
                    </table></td>
                <tr>
                  <?php endforeach; else: ?>
                <tr>
                  <td colspan="4"><center>
                      <b>Tidak Terdapat Data</b>
                    </center></td>
                </tr>
                <?php endif; ?>
                  </tbody>
                
              </table>
          	</div>
          	<div style="height:25px;"></div>
            <div class="row">
              <div class="col-lg-12">
                <center>
                	<button class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= site_url(); ?>licensing/form/third/<?= $req[0]['direktorat_id'];?>/<?= $req[0]['kd_izin'];?>/<?= $req[0]['tipe_permohonan'];?>/<?= $permohonan_id;?>"><i class="fa fa-undo"></i>Kembali</button>
                	<button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewtngahli'); return false;"><i class="fa fa-save"></i>Simpan</button>
                </center>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script><script>$(document).ready(function(e){ $(".datepickers").datepicker({autoclose: true});});</script>