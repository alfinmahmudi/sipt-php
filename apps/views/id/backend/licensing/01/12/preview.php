<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="page-head">
    <h3 class="m-b-less"> Preview Permohonan</h3>
    <span class="sub-title">
        <?= $sess['nama_izin']; ?>
    </span> </div>
<div class="wrapper">
    <form action="<?= $act; ?>" method="post" id="fpreview" name="fpreview" autocomplete = "off" data-redirect="true">
        <input type="hidden" name="data[id]" value="<?= hashids_encrypt($sess['id'], _HASHIDS_, 9); ?>">
        <input type="hidden" name="data[kd_izin]" value="<?= hashids_encrypt($sess['kd_izin'], _HASHIDS_, 9); ?>">
        <input type="hidden" name="data[no_aju]" value="<?= $sess['no_aju'] ?>">
        <input type="hidden" name="direktorat" value="<?= hashids_encrypt($sess['direktorat_id'], _HASHIDS_, 9); ?>">
        <input type="hidden" name="tipe" value="<?= base64_encode($sess['permohonan']); ?>">
        <input type="hidden" name="SEBELUM" value="<?= $sess['status'] ?>">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"> Data Permohonan</header>
                    <div class="panel-body">
                        <section class="isolate-tabs">
                            <ul class="nav nav-tabs">
                                <li class="active"> <a data-toggle="tab" href="#first">Data Permohonan</a> </li>
                                <li class=""> <a data-toggle="tab" href="#barang">Data Barang</a> </li>
                                <li class=""> <a data-toggle="tab" href="#second">Data Pemilik/Penanggung Jawab</a> </li>
                                <li class=""> <a data-toggle="tab" href="#third">Data Prinsipal Produsen dan Supplier</a> </li>
                                <li class=""> <a data-toggle="tab" href="#fourth">Data Persyaratan</a> </li>
                            </ul>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div id="first" class="tab-pane active">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <b>Data Permohonan</b></header>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Status Permohonan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['permohonan']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Produk<font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['produk']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Jenis Produksi <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['produksi']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Status<font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['jenis_agen']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                          <div class="row">
                                                            <label class="col-sm-3 control-label">Nomor Pengajuan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                              <?= $sess['no_aju']; ?>
                                                            </div>
                                                          </div>
                                                          <div style="height:5px;"></div>
                                                          <div class="row">
                                                            <label class="col-sm-3 control-label">Tanggal Pengajuan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                              <?= $sess['tgl_aju']; ?>
                                                            </div>
                                                          </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <strong><b>Data Perusahaan</b></strong></header>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">NPWP <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['npwp']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Bentuk Usaha <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['tipe_perusahaan']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['nm_perusahaan']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['almt_perusahaan']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['prop_perusahaan']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kab_perusahaan']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kec_perusahaan']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kel_perusahaan']; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kode Pos</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kdpos']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Telp/HP</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['telp']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Fax</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['fax']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Bidang Usaha <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['bidang_usaha']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Pemasaran <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['pemasaran']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="barang" class="tab-pane">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <b>Data Barang</b></header>
                                                    <div class="panel-body">
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Jenis Barang <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= str_replace("\n", "<br>", $sess['jenis']); ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Merk Barang <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['merk']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Nomor HS<font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['hs']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>	
                                        </div>	
                                    </div>	
                                    <div id="second" class="tab-pane">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <b>Data Pemilik/Penanggung Jawab</b></header>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Jenis Identitas <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['identitas_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Nomor Identitas <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['noidentitas_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Jabatan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['jabatan_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Nama Lengkap <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['nama_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Tempat Lahir <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-3">
                                                                <?= $sess['tmpt_lahir_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Tanggal Lahir <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-3">
                                                                <?= date_indo($sess['tgl_lahir_pj']); ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Alamat <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['alamat_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['prop_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kab_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kec_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['kel_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Telp/Hp</label>
                                                            <div class="col-sm-2">
                                                                <?= $sess['telp_pj']; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Fax</label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['fax']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <b>Data Tenaga Kerja</b></header>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Jumlah Tenaga Kerja Nasional <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['pegawai_lokal']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Jumlah Tenaga Kerja Asing <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['pegawai_asing']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="third" class="tab-pane">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <b>Data Prinsipal Produsen</b></header>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['nm_perusahaan_prod']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Bentuk Badan Hukum <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['tipe_perusahaan_prod']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Tanggal Pendirian <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= date_indo($sess['tgl_pendirian_prod']); ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Alamat Kantor Pusat <font size ="2" color="red">*</font></label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['almt_perusahaan_prod']; ?>
                                                            </div>
                                                        </div>
                                                        <div id="prov_prod" style="<?php echo ($sess['produksi'] == '01') ? "" : "display:none;"; ?>">
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
                                                                <div class="col-sm-9">
                                                                    <?= $sess['prop_prod']; ?>
                                                                </div>
                                                            </div>
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Kabupaten/Kota <font size ="2" color="red">*</font></label>
                                                                <div class="col-sm-9">
                                                                    <?= $sess['kab_prod']; ?>
                                                                </div>
                                                            </div>
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
                                                                <div class="col-sm-9">
                                                                    <?= $sess['kec_prod']; ?>
                                                                </div>
                                                            </div>
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Kelurahan/Desa <font size ="2" color="red">*</font></label>
                                                                <div class="col-sm-9">
                                                                    <?= $sess['kel_prod']; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Telepon </label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['telp_prod']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Fax </label>
                                                            <div class="col-sm-9">
                                                                <?= $sess['fax_prod']; ?>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="row">
                                                            <div class="col-lg-12">
                                                                <section class="panel">
                                                                    <header class="panel-heading"> <b>Data Legalitas</b></header>
                                                                    <div class="panel-body">
                                                                        <div class="row col-lg-12">
                                                                            <table class="table">
                                                                                <thead>
                                                                                    <tr class="control-label">
                                                                                        <th class="control-label" width= "3%">No</th>
                                                                                        <th>Nama Dokumen</th>
                                                                                        <th>Status</th>
                                                                                        <th><center>Pilih</center></th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php
                                                                                    if (count($req) > 0):$no = 1;
                                                                                        foreach ($req_legal as $reqs):
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td align="center"><?php echo $no++; ?></td>
                                                                                                <td><?php
                                                                                                    echo $reqs['keterangan'];
                                                                                                    echo ($reqs['tipe'] == '1') ? ' <font size ="2" color="red">*</font>' : "";
                                                                                                    ?></td>
                                                                                                <td><?= $reqs['uraian']; ?></td>
                                                                                                <td>&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td nowrap="nowrap">&nbsp;</td>
                                                                                                <td colspan="3">
                                                                                                    <table width="100%" class="table table-nomargin table-condensed table-striped">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th><center>No. Dokumen</center></th>
                                                                                                        <th><center>Penerbit</center></th>
                                                                                                        <th><center>Tgl. Dokumen</center></th>
                                                                                                        <th><center>Tgl. Akhir</center></th>
                                                                                                        <th><center>File</center></th>
                                                                                            </tr> 
                                                                                            </thead>
                                                                                        <tbody <?= (int) $reqs['multi'] == 1 ? 'id="bodysyarat"' : ''; ?>>
                                                                                            <?php
                                                                                            $banyakSess = count($sess_legal[$reqs['dok_id']]);
                                                                                            if ($banyakSess > 0) {
                                                                                                for ($i = 0; $i < count($sess_legal[$reqs['dok_id']]); $i++) {
                                                                                                    ?>
                                                                                                    <tr>
                                                                                                        <td width="20%"><center>

                                                                                                    <span id="no_dok_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['nomor']; ?></span></center></td>
                                                                                                <td width="35%"><center><span id="penerbit_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['penerbit_dok']; ?></span></center></td>
                                                                                                <td width="20%"><center><span id="tgl_dok_<?= $reqs['dok_id']; ?>"><?= date_indo($sess_legal[$reqs['dok_id']][$i]['tgl_dok']); ?></span></center></td>
                                                                                                <td width="20%"><center><span id="tgl_exp_<?= $reqs['dok_id']; ?>"><?= date_indo($sess_legal[$reqs['dok_id']][$i]['tgl_exp']); ?></span></center></td>
                                                                                                <td width="10%"><center><span id="View_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['View']; ?></span></center></td>
                                                                                                </tr>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td width="20%"><center>                                                
                                                                                                <span id="no_dok_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['nomor']; ?></span></center></td>
                                                                                            <td width="35%"><center><span id="penerbit_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['penerbit_dok']; ?></span></center></td>
                                                                                            <td width="20%"><center><span id="tgl_dok_<?= $reqs['dok_id']; ?>"><?= date_indo($sess_legal[$reqs['dok_id']][$i]['tgl_dok']); ?></span></center></td>
                                                                                            <td width="20%"><center><span id="tgl_exp_<?= $reqs['dok_id']; ?>"><?= date_indo($sess_legal[$reqs['dok_id']][$i]['tgl_exp']); ?></span></center></td>
                                                                                            <td width="10%"><center><span id="View_<?= $reqs['dok_id']; ?>"><?= $sess_legal[$reqs['dok_id']][$i]['View']; ?></span></center></td>
                                                                                            </tr>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    </td>
                                                                                    <tr>
                                                                                    <?php
                                                                                    endforeach;
                                                                                else:
                                                                                    ?>
                                                                                <tr>
                                                                                    <td colspan="4"><center>
                                                                                    <b>Tidak Terdapat Data</b>
                                                                                </center></td>
                                                                                </tr>
<?php endif; ?>
                                                                            </tbody>

                                                                            </table>
                                                                        </div>                    
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div> -->
                                                    </div>                        
                                                </section>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <b>Data Prinsipal Supplier (Wajib di Isi Apabila Perjanjian Dari Prinsipal Supplier Dan Tidak Di Isi Jika Perjanjian Langsung Dengan Prinsipal Produsen)</b></header>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Nama Perusahaan </label>
                                                            <div class="col-sm-9">
<?= $sess['nm_perusahaan_supl']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Bentuk Badan Hukum </label>
                                                            <div class="col-sm-9">
<?= $sess['tipe_perusahaan_supl']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Tanggal Pendirian </label>
                                                            <div class="col-sm-9">
<?= date_indo($sess['tgl_pendirian_supl']); ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Alamat Kantor Pusat </label>
                                                            <div class="col-sm-9">
<?= $sess['almt_perusahaan_supl']; ?>
                                                            </div>
                                                        </div>
                                                        <div id="prov_supl" style="<?php echo ($sess['produksi'] == '01') ? "" : "display:none;"; ?>">
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Provinsi </label>
                                                                <div class="col-sm-9">
<?= $sess['prop_supl']; ?>
                                                                </div>
                                                            </div>
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Kabupaten/Kota </label>
                                                                <div class="col-sm-9">
<?= $sess['kab_supl']; ?>
                                                                </div>
                                                            </div>
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Kecamatan </label>
                                                                <div class="col-sm-9">
<?= $sess['kec_supl']; ?>
                                                                </div>
                                                            </div>
                                                            <div style="height:5px;"></div>
                                                            <div class="row">
                                                                <label class="col-sm-3 control-label">Kelurahan/Desa </label>
                                                                <div class="col-sm-9">
<?= $sess['kel_supl']; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Telepon </label>
                                                            <div class="col-sm-9">
<?= $sess['telp_supl']; ?>
                                                            </div>
                                                        </div>
                                                        <div style="height:5px;"></div>
                                                        <div class="row">
                                                            <label class="col-sm-3 control-label">Fax </label>
                                                            <div class="col-sm-9">
<?= $sess['fax_supl']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="fourth" class="tab-pane">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"> <b>Data Legalitas dan Persyaratan</b></header>
                                                    <div class="panel-body">
                                                        <div class="row col-lg-12">
                                                            <table class="table table-striped">
                                                                <thead>
                                                                    <tr class="control-label">
                                                                        <th class="control-label" width= "3%">No</th>
                                                                        <th>Nama Dokumen</th>
                                                                        <th width= "20%">Status</th>
                                                                        <th width= "5%">&nbsp;</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
<?php
if (count($req) > 0):$no = 1;
    foreach ($req as $reqs):
        ?>
                                                                        <td align="center"><?php echo $no++; ?></td>
                                                                        <td><?php
                                                                    echo $reqs['keterangan'];
                                                                    echo ($reqs['tipe'] == '1') ? ' <font size ="2" color="red">*</font>' : "";
                                                                    ?></td>
                                                                        <td><?= $reqs['uraian'] ?></td>
                                                                        <td><center>
                                                                            <div id="btn"> </div>
                                                                        </center></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td nowrap="nowrap"></td>
                                                                            <td colspan="3"><table width="100%" class="table table-nomargin table-condensed table-striped">
                                                                                    <tr>
                                                                                        <th><center>No. Dokumen</center></th>
                                                                                    <th><center>Penerbit</center></th>
                                                                                    <th><center>Tgl. Dokumen</center></th>
                                                                                    <th><center>Tgl. Akhir</center></th>
                                                                                    <th><center>File</center></th>
                                                                        </tr>
        <?php if ($sess_syarat):for ($i = 0; $i < count($sess_syarat[$reqs['dok_id']]); $i++): ?>
                                                                                <tr>
                                                                                    <td width="20%"><center>
                                                                                    <span id="no_dok_<?= $reqs['dok_id']; ?>">
                                                                                        <?= $sess_syarat[$reqs['dok_id']][$i]['nomor']; ?>
                                                                                    </span>
                                                                                </center></td>
                                                                                <td width="35%"><center>
                                                                                    <span id="penerbit_<?= $reqs['dok_id']; ?>">
                                                                                        <?= $sess_syarat[$reqs['dok_id']][$i]['penerbit_dok']; ?>
                                                                                    </span>
                                                                                </center></td>
                                                                                <td width="20%"><center>
                                                                                    <span id="tgl_dok_<?= $reqs['dok_id']; ?>">
                                                                                        <?= date_indo($sess_syarat[$reqs['dok_id']][$i]['tgl_dok']); ?>
                                                                                    </span>
                                                                                </center></td>
                                                                                <td width="20%"><center>
                                                                                    <span id="tgl_exp_<?= $reqs['dok_id']; ?>">
                                                                                        <?= date_indo($sess_syarat[$reqs['dok_id']][$i]['tgl_exp']); ?>
                                                                                    </span>
                                                                                </center></td>
                                                                                <td width="10%"><center>
                                                                                    <span id="View_<?= $reqs['dok_id']; ?>">
                                                                                <?= ($sess_syarat[$reqs['dok_id']][$i]['upload_id'] == 0) ? "" : $sess_syarat[$reqs['dok_id']][$i]['View']; ?>
                                                                                    </span>
                                                                                </center></td>
                                                                                </tr>
            <?php
            endfor;
        else:
            ?>
                                                                            <tr>
                                                                                <td colspan="5"><center>
                                                                                <b>Tidak Terdapat Data</b>
                                                                            </center></td>
                                                                            </tr>
        <?php endif; ?>
                                                                    </table></td>
                                                                    <tr>
    <?php
    endforeach;
else:
    ?>
                                                                <tr>
                                                                    <td colspan="4"><center>
                                                                    <b>Tidak Terdapat Data</b>
                                                                </center></td>
                                                                </tr>
<?php endif; ?>
                                                            </tbody>

                                                            </table>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading"> Data Proses Permohonan </header>
                    <div class="panel-body">
                                <?= $input; ?>
                                    <?php
                                    if (count($proses) > 0) {
                                        ?>
                            <div class="row">
                                <div class="col-md-12">
    <?php if ($sess['status'] != '1000') { ?>
                                        <div class="form-group">
                                                    <?php if ($this->newsession->userdata('role') != '05') { ?>
                                                <label for="catatan">Catatan</label>
                                                <textarea class="jqte-catatan form-control mb-10" name="catatan"  id="catatan"></textarea>
        <?php } ?>
                                            <label class="checkbox-custom inline check-success">
                                                <input value="1" id="checkbox-agreement" type="checkbox" check-agreement="yes"> 
                                                <label for="checkbox-agreement">
                                        <?php echo $agrement ?>
                                                </label>
                                            </label>
                                        </div>
                                        <?php
                                    }
                                    if (count($proses) > 0) {
                                        foreach ($proses as $x) {
                                            echo $x . ' ';
                                        }
                                        if ($proses[5] == "") {
                                            echo $cetak;
                                        }
                                    }
                                    ?>

                                </div>
                            </div>
                            <?php
                        } else {
                            ?>
                            <button class="btn btn-sm btn-info addon-btn m-b-10" onclick="javascript:window.history.back(); return false;"><i class="fa fa-info pull-right"></i>Kembali</button>
    <?php
}
?>
                        <div class="row">&nbsp;</div>
                        <div class="row">
                            <div class="col-md-12">
                                <section class="panel-timeline">
                                    <div class="time-line-wrapper">
                                        <div class="time-line-caption">
                                            <h3 class="time-line-title">Histori Permohonan <sup><a href="javascript:void(0);" id="<?= rand(); ?>" class="log-izin" data-url = "<?= $urllog; ?>" data-target="#tmplog"><span class="badge bg-info"> <?= $jmllog; ?> </span></a></sup></h3>
                                        </div>
                                        <div id="tmplog"><div>
                                            </div>
                                            </section>
                                        </div>
                                    </div>
                            </div>
                            </section>
                        </div>
                    </div>
                    </form>
            </div>
            <script src="<?= base_url(); ?>assets/bend/js/a.js?v=<?= date('Y-m-d-s'); ?>"></script>
<?php if (in_array($sess['status'], array('1000')) === false) { ?>
                <script>
                                $(document).ready(function () {
                                    if (<?= $this->newsession->userdata('role') ?> == '07') {
                                        BootstrapDialog.show({
                                            title: '',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                        });
                                    }
                                    if (<?= $this->newsession->userdata('role') ?> == '06') {
                                        BootstrapDialog.show({
                                            title: '',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                        });
                                    }
                                    if (<?= $this->newsession->userdata('role') ?> == '02') {
                                        BootstrapDialog.show({
                                            title: '',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                        });
                                    }
                                    if (<?= $this->newsession->userdata('role') ?> == '01') {
                                        BootstrapDialog.show({
                                            title: '',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: '<?php echo str_replace('\r\n', '', $telaah) ?>'
                                        });
                                    }
                                });
                </script>
<?php } ?>