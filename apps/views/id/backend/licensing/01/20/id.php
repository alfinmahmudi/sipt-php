		<div style="height:5px;"></div>
			<div class="row">
              <label class="col-sm-3 control-label">Nama Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="STPW[nm_perusahaan]"  wajib="yes" value="<?= (array_key_exists('nm_perusahaan', $sess) ? $sess['nm_perusahaan'] : $this->newsession->userdata('nm_perusahaan')); ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat Perusahaan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <textarea class="form-control mb-10" name="STPW[almt_perusahaan]"><?= array_key_exists('almt_perusahaan', $sess) ? $sess['almt_perusahaan'] : $this->newsession->userdata('almt_perusahaan'); ?>
</textarea>
              </div>
            </div>
			<div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telp/Hp</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" name="STPW[telp]" wajib="yes" value="<?= (array_key_exists('telp', $sess) ? $sess['telp'] : $this->newsession->userdata('telp')); ?>" />
              </div>
			</div>  
			 <div style="height:5px;"></div>
             <div class="row">  
              <label class="col-sm-3 control-label">Fax</label>
              <div class="col-sm-2">
                <input type="text" class="form-control mb-10" name="STPW[fax]" wajib="yes" value="<?= (array_key_exists('fax', $sess) ? $sess['fax'] : $this->newsession->userdata('fax')); ?>" />
              </div>
			</div>  