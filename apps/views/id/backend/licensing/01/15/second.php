<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewpj" name="fnewpj" autocomplete = "off" data-redirect="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Data Pemilik/Penanggung Jawab</b></header>
          <div class="panel-body">
            <input type="hidden" class="form-control mb-10" id="direktorat" name="direktorat" wajib="yes" value="<?= $direktorat; ?>" />
            <input type="hidden" class="form-control mb-10" id="id" name="id" wajib="yes" value="<?= $sess['id']; ?>" />
            <input type="hidden" class="form-control mb-10" id="kd_izin" name="kd_izin" wajib="yes" value="<?= $sess['kd_izin']; ?>" />
            <input type="hidden" class="form-control mb-10" id="tipe_permohonan" name="tipe_permohonan" wajib="yes" value="<?= $sess['tipe_permohonan']; ?>" />
            <div class="row">
              <label class="col-sm-3 control-label">Jenis Identitas <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('pj[identitas_pj]', $jenis_identitas, $this->newsession->userdata('identitas_pj'), 'id="identitas_pj" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nomor Identitas <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="pj[noidentitas_pj]" wajib="yes" readonly value="<?= ($sess['noidentitas_pj']=="")?$this->newsession->userdata('no_identitas'):$sess['noidentitas_pj']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Jabatan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
               <?= form_dropdown('pj[jabatan_pj]', $jabatan_pj, $this->newsession->userdata('jabatan_pj'), 'id="jabatan_pj" wajib="yes" class="form-control mb-10"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Nama Lengkap <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="pj[nama_pj]" wajib="yes" readonly value="<?= ($sess['nama_pj']=="")?$this->newsession->userdata('na_pj'):$sess['nama_pj']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tempat Lahir <font size ="2" color="red">*</font></label>
              <div class="col-sm-3">
                 <input type="text" class="form-control mb-10" name="pj[tmpt_lahir_pj]" wajib="yes" readonly value="<?= ($sess['tmpt_lahir_pj']=="")?$this->newsession->userdata('tmpt_lahir_pj'):$sess['tmpt_lahir_pj']; ?>" />
              </div>
              <label class="col-xs-3 control-label">Tanggal Lahir <font size ="2" color="red">*</font></label>
              <div class="col-sm-3">
                <input type="text" class="form-control mb-10 datepickers" name="pj[tgl_lahir_pj]" readonly wajib="yes" value="<?= ($sess['tgl_lahir_pj']=="")?$this->newsession->userdata('tgl_lahir_pj'):$sess['tgl_lahir_pj']; ?>" data-date-format = "yyyy-mm-dd" />
               </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Alamat <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
               <textarea class="form-control mb-10" wajib="yes" readonly name="pj[alamat_pj]"><?= ($sess['alamat_pj']=="")?$this->newsession->userdata('alamat_pj'):$sess['alamat_pj']; ?>
			   </textarea>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('pj[kdprop_pj]', $propinsi, ($sess['kdprop_pj']=="")?$this->newsession->userdata('kdprop_pj'):$sess['kdprop_pj'], 'id="kdprop_pj" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kota/" onChange = "combobox($(this), \'#kdkab_pj\'); return false;"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kabupaten / Kota <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('pj[kdkab_pj]', $kabupaten, ($sess['kdkab_pj']=="")?$this->newsession->userdata('kdkab_pj'):$sess['kdkab_pj'], 'id="kdkab_pj" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kec/" onChange = "combobox($(this), \'#kdkec_pj\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kecamatan <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('pj[kdkec_pj]', $kecamatan, ($sess['kdkec_pj']=="")?$this->newsession->userdata('kdkec_pj'):$sess['kdkec_pj'], 'id="kdkec_pj" wajib="yes" class="form-control input-sm select2" data-url = "'.site_url().'get/cb/set_kel/" onChange = "combobox($(this), \'#kdkel_pj\');"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Kelurahan / Desa <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('pj[kdkel_pj]', $kelurahan, ($sess['kdkel_pj']=="")?$this->newsession->userdata('kdkel_pj'):$sess['kdkel_pj'], 'id="kdkel_pj" wajib="yes" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Telp</label>
              <div class="col-sm-2">
                 <input type="text" class="form-control mb-10" name="pj[telp_pj]" readonly value="<?= ($sess['telp_pj']=="")?$this->newsession->userdata('telp_pj'):$sess['telp_pj']; ?>" />
              </div>
              <!-- <label class="col-xs-2 control-label">Fax</label>
              <div class="col-sm-2">
                 <input type="text" class="form-control mb-10" name="pj[fax_pj]" readonly value="<?= ($sess['fax_pj']=="")?$this->newsession->userdata('fax_pj'):$sess['fax_pj']; ?>" />
              </div> -->
            </div>
          </div>
        </section>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> <b>Lokasi Pameran/Acara</b></header>
          <div class="panel-body">
            <div class="row">
              <label class="col-sm-3 control-label">Nama Lokasi <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="pj[lokasi_pameran]" wajib="yes" value="<?= $sess['lokasi_pameran']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Provinsi <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <?= form_dropdown('pj[kdprop_pameran]', $propinsi_tmpt, $sess['kdprop_pameran'], 'id="kdprop_pameran" wajib="yes" class="form-control input-sm select2"'); ?>
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tanggal Mulai <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 datepickers" data-date-format = "yyyy-mm-dd" name="pj[awal_pameran]" wajib="yes" value="<?= $sess['awal_pameran']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Tanggal Berakhir <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10 datepickers" data-date-format = "yyyy-mm-dd" name="pj[akhir_pameran]" wajib="yes" value="<?= $sess['akhir_pameran']; ?>" />
              </div>
            </div>
            <div style="height:5px;"></div>
            <div class="row">
              <label class="col-sm-3 control-label">Judul/Tema <font size ="2" color="red">*</font></label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-10" name="pj[tema_pameran]" wajib="yes" value="<?= $sess['tema_pameran']; ?>" />
              </div>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if($sess['identitas_pj']):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewpj'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/fourth/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Data Persyaratan</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $sess['kd_izin']; ?>/<?= $sess['tipe_permohonan']; ?>/<?= $sess['id']; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectfirst" data-url = "<?= $urifirst; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> --> 
                <span class="pull-right">
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewpj'); return false;"><i class="fa fa-arrow-right"></i>Selanjutnya</button>
                </span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script><script>$(document).ready(function(e){ $(".datepickers").datepicker({autoclose: true});});</script>