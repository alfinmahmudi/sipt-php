<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/css/select2-bootstrap.css" type="text/css" cache="false" />
<link rel="stylesheet" href="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker3.min.css" type="text/css" cache="false" />
<div class="page-head">
  <h3 class="m-b-less"> Form Permohonan</h3>
  <span class="sub-title"><b>
  <?= $nama_izin;?>
  </b></span> </div>
<div class="wrapper">
  <form action="<?= $act; ?>" method="post" id="fnewsyarat" name="fnewsyarat" autocomplete = "off" data-redirect="true" data-hidden="true">
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading"> Data Legalitas dan Persyaratan</header>
          <div class="panel-body">
            <div class="row col-lg-12">
              <input type="hidden" name="SIUPMB[izin_id]" value="<?= $izin_id;?>" readonly="readonly"/>
              <input type="hidden" name="SIUPMB[permohonan_id]" value="<?= $permohonan_id;?>"/>
              <table class="table">
                <thead>
                  <tr class="control-label">
                    <th class="control-label" width= "3%">No</th>
                    <th>Nama Dokumen</th>
                    <th>Status</th>
                    <th><center>
                        Pilih
                      </center></th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(count($req) > 0):$no = 1;foreach($req as $reqs):?>
                <td align="center"><?php echo $no++; ?></td>
                  <td><?php echo $reqs['keterangan']; echo ($reqs['tipe'] =='1')?' <font size ="2" color="red">*</font>':""; ?></td>
                  <td><?= $reqs['uraian'];?></td>
                  <td><center>
                      <div id="btn">
                        <button type="button" class="btn btn-sm btn-success addon-btn m-b-10" data-url="<?= site_url(); ?>licensing/popup_syarat/<?= $reqs['dok_id']; ?>/" data-target="{upload_id_<?= $reqs['dok_id']; ?>}.{no_dok_<?= $reqs['dok_id']; ?>}.{penerbit_<?= $reqs['dok_id']; ?>}.{tgl_dok_<?= $reqs['dok_id']; ?>}.{tgl_exp_<?= $reqs['dok_id']; ?>}.{View_<?= $reqs['dok_id']; ?>}" data-title = "List Data <?= $reqs['keterangan']?>" style="cursor:pointer;" id="btn-syarat-<?= $reqs['dok_id']; ?>" data-callback = "get.search.requirements.{upload_id}.<?= $reqs['dok_id']; ?>" data-fieldcallback = "upload_id_<?= $reqs['dok_id']; ?>.no_dok_<?= $reqs['dok_id']; ?>.penerbit_<?= $reqs['dok_id']; ?>.tgl_dok_<?= $reqs['dok_id']; ?>.tgl_exp_<?= $reqs['dok_id']; ?>.View_<?= $reqs['dok_id']; ?>" onclick="popuptabel($(this));" data-multi = "<?= $reqs['multi']; ?>" <?= (int)$reqs['multi'] == 1 ? 'data-body="bodysyarat_'.$reqs["dok_id"].'" data-doc="'.$izin_id.'"' : ''; ?>><i class="fa fa-search"></i> Pilih </button>
                      </div>
                    </center></td>
                </tr>
                <tr>
                  <td nowrap="nowrap"><div id="divHapus_<?php echo $dokWajib['idPendukung']; ?>"></div></td>
                  <td colspan="3">
                    <table width="100%" class="table table-nomargin table-condensed table-striped">
                      <thead>
                        <tr>
                        <th><center>
                            No. Dokumen
                          </center></th>
                        <th><center>
                            Penerbit
                          </center></th>
                        <th><center>
                            Tgl. Dokumen
                          </center></th>
                        <th><center>
                            Tgl. Akhir
                          </center></th>
                        <th><center>
                            File
                          </center></th>
                         </tr> 
                      </thead>
                      <tbody <?= (int)$reqs['multi'] == 1 ? 'id="bodysyarat_'.$reqs["dok_id"].'"' : ''; ?>>
                        <?php $banyakSess = count($sess[$reqs['dok_id']]); if($banyakSess > 0){ 
            for($i = 0; $i < count($sess[$reqs['dok_id']]); $i++){ 
            ?>
                      <tr>
                        <td width="20%"><center>
                        <input type="hidden" id="id[]" name="REQUIREMENTS[id][]" value="<?= $sess[$reqs['dok_id']][$i]['id'];?>" readonly="readonly"/>
                        <?php
            //if((int)$reqs['multi'] == 0){
            ?>
                        <input type="hidden" name="REQUIREMENTS[dok_id][]" value="<?= $reqs['dok_id'];?>" readonly="readonly"/>
                        <input type="hidden" name="REQUIREMENTS[upload_id][]" <?= ($reqs['tipe'] =='1')?'wajib="yes"':""; ?> id="upload_id_<?= $reqs['dok_id']; ?>" readonly="readonly" value="<?= $sess[$reqs['dok_id']][$i]['upload_id'];?>"/>
                        <?php
            //}
            ?>
                        <span id="no_dok_<?= $reqs['dok_id']; ?>"><?= $sess[$reqs['dok_id']][$i]['nomor'];?></span></center></td>
                        <td width="35%"><center><span id="penerbit_<?= $reqs['dok_id']; ?>"><?= $sess[$reqs['dok_id']][$i]['penerbit_dok'];?></span></center></td>
                        <td width="20%"><center><span id="tgl_dok_<?= $reqs['dok_id']; ?>"><?= $sess[$reqs['dok_id']][$i]['tgl_dok'];?></span></center></td>
                        <td width="20%"><center><span id="tgl_exp_<?= $reqs['dok_id']; ?>"><?= $sess[$reqs['dok_id']][$i]['tgl_exp'];?></span></center></td>
                        <td width="10%"><center><span id="View_<?= $reqs['dok_id']; ?>"><?= ($sess[$reqs['dok_id']][$i]['upload_id']==0)?"":$sess[$reqs['dok_id']][$i]['View'];?></span></center></td>
                      </tr>
                      <?php
            } }else{
            ?>
            <tr>
                        <td width="20%"><center>
                        <input type="hidden" id="id[]" name="REQUIREMENTS[id][]" value="<?= $sess[$reqs['dok_id']][$i]['id'];?>" readonly="readonly"/>
                        <?php
            //if((int)$reqs['multi'] == 0){
            ?>
                        <input type="hidden" name="REQUIREMENTS[dok_id][]" value="<?= $reqs['dok_id'];?>" readonly="readonly"/>
                        <input type="hidden" name="REQUIREMENTS[upload_id][]" <?= ($reqs['tipe'] =='1')?'wajib="yes"':""; ?> id="upload_id_<?= $reqs['dok_id']; ?>" readonly="readonly" value="<?= $sess[$reqs['dok_id']][$i]['upload_id'];?>"/>
                        <?php
            //}
            ?>
                        <span id="no_dok_<?= $reqs['dok_id']; ?>"><?= $sess[$reqs['dok_id']][$i]['nomor'];?></span></center></td>
                        <td width="35%"><center><span id="penerbit_<?= $reqs['dok_id']; ?>"><?= $sess[$reqs['dok_id']][$i]['penerbit_dok'];?></span></center></td>
                        <td width="20%"><center><span id="tgl_dok_<?= $reqs['dok_id']; ?>"><?= $sess[$reqs['dok_id']][$i]['tgl_dok'];?></span></center></td>
                        <td width="20%"><center><span id="tgl_exp_<?= $reqs['dok_id']; ?>"><?= $sess[$reqs['dok_id']][$i]['tgl_exp'];?></span></center></td>
                        <td width="10%"><center><span id="View_<?= $reqs['dok_id']; ?>"><?= ($sess[$reqs['dok_id']][$i]['upload_id']==0)?"":$sess[$reqs['dok_id']][$i]['View'];?></span></center></td>
                      </tr>
            <?php
            }
            ?>
                      </tbody>
                    </table>
                    </td>
                <tr>
                  <?php endforeach; else: ?>
                <tr>
                  <td colspan="4"><center>
                      <b>Tidak Terdapat Data</b>
                    </center></td>
                </tr>
                <?php endif; ?>
                  </tbody>
                
              </table>
            </div>
            <div style="height:25px;"></div>
            <div class="row">
              <div class="col-sm-12">
                <?php if(!$baru):?>
                <div class="btn-toolbar" role="toolbar">
                  <div class="btn-group" role="group">
                    <button class="btn btn-success btn-sm addon-btn m-b-10" type="button" onclick="post('#fnewsyarat'); return false;" value="Edit Data" data-modal="true"><i class="fa fa-check"></i> Update </button>
                  </div>
                  <div class="btn-group dropup">
                    <button class="btn btn-sm btn-primary addon-btn m-b-10 dropdown-toggle info-number" type="button" data-toggle="dropdown" aria-expanded="false" style="height:30px;"><span class="fa fa-list"></span> Form Permohonan </button>
                    <ul role="menu" class="dropdown-menu">
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/first/<?= $direktorat; ?>/<?= $izin_id; ?>/<?= $type; ?>/<?= $permohonan_id; ?>" onclick="redirect($(this)); return false;">Data Permohonan</a></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/form/second/<?= $direktorat; ?>/<?= $izin_id; ?>/<?= $type; ?>/<?= $permohonan_id; ?>" onclick="redirect($(this)); return false;">Data Pemilik/Penanggung Jawab</a></li>
                      <li class="divider"></li>
                      <li><a href="javascript:void(0);" id="<?= rand(); ?>" data-url = "<?= site_url(); ?>licensing/preview/<?= $direktorat; ?>/<?= $izin_id; ?>/<?= $type; ?>/<?= $permohonan_id; ?>" onclick="redirect($(this)); return false;">Preview Permohonan</a></li>
                    </ul>
                  </div>
                </div>
                <?php else:?>
                <!-- <span class="pull-left">
                <button type="button" class="btn btn-sm btn-danger addon-btn m-b-10" onclick="redirect($(this)); return false;" id="btnredirectthird" data-url = "<?= $urithird; ?>"><i class="fa fa-arrow-left"></i> Kembali </button>
                </span> --> <span class="pull-right">
                <button class="btn btn-sm btn-success addon-btn m-b-10" onclick="post('#fnewsyarat'); return false;"><i class="fa fa-arrow-right"></i>Selesai</button>
                </span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </form>
</div>
<script src="<?= base_url(); ?>assets/bend/js/select2.js"></script><script src="<?= base_url(); ?>assets/bend/js/select2-init.js"></script><script src="<?= base_url(); ?>assets/bend/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script><script> $(document).ready(function(e){ $(".datepickers").datepicker({autoclose: true }); });</script>