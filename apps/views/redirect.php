<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="author" content="PDN" />
<meta name="keyword" content="PDN" />
<meta name="description" content="" />
<link rel="shortcut icon" href="<?= base_url(); ?>assets/favicon.png"/>
<link href="<?= base_url(); ?>assets/bend/css/style.css" rel="stylesheet">
<link href="<?= base_url(); ?>assets/bend/css/style-responsive.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="<?= base_url(); ?>assets/bend/js/html5shiv.min.js"></script>
<script src="<?= base_url(); ?>assets/bend/js/respond.min.js"></script>
<![endif]-->
</head>
<body class="body-500">
<section class="error-wrapper">
  <div class="text-center">
    <h2 class="green-bg">Notifikasi</h2>
  </div>
  <p class="text-center">
    <?= $message; ?>
  </p>
  <a href="<?= $url; ?>" class="back-btn">Kembali</a></section>
</body>
</html>
<?//= redirect(''.$url.$this->uri->segment($seg),'refresh',302, 10000);?>