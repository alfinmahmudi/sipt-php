<?php
$this->load->library('newphpexcel');
					$this->newphpexcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
					$this->newphpexcel->getDefaultStyle()->getFont()->setName('Calibri')->setSize(10);
					$arrheader = array('font' => array('bold' => true),
									   'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
														  'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
														  'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
										'fill' => array('type' => (PHPExcel_Style_Fill::FILL_SOLID), 
										'color' => array('rgb' => 'C0C0C0')));
					$this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A1', 'NO')
															  ->setCellValue('B1', 'No Pendaftaran')
															  ->setCellValue('C1', 'Tanggal Daftar')
															  ->setCellValue('D1', 'Tanggal Proses')
															  ->setCellValue('E1', 'NPWP Perusahaan')
															  ->setCellValue('F1', 'Nama Perusahaan')
															  ->setCellValue('G1', 'No Izin')
															  ->setCellValue('H1', 'Tanggal Izin')
															  ->setCellValue('I1', 'Daftar Via');
					$this->newphpexcel->getActiveSheet()
						 ->getStyle( $this->newphpexcel->getActiveSheet()->calculateWorksheetDimension() )
						 ->getAlignment()
						 ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);  
					$this->newphpexcel->getActiveSheet()->getStyle('A1')->applyFromArray($arrheader);
					$this->newphpexcel->getActiveSheet()->getStyle('B1')->applyFromArray($arrheader);
					$this->newphpexcel->getActiveSheet()->getStyle('C1')->applyFromArray($arrheader);
					$this->newphpexcel->getActiveSheet()->getStyle('D1')->applyFromArray($arrheader);
					$this->newphpexcel->getActiveSheet()->getStyle('E1')->applyFromArray($arrheader);			
					$this->newphpexcel->getActiveSheet()->getStyle('F1')->applyFromArray($arrheader);			
					$this->newphpexcel->getActiveSheet()->getStyle('G1')->applyFromArray($arrheader);			
					$this->newphpexcel->getActiveSheet()->getStyle('H1')->applyFromArray($arrheader);			
					$this->newphpexcel->getActiveSheet()->getStyle('I1')->applyFromArray($arrheader);		
					$this->newphpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
					$this->newphpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
					$this->newphpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
					$this->newphpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
					$this->newphpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
					$this->newphpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
					$this->newphpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
					$this->newphpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
					$this->newphpexcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
					
					//Get Data
					
					$i=2;
					$no=1;
					foreach($data as $row ){
						$this->newphpexcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $no)
																  ->setCellValue('B'.$i, $row['No_Pendaftaran'])
																  ->setCellValue('C'.$i, $row['Tanggal_Daftar'])
															      ->setCellValue('D'.$i, $row['Tanggal Proses'])
															      ->setCellValue('E'.$i, $row['NPWP_Perusahaan'])
															      ->setCellValue('F'.$i, $row['Nama_Perusahaan'])
															      ->setCellValue('G'.$i, $row['No_Izin'])
															      ->setCellValue('H'.$i, $row['Tanggal_Izin'])
															      ->setCellValue('I'.$i, 'Daftar Via');
						$i++;									  
						$no++;									  
					}
					
					
					ob_clean();
					$file = "test.xls";
					header("Content-type: application/x-msdownload");
					header("Content-Disposition: attachment;filename=$file");
					header("Cache-Control: max-age=0");
					header("Pragma: no-cache");
					header("Expires: 0");
					$objWriter = PHPExcel_IOFactory::createWriter($this->newphpexcel, 'Excel5'); 
					$objWriter->save('php://output');
					exit();		
?>